<?php

use App\Pageviews;
use App\Core\SettingManagement;

class CronPageViewControllerTest extends TestCase {

	/**
	 * A basic functional test example.
	 *
	 * @return void
	 */
	public $cronPageViews;
	function __construct(){
		$this->cronPageViews = new Pageviews();
	}

	public function testGetSettingShouldBeThree(){
		$arrSetting = $this->cronPageViews->getSetting();
		$this->assertEquals(3, count($arrSetting));
	}
	public function testGAConnect()
	{
		$ga = $this->cronPageViews->gaConnect();
		$this->assertNotEmpty($ga);
	}
	public function testGetTrickMomListDataShouldBeMoreThanZero(){
		$dt = $this->cronPageViews->getTrickMomListData();
		$this->assertGreaterThan(0, count($dt));
	}
	public function testGetTrickmomViewShouldBeGetArrayPageViews(){
		$ga = $this->cronPageViews->gaConnect();
		$dt = $this->cronPageViews->getTrickMomListData();

		$path = $this->cronPageViews->getLang($dt[0]->obj_lang) . "/momtip/" . $dt[0]->type_mom . "/" . $dt[0]->trick_mom_id;

		$dtViews = $this->cronPageViews->gaGetView($ga, $path);

		$this->assertArrayHasKey("totalsForAllResults", $dtViews);
	}

	public function testUpdateTrickMomListDataShouldBeOne(){
		$ga = $this->cronPageViews->gaConnect();
		$dt = $this->cronPageViews->getTrickMomListData();

		$path = $this->cronPageViews->getLang($dt[0]->obj_lang) . "/momtip/" . $dt[0]->type_mom . "/" . $dt[0]->trick_mom_id;

		$dtViews = $this->cronPageViews->gaGetView($ga, $path);
		$dtUpdate = $this->cronPageViews->updateTrickMomDataViews($dt[0]->trick_mom_id, $dt[0]->obj_lang, $dtViews["totalsForAllResults"]["ga:pageviews"]);
		$this->assertEquals(1, count($dtUpdate));
	}

	public function testUpdateTrickMomViewsAllData(){
		$countRow = $this->cronPageViews->momtipViews();
		$this->assertGreaterThan(0, $countRow);
	}

	public function testGetPromotionListDataShouldBeMoreThanZero(){
		$dt = $this->cronPageViews->getPromotionListData();
		$this->assertGreaterThan(0, count($dt));
	}
	public function testGetPromotionViewShouldBeGetArrayPageViews(){
		$ga = $this->cronPageViews->gaConnect();
		$dt = $this->cronPageViews->getPromotionListData();

		$path = $this->cronPageViews->getLang($dt[0]->obj_lang) . "/promotion/detail/" . $dt[0]->promotion_id;

		$dtViews = $this->cronPageViews->gaGetView($ga, $path);

		$this->assertArrayHasKey("totalsForAllResults", $dtViews);
	}

	public function testUpdatePromotionDataShouldBeOne(){
		$ga = $this->cronPageViews->gaConnect();
		$dt = $this->cronPageViews->getPromotionListData();

		$path = $this->cronPageViews->getLang($dt[0]->obj_lang) . "/promotion/detail/" . $dt[0]->promotion_id;

		$dtViews = $this->cronPageViews->gaGetView($ga, $path);

		$dtUpdate = $this->cronPageViews->updateTrickMomDataViews($dt[0]->promotion_id, $dt[0]->obj_lang, $dtViews["totalsForAllResults"]["ga:pageviews"]);
		$this->assertEquals(1, count($dtUpdate));
	}

	public function testUpdatePromotionViewsAllData(){
		$countRow = $this->cronPageViews->promotionViews();
		$this->assertGreaterThan(0, $countRow);
	}

	public function testGetKnowledgeListDataShouldBeMoreThanZero(){
		$dt = $this->cronPageViews->getKnowledgeListData();
		$this->assertGreaterThan(0, count($dt));
	}
	public function testGetKnowledgeViewShouldBeGetArrayPageViews(){
		$ga = $this->cronPageViews->gaConnect();
		$dt = $this->cronPageViews->getKnowledgeListData();

		$path = $this->cronPageViews->getLang($dt[0]->obj_lang) . "/knowledge/detail/" . $dt[0]->knowledge_list_id;

		$dtViews = $this->cronPageViews->gaGetView($ga, $path);

		$this->assertArrayHasKey("totalsForAllResults", $dtViews);
	}

	public function testUpdateKnowledgeListDataShouldBeOne(){
		$ga = $this->cronPageViews->gaConnect();
		$dt = $this->cronPageViews->getKnowledgeListData();

		$path = $this->cronPageViews->getLang($dt[0]->obj_lang) . "/knowledge/detail/" . $dt[0]->knowledge_list_id;

		$dtViews = $this->cronPageViews->gaGetView($ga, $path);

		$dtUpdate = $this->cronPageViews->updateTrickMomDataViews($dt[0]->knowledge_list_id, $dt[0]->obj_lang, $dtViews["totalsForAllResults"]["ga:pageviews"]);
		$this->assertEquals(1, count($dtUpdate));
	}

	public function testUpdateKnowledgeViewsAllData(){
		$countRow = $this->cronPageViews->knowledgeViews();
		$this->assertGreaterThan(0, $countRow);
	}


}
