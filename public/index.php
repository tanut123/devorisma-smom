<?php
ini_set('display_errors', 'Off');
session_start();

//$t1 = time_profile("Laravel Index Start");

if(preg_match('/index\.php/i', $_SERVER['REQUEST_URI']) === 1){
	header('HTTP/1.0 404 Not Found');
	header('status: 404 Not Found');
	echo "404 Not Found";
	exit;
}
$check_pattern = "/".str_replace(".","\.",$_SERVER["SERVER_NAME"])."/";
if ($_SERVER['REQUEST_METHOD'] == "POST" && !preg_match($check_pattern,$_SERVER["HTTP_REFERER"])) {
	header('HTTP/1.0 404 Not Found');
	header('status: 404 Not Found');
	echo "404 Not Found";
	exit;
}
/**
 * Laravel - A PHP Framework For Web Artisans
 *
 * @package  Laravel
 * @author   Taylor Otwell <taylorotwell@gmail.com>
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| Composer provides a convenient, automatically generated class loader for
| our application. We just need to utilize it! We'll simply require it
| into the script here so that we don't have to worry about manual
| loading any of our classes later on. It feels nice to relax.
|
*/

require __DIR__.'/../bootstrap/autoload.php';

//$t2 = time_profile("Laravel Index S1",$t1);
/*
|--------------------------------------------------------------------------
| Turn On The Lights
|--------------------------------------------------------------------------
|
| We need to illuminate PHP development, so let us turn on the lights.
| This bootstraps the framework and gets it ready for use, then it
| will load up this application so that we can run it and send
| the responses back to the browser and delight our users.
|
*/

$app = require_once __DIR__.'/../bootstrap/app.php';

/*
|--------------------------------------------------------------------------
| Run The Application
|--------------------------------------------------------------------------
|
| Once we have the application, we can simply call the run method,
| which will execute the request and send the response back to
| the client's browser allowing them to enjoy the creative
| and wonderful application we have prepared for them.
|
*/
//$t3 = time_profile("Laravel Index S2",$t2);

$kernel = $app->make('Illuminate\Contracts\Http\Kernel');


$response = $kernel->handle(
	$request = Illuminate\Http\Request::capture()
);
// var_dump($response);
// exit();
//$t4 = time_profile("Laravel Index S3",$t3);


$response->send();

//$t5 = time_profile("Laravel Index S4",$t4);

$kernel->terminate($request, $response);

/*$t6 = time_profile("Laravel Index End",$t5);

function time_profile($text,$time="") {
	$diff = "";
	if ($time == "") {
		$c_time = microtime(true);
	} else {
		$c_time = microtime(true);
		$diff = $c_time - $time;
	}
	echo "\n<!--".$text." Microtime:".$c_time.($diff != "" ? " Diff:".$diff : "")."-->";
	return $c_time;
}*/