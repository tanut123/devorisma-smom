<?php
	function gen_form_page_unicode($session="") {
		print "
			<form action='' method='post' style='margin:0lpadding:0'>
				<div>
					Force Page View Unicode : <select name='s_page_unicode'>
						<option value='1'";
						if ($session == 1) {
							print " selected";
						}
						print ">NONE</option>
						<option value='2'";
						if ($session == 2) {
							print " selected";
						}
						print ">UTF-8</option>
						<option value='3'";
						if ($session == 3) {
							print " selected";
						}
						print ">TIS-620</option>
					</select>
					<input type='submit' value='Select'>
				</div>
			</form>";	
	}
	function gen_form_db_unicode($session="") {
		print "
			<form action='' method='post' style='margin:0lpadding:0'>
				<div>
					Database Result Unicode (Reading) : <select name='s_db_unicode'>
						<option value='1'";
						if ($session == 1) {
							print " selected";
						}
						print ">NONE</option>
						<option value='2'";
						if ($session == 2) {
							print " selected";
						}
						print ">UTF-8</option>
						<option value='3'";
						if ($session == 3) {
							print " selected";
						}
						print ">TIS-620</option>
					</select>
					<input type='submit' value='Select'>
				</div>
			</form>";	
	}
	function gen_server_connect_form($suffix="") {
		print "
			<form action='' method='post' style='margin:0lpadding:0'>
				<div>
					Database Server : <input type='text' value='localhost' name='db_server$suffix'>
				</div>
				<div>
					Database Username : <input type='text' value='root' name='db_username$suffix'>
				</div>
				<div>
					Database Password : <input type='password' value='' name='db_password$suffix'>
				</div>
				<div style='text-align:center;'>
					<input type='submit' name='act$suffix' value='Connect'><input type='reset' value='Reset'>
				</div>
			</form>";
	}
	function gen_server_disconnect_form($suffix="") {
		print "
			<form action='' method='post' style='margin:0lpadding:0'>
				<div>
					<input type='hidden' name='disconnect_db' value='db_server$suffix'>
					Connected : ".$_SESSION["db_server".$suffix]." <input type='submit' name='act' value='Disconnect'>
				</div>
			</form>";
	}
	function gen_select_db_form($connect,$database_name="",$suffix="") {
			$db_name = mysqli_query($connect,"SHOW DATABASES");
			print "
				<form action='' method='post' style='margin:0lpadding:0'>
					<div>Database ";
				if ($database_name) {
					print "Selected : $database_name
					</div>
					<div>";
				}
					print "
						<select name='db_name$suffix'>";
			while ($db_name_f = mysqli_fetch_array($db_name)) {
				print "
							<option value='$db_name_f[0]'".check_database_name($database_name,$db_name_f[0]).">$db_name_f[0]</value>";
			}
			print "
						</select>
						<input type='submit' name='act' value='Select Database'>
					</div>
				</form>";	
	}
	function gen_select_table($connect,$table_name="",$suffix="") {
		$table_data = mysqli_query($connect,"SHOW TABLES");
		print "
			<form action='' method='post' style='margin:0lpadding:0'>
				<div>
					Convert Table : <select name='table_name$suffix'>";
		while ($table_data_f = mysqli_fetch_array($table_data)) {
			print "
						<option value='$table_data_f[0]'";
				if ($table_data_f[0] == $_SESSION['table_name'.$suffix]) {
					print " selected";
				}
						print ">$table_data_f[0]</option>";
		}
		print "
					</select>
					<input type='submit' name='act' value='Select Table'>
				</div>
			</form>";	
	}
	function check_database_name($a,$b) {
		if ($a==$b) {
			return " selected";
		}
	}
	function arrayList($arrayData) {
		if (count($arrayData) > 0) {
		print "
			<div style='padding-left:30px;padding-top:10px;padding-bottom:10px;border-top:1px #000 solid;border-bottom:1px #000 solid;'>";
		foreach ($arrayData as $key => $val ) {
			print  "
				<div style='padding-left:30px;'>
					<b>$key : </b>";
					if (is_array($val)) {
						print "<font color='#CC0000'>$val</font>";
						arrayList($arrayData[$key]);
					} else {
						print "<font color='#009900'>$val</font>";	
					}
				print "
				</div>";
		}
		print "
			</div>";
		} else {
			print "<font color='#009900'>Input Incorrect.</font>";	
		}
	}
?>