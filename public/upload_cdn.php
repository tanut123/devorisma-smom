<?php
	if (!defined('DS')) {
		define('DS', DIRECTORY_SEPARATOR);
	}
	if (!defined('ROOT_DIR')) {
		$__file__ = explode( "/public".DS , __FILE__ );
	    define('ROOT_DIR', $__file__[0] );
	}

	require ROOT_DIR . "/vendor/autoload.php";

	use OpenCloud\Rackspace;
	define("RACKSPACE_USER", "smomclubnwaewpatch");
	define("RACKSPACE_APIKEY", "32185c69318a4977abb55aa012069ff8");
	define("RACKSPACE_REGION", "HKG");

	$client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
	    'username' => RACKSPACE_USER,
	    'apiKey'   => RACKSPACE_APIKEY
	),
    [
        // Guzzle ships with outdated certs
        Rackspace::SSL_CERT_AUTHORITY => 'system',
        Rackspace::CURL_OPTIONS => [
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
        ],
    ]);
	$log = ROOT_DIR .'/public/stocks/logs/upload2cdn/';
	if(!is_dir($log)){
		mkdir($log,0777,true);
	}

	$logname = $log."log".date("Ymd").".log";
	file_put_contents($logname, "-------------Start---------------\r\n",FILE_APPEND);

	if(!isset($argv[1])){
		echo "Invalid command";
		file_put_contents($logname, "Invalid command\r\n",FILE_APPEND);
		exit();
	}



	$objectStoreService = $client->objectStoreService(null, RACKSPACE_REGION);
	$container = $objectStoreService->getContainer('s-momclub.com');
	if(!isset($argv[2])){
		$dir = ROOT_DIR .'/public/' . htmlspecialchars($argv[1],ENT_QUOTES, 'UTF-8') . '/';

		if(!is_dir($dir)){
			echo "Folder not found::" . $dir;
			file_put_contents($logname, "Folder not found::\r\n" .$dir,FILE_APPEND);
			exit();
		}
		$results = getDirContents($dir,$container);
	}else{
		try {
			$localFileName = $argv[1];
			if(is_file($localFileName)){
				$remoteFileName = str_replace(ROOT_DIR."/public", '', $localFileName);
				$splitFolder = explode('/',$remoteFileName);
				$lastIndex = count($splitFolder) - 1;
				$splitFolder[$lastIndex] = rawurlencode($splitFolder[$lastIndex]);
				$remoteFileName = implode('/', $splitFolder);
				$handle = fopen($localFileName, 'r');
				$upload = $container->uploadObject($remoteFileName, $handle);
				fclose($handle);

				if(count($upload->getPublicUrl()) > 0){
					// echo "OK::" . $remoteFileName. "\r\n";
					file_put_contents($logname, "OK::" . $remoteFileName. "\r\n",FILE_APPEND);
				}else{
					// echo "ERROR::" . $remoteFileName. "\r\n";
					file_put_contents($logname, "ERROR::" . $remoteFileName. "\r\n",FILE_APPEND);
				}
			}else{
				// echo "NOT FOUND::" . $remoteFileName . "\r\n<br/>";
				file_put_contents($logname, "NOT FOUND::" . $localFileName. "\r\n",FILE_APPEND);
			}

		} catch (Exception $e) {
			file_put_contents($logname, "ERROR::PATH::" .$localFileName. "Message::" . $e->getMessage(). "\r\n",FILE_APPEND);
		}
	}
	file_put_contents($logname, "-------------End---------------\r\n",FILE_APPEND);

	function getDirContents($dir, $container){
		global $logname;
	    $files = scandir($dir);
	    foreach($files as $key => $value){
	        $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
	        if(!is_dir($path)) {
	            $results[] = $valFile = $path;
	            if(strpos($valFile, ".htaccess")){
					continue;
				}else if(strpos($valFile, ".DS_Store")){
					continue;
				}else{
					$localFileName  = $valFile;

					try {

						if(is_file($localFileName)){
							$remoteFileName = str_replace(ROOT_DIR."/public", '', $valFile);
							$splitFolder = explode('/',$remoteFileName);
							$lastIndex = count($splitFolder) - 1;
							$splitFolder[$lastIndex] = rawurlencode($splitFolder[$lastIndex]);
							$remoteFileName = implode('/', $splitFolder);
							$handle = fopen($localFileName, 'r');
							$upload = $container->uploadObject($remoteFileName, $handle);
							fclose($handle);

							if(count($upload->getPublicUrl()) > 0){
								// echo "OK::" . $remoteFileName. "\r\n";
								file_put_contents($logname, "OK::" . $localFileName . "\r\n",FILE_APPEND);
							}else{
								// echo "ERROR::" . $remoteFileName. "\r\n";
								file_put_contents($logname, "ERROR::" . $remoteFileName. "\r\n",FILE_APPEND);
							}
						}else{
							// echo "NOT FOUND::" . $remoteFileName . "\r\n<br/>";
							file_put_contents($logname, "NOT FOUND::" . $localFileName. "\r\n",FILE_APPEND);
						}

					} catch (Exception $e) {
						file_put_contents($logname, "ERROR::PATH::" .$localFileName. "\r\n::Message::" . $e->getMessage(). "\r\n",FILE_APPEND);
					}
				}
	        } else if($value != "." && $value != "..") {
	            getDirContents($path, $container);
	            // folder
	            $results[] = $path;
	        }
	    }

	    return $results;
	}
?>