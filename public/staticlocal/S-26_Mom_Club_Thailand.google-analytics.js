(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-62402337-1', 'auto');
ga('require', 'displayfeatures');
ga('set', 'anonymizeIp', true);
ga('send', 'pageview');

ga('create', 'UA-62774158-1', 'auto', {'name': 'rolloutTracker'});
ga('rolloutTracker.require', 'displayfeatures');
ga('rolloutTracker.set', 'anonymizeIp', true);
ga('rolloutTracker.set', 'dimension2', 'Thailand');
ga('rolloutTracker.set', 'dimension3', 'Maternal and Infant Nutrition');
ga('rolloutTracker.set', 'dimension4', 'S-26');
ga('rolloutTracker.set', 'dimension5', 'S-26 Mom Club Thailand');
ga('rolloutTracker.set', 'dimension6', 'Brand');
ga('rolloutTracker.set', 'dimension7', 'Site');
ga('rolloutTracker.set', 'dimension9', 'Thai');
ga('rolloutTracker.set', 'dimension10', 'Php');
ga('rolloutTracker.set', 'dimension12', '7309');
ga('rolloutTracker.send', 'pageview');

ga('create', 'UA-27534376-1', 'auto', {'name': 'globalTracker'});
ga('globalTracker.require', 'displayfeatures');
ga('globalTracker.set', 'anonymizeIp', true);
ga('globalTracker.set', 'dimension1', 'AOA');
ga('globalTracker.set', 'dimension2', 'Thailand');
ga('globalTracker.set', 'dimension3', 'Maternal and Infant Nutrition');
ga('globalTracker.set', 'dimension4', 'S-26');
ga('globalTracker.set', 'dimension5', 'S-26 Mom Club Thailand');
ga('globalTracker.set', 'dimension6', 'Brand');
ga('globalTracker.set', 'dimension7', 'Site');
ga('globalTracker.set', 'dimension8', location.pathname.substr(0,63));
ga('globalTracker.set', 'dimension9', 'Thai');
ga('globalTracker.set', 'dimension10', 'Php');
ga('globalTracker.set', 'dimension12', '7309');
ga('globalTracker.send', 'pageview', '/aoa/thailand/s-26 mom club thailand');