﻿<?php
if (!$conn1 = mysqli_connect("mysql.orisma.alpha","devorisma","0rism@**","NES15A_SMOMCLUB")) {
	echo "Connection fail.";
	exit();
}
mysqli_query($conn1,"SET NAMES UTF8");
mysqli_query($conn1,"SET character_set_results=utf8");
mysqli_query($conn1,"SET character_set_client=utf8");
mysqli_query($conn1,"SET character_set_connection=utf8");

$knowledge[1]["title"] = "มีอะไรซ่อนอยู่ในการนอนของเด็ก?";
$knowledge[2]["title"] = "การนอน เป็นตัววัดความฉลาดในอนาคตจริงหรือ?";
$knowledge[3]["title"] = "รู้หรือไม่? สมองลูกไม่เคยหลับ";
$knowledge[4]["title"] = "สมองลูกน้อยตื่นตัว และพร้อมเรียนรู้แม้ ในยาม...";
$knowledge[5]["title"] = "ร่วมเป็นส่วนหนึ่งในการแชร์ความมหัศจรรย์";
$knowledge[6]["title"] = "ท้องผูก..ดูแลอย่างไร";
$knowledge[7]["title"] = "4 ข้อสำคัญ...การเก็บนมผงให้ถูกวิธี";
$knowledge[8]["title"] = "แม่รู้มั้ย! การนอนของลูกช่วยพัฒนาสมองอย่างไร";
$knowledge[9]["title"] = "มีอะไรซ่อนอยู่ในการนอนของเด็ก";
$knowledge[10]["title"] = "ความจริง VS ความเชื่อ ? หากนอนไม่พอตอนเด็ก เสี่ยงเกิดโรค";
$knowledge[11]["title"] = "ทุกตำราชี้ชัด สมองลูกพัฒนาได้แม้ยามหลับ";
$knowledge[12]["title"] = "ลูกน้อยนอนกลางวัน...สิ่งที่ควรรู้เมื่อลูกนอนหลับ";
$knowledge[13]["title"] = "ทําไมลูกน้อยถึงต้องนอนมากๆ? แล้วการนอนหลับมีผลกับความฉลาดของลูกอย่างไร?";
$knowledge[14]["title"] = "สวนผักเล็กๆกับความสุขใหญ่ๆ ของลูกน้อย...สามารถเชื่อมโยงไปสู่การสร้างกระบวนการเรียนรู้";
$knowledge[15]["title"] = "แม่ท้องกับการนอนที่ดี...การนอนที่ดี จะทำให้ลูกในครรภ์นอนหลับได้ดี";
$knowledge[16]["title"] = "สนุกและเสริมทักษะไปกับหนัง...เพื่อเสริมสร้างจินตนาการและพัฒนาการ";
$knowledge[17]["title"] = "เมื่อต้องเป็น Working Mom...ต้องเตรียมอะไร อย่างไรบ้าง???";
$knowledge[18]["title"] = "เคล็บลับอกสวยหลังหย่านม";

$knowledge[1]["view"] = 59843;
$knowledge[2]["view"] = 79436;
$knowledge[3]["view"] = 80145;
$knowledge[4]["view"] = 79901;
$knowledge[5]["view"] = 34586;
$knowledge[6]["view"] = 45876;
$knowledge[7]["view"] = 49193;
$knowledge[8]["view"] = 77412;
$knowledge[9]["view"] = 89899;
$knowledge[10]["view"] = 85988;
$knowledge[11]["view"] = 96532;
$knowledge[12]["view"] = 33123;
$knowledge[13]["view"] = 95521;
$knowledge[14]["view"] = 21254;
$knowledge[15]["view"] = 25132;
$knowledge[16]["view"] = 21228;
$knowledge[17]["view"] = 25695;
$knowledge[18]["view"] = 29732;



$magazine[1]["title"]="ฉบับเดือน มกราคม-มีนาคม 2557";
$magazine[2]["title"]="ฉบับเดือน เมษายน-มิถุนายน 2557";
$magazine[3]["title"]="ฉบับเดือน กรกฎาคม-กันยายน 2557";
$magazine[4]["title"]="ฉบับเดือน ตุลาคม-ธันวาคม 2557";
$magazine[5]["title"]="ฉบับเดือน มกราคม-มีนาคม 2558";
$magazine[6]["title"]="ฉบับเดือน เมษายน-มิถุนายน 2558";
$magazine[7]["title"]="ฉบับเดือน กรกฎาคม-กันยายน 2558";

$magazine[1]["view"]=3452;
$magazine[2]["view"]=4315;
$magazine[3]["view"]=3988;
$magazine[4]["view"]=4689;
$magazine[5]["view"]=5413;
$magazine[6]["view"]=5567;
$magazine[7]["view"]=5278;

$trick_mom[1]["title"] = "เมื่อคุณแม่ตั้งครรภ์";
$trick_mom[2]["title"] = "กระตุ้นสมอง สร้างลูกฉลาดตั้งแต่ในครรภ์";
$trick_mom[3]["title"] = "คุณพ่อจ๋าเข้าใจหน่อย";
$trick_mom[4]["title"] = "เทคนิคเสริมสร้างพัฒนาการลูกน้อยในครรภ์";
$trick_mom[5]["title"] = "แม่ท้องอย่ากังวล อาการแบบนี้ไม่มีปัญหา";
$trick_mom[6]["title"] = "อัลตร้าซาวด์ ประโยชน์มากกว่าที่คิด";
$trick_mom[7]["title"] = "7 วิธีสร้างความสุขยามตั้งครรภ์";
$trick_mom[8]["title"] = "6 ท่าบริหารร่างกายช่วยคุณแม่ท้องคลายกังวล";
$trick_mom[9]["title"] = "5 สิ่งที่ต้องเตรียมพร้อมก่อนคลอด";
$trick_mom[10]["title"] = "4 สัญญาณเจ็บท้องคลอดจริง ที่คุณแม่ต้องรู้";
$trick_mom[11]["title"] = "คุณแม่ตั้งครรภ์รู้หรือไม่ “น้ำหนักแม่” ใครว่าไม่สำคัญ";
$trick_mom[12]["title"] = "Sleep Secret…เพื่อคุณหนู";
$trick_mom[13]["title"] = "คุณส่งเสริมให้ลูกน้อยของคุณ ฉลาดแล้วหรือยัง ?";
$trick_mom[14]["title"] = "ลูกน้อยเล่นของเล่นที่ส่งเสริมพัฒนาการแล้วหรือยัง?";
$trick_mom[15]["title"] = "ความรู้เกี่ยวกับทารกในขวบปีแรกที่แม่ควรรู้";
$trick_mom[16]["title"] = "สร้างภูมิคุ้มกัน...เพื่อวันเติบโต";
$trick_mom[17]["title"] = "New Look for New Mom";
$trick_mom[18]["title"] = "10 เรื่องโภชนาการเพื่อสุขภาพที่ดีของลูกน้อย ที่คุณแม่ต้องรู้";
$trick_mom[19]["title"] = "3 ความลงตัว : เคล็ดลับคุณแม่มือใหมให้อาหารลูกน้อย";
$trick_mom[20]["title"] = "3 อาการระบบขับถ่ายที่คุณแม่กังวล";
$trick_mom[21]["title"] = "ฟันของลูก...รู้ได้อย่างไรว่าฟันลูกขึ้นแล้ว?";
$trick_mom[22]["title"] = "เตรียมพร้อมให้นมแม่..เมื่อต้องกลับไปทำงาน";
$trick_mom[23]["title"] = "คุณแม่มือใหม่ต้องรู้! กับพัฒนาการลูกน้อยวัย 4-6 เดือน";
$trick_mom[24]["title"] = "สร้างความสุข สร้างพัฒนาการ";
$trick_mom[25]["title"] = "คุณแม่มือใหม่ต้องรู้! กับพัฒนาการลูกน้อยวัย 7-9 เดือน";
$trick_mom[26]["title"] = "คุณแม่มือใหม่ต้องรู้ ผัก 5 ชนิด สร้างน้ำนม";
$trick_mom[27]["title"] = "คุณแม่มือใหม่ต้องรู้! กับพัฒนาการลูกน้อยวัย 0-1 เดือน";
$trick_mom[28]["title"] = "คุณแม่มือใหม่ต้องรู้! พัฒนาการลูกน้อยวัย 2-3 เดือน";
$trick_mom[29]["title"] = "คุณแม่มือใหม่ต้องรู้! พัฒนาการลูกน้อยวัย 10-12 เดือน";
$trick_mom[30]["title"] = "สัมผัสกระตุ้นพัฒนาการเด็กแรกเกิด";
$trick_mom[31]["title"] = "เล่นกระตุ้นความจำลูกวัยแรกเกิด...";
$trick_mom[32]["title"] = "กล้ามเนื้อมือของหนูแข็งแรงแค่ไหนนะ...";
$trick_mom[33]["title"] = "รู้ใจเจ้าตัวเล็ก เรื่องนี้ไม่ยากที่จะเข้าใจ...";
$trick_mom[34]["title"] = "อ่านใจลูก...จากเสียงร้อง";
$trick_mom[35]["title"] = "ช่วยลูกสะอึก แหวะนม อย่างไรดี?";
$trick_mom[36]["title"] = "อย่ามองข้าม..จุดสำคัญของหนู";
$trick_mom[37]["title"] = "กลยุทธ์การอ่านเสริมสร้างสมอง";
$trick_mom[38]["title"] = "หนูเป็นนักจดจำเก่ง";
$trick_mom[39]["title"] = "ฝึกลูกตั้งไข่ เดินได้ปลอดภัยทุกก้าว";
$trick_mom[40]["title"] = "ของเล่นสุดโปรด ประโยชน์รอบด้าน";
$trick_mom[41]["title"] = "วัยของหนู..คือผู้ช่วยและผู้ให้";
$trick_mom[42]["title"] = "รู้ทันไข้สมองอักเสบเจอีในเด็ก";
$trick_mom[43]["title"] = "“โรคมือเท้าปาก” ป้องกันได้ สบายมาก";
$trick_mom[44]["title"] = "หอบหืด จากคุณพ่อคุณแม่สู่ลูกน้อย...ถ่ายทอดกันได้จริงหรือไม่";
$trick_mom[45]["title"] = "น้ำนม...คุณค่าเพื่อลูกน้อย";
$trick_mom[46]["title"] = "น้ำนมเท่าไรจึงจะพอกับลูกน้อย";
$trick_mom[47]["title"] = "6 วิธีเด็ดเคล็ด (ไม่) ลับ กระตุ้นน้ำนมแม่";
$trick_mom[48]["title"] = "ดนตรีสนุก ช่วยเพิ่มพัฒนาการ";
$trick_mom[49]["title"] = "รู้ยัง! ตารางฉีดวัคซีนลูก ปี 2558";
$trick_mom[50]["title"] = "“ของหนู” หนูรู้";
$trick_mom[51]["title"] = "10 วิธีเอาชนะลูกน้อย กินยาก ช่างเลือก";
$trick_mom[52]["title"] = "9 เคล็ดลับ แก้ปัญหาลูกกินยาก ช่างเลือก";
$trick_mom[53]["title"] = "7 เคล็ดลับแก้ปัญหาลูกชอบอมข้าว";
$trick_mom[54]["title"] = "รับมือกับไวรัสโรต้า...ก่อนพัฒนาการลูกน้อยชะงัก";
$trick_mom[55]["title"] = "ลูกทำงานบ้านได้มากกว่าที่คิด...";
$trick_mom[56]["title"] = "สอนลูกให้รักสะอาด... เริ่มอย่างไรดี ";
$trick_mom[57]["title"] = "มากินอาหารเช้า..สร้าง IQ กันเถอะ";
$trick_mom[58]["title"] = "สอนลูกน้อยให้น่ารักและมีน้ำใจ...";

$trick_mom[1]["view"] = 21890;
$trick_mom[2]["view"] = 25976;
$trick_mom[3]["view"] = 23567;
$trick_mom[4]["view"] = 25894;
$trick_mom[5]["view"] = 25980;
$trick_mom[6]["view"] = 11324;
$trick_mom[7]["view"] = 23558;
$trick_mom[8]["view"] = 25789;
$trick_mom[9]["view"] = 27312;
$trick_mom[10]["view"] = 13178;
$trick_mom[11]["view"] = 21256;
$trick_mom[12]["view"] = 17643;
$trick_mom[13]["view"] = 14198;
$trick_mom[14]["view"] = 19678;
$trick_mom[15]["view"] = 16891;
$trick_mom[16]["view"] = 16714;
$trick_mom[17]["view"] = 11449;
$trick_mom[18]["view"] = 19091;
$trick_mom[19]["view"] = 27167;
$trick_mom[20]["view"] = 24190;
$trick_mom[21]["view"] = 11119;
$trick_mom[22]["view"] = 15665;
$trick_mom[23]["view"] = 13764;
$trick_mom[24]["view"] = 19143;
$trick_mom[25]["view"] = 11112;
$trick_mom[26]["view"] = 12226;
$trick_mom[27]["view"] = 10002;
$trick_mom[28]["view"] = 19991;
$trick_mom[29]["view"] = 19879;
$trick_mom[30]["view"] = 16662;
$trick_mom[31]["view"] = 11118;
$trick_mom[32]["view"] = 19991;
$trick_mom[33]["view"] = 19910;
$trick_mom[34]["view"] = 16667;
$trick_mom[35]["view"] = 11119;
$trick_mom[36]["view"] = 11198;
$trick_mom[37]["view"] = 20017;
$trick_mom[38]["view"] = 14398;
$trick_mom[39]["view"] = 16679;
$trick_mom[40]["view"] = 13312;
$trick_mom[41]["view"] = 15510;
$trick_mom[42]["view"] = 17710;
$trick_mom[43]["view"] = 12214;
$trick_mom[44]["view"] = 10005;
$trick_mom[45]["view"] = 17719;
$trick_mom[46]["view"] = 13651;
$trick_mom[47]["view"] = 11054;
$trick_mom[48]["view"] = 12290;
$trick_mom[49]["view"] = 15532;
$trick_mom[50]["view"] = 15517;
$trick_mom[51]["view"] = 18814;
$trick_mom[52]["view"] = 14492;
$trick_mom[53]["view"] = 21109;
$trick_mom[54]["view"] = 18832;
$trick_mom[55]["view"] = 17676;
$trick_mom[56]["view"] = 14456;
$trick_mom[57]["view"] = 19652;
$trick_mom[58]["view"] = 14681;

$sql_truncate = "TRUNCATE page_view";

echo "<div>".$sql_truncate.";</div>";

$sql = "SELECT * FROM knowledge_list";
$kwdata = mysqli_query($conn1,$sql);
while ($kwdata_fetch = mysqli_fetch_array($kwdata)) {
	echo "<div>INSERT INTO page_view SET module='knowledge' ,last_update=NOW(), obj_lang='THA',ref_id=".$kwdata_fetch["knowledge_list_id"].", total=1;</div>";
}

$sql = "SELECT * FROM trick_mom";
$tmdata = mysqli_query($conn1,$sql);
while ($tmdata_fetch = mysqli_fetch_array($tmdata)) {
	echo "<div>INSERT INTO page_view SET module='trick_mom' ,last_update=NOW(), obj_lang='THA',ref_id=".$tmdata_fetch["trick_mom_id"].", total=1;</div>";
}


foreach ($knowledge as $key => $value) {
    echo "<div>/*".$value["title"]."*/</div>";
    $sql = "SELECT * FROM knowledge_list WHERE title LIKE '%".$value["title"]."%'";
    // var_dump($sql);
    $data_res = mysqli_query($conn1,$sql);
    // var_dump($data_res);
    $data = mysqli_fetch_array($data_res);
    // var_dump($data);
    //echo "<div>DELETE FROM page_view WHERE module='knowledge' AND ref_id=".$data["knowledge_list_id"].";";
    //echo "<div>INSERT INTO page_view SET module='knowledge' ,last_update=NOW(), obj_lang='THA',ref_id=".$data["knowledge_list_id"].", total=".$value["view"].";</div>";
	echo "<div>UPDATE page_view SET total=".$value["view"]." WHERE ref_id=".$data["knowledge_list_id"]." AND module='knowledge';</div>";
}
foreach ($trick_mom as $key => $value_trick) {
	echo "<div>/*".$value_trick["title"]."*/</div>";
	$sql = "SELECT * FROM trick_mom WHERE title LIKE '%".$value_trick["title"]."%'";
	$data_res = mysqli_query($conn1,$sql);
	$data = mysqli_fetch_array($data_res);
	//echo "<div>DELETE FROM page_view WHERE module='trick_mom' AND ref_id=".$data["trick_mom_id"].";";
	//echo "<div>INSERT INTO page_view SET module='trick_mom' ,last_update=NOW(), obj_lang='THA',ref_id=".$data["trick_mom_id"].", total=".$value_trick["view"].";</div>";
	echo "<div>UPDATE page_view SET total=".$value_trick["view"]." WHERE ref_id=".$data["trick_mom_id"]." AND module='trick_mom';</div>";
}
foreach ($magazine as $key => $value_mag) {
 	echo "<div>/*".$value_mag["title"]."*/</div>";
 	$sql = "SELECT * FROM magazine WHERE title_th LIKE '%".$value_mag["title"]."%'";
 	$data_res = mysqli_query($conn1,$sql);
 	$data = mysqli_fetch_array($data_res);
 	//echo "<div>DELETE FROM page_view WHERE module='magazine' AND ref_id=".$data["magazine_id"].";";
 	echo "<div>INSERT INTO page_view SET module='magazine' ,last_update=NOW(), obj_lang='THA',ref_id=".$data["magazine_id"].", total=".$value_mag["view"].";</div>";
}
// $data_res = mysqli_query($conn1,"SELECT * FROM knowledge_list");
// while ($data = mysqli_fetch_array($data_res,MYSQLI_ASSOC)) {
//     echo $data["title"]."\n";
// }
/*echo "<div>DELETE FROM page_view WHERE module='first-270-days';</div>\n";
echo "<div>DELETE FROM page_view WHERE module='second-365-days';</div>\n";
echo "<div>DELETE FROM page_view WHERE module='final-365-days';</div>\n";*/
echo "<div>INSERT INTO page_view(module, ref_id, last_update, total, obj_lang)
VALUES
	('first-270-days', 1, '2015-11-19 14:33:57', 22679, 'THA'),
	('first-270-days', 2, '2015-11-19 14:34:00', 15267, 'THA'),
	('first-270-days', 3, '2015-11-19 14:34:03', 29435, 'THA'),
	('second-365-days', 4, '2015-11-19 14:39:39', 21235, 'THA'),
	('second-365-days', 5, '2015-11-19 14:54:24', 22896, 'THA'),
	('second-365-days', 6, '2015-11-19 14:39:44', 21893, 'THA'),
	('final-365-days', 7, '2015-11-19 14:14:04', 23645, 'THA'),
	('final-365-days', 8, '2015-11-19 14:13:06', 27123, 'THA'),
	('final-365-days', 9, '2015-11-19 14:54:38', 28412, 'THA');</div>";
/*มีอะไรซ่อนอยู่ในการนอนของเด็ก?*/
/*DELETE FROM page_view WHERE module='knowledge' AND ref_id=1432026198656;*/
//INSERT INTO page_view SET module='knowledge' ,last_update=NOW(), obj_lang='THA',ref_id=1432026198656, total=59843;


/*มีอะไรซ่อนอยู่ในการนอนของเด็ก*/
/*DELETE FROM page_view WHERE module='knowledge' AND ref_id=1443675265039;*/
//INSERT INTO page_view SET module='knowledge' ,last_update=NOW(), obj_lang='THA',ref_id=1443675265039, total=89899;
?>
<pre>

UPDATE page_view SET total=59843 WHERE module='knowledge' AND ref_id=1432026198656;

UPDATE page_view SET total=89899 WHERE module='knowledge' AND ref_id=1443675265039;

UPDATE page_view SET total=floor(((RAND() * 15000) + 5000)) WHERE total<5000;
</pre>