<?php
	if (!defined('DS')) {
		define('DS', DIRECTORY_SEPARATOR);
	}
	if (!defined('ROOT_DIR')) {
		$__file__ = explode( "/public".DS , __FILE__ );
	    define('ROOT_DIR', $__file__[0] );
	}
	if(isset($_POST["file"]) && $_POST["file"] != ""){
		$urlSplit = explode(PHP_EOL, $_POST["file"]);
		if(!empty($urlSplit) && count($urlSplit) > 0){
			foreach ($urlSplit as $url) {
				$urlList = explode("stocks/", $url);
				$path = urlencode($urlList["1"]);
				$filePath = ROOT_DIR . "/public/stocks/" . urldecode($path);
				if(is_file($filePath)){
					unlink($filePath);
					echo "<div style='color:green;'>" . $url . "</div>";
				}else{
					echo "<div style='color:red;'>" . $url . "</div>";
				}
			}
		}else{

		}
	}
?>

<form method="post" action="?" name=post>
	<div>Url</div>
	<textarea name="file" style="height:200px;width:400px;"></textarea>
	<div><input type="submit" name="submit"></div>
</form>
