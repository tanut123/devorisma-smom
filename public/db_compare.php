<?php
	error_reporting(E_ERROR | E_PARSE);
	session_start();
	if (!$_SESSION["sql_mode"]) {
		$_SESSION["sql_mode"] = "sql_only";
	}
?>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
</head>
<script>
	function confirmCreateTable(formEle,tb_name) {
		if (confirm("Confirm Create Table : "+tb_name) == true) {
			formEle.submit();
		}
	}
	function confirmDeleteTable(formEle,tb_name) {
		if (confirm("Confirm Delete Table : "+tb_name) == true) {
			formEle.submit();
		}
	}
	function confirmAddField(formEle,msg) {
		if (confirm("Confirm Add Field "+msg) == true) {
			formEle.submit();
		}
	}
</script>
<?php
	include_once "tool_function.php";
	set_time_limit(500);
	arrayList($_REQUEST);
	if ($_REQUEST['act'] == "Disconnect") {
		if ($_REQUEST['disconnect_db']) {
			$_SESSION[$_REQUEST['disconnect_db']] = "";
			$_SESSION['db_name_1'] = "";
			$_SESSION['db_name_2'] = "";
		}
	}
	if ($_REQUEST['act_1'] == "Connect") {
		$_SESSION['db_server_1'] =  $_REQUEST['db_server_1'];
		$_SESSION['db_username_1'] =  $_REQUEST['db_username_1'];
		$_SESSION['db_password_1'] =  $_REQUEST['db_password_1'];
	}
	if ($_REQUEST['act_2'] == "Connect") {
		$_SESSION['db_server_2'] =  $_REQUEST['db_server_2'];
		$_SESSION['db_username_2'] =  $_REQUEST['db_username_2'];
		$_SESSION['db_password_2'] =  $_REQUEST['db_password_2'];
	}
	if ($_REQUEST['act'] == "Select Database") {
		if ($_REQUEST['db_name_1']) {
			$_SESSION['db_name_1'] = $_REQUEST['db_name_1'];
		}
		if ($_REQUEST['db_name_2']) {
			$_SESSION['db_name_2'] = $_REQUEST['db_name_2'];
		}
	}
	print "
		<div>
			<div>Source Database Server : </div>
			<div style='padding-left:30px;width:300px;'>";
	if (!$_SESSION['db_server_1'] || !$_SESSION['db_username_1'] || !$_SESSION['db_password_1']) {
		gen_server_connect_form("_1");
	} else {
		if (@$conn1 = mysqli_connect($_SESSION['db_server_1'],$_SESSION['db_username_1'],$_SESSION['db_password_1'],$_SESSION['db_name_1'])) {
			$connect_1_pass = true; 	
			gen_server_disconnect_form("_1");	
		} else {
			print "<div style='color:red'>Connection Fail.</div>";
			gen_server_connect_form("_1");	
		}
		if ($connect_1_pass == true) {
			gen_select_db_form($conn1,$_SESSION['db_name_1'],"_1");
			if ($_SESSION['db_name_1']) {
				
			}
		}
	}	
	print "
			</div>
		</div>";
	print "
		<div>
			<div>Destination Database Server : </div>
			<div style='padding-left:30px;width:300px;'>";
	if (!$_SESSION['db_server_2'] || !$_SESSION['db_username_2'] || !$_SESSION['db_password_2']) {
		gen_server_connect_form("_2");
	} else {
		if (@$conn2 = mysqli_connect($_SESSION['db_server_2'],$_SESSION['db_username_2'],$_SESSION['db_password_2'],$_SESSION['db_name_2'])) {
			$connect_2_pass = true;	
			gen_server_disconnect_form("_2");
		} else {
			print "<div style='color:red'>Connection Fail.</div>";
			gen_server_connect_form("_2");	
		}
		if ($connect_2_pass) {
			gen_select_db_form($conn2,$_SESSION['db_name_2'],"_2");
			if ($_SESSION['db_name_2']) {
				
			}
		}
	}	
	print "
			</div>
		</div>
		<div style='margin-bottom:20px;'>
			<form action='' method='post' style='margin:0lpadding:0'>
				<input type='submit' value='SQL Only' name='sql_only'>
				<input type='submit' value='Create Table' name='create_table'>
			</form>
			Mode : ".$_SESSION['sql_mode']."
		</div>
		";
	if ($_REQUEST['sql_only'] == "SQL Only") {
		$_SESSION["sql_mode"] = "sql_only";
	}
	if ($_REQUEST['create_table'] == "Create Table") {
		$_SESSION["sql_mode"] = "create_table";
	}

	if ($connect_1_pass == true && $connect_2_pass == true && $_SESSION['db_name_1'] && $_SESSION['db_name_2']) {
		if ($_REQUEST['db_tb_tranfer']) {
			$read_source_structure_qstr = "SHOW CREATE TABLE ".$_REQUEST['db_tb_tranfer'];
			$read_source_structure_f = mysqli_fetch_array(mysqli_query($conn1,$read_source_structure_qstr));
			if ($_SESSION["sql_mode"] == "sql_only") {
				print $read_source_structure_f[1];
			}
			if ($_SESSION["sql_mode"] == "create_table") {
				mysqli_query($conn2,$read_source_structure_f[1]);
			}
		}
		if ($_REQUEST['db_tb_delete']) {
			$delete_not_exist_table_in_source_qstr = "DROP TABLE ".$_REQUEST['db_tb_delete'];
			mysqli_query($conn2,$delete_not_exist_table_in_source_qstr);
		}
		if ($_REQUEST['add_new_field']) {
			if ($_REQUEST['null_type'] == "YES") {
				$not_null_qstr = " NULL";
			} else {
				$not_null_qstr = " NOT NULL";
			}
			if ($_REQUEST['key_type'] == "PRI") {
				$key_type_qstr = " primary key";
			}
			if (strlen($_REQUEST['default']) > 0) {
				$default_qstr = " DEFAULT '".$_REQUEST['default']."'";
			}
			if ($_REQUEST['extra']) {
				$extra_qstr = " $_REQUEST[extra]";
			}
			$add_field_qstr = "ALTER TABLE $_REQUEST[table_name] ADD $_REQUEST[field_name] $_REQUEST[data_type] $not_null_qstr $key_type_qstr $default_qstr $extra_qstr";
			$add_field_qstr = str_replace('\\','',$add_field_qstr);
			print "$add_field_qstr";
			mysqli_query($conn2,$add_field_qstr);
		}
		$destination_table_data_qstr = "SHOW TABLES";
		$destination_table_data_res = mysqli_query($conn2,$destination_table_data_qstr);
		while ($destination_table_data_f = mysqli_fetch_array($destination_table_data_res)) {
			$destination_table_array[$destination_table_data_f[0]] = true;
		}
		$source_table_data_qstr = "SHOW TABLES";
		$source_table_data_res = mysqli_query($conn1,$source_table_data_qstr);

		while ($source_table_data_f = mysqli_fetch_array($source_table_data_res)) {
			if ($destination_table_array[$source_table_data_f[0]] == true) {
				$match_table_array[$source_table_data_f[0]] = true;
				$destination_table_array[$source_table_data_f[0]] = false;
				
			} elseif ($destination_table_array[$source_table_data_f[0]] == "") {
				$not_exists_table_in_des_array[$source_table_data_f[0]] = true; 
				$destination_table_array[$source_table_data_f[0]] = false;
			}
		}
		if (count($not_exists_table_in_des_array) > 0) {	
			print "
				<div>
					<div>Table Not Exists In Destination Database</div>
					<div style='padding-left:30px;position:relative'>";
			foreach ($not_exists_table_in_des_array as $key => $val) {
					print "
						<div style='float:left;width:200px;color:red'>$key</div>
						<div style='float:left;width:200px;'>
							<form action='' method='post' style='margin:0;padding:0'>
								<input type='hidden' name='db_tb_tranfer' value='$key'>
								<input type='button' name='c_tb' value='Tranfer Structure' onclick=\"confirmCreateTable(this.parentNode,'$key')\">
							</form>
						</div>
						<div style='clear:both'></div>";
	
			}
			print "
					</div>
				</div>";
		}	
		print "
			<div>
				<div>Table Not Exists In Source Database</div>
				<div style='padding-left:30px;position:relative'>";
		foreach ($destination_table_array as $key => $val) {
			if ($val == true) {
				print "
					<div style='float:left;width:200px;color:green'>$key</div>
					<div style='float:left;width:200px;'>
						<form action='' method='post' style='margin:0;padding:0'>
							<input type='hidden' name='db_tb_delete' value='$key'>
							<input type='button' name='c_tb' value='Delete Table' onclick=\"confirmDeleteTable(this.parentNode,'$key')\">
						</form>
					</div>
					<div style='clear:both'></div>";
			}

		}
		print "
				</div>
			</div>";
			
		print "
			<div>
				<div>Compare Field</div>
				<div style='padding-left:30px;position:relative'>";
		//print count($match_table_array);
		foreach ($match_table_array as $key => $val) {
			unset($source_field);
			unset($destination_field);
			print "
				<div style='color:blue'>$key</div>
				<div style='padding-left:30px;'>";
				$get_table_field_qstr = "DESCRIBE ".$key;
				$source_table_field_res=mysqli_query($conn1,$get_table_field_qstr);
				while ($source_table_field_f = mysqli_fetch_array($source_table_field_res)) {
					$source_field[$source_table_field_f[0]]['field_name'] =  $source_table_field_f[0];
					$source_field[$source_table_field_f[0]]['type'] =  $source_table_field_f[1];
					$source_field[$source_table_field_f[0]]['null'] =  $source_table_field_f[2];
					$source_field[$source_table_field_f[0]]['key'] =  $source_table_field_f[3];
					$source_field[$source_table_field_f[0]]['default'] =  $source_table_field_f[4];
					$source_field[$source_table_field_f[0]]['extra'] =  $source_table_field_f[5];
				}
				$destination_table_field_res=mysqli_query($conn2,$get_table_field_qstr);
				while ($destination_table_field_f = mysqli_fetch_array($destination_table_field_res)) {
					$destination_field[$destination_table_field_f[0]]['field_name'] =  $destination_table_field_f[0];
					$destination_field[$destination_table_field_f[0]]['type'] =  $destination_table_field_f[1];
					$destination_field[$destination_table_field_f[0]]['null'] =  $destination_table_field_f[2];
					$destination_field[$destination_table_field_f[0]]['key'] =  $destination_table_field_f[3];
					$destination_field[$destination_table_field_f[0]]['default'] =  $destination_table_field_f[4];
					$destination_field[$destination_table_field_f[0]]['extra'] =  $destination_table_field_f[5];
					$destination_field[$destination_table_field_f[0]]['check'] =  false;
				}
				$temp_before = "";
				foreach ($source_field as $field_data => $detail) {
					$match = true;
					if (!$destination_field[$field_data]) {
						print "
							<div style='color:red'>
								<form action='' method='post' style='margin:0;padding:0'>
									<input type='hidden' name='add_new_field' value='1'>
									<input type='hidden' name='table_name' value='$key'>
									<input type='hidden' name='field_name' value='".$source_field[$field_data]['field_name']."'>
									<input type='hidden' name='data_type' value=\"".$source_field[$field_data]['type']."\">
									<input type='hidden' name='key_type' value='".$source_field[$field_data]['key']."'>
									<input type='hidden' name='null_type' value='".$source_field[$field_data]['null']."'>
									<input type='hidden' name='default' value=\"".$source_field[$field_data]['default']."\">
									<input type='hidden' name='extra' value='".$source_field[$field_data]['extra']."'>
									<input type='hidden' name='after_field' value='$temp_before'>
									$field_data :: Not Exists <input type='button' name='add_field' value='Add Field' onclick=\"confirmAddField(this.parentNode,'".$source_field[$field_data]['field_name']." Into Table $key')\">
								</form>
							</div>";
							$match = false;
					} else {
						foreach ($detail as $v_type => $value) {
							if ($destination_field[$field_data][$v_type] != $source_field[$field_data][$v_type]) {
								print "
									<div style='color:red'>$field_data :: $v_type Not Match  [". $source_field[$field_data][$v_type]."::".$destination_field[$field_data][$v_type]."]</div>";
								$match=false;
							}
							$destination_field[$field_data]['check'] =  true;
						}
						$temp_before = $field_data;
					}
					if ($match == true) {
						print "
							<div style='color:darkgreen'>$field_data :: Match</div>";
					}
				}
				foreach ($destination_field as $field_data => $detail) {
					if ($destination_field[$field_data]['check'] == false) {
						print "
							<div style='color:#008'>$field_data :: Not Found in Source Table</div>";
						
					}
				}
			print "
				</div>";
		}
		print "
				</div>
			</div>";
	}
?>