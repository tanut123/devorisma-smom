<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;
use DateTime;

class Activity extends Model {

	public function load_detail($slug){
		$dt = DB::table('activity')->where('slug',$slug)->get();
		return $dt;
	}

}