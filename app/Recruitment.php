<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use DateTime;
use Crypto;
use App;

class Recruitment extends Model {

	public $error_message = "";

	function getProvince(){
		$dt = DB::select("SELECT province,smom_address_id  FROM smom_address GROUP BY province ORDER BY CONVERT( province USING tis620 ) ASC ");
		return $dt;
	}
	function getDistrict($province){
		$dt = DB::select("SELECT district,smom_address_id  FROM smom_address WHERE province = ? GROUP BY district ORDER BY CONVERT( district USING tis620 ) ASC ",[$province]);
		return $dt;
	}
	function getSubDistrict($district){
		$dt = DB::select("SELECT smom_address_id,subdistrict,zipcode  FROM smom_address WHERE district = ? ORDER BY CONVERT( subdistrict USING tis620 ) ASC ",[$district]);
		return $dt;
	}
	function getMomProduct(){
		$dt = DB::select("SELECT smom_mom_product_id,title  FROM smom_mom_product ORDER BY CONVERT( title USING tis620 ) ASC ");
		return $dt;
	}
	function getChildProduct(){
		$dt = DB::select("SELECT smom_child_product_id,product_name,formula_name  FROM smom_child_product ORDER BY CONVERT( product_name USING tis620 ),CONVERT( formula_name USING tis620 ) ASC ");
		return $dt;
	}
	function getPlace($type = ""){
		$sql = "SELECT smom_place_id,name,type  FROM smom_place ORDER BY CONVERT( name USING tis620 ) ASC ";
		if($type == ""){
			$dt = DB::select($sql);
		}else{
			$dt = DB::select("SELECT smom_place_id,name,type  FROM smom_place  WHERE type like ? ORDER BY CONVERT( name USING tis620 ) ASC ",[$type."%"]);
		}
		return $dt;
	}
	function getOccupation(){
		$dt = DB::select("SELECT smom_occupation_id,title  FROM smom_occupation ORDER BY smom_occupation_id ASC ");
		return $dt;
	}
	function getReason(){
		$dt = DB::select("SELECT smom_reson_change_id,remark  FROM smom_reson_change ORDER BY CONVERT( remark USING tis620 ) ASC ");
		return $dt;
	}

    function insertData($dataRecruitment) {
	    $dtMemberDraft = DB::table("member_recruitment_draft")->insert($dataRecruitment);
        if($dtMemberDraft){
        	return true;
        }
        return false;
    }

    function insertChildData($dataRecruitment){
		$dtMemberChildDraft = DB::table("member_recruitment_child_draft")->insert($dataRecruitment);
		if($dtMemberChildDraft){
			return true;
		}
        return false;
    }

    function changeDateFormat($date){
    	return implode("-", array_reverse(explode("/",$date)));
    }

    function generateUUID() {
        $date = new DateTime();
        return (($date->format('U') * 1000) + mt_rand(0,999));
    }

}
