<?php namespace App\Http\Controllers;

use App;
use Illuminate\Routing\Route;
use Meta;
use Crypto;
use App\Momtip;
use App\MemberWeb;
use App\Core;
use Input;
use Response;
use View;
use App\Core\Lng;
use Cache;
use App\Core\ViewSession;
use App\Pageviews;
use Session;

class MomtipController extends App\Core\Controller {
	protected $dataTable;
	public function __construct()
    {
    	parent::__construct();
    	// Meta::meta('title', trans('title.TITLE_MOMTIP') );
    	$this->dataTable = new Momtip();
    	$this->dataMemberWeb = new MemberWeb();
	}

	public function rating(){
		$data = Input::all();
		$mode = $data['mode'];
		$page_uid = $data['page_uid'];
		$rate = $data['rate'];
		if($mode == "add"){
			$dsMemberInsert = $this->dataTable->insertRating($data);
		}
	}

	public function listData($type){
		$cache_name = 'MomtipListCache_' . $type . "_". \Illuminate\Support\Facades\Session::get("webMemberId"). "_" . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
	        $dt['type'] = $this->dataTable->momtipPage($type);
	        $type_mom_title = array(1=>trans('title.TITLE_MOMTIP_PREGNANCY'), 2=>trans('title.TITLE_MOMTIP_LACTATING'), 3=>trans('title.TITLE_MOMTIP_TODDLER'));
	        Meta::meta('title',  $type_mom_title[$dt['type']] );
			$view_cache = (string)view('momtip.index', $dt);
	        Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

 		return $view_cache;
	}

	public function detail($type, $slug = null){
		$arr_redi = array();

		$arr_redi['lactating-mom'] = array('30','31','ทำไงดีนํ้านมไม่พอเลี้ยงเจ้าตัวเล็ก','ทำไมต้องเลี้ยงลูกด้วยนมแม่','พุงฟีบเมื่อเลี้ยงลูกด้วยนมแม่');
		$arr_redi['toddler-mom'] = array('พัฒนาการลูกน้อยเตาะแตะ-1-2ขวบปี','ลูกน้อยวัย2ปีกับพัฒนาการด้านอารมณ์');

		$arr_url = array();
		$arr_url['30'] = '/knowledge/พัฒนาการเด็ก1เดือน';
		$arr_url['31'] = '/knowledge/พัฒนาการเด็ก2เดือน';
		$arr_url['ทำไงดีนํ้านมไม่พอเลี้ยงเจ้าตัวเล็ก'] = '/knowledge/น้ำนมแม่ไม่พอไม่มี';
		$arr_url['พัฒนาการลูกน้อยเตาะแตะ-1-2ขวบปี'] = '/knowledge/พัฒนาการเด็ก1ขวบ';
		$arr_url['ลูกน้อยวัย2ปีกับพัฒนาการด้านอารมณ์'] = '/knowledge/พัฒนาการเด็ก2ขวบดื้อ';
		$arr_url['ทำไมต้องเลี้ยงลูกด้วยนมแม่'] = '/knowledge/ปัญหาการเลี้ยงลูกด้วยนมแม่';

		$arr_url['พุงฟีบเมื่อเลี้ยงลูกด้วยนมแม่'] = '/knowledge/ลูกไม่กินนมแม่';


		if(!empty($arr_redi[$type]) && in_array($slug, $arr_redi[$type])){
			$newlink = url()."/".\App\Core\Lng::url().$arr_url[$slug];
			header('Location: '.$newlink);
			exit();
		}

		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		$cache_name = 'MomtipDetailCache_' . $type . "_" . $slug . "_" . $member_web_id . "_" . App::getLocale();
		$view_cache = Cache::get($cache_name);
		$cache_id = 'MomtipIDCache_' . $type . "_" . $slug . "_" . $member_web_id . "_" . App::getLocale();
		$cache_id_data = Cache::get($cache_id);
		if($view_cache == "" || $view_cache == null){

			$type = $this->dataTable->momtipPage($type);
			$dt['content'] = $this->dataTable->getTrickMomDetail($type, $slug);
			if(count($dt['content']) <= 0){
				abort("404");
				exit();
			}

			$sub_cat = $dt['content'][0]->sub_category;
			$dt['relate'] = $this->dataTable->getTrickMomListRelate($type, $slug, $sub_cat);
			$bookmarkDataArr = $this->dataMemberWeb->getBookmarkByMember($member_web_id);
			$dataBookmark = [];
	        foreach ($bookmarkDataArr as $key => $dataArr) {
	        	$dataBookmark[] = $dataArr->trick_mom_id;
	        }
	        $dt['bookmarkList']	= $dataBookmark;
			$dt['member_web_id'] = $member_web_id;
	        Meta::meta('title', $dt['content'][0]->title);
	        Meta::meta('description', $dt['content'][0]->detail);
	        Meta::meta('image', Core\OMImage::readFileName($dt['content'][0]->image_gen,$dt['content'][0]->image, "c230x205", "trick_mom"));
	        $dt['type'] = $type;
	        $dt['member_web_id'] = $member_web_id;
	        $bookmark = $this->dataMemberWeb->getBookmarkBySlug($slug,$member_web_id);
	        $dt["bookmark"] = "F";
	        if($bookmark){
	        	$dt["bookmark"] = "T";
	        }
			$view_cache = (string)view('momtip.detail', $dt);
	        Cache::put($cache_name, $view_cache, 10);
	        $cache_id_data = $dt['content'][0]->trick_mom_id;
	        Cache::put($cache_id, $cache_id_data, 10);
	    }
        if($member_web_id != ""){
			$this->dataMemberWeb->addLastArticleByUser($member_web_id ,$cache_id_data);
			Cache::forget('MomtipBookmarkCache_'. $member_web_id .'_' . App::getLocale());
        }
        $type = $this->dataTable->updateViews($cache_id_data,  Lng::db());
	    ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

	   	return $view_cache;
	}
	public function pageview($type, $slug = null){

	}

	public function listAjax(){
		$data = Input::all();
		$dt = array();
        $member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
        $cache_name = 'MomtipListDataCache_' . $data["type"]. "_" .$data["page"]. "_" .$data["limit"] .  "_" . $member_web_id . "_" . App::getLocale();
        $view_cache_name = 'MomtipViewCache_' . $data["type"]. "_" .$data["page"]. "_" .$data["limit"] .  "_" . $member_web_id . "_" . App::getLocale();
        $view_data = Cache::get($view_cache_name);
		$return_dt = Cache::get($cache_name);

		if($return_dt == "" || $return_dt == null || $view_data == null || $view_data == ""){
	        $dt['type'] = $data["type"];
	        $subcat = $this->dataTable->getSubcat($dt['type']);
	        $type_mom_array = array(1=>"pregnancy-mom", 2=>"lactating-mom", 3=>"toddler-mom");
	        $count = 1;

	    	$return_dt["list"] = "";
	    	$return_dt["sub_panel"] = "";

	    	$bookmarkDataArr = $this->dataMemberWeb->getBookmarkByMember($member_web_id);
	        $dataBookmark = [];
	        foreach ($bookmarkDataArr as $key => $dataArr) {
	        	$dataBookmark[] = $dataArr->trick_mom_id;
	        }
			$icon_view = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAASCAYAAAA6yNxSAAABTElEQVR4Ac2WJVBEURSG1y3iDhnvM9szBW1IpOP0gLtDpG9eKi4Nh4q768/MCWfuPDkX35mv3PPL0/vWofNrr03PA60gCg7BG3EIojTLk6XJS12gCMyBNyFz5HF9tTwMlk1KFsAEsWCkIW/4M8Uh0GUSOgNyDDw5NDPydIGQtLwQbJgETQOPhddDGiPvBii0Ky8BNyYB2yCoPBuFhIutB0lrlHEDSszKW0hkRhXTpoI1NlsDqWxeY5PVwoudoEfwZMcyT8RgHmHzOEFeD3B+iAcE4nMW7gYvqobW3Ex3IcgdkB7ArXK77gw0d4rmVnYA8luQxcJHDOYjbJ4lvQU6D2G9sk9MgQdiCoTYvEHwEGq/hicg2eC99yhryaTVew2FG9E8iLXwxoIF/Y1IbyveBxUgwPQBWtuXb8Vf/xjdgFXiRvAx+p+f4z//Q/IOPYCpMz+bvCEAAAAASUVORK5CYII=";
			$v_id = array();

	        foreach ($subcat as $value) {
	        	$type_mom_data =  $type_mom_array[$dt["type"]];
	        	$data_list = $this->dataTable->getListBySubCat($value->sub_category_child_id);
	        	$dt[$value->sub_category_child_id."_html"] ="";
	        	$subcat = ((App::getLocale()=="en")? $value->sub_category_text_en : $value->sub_category_text_th);
	        	$html_subpanel = "";
	        	if(count($data_list) > 0){
		        	$html_subpanel .="<div class='lazyload ".(($value->sort_order% 2 == 0) ? "panel_subcat_odd":"panel_subcat_even")."' data-bgset='".(($value->sort_order% 2 == 0) ? env('BASE_CDN', '')."/images/momtip/bg-odd_new1.jpg": env('BASE_CDN', '')."/images/momtip/bg-even_new1.jpg")."' data-expand='+10' >
							<div class='list-content-panel'>
								<div class='title-subcat FXregular'>".$subcat."</div>
								<div class='trick-mom-list'>
									<div id='list-panel_".$value->sub_category_child_id."' class='list-panel order-panel_".$value->sort_order."'>
									</div>
								</div>
								<div class='clearfix'></div>
							</div>
						</div>";


		        	$return_dt["sub_panel_list"][] = $value->sub_category_child_id;
		        	$return_dt["sub_panel"] .= $html_subpanel;
	        	}

	        	foreach ($data_list as $key => $data_child) {
	        		$v_id[] = $data_child->trick_mom_id;
	        	}



	        	$return_dt[$value->sub_category_child_id."_html"] = "";
	        	foreach ($data_list as $key => $data_child) {
			        $bookmark = "F";
			        if(in_array($data_child->trick_mom_id, $dataBookmark)){
			        	$bookmark = "T";
			        }

				$d_day = date('d', strtotime($data_child->article_date));
				$d_month = $this->convetMonth(date('m', strtotime($data_child->article_date)));
	        		$html_data = "";
	        		$img_thumb = \App\Core\OMImage::readFileName($data_child->image_gen,$data_child->image,'c280x207','trick_mom');
	        		$html_data .=
	        		"<div data-id='".$data_child->trick_mom_id."' class='lazyload ".(($count% 2 == 0) ? "list-item list-item-center":"list-item")." list-item-momtip' data-bgset='".env('BASE_CDN', '')."/images/knowledge/bg_list.png'  data-expand='+10'>

							<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-img-panel'>
								<img data-src='".$img_thumb."' alt='' class='lazyload' data-expand='+10'>".
								($data_child->youtube_source != "" || $data_child->file_mp4 != "" ? "
									<span class='cover-video'>
										<img data-src='".env('BASE_CDN', '')."/images/home/trick-mom/icon-video.png' class='lazyload' data-expand='+10'>
									</span>"
								: "")."
							</a>
							<div class='badge-list lazyload' data-bgset='".env('BASE_CDN', '')."/images/knowledge/badge.png' data-expand='+10'>
								<span class='txt-day'>".$d_day."</span>
								<span class='txt-month'>".$d_month."</span>
							</div>
							<div class='list-body-content-panel'>
								<div class='view-panel FX'>
                                	<img data-src='".$icon_view."' class='view_icon lazyload' data-expand='+10'><span class='view_number' >".number_format(intval($data_child->pageview))." Views</span>
                            	</div>
								<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-title'>
									<strong class='FXregular'>".\App\Core\OM::trimWithDot($data_child->title, 56)."</strong>
								</a>
								<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-description FMed'>".\App\Core\OM::trimWithDot($data_child->description, 70)."

								</a>
							</div>";
							if($member_web_id != "" && $member_web_id != null && $bookmark == "F"){
								$html_data .= "<a href='javascript:void(0);' onClick='javascript:addBookmark(\"". $data_child->trick_mom_id ."\", this)' data-url='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-link-bookmark'>
									<img class='list-body-logo lazyload' data-src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAABAklEQVQ4T92TsUoDQRRFz0sTQf/BsRQbK79DJJ0guwELBTGlFjuuY2EnCZZKJk0+IHY2foCVneWO+AUWakTwSSQLy5ospjRT3nfPhXd5I0x5NjYXoAdKbe/MZ9eTbFIBZ4ABBs6HzVnhACz/AzjdX1vSt9dLFUZdNIBF4Bm4E3g59eGwuPuvwpKmORLlvFTQZ02kkXazm0p4NCwFfAFbzodBuXFJInMrQh2VvutlV7khiY0VOEF0x3Wf+rluo5VdRLdV+RAbGx0POs6HVjE9jcx62gsPRc3Gpg387F4JTzqMOYMfgftpH6WgbwCreWHvwMIfoLJlKEnTtMYXNUvAUIXjb9UseSHfehsXAAAAAElFTkSuQmCC' data-expand='+10'>
									<span class='FXMed text-bookmark'>จัดเก็บ</span>
								</a>";
							}else{
								$html_data .= "<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-link'>
									<img class='list-body-logo lazyload' data-src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABsklEQVRIib2WvW4CMQzHLXgIRsTWxO5asXRiY6/UnYUFnuEeo7qJgZytLrwGEr3DfgIWtk4sSEgn0QGq0g84rq3y3yLZ+SXxRwxQoXnWaWnmRyo0U8aVCpUmtFeh8rieaeZH86zTqtrrrIrg2sY4UaFShdbGmBrToGDfy6euW7DvGdPAGFMVWqtQaYyTIrh2LZCyH6rQ1pgWy+D6SQKNS/ZJAo1lcH1jWqjQVtkPKyHPD9A0xtQEd8Z+XAX5CWrsxwd/TJ8foHnW2BhTZdzk7O/rQL4qZ3+vjBtjTH80UPZDE9z9FXQKNMHdtyctgmsfYuTH55w14J0KPV18mi8y9mMV2n5KGmOcGNPiUoxepje3yvhqjOFa4CGGtDDGCQAc60ioXAbXr3L+DXAZXF+FynnWacGxYNfXZl5dYJJAQ4XWmvkRqNDsbNb8E/BY+DNQxpUxDerA6gKNaaCMK1ChsmDfqwurAyzY91SoBBPa51PX/Q0MAMACPprQ/lKLyqeua0L7uDeLHbOI2RizzqJ2EICIvREgctcHiPiffZwk0k8NEHkGeVeU6epU0ebGU/3nRPwGRm3+AjGDpggAAAAASUVORK5CYII=' data-expand='+10'>
								</a>";
							}
						$html_data .= "</div>";
					$return_dt[$value->sub_category_child_id."_html"] .= $html_data;
					$count++;
	        	}
	        }
	        $view_data = implode(",",$v_id);
	        Cache::put($view_cache_name , $view_data, 10);
	        Cache::put($cache_name, $return_dt, 10);
	    }

	    $table = new Pageviews();
		if( !empty($view_data)){
			$dt = $table->getMomTipsView($view_data);
			foreach ($dt as $key => $value_view) {
				ViewSession::set("view_".$value_view->ref_id,number_format($value_view->total));
			}
		}
		$json = json_encode($return_dt);
		return ViewSession::make($json);
        //return Response::json($return_dt);
	}
	public function convetMonth($input){
		$d_th_month = array(
		        	"0"=>"",
				    "1"=>"ม.ค.",
				    "2"=>"ก.พ.",
				    "3"=>"มี.ค.",
				    "4"=>"เม.ย.",
				    "5"=>"พ.ค.",
				    "6"=>"มิ.ย.",
				    "7"=>"ก.ค.",
				    "8"=>"ส.ค.",
				    "9"=>"ก.ย.",
				    "10"=>"ต.ค.",
				    "11"=>"พ.ย.",
				    "12"=>"ธ.ค."
		           );
		$d_en_month = array(
		        	"0"=>"",
				    "1"=>"JAN",
				    "2"=>"FEB",
				    "3"=>"MAR",
				    "4"=>"APR",
				    "5"=>"MAY",
				    "6"=>"JUN",
				    "7"=>"JUL",
				    "8"=>"AUG",
				    "9"=>"SEP",
				    "10"=>"OCT",
				    "11"=>"NOV",
				    "12"=>"DEC"
		            );
		if(App::getLocale() == "en"){
			return $d_en_month[(int)$input];
		}else{
			return $d_th_month[(int)$input];
		}
	}
}
