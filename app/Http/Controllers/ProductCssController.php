<?php namespace App\Http\Controllers;

use App;
use Meta;
use Crypto;
use App\Core;
use Input;
use Response;
use View;

class CustomCssController extends App\Core\Controller {
    public function getCssForPage($filename_str) {
        list($gen,$filename) = explode("&",$filename_str);
        $dt_table = new App\ProductPage();
        $data = (array)$dt_table->loadCustomCss($gen,$filename);
        if (count($data) > 0) {
            $data = (array)$data[0];
            $response = Response::make($data["data"]);
            $response->header('Content-Type', 'text/css');
            return $response;
        } else {
            return "";
        }
    }
    public function getCssForWCM($gen) {
        $dt_table = new App\ProductPage();
        $data = (array)$dt_table->loadCustomCss($gen);
        if (count($data) > 0) {
            $data = (array)$data[0];
            $font_data = readfile(public_path()."/css/font.css");
            $response = Response::make($font_data.$data["data"]);
            $response->header('Content-Type', 'text/css');
            return $response;
        } else {
            return "";
        }
    }
}
