<?php namespace App\Http\Controllers;

use App;
use App\Core\ClearCache;

class ClearCacheController extends App\Core\Controller {
	public function clear() {
       $removeCache = ClearCache::clear();
       if($removeCache){
       		echo "Clear All Cache Success.";
       }else{
       		echo "Cannot remove Cache";
       }
	}
}