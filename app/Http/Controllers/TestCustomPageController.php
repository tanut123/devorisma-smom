<?php namespace App\Http\Controllers;

use App;
use Meta;
use App\Core;

class TestCustomPageController extends App\Core\Controller {

	public function index($pagename){
        return view('test-custompage.'.$pagename);
	}

}
