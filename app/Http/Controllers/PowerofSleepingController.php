<?php namespace App\Http\Controllers;

use App;
use Illuminate\Routing\Route;
use Meta;
use Crypto;
use App\PowerofSleeping;
use App\Core;
use Input;
use Response;
use View;
use Cache;
use App\Core\ViewSession;
use Session;

class PowerofSleepingController extends App\Core\Controller {

	// private $dt_product;

	// public function __construct()
 //    {
 //    	parent::__construct();
 //    	$this->dt_product = new ProductPage();
	// }

	public function index(){
		// $cache_name = 'PowerofSleepingCache_'. App::getLocale();
		// $view_cache = Cache::get($cache_name);
		// if($view_cache == "" || $view_cache == null){
		// 	$page = "";
		// 	if($type == "mom_gold"){
	 //    		$page = $this->mom_gold();
	 //    	}else if($type == "progress_gold"){
	 //    		$page = $this->progress_gold();
	 //    	}else if($type == "pe_gold"){
	 //    		$page = $this->pe_gold();
	 //    	}else{
	 //    		$page = $this->progress();
	 //    	}

		// 	$view_cache = (string)$page;
	 //        Cache::put($cache_name, $view_cache, 10);
	 //    }
		$cache_name = 'PowerofSleepingCache_'. App::getLocale();
		$view_data = Cache::get($cache_name);
		if($view_data == "" || $view_data == null){
			$power_of_sleep_model = new App\PowerOfSleep();
			$banner_top_data = $power_of_sleep_model->getBannerTop();
			$banner_bottom_data = $power_of_sleep_model->getBannerBottom();
			$video_data = $power_of_sleep_model->getVideo();
			//loop banner
			$str_banner_top_html = "";
			if($banner_top_data > 0){
				$str_banner_top_html ='<div class="hl-banner-secfirst">';
					foreach ($banner_top_data as $key => $data_banner_top) {
						$url_image = \App\Core\OMImage::readFileName($data_banner_top->image_data_gen,$data_banner_top->image_data,'w899x274','power_of_sleep_banner');
						$image_name = $data_banner_top->banner_name;
						$image_link = $data_banner_top->link_url;

						$str_banner_top_html .="<a class='border-img' href='".$image_link."' target='_blank'>";
	                		$str_banner_top_html .="<div class='snap-list-banner' style=\"background-image:url('".$url_image."');\" alt='".$image_name."'></div>";
        				$str_banner_top_html .='</a>';
					}
        		$str_banner_top_html .='</div>';
			}else{
				$str_banner_top_html .='<div class="hl-banner-secfirst">';
                	$str_banner_top_html .='<div class="snap-list-banner">-- No banner Data --</div>';
            	$str_banner_top_html .='</div>';
			}
			$str_banner_bottom_html = "";

			if($banner_bottom_data > 0){
				$str_banner_bottom_html ='<div class="hl-banner-sectwo">';
					foreach ($banner_bottom_data as $key => $data_banner_bottom) {
						$url_image = \App\Core\OMImage::readFileName($data_banner_bottom->image_data_gen,$data_banner_bottom->image_data,'w899x274','power_of_sleep_banner');
						$image_name = $data_banner_bottom->banner_name;
						$image_link = $data_banner_bottom->link_url;
						$str_banner_bottom_html .="<a class='border-img' href='".$image_link."' target='_blank'>";
	                		$str_banner_bottom_html .="<div class='snap-list-banner' style=\"background-image:url('".$url_image."');\" alt='".$image_name."'></div>";
	                	$str_banner_bottom_html .='</a>';
					}
        		$str_banner_bottom_html .='</div>';
			}else{
				$str_banner_bottom_html .='<div class="hl-banner-sectwo">';
                	$str_banner_bottom_html .='<div class="snap-list-banner">-- No banner Data --</div>';
            	$str_banner_bottom_html .='</div>';
			}
			$data["banner_top"] = $str_banner_top_html;
			$data["banner_bottom"] = $str_banner_bottom_html;

			//loop video
			$video_str = "";
			foreach ($video_data as $value) {
				$video_url_image = \App\Core\OMImage::readFileName($value->image_gen,$value->image,'w384x288','power_of_sleep_video');
				if (strlen($value->file_mp4) > 0 && strlen($value->file_mp4_gen) > 0) {
					$video_type = "mp4";
					$video_file = \App\Core\OMImage::readFileName($value->file_mp4_gen,$value->file_mp4,'o0x0','power_of_sleep_video');
				} else {
					$video_type = "youtube";
					$video_id = $value->youtube;
				}
				$video_str .= "
				<div class='video_object'>
					<div class='video' style=\"background-image:url('".$video_url_image."')\">
						<div class='play_icon_align'>
							<div class='play_video_icon' video_data='".($video_type == "mp4" ? $video_file : $video_id)."' type='".$video_type."'>

							</div>
						</div>
					</div>
					".(strlen($value->title) > 0 ? "
					<div class='text FX anti_alias'>Credit: ".
						$value->title
					."</div>" : "")."
				</div>";
			}
			$data["video"] = $video_str;
			$view_data = (string)view('thepowerofsleeping.index',$data);
			Cache::put($cache_name, $view_data, 10);
		}
		ViewSession::viewSessionGlobal();
		$return_data = ViewSession::make($view_data);
		return $return_data;

		// return $view_cache;
	}
}