<?php namespace App\Http\Controllers;

use App;
use Meta;
use Cache;
use App\Core\ViewSession;

class ImportantnoticeController extends App\Core\Controller {

	public function index()
	{
		$cache_name = 'ImportantNoticeCache_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
			Meta::meta('title', 'สิ่งสำคัญที่คุณแม่ควรทราบ');
			$view_cache = (string)view('important-notice.index');
	        Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

}
