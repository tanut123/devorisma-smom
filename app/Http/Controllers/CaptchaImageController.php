<?php namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\Session;
use Meta;
use App\Core;
use Cache;
use PHPMailer;
use Intervention\Image\ImageManagerStatic as Image;

class CaptchaImageController extends App\Core\Controller {

	public function __construct(){
    	parent::__construct();
    	Meta::meta('title', 'Captcha');
	}
	public function genChaptcha($date) {
		$captcha = new App\Core\OMCaptcha(100,25,public_path()."/fonts/db_helvethaica_bold.ttf",15,6);
		$captcha->setBackgroundColor("#FFFFFF");
		//$captcha->setBackground(public_path()."/images/live-chat/bg-main.png");
		Session::set("captcha_code",$captcha->text);
		return Image::make($captcha->makeImage())->response('png');

	}
}
