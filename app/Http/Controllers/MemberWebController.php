<?php namespace App\Http\Controllers;

use App;
use Illuminate\Routing\Route;
use Meta;
use Crypto;
use App\MemberWeb;
use App\Home;
use App\Core;
use Input;
use Response;
use View;
use App\Core\Lng;
use Cache;
use App\Core\ViewSession;
use Redirect;
use Log;
use PHPMailer;
use CacheController;
use App\Core\ClearCache;
use App\Core\OMMail;


class MemberWebController extends App\Core\Controller {
	protected $dataTable;
	protected $dt_home;
	public function __construct()
    {
    	parent::__construct();
    	$this->dataTable = new MemberWeb();
    	$this->dt_home = new Home();
	}
	public function favorite(){
		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		$cache_name = 'MomtipFavoriteCache_'. $member_web_id .' _ ' . App::getLocale();
		$return_data = Cache::get($cache_name);
		if($return_data == ""){
			$dataFavorite = $this->dataTable->getFavoriteByUser($member_web_id);
			if($dataFavorite){
				foreach ($dataFavorite as $valFavorite) {
					$return_data["favorite"][] = $valFavorite->trick_mom_id;
				}
			}else{
				$return_data = [];
			}
		}
		return json_encode($return_data);
	}

	public function removeFavorite(){
		$data = Input::all();
		$dt = array();
        $dt['member_web_id'] = \Illuminate\Support\Facades\Session::get("webMemberId");
        $dt['momtip_id'] = $data["momtip_id"];
        $dataFavorite = $this->dataTable->removeFavoriteByUser($dt['momtip_id'],$dt['member_web_id']);
       	if($dataFavorite > 0 && $this->dataTable->error_message == ""){
       		$return_dt["cmd"] = "y";
       		$return_dt["message"] = "ok";
       		ClearCache::clear();
       	}else{
       		$return_dt["cmd"] = "n";
       		$return_dt["message"] = $dataFavorite;
       		$return_dt["err"] = $this->dataTable->error_message;
       	}
        return Response::json($return_dt);
	}

	function addFavorite(){
		$data = Input::all();
		$dt = array();
        $dt['member_web_id'] = \Illuminate\Support\Facades\Session::get("webMemberId");
        $dt['momtip_id'] = $data["momtip_id"];
        $dataFavorite = $this->dataTable->addFavoriteByUser($dt['momtip_id'],$dt['member_web_id']);
       	$return_dt["cmd"] = "n";
       	if($dataFavorite > 0){
	       	$return_dt["cmd"] = "y";
       		$return_dt["message"] = "ok";
       		ClearCache::clear();
       	}else if($dataFavorite == 0){
       		$return_dt["message"] = "Cannot save Bookmark.";
       	}else{
       		$return_dt["message"] = "Duplicate Or Sql Error.";
       	}

        return Response::json($return_dt);
	}

	public function bookmark(){
		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		if($member_web_id == ""){
			return redirect(App::getLocale() . '/home');
		}
		$cache_name = 'MomtipBookmarkCache_'. $member_web_id .'_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
			Meta::meta('title', 'บทความของคุณ');
			$type_mom_array = array(1=>"pregnancy-mom", 2=>"lactating-mom", 3=>"toddler-mom");
			$count = 0;
			$databookmark = $this->dataTable->getLastArticleByUser($member_web_id);
			$dataArticle["html"] = "";
	    	$itemCount = (count($databookmark)-1);
	    	$icon_view = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAASCAYAAAA6yNxSAAABTElEQVR4Ac2WJVBEURSG1y3iDhnvM9szBW1IpOP0gLtDpG9eKi4Nh4q768/MCWfuPDkX35mv3PPL0/vWofNrr03PA60gCg7BG3EIojTLk6XJS12gCMyBNyFz5HF9tTwMlk1KFsAEsWCkIW/4M8Uh0GUSOgNyDDw5NDPydIGQtLwQbJgETQOPhddDGiPvBii0Ky8BNyYB2yCoPBuFhIutB0lrlHEDSszKW0hkRhXTpoI1NlsDqWxeY5PVwoudoEfwZMcyT8RgHmHzOEFeD3B+iAcE4nMW7gYvqobW3Ex3IcgdkB7ArXK77gw0d4rmVnYA8luQxcJHDOYjbJ4lvQU6D2G9sk9MgQdiCoTYvEHwEGq/hicg2eC99yhryaTVew2FG9E8iLXwxoIF/Y1IbyveBxUgwPQBWtuXb8Vf/xjdgFXiRvAx+p+f4z//Q/IOPYCpMz+bvCEAAAAASUVORK5CYII=";
	    	if($databookmark){
	    		$BASE_LANG = url()."/".App\Core\Lng::url()."/";
	    		$dt_sub_category = $this->dt_home->getChildSubCat();
	    		foreach ($dt_sub_category as $key => $dt_sub_category) {
		        	$subcat[$dt_sub_category->sub_category_child_id] = $dt_sub_category->sub_category_text_th;
		        }
		        foreach ($databookmark as $key => $data_child) {
		        	$count++;
		        	$type_mom_data =  $type_mom_array[$data_child->type_mom];
		        	$html_subpanel = "";
		    		$html_data = "";
		    		$img_thumb = \App\Core\OMImage::readFileName($data_child->image_gen,$data_child->image,'c280x207','trick_mom');
		    		$html_data .=
		    		"<li data-id='".$data_child->trick_mom_id."' class='".(($count% 2 == 0) ? "list-item list-item-center":"list-item")." list-item-momtip'>
		    				<div class='cat-title'>
                        		<a href='".\App\Core\Lng::url()."/".$type_mom_data ."'>
	                            	<div class='txt txt-blue FXMed'>".$data_child->title_cat."</div>
	                            </a>
                            </div>
							<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-img-panel'>
								<img src='".$img_thumb."' alt=''>".
								($data_child->youtube_source != "" || $data_child->file_mp4 != "" ? "
									<span class='cover-video'>
										<img src='".env('BASE_CDN', '')."/images/home/trick-mom/icon-video.png'>
									</span>"
								: "")."
							</a>
							<div class='list-body-content-panel'>
								<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-title'>
									<div class='view-panel'>
		                                <img src='".$icon_view."' class='view_icon'><span class='view_number FX'>".number_format($data_child->view)." Views</span>
		                            </div>
									<div class='txt txt-gold'>".($data_child->sub_category != null ? $subcat[$data_child->sub_category] : "")."</div>
									<strong class='FXregular'>".\App\Core\OM::trimWithDot($data_child->title, 56)."</strong>
								</a>
							</div>
							<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-link'>
								<img class='list-body-logo' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABsklEQVRIib2WvW4CMQzHLXgIRsTWxO5asXRiY6/UnYUFnuEeo7qJgZytLrwGEr3DfgIWtk4sSEgn0QGq0g84rq3y3yLZ+SXxRwxQoXnWaWnmRyo0U8aVCpUmtFeh8rieaeZH86zTqtrrrIrg2sY4UaFShdbGmBrToGDfy6euW7DvGdPAGFMVWqtQaYyTIrh2LZCyH6rQ1pgWy+D6SQKNS/ZJAo1lcH1jWqjQVtkPKyHPD9A0xtQEd8Z+XAX5CWrsxwd/TJ8foHnW2BhTZdzk7O/rQL4qZ3+vjBtjTH80UPZDE9z9FXQKNMHdtyctgmsfYuTH55w14J0KPV18mi8y9mMV2n5KGmOcGNPiUoxepje3yvhqjOFa4CGGtDDGCQAc60ioXAbXr3L+DXAZXF+FynnWacGxYNfXZl5dYJJAQ4XWmvkRqNDsbNb8E/BY+DNQxpUxDerA6gKNaaCMK1ChsmDfqwurAyzY91SoBBPa51PX/Q0MAMACPprQ/lKLyqeua0L7uDeLHbOI2RizzqJ2EICIvREgctcHiPiffZwk0k8NEHkGeVeU6epU0ebGU/3nRPwGRm3+AjGDpggAAAAASUVORK5CYII='>
							</a>
						</li>";
					$dataArticle["html"] .= $html_data;
					if($count % 3 == 0 || $key == $itemCount){
						 $count = 0;
					} else{
						$dataArticle["html"] .= '';
					}
		        }
		    }else{
				$dataArticle["html"] .= "";
		    }

			$view_cache = (string)view('member_web.bookmark',$dataArticle);
	        Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

 		return $view_cache;
	}

	public function bookmarkList(){
		$data = Input::all();
		$dt = array();
		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		if($member_web_id == ""){
			App::abort(404);
			exit();
		}
		$cache_name = 'MomtipBookmarkListCache_'. $member_web_id . '_' . $data["page"] . '_' . $data["limit"] . '_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
	        $dt['user'] = $member_web_id;
	        $dt['page'] = $data["page"];
	        $dt['limit'] = $data["limit"];
	        $icon_view = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAASCAYAAAA6yNxSAAABTElEQVR4Ac2WJVBEURSG1y3iDhnvM9szBW1IpOP0gLtDpG9eKi4Nh4q768/MCWfuPDkX35mv3PPL0/vWofNrr03PA60gCg7BG3EIojTLk6XJS12gCMyBNyFz5HF9tTwMlk1KFsAEsWCkIW/4M8Uh0GUSOgNyDDw5NDPydIGQtLwQbJgETQOPhddDGiPvBii0Ky8BNyYB2yCoPBuFhIutB0lrlHEDSszKW0hkRhXTpoI1NlsDqWxeY5PVwoudoEfwZMcyT8RgHmHzOEFeD3B+iAcE4nMW7gYvqobW3Ex3IcgdkB7ArXK77gw0d4rmVnYA8luQxcJHDOYjbJ4lvQU6D2G9sk9MgQdiCoTYvEHwEGq/hicg2eC99yhryaTVew2FG9E8iLXwxoIF/Y1IbyveBxUgwPQBWtuXb8Vf/xjdgFXiRvAx+p+f4z//Q/IOPYCpMz+bvCEAAAAASUVORK5CYII=";
	        $databookmark = $this->dataTable->getBookmarkByUser($dt['user'],$dt['page'],$dt['limit']+1);
	        $type_mom_array = array(1=>"pregnancy-mom", 2=>"lactating-mom", 3=>"toddler-mom");
	        $count = 0;
	        $countLoop = 0;


	        $return_dt["l"] = "n";
	        $bookmark_count = (array)$this->dataTable->countBookmark();
	        $return_dt["count"] = "(". $bookmark_count["count"] .")";

	    	$return_dt["html"] = "";
	    	$itemCount = (count($databookmark)-1);
	    	if($databookmark){
	    		$dt_sub_category = $this->dt_home->getChildSubCat();
	    		foreach ($dt_sub_category as $key => $dt_sub_category) {
		        	$subcat[$dt_sub_category->sub_category_child_id] = $dt_sub_category->sub_category_text_th;
		        }
		        foreach ($databookmark as $key => $data_child) {
		        	$count++;
		        	$countLoop++;
		        	if($countLoop <= $data["limit"]){
			        	$type_mom_data =  $type_mom_array[$data_child->type_mom];
			        	$html_subpanel = "";
			    		$html_data = "";
			    		$img_thumb = \App\Core\OMImage::readFileName($data_child->image_gen,$data_child->image,'c280x207','trick_mom');
			    		$html_data .=
			    		"<li data-id='".$data_child->trick_mom_id."' class='".(($count% 2 == 0) ? "list-item list-item-center":"list-item")." list-item-momtip'>
			    				<div class='cat-title'>
		                    		<a href='".\App\Core\Lng::url()."/".$type_mom_data ."'>
		                            	<div class='txt txt-blue FXMed'>".$data_child->title_cat."</div>
		                            </a>
		                        </div>
								<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-img-panel'>
									<img src='".$img_thumb."' alt=''>".
									($data_child->youtube_source != "" || $data_child->file_mp4 != "" ? "
										<span class='cover-video'>
											<img src='".env('BASE_CDN', '')."/images/home/trick-mom/icon-video.png'>
										</span>"
									: "")."
								</a>
								<div class='list-body-content-panel'>
									<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-title'>
										<div class='view-panel'>
			                                <img src='".$icon_view."' class='view_icon'><span class='view_number FX'>".number_format($data_child->view)." Views</span>
			                            </div>
										<div class='txt txt-gold'>".($data_child->sub_category != null ? $subcat[$data_child->sub_category] : "")."</div>
										<strong class='FXregular'>".\App\Core\OM::trimWithDot($data_child->title, 56)."</strong>
									</a>
								</div>
								<a href='".\App\Core\Lng::url()."/".$type_mom_data."/".$data_child->slug."' class='list-body-link'>
									<img class='list-body-logo' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABsklEQVRIib2WvW4CMQzHLXgIRsTWxO5asXRiY6/UnYUFnuEeo7qJgZytLrwGEr3DfgIWtk4sSEgn0QGq0g84rq3y3yLZ+SXxRwxQoXnWaWnmRyo0U8aVCpUmtFeh8rieaeZH86zTqtrrrIrg2sY4UaFShdbGmBrToGDfy6euW7DvGdPAGFMVWqtQaYyTIrh2LZCyH6rQ1pgWy+D6SQKNS/ZJAo1lcH1jWqjQVtkPKyHPD9A0xtQEd8Z+XAX5CWrsxwd/TJ8foHnW2BhTZdzk7O/rQL4qZ3+vjBtjTH80UPZDE9z9FXQKNMHdtyctgmsfYuTH55w14J0KPV18mi8y9mMV2n5KGmOcGNPiUoxepje3yvhqjOFa4CGGtDDGCQAc60ioXAbXr3L+DXAZXF+FynnWacGxYNfXZl5dYJJAQ4XWmvkRqNDsbNb8E/BY+DNQxpUxDerA6gKNaaCMK1ChsmDfqwurAyzY91SoBBPa51PX/Q0MAMACPprQ/lKLyqeua0L7uDeLHbOI2RizzqJ2EICIvREgctcHiPiffZwk0k8NEHkGeVeU6epU0ebGU/3nRPwGRm3+AjGDpggAAAAASUVORK5CYII='>
								</a>
							</li>";
						$return_dt["html"] .= $html_data;
						if($count % 3 == 0 || $key == $itemCount){
							 $return_dt["html"] .= '<li class="clearfix"></li>';
							 $count = 0;
						} else{
							$return_dt["html"] .= '';
						}

					}else{
						$return_dt["l"] = "y";
					}
		        }
		    }else{
				$return_dt["html"] .= "<span class='no-data'>" . trans('member_web_bookmark.BOOKMARK_NO_DATA') . "</span>";
		    }
		    $view_cache = $return_dt;
		    Cache::put($cache_name, $return_dt, 10);
		}
        return Response::json($view_cache);
	}

	public function removeBookmark(){
		$data = Input::all();
		$dt = array();
        $dt['member_web_id'] = \Illuminate\Support\Facades\Session::get("webMemberId");
        $dt['momtip_id'] = $data["momtip_id"];
        $databookmark = $this->dataTable->removeBookmarkByUser($dt['momtip_id'],$dt['member_web_id']);
       	if($databookmark > 0 && $this->dataTable->error_message == ""){
       		$return_dt["cmd"] = "y";
       		$return_dt["message"] = "ok";
       		ClearCache::clear();
       	}else{
       		$return_dt["cmd"] = "n";
       		$return_dt["message"] = $databookmark;
       		$return_dt["err"] = $this->dataTable->error_message;
       	}
        return Response::json($return_dt);
	}

	function addBookmark(){
		$data = Input::all();
		$dt = array();
        $dt['member_web_id'] = \Illuminate\Support\Facades\Session::get("webMemberId");
        $dt['momtip_id'] = $data["momtip_id"];
        $databookmark = $this->dataTable->addBookmarkByUser($dt['momtip_id'],$dt['member_web_id']);
       	$return_dt["cmd"] = "y";
       	if($databookmark > 0){
       		$return_dt["message"] = "ok";
       		ClearCache::clear();
       	}else if($databookmark == 0){
       		$return_dt["message"] = "Cannot save Bookmark.";
       	}else{
       		$return_dt["message"] = "Duplicate Or Sql Error.";
       	}
       	$bookmark_count = (array)$this->dataTable->countBookmark();
        $return_dt["count"] = "(". $bookmark_count["count"] .")";

        return Response::json($return_dt);
	}
	function verify($token) {
		if(\Illuminate\Support\Facades\Session::get("webMemberId") != ""){
			return redirect(App::getLocale() . '/home');
		}
		$token_dt = $this->dataTable->getToken($token);
		$view_status = "fail";
		$result_status = "";
		if (count($token_dt) == 1) {
			$token_array = (array)$token_dt[0];
			$this->dataTable->updateTokenVerify($token_array["web_member_id"]);
			// ViewSession::set("verify_status",trans("verify.VERIFY_SUCCESS"));
			$result_status = trans("verify.VERIFY_SUCCESS");
			$view_status = "success";

		} else {
			// ViewSession::set("verify_status",trans("verify.VERIFY_FAIL"));
			$result_status = trans("verify.VERIFY_FAIL");
		}
		$cache_name = "verify_page_".App::getLocale() . "_" . $token;
		$view_cache = Cache::get($cache_name);
		if ($view_cache == "") {
			$view_cache = (string)view('verify.verify_result_'.$view_status , array('result_status' => $result_status ));
			Cache::put($cache_name,$view_cache,10);
		}
		ViewSession::viewSessionGlobal();
		$view_cache = ViewSession::make($view_cache);
		return $view_cache;
	}
	public function reset_pass() {
		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberIdPassExpire");
		if ($member_web_id != "") {
			$cache_name = "PasswordForm_".App::getLocale();
			$view_cache = Cache::get($cache_name);
			// if ($view_cache == "") {
			$view_cache = (string)view('password_form.form');
			Cache::put($cache_name,$view_cache,10);
			// }
			ViewSession::viewSessionGlobal();
			$view_cache = ViewSession::make($view_cache);
			return $view_cache;
		} else {
			App::abort(404);
		}
	}
	public function change_pass() {
		$data = Input::all();
		//var_dump($data);
		if (empty($data["new_pass"])) {
			$return_data["status"] = "fail";
			return json_encode($return_data);
		}
        $pass_check = true;
        if (strlen($data["new_pass"]) < 10) {
            $pass_check = false;
            //$return_data["debug"][] = "length";
        }
        if (!preg_match("/[A-Z]/",$data["new_pass"])) {
            $pass_check = false;
            //$return_data["debug"][] = "/[A-Z]/";
        }
        if (!preg_match("/[a-z]/",$data["new_pass"])) {
            $pass_check = false;
            //$return_data["debug"][] = "/[a-z]/";
        }
        if (!preg_match("/[0-9]/",$data["new_pass"])) {
            $pass_check = false;
            //$return_data["debug"][] = "/[0-9]/";
        }
        if (!preg_match("/[ !\"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~]/",$data["new_pass"])) {
            $pass_check = false;
            //$return_data["debug"][] = "special";
        }
        if ($pass_check == false) {
            $return_data["status"] = "fail2";
            return json_encode($return_data);
        }
		$member_id = \Illuminate\Support\Facades\Session::get("webMemberIdPassExpire");
		$user_data = $this->dataTable->getUserData($member_id);

		$passNewMD5 = md5($data["new_pass"]);
		$passNewSha = hash('sha256',$data["new_pass"]);

		if ($passNewMD5 == $user_data[0]->password || $passNewSha == $user_data[0]->password) {
			$return_data["status"] = "using";
			return json_encode($return_data);
		}
		$check_pass = $this->dataTable->getPasswordHistory($passNewMD5,$member_id);
		$check_pass_sha = $this->dataTable->getPasswordHistory($passNewSha,$member_id);
		if (count($check_pass) || count($check_pass_sha)) {
			$return_data["status"] = "fail";
			return json_encode($return_data);
		}
		$update_data["password"] = $passNewSha;
		$update_password = $this->dataTable->updatePassword($member_id,$update_data);
		$record_password_history = $this->dataTable->keepOldPassword($member_id,$user_data[0]->password);
		if ($update_password && $record_password_history) {
			\Illuminate\Support\Facades\Session::forget("webMemberIdPassExpire");
			\Illuminate\Support\Facades\Session::forget("web_member_display_name");
			\Illuminate\Support\Facades\Session::forget("web_member_email");
			if (\Illuminate\Support\Facades\Session::get("new_pass_mode") == "reset") {
				$this->dataTable->removeVerifyToken($member_id);
				\Illuminate\Support\Facades\Session::forget("new_pass_mode");
			} else {
				\Illuminate\Support\Facades\Session::forget("new_pass_mode");
			}
			$key = "lockout_" . $user_data[0]->email;
		     if (Cache::has($key)) {
		        Cache::forget($key);
		     }
		     $key = "lockout_" . $data["csrf_token"];
		     if (Cache::has($key)) {
		        Cache::forget($key);
		     }
			$return_data["status"] = "success";
			return json_encode($return_data);
		} else {
			Log::Error("Chage Password Error: Update =".$update_password.", Record = ".$record_password_history);
		}
	}
	public function forget_password() {
		Meta::meta('title', 'ลืมรหัสผ่าน');
		$cache_name = "ForgetPasswordForm_".App::getLocale();
		$view_cache = Cache::get($cache_name);
		if ($view_cache == "") {
			$view_cache = (string)view('password_form.forget_pass');
			Cache::put($cache_name,$view_cache,10);
		}
		ViewSession::viewSessionGlobal();
		$view_cache = ViewSession::make($view_cache);
		return $view_cache;
	}
	public function send_request_reset_pass() {
		$data = Input::All();
		$pattern = '/^(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){255,})(?!(?:(?:\\x22?\\x5C[\\x00-\\x7E]\\x22?)|(?:\\x22?[^\\x5C\\x22]\\x22?)){65,}@)(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22))(?:\\.(?:(?:[\\x21\\x23-\\x27\\x2A\\x2B\\x2D\\x2F-\\x39\\x3D\\x3F\\x5E-\\x7E]+)|(?:\\x22(?:[\\x01-\\x08\\x0B\\x0C\\x0E-\\x1F\\x21\\x23-\\x5B\\x5D-\\x7F]|(?:\\x5C[\\x00-\\x7F]))*\\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-+[a-z0-9]+)*\\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-+[a-z0-9]+)*)|(?:\\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\\]))$/iD';
		if ($data["email"] == "" || !preg_match($pattern,$data["email"])) {
			$return_data["status"] = "fail";
			return json_encode($return_data);
		}
		$check_data = $this->dataTable->getMemberByEmail($data["email"]);
		if (count($check_data) > 0) {
			$user_data_dt = $this->dataTable->getUserData($check_data[0]->member_web_id);
			$user_data = (array)$user_data_dt[0];
			$prefix_token = md5($user_data["member_web_id"].$user_data["email"].$user_data["register_date"].date("Y-m-d H:i:s"));
			$suffix_token = md5(date("Y-m-d H:i:s"));
			$token_insert["web_member_id"] = $user_data["member_web_id"];
			$token_insert["token"] = $prefix_token.$suffix_token;
			$token_insert_resp = $this->dataTable->setToken($token_insert);
			if ($token_insert_resp) {
				$mail = new OMMail(true); // notice the \  you have to use root namespace here
				$mail->isSMTP(); // tell to use smtp
				$mail->CharSet = "utf-8"; // set charset to utf8
				// $mail->SMTPAuth = true;  // use smpt auth
				// $mail->AuthType = "CRAM-MD5";  // use smpt auth
				// $mail->SMTPSecure = env('MAIL_SMTP_SECURE'); // or ssl
				Log::Info("Mail =".env('MAIL_HOST') . ":" .env('MAIL_PORT') .":" .  env('MAIL_USERNAME').": ". env('MAIL_PASSWORD'));
				$mail->setFrom("no-reply@s-momclub.com", "S-mom Club Web Member Password Reset No-Reply");
				$mail->Subject = "S-mom Club Web Member Password Reset";
				// $mail->addCustomHeader("X-Transactional-Group: S-mom Club Web Member");
				// $mail->TemplateKey = '17656408854a4bfe';
				$mail->addAddress($user_data["email"],$user_data["firstname"]." ".$user_data["lastname"]);
				$cf_data["CF_content"] = "<div>
                            <div style='font-size:14px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;line-height:27px;'>
                                เรียน ท่านสมาชิก
                            </div>
                            <div style='font-size:14px;line-height:27px;'>
                                <a href='".url()."/".Core\Lng::url()."/password/reset/".$token_insert["token"]."' style='color:#FFF;'><span style='line-height: 37px;padding: 6px 20px;border-radius: 22px;background-color:#8e6d1d'>คลิกที่นี่</span></a> เพื่อกำหนดรหัสผ่านใหม่ได้เลยค่ะ
                            </div>
                            <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;'>
                                หากคุณไม่สามารถคลิกลิงก์นี้ได้ กรุณาคัดลอกลิงค์ด้านล่างไปวางที่เว็บเบราเซอร์ของคุณค่ะ
                            </div>
                            <div style='line-height:20px;font-size: 10px;width:550px;'>
                               <a style='color:#8e6d1d'>".url()."/".Core\Lng::url()."/password/reset/".$token_insert["token"]."</a>
                            </div>
                            <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;'>
                                ขอบคุณค่ะ<br>
                                ทีมงาน S-Mom Club
                            </div>
                        </div>";
				// $mail->MsgHTML(json_encode($cf_data));
				$mail->MsgHTML($cf_data["CF_content"]);
				if ($mail->sendMail()){
					Log::Debug("Password Reset Mail Sent To:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
					$return_data["status"] = "success";
					return json_encode($return_data);
				} else {
					Log::Error("Password Reset Mail Sending Error:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
				}
			}
		}
		$return_data["status"] = "fail";

		if($return_data["status"] = "fail"){
			$return_data["status"] = "success";
		}

		return json_encode($return_data);
	}
	public function password_reset($type,$token) {
		$check_verify = $this->dataTable->checkVerify($token);
		//var_dump($check_verify);
		if (count($check_verify) > 0) {
			$user_data_dt = $this->dataTable->getUserData($check_verify[0]->web_member_id);
			if(count($user_data_dt) > 0){
				$member_data = (array)$user_data_dt[0];
				\Illuminate\Support\Facades\Session::set("new_pass_mode","reset");
				\Illuminate\Support\Facades\Session::set("webMemberIdPassExpire",$member_data["member_web_id"]);
				\Illuminate\Support\Facades\Session::set("web_member_display_name",$member_data["displayname"]);
				\Illuminate\Support\Facades\Session::set("web_member_email",$member_data["email"]);
				$cache_name = "PasswordForm_".App::getLocale();
				$view_cache = Cache::get($cache_name);
				// if ($view_cache == "") {
				$view_cache = (string)view('password_form.form',array('type'=>$type));
				Cache::put($cache_name,$view_cache,10);
				// }
				ViewSession::viewSessionGlobal();
				$view_cache = ViewSession::make($view_cache);
				return $view_cache;
			}else{
				App::abort(404);
			}
		} else {
			App::abort(404);
		}
	}
}
