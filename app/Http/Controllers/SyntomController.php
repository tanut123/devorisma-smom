<?php namespace App\Http\Controllers;

use App;
use App\Syntom;
use Illuminate\Support\Facades\Cache;
use Input;
use Response;
use Meta;
use App\Core;
use App\Core\ViewSession;
use App\Core\Lng;

class SyntomController extends App\Core\Controller {
	protected $dataTable;
	public function __construct(){
    	parent::__construct();
    	$this->dataTable = new Syntom();
	}

	public function convetMonth($input){
		$d_th_month = array(
		        	"0"=>"",
				    "1"=>"ม.ค.",
				    "2"=>"ก.พ.",
				    "3"=>"มี.ค.",
				    "4"=>"เม.ย.",
				    "5"=>"พ.ค.",
				    "6"=>"มิ.ย.",
				    "7"=>"ก.ค.",
				    "8"=>"ส.ค.",
				    "9"=>"ก.ย.",
				    "10"=>"ต.ค.",
				    "11"=>"พ.ย.",
				    "12"=>"ธ.ค."
		           );
		$d_en_month = array(
		        	"0"=>"",
				    "1"=>"JAN",
				    "2"=>"FEB",
				    "3"=>"MAR",
				    "4"=>"APR",
				    "5"=>"MAY",
				    "6"=>"JUN",
				    "7"=>"JUL",
				    "8"=>"AUG",
				    "9"=>"SEP",
				    "10"=>"OCT",
				    "11"=>"NOV",
				    "12"=>"DEC"
		            );
		if(App::getLocale() == "en"){
			return $d_en_month[(int)$input];
		}else{
			return $d_th_month[(int)$input];
		}
	}

	public function createDateString($input){
		list($date,$time) = explode(" ",$input);
		list($year,$month,$days) = explode("-",$date);

		$return_val = $days." ".$this->convetMonth($month)." ".(\App\Core\Lng::url()=="th" ? $year+543 : $year);
		return $return_val;
	}

	public function Detail($slug){
		$cache_name = "syntom_".$slug;
		$syntom_view = Cache::get($cache_name);
		if($syntom_view == "") {
			$table = new Syntom();
			$dt = $table->load_detail($slug);
			if(count($dt) <= 0){
				abort("404");
				exit();
			}

			Meta::meta('title', $dt[0]->title);
			Meta::meta('description', $dt[0]->detail);
			Meta::meta('image', Core\OMImage::readFileName($dt[0]->image_gen,$dt[0]->image, "c230x205", "syntom"));

			$return = (array)$dt[0];
			$return["datestring"] = $this->createDateString($return["valid_date"]);

			$syntom_view = (string)view('syntom.detail',["data" => $return]);
			Cache::put($cache_name,$syntom_view,10);
		}

		ViewSession::viewSessionGlobal();
		$syntom_view = ViewSession::make($syntom_view);
		return $syntom_view;
	}

}
