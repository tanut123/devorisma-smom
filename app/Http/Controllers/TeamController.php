<?php namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\Input;
use Response;
use Meta;
use Cache;
use App\Core\ViewSession;

class TeamController extends App\Core\Controller {

/*    public function __construct()
    {
        parent::__construct();


        Meta::meta('http-equiv', "X-UA-Compatible");
        Meta::meta('content',"IE=9");
    }*/

	public function index() {
		Meta::meta('title', trans('title.TITLE_CONTACT_US') );
		$cache_name = 'TeamCache_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
	        $table = new App\TeamData();
	        $person_data = $table->getAllPerson();
	        if (count($person_data) > 0) {
	            foreach ($person_data as $key => $val) {
	                $data[$key] = (array)$val;
	                $data[$key]["image_url"] = \App\Core\OMImage::readFileName($val->person_pic_gen,$val->person_pic,'c249x228','team_person');
	                $data[$key]["name"] = $data[$key]["name_".App\Core\Lng::url()];
	                $data[$key]["position"] = $data[$key]["position_".App\Core\Lng::url()];

	            };
	            $view_cache = (string)view('team.index',["person_data" => $data]);
	        }else{
	        	$view_cache = (string)view('team.index',["person_data" => array()]);
	        }
	        Cache::put($cache_name, $view_cache, 10);
	    }
	   	ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

	    return $view_cache;
	}
    public function loadAjax() {
        $person_id = Input::get('person_id');
        $table = new App\TeamData();
        $person_data = (array)$table->loadPersonData($person_id)[0];
        if (count($person_data) > 0) {
            $person_data["image_url"] = \App\Core\OMImage::readFileName($person_data['person_pic_gen'],$person_data['person_pic'],'c249x228','team_person');
            $person_data["name"] = $person_data["name_".App\Core\Lng::url()];
            $person_data["position"] = $person_data["position_".App\Core\Lng::url()];
            $person_data["detail"] = $person_data["detail_".App\Core\Lng::url()];
        }
        return Response::json($person_data);
    }
}