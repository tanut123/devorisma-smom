<?php namespace App\Http\Controllers;

use App;
use Meta;
use Crypto;
use Cache;
use App\Core\ViewSession;
use App\Pageviews;

class WyethController extends App\Core\Controller {

	public function __construct()
    {
    	parent::__construct();
    	Meta::meta('title', 'Wyeth Nutrition');
	}

	public function index()
	{
		if (App::getLocale() == "en") {
			return abort(404);
		}
		$cache_name = 'Wyeth_Nutrition_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null || env('ENV_MODE') == "DEV"){
			$view_cache = (string)view('wyeth.index');
	    	Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	   	$view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

	public function legacy($product = null)
	{
		if (App::getLocale() == "en") {
			return abort(404);
		}
		$cache_name = 'Wyeth_Nutrition_' . App::getLocale() . '_Legacy';
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null || env('ENV_MODE') == "DEV"){
			$view_cache = (string)view('wyeth.legacy',["product" => $product]);
	    	Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	   	$view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

	public function detail($page)
	{
		if (App::getLocale() == "en") {
			return abort(404);
		}
		$cache_name = 'Wyeth_Nutrition_' . App::getLocale() . '_' . $page;
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null || env('ENV_MODE') == "DEV"){
			$view_cache = (string)view('wyeth.'.$page);
	    	Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	   	$view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

}
