<?php namespace App\Http\Controllers;

use App;
use Illuminate\Routing\Route;
use Meta;
use Crypto;
use App\Core;
use Input;
use Response;
use View;
use Cache;
use Carbon\Carbon;
use App\Core\ViewSession;
use Log;
use PHPMailer;
use App\MemberWeb;
use DateTime;
use App\Core\OMDateTimeConverter;
use App\Core\OMMail;

class MemberWebAuthen extends App\Core\Controller {
	protected $dataTable;
	public function __construct()
    {
    	parent::__construct();
    	//Meta::meta('title', 'sign in');
    	$this->dataTable = new MemberWeb();
	}

  public function lockReset($value) {
     $key = "lockout_" . $value;
     if (Cache::has($key)) {
        Cache::forget($key);
     }

  }
  public function lockBy($value,$email = '') {
    $key = "lockout_" . $value;
    if (Cache::has($key)) {
		if(Cache::get($key) >= 10){
		  	//locked out
			$return_data["status"] = "lock";
			$return_data["msg"] = "Your account is locked out.";
			if($email != ""){
				$member_model = new \App\MemberWeb();
				$member_model->updateMemberStatus($email);
				$this->mailInvalidLogin($email,"more10");
			}
			echo json_encode($return_data);
			exit();
		}
	      	// invalid user or password
	      	Cache::increment($key, 1);
	    }else{
	      $expiresAt = Carbon::now()->addMinutes(30);
	      Cache::add($key, 1 , $expiresAt);
	    }
	}

	public function sign_in() {
		$inputData = Input::all();
		// $this->lockReset($inputData["ucsrf"]);
    	// $this->lockReset($inputData["ux"]);
		if ($inputData["ucsrf"] != csrf_token()) {
     		$this->lockBy($inputData["ux"],$inputData["ux"]);
			App::abort(404);
		}
        $pass_check = true;
        // $return_data["debug"] = $inputData["up"];
        if (strlen($inputData["up"]) < 10) {
            $pass_check = false;
        }
        if (!preg_match("/[A-Z]/",$inputData["up"])) {
            $pass_check = false;
        }
        if (!preg_match("/[a-z]/",$inputData["up"])) {
            $pass_check = false;
        }
        if (!preg_match("/[0-9]/",$inputData["up"])) {
            $pass_check = false;
        }
        if (!preg_match("/[ !\"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~]/",$inputData["up"])) {
            $pass_check = false;
        }
        if ($pass_check == false) {
            $return_data["status"] = "fail3";
            $return_data["msg"] = "login_failed";
             $this->lockBy($inputData["ucsrf"],$inputData["ux"]);
             $this->mailInvalidLogin($inputData["ux"],"login");
            return json_encode($return_data);
        }

    if (\Illuminate\Support\Facades\Session::get("webMemberId") != "") {
      $return_data["status"] = "success";
      return json_encode($return_data);
    } else {
      $this->lockBy($inputData["ucsrf"],$inputData["ux"]);
      $this->lockBy($inputData["ux"],$inputData["ux"]);
  		$member_model = new \App\MemberWeb();
  		$data = (array)$member_model->userLogin($inputData["ux"]);
			$email = $inputData["ux"];
			if (count($data) == 1) {
				$member_data = (array)$data[0];
				if($member_data["verify_status"] == "true"){
					$old_password = $member_data["password"];
					$passwordCheck = "";
					if(strlen($old_password) == 32){
						$passwordCheck = md5($inputData["up"]);
					}else{
						$passwordCheck = hash('sha256',$inputData["up"]);
					}

					if ( $passwordCheck == $member_data["password"] ) {
						$member_status = $member_data["member_status"];
						if($member_status == 1){
				            // reset login count
				            $this->lockReset($inputData["ucsrf"]);
				            $this->lockReset($inputData["ux"]);
				            $login = $member_model->updateLogin($member_data["email"],0);
										\Illuminate\Support\Facades\Session::set("memberExpireTime",time());

				            // check password_expire
				            $date_current = strtotime(date("Y-m-d H:i:s"));
				            $check_date = strtotime($member_data["password_update_time"]);
				            $diff = ($date_current - $check_date)/60/60/24;
							if ($diff < 90) {
								\Illuminate\Support\Facades\Session::set("webMemberId",$member_data["member_web_id"]);
								\Illuminate\Support\Facades\Session::set("webMemberName",$member_data["firstname"]." ".$member_data["lastname"]);
								$return_data["status"] = "success";
								return json_encode($return_data);
							} else {
								\Illuminate\Support\Facades\Session::set("new_pass_mode","expire");
								\Illuminate\Support\Facades\Session::set("webMemberIdPassExpire",$member_data["member_web_id"]);
								\Illuminate\Support\Facades\Session::set("web_member_display_name",$member_data["displayname"]);
								\Illuminate\Support\Facades\Session::set("webMemberLastLogin",$member_data["last_login"]);
								\Illuminate\Support\Facades\Session::set("web_member_email",$member_data["email"]);
								$return_data["status"] = "password_expire";
								return json_encode($return_data);
							}
						}else if($member_status == 2){
							//block
							$return_data["status"] = "fail4";
							$return_data["msg"] = "login_failed";
							return json_encode($return_data);
						}else{

						}
					}
				}else{
					//not verify
					$this->genVerify($member_data["member_web_id"]);
					$return_data["status"] = "fail5";
					$return_data["msg"] = "login_failed";
		      		// $this->lockBy($inputData["ux"]);
					return json_encode($return_data);
				}
			}
      		$this->lockBy($inputData["ux"],$inputData["ux"]);
      		$this->mailInvalidLogin($inputData["ux"],"login");
      		$return_data["status"] = "fail2";
            $return_data["msg"] = "login_failed";
			return json_encode($return_data);
		}
	}

	public function signInFacebook(){
		$data = Input::all();
		$member_web_model = new \App\MemberWeb();
		$dtMember = $member_web_model->getMemberByFacebookID($data["facebook_id"]);

		$dt_return["cmd"] = "n";
		if(!empty($dtMember) && $dtMember != -1){
			$dtMember = (array)$dtMember[0];
			if ($dtMember["member_web_id"] != "") {
				$login = $member_web_model->updateLogin($dtMember["member_web_id"],0,'facebook');
				\Illuminate\Support\Facades\Session::set("webMemberId",$dtMember["member_web_id"]);
				\Illuminate\Support\Facades\Session::set("webMemberName",$dtMember["firstname"]." ".$dtMember["lastname"]);
				\Illuminate\Support\Facades\Session::set("memberExpireTime",time());
				$dt_return["status"] = "success";
			} else {
				$dt_return["status"] = "fail";
				$dt_return["msg"] = "login_failed";
			}
			$dt_return["cmd"] = "y";
		}else if($dtMember == -1){
			$dt_return["cmd"] = "e";
			$dt_return["status"] = "fail";
			$dt_return["msg"] = "login_failed";
		}else{
			$dt_return["status"] = "fail";
			$dt_return["msg"] = "login_failed";
		}
		return json_encode($dt_return);
	}

	public function sign_out() {
		\Illuminate\Support\Facades\Session::forget("webMemberId");
		\Illuminate\Support\Facades\Session::forget("webMemberName");
		\Illuminate\Support\Facades\Session::forget("webMemberLastLogin");
		\Illuminate\Support\Facades\Session::forget("memberExpireTime");
		$return_data["status"] = "success";
		$return_data["html"] = \App\Core\LoginPanelGen::genLoginForm();
		return json_encode($return_data);
	}

	public function checkDateExpire($last_login_date){
		$now = date("Y-m-d H:i:s");
		$from_time = strtotime($last_login_date);
		$now = strtotime($now);
		$diffTime = round(abs($now - $from_time) / 60);

		if($diffTime < 30){ // 30 minute
			return false;
		}

		return true;
	}

	public function checkUser() {
		$member_model = new \App\MemberWeb();
		$lastLogin = \Illuminate\Support\Facades\Session::get("webMemberLastLogin");
		$webMemberId = \Illuminate\Support\Facades\Session::get("webMemberId");
		$ds = $member_model->getMemberLoginByID($webMemberId);
		$return_data = array('d'=>'y');
		if(count($ds) > 0){
			$checkTime = true;
			if(\Illuminate\Support\Facades\Session::get("memberExpireTime") < (time() - 1200)){
				$checkTime = false;
			}
			if($lastLogin != $ds[0]->last_login_success || $ds[0]->member_status != 1 || !$checkTime){
				\Illuminate\Support\Facades\Session::forget("webMemberId");
				\Illuminate\Support\Facades\Session::forget("webMemberName");
				\Illuminate\Support\Facades\Session::forget("webMemberLastLogin");
				\Illuminate\Support\Facades\Session::forget("memberExpireTime");
				$return_data = array('d'=>'e');
			}
		}
		return json_encode($return_data);
	}

	private function mailInvalidLogin($email,$dataType){
		if ($email == "") {
			Log::Debug("No email exist.");
			return false;
		}

		$message = '';

		if($dataType == 'login'){
			$message = 'คุณกรอกรหัสผ่านไม่ถูกต้อง';
		}else if($dataType == 'more10'){
			$message = 'คุณกรอกรหัสผ่านไม่ถูกต้องจำนวน 10 ครั้ง';
		}

		$data = $this->dataTable->getMemberByEmail($email);
		if(count($data) > 0){
			$user_data = (array)$data[0];
			$prefix_token = md5($user_data["member_web_id"].$user_data["email"].$user_data["register_date"].date("Y-m-d H:i:s"));
			$suffix_token = md5(date("Y-m-d H:i:s"));
			$token_insert["web_member_id"] = $user_data["member_web_id"];
			$token_insert["token"] = $prefix_token.$suffix_token;
			$token_insert_resp = $this->dataTable->setToken($token_insert);
			if ($token_insert_resp) {
				$date_time = new OMDateTimeConverter();
				$submit_date = $date_time->gen_date(date("Y-m-d H:i:s"),"%A ที่ %d %B %Y %H:%M:%S","th");
				$mail = new OMMail(true); // notice the \  you have to use root namespace here
				$mail->isSMTP(); // tell to use smtp
				$mail->CharSet = "utf-8"; // set charset to utf8
				// $mail->SMTPAuth = true;  // use smpt auth
				// $mail->AuthType = "CRAM-MD5";  // use smpt auth
				// $mail->SMTPSecure = env('MAIL_SMTP_SECURE'); // or ssl
				$mail->setFrom("no-reply@s-momclub.com", "S-mom Club Web Member Login No-Reply");
				$mail->Subject = "S-mom Club Web Member Login";
				// $mail->addCustomHeader("X-Transactional-Group: S-mom Club Web Member");
				// $mail->TemplateKey = '17656408854a4bfe';
				$mail->addAddress($user_data["email"],$user_data["firstname"]." ".$user_data["lastname"]);
				$cf_data["CF_content"] = "<div>
		                        <div style='font-size:14px;line-height:27px;margin-top:20px;'>
		                            สวัสดีค่ะ คุณ ".$user_data["firstname"]." ".$user_data["lastname"]."
		                        </div>
		                        <div style='font-size:13px;	-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;line-height:27px;'>
		                            วัน" . $submit_date . " เราพบว่ามีการพยายามลงทะเบียนด้วยบัญชีของคุณ ".$user_data["firstname"]." <br/>จากเว็บไซต์ S-Mom Club
		                        </div>
		                        <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;'>
									" . $message . "
		                            หากไม่ใช่การลงทะเบียนจากคุณหรือมีบุคคลอื่นพยายามลงทะเบียนโดยใช้ข้อมูลของคุณ เพื่อความปลอดภัยสามารถ <a href='".url()."/".Core\Lng::url()."/password/reset/".$token_insert["token"]."' style='color:#FFF;'><span style='line-height: 37px;padding: 6px 20px;border-radius: 22px;background-color:#8e6d1d'>คลิกที่นี่</span></a> เพื่อเปลี่ยนรหัสผ่านของคุณค่ะ
		                        </div>
		                        <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
			-moz-osx-font-smoothing: grayscale;'>
		                                หากคุณไม่สามารถคลิกลิงก์นี้ได้ กรุณาคัดลอกลิงค์ด้านล่างไปวางที่เว็บเบราเซอร์ของคุณค่ะ
		                            </div>
		                        <div style='line-height:20px;font-size: 10px;width:550px;'>
		                           <a style='color:#8e6d1d'>".url()."/".Core\Lng::url()."/password/reset/".$token_insert["token"]."</a>
		                        </div>
		                        <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;'>
		                            ขอบคุณค่ะ<br>
		                            ทีมงาน S-Mom Club
		                        </div>
		                    </div>";
				$mail->MsgHTML($cf_data["CF_content"]);
				// $mail->MsgHTML(json_encode($cf_data));
				if ($mail->sendMail()){
					Log::Debug("login fail Mail Sent To:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
					return true;
				} else {
					Log::Error("login fail Mail Sending Error:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
					return false;
				}
			}else{
				Log::Error("Cannot insert Token");
				return false;
			}
		}else{
			Log::Error("no member data");
			return false;
		}
	}

	public function genVerify($member_id = "") {
		if (!$member_id) {
			$member_id = "1446606033268";
		}
		$data = $this->dataTable->getUserData($member_id);
		if(count($data) > 0){
			$user_data = (array)$data[0];
			$prefix_token = md5($user_data["member_web_id"].$user_data["email"].$user_data["register_date"].date("Y-m-d H:i:s"));
			$suffix_token = md5(date("Y-m-d H:i:s"));
			$token_insert["web_member_id"] = $user_data["member_web_id"];
			$token_insert["token"] = $prefix_token.$suffix_token;
			$token_insert_resp = $this->dataTable->setToken($token_insert);
			if ($token_insert_resp) {
				$mail = new OMMail(true); // notice the \  you have to use root namespace here
				$mail->isSMTP(); // tell to use smtp
				$mail->CharSet = "utf-8"; // set charset to utf8
				$mail->SMTPAuth = true;  // use smpt auth
				$mail->AuthType = "CRAM-MD5";  // use smpt auth
				$mail->SMTPSecure = env('MAIL_SMTP_SECURE'); // or ssl
				$mail->setFrom("no-reply@s-momclub.com", "S-mom Club Web Member Verify No-Reply");
				$mail->Subject = "S-mom Club Web Member Verify";
				$mail->addCustomHeader("X-Transactional-Group: S-mom Club Web Member");
				$mail->TemplateKey = '17656408854a4bfe';
				$mail->addAddress($user_data["email"],$user_data["firstname"]." ".$user_data["lastname"]);
	//			$cf_data["CF_displayname"] = $user_data["firstname"]." ".$user_data["lastname"];
	//			$cf_data["CF_textcontent"] = "Verify link :".url()."/".Core\Lng::url()."/verify/".$token_insert["token"];
				$cf_data["CF_content"] = "<div>
	                            <div style='font-size:14px;line-height:27px;margin-top:20px;'>
	                                ยินดีต้อนรับ คุณ ".$user_data["firstname"]." ".$user_data["lastname"]."
	                            </div>
	                            <div style='font-size:13px;	-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;line-height:27px;'>
	                                ขอบคุณที่ท่านให้ความสนใจและเป็นส่วนหนึ่งกับ S-Mom Club
	                            </div>
	                            <div style='font-size:13px;	-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;line-height:27px;'>
	                                กรุณาคลิกลิงก์ด้านล่างเพื่อยืนยันการสมัครสมาชิก
	                            </div>

	                            <div style='margin-top:20px;line-height:25px;font-size: 14px;width:550px;'>
	                                <a href='".url()."/".Core\Lng::url()."/verify/".$token_insert["token"]."' style='color:#8e6d1d'>คลิกที่นี่</a>
	                            </div>
	                            <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;'>
	                                หากคุณไม่สามารถคลิกลิงก์นี้ได้ กรุณาคัดลอกลิงก์ด้านล่างไปวางที่เว็บเบราเซอร์ของคุณค่ะ
	                            </div>
	                            <div style='line-height:20px;font-size: 10px;width:550px;'>
	                               ".url()."/".Core\Lng::url()."/verify/".$token_insert["token"]."
	                            </div>
	                            <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;'>
	                                ขอบคุณค่ะ<br>
	                                ทีมงาน S-Mom Club
	                            </div>
	                        </div>";
				$mail->MsgHTML(json_encode($cf_data));
				if ($mail->sendMail()){
					Log::Debug("Verify Mail Sent To:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
				} else {
					Log::Error("Verify Mail Sending Error:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
				}
			}
		}else{
			Log::Error("no member data");
		}
	}
}
