<?php namespace App\Http\Controllers;

use App;
use Meta;
use Crypto;
use App\Core;
use Input;
use Response;
use View;

class DownloadController extends App\Core\Controller {
	public function manage($media,$gen,$filename) {
        $file_gen = \App\Core\OMImage::readFileName($gen,$filename,'0x0',$media);
        $file_part = str_replace(url(),base_path()."/public",$file_gen);
        if (file_exists($file_part)) {
            header ("Content-type: octet/stream");
            header ("Content-disposition: attachment; filename=\"".$filename."\";");
            header("Content-Length: ".filesize($file_part));
            readfile($file_part);
            exit();
        } else {
            App::abort(404);
        }
    }
}
