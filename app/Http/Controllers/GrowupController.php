<?php namespace App\Http\Controllers;

use App;
use Meta;
use Crypto;
use Cache;
use App\Core\ViewSession;
use App\Pageviews;

class GrowupController extends App\Core\Controller {

	public function __construct()
    {
    	parent::__construct();
    	Meta::meta('title', 'สฟิงโกไมอีลิน หนึ่งในสารอาหารสำคัญ ต่อสมองลูกน้อย');
    	Meta::meta('description','สฟิงโกไมอีลิน พบมากในนมแม่ อาหารประเภทไข่ นม ชีส และผลิตภัณฑ์นม ช่วยสร้างไมอีลิน ซึ่งมีผลต่อประสิทธิภาพสมองของเด็ก');
    	Meta::set('keywords','สฟิงโกไมอีลิน, พัฒนาสมองเด็ก, นมผง, นมผงเด็ก');
	}

	// public function index(){
	// 	if (App::getLocale() == "en") {
	// 		return abort(404);
	// 	}
	// 	$cache_name = 'DAYS1000List_' . App::getLocale();
	// 	$view_cache = Cache::get($cache_name);
	// 	if($view_cache == "" || $view_cache == null){
	// 		$view_cache = (string)view('1000-days.index');
	//     	Cache::put($cache_name, $view_cache, 10);
	//     }
	// 	ViewSession::viewSessionGlobal();
	//    	$view_cache = ViewSession::make($view_cache);

 //        return $view_cache;
	// }

	// public function subPage($page = null, $sub_page = null){
	// 	if (App::getLocale() == "en") {
	// 		return abort(404);
	// 	}
	// 	if($sub_page == ""){
	// 		$sub_page = "index";
	// 	}
	// 	$cache_name = 'DAYS1000Detail_'. $page . "_" . $sub_page . App::getLocale();

	// 	$module["day-1-90"] = 1;
	// 	$module["day-91-180"] = 2;
	// 	$module["day-181-270"] = 3;
	// 	$module["day-271-453"] = 4;
	// 	$module["day-454-544"] = 5;
	// 	$module["day-545-635"] = 6;
	// 	$module["day-636-757"] = 7;
	// 	$module["day-758-878"] = 8;
	// 	$module["day-879-1000"] = 9;

	// 	$page_view_model = new Pageviews();

	// 	if ($sub_page != "index") {
	// 		$page_view_model->view($page,$module[$sub_page]);
	// 	}

	// 	$view_data = $page_view_model->get1000DaysView($page);

	// 	foreach($view_data as $val) {
	// 		$val_array = (array)$val;
	// 		ViewSession::set($page."_".$val_array["ref_id"],number_format($val_array["total"]));
	// 	}


	// 	$view_cache = Cache::get($cache_name);
	// 	if($view_cache == "" || $view_cache == null){
	// 		$view_cache = (string)view('1000-days.' .$page. '.' .$sub_page, ["page" => $page, "sub_page" => $sub_page]);
	//         Cache::put($cache_name, $view_cache, 10);
	//     }
	// 	ViewSession::viewSessionGlobal();
	//     $view_cache = ViewSession::make($view_cache);

 //        return $view_cache;
	// }

	public function sphingomyelin(){
		if (App::getLocale() == "en") {
			return abort(404);
		}
		$cache_name = 'sphingomyelin_index';
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('sphingomyelin.index');
	    	Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	   	$view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

	public function sphingomyelinPage($page = null, $sub_page = null, $detail_page = null){
		
		if (App::getLocale() == "en") {
			return abort(404);
		}

		$view_page = "";
		$view_subpage = "";
		$page_current = "";
		$detail_menu = "";

		if($page == "การทำงานของสมอง"){
			$view_page = "detail.index";
			$page_current = "brain_fuction";
			if($sub_page == "ผ่าสมองไอน์สไตน์"){
				$view_page = "detail.detail01";
				$detail_menu = "1";
			}else if($sub_page == "เจาะความลับของสมอง"){
				$view_page = "detail.detail02";
				$detail_menu = "2";
			}else if($sub_page == "พัฒนาการสมองกับความฉลาด"){
				$view_page = "detail.detail03";
				$detail_menu = "3";
			}else if($sub_page == "วัยเพื่อการพัฒนาสมอง"){
				$view_page = "detail.detail04";
				$detail_menu = "4";
			}
		}else if($page == "นมแม่"){
			$view_page = "breast_milk";
		}else if($page == "ความสำคัญของ1000วันแรก"){
			if($sub_page == "270วัน"){
				$view_page = "first_1000_days.270_days";
				$view_subpage = "270";
			}else if($sub_page == "635วัน"){
				$view_page = "first_1000_days.635_days";
				$view_subpage = "635";
			}else if($sub_page == "1000วัน"){
				$view_page = "detail.index";
				$view_subpage = "1000";
				$page_current = "1000_days";
				if($detail_page == "ผ่าสมองไอน์สไตน์"){
					$view_page = "detail.detail01";
					$detail_menu = "1";
				}else if($detail_page == "เจาะความลับของสมอง"){
					$view_page = "detail.detail02";
					$detail_menu = "2";
				}else if($detail_page == "พัฒนาการสมองกับความฉลาด"){
					$view_page = "detail.detail03";
					$detail_menu = "3";
				}else if($detail_page == "วัยเพื่อการพัฒนาสมอง"){
					$view_page = "detail.detail04";
					$detail_menu = "4";
			}
			}else{
				$view_page = "first_1000_days.index";
			}
		}else{
			return abort(404);
		}

		$cache_name = 'Sphingomyelin_'. $view_page;

		$view_cache = Cache::get($cache_name);
		// if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('sphingomyelin.' .$view_page,
				["view_subpage" => $view_subpage, "page_current" => $page_current, "detail_menu" => $detail_menu]);
	        Cache::put($cache_name, $view_cache, 10);
	    // }
		ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

}
