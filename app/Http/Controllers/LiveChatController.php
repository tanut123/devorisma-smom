<?php namespace App\Http\Controllers;

use App;
use Meta;
use App\Core;
use View;
use Cache;
use App\Core\ViewSession;

class LiveChatController extends App\Core\Controller {
	public function __construct()
    {
    	parent::__construct();
    	Meta::meta('title', 'ปรึกษาผู้เชี่ยวชาญ');
	}

	public function index(){
		$cache_name = 'LiveChatCache_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('live-chat.commingsoon');
	        Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

	public function mainpage(){
		$cache_name = 'LiveChatCache_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('live-chat.index');
	        Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}
}
