<?php namespace App\Http\Controllers;

use App;
use App\QuestionArticle;
use App\Knowledge;
use App\MemberWeb;
use Illuminate\Support\Facades\Cache;
use Input;
use Response;
use Meta;
use App\Core;
use App\Core\ViewSession;
use App\Core\Lng;

class QuestionArticleController extends App\Core\Controller {
	protected $dataTable;
	public function __construct(){
    	parent::__construct();
    	$this->dataTable = new QuestionArticle();
    	$this->dataMemberWeb = new MemberWeb();
	}

	public function rating(){
		$data = Input::all();
		$mode = $data['mode'];
		$page_uid = $data['page_uid'];
		$rate = $data['rate'];
		if($mode == "add"){
			$dsMemberInsert = $this->dataTable->insertRating($data);
		}
	}

	public function index(){
		if(App::getLocale() == "en"){
			abort("404");
			exit();
		}
		Meta::meta('title', 'คุณแม่ถาม-คุณหมอตอบ' );
		$cache_name = "question_article_index_".App::getLocale();
		$question_article_index_view = Cache::get($cache_name);
		$dt_qa = $this->dataQuestionArticleType();
		if ($question_article_index_view == "") {
			$question_article_index_view = (string)view('question_article.index',$dt_qa);
			Cache::put($cache_name,$question_article_index_view,10);
		}
		ViewSession::viewSessionGlobal();
		$question_article_index_view = ViewSession::make($question_article_index_view);
		return $question_article_index_view;
	}

	public function dataQuestionArticleType(){
		$data["question"] = array();
		$list = array();
		$type = $this->dataTable->getQuestionArticleType();
		if(count($type) > 0){
			foreach ($type as $type_value) {
				$dt = $this->dataTable->getQuestionArticleData($type_value->question_type);
				if(count($dt) > 0){
					foreach ($dt as $key => $value) {
						$d_day = date('d', strtotime($value->valid_date));
						$d_month = date('m', strtotime($value->valid_date));
						if (strlen($value->link_url) > 0) {
		                    $link = $value->link_url;
		                } else {
		                    $link = url()."/หมอเด็ก/".$value->question_type."/".$value->slug;
		                }
		                $list[$value->question_type][$value->question_article_id] = "
							 <a  href='".$link."'>
                                <li class='list-item'>
									<div class='list-img'>
										<img src='".\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c307x252','question_article')."' width='307' height='252'>
									</div>
									<div class='badge-list'>
										<span class='txt-day'>".$d_day."</span>
										<span class='txt-month'>".$this->convertMonth($d_month)."</span>
									</div>
									<div class='view-content'>
										<span class='view-icon icon-qa'></span>
										<span class='view-text FXMed'>" . ($value->total == ""? 0:$value->total ) . " Views</span>
									</div>
									<div class='txt-list-panel'>
										<div class='list-title FX'>".$value->title."</div>
										<div class='short-desc FX'>".\App\Core\OM::trimWithDot($value->description,100)."</div>
									</div>
								</li>
                            </a>";
					}
				}
			}
		}
		$data["question"] = $list;
		return $data;
	}

	public function listData($qa_type = ""){
		if(App::getLocale() == "en"){
			abort("404");
			exit();
		}
		$cache_name = "qa_list_".App::getLocale()."_".$qa_type;
		$list_view = Cache::get($cache_name);

		$list_data = array();
		$list_data["type"] = $qa_type;
		if($qa_type == 1){
			$list_data["type_title"] = "คุณแม่ตั้งครรภ์<br/>รวมบทความดีๆ จากหมอ...สู่เเม่";
		}else if($qa_type == 2){
			$list_data["type_title"] = "คุณแม่ให้นมบุตร<br/>สารพัดบทความดีๆ จากกุมารแพทย์…สู่แม่";
		}else if($qa_type == 3){
			$list_data["type_title"] = "คุณแม่เลี้ยงลูกวัยเตาะแตะ<br/>นานาสาระประโยชน์จากกุมารแพทย์…สู่แม่";
		}else{
			$list_data["type_title"] = "";
		}
		Meta::meta('title', $list_data["type_title"]);

		if ($list_view == "") {
			$list_view = (string)view('question_article.list',$list_data);
			Cache::put($cache_name,$list_view,10);
		}
		ViewSession::viewSessionGlobal();
		$list_view = ViewSession::make($list_view);
		return $list_view;
	}

	public function loadAjax(){
		if(App::getLocale() == "en"){
			abort("404");
			exit();
		}
		$data = Input::all();
		$pagelist = $data["page"];
		$limit_start = $data["limit"];
		$start = ($pagelist - 1) * $limit_start;
		$limit = $data["limit"]+1;
		$category = $data["category"];

		$table = new QuestionArticle();
		$dt = $table->loadlist($category,$start,$limit);

		$count = 1;
		$count_data = 1;
		$has_loadmore = false;
		$ajax_html = "";

		foreach ($dt as $key => $value) {
			if($count_data <= $limit_start){
				$d_day = date('d', strtotime($value->valid_date));
				$d_month = date('m', strtotime($value->valid_date));
                if (strlen($value->link_url) > 0) {
                    $link = $value->link_url;
                } else {
                    $link = url()."/หมอเด็ก/".$category."/".$value->slug;
                }
				$ajax_html.="
				            <a  href='".$link."'>
                                <li class='list-item ".($count == 2 ? "list-item-center":"")."'>
									<div class='list-img'>
										<img src='".\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c307x252','question_article')."' alt=''>
									</div>
									<div class='badge-list icon-qa'>
										<span class='txt-day FXMed'>".$d_day."</span>
										<span class='txt-month FBold'>".$this->convertMonth($d_month)."</span>
									</div>
									<div class='view-content'>
										<span class='view-icon icon-qa'></span>
										<span class='view-text FXMed'>" . ($value->total == ""? 0:$value->total ) . " Views</span>
									</div>
									<div class='txt-list-panel'>
										<div class='list-title FX'>".$value->title."</div>
										<div class='short-desc FX'>".\App\Core\OM::trimWithDot($value->description,100)."</div>
									</div>
								</li>
                            </a>";
				$count++;
				if($count == 3){
					$count =0;
				}
				$count_data++;
			}else{
				$has_loadmore = true;
			}
		}
		return Response:: json(["ajax_html" => $ajax_html,"has_loadmore" => $has_loadmore]);
	}

	public function listDetail($qa_type,$slug){
		if(App::getLocale() == "en"){
			abort("404");
			exit();
		}
		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		$cache_name = "question_article_detail_".$slug."_".App::getLocale()."_".$qa_type."_".$member_web_id;
		$detail_view = Cache::get($cache_name);
		if ($detail_view == "") {
			$table = new QuestionArticle();
			$dt = $table->load_detail($qa_type,$slug);
			if(count($dt) <= 0){
				abort("404");
				exit();
			}

			Meta::meta('title', $dt[0]->title);
			Meta::meta('description', $dt[0]->detail);
			Meta::meta('image', Core\OMImage::readFileName($dt[0]->image_gen,$dt[0]->image, "c230x205", "question_article"));

			$dt[0]->category = $qa_type;
			$dt[0]->relate = $table->getRelatedData($qa_type,$slug);
			$momtip_list =  new Knowledge();
			$dt[0]->relate_momtip = $momtip_list->getRelatedData($qa_type);
			$dt[0]->title_related = $qa_type;

			$bookmarkDataArr = $this->dataMemberWeb->getBookmarkByMember($member_web_id);
			$dataBookmark = [];
	        foreach ($bookmarkDataArr as $key => $dataArr) {
	        	$dataBookmark[] = $dataArr->trick_mom_id;
	        }
			$dt[0]->bookmarkList = $dataBookmark;
			$dt[0]->member_web_id = $member_web_id;

			$return = (array)$dt[0];
			$return["datestring"] = $this->createDateString($return["valid_date"]);

			$detail_view = (string)view('question_article.listdetail',["data" => $return]);
			Cache::put($cache_name,$detail_view,10);
		}

		ViewSession::viewSessionGlobal();
		$detail_view = ViewSession::make($detail_view);

		return $detail_view;
	}

	public function createDateString($input){

		list($date,$time) = explode(" ",$input);
		list($year,$month,$days) = explode("-",$date);

		$return_val = $days." ".$this->convertMonth($month)." ".(\App\Core\Lng::url()=="th" ? $year+543 : $year);
		return $return_val;
	}

	public function convertMonth($input){
		$d_th_month = array(
		        	"0"=>"",
				    "1"=>"ม.ค.",
				    "2"=>"ก.พ.",
				    "3"=>"มี.ค.",
				    "4"=>"เม.ย.",
				    "5"=>"พ.ค.",
				    "6"=>"มิ.ย.",
				    "7"=>"ก.ค.",
				    "8"=>"ส.ค.",
				    "9"=>"ก.ย.",
				    "10"=>"ต.ค.",
				    "11"=>"พ.ย.",
				    "12"=>"ธ.ค."
		           );
		$d_en_month = array(
		        	"0"=>"",
				    "1"=>"JAN",
				    "2"=>"FEB",
				    "3"=>"MAR",
				    "4"=>"APR",
				    "5"=>"MAY",
				    "6"=>"JUN",
				    "7"=>"JUL",
				    "8"=>"AUG",
				    "9"=>"SEP",
				    "10"=>"OCT",
				    "11"=>"NOV",
				    "12"=>"DEC"
		            );
		if(App::getLocale() == "en"){
			return $d_en_month[(int)$input];
		}else{
			return $d_th_month[(int)$input];
		}
	}

}