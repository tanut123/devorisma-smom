<?php namespace App\Http\Controllers;

use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Meta;
use App\Member;
use App\Core;
use Cache;
use App\Core\ViewSession;
use PHPMailer;
use App\MemberWeb;

class TestModuleController extends App\Core\Controller {

	public function __construct(){
    	parent::__construct();
    	Meta::meta('title', 'Test');
	}
	public function index(){

	}
	public function password_expire() {
		$view = view('testModule.passwrod_expire');
		ViewSession::viewSessionGlobal();
		$view_cache = ViewSession::make($view);
		return $view_cache;
	}
	public function password_expire_set() {
		$data = Input::All();
		$date_set = date("Y-m-d H:i:s",mktime(date("H"),date("i"),date("s"),date("m"),date("d")-90,date("Y")));
		echo "Set Date Expire To:".$date_set;
		$dt = new MemberWeb();
		$res = $dt->setExpireDate($data["email"],$date_set);
		if ($res) {
			echo " Success.";
		} else {
			echo " Fail.";
		}
	}
	public function test_captcha() {
		echo "11".Session::get("captcha_code");
		exit();
	}
}
