<?php namespace App\Http\Controllers;

use App;
use Meta;
use Crypto;
use App\Core;
use Input;
use Response;
use View;
use Illuminate\Http\Request;

class CustomCssController extends App\Core\Controller {
    public function getCssForPage(Request $request, $filename_str) {
        $file_explode  = explode("&",$filename_str);
        if (count($file_explode) == 2) {
            list($gen,$filename) = $file_explode;
            $dt_table = new App\CustomPage();
            $data = (array)$dt_table->loadCustomCss($gen,$filename);
            $server_modify_date = $request->server('HTTP_IF_MODIFIED_SINCE');
            if (count($data) > 0) {
                $data = (array)$data[0];
                $modify_data = strtotime($data["upload_date"]);
                if (isset($server_modify_date) && strtotime($server_modify_date) >= $modify_data) {
                    //header('HTTP/1.0 304 Not Modified');
                    $response = Response::make(null, 304);
                    return $response;
                }
                $modify_date_string = gmdate('D, d M Y H:i:s T',  $modify_data);
                $response = Response::make($data["data"]);
                $response->header('Content-Type', 'text/css');
                $response->header('Last-Modified', $modify_date_string);
                return $response;
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
    public function getCssForWCM($gen) {
        $dt_table = new App\CustomPage();
        $data = (array)$dt_table->loadCustomCss($gen);
        if (!file_exists(public_path()."/css/site_tiny.css")) {
            copy(base_path()."/resources/assets/css/site_tiny.css" , public_path()."/css/site_tiny.css");
        }
        if (count($data) > 0) {
            $data = (array)$data[0];
            $font_data = readfile(base_path()."/resources/assets/css/font.css");
            $response = Response::make($font_data.$data["data"]);
            $response->header('Content-Type', 'text/css');
            return $response;
        } else {
            return "";
        }
        session();
    }
}
