<?php namespace App\Http\Controllers;

use App;
use App\Knowledge;
use App\MemberWeb;
use Illuminate\Support\Facades\Cache;
use Input;
use Response;
use Meta;
use App\Core;
use App\Core\ViewSession;
use App\Core\Lng;

class KnowledgeController extends App\Core\Controller {
	protected $dataTable;
	public function __construct(){
    	parent::__construct();
    	$this->dataTable = new Knowledge();
    	$this->dataMemberWeb = new MemberWeb();
	}

	public function rating(){
		$data = Input::all();
		$mode = $data['mode'];
		$page_uid = $data['page_uid'];
		$rate = $data['rate'];
		if($mode == "add"){
			$dsMemberInsert = $this->dataTable->insertRating($data);
		}
	}

	public function index($know_type = ""){
		Meta::meta('title', trans('title.TITLE_KNOWLEDGE') );
		$cache_name = "knowledge_index_".App::getLocale()."_".$know_type;
		$knowledge_index_view = Cache::get($cache_name);
		$dt_category = $this->dataCategory($know_type);
		if ($knowledge_index_view == "") {
			$knowledge_index_view = (string)view('knowledge.index',$dt_category);
			Cache::put($cache_name,$knowledge_index_view,10);
		}
		ViewSession::viewSessionGlobal();
		$knowledge_index_view = ViewSession::make($knowledge_index_view);
		return $knowledge_index_view;
	}


	public function dataCategory($know_type){
		$know_type_db = "";
		$know_title = "";
		$know_desc = "";
		$know_type_list["knowledge_subcategory"] = array();
		if($know_type == "pickyeater"){
			$know_type_db = "b";
			$know_title = trans('title.TITLE_KNOWLEDGE_PICKYEATER');
			$know_desc = trans('desc.DESC_KNOWLEDGE_PICKYEATER');
		}else if($know_type == "pregnancy"){
			$know_type_db = "a";
			$know_title = trans('title.TITLE_KNOWLEDGE_PREGNANCY');
			$know_desc = trans('desc.DESC_KNOWLEDGE_PREGNANCY');
		}else if($know_type == "toddler"){
			$know_type_db = "d";
			$know_title = trans('title.TITLE_KNOWLEDGE_TODDLER');
			$know_desc = trans('desc.DESC_KNOWLEDGE_TODDLER');
		}
		$know_type_list["know_category"] = $know_type;
		$know_type_list["knowledge_title"] = $know_title;
		$know_type_list["knowledge_desc"] = $know_desc;
		$table = new Knowledge();
		$sub = $table->getSubCategory($know_type_db);
		$list = array();
		if (count($sub) > 0) {
			foreach ($sub as $sub_value) {
				$dt = $table->getCategory($sub_value->knowledge_subcategory);
				if(count($dt) > 0){
					foreach ($dt as $key => $value) {
						$know_sub_type = substr($sub_value->knowledge_subcategory,1,1);
						$d_day = date('d', strtotime($value->valid_date));
						$d_month = date('m', strtotime($value->valid_date));
						if (strlen($value->link_url) > 0) {
		                    $link = $value->link_url;
		                } else {
		                    $link = url()."/".\App\Core\Lng::url()."/".$know_type."/".$know_sub_type."/".$value->slug;
		                }
						$list[$value->knowledge_subcategory][$value->knowledge_list_id] = "
						            <a  href='".$link."'>
		                                <li class='list-item'>
											<div class='list-img'>
												<img src='".\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c307x252','knowledge_list')."' width='307' height='252'>
											</div>
											<div class='badge-list'>
												<span class='txt-day'>".$d_day."</span>
												<span class='txt-month'>".$this->convetMonth($d_month)."</span>
											</div>
											<div class='txt-list-panel'>
												<div class='list-title'>".$value->title."</div>
												<div class='short-desc'>".\App\Core\OM::trimWithDot($value->description,100)."</div>
											</div>
										</li>
		                            </a>";
					}
				}
			}
		}
		$know_type_list["knowledge_subcategory"] = $list;
		return $know_type_list;
	}

	public function listIndex($know_type = "",$know_sub_type = ""){
		$cache_name = "knowledge_list_".App::getLocale()."_".$know_type."_".$know_sub_type;
		$knowledge_list_view = Cache::get($cache_name);

		$know_type_db = "";
		if($know_type == "pickyeater"){
			$know_type_db = "b";
		}else if($know_type == "pregnancy"){
			$know_type_db = "a";
		}else if($know_type == "toddler"){
			$know_type_db = "d";
		}
		$know_sub_type_db = $know_type_db.$know_sub_type;
		$know_data = array();
		$know_data["type"] = $know_type;
		$know_data["sub_type"] = $know_sub_type;
		$know_data["sub_type_name"] = $know_sub_type_db;

		Meta::meta('title', trans('subtitle.SUBTITLE_KNOWLEDGE_'.strtoupper($know_sub_type_db)) );

		if ($knowledge_list_view == "") {
			$knowledge_list_view = (string)view('knowledge.list',$know_data);
			Cache::put($cache_name,$knowledge_list_view,10);
		}
		ViewSession::viewSessionGlobal();
		$knowledge_list_view = ViewSession::make($knowledge_list_view);
		return $knowledge_list_view;
	}

	public function listIndexNoSub($knowledge_type_no_sub){
		$cache_name = "knowledge_list_".App::getLocale()."_".$knowledge_type_no_sub;
		$knowledge_list_view = Cache::get($cache_name);

		$know_type = $knowledge_type_no_sub;
		$know_sub_type = "";
		$know_sub_type_db = "";

		$know_data = array();
		$know_data["type"] = $know_type;
		$know_data["sub_type"] = $know_sub_type;
		$know_data["sub_type_name"] = $knowledge_type_no_sub;

		$metaTitle = "";
		if( $knowledge_type_no_sub == "ลูกน้อยถ่ายสบาย" ){
			$metaTitle = "excrete";
		}else{
			$metaTitle = $knowledge_type_no_sub;
		}

		Meta::meta('title', trans('subtitle.SUBTITLE_KNOWLEDGE_'.strtoupper($metaTitle)) );

		if ($knowledge_list_view == "") {
			$knowledge_list_view = (string)view('knowledge.list',$know_data);
			Cache::put($cache_name,$knowledge_list_view,10);
		}
		ViewSession::viewSessionGlobal();
		$knowledge_list_view = ViewSession::make($knowledge_list_view);
		return $knowledge_list_view;
	}

	public function loadAjax(){
		$data = Input::all();
		$pagelist = $data["page"];
		$limit_start = $data["limit"];
		$start = ($pagelist - 1) * $limit_start;
		$limit = $data["limit"]+1;
		$category = $data["category"];
		$sub_category = $data["sub_category"];
		$category_db = "";
		$sub_category_db = "";

		if($category == "pickyeater"){
			$category_db = "b";
		}else if($category == "pregnancy"){
			$category_db = "a";
		}else if($category == "braindevelopment"){
			$category_db = "c";
		}else if($category == "toddler"){
			$category_db = "d";
		}else if($category == "lactating"){
			$category_db = "e";
		}else if($category == "ลูกน้อยถ่ายสบาย"){
			$category_db = "f";
		}

		if($category_db == "c" || $category_db == "e" || $category_db == "f"){
			$sub_category_db = "";
		}else{
			$sub_category_db = $category_db.$sub_category;
		}

		$table = new Knowledge();
		$dt = $table->loadlist($category_db,$sub_category_db,$start,$limit);

		$count = 1;
		$count_data = 1;
		$has_loadmore = false;
		$ajax_html = "";

		foreach ($dt as $key => $value) {
			if($count_data <= $limit_start){
				$d_day = date('d', strtotime($value->valid_date));
				$d_month = date('m', strtotime($value->valid_date));
                if (strlen($value->link_url) > 0) {
                    $link = $value->link_url;
                } else if($category == "braindevelopment" || $category == "lactating" || $category == "ลูกน้อยถ่ายสบาย"){
                    $link = url()."/".\App\Core\Lng::url()."/".$category."/".$value->slug;
                } else {
                	 $link = url()."/".\App\Core\Lng::url()."/".$category."/".$sub_category."/".$value->slug;
                }
				$ajax_html.="
				            <a  href='".$link."'>
                                <li class='list-item lazyload ".($count == 2 ? "list-item-center":"")."' data-bgset='". env('BASE_CDN', '') ."/images/knowledge/bg_list.png' data-expand='+10'>
									<div class='list-img'>
										<img class='lazyload' data-src='".\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c307x252','knowledge_list')."' data-expand='+10' width='307' height='252'>
									</div>
									<div class='badge-list lazyload' data-bgset='". env('BASE_CDN', '') ."/images/knowledge/badge.png' data-expand='+10'>
										<span class='txt-day'>".$d_day."</span>
										<span class='txt-month'>".$this->convetMonth($d_month)."</span>
									</div>
									<div class='txt-list-panel'>
										<div class='list-title'>".$value->title."</div>
										<div class='short-desc'>".\App\Core\OM::trimWithDot($value->description,100)."</div>
									</div>
								</li>
                            </a>";
				$count++;
				if($count == 3){
					$count =0;
				}
				$count_data++;
			}else{
				$has_loadmore = true;
			}
		}
		return Response:: json(["ajax_html" => $ajax_html,"has_loadmore" => $has_loadmore]);
	}

	public function convetMonth($input){
		$d_th_month = array(
		        	"0"=>"",
				    "1"=>"ม.ค.",
				    "2"=>"ก.พ.",
				    "3"=>"มี.ค.",
				    "4"=>"เม.ย.",
				    "5"=>"พ.ค.",
				    "6"=>"มิ.ย.",
				    "7"=>"ก.ค.",
				    "8"=>"ส.ค.",
				    "9"=>"ก.ย.",
				    "10"=>"ต.ค.",
				    "11"=>"พ.ย.",
				    "12"=>"ธ.ค."
		           );
		$d_en_month = array(
		        	"0"=>"",
				    "1"=>"JAN",
				    "2"=>"FEB",
				    "3"=>"MAR",
				    "4"=>"APR",
				    "5"=>"MAY",
				    "6"=>"JUN",
				    "7"=>"JUL",
				    "8"=>"AUG",
				    "9"=>"SEP",
				    "10"=>"OCT",
				    "11"=>"NOV",
				    "12"=>"DEC"
		            );
		if(App::getLocale() == "en"){
			return $d_en_month[(int)$input];
		}else{
			return $d_th_month[(int)$input];
		}
	}

	public function createDateString($input){

		list($date,$time) = explode(" ",$input);
		list($year,$month,$days) = explode("-",$date);

		$return_val = $days." ".$this->convetMonth($month)." ".(\App\Core\Lng::url()=="th" ? $year+543 : $year);
		return $return_val;
	}

	public function listDetail($know_type,$know_sub_type,$slug){
		$arr_fix = array();
		$arr_fix['พัฒนาการเด็ก1เดือน'] =  array('title' => 'พัฒนาการเด็ก 1 เดือน สิ่งที่พ่อแม่มือใหม่ทุกคนต้องรู้', 'des' => 'จากทารกแรกเกิด 0 เดือน สู่พัฒนาการเด็กวัย 1 เดือนแรก เป็นช่วงวัยที่ต้องเอาใจใส่เป็นพิเศษ พ่อแม่ควรเข้าใจกับพัฒนาการทารกทั้งด้านสมอง การตอบสนอง พฤติกรรมเด็ก 1 เดือน อะไรคือกุญแจสำคัญของพัฒนาการเด็ก 0-1 เดือน', 'keywords' => 'พัฒนาการเด็ก 1 เดือน, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['พัฒนาการเด็ก2เดือน'] =  array('title' => 'พัฒนาการเด็ก 2 เดือน เคล็ดลับสำคัญส่งเสริมพัฒนาการ', 'des' => 'พ่อแม่จะต้องทำอย่างไรเพื่อส่งเสริมพัฒนาการที่ดีให้กับเด็กวัย 2 เดือน ในช่วงวัยนี้พ่อแม่มือใหม่คงจะเริ่มเข้าใจอะไรหลายอย่างมากขึ้น แต่ยังมีสิ่งที่ต้องรู้อีกมากเกี่ยวกับพัฒนาการเด็กวัย 1 เดือนไปสู่พัฒนาการเด็ก 2 เดือนนี้', 'keywords' => 'พัฒนาการเด็ก 2 เดือน, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['น้ำนมแม่ไม่พอไม่มี'] =  array('title' => 'น้ำนมแม่ไม่มีพอ 6 เคล็ดลับสำหรับแม่ที่น้ำนมไม่พอเลี้ยงเจ้าตัวเล็ก', 'des' => 'เมื่อคุณไม่มีน้ำนมเพียงพอต่อการเลี้ยงลูก น้ำนมแม่ไม่มี น้ำนมแม่ไม่พอ น้อยเกินไป นมคัด มาดูเคล็ดลับทั้ง 6 วิธีช่วยคุณแม่เพิ่มน้ำนมแม่ให้เพียงพอต่อการเลี้ยงเจ้าตัวเล็ก', 'keywords' => 'น้ำนมแม่ไม่พอไม่มี, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['พัฒนาการเด็ก1ขวบ'] =  array('title' => 'พัฒนาการเด็ก 1 ขวบ อะไรคือกุณแจสำคัญให้ลูกพัฒนาเร็วขึ้น', 'des' => 'ลูกอายุครบ 1 ขวบแล้ว มีอะไรเปลี่ยนแปลงไปบ้าง เมื่อลูกอายุ 1 ขวบ พ่อแม่จะเริ่มได้ทำกิจกรรม สื่อสาร การพูดคุยและมีการตอบสนองมากขึ้นจากลูกน้อยเป็นครั้งแรก พัฒนาการเด็ก 1 ขวบ อะไรเป็นกุญแจสำคัญให้ลูกพัฒนาได้อย่างรวดเร็ว', 'keywords' => 'พัฒนาการเด็ก 1 ขวบ, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['สารอาหารในนมแม่'] =  array('title' => 'สารอาหารในนมแม่มีอะไรบ้าง ประโยชน์ของน้ำนมแม่ดีที่สุดในช่วงไหน', 'des' => 'นมแม่มีประโยชน์อย่างไร พบกับสารอาหารในนมแม่ สารอาหารต่างๆ ที่พบได้ในนมแม่ตามธรรมชาติ ที่ช่วยพัฒนาสมอง และดีต่อสุขภาพลูก มาหาคำตอบกันว่าในนมแม่มีสารอาหารอะไรบ้างที่ดีต่อเด็กและทารกที่ดื่มนมแม่', 'keywords' => 'สารอาหารในนมแม่, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['พัฒนาการเด็ก2ขวบดื้อ'] =  array('title' => 'พัฒนาการเด็ก 2 ขวบ เข้าใจอารมณ์ลูกเพื่อแก้ปัญหาได้ถูกจุด', 'des' => 'ลูกเริ่มดื้อ เอาแต่ใจตัวเอง เด็ก 2 ขวบอารณ์ขึ้นลงอย่างไร และพัฒนาการเด็กวัย 2 ขวบนี้เปลี่ยนไปอย่างไรบ้างทางด้านอารมณ์ เด็ก 2 ปีมีพฤติกรรมเปลี่ยนแปลงอย่างไรบ้างที่พ่อแม่มือใหม่ต้องรับมือ', 'keywords' => 'พัฒนาการเด็ก2ขวบดื้อ, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['ปัญหาการเลี้ยงลูกด้วยนมแม่'] =  array('title' => 'แก้ปัญหาการเลี้ยงลูกด้วยนมแม่ และการเลี้ยงลูกด้วยนมแม่', 'des' => 'เลี้ยงลูกด้วยนมแม่ดีอย่างไร ทำไมทุกคนถึงแนะนำว่าการเลี้ยงลูกด้วยนมแม่ดีที่สุด และปัญหาลูกไม่สามาถกินนมแม่ ปัญหาของการเลี้ยงลูกด้วยนมแม่คืออะไร อาจเกิดกับคุณได้หรือไม่ หาคำตอบกันค่ะ', 'keywords' => 'ปัญหาการเลี้ยงลูกด้วยนมแม่, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['อาหารบำรุงสมองเด็ก'] =  array('title' => 'อาหารบำรุงสมองเด็กที่ดีที่สุดมีอะไรบ้าง', 'des' => 'พบกับสุดยอดเมนูอาหารเช้า เพื่อเสริมสร้างบำรุงสมองเด็ก 1-3 ขวบ เพิ่ม IQ กันเถอะ ทำไมลูกควรทานอาหารเช้า และมื้อเช้าควรให้ลูกทานอะไรบ้าง เพื่อส่งเสริมพัฒนาการสมองลูก เลี้ยงลูกให้ฉลาดด้วยการกินอาหารบำรุงสมองเด็กอย่างถูกต้อง', 'keywords' => 'อาหารบำรุงสมองเด็ก, อาหารบำรุงสมอง, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['อาหารเสริมบำรุงสมองเด็ก'] =  array('title' => 'อาหารเสริมบำรุงสมองเด็ก เพื่อพัฒนาการสมองลูกน้อย', 'des' => 'ลูกควรทานอาหารเสริมหรือไม่ เพื่อบำรุงสมองเด็กให้มีพัฒนาการที่ดีขึ้น และถ้าต้องทานอาหารเสริม มีทางเลือกอะไรบ้างสำหรับเด็ก เลี้ยงลูกให้ฉลาด ควรทานอาหารเสริมหรือไม่', 'keywords' => 'อาหารเสริมบำรุงสมองเด็ก, อาหารบำรุงสมอง, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['ลูกไม่กินนมแม่'] =  array('title' => 'ลูกไม่ยอมกินนมแม่ แม่ต้องทำอย่างไร', 'des' => 'อะไรเป็นสาเหตุให้ลูกไม่กินนมแม่ และแม่มือใหม่จะต้องรับมืออย่างไรเมื่อพบปัญหาลูกไม่ยอมกินนมแม่ จากที่เคยกินนมแม่ปกติ แต่อยู่ๆ ก็กลับปฎิเสธนมแม่ไปดื้อ และมีทางแก้ไขอะไรบ้างที่จะทำให้ลูกน้อยกลับมายอมกินนมแม่เหมือนเดิม', 'keywords' => 'ลูกไม่ยอมกินนมแม่เลย, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');
		$arr_fix['เลี้ยงลูกอย่างไรให้ฉลาด'] =  array('title' => 'เคล็ดลับเลี้ยงลูก เลี้ยงลูกอย่างไรให้ฉลาด อย่างมีความสุข', 'des' => 'เลี้ยงลูกอย่างไรให้ฉลาดสุดๆ เคล็ดลับการเลี้ยงลูก พัฒนาสมองลูกน้อยให้ไม่ตามหลัง เสริมสร้างสมองให้ลูกหัวไว ฉลาดหลักแหลม แม่มือใหม่จะต้องทำอย่างไร ทั้งทางด้านอาหารเสริม และการเลี้ยงดู', 'keywords' => 'เลี้ยงลูกให้ฉลาด, อาหารบำรุงสมอง, s-mom, sphingomyelin, สฟิงโกไมอีลิน, นมผง s26, wyeth nutrition');

		$arr_fix['พัฒนาการเด็ก3เดือน'] =  array('title' => 'พัฒนาการเด็ก 3 เดือน เคล็ดลับส่งเสริมพัฒนาการลูกน้อย', 'des' => 'พ่อแม่จะต้องทำอย่างไรเพือส่งเสริมพัฒนาการที่ดีให้กับเด็กวัย 3 เดือนนี้ เดือนนี้เป็นช่วงที่ลูกจะเปลี่ยนแปลงอย่างเห็นได้ชัด พ่อแม่มือใหม่คงจะเริ่มเข้าใจอะไรหลายอย่างมากขึ้น', 'keywords' => 'พัฒนาการเด็ก 3 เดือน, ความเปลี่ยนแปลงของเด็ก 3 เดือน, s-mom, sphingomyelin, สฟิงโกไมอีลิน, s26');
		$arr_fix['วิธีเก็บรักษานมแม่'] =  array('title' => 'เคล็ดลับวิธีเก็บรักษานมแม่ อย่างถูกวิธี', 'des' => 'เคล็ดลับการเก็บรักษานมแม่ ปั้มนมเก็บไว้สำหรับลูกน้อย ข้อควรปฎิบัติ การเก็บรักษานมแม่ และวิธีการนำนมแม่มาใช้ใหม่อย่างถูกวิธี ให้ถูกอุณหภูมิ เพื่อสุขภาพของลูก', 'keywords' => 'วิธีเก็บรักษานมแม่, การเก็บนมแม่, วิธีเก็บนมแม่, s-mom, sphingomyelin, สฟิงโกไมอีลิน, s26');
		$arr_fix['ประโยชน์ของน้ำนมแม่'] =  array('title' => 'ประโยชน์ของน้ำนมแม่ และการเปลี่ยนแปลงของสารอาหารในน้ำนม', 'des' => 'ใครๆ ก็รู้ว่านมแม่มีประโยชน์อย่างไรบ้าง แต่หลายคนอาจยังไม่รู้ว่า ช่วงเดือนไหนที่นมแม่มีประโยชน์มากที่สุด และอาหารการกินต้องดีแค่ไหน ถึงจะช่วยทำให้นมแม่มีประโยชน์สูงสุดกับลูกน้อย เพราะน้ำนมแม่มีประโยชน์มากกว่าที่คุณคิด', 'keywords' => 'ประโยชน์น้ำนมแม่, s-mom, sphingomyelin, สฟิงโกไมอีลิน, s26');
		$arr_fix['เมนูอาหารเด็ก1ขวบ'] =  array('title' => 'เมนูอาหารเด็ก 1 ขวบ เมนูและสารอาหารต้องมีอะไรบ้าง', 'des' => 'เด็ก 1 ขวบควรทานเมนูอาหารแบบไหนดีล่ะ ที่จะช่วยเสริมพัฒนาการทางด้านร่างกายและสมอง และเป็นเมนูอาหารที่กินง่าย ช่วยแก้ปัญหาลูกกินยาก สารอาหารสำหรับเด็ก 1 ขวบ – 3 ขวบ หาคำตอบที่นี่กันค่ะ', 'keywords' => 'เมนูอาหารเด็ก 1 ขวบ, เมนูอาหารเด็ก, s-mom, sphingomyelin, สฟิงโกไมอีลิน, s26');


		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		$know_type_db = "";
		if($know_type == "pickyeater"){
			$know_type_db = "b";
			$data_related = 3;
		}else if($know_type == "pregnancy"){
			$know_type_db = "a";
			$data_related = 1;
		}else if($know_type == "braindevelopment"){
			$know_type_db = "c";
			$data_related = "";
		}else if($know_type == "toddler"){
			$know_type_db = "d";
			$data_related = "";
		}else if($know_type == "lactating"){
			$know_type_db = "e";
			$data_related = "";
		}else if($know_type == "ลูกน้อยถ่ายสบาย"){
			$know_type_db = "f";
			$data_related = "";
		}

		if($know_type_db == "c" || $know_type_db == "e" || $know_type_db == "f"){
			$know_sub_type_db = "";
		}else{
			$know_sub_type_db = $know_type_db.$know_sub_type;
		}

		$cache_name = "knowledge_list_".$slug."_".App::getLocale()."_".$know_type."_".$member_web_id;
    	error_log( $cache_name );
		$knowledge_view = Cache::get($cache_name);
		if ($knowledge_view == "") {
			$table = new Knowledge();
			$dt = $table->load_detail($know_type_db,$know_sub_type_db,$slug);
			if(count($dt) <= 0){
				abort("404");
				exit();
			}

			Meta::meta('title', $dt[0]->title);
			Meta::meta('description', $dt[0]->detail);
			Meta::meta('image', Core\OMImage::readFileName($dt[0]->image_gen,$dt[0]->image, "c230x205", "knowledge_list"));

			if(!empty($arr_fix[$slug])){
				$meta_fix = $arr_fix[$slug];
				
				Meta::meta('title', $meta_fix['title']);
				Meta::meta('description', $meta_fix['des']);
				Meta::meta('keywords', $meta_fix['keywords']);
			}

			$dt[0]->category = $know_type;
			$dt[0]->sub_category = $know_sub_type;
			$dt[0]->relate = $this->dataTable->getRelatedData($data_related);
			$dt[0]->relateQuestion = $this->dataTable->getRelatedQuestionData($data_related);

			$bookmarkDataArr = $this->dataMemberWeb->getBookmarkByMember($member_web_id);
			$dataBookmark = [];
	        foreach ($bookmarkDataArr as $key => $dataArr) {
	        	$dataBookmark[] = $dataArr->trick_mom_id;
	        }
			$dt[0]->bookmarkList = $dataBookmark;
			$dt[0]->member_web_id = $member_web_id;
			$dt[0]->title_related = $data_related;

			$return = (array)$dt[0];
			$return["datestring"] = $this->createDateString($return["valid_date"]);

			$knowledge_view = (string)view('knowledge.listdetail',["data" => $return]);
			Cache::put($cache_name,$knowledge_view,10);
		}

		ViewSession::viewSessionGlobal();
		$knowledge_view = ViewSession::make($knowledge_view);

		return $knowledge_view;
	}

	public function listDetailOld($slug){
			$table = new Knowledge();
			$dt = $table->load_detail_old($slug);

			if(count($dt) <= 0){
				abort("404");
				exit();
			}
			$cat = $dt[0]->knowledge_category;
			$sub_cat = $dt[0]->knowledge_subcategory;

			if($cat == "b"){
				$cat = "pickyeater";
			}else if($cat == "a"){
				$cat = "pregnancy";
			}else if($cat == "d"){
				$cat = "toddler";
			}else if($cat == "e"){
				$cat = "lactating";
			}else if($cat == "c"){
				$cat = "braindevelopment";
			}else if($cat == "f"){
				$cat = "ลูกน้อยถ่ายสบาย";
			}
			$sub_cat = substr($sub_cat,1,1);

		return $this->listDetail($cat,$sub_cat,$slug);
	}

	public function listDetailNoSub($know_type_no_sub,$slug){
		$cat = $know_type_no_sub;
		$sub_cat = "";
		return $this->listDetail($cat,$sub_cat,$slug);
	}

}
