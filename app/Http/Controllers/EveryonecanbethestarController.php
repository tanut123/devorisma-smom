<?php namespace App\Http\Controllers;

use App;
use Meta;
use App\Core;
use Cache;
use App\Core\ViewSession;

class EveryonecanbethestarController extends App\Core\Controller {

	public function __construct()
    {
    	parent::__construct();
    	Meta::meta('title', 'everyonecanbethestar');
	}

	public function index(){
		$cache_name = "app_controller_cache_".App::getLocale();
		$app_view = Cache::get($cache_name);
		if ($app_view == "") {
			$app_view = (string)view('everyonecanbethestar.index');
			Cache::put($cache_name,$app_view,10);
		}
		ViewSession::viewSessionGlobal();
		$app_view = ViewSession::make($app_view);
        return $app_view;
	}
}
