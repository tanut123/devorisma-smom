<?php namespace App\Http\Controllers;

use App;
use Meta;
use App\CustomPage;
use App\Core;
use Cache;
use App\Core\ViewSession;


class CustomPageController extends App\Core\Controller {

	public function __construct(){
    	parent::__construct();
	}

	public function pageData($slug){

		$cache_name = "custom_page_".$slug."_".App::getLocale();
		$custom_page_view = Cache::get($cache_name);
		if ($custom_page_view == "") {
			$dt = array();
			$table = new CustomPage();
			$dt = $table->getCustomPage($slug);
			if(count($dt) <= 0){
				return abort(404);
			}
			Meta::meta('title', $dt[0]->title);
			$dt['custom_page'] = $dt;

			$custom_page_view = (string)view('custom_page.index', $dt);
			Cache::put($cache_name,$custom_page_view,10);


        //return view('custom_page.index', $dt);
		}
		ViewSession::viewSessionGlobal();
		$custom_page_view = ViewSession::make($custom_page_view);
		return $custom_page_view;
	}
}
