<?php namespace App\Http\Controllers;

use App;
use Meta;
use App\Video;
use Cache;
use App\Core\ViewSession;

class VideoController extends App\Core\Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	// public function __construct()
	// {
	// 	$this->middleware('auth');
	// }

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index($lang = null)
	{

		$data = array();

		$cache_name = 'VideoCache_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){

			$video = new Video();

			$dt_video_hl = $video->getHlVideo();

	        $dt_video_list = $video->getVideo();


			if(!empty($dt_video_list)){
				foreach ($dt_video_list as $key => $value) {
					$dt_video_list[$key]->bg_image = \App\Core\OMImage::readFileName($value->image_gen,$value->image,'c395x221','vdo_channel');
	                if (strlen($value->file_mp4) > 0 && strlen($value->file_mp4_gen) > 0) {
	                    $dt_video_list[$key]->type = "mp4";
	                    $dt_video_list[$key]->video_data = \App\Core\OMImage::readFileName($value->file_mp4_gen,$value->file_mp4,'o0x0','vdo_channel');
	                } else {
	                    $dt_video_list[$key]->type = "youtube";
	                    $dt_video_list[$key]->video_data = $value->youtube;
	                }
				}
			}

			if(!empty($dt_video_hl)){
	            $video_hl = (array)array_shift($dt_video_hl);

	            $dt_video_hl_title = $video_hl['title_'.App\Core\Lng::url()];
	            if (strlen($video_hl["file_mp4"]) > 0 && strlen($video_hl["file_mp4_gen"]) > 0) {
	                $hl_type = "mp4";
	                $hl_mp4_file = \App\Core\OMImage::readFileName($video_hl["file_mp4_gen"],$video_hl["file_mp4"],'o0x0','vdo_channel');
	            } else {
	                $hl_type = "youtube";
	                $hl_youtube_id = $video_hl['youtube'];
	            }
			}
			$dt_video_list = $dt_video_list;

			$view_cache .= '<div id="hl_header">';
				$view_cache .= '<div id="hl_header_in">';
					$view_cache .= '<div id="header_text">';
						$view_cache .= '<div class="row1">';
							$view_cache .= 'VDO <span class="thin">CHANNEL</span>';
						$view_cache .= '</div>';
						$view_cache .= '<div class="row2">';
							$view_cache .= 'คลิปวีดิโอหลากหลายเรื่องราวที่น่ารู้<br/>';
							$view_cache .= 'ช่วยพัฒนาศักยภาพลูกน้อย';
						$view_cache .= '</div>';
						$view_cache .= '<div class="row3">';

						if(!empty($hl_type)){
						    $view_cache .= $dt_video_hl_title;
					    }

						$view_cache .= '</div>';
					$view_cache .= '</div>';
					if(!empty($hl_type)){
					    if ($hl_type == "youtube"){
		                $view_cache .= '<div id="header_video">';
		                    $view_cache .= '<div id="display_video_hl">';
		                        $view_cache .= '<div id="display_video_hl_in">';
		                            $view_cache .= '<iframe id="youtube_iframe" video_type="youtube" src="https://www.youtube.com/embed/' .$hl_youtube_id .'?enablejsapi=1&autoplay=1" frameborder="0" allowfullscreen></iframe>';
		                        $view_cache .= '</div>';
		                    $view_cache .= '</div>';
		                $view_cache .= '</div>';
		                }else{
		                $view_cache .= '<div id="header_video">';
		                    $view_cache .= '<div id="display_video_hl">';
		                        $view_cache .= '<div id="display_video_hl_in">';
		                             $view_cache .= '<video id="youtube_iframe" video_type="mp4" controls autoplay>';
		                              $view_cache .= '<source src="' . $hl_mp4_file . '" type="video/mp4">';
		                            $view_cache .= '</video>';
		                        $view_cache .= '</div>';
		                    $view_cache .= '</div>';
		                $view_cache .= '</div>';
		                }
					}
					$view_cache .= '<div class="clearfix"></div>';
				$view_cache .= '</div>';
			$view_cache .= '</div>';
			if(!empty($dt_video_list)){
			$view_cache .= '<div id="display_slide_video">';
				$view_cache .= '<div id="display_slide_video_in">';
					$view_cache .= '<div id="video_list">';
						foreach($dt_video_list as $value){
						$view_cache .= '<div class="video_list">';
							$view_cache .= '<div class="video_thumbnail" style="background-image: url( ' .$value->bg_image . '); background-repeat:no-repeat; background-position: center center; background-size:100% auto;">';
								$view_cache .= '<a class="video_btn_play" target="_blank" video_data="' . $value->video_data . '" type="' . $value->type . '"></a>';
							$view_cache .= '</div>';
							$view_cache .= '<div class="video_title">';
								$view_cache .= $value->title_th;
							$view_cache .= '</div>';
						$view_cache .= '</div>';
					  	}
					$view_cache .= '</div>';
				$view_cache .= '</div>';
			$view_cache .= '</div>';
			}
			$data["video"] = $view_cache;
			$view_cache = (string)view('media.video.index',$data);
			Cache::put($cache_name, $view_cache, 10);
		}
		ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

 		return $view_cache;
	}
}
