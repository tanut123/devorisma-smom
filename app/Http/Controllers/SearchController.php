<?php namespace App\Http\Controllers;

use App;
use App\Search;
use Meta;
use View;
use App\Core\SettingManagement;
use Input;
use Response;
use Cache;
use App\Core\ViewSession;

class SearchController extends App\Core\Controller {

	protected $search;

	public function __construct()
    {
    	$this->search = new Search();
    	parent::__construct();

	}

    public function searchViews()
    {
	    $inputData = Input::all();
		if (empty($inputData["csrf_token"]) || $inputData["csrf_token"] != csrf_token()) {
			App::abort(404);
		}
		$searchWord = "";
    	if(isset($inputData["q"])){
    		$searchWord = is_array($inputData["q"])?implode(",", $inputData["q"]):$inputData["q"];
    	}
    	$cache_name = "searchCache_". $searchWord ."_". App::getLocale();
        $home_view = Cache::get($cache_name);
        if ($home_view == "") {
	    	$setting = new SettingManagement();
	    	foreach ($inputData as $key => $value) {
	    		if( !is_string($value)) return response(null,400);
	    	}
	        Meta::meta('title', trans('core.CORE_SEARCH'));
	        Meta::meta('description', trans('core.CORE_SEARCH'));
	        View::share('SEARCH_LIMIT', $setting->get("SEARCH_LIMIT"));
	        View::share('search_word', $searchWord);
        	$home_view = (string)view('search.index');
	    }
		ViewSession::viewSessionGlobal();
        $home_view = ViewSession::make($home_view);
	    return $home_view;
    }

    public function listData()
    {
    	$data = Input::all();

        $returnData = $this->search->listData($data);

        return Response::json($returnData);
    }




}