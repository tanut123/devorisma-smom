<?php namespace App\Http\Controllers;

use App;
use Input;
use Response;
use Meta;
use App\Core;
use Cache;
use App\Core\ViewSession;

class JoinsmonClubController extends App\Core\Controller {

	public function __construct()
    {
    	parent::__construct();
		Meta::meta('title');
        Meta::meta('image', asset(env('BASE_CDN', '') . '/images/smom_logo_new.png'));
    }
    
	public function index()
	{
		$cache_name = "JoinsmonCache_".App::getLocale();
		$view_cache = Cache::get($cache_name);
		if ($view_cache == "") {
			$view_cache = (string)view('joinsmom.index');
			Cache::put($cache_name,$view_cache,10);
		}
		ViewSession::viewSessionGlobal();
		$view_cache = ViewSession::make($view_cache);
        return $view_cache;
	}
}
