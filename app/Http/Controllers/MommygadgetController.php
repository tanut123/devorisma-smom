<?php namespace App\Http\Controllers;

use App;
use App\Mommygadget;
use App\MemberWeb;
use Illuminate\Support\Facades\Cache;
use Input;
use Response;
use Meta;
use App\Core;
use App\Core\ViewSession;
use App\Core\Lng;

class MommygadgetController extends App\Core\Controller {
	protected $dataTable;
	public function __construct(){
    	parent::__construct();
    	$this->dataTable = new Mommygadget();
    	$this->dataMemberWeb = new MemberWeb();
	}

	public function rating(){
		$data = Input::all();
		$mode = $data['mode'];
		$page_uid = $data['page_uid'];
		$rate = $data['rate'];
		if($mode == "add"){
			$dsMemberInsert = $this->dataTable->insertRating($data);
		}
	}

	public function index(){
		Meta::meta('title', 'เทรนด์คุณแม่' );
		$cache_name = "mommygadget_index_".App::getLocale();
		$mommygadget_index_view = Cache::get($cache_name);
		if ($mommygadget_index_view == "") {
			$mommygadget_index_view = (string)view('mommygadget.index');
			Cache::put($cache_name,$mommygadget_index_view,10);
		}
		ViewSession::viewSessionGlobal();
		$mommygadget_index_view = ViewSession::make($mommygadget_index_view);
		return $mommygadget_index_view;
	}

	public function loadAjax(){
		$data = Input::all();
		$pagelist = $data["page"];
		$limit_start = $data["limit"];
		$start = ($pagelist - 1) * $limit_start;
		$limit = $data["limit"]+1;

		$table = new Mommygadget();
		$dt = $table->loadlist($start,$limit);

		$count = 1;
		$count_data = 1;
		$has_loadmore = false;
		$ajax_html = "";

		foreach ($dt as $key => $value) {
			if($count_data <= $limit_start){
				$d_day = date('d', strtotime($value->valid_date));
				$d_month = date('m', strtotime($value->valid_date));
                if (strlen($value->link_url) > 0) {
                    $link = $value->link_url;
                } else {
                	 $link = url()."/".\App\Core\Lng::url()."/mommygadget/".$value->slug;
                }
				$ajax_html.="
				            <a  href='".$link."'>
                                <li class='list-item ".($count == 2 ? "list-item-center":"")."'>
									<div class='list-img'>
										<img src='".\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c307x252','mommygadget')."' width='307' height='252'>
									</div>
									<div class='badge-list'>
										<span class='txt-day'>".$d_day."</span>
										<span class='txt-month'>".$this->convetMonth($d_month)."</span>
									</div>
									<div class='view-content'>
										<span class='view-icon'></span>
										<span class='view-text FXMed'>" . ($value->total == ""? 0:$value->total ) . " Views</span>
									</div>
									<div class='txt-list-panel'>
										<div class='list-title'>".$value->title."</div>
										<div class='short-desc'>".\App\Core\OM::trimWithDot($value->description,100)."</div>
									</div>
								</li>
                            </a>";
				$count++;
				if($count == 3){
					$count =0;
				}
				$count_data++;
			}else{
				$has_loadmore = true;
			}
		}
		return Response:: json(["ajax_html" => $ajax_html,"has_loadmore" => $has_loadmore]);
	}

	public function convetMonth($input){
		$d_th_month = array(
		        	"0"=>"",
				    "1"=>"ม.ค.",
				    "2"=>"ก.พ.",
				    "3"=>"มี.ค.",
				    "4"=>"เม.ย.",
				    "5"=>"พ.ค.",
				    "6"=>"มิ.ย.",
				    "7"=>"ก.ค.",
				    "8"=>"ส.ค.",
				    "9"=>"ก.ย.",
				    "10"=>"ต.ค.",
				    "11"=>"พ.ย.",
				    "12"=>"ธ.ค."
		           );
		$d_en_month = array(
		        	"0"=>"",
				    "1"=>"JAN",
				    "2"=>"FEB",
				    "3"=>"MAR",
				    "4"=>"APR",
				    "5"=>"MAY",
				    "6"=>"JUN",
				    "7"=>"JUL",
				    "8"=>"AUG",
				    "9"=>"SEP",
				    "10"=>"OCT",
				    "11"=>"NOV",
				    "12"=>"DEC"
		            );
		if(App::getLocale() == "en"){
			return $d_en_month[(int)$input];
		}else{
			return $d_th_month[(int)$input];
		}
	}

	public function createDateString($input){
		list($date,$time) = explode(" ",$input);
		list($year,$month,$days) = explode("-",$date);

		$return_val = $days." ".$this->convetMonth($month)." ".(\App\Core\Lng::url()=="th" ? $year+543 : $year);
		return $return_val;
	}

	public function Detail($slug){
		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");

		$cache_name = "mommygadget_".$slug."_".App::getLocale()."_".$member_web_id;
		$mommygadget_view = Cache::get($cache_name);
		if($mommygadget_view == "") {
			$table = new Mommygadget();
			$dt = $table->load_detail($slug);
			if(count($dt) <= 0){
				abort("404");
				exit();
			}

			Meta::meta('title', $dt[0]->title);
			Meta::meta('description', $dt[0]->detail);
			Meta::meta('image', Core\OMImage::readFileName($dt[0]->image_gen,$dt[0]->image, "c230x205", "mommygadget"));

			$dt[0]->relate = $this->dataTable->getRelatedData($dt[0]->mommygadget_id);
			$dt[0]->member_web_id = $member_web_id;

			$return = (array)$dt[0];
			$return["datestring"] = $this->createDateString($return["valid_date"]);

			$mommygadget_view = (string)view('mommygadget.detail',["data" => $return]);
			Cache::put($cache_name,$mommygadget_view,10);
		}

		ViewSession::viewSessionGlobal();
		$mommygadget_view = ViewSession::make($mommygadget_view);
		return $mommygadget_view;
	}

}
