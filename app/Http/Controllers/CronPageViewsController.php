<?php namespace App\Http\Controllers;

use App;
use App\Core;
use App\Pageviews;
use Illuminate\Http\Request;

class CronPageViewsController extends App\Core\Controller {

	public $page_views;

	function __construct(){
		$this->page_views = new Pageviews();
	}

	public function allViews(){
		echo $this->page_views->allViews();
		exit();
	}

	public function momtipViews(){
		echo $this->page_views->momtipViews();
		exit();
	}

	public function promotionViews(){
		echo $this->page_views->promotionViews();
		exit();
	}

	public function knowledgeViews(){
		echo $this->page_views->knowledgeViews();
		exit();
	}
	public function view(Request $request,$module,$id){
		if($module == "static"){
			$path_uri = explode("/",$request->header('Referer'),4);
			// var_dump($path_uri[3]);
			$module = str_replace("/", "_", $path_uri[3]);
		}
		echo $this->page_views->view($module,$id);
		exit();
	}
	public function viewMagazine($id){
		$count = $this->page_views->view("magazine",$id);
		$json["m_id"] = $id;
		$json["count"] = $count;
		echo json_encode($json);
		exit();
	}


}
