<?php namespace App\Http\Controllers;

use App;
use App\Member;
use Meta;
use View;
use App\Core\SettingManagement;
use Input;
use Cache;
use App\Core\ViewSession;

class MemberController extends App\Core\Controller {

	public function __construct()
    {
    	parent::__construct();

	}

    public function register()
    {
      return redirect("/th/register");
  //   	$cache_name = 'MemberCache_' . App::getLocale();
		// $view_cache = Cache::get($cache_name);
		// if($view_cache == "" || $view_cache == null){
	 //    	$setting = new SettingManagement();
	 //        Meta::meta('title', 'สมัครสมาชิก');
	 //        Meta::meta('description', 'สมัครสมาชิก');
		// 	$view_cache = (string)view('member.index',array("is_member_page" => true));
	 //        Cache::put($cache_name, $view_cache, 10);
	 //    }
		// ViewSession::viewSessionGlobal();
	 //    $view_cache = ViewSession::make($view_cache);

  //       return $view_cache;
    }

    public function cmd()
    {
    	$inputData = Input::all();
    	$invalidData = false;
    	if($inputData["cmd"] == "register"){
	    	if($inputData["type_register"] == ""){
	    		$invalidData = true;
	    	}
	    	if($inputData["firstname"] == ""){
	    		$invalidData = true;
	    	}
	    	if($inputData["lastname"] == ""){
	    		$invalidData = true;
	    	}
	    	if($inputData["mobile"] == "" && $inputData["email"] == ""){
	    		$invalidData = true;
	    	}
	    	if($inputData["status"] == ""){
	    		$invalidData = true;
	    	}
	    	if($inputData["status"] == 1 && $inputData["child_date"] == "" && $inputData["antenatal_place"] == ""){
	    		$invalidData = true;
	    	}
	    	if(!$invalidData){
	    		$memberDb = new Member();
	    		$dsMember = $memberDb->getMemberExist($inputData["email"],$inputData["mobile"]);
	    		if(count($dsMember) > 0){
	    			$data["c"] = "re";
	    			$data["s"] = "3";
	    		}else{
	    			$dsMemberInsert = $memberDb->insertMember($inputData);
	    			if($dsMemberInsert){
	    				$data["c"] = "y";
	    				$data["s"] = "1";
	    			}else{
	    				$data["c"] = "err";
	    				$data["s"] = "4";
	    			}
	    		}
	    	}else{
	    		$data["c"] = "err";
    			$data["s"] = "5";
	    	}
    	}else{
    		$data["c"] = "err";
    		$data["s"] = "2";
    	}

        echo json_encode($data);
        exit();
    }
}