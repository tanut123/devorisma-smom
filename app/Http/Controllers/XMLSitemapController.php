<?php namespace App\Http\Controllers;

use App;
use App\Sitemap;

class XMLSitemapController extends App\Core\Controller {
	private $sitemap;
	function __construct(){
		$this->sitemap = new Sitemap();
	}

	public function manage($type = "all") {
		return $this->sitemap->genSitemap($type);
	}

	public function remove($type = "all") {
		return $this->sitemap->removeSitemap($type);
	}
}