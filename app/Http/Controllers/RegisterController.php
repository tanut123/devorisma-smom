<?php namespace App\Http\Controllers;

use App;

use Meta;
use App\Core;
use Cache;
use App\Core\ViewSession;
use App\Member;
use App\MemberWeb;
use Input;
use PHPMailer;
use Log;
use DateTime;
use App\Core\OMDateTimeConverter;
use App\Core\OMMail;

class RegisterController extends App\Core\Controller {
	protected $dataTable;
	public function __construct()
    {
    	parent::__construct();
    	Meta::meta('title', 'สมัครสมาชิก');
    	$this->dataTable = new Memberweb();
    	$this->memberCard = new Member();
	}

	public function index(){
		$cache_name = "RegisterCache_".App::getLocale();
		$view_cache = Cache::get($cache_name);
		if ($view_cache == "") {
			$view_cache = (string)view('register.index');
			Cache::put($cache_name,$view_cache,10);
		}
		ViewSession::viewSessionGlobal();
		$view_cache = ViewSession::make($view_cache);
        return $view_cache;
	}

	public function form($type){
		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		$cache_name = "RegisterFormCache_". $type . "_" . $member_web_id . "_" .App::getLocale();
		$view_cache = Cache::get($cache_name);
		$dtMember = [];

		if ($view_cache == "") {
			if($type == "profile"){
				if($member_web_id == ""){
					return redirect(App::getLocale() . '/home');
				}
				Meta::meta('title', 'ข้อมูลส่วนตัว');
				$dtMember = $this->dataTable->getMemberByID($member_web_id);
				$dtMemberChild = $this->dataTable->getMemberDataChildByID($member_web_id);
				$dtCheckMemberCard = $this->memberCard->getMemberExist($dtMember[0]->email,$dtMember[0]->email);
				$dtMember = (array)$dtMember[0];
				$dtMember["child"] = $dtMemberChild;
				$dtMember["member_card"] = false;
				if($dtCheckMemberCard){
					$dtMember["member_card"] = true;
				}
			}else{
				if($member_web_id != ""){
					return redirect(App::getLocale() . '/register/form/profile');
				}
			}
			$view_cache = (string)view('register.form'. '_' . $type, $dtMember);
			Cache::put($cache_name,$view_cache,10);
		}
		ViewSession::viewSessionGlobal();
		$view_cache = ViewSession::make($view_cache);
        return $view_cache;
	}

	public function register(){
		$data = Input::all();
		$mode = (isset($data["mode"]) ? $data["mode"] : "");
		$dtMemberInsert = false;
		$dtMemberCheckEmail = [];

		if($mode == "add"){
			if($data["member_type"] == 1){
				if (!isset($data["captcha_code"]) || $data["captcha_code"] != \Illuminate\Support\Facades\Session::get("captcha_code")) {
					$dt_return["cmd"] = "ERR_CAPTCHA";
					$dt_return["message"] = trans("register.CAPTCHA_MISMATCH");
					return json_encode($dt_return);
				}
	            //Check Password Case Sensitive
	            if (isset($data["password"]) && $data["password"] != "") {
	                $pass_check = true;
	                if (strlen($data["password"]) < 10) {
	                    $pass_check = false;
	                }
	                if (!preg_match("/[A-Z]/",$data["password"])) {
	                    $pass_check = false;
	                }
	                if (!preg_match("/[a-z]/",$data["password"])) {
	                    $pass_check = false;
	                }
	                if (!preg_match("/[0-9]/",$data["password"])) {
	                    $pass_check = false;
	                }
	                if (!preg_match("/[ !\"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~]/",$data["password"])) {
	                    $pass_check = false;
	                }
	                if ($pass_check == false) {
	                    $dt_return["cmd"] = "ERR_02";
	                    $dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
	                    return json_encode($dt_return);
	                }
	            } else {
	                $dt_return["cmd"] = "ERR_02";
	                $dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
	                return json_encode($dt_return);
	            }
	            //
			}else{
				if(isset($data["displayname"]) && $data["displayname"] != ""){
	        		$name = explode(" ",$data["displayname"]);
	        		$data['firstname'] = $name[0];
	        		$data['lastname'] = end($name);
	        	}
			}
			if(isset($data["email"]) && $data["email"] != ""){
				$dtMemberCheckEmail = $this->dataTable->getMemberByEmail($data["email"]);
			}

			if(count($dtMemberCheckEmail) > 0){
				if($this->dataTable->error_message != ""){
					$dt_return["cmd"] = "ERR_02";
					$dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
					$dt_return["exception"] = $this->dataTable->error_message;
				}else{
					$dt_return["cmd"] = "ERR_03";
					// if($data["member_type"] == 2){
					// 	$dt_return["message"] = trans("register.REGISTER_FACEBOOK_DUPLICATE_MAIL");
					// }else{
						$send_mail_dup = $this->registerDuplicateEmail($data["email"]);
						if($send_mail_dup){
							$dt_return["message"] = trans("register.REGISTER_DUPLICATE_MAIL");
						}else{
							$dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
						}
					// }
				}
			}else{
				// All pass insert
				$dtMemberInsert = $this->dataTable->insertMember($member_web_id, $data);
				$data["member_web_id"] = $member_web_id;
				if($dtMemberInsert){
					$dataMemberCard = $this->insertMemberCard($data);
						$dt_return["cmd"] = "y";
					if($dataMemberCard){
						$dt_return["message"] = "Insert complete.";
					}else{
						$dt_return["message"] = "Insert complete, cannot insert member card";
					}
					if($data["member_type"] == 1){
						$last_member_dt = $this->dataTable->getLastMember();
						$last_member_array = (array)$last_member_dt[0];
						$last_member_id = $last_member_array["member_web_id"];
						$this->genVerify($last_member_id);
					}
				}else{
					$dt_return["cmd"] = "ERR_04";
					$dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
				}
			}
		}else if($mode == "edit"){
			if(isset($data["member_web_id"]) && $data["member_web_id"] != ""){
				if(isset($data["email"]) && $data["email"] != ""){
					$dtMemberCheckEmail = $this->dataTable->getMemberByEmail($data["email"]);
				}

				if(!empty($dtMemberCheckEmail) && $this->dataTable->error_message != ""){
					$dt_return["cmd"] = "ERR_05";
					$dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
					$dt_return["exception"] = $this->dataTable->error_message;
				}else{
					if(isset($dtMemberCheckEmail[0])){
						$dtMemberCheckEmail = (array)$dtMemberCheckEmail[0];
					}
					if(!empty($dtMemberCheckEmail) && ($data["email"] == $dtMemberCheckEmail["email"] && $data["member_web_id"] != $dtMemberCheckEmail["member_web_id"])){
						$dt_return["cmd"] = "ERR_06";
						// if($data["member_type"] == 2){
						// 	$dt_return["message"] = trans("register.REGISTER_FACEBOOK_DUPLICATE_MAIL");
						// }else{
							$send_mail_dup = $this->registerDuplicateEmail($data["email"]);
							if($send_mail_dup){
								$dt_return["message"] = trans("register.REGISTER_DUPLICATE_MAIL");
							}else{
								$dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
							}
						// }
					}else{
						$passwordValidate = true;
						if(isset($data["password"]) && $data["password"] != ""){
                            $pass_check = true;
							if(isset($data["old_password"]) && $data["old_password"] != ""){
								if(!$this->checkOldPassword($data["old_password"])){
									$pass_check = false;
									$dt_return["cmd"] = "ERR_09";
	                                $dt_return["message"] = trans("register.REGISTER_ERROR_INVALID_OLD_MESSAGE");
	                                return json_encode($dt_return);
								}
							}else{
								$pass_check = false;
							}
                            if (strlen($data["password"]) < 10) {
                                $pass_check = false;
                            }
                            if (!preg_match("/[A-Z]/",$data["password"])) {
                                $pass_check = false;
                            }
                            if (!preg_match("/[a-z]/",$data["password"])) {
                                $pass_check = false;
                            }
                            if (!preg_match("/[0-9]/",$data["password"])) {
                                $pass_check = false;
                            }
                            if (!preg_match("/[ !\"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~]/",$data["password"])) {
                                $pass_check = false;
                            }
                            if ($pass_check == false) {
                                $dt_return["cmd"] = "ERR_02";
                                $dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
                                return json_encode($dt_return);
                            }
							$passwordValidate = $this->checkPassword($data["password"]);
						}

						if($passwordValidate){
							// All pass update
							$dtMemberUpdate = $this->dataTable->updateMember($data);
							if($dtMemberUpdate){
								$dataMemberCard = $this->insertMemberCard($data);
								Cache::forget("RegisterFormCache_profile_" . $data["member_web_id"] . "_" .App::getLocale());
								$dt_return["cmd"] = "y";
								if($dataMemberCard){
									$dt_return["message"] = "Update complete.";
								}else{
									$dt_return["message"] = "Update complete, cannot insert member card";
								}
							}else{
								$dt_return["cmd"] = "ERR_07";
								$dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
							}
						}else{
							$dt_return["cmd"] = "ERR_08";
							$dt_return["message"] = trans("register.REGISTER_OLD_PASSWORD");
						}
					}
				}
			}else{
				$dt_return["cmd"] = "ERR_09";
				$dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
			}
		}else{
			$dt_return["cmd"] = "ERR_01";
			$dt_return["message"] = trans("register.REGISTER_ERROR_MESSAGE");
		}

		if($dt_return["message"] == trans("register.REGISTER_DUPLICATE_MAIL")){
			$dt_return["cmd"] = "y";
			$dt_return["message"] = "Update complete.";
		}

		return json_encode($dt_return);
	}

	private function insertMemberCard($data){
		if(isset($data["member_card_register"]) && $data["member_card_register"] == 1){
			$dtCheckMemberCard = $this->memberCard->getMemberExist($data["email"],$data["mobile"]);
			if($dtCheckMemberCard){
				return true;
			}else{
				$dataMemberCard["firstname"] = $data["firstname"];
				$dataMemberCard["lastname"] = $data["lastname"];
				$dataMemberCard["mobile"] = $data["mobile"];
				$dataMemberCard["status"] = $data["status_mom"];
				$dataMemberCard["type_register"] = ($data["member_type"] == 1) ? 2 : 1;
				$dataMemberCard["email"] = $data["email"];
				$dataMemberCard["eating_choice"] = (isset($data["eating"])?$data["eating"]:"");
				$dataMemberCard["eating_text"] = (isset($data["eating_text"])?$data["eating_text"]:"");
				$dtMemberUpdate = $this->memberCard->insertMember($dataMemberCard);

				if($dtMemberUpdate){
					return true;
				}
				return false;
			}
		}else{
			return true;
		}
	}

	private function registerDuplicateEmail($email){
		if ($email == "") {
			Log::Debug("No email exist.");
			return false;
		}
		$data = $this->dataTable->getMemberByEmail($email);
		$user_data = (array)$data[0];
		$prefix_token = md5($user_data["member_web_id"].$user_data["email"].$user_data["register_date"].date("Y-m-d H:i:s"));
		$suffix_token = md5(date("Y-m-d H:i:s"));
		$token_insert["web_member_id"] = $user_data["member_web_id"];
		$token_insert["token"] = $prefix_token.$suffix_token;
		$token_insert_resp = $this->dataTable->setToken($token_insert);
		if ($token_insert_resp) {
			$date_time = new OMDateTimeConverter();
			$submit_date = $date_time->gen_date(date("Y-m-d H:i:s"),"%A ที่ %d %B %Y %H:%M:%S","th");
			$mail = new OMMail(true); // notice the \  you have to use root namespace here
			$mail->isSMTP(); // tell to use smtp
			$mail->CharSet = "utf-8"; // set charset to utf8
			// $mail->SMTPAuth = true;  // use smpt auth
			// $mail->AuthType = "CRAM-MD5";  // use smpt auth
			// $mail->SMTPSecure = env('MAIL_SMTP_SECURE'); // or ssl
			$mail->setFrom("no-reply@s-momclub.com", "S-mom Club Web Member Register No-Reply");
			$mail->Subject = "S-mom Club Web Member Register";
			$mail->addCustomHeader("X-Transactional-Group: S-mom Club Web Member");
			// $mail->TemplateKey = '17656408854a4bfe';
			$mail->addAddress($user_data["email"],$user_data["firstname"]." ".$user_data["lastname"]);
			$cf_data["CF_content"] = "<div>
	                        <div style='font-size:14px;line-height:27px;margin-top:20px;'>
	                            สวัสดีค่ะ คุณ ".$user_data["firstname"]." ".$user_data["lastname"]."
	                        </div>
	                        <div style='font-size:13px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;line-height:27px;'>
	                            วัน" . $submit_date . " เราพบว่ามีการพยายามลงทะเบียนด้วยบัญชีของคุณ ".$user_data["firstname"]." <br/>จากเว็บไซต์ S-Mom Club
	                        </div>
	                        <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;'>
	                            หากไม่ใช่การลงทะเบียนจากคุณหรือมีบุคคลอื่นพยายามลงทะเบียนโดยใช้ข้อมูลของคุณ เพื่อความปลอดภัยสามารถ <a href='".url()."/".Core\Lng::url()."/password/reset/".$token_insert["token"]."' style='color:#FFF;'><span style='line-height: 37px;padding: 6px 20px;border-radius: 22px;background-color:#8e6d1d'>คลิกที่นี่</span></a> เพื่อเปลี่ยนรหัสผ่านของคุณค่ะ
	                        </div>
	                        <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;'>
	                                หากคุณไม่สามารถคลิกลิงก์นี้ได้ กรุณาคัดลอกลิงค์ด้านล่างไปวางที่เว็บเบราเซอร์ของคุณค่ะ
	                            </div>
	                        <div style='line-height:20px;font-size: 10px;width:550px;'>
	                           <a style='color:#8e6d1d'>".url()."/".Core\Lng::url()."/password/reset/".$token_insert["token"]."</a>
	                        </div>
	                        <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;'>
	                            ขอบคุณค่ะ<br>
	                            ทีมงาน S-Mom Club
	                        </div>
	                    </div>";
			// $mail->MsgHTML(json_encode($cf_data));
			$mail->MsgHTML($cf_data["CF_content"]);
			if ($mail->sendMail()){
				Log::Debug("Duplicate Mail Sent To:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
				return true;
			} else {
				Log::Error("Duplicate Mail Sending Error:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
				return false;
			}
		}else{
			Log::Error("Cannot insert Token");
			return false;
		}
	}

	private function checkMomStatus($date){
		$now = date("Y-m-d");
		$child_date = new DateTime(date('Y-m-d', strtotime($date)));
		$date_now = new DateTime(date('Y-m-d', strtotime($now)));
		$days = $date_now->diff($child_date)->days;

		if($days <= 270){
			$type = 1;
		}else if($days <= 635){
			$type = 2;
		}else{
			$type = 3;
		}
		return $type;
	}

	private function checkPassword($new_password){
		$member_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		$dtMemberCheckEmail = $this->dataTable->getPasswordHistory($new_password,$member_id);
		if($dtMemberCheckEmail){
			return false;
		}
		return true;
	}

	private function checkOldPassword($password){
		$member_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		$check = $this->dataTable->getOldPassword($password,$member_id);
		if($check){
			return true;
		}
		return false;
	}

	public function genVerify($member_id = "") {
		if (!$member_id) {
			$member_id = "1446606033268";
		}
		$data = $this->dataTable->getUserData($member_id);
		$user_data = (array)$data[0];
		$prefix_token = md5($user_data["member_web_id"].$user_data["email"].$user_data["register_date"].date("Y-m-d H:i:s"));
		$suffix_token = md5(date("Y-m-d H:i:s"));
		$token_insert["web_member_id"] = $user_data["member_web_id"];
		$token_insert["token"] = $prefix_token.$suffix_token;
		$token_insert_resp = $this->dataTable->setToken($token_insert);
		if ($token_insert_resp) {
			$mail = new OMMail(true); // notice the \  you have to use root namespace here
			$mail->isSMTP(); // tell to use smtp
			$mail->CharSet = "utf-8"; // set charset to utf8
			// $mail->SMTPAuth = true;  // use smpt auth
			// $mail->AuthType = "CRAM-MD5";  // use smpt auth
			// $mail->SMTPSecure = env('MAIL_SMTP_SECURE'); // or ssl
			$mail->setFrom("no-reply@s-momclub.com", "S-mom Club Web Member Verify No-Reply");
			$mail->Subject = "S-mom Club Web Member Verify";
			$mail->addCustomHeader("X-Transactional-Group: S-mom Club Web Member");
			// $mail->TemplateKey = '17656408854a4bfe';
			$mail->addAddress($user_data["email"],$user_data["firstname"]." ".$user_data["lastname"]);
//			$cf_data["CF_displayname"] = $user_data["firstname"]." ".$user_data["lastname"];
//			$cf_data["CF_textcontent"] = "Verify link :".url()."/".Core\Lng::url()."/verify/".$token_insert["token"];
			$cf_data["CF_content"] = "<div>
                            <div style='font-size:14px;line-height:27px;margin-top:20px;'>
                                ยินดีต้อนรับ คุณ ".$user_data["firstname"]." ".$user_data["lastname"]."
                            </div>
                            <div style='font-size:13px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;line-height:27px;'>
                                ขอบคุณที่ท่านให้ความสนใจและเป็นส่วนหนึ่งกับ S-Mom Club
                            </div>
                            <div style='font-size:13px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;line-height:27px;'>
                                กรุณาคลิกลิงก์ด้านล่างเพื่อยืนยันการสมัครสมาชิก
                            </div>

                            <div style='margin-top:20px;line-height:25px;font-size: 14px;width:550px;'>
                                <a href='".url()."/".Core\Lng::url()."/verify/".$token_insert["token"]."' style='color:#8e6d1d'>คลิกที่นี่</a>
                            </div>
                            <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;'>
                                หากคุณไม่สามารถคลิกลิงก์นี้ได้ กรุณาคัดลอกลิงก์ด้านล่างไปวางที่เว็บเบราเซอร์ของคุณค่ะ
                            </div>
                            <div style='line-height:20px;font-size: 10px;width:550px;'>
                               ".url()."/".Core\Lng::url()."/verify/".$token_insert["token"]."
                            </div>
                            <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;'>
                                ขอบคุณค่ะ<br>
                                ทีมงาน S-Mom Club
                            </div>
                        </div>";
			$mail->MsgHTML($cf_data["CF_content"]);
			if ($mail->sendMail()){
				Log::Debug("Verify Mail Sent To:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
			} else {
				Log::Error("Verify Mail Sending Error:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
			}
		}
	}

	public function policy(){
		$cache_name = "RegisterPolicyCache_".App::getLocale();
		$view_cache = Cache::get($cache_name);
		// if ($view_cache == "") {
			$view_cache = (string)view('register.policy');
			Cache::put($cache_name,$view_cache,10);
		// }
		ViewSession::viewSessionGlobal();
		$view_cache = ViewSession::make($view_cache);
        return $view_cache;
	}

	public function send_mail_register(){
		$data = Input::all();
		$email = $data["email"];
		$cmd = ['c'=>'n', 'm'=>'error'];
		if ($email == "") {
			Log::Debug("No email exist.");
			$cmd = ['c'=>'n', 'm'=>'No email exist.'];
			return false;
		}
		$data = $this->dataTable->getMemberByEmail($email);
		$user_data = (array)$data[0];
		$prefix_token = md5($user_data["member_web_id"].$user_data["email"].$user_data["register_date"].date("Y-m-d H:i:s"));
		$suffix_token = md5(date("Y-m-d H:i:s"));
		$token_insert["web_member_id"] = $user_data["member_web_id"];
		$token_insert["token"] = $prefix_token.$suffix_token;
		$token_insert_resp = $this->dataTable->setToken($token_insert);
		if ($token_insert_resp) {
			$mail = new OMMail(true); // notice the \  you have to use root namespace here
			$mail->isSMTP(); // tell to use smtp
			$mail->CharSet = "utf-8"; // set charset to utf8
			// $mail->SMTPAuth = true;  // use smpt auth
			// $mail->AuthType = "CRAM-MD5";  // use smpt auth
			// $mail->SMTPSecure = env('MAIL_SMTP_SECURE'); // or ssl
			$mail->setFrom("no-reply@s-momclub.com", "S-mom Club Web Member Register No-Reply");
			$mail->Subject = "S-mom Club Web Member Register";
			$mail->addCustomHeader("X-Transactional-Group: S-mom Club Web Member");
			// $mail->TemplateKey = '17656408854a4bfe';
			$mail->addAddress($user_data["email"],$user_data["firstname"]." ".$user_data["lastname"]);
			$cf_data["CF_content"] = "<div>
	                        <div style='font-size:14px;line-height:27px;margin-top:20px;'>
	                            ยินดีต้อนรับ คุณ ".$user_data["firstname"]." ".$user_data["lastname"]."
	                        </div>
	                        <div style='font-size:13px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;line-height:27px;'>
	                            ขอบคุณที่ท่านให้ความสนใจและเป็นส่วนหนึ่งกับ S-Mom Club
	                        </div>
	                        <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;'>
	                            กรุณา <a href='".url()."/".Core\Lng::url()."/password/create/".$token_insert["token"]."' style='color:#FFF;'><span style='line-height: 37px;padding: 6px 20px;border-radius: 22px;background-color:#8e6d1d'>คลิกที่นี่</span></a> เพื่อเปลี่ยนรหัสผ่านของคุณค่ะ
	                        </div>
	                        <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;'>
	                                หากคุณไม่สามารถคลิกลิงก์นี้ได้ กรุณาคัดลอกลิงค์ด้านล่างไปวางที่เว็บเบราเซอร์ของคุณค่ะ
	                            </div>
	                        <div style='line-height:20px;font-size: 10px;width:550px;'>
	                           <a style='color:#8e6d1d'>".url()."/".Core\Lng::url()."/password/create/".$token_insert["token"]."</a>
	                        </div>
	                        <div style='margin-top:20px;line-height:20px;font-size: 14px;width:550px;	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;'>
	                            ขอบคุณค่ะ<br>
	                            ทีมงาน S-Mom Club
	                        </div>
	                    </div>";
			$mail->MsgHTML($cf_data["CF_content"]);
			if ($mail->sendMail()){
				Log::Debug("Register Mail Sent To:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
				$cmd = ['c'=>'y', 'm'=>"Register Mail Sent To: ".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")"];
			} else {
				Log::Error("Register Mail Sending Error:".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")");
				$cmd = ['c'=>'n', 'm'=>"Register Mail Sending Error: ".$user_data["email"]." (".$user_data["firstname"]." ".$user_data["lastname"].")"];
			}
		}else{
			Log::Error("Cannot insert Token");
			$cmd = ['c'=>'n', 'm'=>"Cannot insert Token"];
		}
		echo json_encode($cmd);
		exit();
	}
}
