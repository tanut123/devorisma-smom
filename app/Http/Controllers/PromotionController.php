<?php namespace App\Http\Controllers;

use App;
use App\Promotion;
use Input;
use Response;
use Meta;
use App\Core;
use Cache;
use App\Core\ViewSession;

class PromotionController extends App\Core\Controller {
	public function index()
	{
		$cache_name = 'PromotionCache_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('promotion.index');
	        Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
        $view_cache = ViewSession::make($view_cache);

 		return $view_cache;
	}
	public function loadAjax(){
		$data = Input::all();
		$pagelist = $data["page"];
		$limit_start = $data["limit"];
		$start = ($pagelist - 1) * $limit_start;
		$limit = $data["limit"]+1;

		$table = new Promotion();
		$dt = $table->loadlist($start,$limit);

		$count = 1;
		$count_data = 1;
		$has_loadmore = false;
		$ajax_html = "";
		foreach ($dt as $key => $value) {
			if($count_data <= $limit_start){
				$d_day = date('d', strtotime($value->valid_date));
				$d_month = date('m', strtotime($value->valid_date));

				$ajax_html.="<a class='list-title' href='".url()."/".\App\Core\Lng::url()."/promotion/".$value->slug."'>
				                <li class='list-item ".($count == 2 ? "list-item-center":"")."'>
									<div class='list-img'>
										<img class='lazyload' data-expand='+10' data-src='".\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c300x216','promotion')."' alt=''>
									</div>
									<div class='txt-list-panel'>
										".$value->title."
									</div>
								</li>
                            </a>";
				$count++;
				if($count == 3){
					$count =0;
				}
				$count_data++;
			}else{
				$has_loadmore = true;
			}
		}



		return Response:: json(["ajax_html" => $ajax_html,"has_loadmore" => $has_loadmore]);
		// return Response:: json(["start" => $start]);
		// return Response:: json($dt);
	}
	public function convetMonth($input){
		$d_th_month = array(
		        	"0"=>"",
				    "1"=>"ม.ค.",
				    "2"=>"ก.พ.",
				    "3"=>"มี.ค.",
				    "4"=>"เม.ย.",
				    "5"=>"พ.ค.",
				    "6"=>"มิ.ย.",
				    "7"=>"ก.ค.",
				    "8"=>"ส.ค.",
				    "9"=>"ก.ย.",
				    "10"=>"ต.ค.",
				    "11"=>"พ.ย.",
				    "12"=>"ธ.ค."
		           );
		return $d_th_month[(int)$input];
	}
	public function createDateString($input){

		list($date,$time) = explode(" ",$input);
		list($year,$month,$days) = explode("-",$date);

		$return_val = $days." ".$this->convetMonth($month)." ".(\App\Core\Lng::url()=="th" ? $year+543 : $year);
		// var_dump($year,$month,$days);
		// var_dump($return_val);
		// exit();
		return $return_val;
	}
	public function listDetail($slug)
	{
		$cache_name = 'PromotionDetailCache_' . $slug . "_" . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
			$table = new Promotion();
			$dt = $table->load_detail($slug);

			if(count($dt) <= 0){
				$view_cache = (string)abort("404");
			}else{
				Meta::meta('title', $dt[0]->title);
		        Meta::meta('description', $dt[0]->title);
		        Meta::meta('image', Core\OMImage::readFileName($dt[0]->image_gen,$dt[0]->image, "c230x205", "promotion"));

				$return = (array)$dt[0];
				$return["datestring"] = $this->createDateString($return["valid_date"]);
				$view_cache = (string)view('promotion.listdetail',["data" => $return]);
			}
			Cache::put($cache_name, $view_cache, 10);
		}
		ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

	    return $view_cache;
	}
}
