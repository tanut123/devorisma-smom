<?php namespace App\Http\Controllers;

use App;
use Illuminate\Routing\Route;
use Meta;
use Crypto;
use App\ProductPage;
use App\Core;
use Input;
use Response;
use View;
use App\Core\Lng;
use Cache;
use App\Core\ViewSession;
use App\Pageviews;
use App\MemberWeb;
use Session;

class ProductController extends App\Core\Controller {

	private $dt_product;

	public function __construct()
    {
    	parent::__construct();
    	$this->dt_product = new ProductPage();
	}

	public function index($type){
		$cache_name = 'ProductCache_'. $type . "_" . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
                    $page = "";
                    if($type == "mom_gold" || $type == "promama"){
                            $page = $this->mom_gold();
                    }else if($type == "progress_gold" || $type == "gold_progress"){
                            $page = $this->progress_gold();
                    }else if($type == "pe_gold"){
                            $page = $this->pe_gold();
                    }else{
                            $page = $this->progress();
                    }
                    $view_cache = (string)$page;
	        Cache::put($cache_name, $view_cache, 10);
	    }
    	ViewSession::viewSessionGlobal();
        $view_cache = ViewSession::make($view_cache);

    	return $view_cache;
	}

	public function getMomTip($data){
		$momtip_data = "";
         $icon_link = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABsklEQVRIib2WvW4CMQzHLXgIRsTWxO5asXRiY6/UnYUFnuEeo7qJgZytLrwGEr3DfgIWtk4sSEgn0QGq0g84rq3y3yLZ+SXxRwxQoXnWaWnmRyo0U8aVCpUmtFeh8rieaeZH86zTqtrrrIrg2sY4UaFShdbGmBrToGDfy6euW7DvGdPAGFMVWqtQaYyTIrh2LZCyH6rQ1pgWy+D6SQKNS/ZJAo1lcH1jWqjQVtkPKyHPD9A0xtQEd8Z+XAX5CWrsxwd/TJ8foHnW2BhTZdzk7O/rQL4qZ3+vjBtjTH80UPZDE9z9FXQKNMHdtyctgmsfYuTH55w14J0KPV18mi8y9mMV2n5KGmOcGNPiUoxepje3yvhqjOFa4CGGtDDGCQAc60ioXAbXr3L+DXAZXF+FynnWacGxYNfXZl5dYJJAQ4XWmvkRqNDsbNb8E/BY+DNQxpUxDerA6gKNaaCMK1ChsmDfqwurAyzY91SoBBPa51PX/Q0MAMACPprQ/lKLyqeua0L7uDeLHbOI2RizzqJ2EICIvREgctcHiPiffZwk0k8NEHkGeVeU6epU0ebGU/3nRPwGRm3+AjGDpggAAAAASUVORK5CYII=";
        $icon_link_bookmark = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAABAklEQVQ4T92TsUoDQRRFz0sTQf/BsRQbK79DJJ0guwELBTGlFjuuY2EnCZZKJk0+IHY2foCVneWO+AUWakTwSSQLy5ospjRT3nfPhXd5I0x5NjYXoAdKbe/MZ9eTbFIBZ4ABBs6HzVnhACz/AzjdX1vSt9dLFUZdNIBF4Bm4E3g59eGwuPuvwpKmORLlvFTQZ02kkXazm0p4NCwFfAFbzodBuXFJInMrQh2VvutlV7khiY0VOEF0x3Wf+rluo5VdRLdV+RAbGx0POs6HVjE9jcx62gsPRc3Gpg387F4JTzqMOYMfgftpH6WgbwCreWHvwMIfoLJlKEnTtMYXNUvAUIXjb9UseSHfehsXAAAAAElFTkSuQmCC'>
								<span class='FXMed text-bookmark";
        $icon_view = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAASCAYAAAA6yNxSAAABTElEQVR4Ac2WJVBEURSG1y3iDhnvM9szBW1IpOP0gLtDpG9eKi4Nh4q768/MCWfuPDkX35mv3PPL0/vWofNrr03PA60gCg7BG3EIojTLk6XJS12gCMyBNyFz5HF9tTwMlk1KFsAEsWCkIW/4M8Uh0GUSOgNyDDw5NDPydIGQtLwQbJgETQOPhddDGiPvBii0Ky8BNyYB2yCoPBuFhIutB0lrlHEDSszKW0hkRhXTpoI1NlsDqWxeY5PVwoudoEfwZMcyT8RgHmHzOEFeD3B+iAcE4nMW7gYvqobW3Ex3IcgdkB7ArXK77gw0d4rmVnYA8luQxcJHDOYjbJ4lvQU6D2G9sk9MgQdiCoTYvEHwEGq/hicg2eC99yhryaTVew2FG9E8iLXwxoIF/Y1IbyveBxUgwPQBWtuXb8Vf/xjdgFXiRvAx+p+f4z//Q/IOPYCpMz+bvCEAAAAASUVORK5CYII=";
       	$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
        $memberWeb = new MemberWeb();
        if(count($data) > 0) {
        	$BASE_LANG = url()."/".App\Core\Lng::url()."/";
        	$momtip_data .= "<div class='trick-mom-box'>";
        	$momtip_data .= "<ul class='trick-mom-body-list FXregular'>";
        	$bookmarkDataArr = $memberWeb->getBookmarkByMember($member_web_id);
            $dataBookmark = [];
            foreach ($bookmarkDataArr as $key => $dataArr) {
            	$dataBookmark[] = $dataArr->trick_mom_id;
            }
        	foreach ($data as $key => $data) {
        		 $type_mom_data = str_replace([1,2,3],['pregnancy-mom','lactating-mom','toddler-mom'], $data->type_mom);
                    $momtip_data.="
                    <span data-id='" . $data->trick_mom_id . "' href='javascript:void(0)' class='list-item-momtip list-body-img-panel'>
                        <li class='list-body list-body-active lazyload' data-bgset='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWP4////GQAJyAPKSOz6nwAAAABJRU5ErkJggg==' data-expand='+10'>
                            <a href='". $BASE_LANG.$type_mom_data ."/". $data->slug ."' class='list-content'>
                                <div class='list-body-img-panel_cover'>
									 <img data-src='".\App\Core\OMImage::readFileName($data->image_gen,$data->image,'c280x205','trick_mom')."' width='280' height='205' class='trick-img lazyload' data-expand='+10'/>
                                     ";
                    if ($data->youtube_source != '' || $data->file_mp4 != '') {
                        $momtip_data.="
                            <span class='cover-video'>
                                <img class='lazyload' data-expand='+10' data-src='". env('BASE_CDN', '') ."/images/home/trick-mom/icon-video.png'>
                            </span>";
                    }

                    $momtip_data.="
                            </div>
                        <div class='list-body-content-panel'>
                            <div class='view-panel'>
                                <img data-src='".$icon_view."' class='view_icon lazyload' data-expand='+10'><span class='view_number'>".number_format($data->view)." Views</span>
                            </div>
                            <div class='list-body-title'>
								<div class='txt color-blue FXMed'>". $data->title ."</div>
                            </div>
                            <div class='list-body-desc'>
								<div class='txt color-gray FXMed'>". $data->description ."</div>
                            </div>
                        </div>
	                        </a>";

	                $bookmark = "F";
			        if(in_array($data->trick_mom_id, $dataBookmark)){
			        	$bookmark = "T";
			        }
                    if($member_web_id != "" && $member_web_id != null && $bookmark == "F"){
                    	$momtip_data.="<a href='javascript:void(0);' onClick='javascript:addBookmark(\"". $data->trick_mom_id ."\", this)' data-url='". $BASE_LANG.$type_mom_data ."/". $data->slug ."' class='list-body-link-bookmark'>
	                            <img class='list-body-logo lazyload' data-src='".$icon_link_bookmark."' data-expand='+10'>
	                            <span class='FXMed text-bookmark'>จัดเก็บ</span>
	                        </a>";
                    }else{
                   		 $momtip_data.="<a href='". $BASE_LANG.$type_mom_data ."/". $data->slug ."' class='list-body-link'>
	                            <img class='list-body-logo lazyload' data-src='".$icon_link."' data-expand='+10'>
	                        </a>";
                    }

                    $momtip_data.=" </li>
                   		</span>";
        	}
		   	$momtip_data .= "</ul></div>";
		}
		return $momtip_data;
	}

	public function getCustomDataList($data){
		$custom_data_list = "";
		if(count($data) > 0) {
			foreach ($data as $key => $data) {
				$custom_data_list .= "
					<a href='".$data->link_url."' target='_blank' class='custom-page-link'>
						<div class='custom-page-body-box'>
							<img class='custom-page-img' src='".\App\Core\OMImage::readFileName($data->image_data_gen,$data->image_data,'o0x0','custom_link_product')."' title='".$data->custom_link_title."' >
						</div>
					</a>
				";
			}
		}
		return $custom_data_list;
	}


    public function mom_gold()
    {
        Meta::meta('title', 'S26 PROMAMA นมสำหรับแม่ตั้งครรภ์ นมผงสูตรใหม่ ไขมันต่ำ ผสมใยอาหาร');
        Meta::meta('description', 'นมสำหรับคนท้อง ไขมันต่ำ นมผงสูตรใหม่ S26 PROMAMA โปรมาม่า มีสารอาหารสำคัญเพื่อแม่ตั้งครรภ์ช่วงให้นมบุตร ผสมใยอาหาร ไอโอดีน สังกะสีสูง แมคนีเซียม ฯลฯ เพื่อสุขภาพแม่ท้องและน้ำนมแม่');
        Meta::set('keywords','นมสำหรับคนท้อง, นมสำหรับแม่ตั้งครรภ์, นมผงที่ดี, นมผง, wyeth nutrition, นมผงคนท้อง, s26, promama, โปรมาม่า, อาหารเพิ่มน้ำนม');
        $data = array();
		$data["list"] = $this->dt_product->getMomTipList("1");
		$data["list_data"] = $this->getMomTip($data["list"]);
        return view('product.mom_gold.index',$data);
    }
	public function progress_gold()
	{
		Meta::meta('title', 'S26 Gold Progress โกลด์ โปรเกรส ผลิตภัณฑ์นมผงสูตรใหม่');
		Meta::meta('description', 'ผลิตภัณฑ์นมผง สูตรใหม่จาก S26 Gold Progress มีฟอสโฟไลปิด และเพิ่มสฟิงโกไมอีลิน พร้อมสาธิตวิธีเตรียมนมผง การเลี้ยงลูกด้วยนมผงสูตรใหม่ โกลด์ โปรเกรส เพื่อพัฒนาการสมองของลูกน้อย');
		Meta::set('keywords','ผลิตภัณฑ์นมผง, นมผงเด็ก, สฟิงโกไมอีลิน, พัฒนาสมองเด็ก, s26');
		$data = array();
		$data["list"] = $this->dt_product->getMomTipList("2");
		$data["list_data"] = $this->getMomTip($data["list"]);
		return view('product.progress_gold.index',$data);
	}

	public function pe_gold()
	{
		Meta::meta('title', 's-26 พีอี โกลด์');
		Meta::meta('description', 's26 พี โกลด์์');
		$data = array();
		$data["list"] = $this->dt_product->getMomTipList("3");
		$data["list_data"] = $this->getMomTip($data["list"]);
		$data["custom_data"] = $this->dt_product->getCustomData();
		$data["custom_data_list"] = $this->getCustomDataList($data["custom_data"]);
		return view('product.pe_gold.index',$data);
	}

	public function progress()
	{
		Meta::meta('title', 's-26 โปรเกรส');
		Meta::meta('description', 's26 โปรเกรส์');
		$data = array();
		$data["list"] = $this->dt_product->getMomTipList("4");
		$data["list_data"] = $this->getMomTip($data["list"]);
		return view('product.progress.index',$data);
	}
}