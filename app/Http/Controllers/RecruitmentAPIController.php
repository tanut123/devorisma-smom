<?php namespace App\Http\Controllers;

use App;

use Meta;
use App\Core;
use Cache;
use App\Core\ViewSession;
use App\Recruitment;
use Input;
use PHPMailer;
use Log;
use DateTime;
use App\Core\OMDateTimeConverter;
use App\Core\OMMail;

class RecruitmentAPIController extends App\Core\Controller {
	protected $dataTable;

	public $error_message = "";
	public function __construct()
    {
    	parent::__construct();
    	Meta::meta('title', 'สมัครสมาชิก');
    	$this->dataTable = new Recruitment();
	}

	public function listData($type){
		$dt = [];
		$param = Input::all();
		if($type == 'province'){
			$dt = $this->dataTable->getProvince();
		}elseif($type == 'district'){
			$dt = $this->dataTable->getDistrict($param['province']);
		}elseif($type == 'subdistrict'){
			$dt = $this->dataTable->getSubDistrict($param['district']);
		}elseif($type == 'mom_product'){
			$dt = $this->dataTable->getMomProduct();
		}elseif($type == 'child_product'){
			$dt = $this->dataTable->getChildProduct();
		}elseif($type == 'place'){
			$dt = $this->dataTable->getPlace($param['type']);
		}elseif($type == 'occupation'){
			$dt = $this->dataTable->getOccupation();
		}elseif($type == 'reason'){
			$dt = $this->dataTable->getReason();
		}
		echo json_encode($dt);
		exit();
	}

	public function register(){
		$dataRecruitment = Input::all();
		$reg_date = date("Y-m-d H:i:s");
        $id = $this->dataTable->generateUUID();
		$dtMemberInsert = false;
		$dt_return["cmd"] = 'n';

		$data_draft['member_recruitment_id'] = $id;

		try {
			$data_draft['firstname'] = $this->validateData($dataRecruitment,'firstname',true);
			$data_draft['lastname'] = $this->validateData($dataRecruitment,'lastname',true);
			$data_draft['birthday'] = $this->dataTable->changeDateFormat($this->validateData($dataRecruitment,'birthday',false));
			$data_draft['mobile'] = $this->validateData($dataRecruitment,'mobile',true);
			$data_draft['occupation'] = $this->validateData($dataRecruitment,'occupation',false);
			$data_draft['salary'] = $this->validateData($dataRecruitment,'salary',false);
			$data_draft['email'] = $this->validateData($dataRecruitment,'email',false);
			$data_draft['line_id'] = $this->validateData($dataRecruitment,'line_id',false);
			$data_draft['status_mom'] = $this->validateData($dataRecruitment,'status_mom',false);
			$data_draft['has_one_child'] = $this->validateData($dataRecruitment,'has_one_child',false);
			$data_draft['address'] = $this->validateData($dataRecruitment,'address',true);
			$data_draft['moo'] = $this->validateData($dataRecruitment,'moo',true);
			$data_draft['alley'] = $this->validateData($dataRecruitment,'alley',true);
			$data_draft['road'] = $this->validateData($dataRecruitment,'road',true);
			$data_draft['sub_district'] = $this->validateData($dataRecruitment,'sub_district',true);
			$data_draft['district'] = $this->validateData($dataRecruitment,'district',true);
			$data_draft['province'] = $this->validateData($dataRecruitment,'province',true);
			$data_draft['zipcode'] = $this->validateData($dataRecruitment,'zipcode',true);
			$data_draft['advertisement'] = $this->validateData($dataRecruitment,'advertisement',true);
			$data_draft['smom_product_name'] = $this->validateData($dataRecruitment,'smom_product_name',true);
			$data_draft['smom_product_name_other'] = $this->validateData($dataRecruitment,'smom_product_name_other',false);
			if($data_draft['status_mom'] == 1){
				$data_draft['current_mom_product'] = $this->validateData($dataRecruitment,'current_product',true);
				$data_draft['current_child_product'] = 0;
				$data_draft['previous_product'] = 0;
			}else{
				$data_draft['current_child_product'] = $this->validateData($dataRecruitment,'current_product',true);
				$data_draft['current_mom_product'] = 0;
				$data_draft['previous_product'] = $this->validateData($dataRecruitment,'previous_product',true);
				$data_draft['reason_to_change'] = $this->validateData($dataRecruitment,'reason_to_change',false);
			}
			$data_draft['web_page'] = $this->validateData($dataRecruitment,'web_page',false);
			$data_draft['register_date'] = $reg_date;
	        $data_draft['obj_lang'] = "MAS";
	        $data_draft['obj_rev'] = 1;
	        $data_draft['obj_status'] = "active";
	        $data_draft['obj_state'] = "published";
	        $data_draft['obj_created_date'] = $reg_date;
	        $data_draft['obj_created_user_id'] = 0;
	        $data_draft['obj_modified_date'] = $reg_date;
	        $data_draft['obj_modified_user_id'] = 0;
	        $data_draft['obj_published_date'] = $reg_date;
	        $data_draft['obj_published_user_id'] = 0;

			// All pass insert
			if($this->error_message == ""){
				$dtMemberInsert = $this->dataTable->insertData($data_draft);
				$dataRecruitment["member_recruitment_id"] = $id;
				if($dtMemberInsert){
					$dataChild = $this->insertDataChild($dataRecruitment);
					if($dataChild){
						$dt_return["cmd"] = "y";
						$dt_return["message"] = "Insert complete.";
					}else{
						$dt_return["cmd"] = "ERR";
						$dt_return["message"] = "Invalid Data.";
						if(isset($dataRecruitment['dd'])){
							$dt_return["message"] = "Cannot Insert data child.";
							$dt_return["err_message"] = $this->error_message;
						}
					}
				}else{
					$dt_return["cmd"] = "ERR";
					$dt_return["message"] = "Invalid Data.";
					if(isset($dataRecruitment['dd'])){
						$dt_return["err_message"] = "Cannot Insert data.";
					}
				}
			}else{
				$dt_return["cmd"] = "ERR";
				$dt_return["message"] = "Invalid Data.";
				if(isset($dataRecruitment['dd'])){
					$dt_return["error_message"] = $this->error_message;
				}
			}
		} catch (Exception $e) {
			$dt_return["cmd"] = "ERR";
			$dt_return["message"] = "Invalid Data.";
			if(isset($dataRecruitment['dd'])){
				$dt_return["error_message"] = $e->getMessage();
			}
		}

		return json_encode($dt_return);
	}

	private function insertDataChild($dataRecruitment){
		$this->error_message = '';
		$date_now = date("Y-m-d H:i:s");
        $cc = 0;
        $countChild = 0;
        if(isset($dataRecruitment['child'])){
        	$countChild = count($dataRecruitment['child']);
        	if($countChild > 0){
	        	foreach ($dataRecruitment["child"] as $key => $dataChild) {
	        		$member_web_child_data_id = $this->dataTable->generateUUID();
					$data_draft['member_recruitment_child_id'] = $member_web_child_data_id;
					$data_draft['child_date'] = $this->dataTable->changeDateFormat($this->validateData($dataChild,'child_date',true));
					$data_draft['firstname'] = $this->validateData($dataChild,'firstname',true);
					$data_draft['lastname'] = $this->validateData($dataChild,'lastname',true);
					$data_draft['gender'] = $this->validateData($dataChild,'gender',true);
					$data_draft['hospital'] = $this->validateData($dataChild,'hospital',true);
					$data_draft['province'] = $this->validateData($dataChild,'province',true);
					$data_draft['treatment_rights'] = $this->validateData($dataChild,'treatment_rights',true);
			        $data_draft['obj_lang'] = "MAS";
			        $data_draft['obj_rev'] = 1;
			        $data_draft['obj_parent_id'] = $dataRecruitment["member_recruitment_id"];
			        $data_draft['obj_status'] = "active";
			        $data_draft['obj_state'] = "draft";
			        $data_draft['obj_tmp_session_id'] = 0;
			        $data_draft['obj_created_date'] = $date_now;
			        $data_draft['obj_created_user_id'] = 0;
			        $data_draft['obj_modified_date'] = $date_now;
			        $data_draft['obj_modified_user_id'] = 0;
			        if($this->error_message == ''){
				        $dtMemberChildDraft = $this->dataTable->insertChildData($data_draft);
				        if($dtMemberChildDraft){
				        	$cc++;
				        }
			        }
		    	}

		    	if($cc == $countChild){
		    		return true;
		    	}else{
		    		return false;
		    	}
		    }else{
		    	return true;
		    }
        }else{
        	return true;
        }
	}

	private function validateData($data,$field,$require){
		$dataReturn = '';
		if($require){
			if(!isset($data[$field])){
				$this->error_message = "Please fill " . $field;
				$dataReturn =  '';
			}else{
				$dataReturn = $data[$field];
			}
		}else{
			if(!isset($data[$field])){
				$dataReturn = '';
			}else{
				$dataReturn = $data[$field];
			}
		}
		return $dataReturn;
	}
}
