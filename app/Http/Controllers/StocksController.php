<?php namespace App\Http\Controllers;

use App\Core\OMImage;
use App\Media;
use OpenCloud\Rackspace;

class StocksController extends Controller {


	public function read($module, $mode, $width, $height, $hash1, $hash2, $uid, $original_name){
		$file_data = $this->write($module, $mode, $width, $height, $hash1, $hash2, $uid, $original_name);
		$file_type = $file_data["content_type"];
		$target_url = $file_data["target_url"];


		header("Content-Type: ". $file_type);
		readfile($target_url);

		// Optimize Image
		$target_url_new = str_replace(public_path()."/stocks", "", $target_url);
		if(env('CDN_ENABLE', false)){
			exec("curl -k  ". url() ."/img_optim?t=".urlencode($target_url_new) ."");

			$client = new Rackspace(Rackspace::US_IDENTITY_ENDPOINT, array(
			    'username' => env('RACKSPACE_USER', ''),
			    'apiKey'   => env('RACKSPACE_APIKEY', '')
			),
		    [
		        // Guzzle ships with outdated certs
		        Rackspace::SSL_CERT_AUTHORITY => 'system',
		        Rackspace::CURL_OPTIONS => [
		            CURLOPT_SSL_VERIFYPEER => true,
		            CURLOPT_SSL_VERIFYHOST => 2,
		            CURLOPT_IPRESOLVE => CURL_IPRESOLVE_V4
		        ],
		    ]);

			$objectStoreService = $client->objectStoreService(null, env('RACKSPACE_REGION', ''));
			$container = $objectStoreService->getContainer('s-momclub.com');
			if(is_file($target_url)){
				$handle = fopen($target_url, 'r');
				$splitFolder = explode('/',$target_url_new);
				$lastIndex = count($splitFolder) - 1;
				$splitFolder[$lastIndex] = rawurlencode($splitFolder[$lastIndex]);
				$target_url_new = implode('/', $splitFolder);
				$upload = $container->uploadObject('/stocks' . $target_url_new , $handle);
				fclose($handle);
			}
		}else{
			exec("curl -k  ". url() ."/img_optim?t=".urlencode($target_url_new) ." > /dev/null 2>&1 &");
		}


		exit();
	}

	private function write($module, $mode, $width, $height, $hash1, $hash2, $uuname, $original_name){
		$file_data = array();

		$file_extention = explode(".", $original_name);
		$file_extention = strtolower(end($file_extention));

		//Query media from database
		$dt_media = new Media();
		$dt_file = $dt_media->getMedia($module, $uuname, $original_name);

		if($dt_file != "" && isset($dt_file->content_type)){

			$data_file = $dt_file->data;
			$content_type = $dt_file->content_type;

			//Set path media
			$path = $hash1 ."/". $hash2;
			$dir = public_path() ."/stocks/". $module ."/". $mode . $width ."x". $height ."/". $path ."/" . $uuname . "/";
			$target_file = $dir.$original_name;
			$s = false;

			//Create folder
			$create_folder = $this->createFolder($dir);

			// Get mode resize
			$m = $this->getMode($content_type, $mode);


			if($m != "original"){
				if(!file_exists($target_file)){
					file_put_contents($target_file, $data_file);

					$s = OMImage::ResizeImage($target_file, $target_file, $width, $height, $file_extention, $m, null);
				}
			}else{
				file_put_contents($target_file, $data_file);
			}
			if( $content_type == "images/jpeg"){
				// phpjpegoptim --strip-all -q  $data_file
			}

			$file_data["content_type"] = $content_type;
			$file_data["target_url"] = $target_file;
			return $file_data;
		}else{
			abort(404);
		}
	}


	private function createFolder($target_dir){
		if(!is_dir($target_dir)){
			mkdir($target_dir, 0777,true);
			return true;
		}
		return false;
	}

	private function getMode($file_type, $mode){
		$m = "original";

		if($file_type == "image/pjpeg" ||  $file_type == "image/jpeg" || $file_type == "image/png" || $file_type == "image/gif"){

			if ($mode == "c") {
				$m = "crop";
			} else if ($mode == "l") {
				$m = "letterbox";
			} else if ($mode == "w") {
				$m = "fixwidth";
			} else if ($mode == "h") {
				$m = "fixheight";
			} else if ($mode == "s") {
				$m = "scale";
			} else if ($mode == "d") {
				$m = "scaledown";
			} else if ($mode == "o") {
				$m = "original";
			} else {
				abort(404);
				exit();
			}

		}else{
			$m = "original";
		}

		return $m;
	}



}
