<?php namespace App\Http\Controllers;

use App\Media;
use Log;

class CronClearMediaController extends Controller {

	public function clearMediaFile(){
		Log::debug("START");

		$mediaDb = new Media();
		// Query All media_file
	    $dtTable = $mediaDb->getAllMedia();
	    $countValid = 0;
	    if(count($dtTable) > 0 &&  !empty($dtTable)){

	    	foreach ($dtTable as $row) {
	    		$table_name = $row->TABLE_NAME;
	    		Log::debug("START CLEAR :: " . $table_name);

	    		// Delete Media file Unuse
	    		$dsDelete = $mediaDb->mediaDelete($table_name);
	    		if($dsDelete > 0){
	    			$countValid += $dsDelete;
					Log::debug("DELETE DATA AMOUNT  :: " . $dsDelete);
				}else{
					Log::debug("NOT FOUND DATA TO DELETE");
				}

	    		$tblname = str_replace("_media_file" , "" ,$table_name);
	    		// Select table found
	    		$ids_active = $mediaDb->tableSelect($tblname);
	    		if(count($ids_active) > 0 ){
		    		// Select Media file By id
		    		$ds = $mediaDb->getJoinMedia($tblname);
		    		if(count($ds) > 0){
			    		foreach ($ds as $media) {
			    			// DELETE Media file By join table
			    			$dsUUnameDelete = $mediaDb->mediaDeleteByUUName($table_name, $media->uuname);
							if($dsUUnameDelete > 0){
								$countValid ++;
								Log::debug("DELETE DATA UUNAME  :: " . $media->uuname);
							}else{
								Log::debug("NOT FOUND DATA JOIN TO DELETE");
							}
			    		}
		    		}else{
		    			Log::debug("NOT FOUND ID FROM :: ".$tblname . "_draft");
		    		}
	    		}else{
			    	Log::debug("NOT FOUND TABLE :: ".$tblname . "_draft");
	    		}
	    	}
	    	return $countValid;
	    }else{
	    	Log::debug("NOT FOUND");
	    }
		Log::debug("END");
		return 0;
	}
}
