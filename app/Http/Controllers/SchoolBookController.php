<?php namespace App\Http\Controllers;

use App;
use GetId3\GetId3Core;
use Meta;
use Crypto;
use App\SchoolBook;
use App\Core;
use Cache;
use App\Core\ViewSession;

class SchoolBookController extends App\Core\Controller {

	public function __construct()
    {
    	parent::__construct();
    	Meta::meta('title', 'สื่อเพื่อลูกน้อย');
	}

	public function index()
	{
		$cache_name = 'SchoolBookCache_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('media.school_book.index');
	        Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	    $view_cache = ViewSession::make($view_cache);

 		return $view_cache;
	}

	public function schoolBookAjax($type){
		$dt = array();
		$table = new SchoolBook();
        if ($type == "music") {
            $db_data = $table->$type();
            $echo_data = "<table class='music_list' border='0'>";
            $no = 1;
            $first_song = "";
            foreach ($db_data as $data) {
                $title_field_name = "song_title_".\App\Core\Lng::url();
                $file_music = \App\Core\OMImage::readFileName($data->file_mp3_gen,$data->file_mp3,'0x0','school_book_song');
                //var_dump($file_music);
                //($this->BASE_LANG."<br>");
                $file_location = str_replace(url(),base_path()."/public",$file_music);
                //var_dump($file_location);
                //if ($file_ext == "mp3") {
                $getID3 = new GetId3Core();
                if (!file_exists($file_location)) {
                    $mp3_data = $table->getMusicMediaFile($data->file_mp3_gen);
                    $dir_explode = explode('/',$file_location);
                    array_pop($dir_explode);
                    $target_dir = implode('/',$dir_explode);
                    //$target_dir.='/';
                    //var_dump($target_dir);
                    if(!is_dir($target_dir)){
                        mkdir($target_dir, 0777,true);
                    }
                    file_put_contents($file_location,$mp3_data[0]->data);

                    $file_data = $getID3->analyze($file_location);
                    //$file_data = new \App\Core\MP3File($file_location);

                } else {

                    $file_data = $getID3->analyze($file_location);
                    //$file_data = new \App\Core\MP3File($file_location);
                }
                //}

                $duration_time = round($file_data["playtime_seconds"]);
                $duration_sec = $duration_time % 60;
                $duration_minute = floor($duration_time/60);

                $echo_data.= "
                    <tr mp3_value='".$file_music."' class='music_list_row".($first_song == "" ? " play_active" : "")."'>
                        <td class='number'>
                            ".$no."
                        </td>
                        <td class='song_name'>
                            ".$data->$title_field_name."
                        </td>
                        <td class='duration'>
                            ".sprintf("%01d:%02d", $duration_minute,$duration_sec)."
                        </td>
                        <td class='download'>
                          <a href='".url()."/download_media/school_book_song/".$data->file_mp3_gen."/".$data->file_mp3."' target='_blank' download='".$data->file_mp3."'><div class='download_icon'></div></a>
                        </td>
                    </tr>";
                if ($first_song == "") {
                    $first_song = $file_music;
                }
                $no++;
                    // <a href='".$file_music."' download='".$data->file_mp3."'><div class='download_icon' download_file='"..""'></div></a>
            }
            $echo_data .= "</table>";
            $dt[$type] = $echo_data;
            $dt["first_song"] = $first_song;
            return view('media.school_book.'.$type, $dt);
        } else {
            $dt[$type] = $table->$type();
            return view('media.school_book.'.$type, $dt);
        }
	}

	public function redirectPage($type,$id)
	{
		$table = new SchoolBook();
		$type_single = $type."_single";
		$db_data = $table->$type_single($id);
		if($type == "game"){
			$link = \App\Core\OMImage::readFileName($db_data[0]->file_pdf_gen,$db_data[0]->file_pdf,'o0x0','school_book_game');
		}else if($type == "diy"){
			$link = \App\Core\OMImage::readFileName($db_data[0]->file_pdf_gen,$db_data[0]->file_pdf,'o0x0','school_book_diy');
		}else if($type == "story") {
			$link = \App\Core\OMImage::readFileName($db_data[0]->file_pdf_gen,$db_data[0]->file_pdf,'o0x0','school_book_story');
		}else{
			$link = url()."/download_media/school_book_song/".$db_data[0]->file_mp3_gen."/".$db_data[0]->file_mp3;
			// var_dump($link);
			// die();
			return redirect($link);
		}
		return redirect($link);
	}

}
