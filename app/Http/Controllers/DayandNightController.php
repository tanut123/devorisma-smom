<?php namespace App\Http\Controllers;

use App;
use Meta;
use Cache;
use App\Core\ViewSession;

class DayandNightController extends App\Core\Controller {

	public function index()
	{

		$cache_name = "day_and_night_cache_".App::getLocale();
		$day_and_night_view = Cache::get($cache_name);
		if ($day_and_night_view == "") {
			Meta::meta('title', 'Day & Night');
			$day_and_night_view = (string)view('day-and-night.index');
			Cache::put($cache_name,$day_and_night_view,10);
		}
		ViewSession::viewSessionGlobal();
		$day_and_night_view = ViewSession::make($day_and_night_view);
		return $day_and_night_view;

		//return view('day-and-night.index');
	}

}
