<?php namespace App\Http\Controllers;

use App\Home;
use App\MemberWeb;
use App;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\View;
use Meta;
use Cache;
use App\Core\ViewSession;
use Session;

class HomeController extends App\Core\Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	public function __construct()
    {
    	parent::__construct();
    	Meta::meta('title', trans('title.TITLE_HOME') );
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

        $member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
        $cache_name = "home_view_cache_". $member_web_id ."_". App::getLocale();
        $home_view = Cache::get($cache_name);
        if ($home_view == "") {
            $content = array();
            $dt_home = new Home();
            $trick_mom_data = "";
            $dt_sub_category = $dt_home->getChildSubCat();
  			$dt_trick_mom_list = $dt_home->getTrickMomList('all');
  			$trick_mom_data .= $this->getTrickMom($dt_trick_mom_list,'all',$dt_sub_category);
            $dt_trick_mom_pregnancy = $dt_home->getTrickMomList('pregnancy');
  			$trick_mom_data .= $this->getTrickMom($dt_trick_mom_pregnancy,'pregnancy',$dt_sub_category);
            $dt_trick_mom_lactating = $dt_home->getTrickMomList('lactating');
  			$trick_mom_data .= $this->getTrickMom($dt_trick_mom_lactating,'lactating',$dt_sub_category);
            $dt_trick_mom_toddler = $dt_home->getTrickMomList('toddler');
  			$trick_mom_data .= $this->getTrickMom($dt_trick_mom_toddler,'toddler',$dt_sub_category);
            $content["trick_mom"] = $trick_mom_data;

            $home_banner_data = "";
            $dt_home_banner = $dt_home->getHomeBanner();
            if (isset($dt_home_banner) && count($dt_home_banner) > 0) {
                foreach($dt_home_banner as $key => $value) {
                    $home_banner_data.="<div id='banner-list".$value->home_banner_id."' class='banner-list' data-delay='".$value->duration."'>";
                    if ($value->youtube_source == '' && $value->file_mp4_gen == '') {
                        if ($value->url != "") {
                            $home_banner_data.="<a href=".$value->url." target='_blank'>";
                        }
                        $home_banner_data.="<div class='image-loader' data-type='image' data-image_desktop='".\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c1280x750','home_banner')."' data-image_mobile='".\App\Core\OMImage::readFileName($value->image_mobile_gen,$value->image_mobile,'c640x1095','home_banner')."'  style='display:none;'></div>";
                        if ($value->url) {
                            $home_banner_data.="</a>";
                        }
                    }else {
                    	$dataMp4 = explode(".", $value->youtube_source);
						$data_youtupe_mp4 = "";
						if(end($dataMp4) == "mp4"){
							$data_youtupe_mp4 = "mp4";
						}
						if ($value->url != "") {
                            $home_banner_data.="<a href=".$value->url." class='link-video' target='_blank'></a>";
                        }
                        if (($value->youtube_source == '' || $data_youtupe_mp4 == 'mp4') || $value->file_mp4_gen != '') {

                        	if($data_youtupe_mp4 == "mp4"){
								$file_mp4 = asset($value->youtube_source);
							}else{
								$file_mp4 = \App\Core\OMImage::readFileName($value->file_mp4_gen,$value->file_mp4,'o0x0','home_banner');
							}

                       $home_banner_data.=" <div class='image-loader-video' data-type='image' data-image_desktop='".\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c1280x750','home_banner')."' data-image_mobile='".\App\Core\OMImage::readFileName($value->image_mobile_gen,$value->image_mobile,'c640x1095','home_banner')."'  style='display:none;'></div>
                        <div id='btnVideoPlay".$value->home_banner_id."' class='btnPlayVideo slide-banner-list' data-type='video' data-source='".$file_mp4."'></div>
                            <div class='play-control visible-xs' onClick='playPauseVideo();'>
                        </div>";

                        } else {
                            $home_banner_data.="<div id='player-".$value->home_banner_id."' data-player='".$key."' data-source='".$value->youtube_source."' data-type='youtube' class='player_list slide-banner-list'></div>";
                        }
                    }
                    if($value->title != "" || $value->description != "") {
                        $home_banner_data.="<div class='display-description lazyload' data-bgset='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWPI9mTYDAADPwFoco/XFgAAAABJRU5ErkJggg==' data-expand='-10'>";
                        if($value->title != "") {
                            $home_banner_data.="
                        <div class='display-title-text'>
                            ".$value->title."
                        </div>";
                        }
                        if($value->description != "") {
                            $home_banner_data.="
                        <div class='display-description-text'>
                            <span class='dyntextval'>
                            ".\App\Core\OM::trimWithDot($value->description,150)."
                            </span>
                        </div>";
                        }
                        $home_banner_data.="
                    </div>";
                    }
                    $home_banner_data.="
            </div>";
                }
            }
            $content["home_banner"] = $home_banner_data;


            $kl_left_data = "";
            $dt_kl_left = $dt_home->getKnowledgeLeftBanner();
            if (isset($dt_kl_left) && count($dt_kl_left) > 0) {
                foreach ($dt_kl_left as $val) {
                    $kl_left_data .= "
            <a href='".$val->link_url."'><div class='left_banner_data'>
                <img class='img_banner_left' src='".\App\Core\OMImage::readFileName($val->image_data_gen,$val->image_data,'c727x727','knowledge')."' title='".$val->knowledge_name."'>
            </div></a>";
                }
            }

            $kl_right1_data = "";
            $dt_kl_right1 = $dt_home->getKnowledgeRight1Banner();
            if (isset($dt_kl_right1) && count($dt_kl_right1) > 0) {
                foreach ($dt_kl_right1 as $val) {
                    $kl_right1_data .= "
                <a href='".$val->link_url."'><div class='right_top_banner_data'>
                    <img class='img_banner_right1' src='".\App\Core\OMImage::readFileName($val->image_data_gen,$val->image_data,'c344x344','knowledge')."' title='".$val->knowledge_name."'>
                </div></a>";
                }
            }

            $kl_right2_data = "";
            $dt_kl_right2 = $dt_home->getKnowledgeRight2Banner();
            if (isset($dt_kl_right2) && count($dt_kl_right2) > 0) {
                foreach ($dt_kl_right2 as $val) {
                    $kl_right2_data .= "
            <a href='".$val->link_url."'><div class='right_top_banner_data'>
                <img class='img_banner_right2' src='".\App\Core\OMImage::readFileName($val->image_data_gen,$val->image_data,'c344x344','knowledge')."' title='".$val->knowledge_name."'>
            </div></a>";
                }
            }

            $kl_rightb_data = "";
            $dt_kl_rightb = $dt_home->getKnowledgeRightBBanner();
            if (isset($dt_kl_rightb) && count($dt_kl_rightb) > 0) {
                foreach ($dt_kl_rightb as $val) {
                    $kl_rightb_data .= "
            <a href='".$val->link_url."'><div class='right_bottom_banner_data'>
                <img class='img_banner_rightb' src='".\App\Core\OMImage::readFileName($val->image_data_gen,$val->image_data,'c727x344','knowledge')."' title='".$val->knowledge_name."'>
            </div></a>";
                }
            }

            $content['kl_left'] = $kl_left_data;
            $content['kl_right1'] = $kl_right1_data;
            $content['kl_right2'] = $kl_right2_data;
            $content['kl_rightb'] = $kl_rightb_data;

            $knowledge_list_data_pinned = "";
            $dt_knowledge_list = $dt_home->getKnowledgeList();
             if (isset($dt_knowledge_list) && count($dt_knowledge_list) > 0){
             	 foreach ($dt_knowledge_list as $val) {
             	 	$d_day = date('d', strtotime($val->valid_date));
					$d_month = date('m', strtotime($val->valid_date));
	                if (strlen($val->link_url) > 0) {
	                    $link = $val->link_url;
	                } else {
	                	$category_name = $val->knowledge_category;
	                	$hasSub = true;
	                	if($category_name == "a") {
	                		$category_name = "pregnancy";
	                	}else if($category_name == "b"){
	                		$category_name = "pickyeater";
	                	}else if($category_name == "c"){
	                		$category_name = "braindevelopment";
	                		$hasSub = false;
	                	}else if($category_name == "d"){
	                		$category_name = "toddler";
	                	}else if($category_name == "e"){
	                		$category_name = "lactating";
	                		$hasSub = false;
	                	}else if($category_name == "f"){
	                		$category_name = "ลูกน้อยถ่ายสบาย";
	                		$hasSub = false;
	                	}

	                	$subcategory_name = substr($val->knowledge_subcategory,1,1);

	                	if($hasSub){
	                		$subcategory_name = $subcategory_name."/";
	                	}

	                    $link = url()."/".\App\Core\Lng::url()."/".$category_name."/".$subcategory_name.$val->slug;
	                }

	                $knowledge_list_data_pinned .= "
						 <a  href='".$link."'>
						 	<li class='list-item lazyload' data-bgset='". env('BASE_CDN', '') ."/images/knowledge/bg_list.png' data-expand='+2'>
						 		<div class='list-img'>
									<img data-src='".\App\Core\OMImage::readFileName($val->image_gen,$val->image,'c307x252','knowledge_list')."' alt='' class='lazyload' data-expand='+10'>
								</div> 
								<div class='badge-list lazyload' data-bgset='". env('BASE_CDN', '') ."/images/knowledge/badge.png' data-expand='+2'>
									<span class='txt-day'>".$d_day."</span>
									<span class='txt-month'>".$this->convetMonth($d_month)."</span>
								</div>
								<div class='txt-list-panel'>
									<div class='list-title'>".$val->title."</div>
									<div class='short-desc'>".\App\Core\OM::trimWithDot($val->description,100)."</div>
								</div>
							</li>
	                	</a>";
             	 }
             }

            $content['knowledge_list'] = $knowledge_list_data_pinned;

            $home_view = (string)view('home.index',$content);
            Cache::put($cache_name,$home_view,10);
        }
        ViewSession::viewSessionGlobal();
        $home_view = ViewSession::make($home_view);
        return $home_view;
	}

	public function getTrickMom($data,$str,$dt_sub_category){
		$trick_data = "";
        $icon_link = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABsklEQVRIib2WvW4CMQzHLXgIRsTWxO5asXRiY6/UnYUFnuEeo7qJgZytLrwGEr3DfgIWtk4sSEgn0QGq0g84rq3y3yLZ+SXxRwxQoXnWaWnmRyo0U8aVCpUmtFeh8rieaeZH86zTqtrrrIrg2sY4UaFShdbGmBrToGDfy6euW7DvGdPAGFMVWqtQaYyTIrh2LZCyH6rQ1pgWy+D6SQKNS/ZJAo1lcH1jWqjQVtkPKyHPD9A0xtQEd8Z+XAX5CWrsxwd/TJ8foHnW2BhTZdzk7O/rQL4qZ3+vjBtjTH80UPZDE9z9FXQKNMHdtyctgmsfYuTH55w14J0KPV18mi8y9mMV2n5KGmOcGNPiUoxepje3yvhqjOFa4CGGtDDGCQAc60ioXAbXr3L+DXAZXF+FynnWacGxYNfXZl5dYJJAQ4XWmvkRqNDsbNb8E/BY+DNQxpUxDerA6gKNaaCMK1ChsmDfqwurAyzY91SoBBPa51PX/Q0MAMACPprQ/lKLyqeua0L7uDeLHbOI2RizzqJ2EICIvREgctcHiPiffZwk0k8NEHkGeVeU6epU0ebGU/3nRPwGRm3+AjGDpggAAAAASUVORK5CYII=";
        $icon_link_bookmark = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAABAklEQVQ4T92TsUoDQRRFz0sTQf/BsRQbK79DJJ0guwELBTGlFjuuY2EnCZZKJk0+IHY2foCVneWO+AUWakTwSSQLy5ospjRT3nfPhXd5I0x5NjYXoAdKbe/MZ9eTbFIBZ4ABBs6HzVnhACz/AzjdX1vSt9dLFUZdNIBF4Bm4E3g59eGwuPuvwpKmORLlvFTQZ02kkXazm0p4NCwFfAFbzodBuXFJInMrQh2VvutlV7khiY0VOEF0x3Wf+rluo5VdRLdV+RAbGx0POs6HVjE9jcx62gsPRc3Gpg387F4JTzqMOYMfgftpH6WgbwCreWHvwMIfoLJlKEnTtMYXNUvAUIXjb9UseSHfehsXAAAAAElFTkSuQmCC'>
								<span class='FXMed text-bookmark";
        $icon_view = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAASCAYAAAA6yNxSAAABTElEQVR4Ac2WJVBEURSG1y3iDhnvM9szBW1IpOP0gLtDpG9eKi4Nh4q768/MCWfuPDkX35mv3PPL0/vWofNrr03PA60gCg7BG3EIojTLk6XJS12gCMyBNyFz5HF9tTwMlk1KFsAEsWCkIW/4M8Uh0GUSOgNyDDw5NDPydIGQtLwQbJgETQOPhddDGiPvBii0Ky8BNyYB2yCoPBuFhIutB0lrlHEDSszKW0hkRhXTpoI1NlsDqWxeY5PVwoudoEfwZMcyT8RgHmHzOEFeD3B+iAcE4nMW7gYvqobW3Ex3IcgdkB7ArXK77gw0d4rmVnYA8luQxcJHDOYjbJ4lvQU6D2G9sk9MgQdiCoTYvEHwEGq/hicg2eC99yhryaTVew2FG9E8iLXwxoIF/Y1IbyveBxUgwPQBWtuXb8Vf/xjdgFXiRvAx+p+f4z//Q/IOPYCpMz+bvCEAAAAASUVORK5CYII=";
        $member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
        $subcat = array();
        $memberWeb = new MemberWeb();
		foreach ($dt_sub_category as $key => $dt_sub_category) {
			if (App::getLocale() == "en") {
        		$subcat[$dt_sub_category->sub_category_child_id] = $dt_sub_category->sub_category_text_en;
			} else {
        		$subcat[$dt_sub_category->sub_category_child_id] = $dt_sub_category->sub_category_text_th;
			}
        }
		if(count($data) > 0) {
                $BASE_LANG = url()."/".App\Core\Lng::url()."/";
                if($str == 'all'){
                	$trick_data.="<div class='trick-mom-box visible-xs mobile'>";
                }else{
                	$trick_data.="<div class='trick-mom-box hidden-xs'>";
                }
                $trick_data.="<ul class='trick-mom-body-list FXregular'>";

                $bookmarkDataArr = $memberWeb->getBookmarkByMember($member_web_id);
                $dataBookmark = [];
                foreach ($bookmarkDataArr as $key => $dataArr) {
                	$dataBookmark[] = $dataArr->trick_mom_id;
                }
                foreach ($data as $key => $data) {

                    $type_mom_data = str_replace([1,2,3],['pregnancy-mom','lactating-mom','toddler-mom'], $data->type_mom);
                    $trick_data.="
                    <span data-id='" . $data->trick_mom_id . "' href='javascript:void(0)' class='list-item-momtip list-body-img-panel'>
                        <li class='list-body list-body-active lazyload' data-bgset='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVQImWP4////GQAJyAPKSOz6nwAAAABJRU5ErkJggg==' data-expand='+10'>
                        		<div class='cat-title'>
	                        		<a href='". $BASE_LANG . $type_mom_data . "'>
		                            	<div class='txt txt-blue FXMed'>".$data->title_cat."</div>
		                            </a>
	                            </div>
                                <div class='list-body-img-panel_cover'>
                                  <a href='". $BASE_LANG.$type_mom_data ."/". $data->slug ."'>
									 <img data-src='".\App\Core\OMImage::readFileName($data->image_gen,$data->image,'c280x205','trick_mom')."' width='280' height='205' class='trick-img lazyload' data-expand='+10'/>
                                     ";
                    if ($data->youtube_source != '' || $data->file_mp4 != '') {
                        $trick_data.="
                            <span class='cover-video'>
                                <img data-src='". env('BASE_CDN', '') ."/images/home/trick-mom/icon-video.png' class='lazyload' data-expand='+10'>
                            </span>";
                    }

                    $trick_data.="
							</a>
                            </div>
                        <div class='list-body-content-panel'>
                            <div class='view-panel'>
                                <img data-src='".$icon_view."' class='view_icon lazyload' data-expand='+10'><span class='view_number'>".number_format($data->view)." Views</span>
                            </div>
                            <div class='list-body-title'>
	                            	<div class='txt txt-gold'>".($data->sub_category != null ? $subcat[$data->sub_category] : "")."</div>
	                            <a href='". $BASE_LANG.$type_mom_data ."/". $data->slug ."'>
	                                <div class='txt txt-black FXMed'>". $data->title ."</div>
	                            </a>
                            </div>
                        </div>";
                    $bookmark = "F";
			        if(in_array($data->trick_mom_id, $dataBookmark)){
			        	$bookmark = "T";
			        }
                    if($member_web_id != "" && $member_web_id != null && $bookmark == "F"){
                    	$trick_data.="<a href='javascript:void(0);' onClick='javascript:addBookmark(\"". $data->trick_mom_id ."\", this)' data-url='". $BASE_LANG.$type_mom_data ."/". $data->slug ."' class='list-body-link-bookmark'>
	                            <img class='list-body-logo lazyload' data-src='".$icon_link_bookmark."' data-expand='+10'>
	                            <span class='FXMed text-bookmark'>จัดเก็บ</span>
	                        </a>";
                    }else{
                   		 $trick_data.="<a href='". $BASE_LANG.$type_mom_data ."/". $data->slug ."' class='list-body-link'>
	                            <img class='list-body-logo lazyload' data-src='".$icon_link."' data-expand='+10'>
	                        </a>";
                    }

                    $trick_data.="	</li>
                    </span>";
	            	}
	            $trick_data.="
	                </ul>
	                </div>";
            }

        return $trick_data;
	}

	public function createDateString($input){
		list($date,$time) = explode(" ",$input);
		list($year,$month,$days) = explode("-",$date);
		$return_val = $days." ".$this->convetMonth($month)." ".(\App\Core\Lng::url()=="th" ? $year+543 : $year);
		return $return_val;
	}

	public function convetMonth($input){
		$d_th_month = array(
		        	"0"=>"",
				    "1"=>"ม.ค.",
				    "2"=>"ก.พ.",
				    "3"=>"มี.ค.",
				    "4"=>"เม.ย.",
				    "5"=>"พ.ค.",
				    "6"=>"มิ.ย.",
				    "7"=>"ก.ค.",
				    "8"=>"ส.ค.",
				    "9"=>"ก.ย.",
				    "10"=>"ต.ค.",
				    "11"=>"พ.ย.",
				    "12"=>"ธ.ค."
		           );
		$d_en_month = array(
		        	"0"=>"",
				    "1"=>"JAN",
				    "2"=>"FEB",
				    "3"=>"MAR",
				    "4"=>"APR",
				    "5"=>"MAY",
				    "6"=>"JUN",
				    "7"=>"JUL",
				    "8"=>"AUG",
				    "9"=>"SEP",
				    "10"=>"OCT",
				    "11"=>"NOV",
				    "12"=>"DEC"
		            );
		if(App::getLocale() == "en"){
			return $d_en_month[(int)$input];
		}else{
			return $d_th_month[(int)$input];
		}
	}

}
