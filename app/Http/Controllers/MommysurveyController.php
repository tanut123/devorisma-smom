<?php namespace App\Http\Controllers;

use App;
use App\Pageviews;
use App\Core\OMImage;
use Meta;
use Cache;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\View;
use App\Core\ViewSession;
use Session;
use DB;
use App\Core\SettingManagement;

class MommysurveyController extends App\Core\Controller {
   public function index()
    {
        $cache_name = "survey_view_cache_". App::getLocale();
        $survey_view = Cache::get($cache_name);
        if($survey_view == ""){
            $setting = new SettingManagement();
            Meta::title($setting->get("META_TITLE_SURVEY"));
            Meta::meta('robots', 'index,follow');
            Meta::meta('keywords', $setting->get("META_KEYWORD_SURVEY"));
            Meta::meta('description', $setting->get("META_DESCTION_SURVEY"));
            Meta::meta('site_name', $setting->get("SITE_NAME"));
            Meta::meta('image', asset(env('BASE_CDN', '') . '/images/pic.jpg'));
            $survey_view = (string) view('mommysurvey.index');
            // $home_view = (string)view('home.index',$content);
            Cache::put($cache_name,$survey_view,10);
        }
        ViewSession::viewSessionGlobal();
        $survey_view = ViewSession::make($survey_view);
        return $survey_view;
    }
    public function indexSurvey()
    {
        $create_date = date("Y-m-d H:i:s");
        $status_mom          =  isset($_POST["status_mom"])?$_POST["status_mom"]:"";
        $age_mom             =  isset($_POST["age_mom"])?$_POST["age_mom"]:"";
        $living_mom          =  isset($_POST["living_mom"])?$_POST["living_mom"]:"";
        $other               =  isset($_POST["other"])?$_POST["other"]:"";
        $value_know          =  isset($_POST["know_about_smom"])?$_POST["know_about_smom"]:"";
        $value_know_from     =  isset($_POST["know_from"])?$_POST["know_from"]:"";
        $service_smom        =  isset($_POST["service_smom"])?$_POST["service_smom"]:"";
        $service_from_fb     =  isset($_POST["service_from_fb"])?$_POST["service_from_fb"]:"";
        $service_from_line   =  isset($_POST["service_from_line"])?$_POST["service_from_line"]:"";
        $service_from_web    =  isset($_POST["service_from_web"])?$_POST["service_from_web"]:"";
        $rating_pleased      =  isset($_POST["rating_pleased"])?$_POST["rating_pleased"]:"";
        $rating_credible     =  isset($_POST["rating_credible"])?$_POST["rating_credible"]:"";
        $rating_conveniently =  isset($_POST["rating_conveniently"])?$_POST["rating_conveniently"]:"";
        $rating_interest     =  isset($_POST["rating_interest"])?$_POST["rating_interest"]:"";
        $rating_useful       =  isset($_POST["rating_useful"])?$_POST["rating_useful"]:"";
        $like_club           =  isset($_POST["like_club"])?$_POST["like_club"]:"";
        $think_about_brand   =  isset($_POST["think_about_brand"])?$_POST["think_about_brand"]:"";


        $dataUpdate["status_mom"]            = $status_mom;
        $dataUpdate["age_mom"]               = $age_mom;
        $dataUpdate["living_mom"]            = $living_mom;
        $dataUpdate["other"]                 = $other;
        $dataUpdate["know_about_smom"]       = $value_know;
        $dataUpdate["know_from"]             = $value_know_from;
        $dataUpdate["service_from_smom"]     = $service_smom;
        $dataUpdate["service_from_line"]     = $service_from_line;
        $dataUpdate["service_from_fb"]       = $service_from_fb;
        $dataUpdate["service_from_web"]      = $service_from_web;
        $dataUpdate["rating_pleased"]        = $rating_pleased;
        $dataUpdate["rating_credible"]       = $rating_credible;
        $dataUpdate["rating_conveniently"]   = $rating_conveniently;
        $dataUpdate["rating_interest"]       = $rating_interest;
        $dataUpdate["rating_useful"]         = $rating_useful;
        $dataUpdate["like_club"]             = $like_club;
        $dataUpdate["think_about_brand"]     = $think_about_brand;
        $dataUpdate["obj_lang"]              = 'MAS';
        $dataUpdate["obj_published_date"]    = $create_date;
        $dataUpdate["obj_published_user_id"] = '1';
        $dataUpdate["obj_modified_date"]     = $create_date;
        $dataUpdate["obj_modified_user_id"]  = '1';

        $dtMommy = array();
        $arr_status_mom = array("ตั้งครรภ์","มีบุตร");
        $arr_age_mom = array("13-17 ปี","18-24 ปี","25-34 ปี","35-44 ปี","มากกว่า 44 ปี");
        $arr_living_mom = array("กรุงเทพและปริมณฑล","ภาคเหนือ","ภาคกลาง","ภาคใต้","ภาคตะวันออก","ภาคตะวันออกเฉียงเหนือ","ภาคตะวันตก");
        $arr_know_about_smom = array("ไม่รู้จัก","รู้จัก");
        $arr_know_from = array("คุณหมอ, พยาบาล, บุคลากรทางการแพทย์","พ่อแม่, ญาติ, เพื่อน คนคุ้นเคย แนะนำ","โฆษณา, สื่อประชาสัมพันธ์","เว็บไซต์, เฟซบุ๊ก, เพจต่างๆ","พนักงานขาย (PC), ร้านค้า","other");
        $arr_service_smom = array("ไม่เคยใช้บริการ","เคยใช้บริการ");
        $arr_service_from_fb = array("1","2","3");
        $arr_service_from_web = array("1","2","3");
        $arr_service_from_line = array("1","2","3");
        $arr_rating_pleased = array("1","2","3","4","5");
        $arr_rating_credible = array("1","2","3","4","5");
        $arr_rating_interest = array("1","2","3","4","5");
        $arr_rating_conveniently = array("1","2","3","4","5");
        $arr_rating_useful = array("1","2","3","4","5");
        $arr_like_club = array("คลับของคุณแม่และลูกน้อย","คลับของคุณแม่ตั้งครรภ์","คลับเสริมความงามของคุณแม่","คลับให้คำปรึกษาผู้มีบุตรยาก");
        $arr_think_about_brand = array("เอนฟา","ดีจี","แอนมัม","ซิมิแลค","S-26","ไฮคิว","ตราหมี","ดูเม็กกซ์","พีเดียชัวร์");
        if($status_mom == "" && $age_mom == "" && $living_mom == "" && $know_about_smom == ""){
            echo json_encode($dtMommy);
            exit();
        }

        if(!in_array($status_mom,$arr_status_mom) || !in_array($age_mom,$arr_age_mom)  || !in_array($living_mom,$arr_living_mom) || !in_array($value_know,$arr_know_about_smom)){
            echo json_encode($dtMommy);
            exit();
        }else{
            if($value_know == "ไม่รู้จัก"){
                $dtMommy = DB::table("mommysurvey")->insert($dataUpdate);
            }else{
                if($service_smom == "ไม่เคยใช้บริการ" && in_array($value_know_from,$arr_know_from)){
                    $dtMommy = DB::table("mommysurvey")->insert($dataUpdate);
                }else{
                    if(in_array($service_smom,$arr_service_smom) && in_array($service_from_fb,$arr_service_from_fb) && in_array($service_from_web,$arr_service_from_web) && in_array($service_from_line,$arr_service_from_line)
                        && in_array($rating_pleased,$arr_rating_pleased) && in_array($rating_credible,$arr_rating_credible) && in_array($rating_interest,$arr_rating_interest) && in_array($rating_conveniently,$arr_rating_conveniently) && in_array($rating_useful,$arr_rating_useful)
                        && in_array($like_club,$arr_like_club) && in_array($think_about_brand,$arr_think_about_brand)
                    ){
                        $dtMommy = DB::table("mommysurvey")->insert($dataUpdate);
                    }
                }
            }
        }

        echo json_encode($dtMommy);
        exit();
    }
}
