<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Core\Compress;

class CompressCotroller extends Controller {
	public function index(Request $request, $type,$data){
        $data = preg_split("/\.js$|\.css$/i",$data);
        $data_input = $data[0];
        //echo ($request->server('HTTP_IF_MODIFIED_SINCE'));
        $data = Compress::loadFile($type,$data_input,$request->server('HTTP_IF_MODIFIED_SINCE'));
       if ($type == "css") {
	        $info["content_type"] = ("text/css");
       } else {
	        $info["content_type"] = ("application/x-javascript; charset=utf-8");
       }
       $base_cdn = env('BASE_CDN', '');
       if($base_cdn != ''){
	        $array_search = ["/(\.\.\/)+(images)/"];
		    $array_match = [$base_cdn."/images"];
		    $data = preg_replace($array_search, $array_match, $data);
       }
          return  response($data, "200")
              ->header('Content-Type', $info["content_type"]);


//        $data_input = Input::get("data");
//        $file_explode = explode(",",$data_input);
//        $this->setlastModified($file_explode,$type);
//        foreach ($file_explode as $val ) {
//            $buffer = file_get_contents(public_path().'/'.$type.'/'.$val.'.'.$type);
//
//            if ($type=='js') {
//                $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
//                $buffer = preg_replace('/\n\s+\/\/\s.*[\n|\r\n]/', '', $buffer);
//                if (!preg_match("/.min/",$val)) {
//                    $buffer = preg_replace('/\t/',"",$buffer);
//                    $buffer = preg_replace('/;\s*\n/',";",$buffer);
//                    $buffer = preg_replace('/{\s*\n/',"{",$buffer);
//                    $buffer = preg_replace('/\n\s*}/',"}",$buffer);
//                    $buffer = preg_replace('/,\s*\n/',",",$buffer);
//                    $buffer = preg_replace('/\n\s*\n/',"\n",$buffer);
//                    $buffer = preg_replace('/}\s*\n/',"}",$buffer);
//                    $buffer = preg_replace('/\n\s*\./',".",$buffer);
//                    $buffer = str_replace("  ","",$buffer);
//                }
//            }
//            if ($type=='css') {
//                $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
//                $buffer = preg_replace('/\s+/'," ",$buffer);
//                $buffer = str_replace(': ', ':', $buffer);
//                $buffer = str_replace(array("\n","\r\n","\t"), '', $buffer);
//            }
//
//            $print_out_data[] = trim($buffer);
//        }
//        if ($type=='js') {
//            echo implode("\n",$print_out_data);
//        } else {
//            echo implode(" ",$print_out_data);
//        }

	}
}
