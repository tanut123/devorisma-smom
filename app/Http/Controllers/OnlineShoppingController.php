<?php namespace App\Http\Controllers;

use App;
use Meta;
use Cache;
use App\Core\ViewSession;

class OnlineShoppingController extends App\Core\Controller {

	public function __construct(){
    	parent::__construct();
    	Meta::meta('title', 'Online Shopping');
	}

	public function index(){
		$cache_name = "online_shopping_cache";
		$app_view = Cache::get($cache_name);
		if ($app_view == "") {
			$app_view = (string)view('online_shopping.index');
			Cache::put($cache_name,$app_view,10);
		}
		ViewSession::viewSessionGlobal();
		$app_view = ViewSession::make($app_view);
        return $app_view;
	}
}
