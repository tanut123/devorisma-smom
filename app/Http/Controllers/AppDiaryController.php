<?php namespace App\Http\Controllers;

use App\Home;
use App\MemberWeb;
use App;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\View;
use Meta;
use Cache;
use App\Core\ViewSession;
use Session;
use Input;

class AppDiaryController extends App\Core\Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	public function __construct()
    {
    	parent::__construct();
    	Meta::set('title', "S-26 Mom GOLD by Wyeth Nutrition");
    	Meta::title("S-26 Mom GOLD by Wyeth Nutrition");
    	Meta::set('description', "App แม่ท้องแห่งแรกในประเทศไทย โดย S-26 Mom GOLD");
    	// Meta::set('url', "app.s-momclub.com");
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index($uuname)
	{
		$uuname = $uuname;
		if($uuname == ""){
			abort("404");
			exit();
		}

		$hasFile = str_split($uuname, 4);
		$has_folder = str_split($hasFile[1], 2);
		// var_dump(url() . '/stocks/mobile_app/' . $has_folder[0] . '/' . $has_folder[1] . '/' . $uuname . '/' . $uuname . '.png');
		// exit();
		Meta::meta('image', url() . '/stocks/mobile_app/' . $has_folder[0] . '/' . $has_folder[1] . '/' . $uuname . '/' . $uuname . '.png');

		$cache_name = "mobile_app_".$has_folder[0].$has_folder[1].$uuname;
		$view = Cache::get($cache_name);
		if ($view == "") {
			$dt = [];
			$view = (string)view('mobile_app.index', $dt);
			Cache::put($cache_name,$view,10);
		}
		ViewSession::viewSessionGlobal();
		$view = ViewSession::make($view);
		return $view;
	}


}
