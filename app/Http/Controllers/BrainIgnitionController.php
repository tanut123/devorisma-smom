<?php namespace App\Http\Controllers;

use App;
use App\Brain;
use Illuminate\Support\Facades\Cache;
use Input;
use Response;
use Meta;
use App\Core;
use App\Core\ViewSession;
use App\Core\Lng;

class BrainIgnitionController extends App\Core\Controller {
	protected $dataTable;
	public function __construct(){
    	parent::__construct();
    	$this->dataTable = new Brain();
	}

	public function index(){
		if(App::getLocale() == "en"){
			abort("404");
			exit();
		}
		Meta::meta('title', 'Brain Ignition' );
		$cache_name = "brain_ignition_index_".App::getLocale();
		$brain_ignition_index_view = Cache::get($cache_name);
		if ($brain_ignition_index_view == "") {

			$content = array();
			$html_banner = "";
			$data_banner = $this->dataTable->loadlistMedia();
			if (isset($data_banner) && count($data_banner) > 0){
				foreach($data_banner as $key => $value) {
					$html_banner .= '<div class="slide-list" id="'.$value->brain_media_id.'">';
					if ($value->youtube_source == '' && $value->file_mp4_gen == '') {
						$html_banner .= '<div class="image-loader slide-list-inner" data-type="image" data-image="'.\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c718x399','brain_media').'"></div>';
					}else{
						$dataMp4 = explode(".", $value->youtube_source);
						$data_youtupe_mp4 = "";
						if(end($dataMp4) == "mp4"){
							$data_youtupe_mp4 = "mp4";
						}
						if (($value->youtube_source == '' || $data_youtupe_mp4 == 'mp4') || $value->file_mp4_gen != '') {
						 	if($data_youtupe_mp4 == "mp4"){
								$file_mp4 = asset($value->youtube_source);
							}else{
								$file_mp4 = \App\Core\OMImage::readFileName($value->file_mp4_gen,$value->file_mp4,'o0x0','brain_media');
							}

							// $html_banner .= '<div class="image-loader-video" data-type="image" data-image="'.\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c718x399','brain_media').'" style="display:none;"></div>';
							$html_banner .= '<div id="btnVideoPlay'.$value->brain_media_id.'" class="btnPlayVideo slide-list-inner" data-type="video" data-source="'.$file_mp4.'"></div>';
						}else{
							$html_banner.="<div id='player-".$value->brain_media_id."' data-player='".$key."' data-source='".$value->youtube_source."' data-type='youtube' class='player_list slide-list-inner'></div>";
						}

					}
					$html_banner .= '</div>';
				}
			}
			$content["banner"] = $html_banner;

			$brain_ignition_index_view = (string)view('brain_ignition.index',$content);
			Cache::put($cache_name,$brain_ignition_index_view,10);

		}
		ViewSession::viewSessionGlobal();
		$brain_ignition_index_view = ViewSession::make($brain_ignition_index_view);
		return $brain_ignition_index_view;
	}

	public function loadAjax(){
		$data = Input::all();
		$pagelist = $data["page"];
		$limit_start = $data["limit"];
		$start = ($pagelist - 1) * $limit_start;
		$limit = $data["limit"]+1;
		$dt = $this->dataTable->loadlist($start,$limit);
		$count = 1;
		$count_data = 1;
		$has_loadmore = false;
		$ajax_html = "";

		foreach ($dt as $key => $value) {
			if($count_data <= $limit_start){
				$d_day = date('d', strtotime($value->valid_date));
				$d_month = date('m', strtotime($value->valid_date));
                $link = url()."/พรสวรรค์สร้างได้ใน1000วันแรก/".$value->slug;
				$ajax_html.="
				            <a  href='".$link."'>
                                <li class='list-item lazyload ".($count == 2 ? "list-item-center":"")."' data-bgset='". env('BASE_CDN', '') ."/images/knowledge/bg_list.png' data-expand='+10'>
									<div class='list-img'>
										<img class='lazyload' data-src='".\App\Core\OMImage::readFileName($value->image_gen,$value->image,'c280x210','brain_ignite_tips')."' width='280' height='210' data-expand='+10'>
									</div>
									<div class='badge-list FXBold lazyload' data-bgset='". env('BASE_CDN', '') ."/images/brain_ignition/badge.png' data-expand='+10'>
										<span class='txt-day'>".$d_day."</span>
										<span class='txt-month'>".$this->convertMonth($d_month)."</span>
									</div>
									<div class='txt-list-panel'>
										<div class='list-title FXMed'>".\App\Core\OM::trimWithDot($value->title,63)."</div>
										<div class='list-writer'>".$value->writer."</div>
									</div>
									<div class='arrow-box'></div>
								</li>
                            </a>";
				$count++;
				if($count == 3){
					$count = 0;
				}
				$count_data++;
			}else{
				$has_loadmore = true;
			}
		}
		return Response:: json(["ajax_html" => $ajax_html,"has_loadmore" => $has_loadmore]);
	}


	public function detail($slug){
		$cache_name = "brain_ignition_detail_".$slug."_".App::getLocale();
    	error_log( $cache_name );
		$knowledge_view = Cache::get($cache_name);
		if ($knowledge_view == "") {
			$dt = $this->dataTable->load_detail($slug);
			if(count($dt) <= 0){
				abort("404");
				exit();
			}
			Meta::meta('title', $dt[0]->title);
			Meta::meta('description', $dt[0]->detail);
			Meta::meta('image', Core\OMImage::readFileName($dt[0]->image_gen,$dt[0]->image, "c230x205", "brain_ignite_tips"));
			$return = (array)$dt[0];
			$return["datestring"] = $this->createDateString($return["valid_date"]);
			$knowledge_view = (string)view('brain_ignition.detail',["data" => $return]);

			Cache::put($cache_name,$knowledge_view,10);
		}

		ViewSession::viewSessionGlobal();
		$knowledge_view = ViewSession::make($knowledge_view);

		return $knowledge_view;
	}

	public function listQA(){
		if (App::getLocale() == "en") {
			return abort(404);
		}
		Meta::meta('title', 'พรสวรรค์สร้างได้ใน1000วันแรก' );
		$cache_name = 'brain_ignition_question_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('brain_ignition.list_question');
	    	Cache::put($cache_name, $view_cache, 10);
	    }
		ViewSession::viewSessionGlobal();
	   	$view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

	public function question($mode){
		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		if (App::getLocale() == "en") {
			return abort(404);
		}
		Meta::meta('title', 'แบบวัดแวว'.$mode );
		$cache_name = 'brain_ignition_question_answer_' . $mode;
		$view_cache = Cache::get($cache_name);
		// if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('brain_ignition.question_answer',["mode" => $mode,"member_web_id" => $member_web_id]);
	    	Cache::put($cache_name, $view_cache, 10);
	    // }
		ViewSession::viewSessionGlobal();
	   	$view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

	public function certificate($mode,$level){
		$member_web_id = \Illuminate\Support\Facades\Session::get("webMemberId");
		if($member_web_id == ""){
			return redirect(App::getLocale() . '/home');
		}
		if (App::getLocale() == "en") {
			return abort(404);
		}
		Meta::meta('title', 'ใบรับรอง'.$mode );
		$cache_name = 'brain_ignition_certificate_' . $mode;
		$view_cache = Cache::get($cache_name);
		// if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('brain_ignition.certificate',["mode" => $mode,"level" => $level]);
	    	Cache::put($cache_name, $view_cache, 10);
	    // }
		ViewSession::viewSessionGlobal();
	   	$view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

	public function certificateSave(){
		if (App::getLocale() == "en") {
			return abort(404);
		}
		$cache_name = 'brain_ignition_certificate_save';
		$view_cache = Cache::get($cache_name);
		// if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('brain_ignition.certificate_save');
	    	Cache::put($cache_name, $view_cache, 10);
	    // }
		ViewSession::viewSessionGlobal();
	   	$view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

	public function certificateShare($id){
		if (App::getLocale() == "en") {
			return abort(404);
		}

		Meta::meta('title', 'พรสวรรค์สร้างได้ใน1000วันแรก');
		Meta::meta('description', 'ลูกสมองไว คุณแม่สร้างได้ มาร่วมค้นหาศักยภาพความถนัดของลูก ผ่านเช็คลิสต์พร้อมคำแนะนำเพื่อค้นหาและสนับสนุนศักยภาพลูกให้ถูกทาง');
		Meta::meta('image',url().'/stocks/certificate/certificate_'.$id.'.png');
		Meta::meta('og:image',url().'/stocks/certificate/certificate_'.$id.'.png');

		$cache_name = 'brain_ignition_certificate_share_'.$id;
		$view_cache = Cache::get($cache_name);
		
		// if($view_cache == "" || $view_cache == null){
			$view_cache = (string)view('brain_ignition.certificate_share');
	    	Cache::put($cache_name, $view_cache, 10);
	    // }
		ViewSession::viewSessionGlobal();
	   	$view_cache = ViewSession::make($view_cache);

        return $view_cache;
	}

	public function createDateString($input){

		list($date,$time) = explode(" ",$input);
		list($year,$month,$days) = explode("-",$date);

		$return_val = $days." ".$this->convertMonth($month)." ".(\App\Core\Lng::url()=="th" ? $year+543 : $year);
		return $return_val;
	}

	public function convertMonth($input){
		$d_th_month = array(
		        	"0"=>"",
				    "1"=>"ม.ค.",
				    "2"=>"ก.พ.",
				    "3"=>"มี.ค.",
				    "4"=>"เม.ย.",
				    "5"=>"พ.ค.",
				    "6"=>"มิ.ย.",
				    "7"=>"ก.ค.",
				    "8"=>"ส.ค.",
				    "9"=>"ก.ย.",
				    "10"=>"ต.ค.",
				    "11"=>"พ.ย.",
				    "12"=>"ธ.ค."
		           );
		$d_en_month = array(
		        	"0"=>"",
				    "1"=>"JAN",
				    "2"=>"FEB",
				    "3"=>"MAR",
				    "4"=>"APR",
				    "5"=>"MAY",
				    "6"=>"JUN",
				    "7"=>"JUL",
				    "8"=>"AUG",
				    "9"=>"SEP",
				    "10"=>"OCT",
				    "11"=>"NOV",
				    "12"=>"DEC"
		            );
		if(App::getLocale() == "en"){
			return $d_en_month[(int)$input];
		}else{
			return $d_th_month[(int)$input];
		}
	}

}