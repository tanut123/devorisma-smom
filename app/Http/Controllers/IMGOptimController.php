<?php namespace App\Http\Controllers;

use App;
use Input;
use App\IMGOptim;


class IMGOptimController extends App\Core\Controller {

	public function index(){
		$inputData = Input::all();
		$imgOptim = new IMGOptim();
		if(isset($inputData["t"])){
			$file = public_path() . "/stocks" . urldecode($inputData["t"]);
			if($imgOptim->optimize($file)){
				echo "OK";
			}else{
				echo "ERR_S2";
			}
		}else{
			echo "ERR_S1";
		}
		exit();
	}
}
