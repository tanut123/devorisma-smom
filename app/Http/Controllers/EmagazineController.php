<?php namespace App\Http\Controllers;

use App;
use App\Magazine;
use App\Pageviews;
use App\Core\OMImage;
use cache;
use App\Core\ViewSession;

class EmagazineController extends App\Core\Controller {

	public function index()
	{
		$cache_name = 'EmagazineCache_' . App::getLocale();
		$view_cache = Cache::get($cache_name);
		$ds_view = new Pageviews();
		$v_magazine = $ds_view->getViewMagazine("magazine");
		foreach ($v_magazine as $key => $value) {
			$view_id[$value->ref_id] = $value->total;
			ViewSession::set("view_magazine_".$value->ref_id,number_format($value->total));
		}
		if($view_cache == "" || $view_cache == null){
			$ds = new Magazine();
			$magazine = $ds->getMagazine();

			if(count($magazine) > 0 && $magazine != ""){
				$view_cache .= '<div class="wapContent">';
					if($magazine[0]->title == "special-emagazine"){
						$view_cache .= '<div class="special-emagazine-panel">';
						  $view_cache .= '<div class="special-emagazine-panel-inner">';
						    $view_cache .= '<div class="special-baby"></div>';
						    $view_cache .= '<div class="panel-text FMonX">';
						      $view_cache .= '<div class="text-title">Magazine ฉบับพิเศษ</div>';
						      $view_cache .= '<div class="text-top">“มายเบบี๋ อึง่าย ถ่ายคล่อง”</div>';
						      $view_cache .= '<div class="text-middle">รวม <span class="num">42</span> คำถาม สารพัดปัญหาการขับถ่ายของลูกรัก<br/>ที่คุณแม่ทางบ้านถามกันเข้ามาบ่อยที่สุด</div>';
						      $view_cache .= '<div class="text-bottom ">ตอบโดยพญ. พัฐ โรจน์มหามงคล<br/>กุมารแพทย์ด้านพัฒนาการและพฤติกรรม</div>';
						      $view_cache .= '<a target="_blank" class="c_load" data-id="'.$magazine[0]->magazine_id.'" href="' . OMImage::readFileName($magazine[0]->file_pdf_gen,$magazine[0]->file_pdf,'o0x0','magazine').'"><div class="btn-download">ดาวน์โหลดฟรี!</div></a>';
						      $view_cache .= '<div class="panel-view-download">';
						      $view_cache .= '<div class="list-body-logo" id="m_view_'.$magazine[0]->magazine_id.'"></div><div class="num-view">Download: '.(isset($view_id[$magazine[0]->magazine_id]) ? '[[view_magazine_'.$magazine[0]->magazine_id.']]' : "0").'</div>';
						      $view_cache .= '</div>';
						    $view_cache .= '</div>';
						    $view_cache .= '<div class="clearfix"></div>';
						  $view_cache .= '</div>';
						$view_cache .= '</div>';
					}else{
						$view_cache .= '<div class="new-emagazine-panel">';
							$view_cache .= '<div id="newEmagazine">';
								$view_cache .= '<div class="m-cover-text-detail visible-xs">';
									$view_cache .= '<h1 class="FLighter">Magazine</h1>';
									$view_cache .= '<h4 class="hidden-xs FThin">นิตยสารที่รวบรวมข้อมูล เคล็ดลับการเลี้ยงดูและการส่งเสริม <br>พัฒนาการลูกน้อย</h4>';
									$view_cache .= '<h4 class="visible-xs">นิตยสารที่รวบรวมข้อมูล เคล็ดลับ <br>การเลี้ยงดูและการส่งเสริม พัฒนาการลูกน้อย</h4>';
								$view_cache .= '</div>';
								$view_cache .= '<div class="mag-cover">';
									$view_cache .= '<img id="#newCover" src="' . OMImage::readFileName($magazine[0]->image_gen,$magazine[0]->image,"c289x289","magazine") .'" alt="">';
								$view_cache .= '</div>';
								$view_cache .= '<div class="cover-text-detail">';
									$view_cache .= '<h1 class="hidden-xs FLighter">Magazine</h1>';
									$view_cache .= '<h4 class="hidden-xs FThin">นิตยสารที่รวบรวมข้อมูล เคล็ดลับการเลี้ยงดูและการส่งเสริม <br>พัฒนาการลูกน้อย</h4>';
									$view_cache .= '<div class="num-mag">No.' . $magazine[0]->book_number . '</div>';
									$view_cache .= '<div class="month-mag">' . $magazine[0]->title . '</div>';
									$view_cache .= '<a target="_blank" class="c_load" data-id="'.$magazine[0]->magazine_id.'" href="' . OMImage::readFileName($magazine[0]->file_pdf_gen,$magazine[0]->file_pdf,'o0x0','magazine') . '"><div class="load-mag">ดาวน์โหลดฟรี !</div></a>';
								$view_cache .= '<div class="panel-view-download"><div class="list-body-logo" id="m_view_'.$magazine[0]->magazine_id.'"></div><div class="num-view">Download: '.(isset($view_id[$magazine[0]->magazine_id]) ? '[[view_magazine_'.$magazine[0]->magazine_id.']]' : "0").'</div></div>';
								$view_cache .= '</div>';
								$view_cache .= '<div class="clearfix"></div>';
							$view_cache .= '</div>';
						$view_cache .= '</div>';
					}
					if(isset($magazine[1])){
						$view_cache .= '<div class="mag-list-panel">';
						$count = 0;
						$year = '';
						foreach($magazine as $key => $data){
							if($key == 0){
								continue;
							}

							$count++;
							$plusYear = 0;
							if(App::getLocale() == 'th'){
								$plusYear = 543;
							}
							$yearDb = date('Y', strtotime($data->create_date))+$plusYear;
							if (isset($magazine[$key+1])) {
							    $nextDataYear = date('Y', strtotime($magazine[$key+1]->create_date))+$plusYear;
							} else {
							    $nextDataYear = "";
							}
							if($year != $yearDb){
								$year = $yearDb;
								$yearDiff = true;
							}else{
								$yearDiff = false;
							}
							if($count == 1){
								$count = 1;
								$view_cache .= '<div class="mag-year">' . $year . '</div>';
								$view_cache .= '<div class="mg-bg">';
								$view_cache .= '<div class="mg-bg-inner">';
							}
								$view_cache .= ($count == 1 || $count == 3? '<div class="mag-book-content"><div class="mag-book-content-inner">' : '');
									$view_cache .= '<div class="mag-book '. ($count == 2 ? 'mag-book-center-left': '' . ' ') . ($count == 3 ? 'mag-book-center-right': '') . '">';
										$view_cache .= '<div class="mag-cover">';
											$view_cache .= '<a target="_blank" class="c_load" data-id="'.$data->magazine_id.'" href="' . OMImage::readFileName($data->file_pdf_gen,$data->file_pdf,'o0x0','magazine') . '">';
												$view_cache .= '<img src="' . OMImage::readFileName($data->image_gen,$data->image,'w175x289','magazine') . '" class="img-responsive" alt="">';
												$view_cache .= '<div class="num-mag FXregular">No.' . $data->book_number . '</div>';
												// $view_cache .= '<div id="m_view_'.$data->magazine_id.'" class="panel-view">[[view_magazine_'.$data->magazine_id.']]</div>';
												$view_cache .= '<div class="month-mag FXregular">' . $data->title . '</div>';
												$view_cache .= '<div class="panel-view-download"><div class="list-body-logo" id="m_view_'.$data->magazine_id.'"></div><div class="num-view">Download: '.(isset($view_id[$data->magazine_id]) ? '[[view_magazine_'.$data->magazine_id.']]' : "0").'</div></div>';
											$view_cache .= '</a>';
										$view_cache .= '</div>';
									$view_cache .= '</div>';
								$view_cache .=  ($count%2 == 0 || count($magazine) == ($key+1) || ($nextDataYear != $yearDb)? '</div></div>' : '');
							if($count == 4 || count($magazine) == ($key+1) || ($nextDataYear != $yearDb)){
								$view_cache .= '</div>';
								$view_cache .= '</div>';
								$view_cache .= '<div class="clearfix"></div>';
								$count = 0;
							}

						}
						$view_cache .= '</div>';
					}
				$view_cache .= '</div>';
			}else{
				$view_cache .= '<div style="text-align: center; margin: 50px auto;">' . trans('core.DATA_NOT_FOUND') . '</div>';
			}
			$dataCache["magazine"] = $view_cache;
			$view_cache = (string)view('media.emagazine.index', $dataCache);
			Cache::put($cache_name, $view_cache, 10);
		}
		//get viewdata
		// set view data
		// ViewSession::set("view_".id,"1234");

		ViewSession::viewSessionGlobal();
        $view_cache = ViewSession::make($view_cache);
		return $view_cache;
	}
	public function redirect_page($id)
	{
		$ds = new Magazine();
		$data = $ds->getSingleMagazine($id);
		$link = \App\Core\OMImage::readFileName($data[0]->file_pdf_gen,$data[0]->file_pdf,'o0x0','magazine');
		// var_dump($link);
		// exit();
		return redirect($link);
	}


}
