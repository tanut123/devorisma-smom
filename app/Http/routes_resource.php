<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/* ------------ If you add this route you should add "App\RouteServiceProvider.php" in section Http/routes_resource.php ------------ */
Route::get('/', 'WelcomeController@index');

// Crontab allview
Route::get('/cron-page-views/allview', 'CronPageViewsController@allViews');

//The Power of Sleeping
Route::get('/thepowerofsleeping', 'PowerofSleepingController@index');

// Stocks
Route::get('/stocks/{module}/{mode}{width}x{height}/{hash1}/{hash2}/{uid}/{original_name}', 'StocksController@read')->where(['module' => '\w+','mode' => '\w{1}','width' => '(\d+)', 'height' => '(\d+)', 'hash1' => '(\w{2})', 'hash2' => '(\w{2})','uid' => '\w{8,}','original_name'=>'^.*\.\w{3,}']);

// Compress
Route::get('/{type}/{data}', 'CompressCotroller@index')->where('type',"(css|js)")->where('data',".+");

// QA
// Route::get('/หมอเด็ก', 'QuestionArticleController@index');
// Route::get('/หมอเด็ก/{qa_type}', 'QuestionArticleController@listData')->where(['qa_type' => '1|2|3']);
// Route::get('หมอเด็ก/{qa_type}/{slug}', 'QuestionArticleController@listDetail')->where(['qa_type' => '1|2|3','slug' => '^.*']);

// TriggerSetting
Route::get('/setting', 'TriggerController@manage');

// Translate
Route::get('/translate', 'TranslateController@manage');

// Download
Route::get('/download_media/{media}/{gen}/{file_name}', 'DownloadController@manage');

// Custom Css
Route::get('/custom_css/{filename_str}', 'CustomCssController@getCssForPage')->where(['filename_str' => '^(.*).css$']);
Route::get('/custom_css_wcm/{gen}', 'CustomCssController@getCssForWCM');


Route::get('/captcha/{date}', 'CaptchaImageController@genChaptcha');

/*Route::get('/product_css/{filename_str}', 'ProductCssController@getCssForPage');
Route::get('/product_css_wcm/{gen}', 'ProductCssController@getCssForWCM');*/

// XML GENERATE
Route::get('/xml-sitemap/{type}', 'XMLSitemapController@manage')->where("type","all|static|momtip|knowledge|promotion|index");
Route::get('/xml-sitemap/remove/{type}', 'XMLSitemapController@remove')->where("type","all|static|momtip|knowledge|promotion|index");

// Route::controllers([
// 	'auth' => 'Auth\AuthController',
// 	'password' => 'Auth\PasswordController',
// ]);
//Cache Control
Route::get('/clear_cache','ClearCacheController@clear');
Route::get('/img_optim','IMGOptimController@index');

// School book
Route::get('/th/school_book', 'SchoolBookController@index');
Route::post('/th/school_book/{type}', 'SchoolBookController@schoolBookAjax')->where(['type' => 'game|diy|story|music']);
Route::get('/th/school_book/{type}/{id}', 'SchoolBookController@redirectPage')->where(['type' => 'game|diy|story|music']);

// 1000-days
// Route::get('/th/1000-days', 'GrowupController@index');
// 1000-days Parent
// Route::get('/th/1000-days/{page}', 'GrowupController@subPage')->where(['page' => 'first-270-days|second-365-days|final-365-days']);
// 1000-days Child
// Route::get('/th/1000-days/{page}/{sub_page}', 'GrowupController@subPage')->where(['page' => 'first-270-days|second-365-days|final-365-days', 'sub_page' => 'day-1-90|day-91-180|day-181-270|day-271-453|day-454-544|day-545-635|day-636-757|day-758-878|day-879-1000']);

// Sphingomyelin
Route::get('/สฟิงโกไมอีลิน', 'GrowupController@sphingomyelin');
Route::get('/สฟิงโกไมอีลิน/{page}', 'GrowupController@sphingomyelinPage')->where(['page' => 'การทำงานของสมอง|นมแม่|ความสำคัญของ1000วันแรก']);
Route::get('/สฟิงโกไมอีลิน/{page}/{sub_page}', 'GrowupController@sphingomyelinPage')->where(['page' => 'ความสำคัญของ1000วันแรก|การทำงานของสมอง', 'sub_page' => '270วัน|635วัน|1000วัน|ผ่าสมองไอน์สไตน์|เจาะความลับของสมอง|พัฒนาการสมองกับความฉลาด|วัยเพื่อการพัฒนาสมอง']);

Route::get('/สฟิงโกไมอีลิน/{page}/{sub_page}/{detail}', 'GrowupController@sphingomyelinPage')->where(['page' => 'ความสำคัญของ1000วันแรก', 'sub_page' => '1000วัน', 'detail' => 'ผ่าสมองไอน์สไตน์|เจาะความลับของสมอง|พัฒนาการสมองกับความฉลาด|วัยเพื่อการพัฒนาสมอง']);

// ------------------------------------- Media

// Video
Route::get('/th/video', 'VideoController@index');
//Emagazine
Route::get('/th/emagazine', 'EmagazineController@index');
Route::get('/th/emagazine/{id}', 'EmagazineController@redirect_page');
// -------------------------------------

//Product
//Route::get('/th/product/{type}', 'ProductController@index')->where(['type' => 'mom_gold|progress_gold|pe_gold|progress']);
Route::get('/th/product/{type}', 'ProductController@index')->where(['type' => 'mom_gold|promama|gold_progress|pe_gold|progress|progress_gold']);

// Mommy survey
Route::get('/mommysurvey', 'MommysurveyController@index');
Route::post('/mommysurveycmd', 'MommysurveyController@indexSurvey');


//App
Route::get('/th/app', 'AppController@index');

//Promotion
Route::get('/th/promotion', 'PromotionController@index');
Route::post('/th/promotion/loadpage/', 'PromotionController@loadAjax');
Route::get('/th/promotion/{slug}', 'PromotionController@listDetail')->where(['slug' => '^.*']);
// Member
Route::get('/th/member/register', 'MemberController@register')->where(['type' => 'register']);
Route::post("/th/member/cmd","MemberController@cmd");
//Important-notice
Route::get('/th/important-notice', 'ImportantnoticeController@index');
//Important-notice
Route::get('/th/day-and-night', 'DayandNightController@index');
//Live Chat
Route::get('/th/live-chat', 'LiveChatController@mainpage');
Route::get('/th/live-chat-demo', 'LiveChatController@index');

// Register
// Route::get('/th/register', 'RegisterController@index');
Route::get('/th/register/form/{type}', 'RegisterController@form')->where(['type' => 'add|profile']);
Route::get('/th/register/password_reset', 'RegisterController@reset_pass');
Route::post('/th/register/register', 'RegisterController@register');
Route::get('/th/register/send_mail_register', 'RegisterController@send_mail_register');
Route::get('/th/register/policy', 'RegisterController@policy');

//MEMBER WEB
Route::get("/th/bookmark","MemberWebController@bookmark");
Route::post("/th/bookmark/list",'MemberWebController@bookmarkList');
Route::post("/th/bookmark/remove",'MemberWebController@removeBookmark');
Route::post("/th/bookmark/add","MemberWebController@addBookmark");
Route::get("/th/verify/{token}","MemberWebController@verify");
Route::get('/th/password/expire', 'MemberWebController@reset_pass');
Route::post('/th/password/change', 'MemberWebController@change_pass');
Route::get('/th/password/forgot', 'MemberWebController@forget_password');
Route::post('/th/password/check_reset_request', 'MemberWebController@send_request_reset_pass');
Route::get('/th/password/{type}/{token}', 'MemberWebController@password_reset')->where(['type' => 'reset|create']);;
Route::get('/th/favorite', 'MemberWebController@favorite');
Route::post('/th/favorite/add', 'MemberWebController@addFavorite');
Route::post('/th/favorite/remove', 'MemberWebController@removeFavorite');

Route::post("/th/sign_in/login",'MemberWebAuthen@sign_in');
Route::post("/th/sign_in/facebook",'MemberWebAuthen@signInFacebook');
Route::post("/th/sign_in/logout",'MemberWebAuthen@sign_out');

// Wyeth
Route::get('/wyethnutrition', 'WyethController@index');
Route::get('/wyethnutrition-legacy', 'WyethController@legacy');
Route::get('/wyethnutrition/{page}', 'WyethController@detail')->where(['page' => 'detail1|detail2|detail3|detail4']);
Route::get('/wyethnutrition-legacy/{product}', 'WyethController@legacy')->where(['product' => 'tops']);

//app route
Route::get('/app/diary/{uuname}', 'AppDiaryController@index');
Route::get('/m/chk', 'MemberWebAuthen@checkUser');

//syntom
Route::get('/syntom/{slug}', 'SyntomController@Detail')->where(['slug' => '^.*']);

//activity
Route::get('/activity/{slug}', 'ActivityController@Detail')->where(['slug' => '^.*']);

// brain ignition
Route::post('/certificate_save', 'BrainIgnitionController@certificateSave');
Route::get('/certificate_share/{id}', 'BrainIgnitionController@certificateShare');

Route::get('/พรสวรรค์สร้างได้ใน1000วันแรก', 'BrainIgnitionController@index');
Route::post('/พรสวรรค์สร้างได้ใน1000วันแรก/loadpage', 'BrainIgnitionController@loadAjax');
Route::get('/พรสวรรค์สร้างได้ใน1000วันแรก/ใบรับรอง/{mode}/{level}','BrainIgnitionController@certificate')->where(['mode' => 'ภาษา|ศิลปะ|ดนตรี|คณิตศาสตร์|วิทยาศาสตร์|ความเป็นผู้นำ','level' => '^.*']);
Route::get('/พรสวรรค์สร้างได้ใน1000วันแรก/แบบวัดแวว', 'BrainIgnitionController@listQA');
Route::get('/พรสวรรค์สร้างได้ใน1000วันแรก/แบบวัดแวว/{mode}', 'BrainIgnitionController@question')->where(['mode' => 'ภาษา|ศิลปะ|ดนตรี|คณิตศาสตร์|วิทยาศาสตร์|ความเป็นผู้นำ']);
Route::get('/พรสวรรค์สร้างได้ใน1000วันแรก/{slug}', 'BrainIgnitionController@detail')->where(['slug' => '^.*']);

// online shopping
Route::get('/online_shopping', 'OnlineShoppingController@index');


// Join S-mon Club
Route::get('/th/recruitment', 'JoinsmonClubController@index');

//reqruitment
Route::post('/recruitment/register', 'RecruitmentAPIController@register');
Route::get('/recruitment/list/{type}', 'RecruitmentAPIController@listData')->where(['type' => 'province|district|subdistrict|mom_product|child_product|place|occupation|reason']);


// everyonecanbethestar

Route::get('/everyonecanbethestar', 'EveryonecanbethestarController@index');

Route::get('/สมองดีเรียนรู้ไว', 'SmartBrainController@index');
Route::get('/สมองดีเรียนรู้ไว/พรสวรรค์ในแบบของเค้า', 'SmartBrainController@show');

Route::get('/S26GoldProgressUHT', 'S26ProgressController@index');


Route::get('/info', function(){
    phpinfo();   
});
