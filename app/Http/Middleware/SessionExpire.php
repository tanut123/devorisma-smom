<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class SessionExpire {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(Session::has("memberExpireTime")){
			if(Session::get("memberExpireTime") > (time() - 1200)){
				if($_SERVER['REDIRECT_URL'] != "/m/chk"){
					Session::set("memberExpireTime",time());
				}
			}else{
				Session::forget("webMemberId");
				Session::forget("webMemberName");
				Session::forget("webMemberLastLogin");
				Session::forget("memberExpireTime");
			}
		}
        return $next($request);
	}

}
