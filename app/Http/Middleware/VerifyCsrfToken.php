<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Symfony\Component\HttpFoundation\Cookie;
use Illuminate\Session\TokenMismatchException;

class VerifyCsrfToken extends BaseVerifier {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if($request->path() == '/' &&  $request->method()!='GET') {
			abort(405);
		}

		try {
			return  parent::handle($request, $next);
		} catch ( TokenMismatchException $e) {
			abort(400);
		}
	}
	protected function addCookieToResponse($request, $response)
	{
		$response->headers->setCookie(
			new Cookie('XSRF-TOKEN', $request->session()->token(), -1, '/', null, true, true)
		);

		return $response;
	}

}
