<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Home
Route::get('/home', 'HomeController@index');

//momtip
Route::get('/{type}', 'MomtipController@listData')->where(['type' => 'pregnancy-mom|lactating-mom|toddler-mom']);
Route::get('/{type}/{slug}', 'MomtipController@detail')->where(['type' => 'pregnancy-mom|lactating-mom|toddler-mom','slug' => '^.*']);
Route::post('/momtip/list/','MomtipController@listAjax');
Route::post('/momtip/rating/','MomtipController@rating');
//Custom page
Route::get('/page/{slug}','CustomPageController@pageData')->where(['slug' => '^.*']);
//Knowledge
Route::get('/knowledge/detail/{slug}', 'KnowledgeController@listDetailOld')->where(['slug' => '^.*']);
Route::get('/knowledge/{slug}', 'KnowledgeController@listDetailOld')->where(['slug' => '^.*']);
Route::post('/knowledge/rating/','KnowledgeController@rating');
Route::get('/{know_type}', 'KnowledgeController@index')->where(['know_type' => 'pickyeater|pregnancy|toddler']);
Route::get('/{know_type}/{know_sub_type}', 'KnowledgeController@listIndex')->where(['know_type' => 'pickyeater|pregnancy|toddler','know_sub_type' => '1|2|3|4']);
Route::post('/{know_type}/{know_sub_type}/loadpage', 'KnowledgeController@loadAjax')->where(['know_type' => 'pickyeater|pregnancy|toddler','know_sub_type' => '1|2|3|4']);
Route::get('/{know_type}/{know_sub_type}/{slug}', 'KnowledgeController@listDetail')->where(['know_type' => 'pickyeater|pregnancy|toddler','know_sub_type' => '1|2|3|4','slug' => '^.*']);
// Knowledge no sub-type
Route::get('/{know_type_no_sub}', 'KnowledgeController@listIndexNoSub')->where(['know_type_no_sub' => 'braindevelopment|lactating|ลูกน้อยถ่ายสบาย']);
Route::get('/{know_type_no_sub}/{slug}', 'KnowledgeController@listDetailNoSub')->where(['know_type_no_sub' => 'braindevelopment|lactating|ลูกน้อยถ่ายสบาย','slug' => '^.*']);
Route::post('/{know_type_no_sub}/loadpage', 'KnowledgeController@loadAjax')->where(['know_type_no_sub' => 'braindevelopment|lactating|ลูกน้อยถ่ายสบาย']);

// QA
// Route::post('/qa/rating/','QuestionArticleController@rating');
// Route::post('qa/{qa_type}/loadpage', 'QuestionArticleController@loadAjax')->where(['qa_type' => '1|2|3']);

// MOMMYGADGET
Route::get('/mommygadget', 'MommygadgetController@index');
Route::get('/mommygadget/{slug}', 'MommygadgetController@Detail')->where(['slug' => '^.*']);
Route::post('/mommygadget/loadpage', 'MommygadgetController@loadAjax');
Route::post('/mommygadget/rating', 'MommygadgetController@rating');

//Team
Route::get('/team', 'TeamController@index');
Route::post('/team/person/', 'TeamController@loadAjax');
//Footer Subscribe
Route::post("/footer/subscribe","SubscribeController@saveData");
Route::post("/footer/setMemberSignUpPanel","CookieController@closeOpenMemberSignUp");
Route::post("/footer/setPopupBreastfeeding","CookieController@closeOpenBreastfeeding");

// Cron Page views
// Route::get('/cron-page-views/momtip', 'CronPageViewsController@momtipViews');
// Route::get('/cron-page-views/promotion', 'CronPageViewsController@promotionViews');
// Route::get('/cron-page-views/knowledge', 'CronPageViewsController@knowledgeViews');
Route::get('/cron-clear-media-file/', 'CronClearMediaController@clearMediaFile');

//Search
Route::get('/page-view/{module}/{id}', 'CronPageViewsController@view')->where(['module' => 'trick_mom|knowledge|promotion|static|question_article|mommygadget|syntom|activity|brain_ignite_tips']);
Route::get('/page-view-magazine/{id}', 'CronPageViewsController@viewMagazine');
Route::get('/search/', 'SearchController@searchViews');
Route::post('/search/list', 'SearchController@listData');

//TestMail
// Route::get("/testmail",'TestMailController@index');
// Route::get("/testverify",'RegisterController@genVerify');
Route::get("/test_pass_expire",'TestModuleController@password_expire');
Route::post("/test_pass_expire/set",'TestModuleController@password_expire_set');
// Route::get("/test_captcha",'TestModuleController@test_captcha');