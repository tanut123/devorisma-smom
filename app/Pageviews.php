<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use Crypto;
use App\Core;
// use App\Core\SettingManagement;
use Log;


class Pageviews extends Model {

	public function getSetting(){
		$settingData = new SiteSetting();
		$setting = $settingData->getSetting();
		$settinGA = array();

		if($setting["GOOGLE_ANALYTIC_CLIENT_ID"] != ""){
			$settinGA["GOOGLE_ANALYTIC_CLIENT_ID"] = $setting["GOOGLE_ANALYTIC_CLIENT_ID"];
		}
		if($setting["GOOGLE_ANALYTIC_EMAIL"] != ""){
			$settinGA["GOOGLE_ANALYTIC_EMAIL"] = $setting["GOOGLE_ANALYTIC_EMAIL"];
		}
		if($setting["GOOGLE_ANALYTIC_ACCOUNT_ID"] != ""){
			$settinGA["GOOGLE_ANALYTIC_ACCOUNT_ID"] = $setting["GOOGLE_ANALYTIC_ACCOUNT_ID"];
		}

		return $settinGA;
	}

	public function gaConnect(){

		$setting = $this->getSetting();
	    // From the APIs console
    	$client_id = $setting["GOOGLE_ANALYTIC_CLIENT_ID"];
	    $email = $setting["GOOGLE_ANALYTIC_EMAIL"];
	    $pathKeyP12 = storage_path() . '/google_key/private_key.p12';
	    $account_id = 'ga:'. $setting["GOOGLE_ANALYTIC_ACCOUNT_ID"];

	    Log::info('Configs: ', ["client_id" => $client_id, "email" => $email, "account_id" => $account_id, "pathKeyP12" => is_file($pathKeyP12) ]);

	    $ga = new Core\GoogleAnalyticsAPI('service');
		$ga->auth->setClientId($client_id); // From the APIs console
		$ga->auth->setEmail($email); // From the APIs console
		$ga->auth->setPrivateKey($pathKeyP12);


	    $auth = $ga->auth->getAccessToken();

		// Try to get the AccessToken
		$accessToken = "";
		if ($auth['http_code'] == 200) {
		    $accessToken = $auth['access_token'];
		    $tokenExpires = $auth['expires_in'];
		    $tokenCreated = time();
	    	Log::info('Access token success: ' . $accessToken . ':: Expires in: ' . $auth['expires_in']);
		} else {
			Log::warning('Cannot access token: ', $auth);
		}

		if($accessToken == ""){
			return $accessToken;
		}


	    $ga->setAccessToken($accessToken);
	    $ga->setAccountId($account_id);

	  	return $ga;
	}

	public function allViews(){
		$ga = $this->gaConnect();

		$defaults = array(
			'metrics' => 'ga:pageviews,ga:uniquePageviews',
			'start-date' => '2015-01-01',
    		'end-date' => date('Y-m-d'),
			'dimentions' => 'ga:pagePath'
		);
	    $data_view = $ga->getContentStatistics($defaults);

	    if(isset($data_view["totalsForAllResults"]["ga:pageviews"]) && $data_view["totalsForAllResults"]["ga:pageviews"] > 0){

			$dataUpdate["last_update"] = date("Y-m-d H:i:s");
			$dataUpdate["total"] = $data_view["totalsForAllResults"]["ga:pageviews"];
			$data["module"] = "site";
			$data["ref_id"] = "0";
			$data["obj_lang"] =  "MAS";
			$page_view = DB::table("page_view")->where($data);

			Log::info('Total Views: ' . $dataUpdate["total"]);
			if($page_view->count() > 0){
				$page_view->update($dataUpdate);
				echo "Update Complete";
			}else{
				$data["last_update"] = $dataUpdate["last_update"];
				$data["total"] = $dataUpdate["total"];
				$page_view->insert($data);
				echo "Insert Complete";
			}
			exit();
		}else{
			Log::info('Views Not found:');
		}
		exit();
	}

	public function gaGetView($ga, $path){
		$defaults = array(
			'metrics' => 'ga:pageviews,ga:uniquePageviews',
			'dimentions' => 'ga:pagePath',
		    'filters' => 'ga:pagePath=@'. $path,
		    'start-date' => '2015-01-01',
    		'end-date' => date('Y-m-d')
		);
	    return $ga->getContentStatistics($defaults);
	}


	public function getTrickMomListData(){
		$dt = new Momtip();
		$datamomtip = $dt->getTrickMomListAll();
		return $datamomtip;
	}

	public function updateTrickMomDataViews($trick_mom_id, $obj_lang, $view){
		$dt = new Momtip();
		$dt->updateTrickMomViews($trick_mom_id, $obj_lang, $view);
		return $dt;
	}

	public function momtipViews(){
		$ga = $this->gaConnect();

		$datamomtip = $this->getTrickMomListData();
		$momtipCount = count($datamomtip);
		Log::debug('momtip Views start update count: '. $momtipCount);

		$counter = 0;
		if($momtipCount > 0){
			foreach ($datamomtip as $key => $val) {
				$obj_lang = $val->obj_lang;
				$trick_mom_id = $val->trick_mom_id;
				$slug = $val->slug;
				$type_mom = $val->type_mom;
				$lang = $this->getLang($obj_lang);

				$type_mom_data = str_replace([1,2,3],["pregnancy-mom","lactating-mom","toddler-mom"],$type_mom);

				$path =  $lang . "/" . $type_mom_data . "/" . $slug;
			    $visitsView = $this->gaGetView($ga, $path);

			    if(isset($visitsView["totalsForAllResults"]["ga:pageviews"])){
				    $dt = $this->updateTrickMomDataViews($trick_mom_id, $obj_lang, $visitsView["totalsForAllResults"]["ga:pageviews"]);
				    if(count($dt) > 0){
			    		$counter++;
				    	Log::debug('Row update: '. $path . ' :: Views :: '.$visitsView["totalsForAllResults"]["ga:pageviews"]);
				    }else{
				    	Log::warning('Cannot update: '. $path);
				    }
			    }else{
					Log::warning('Cannot Get ga:pageviews: '. $path);
				}
			}
			return $counter;
		}else{
			Log::debug('momtip Views No data');
			return 0;
		}
		Log::debug('momtip Views End');
		return 0;
	}

	public function getPromotionListData(){
		$dt = new Promotion();
		$dataPromotion = $dt->loadListAll();
		return $dataPromotion;
	}

	public function updatePromotionDataViews($id, $obj_lang, $view){
		$dt = new Promotion();
		$dt->updatePromotionViews($id, $obj_lang, $view);
		return $dt;
	}

	public function promotionViews(){
		$ga = $this->gaConnect();

		$dataPromotion = $this->getPromotionListData();
		$promotionCount = count($dataPromotion);
		Log::debug('Promotion Views start update count: '. $promotionCount);

		$counter = 0;
		if($promotionCount > 0){
			foreach ($dataPromotion as $key => $val) {
				$obj_lang = $val->obj_lang;
				$promotion_id = $val->promotion_id;
				$slug = $val->slug;
				$lang = $this->getLang($obj_lang);

				$path =  $lang . "/promotion/" . $slug;
			    $visitsView = $this->gaGetView($ga, $path);

			    if(isset($visitsView["totalsForAllResults"]["ga:pageviews"])){
				    $dt = $this->updatePromotionDataViews($promotion_id, $obj_lang, $visitsView["totalsForAllResults"]["ga:pageviews"]);
				    if(count($dt) > 0){
				    	$counter++;
				    	Log::debug('Row update: '. $path . ' :: Views :: '.$visitsView["totalsForAllResults"]["ga:pageviews"]);
				    }else{
				    	Log::warning('Cannot update: '. $path);
				    }
			    }else{
			    	Log::warning('Cannot Get ga:pageviews: '. $path);
			    }
			}
			return $counter;
		}else{
			Log::debug('Promotion Views No data');
			return 0;
		}
		Log::debug('Promotion Views End');
		return 0;
	}

	public function getKnowledgeListData(){
		$dt = new Knowledge();
		$dataKnowledge = $dt->loadListAll();
		return $dataKnowledge;
	}

	public function updateKnowledgeDataViews($id, $obj_lang, $view){
		$dt = new Knowledge();
		$dt->updateKnowledgeViews($id, $obj_lang, $view);
		return $dt;
	}

	public function knowledgeViews(){
		$ga = $this->gaConnect();

		$dt = new Knowledge();
		$dataKnowledge = $this->getKnowledgeListData();
		$knowledgeCount = count($dataKnowledge);
		Log::debug('Knowledge Views start update count: '. $knowledgeCount);

		$counter = 0;
		if($knowledgeCount > 0){
			foreach ($dataKnowledge as $key => $val) {
				$obj_lang = $val->obj_lang;
				$knowledge_id = $val->knowledge_list_id;
				$slug = $val->slug;
				$lang = $this->getLang($obj_lang);

				$path =  $lang . "/knowledge/" . $slug;
			    $visitsView = $this->gaGetView($ga, $path);

			    if(isset($visitsView["totalsForAllResults"]["ga:pageviews"])){
				    $dt = $this->updateKnowledgeDataViews($knowledge_id, $obj_lang, $visitsView["totalsForAllResults"]["ga:pageviews"]);
				    if(count($dt) > 0){
				    	$counter++;
				    	Log::debug('Row update: '. $path . ' :: Views :: '.$visitsView["totalsForAllResults"]["ga:pageviews"]);
				    }else{
				    	Log::warning('Cannot update: '. $path);
				    }
				}else{
					Log::warning('Cannot Get ga:pageviews: '. $path);
				}
			}
			return $counter;
		}else{
			Log::debug('Knowledge Views No data');
			return 0;
		}
		Log::debug('Knowledge Views End');
		return 0;
	}

	public function getLang($lang){
		$langUrl = "th";
		if($lang == "ENG"){
			$langUrl = "en";
		}
		return $langUrl;
	}

	public function view($module,$id) {
		$dataUpdate["last_update"] = date("Y-m-d H:i:s");
		$data["module"] = $module;
		$data["ref_id"] = $id;
		$data["obj_lang"] =  Lng::db();
		$page_view = DB::table("page_view")->where($data);
		if($page_view->count()){
			$page_view->increment('total' ,1,$dataUpdate);
		}else{
			$data["last_update"] = $dataUpdate["last_update"];
			$data["total"] = 1;
			$page_view->insert($data);
		}
		$total =$page_view->select("total")->get()[0];
		return number_format($total->total);
	}

	public function get1000DaysView($module) {
		$sql = "SELECT * FROM page_view WHERE module='".$module."' AND obj_lang='".Lng::db()."' ORDER BY ref_id ASC";
		$dt = DB::select($sql);
		return $dt;
	}

	public function getViewMagazine($module)
	{
		$sql = "SELECT * FROM page_view WHERE module='".$module."' AND obj_lang='".Lng::db()."' ORDER BY ref_id ASC";
		$dt = DB::select($sql);
		return $dt;
	}
	public function getMomTipsView($id_list) {
		$sql = "SELECT * FROM page_view WHERE module='trick_mom' AND ref_id in (".$id_list.") AND obj_lang='".Lng::db()."' ORDER BY ref_id ASC";
		$dt = DB::select($sql);
		return $dt;
	}
}
