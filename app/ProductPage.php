<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;

class ProductPage extends Model {

	protected $table = '';

	function getProductPage($slug){
		$dt = DB::table('product')->select('custom_page_id', 'title', 'slug', 'css_file','css_file_gen', 'detail')->where('slug', trim($slug))->where('obj_lang', Lng::db())->get();
		return $dt;
	}

    function loadProductCss($gen) {
        $dt = DB::table('product_media_file')->select()->where('uuname',$gen)->get();
        return $dt;
    }

    function getCustomData(){
    	$dt = DB::select("SELECT cl.* FROM custom_link_product as cl
    	                 LEFT JOIN custom_link_product_draft as cld
    	                 ON cl.custom_link_product_id = cld.custom_link_product_id
    	                 WHERE cld.obj_status = 'active'
    	                 ORDER BY cld.obj_created_date DESC ");
    	return $dt;
    }

    function getMomTipList($product){
    	$table = "trick_mom";
		$sql = "SELECT trick_mom_id, title, description, image, image_gen, type_mom, file_mp4, youtube_source, article_date,slug,pv.total AS view FROM ".$table." as tm LEFT JOIN page_view AS pv ON pv.ref_id=tm.trick_mom_id WHERE tm.obj_lang = '".Lng::db()."' AND article_date < '".date("Y-m-d H:i:s")."' AND pv.obj_lang = '".Lng::db()."' AND product = '".$product."' ORDER BY sort_order DESC ";
		$dt = DB::select($sql);
		return $dt;
    }

}
