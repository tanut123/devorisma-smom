<?php namespace App\Extensions;

use Config;

class BladeExtensions {

    public static function register()
    {

        \Blade::extend(function($view)
        {
            return preg_replace('/\@readFileName\((.+)\)/U', '<?php echo \App\Core\OMImage::readFileName(${1}); ?>', $view);
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@replaceStockUrl\((.+)\)/U', '<?php echo \App\Core\OM::replaceStockUrl(${1}); ?>', $view);
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@compressCss\((.+)\)/', '<?php echo \App\Core\Compress::read_css(${1}); ?>', $view);
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@compressJs\((.+)\)/', '<?php echo \App\Core\Compress::read_js(${1}); ?>', $view);
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@writeCss\(\)/U', '<?php echo \App\Core\Compress::write_css(); ?>', $view);
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@writeJs\((.*)\)/U', '<?php echo \App\Core\Compress::write_js(${1}); ?>', $view);
            /*            return preg_replace('/\@writeJs\(\)/', '<?php echo \App\Core\Compress::write_js(); ?>', $view);*/
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@genFile\((.*)\)/U', '<?php echo \App\Core\Compress::genFile(${1}); ?>', $view);
            /*            return preg_replace('/\@writeJs\(\)/', '<?php echo \App\Core\Compress::write_js(); ?>', $view);*/
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@trimWithDot\((.+)\)/', '<?php echo \App\Core\OM::trimWithDot(${1}); ?>', $view);
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@dateLang\((.+)\)/', '<?php echo \App\Core\DateLang::formatDateLang(${1}); ?>', $view);
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@loadJs\((.+)\)/', '<?php echo \App\Core\SmallJsPrint::loadJs(${1}); ?>', $view);
        });

        \Blade::extend(function($view)
        {
            return preg_replace('/\@loadCss\(\)/', '<?php echo \App\Core\Compress::instantLoad(); ?>', $view);
        });

    }

}