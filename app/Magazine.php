<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;

class Magazine extends Model {

	protected $table = '';

	function getMagazine(){
		$dt = DB::table('magazine')->select('magazine_id','title_'.App::getLocale().' as title', 'book_number','image','image_gen','file_pdf','file_pdf_gen','create_date')->where("create_date", "<", date("Y-m-d H:i:s"))->orderBy("create_date","DESC")->get();
		// $dt = DB::select("SELECT a.magazine_id, a.title_".Lng::db(true)." AS title, a.book_number, a.image, a.image_gen, a.file_pdf, a.file_pdf_gen, a.create_date, b.ref_id, b.total FROM magazine AS a LEFT JOIN page_view AS b ON a.magazine_id = b.ref_id ORDER BY a.create_date DESC");
		return $dt;
	}
	function getSingleMagazine($id){
		$dt = DB::table('magazine')->select('magazine_id','title_'.App::getLocale().' as title', 'book_number','image','image_gen','file_pdf','file_pdf_gen','create_date')->where("magazine_id",$id)->get();
		return $dt;
	}

}
