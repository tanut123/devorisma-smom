<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;

class Home extends Model {

	protected $table = '';
	protected $table_category = '';

	function getHomeBanner(){
		$date = date("Y-m-d H:i:s");
		$dt = DB::table('home_banner')->select('title_th AS title','description_th AS description','url_th AS url','home_banner_id','file_mp4_gen','file_mp4','youtube_source','image_mobile_gen','image_mobile','image_gen','image','duration')->whereRaw("'$date' between (start_date) and (end_date) and obj_lang = '" . Lng::db() . "' ")->orderBy("priority","ASC")->get();
		return $dt;
	}

	function getChildSubCat(){
		$table = "sub_category_child";
		$dt = DB::select("SELECT sub_category_child_id, sub_category_text_th, sub_category_text_en FROM ".$table." ");
		return $dt;
	}

	function getTrickMomList($type){
		$where = "";
		$sort = "ORDER BY sort_order DESC limit 5";
		if($type == "pregnancy"){
			$where = " type_mom = 1 ";
		}elseif ($type == "lactating") {
			$where = " type_mom = 2 ";
		}elseif ($type == "toddler") {
			$where = " type_mom = 3 ";
		}elseif ($type == "all") {
			$where = " sort_order in (SELECT MAX(sort_order) FROM `trick_mom` WHERE type_mom = tm.type_mom AND obj_lang = '".Lng::db()."' GROUP BY type_mom) ";
		}
		$table = "trick_mom";
		$table_category = "trick_mom_subcat";
		$sql = "SELECT trick_mom_id, title, description, image, image_gen, type_mom, file_mp4, youtube_source, article_date, slug,title_" . App::getLocale() . " AS title_cat,sub_category, pv.total AS view FROM ".$table." as tm LEFT JOIN page_view AS pv ON pv.ref_id=tm.trick_mom_id LEFT JOIN ".$table_category." as tms ON tm.type_mom = tms.category_code WHERE ".$where." AND tm.obj_lang = '".Lng::db()."' AND pv.obj_lang = '".Lng::db()."' AND article_date < '".date("Y-m-d H:i:s")."' ".$sort;
		$dt = DB::select($sql);
		return $dt;
	}

    function getKnowledgeLeftBanner(){
        $table = "knowledge";
        $dt = DB::select("SELECT " .$table. "_id, knowledge_name, image_data, image_data_gen, link_url FROM ". $table . " WHERE section='left' AND obj_lang='".Lng::db()."' ORDER BY priority ASC");
        return $dt;
    }

    function getKnowledgeRight1Banner(){
        $table = "knowledge";
        $dt = DB::select("SELECT " .$table. "_id, knowledge_name, image_data, image_data_gen, link_url FROM ". $table . " WHERE section='right1' AND obj_lang='".Lng::db()."' ORDER BY priority ASC");
        return $dt;
    }

    function getKnowledgeRight2Banner(){
        $table = "knowledge";
        $dt = DB::select("SELECT " .$table. "_id, knowledge_name, image_data, image_data_gen, link_url FROM ". $table . " WHERE section='right2' AND obj_lang='".Lng::db()."' ORDER BY priority ASC");
        return $dt;
    }

    function getKnowledgeRightBBanner(){
        $table = "knowledge";
        $dt = DB::select("SELECT " .$table. "_id, knowledge_name, image_data, image_data_gen, link_url FROM ". $table . " WHERE section='rightb' AND obj_lang='".Lng::db()."' ORDER BY priority ASC");
        return $dt;
    }

    function getKnowledgeList(){
    	$table = "knowledge_list";
    	$sort = "ORDER BY valid_date DESC limit 6";
    	$dt = DB::select(" SELECT * FROM ".$table." WHERE obj_lang='".Lng::db()."' AND valid_date <='".date("Y-m-d H:i:s")."' AND pin_item = 'T' ".$sort);
		return $dt;
    }
}
