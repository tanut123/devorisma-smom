<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use DateTime;
use Crypto;
use App;

class MemberWeb extends Model {

	public $error_message = "";

    function insertMember(&$member_wed_id,$dataMember) {
        $reg_date = date("Y-m-d H:i:s");
        $member_wed_id = $this->generateUUID();
        $data['member_web_id'] = $member_wed_id;
        $data['displayname'] = trim($dataMember["displayname"]);
        $data['member_type'] = trim($dataMember["member_type"]);
        $data['facebook_id'] = (isset($dataMember["facebook_id"]) ? $dataMember["facebook_id"] :"");
        $data['email'] = trim((isset($dataMember["email"]) ? $dataMember["email"] :""));
        $data['password'] = hash('sha256',$dataMember["password"]);
        $data['firstname'] = (isset($dataMember["firstname"]) ? trim($dataMember["firstname"]) :"");
        $data['lastname'] = (isset($dataMember["lastname"]) ? trim($dataMember["lastname"]) :"");
        $data['mobile'] = (isset($dataMember["mobile"]) ? trim($dataMember["mobile"]) :"");
        $data['status_mom'] = (isset($dataMember["status_mom"]) ? $dataMember["status_mom"] :0);
        $data['eating_choice'] = (isset($dataMember["eating"]) ? $dataMember["eating"] :"");
        $data['eating_text'] = (isset($dataMember["eating_text"]) ? $dataMember["eating_text"] :"");
        $data['title'] = (isset($dataMember["title"])) ? $dataMember["title"] : 0;
        $data['gendar'] = (isset($dataMember["gendar"])) ? $dataMember["gendar"] : 0;
        $data['birthday'] = (isset($dataMember["birthday"]) && $dataMember["birthday"] != "")? $this->changeDateFormat($dataMember["birthday"]): null;
        $data['register_date'] = $reg_date;
        $data['obj_lang'] = "MAS";
        $data['obj_published_date'] = $reg_date;
        $data['obj_published_user_id'] = 0;
        $data['verify_status'] = "false";
        if($dataMember["member_type"] == 2){
        	$data['verify_status'] = "true";
        }
        $data['member_card'] = (isset($dataMember["member_card_register"]) && $dataMember["member_card_register"] == 1)? "T": "F";
        $data['password_update_time'] = date("Y-m-d H:i:s");

        $data_draft['member_web_id'] = $member_wed_id;
		$data_draft['displayname'] = trim($dataMember["displayname"]);
		$data_draft['member_type'] = trim($dataMember["member_type"]);
		$data_draft['facebook_id'] = (isset($dataMember["facebook_id"]) ? $dataMember["facebook_id"] :"");
        $data_draft['email'] = trim((isset($dataMember["email"]) ? $dataMember["email"] :""));
        $data_draft['password'] = hash('sha256',$dataMember["password"]);
        $data_draft['firstname'] = (isset($dataMember["firstname"]) ? trim($dataMember["firstname"]) :"");
        $data_draft['lastname'] = (isset($dataMember["lastname"]) ? trim($dataMember["lastname"]) :"");
        $data_draft['mobile'] = (isset($dataMember["mobile"]) ? trim($dataMember["mobile"]) :"");
        $data_draft['status_mom'] = (isset($dataMember["status_mom"]) ? $dataMember["status_mom"] :0);
        $data_draft['eating_choice'] = (isset($dataMember["eating"]) ? $dataMember["eating"] :"");
        $data_draft['eating_text'] = (isset($dataMember["eating_text"]) ? $dataMember["eating_text"] :"");
        $data_draft['title'] = (isset($dataMember["title"])) ? $dataMember["title"] : 0;
        $data_draft['gendar'] = (isset($dataMember["gendar"])) ? $dataMember["gendar"] : 0;
        $data_draft['birthday'] = (isset($dataMember["birthday"]) && $dataMember["birthday"] != "")? $this->changeDateFormat($dataMember["birthday"]): null;
        $data_draft['register_date'] = $reg_date;
        $data_draft['obj_lang'] = "MAS";
        $data_draft['obj_rev'] = "1";
        $data_draft['obj_status'] = "active";
        $data_draft['obj_state'] = "published";
        $data_draft['obj_created_date'] = $reg_date;
        $data_draft['obj_created_user_id'] = 0;
        $data_draft['obj_modified_date'] = $reg_date;
        $data_draft['obj_modified_user_id'] = 0;
        $data_draft['obj_published_date'] = $reg_date;
        $data_draft['obj_published_user_id'] = 0;
        $data_draft['verify_status'] = "false";
        if($dataMember["member_type"] == 2){
        	$data_draft['verify_status'] = "true";
        }else{
        	$this->keepOldPassword($member_wed_id,$data['password']);
        }
        $data_draft['member_card'] = (isset($dataMember["member_card_register"]) && $dataMember["member_card_register"] == 1)? "T": "F";
        $data_draft['password_update_time'] = date("Y-m-d H:i:s");

        $dtMember = DB::table("member_web")->insert($data);
        $dtMemberDraft = DB::table("member_web_draft")->insert($data_draft);

        $dataMember["member_web_id"] = $member_wed_id;
        $this->insertMemberChildData($dataMember);

        if($dtMember && $dtMemberDraft){
        	return true;
        }
        return false;
    }

     function updateMember($dataMember) {
        $up_date = date("Y-m-d H:i:s");
        $member_wed_id = $dataMember["member_web_id"];
        $dataInsertChild = true;

        if(isset($dataMember["email"]) && $dataMember["email"] != ""){
        	$data['email'] = trim($dataMember["email"]);
        	$data_draft['email'] = trim($dataMember["email"]);
        }

        if(isset($dataMember["password"]) && $dataMember["password"] != ""){
        	$data['password'] = hash('sha256',$dataMember["password"]);
        	$data['password_update_time'] = $up_date;

        	$data_draft['password'] = hash('sha256',$dataMember["password"]);
        	$data_draft['password_update_time'] = $up_date;

        	$this->keepOldPassword($member_wed_id,$data['password']);
        }

        $data['displayname'] = trim($dataMember["displayname"]);
        $data['member_type'] = trim($dataMember["member_type"]);
        $data['firstname'] = trim($dataMember["firstname"]);
        $data['lastname'] = trim($dataMember["lastname"]);
        $data['mobile'] = trim($dataMember["mobile"]);
        $data['status_mom'] = (isset($dataMember["status_mom"]) ? $dataMember["status_mom"] :0);
        $data['eating_choice'] = (isset($dataMember["eating"]) ? $dataMember["eating"] :"");
        $data['eating_text'] = (isset($dataMember["eating_text"]) ? $dataMember["eating_text"] :"");
        if($data['eating_choice'] != "T"){
        	$data['eating_text'] = "";
        }
        $data['title'] = (isset($dataMember["title"])) ? $dataMember["title"] : 0;
        $data['gendar'] = (isset($dataMember["gendar"])) ? $dataMember["gendar"] : 0;
        $data['birthday'] = (isset($dataMember["birthday"]) && $dataMember["birthday"] != "")? $this->changeDateFormat($dataMember["birthday"]): null;
        $data['address_number'] = $dataMember["address_number"];
        $data['address_building'] = $dataMember["address_building"];
        $data['address_floor'] = $dataMember["address_floor"];
        $data['address_room'] = $dataMember["address_room"];
        $data['address_village'] = $dataMember["address_village"];
        $data['address_village_no'] = $dataMember["address_village_no"];
        $data['address_alley'] = $dataMember["address_alley"];
        $data['address_road'] = $dataMember["address_road"];
        $data['address_district'] = $dataMember["address_district"];
        $data['address_sub_district'] = $dataMember["address_sub_district"];
        $data['address_province'] = $dataMember["address_province"];
        $data['address_zip_code'] = $dataMember["address_zip_code"];
        $data['address_phone'] = $dataMember["address_phone"];
        $data['address_mobile'] = $dataMember["address_mobile"];
        $data['address_fax'] = $dataMember["address_fax"];
        $data['office_name'] = $dataMember["office_name"];
        $data['office_building'] = $dataMember["office_building"];
        $data['office_floor'] = $dataMember["office_floor"];
        $data['office_room'] = $dataMember["office_room"];
        $data['office_village'] = $dataMember["office_village"];
        $data['office_village_no'] = $dataMember["office_village_no"];
        $data['office_alley'] = $dataMember["office_alley"];
        $data['office_road'] = $dataMember["office_road"];
        $data['office_district'] = $dataMember["office_district"];
        $data['office_sub_district'] = $dataMember["office_sub_district"];
        $data['office_province'] = $dataMember["office_province"];
        $data['office_zip_code'] = $dataMember["office_zip_code"];
        $data['office_phone'] = $dataMember["office_phone"];
        $data['office_mobile'] = $dataMember["office_mobile"];
        $data['office_fax'] = $dataMember["office_fax"];
        $data['obj_lang'] = "MAS";
        $data['obj_published_date'] = $up_date;
        $data['obj_published_user_id'] = 0;
        $data['member_card'] = ($dataMember["member_card_register"] == 1 || $dataMember["member_card_register"] == 2)? "T": "F";

		$data_draft['displayname'] = trim($dataMember["displayname"]);
		$data_draft['member_type'] = trim($dataMember["member_type"]);
        $data_draft['firstname'] = trim($dataMember["firstname"]);
        $data_draft['lastname'] = trim($dataMember["lastname"]);
        $data_draft['mobile'] = trim($dataMember["mobile"]);
        $data_draft['status_mom'] = (isset($dataMember["status_mom"]) ? $dataMember["status_mom"] :0);
        $data_draft['eating_choice'] = (isset($dataMember["eating"]) ? $dataMember["eating"] :"");
        $data_draft['eating_text'] = (isset($dataMember["eating_text"]) ? $dataMember["eating_text"] :"");
        if($data_draft['eating_choice'] != "T"){
        	$data_draft['eating_text'] = "";
        }
        $data_draft['title'] = (isset($dataMember["title"])) ? $dataMember["title"] : 0;
        $data_draft['gendar'] = (isset($dataMember["gendar"])) ? $dataMember["gendar"] : 0;
        $data_draft['birthday'] = (isset($dataMember["birthday"]) && $dataMember["birthday"] != "")? $this->changeDateFormat($dataMember["birthday"]): null;
        $data_draft['address_number'] = $dataMember["address_number"];
        $data_draft['address_building'] = $dataMember["address_building"];
        $data_draft['address_floor'] = $dataMember["address_floor"];
        $data_draft['address_room'] = $dataMember["address_room"];
        $data_draft['address_village'] = $dataMember["address_village"];
        $data_draft['address_village_no'] = $dataMember["address_village_no"];
        $data_draft['address_alley'] = $dataMember["address_alley"];
        $data_draft['address_road'] = $dataMember["address_road"];
        $data_draft['address_district'] = $dataMember["address_district"];
        $data_draft['address_sub_district'] = $dataMember["address_sub_district"];
        $data_draft['address_province'] = $dataMember["address_province"];
        $data_draft['address_zip_code'] = $dataMember["address_zip_code"];
        $data_draft['address_phone'] = $dataMember["address_phone"];
        $data_draft['address_mobile'] = $dataMember["address_mobile"];
        $data_draft['address_fax'] = $dataMember["address_fax"];
        $data_draft['office_name'] = $dataMember["office_name"];
        $data_draft['office_building'] = $dataMember["office_building"];
        $data_draft['office_floor'] = $dataMember["office_floor"];
        $data_draft['office_room'] = $dataMember["office_room"];
        $data_draft['office_village'] = $dataMember["office_village"];
        $data_draft['office_village_no'] = $dataMember["office_village_no"];
        $data_draft['office_alley'] = $dataMember["office_alley"];
        $data_draft['office_road'] = $dataMember["office_road"];
        $data_draft['office_district'] = $dataMember["office_district"];
        $data_draft['office_sub_district'] = $dataMember["office_sub_district"];
        $data_draft['office_province'] = $dataMember["office_province"];
        $data_draft['office_zip_code'] = $dataMember["office_zip_code"];
        $data_draft['office_phone'] = $dataMember["office_phone"];
        $data_draft['office_mobile'] = $dataMember["office_mobile"];
        $data_draft['office_fax'] = $dataMember["office_fax"];
        $data_draft['obj_lang'] = "MAS";
        $data_draft['obj_status'] = "active";
        $data_draft['obj_state'] = "published";
        $data_draft['obj_modified_date'] = $up_date;
        $data_draft['obj_modified_user_id'] = 0;
        $data_draft['obj_published_date'] = $up_date;
        $data_draft['obj_published_user_id'] = 0;
        $data_draft['member_card'] = ($dataMember["member_card_register"] == 1 || $dataMember["member_card_register"] == 2)? "T": "F";

        $dtMember = DB::table("member_web")->where("member_web_id", $member_wed_id)->update($data);
        $dt_rev = DB::table('member_web_draft')->where("member_web_id", $member_wed_id)->max('obj_rev');
        $dtMemberDraft = DB::table("member_web_draft")->where("member_web_id", $member_wed_id)->where("obj_rev", $dt_rev)->update($data_draft);

        $dataDelete = $this->removeMemberChildData($member_wed_id);
        $dataMember["obj_rev"] = $dt_rev;
        $dataInsertChild = $this->insertMemberChildData($dataMember);

        if($dtMember && $dtMemberDraft && $dataInsertChild){
        	return true;
        }
        return false;
    }

    function insertMemberChildData($dataMember){
    	$date_now = date("Y-m-d H:i:s");
        $cc = 0;
        $countChild = 0;
        if(isset($dataMember["mom_status"])){
        	$countChild = count($dataMember["mom_status"]);
        	if($countChild > 0){
	        	foreach ($dataMember["mom_status"] as $key => $status) {
	        		$member_web_child_data_id = $this->generateUUID();
			    	$data['member_web_child_data_id'] = $member_web_child_data_id;
			        $data['mom_status'] = $status;
			        $data['child_date'] = (isset($dataMember["child_date"][$key]) && $dataMember["child_date"][$key] != "")? $this->changeDateFormat($dataMember["child_date"][$key]): null;
			        $data['antenatal_place'] = (isset($dataMember["antenatal_place"][$key]) && $dataMember["child_date"][$key] != "")? $this->changeDateFormat($dataMember["antenatal_place"][$key]): "-";
			        $data['submit_date'] = $date_now;
			        $data['obj_lang'] = "MAS";
			        $data['obj_parent_id'] = $dataMember["member_web_id"];

			        $data_draft['member_web_child_data_id'] = $member_web_child_data_id;
			        $data_draft['mom_status'] = $status;
			        $data_draft['child_date'] = (isset($dataMember["child_date"][$key]) && $dataMember["child_date"][$key] != "")? $this->changeDateFormat($dataMember["child_date"][$key]): null;
			         $data_draft['antenatal_place'] = (isset($dataMember["antenatal_place"][$key]) && $dataMember["antenatal_place"][$key] != "")? $this->changeDateFormat($dataMember["antenatal_place"][$key]): "-";
			        $data_draft['submit_date'] = $date_now;
			        $data_draft['obj_lang'] = "MAS";
			        $data_draft['obj_rev'] = isset($dataMember["obj_rev"])? $dataMember["obj_rev"] : 1;
			        $data_draft['obj_parent_id'] = $dataMember["member_web_id"];
			        $data_draft['obj_status'] = "active";
			        $data_draft['obj_state'] = "draft";
			        $data_draft['obj_tmp_session_id'] = 0;
			        $data_draft['obj_created_date'] = $date_now;
			        $data_draft['obj_created_user_id'] = 0;
			        $data_draft['obj_modified_date'] = $date_now;
			        $data_draft['obj_modified_user_id'] = 0;

			        $dtMemberChild = DB::table("member_web_child_data")->insert($data);
			        $dtMemberChildDraft = DB::table("member_web_child_data_draft")->insert($data_draft);
			        if($dtMemberChild && $dtMemberChildDraft){
			        	$cc++;
			        }
		    	}
		    }
        }


        if($cc == $countChild){
        	return true;
        }
        return false;
    }

    function changeDateFormat($date){
    	return implode("-", array_reverse(explode("/",$date)));
    }

    function removeMemberChildData($member_web_id){
    	$dtDelete = DB::table("member_web_child_data")->select('trick_mom_id')->where('obj_parent_id', $member_web_id)->delete();
    	$dtDelete_draft = DB::table("member_web_child_data_draft")->select('trick_mom_id')->where('obj_parent_id', $member_web_id)->delete();

    	if($dtDelete && $dtDelete_draft){
    		return true;
    	}
    	return false;
    }

    function generateUUID() {
        $date = new DateTime();
        return (($date->format('U') * 1000) + mt_rand(0,999));
    }

    function addBookmarkByUser($momtip_id,$member_web_id) {
     	try {
	        $now = date("Y-m-d H:i:s");
	        $data['member_web_id'] = $member_web_id;
	        $data['trick_mom_id'] = $momtip_id;
	        $data['bookmark_date'] = $now;

	        $dtBookMark = DB::table("bookmark")->insert($data);
	        if($dtBookMark){
	        	return 1;
	        }else{
	        	return 0;
	        }
        }catch(\Exception $e){
        	$this->error_message = $e->getMessage();
			return -1;
		}
    }

    function getLastArticleByUser($member_web_id){
    	$last_view = \Illuminate\Support\Facades\Session::get('lv');
	    $arr_data_last_view = [];
    	if($last_view != ""){
	    	$data_last_view = json_decode($last_view,true);
	    	krsort($data_last_view);
	    	foreach ($data_last_view as $val_last_view) {
				$dt = DB::table('trick_mom')->select('trick_mom.trick_mom_id', 'trick_mom.title', 'trick_mom.description', 'trick_mom.image', 'trick_mom.image_gen', 'trick_mom.type_mom', 'trick_mom.file_mp4', 'trick_mom.youtube_source','trick_mom.slug',DB::raw('title_' . App::getLocale() . ' AS title_cat'),'trick_mom.sub_category','page_view.total AS view')->leftJoin('trick_mom_subcat', 'trick_mom_subcat.category_code', '=', 'trick_mom.type_mom')->leftJoin('page_view', 'page_view.ref_id', '=', 'trick_mom.trick_mom_id')->where('module','trick_mom')->where('trick_mom.obj_lang',Lng::db())->where('trick_mom.trick_mom_id',$val_last_view)->get();
				$arr_data_last_view[] = $dt[0];
	    	}
    	}
		return $arr_data_last_view;
	}

	function addLastArticleByUser($member_web_id,$trick_mom_id) {
        $last_view = \Illuminate\Support\Facades\Session::get('lv');
        $arr_last_view = json_decode($last_view,true);
        $count_last_view = count($arr_last_view);
        $new_array = [];
        if($arr_last_view != ""){
	        if(($key = array_search($trick_mom_id, $arr_last_view)) !== false) {
			    unset($arr_last_view[$key]);
			}
	        foreach ($arr_last_view as $key_last_view => $val_last_view) {
	        	if($count_last_view >= 5 && $key_last_view == 0){
	        		continue;
	        	}else{
	        		$new_array[] = $val_last_view;
	        	}
	        }
        }
        $new_array[] = $trick_mom_id;
        \Illuminate\Support\Facades\Session::put('lv', json_encode($new_array));
        return $new_array;
    }

    function getBookmarkByUser($member_web_id,$page,$limit){
    	DB::connection()->enableQueryLog();
		$dt = DB::table('trick_mom')->select('trick_mom.trick_mom_id', 'trick_mom.title', 'trick_mom.description', 'trick_mom.image', 'trick_mom.image_gen', 'trick_mom.type_mom', 'trick_mom.file_mp4', 'trick_mom.youtube_source','trick_mom.slug',DB::raw('title_' . App::getLocale() . ' AS title_cat'),'trick_mom.sub_category','page_view.total AS view')->join('bookmark', 'bookmark.trick_mom_id', '=', 'trick_mom.trick_mom_id')->leftJoin('trick_mom_subcat', 'trick_mom_subcat.category_code', '=', 'trick_mom.type_mom')->leftJoin('page_view', 'page_view.ref_id', '=', 'bookmark.trick_mom_id')->where('member_web_id',$member_web_id)->where('module','trick_mom')->where('trick_mom.obj_lang',Lng::db())->orderBy("bookmark.favorite","DESC")->orderBy("bookmark.bookmark_date","DESC")->skip($page)->take($limit)->get();
		return $dt;
	}

	function getBookmarkBySlug($slug,$member_web_id){
		$dt = DB::table('trick_mom')->select('trick_mom.trick_mom_id', 'trick_mom.title', 'trick_mom.description', 'trick_mom.image', 'trick_mom.image_gen', 'trick_mom.type_mom', 'trick_mom.file_mp4', 'trick_mom.youtube_source','trick_mom.slug') ->join('bookmark', 'bookmark.trick_mom_id', '=', 'trick_mom.trick_mom_id')->where('slug',$slug)->where('member_web_id',$member_web_id)->orderBy("bookmark.bookmark_date","DESC")->get();
		return $dt;
	}

	function getBookmarkByMember($member_web_id){
		$dt = DB::table('bookmark')->select('bookmark.trick_mom_id')->where('member_web_id',$member_web_id)->orderBy("bookmark.bookmark_date","DESC")->get();
		return $dt;
	}

	function getMemberByFacebookID($facebook_id) {
		try {
	 		$dt = DB::table('member_web')->select('member_web_id','email','firstname','lastname','facebook_id','last_login_success')->where('facebook_id',$facebook_id)->get();
        	return $dt;
	 	 }catch(\Exception $e){
	 	 	$this->error_message = $e->getMessage();
			return [];
		}
    }

    function getMemberByEmail($email) {
    	$dt = [];
	 	try {
	 		$dt = DB::table('member_web')->select('member_web_id','email','firstname','lastname','login_count','last_login','register_date','last_login_success','member_status')->where('email',$email)->get();
	 		if(count($dt) > 0){
        		return $dt;
	 		}else{
	 			$this->error_message = "Empty data";
	 		}
	 	 }catch(\Exception $e){
	 	 	$this->error_message = $e->getMessage();
		}
        return $dt;
    }

    function getMemberByID($member_web_id) {
	 	try {
	 		$dt = DB::table('member_web')->where('member_web_id',$member_web_id)->get();
        	return $dt;
	 	 }catch(\Exception $e){
	 	 	$this->error_message = $e->getMessage();
			return [];
		}
    }

    function getMemberLoginByID($member_web_id) {
	 	try {
	 		$dt = DB::table('member_web')->select('last_login_success','member_status')->where('member_web_id',$member_web_id)->get();
        	return $dt;
	 	 }catch(\Exception $e){
	 	 	$this->error_message = $e->getMessage();
			return [];
		}
    }

    function getMemberDataChildByID($member_web_id) {
	 	try {
	 		$dt = DB::table('member_web_child_data')->where('obj_parent_id',$member_web_id)->orderBy("submit_date", "ASC")->get();
        	return $dt;
	 	 }catch(\Exception $e){
	 	 	$this->error_message = $e->getMessage();
			return [];
		}
    }

    function userLogin($username) {
        $dt = DB::table("member_web")->select('member_web_id','displayname','email','password','firstname','lastname','verify_status','password_update_time','last_login','last_login_success','member_status')->where("email",$username)->get();

        return $dt;
    }

    function getUserData($user_id) {
        $dt = DB::table("member_web")->select('member_web_id','displayname','email','password','firstname','lastname','register_date')->where("member_web_id",$user_id)->where("member_status","!=",3)->get();
        return $dt;
    }

	function removeBookmarkByUser($momtip_id,$member_web_id){
		try {
			$dtBookMark = DB::table("bookmark")->select('trick_mom_id')->where('trick_mom_id', $momtip_id)->where('member_web_id', $member_web_id)->delete();
			if($dtBookMark > 0){
				return $dtBookMark;
			}
		}catch(\Exception $e){
			$this->error_message = $e->getMessage();
			return [];
		}
	}

    function countBookmark() {
        $member_id = \Illuminate\Support\Facades\Session::get("webMemberId");
        $dt = DB::select("SELECT count(bookmark.trick_mom_id) AS count FROM bookmark JOIN trick_mom ON (trick_mom.trick_mom_id = bookmark.trick_mom_id) WHERE member_web_id=".$member_id . " AND trick_mom.obj_lang = '".Lng::db()."'");
        return $dt[0];
    }

    function setToken($data) {
        return DB::table("vertify_token")->insert($data);
    }

    function getLastMember() {
        $dt = DB::select("SELECT member_web_id FROM member_web ORDER BY register_date DESC LIMIT 0,1");
        return $dt;
    }

    function getToken($token) {
        $dt = DB::select("SELECT * FROM vertify_token WHERE token='".$token."'");
        return $dt;
    }

    function updateTokenVerify($web_member_id) {
        $update_data["verify_status"] = "true";
        $check = true;
        if (!DB::table('member_web')->where('member_web_id','=',$web_member_id)->update($update_data)) {
            $check = false;
        }
        if (DB::table('member_web_draft')->where('member_web_id','=',$web_member_id)->update($update_data)) {
            $check = false;
        }
        if (DB::table('vertify_token')->where('web_member_id','=',$web_member_id)->delete()) {
            $check = false;
        }
        return $check;
    }

    function checkVerify($token) {
        $dt = DB::table("vertify_token")->where('token',$token)->get();
        return $dt;
    }

    function removeVerifyToken($web_member_id) {
        return DB::table('vertify_token')->where('web_member_id','=',$web_member_id)->delete();
    }

    function getPasswordHistory($password,$web_member_id) {
        $shaPassword = hash('sha256',$password);
        $md5Password = md5($password);
        $sql = "SELECT * FROM password_history WHERE (old_pass='".$md5Password."' OR old_pass='".$shaPassword."') AND member_web_id=".$web_member_id;
        $dt =  DB::select($sql);
        return $dt;
    }

    function getOldPassword($password,$web_member_id) {
    	$shaPassword = hash('sha256',$password);
        $md5Password = md5($password);
        $sql = "SELECT member_web_id FROM member_web WHERE (password='".$md5Password."' OR password='".$shaPassword."') AND member_web_id=".$web_member_id;
        $dt =  DB::select($sql);
        return $dt;
    }

    function updatePassword($member_id,$update_data) {
        $update_data["password_update_time"] = date("Y-m-d H:i:s");
        $update_data["verify_status"] = "true";
        $update_data["login_count"] = 0;
        $update_data["member_status"] = 1;

        $res = DB::table("member_web")->where("member_web_id",$member_id)->update($update_data);
        $res2 = DB::table("member_web_draft")->where("member_web_id",$member_id)->update($update_data);
        if ($res && $res2) {
            return true;
        } else {
            return false;
        }
    }

    function keepOldPassword($member_id,$old_password) {
        $data["member_web_id"] = $member_id;
        $data["old_pass"] = $old_password;
        $count_dt = DB::select("SELECT COUNT(id) AS count_record FROM password_history WHERE member_web_id='".$member_id."'");
        $count = $count_dt[0]->count_record;
        if ($count < 8) {
            $data["record_number"] = $count+1;
            $res = DB::table("password_history")->insert($data);
        } else {
            $last_rec = DB::select("SELECT record_number FROM password_history WHERE member_web_id='".$member_id."' ORDER BY keep_date ASC");
            $res = DB::table("password_history")->where("record_number",$last_rec[0]->record_number)->where("member_web_id",$member_id)->update($data);
        }
        return $res;
    }

    function setExpireDate($user_mail,$expire_date) {
        $data_update["password_update_time"] = $expire_date;
        $dt = DB::table("member_web")->where("email",$user_mail)->update($data_update);
        $dt2 = DB::table("member_web_draft")->where("email",$user_mail)->update($data_update);
        return $dt;
    }

    function getFavoriteByUser($member_web_id){
		$dt = DB::table('trick_mom')->select('trick_mom.trick_mom_id')->join('bookmark', 'bookmark.trick_mom_id', '=', 'trick_mom.trick_mom_id')->where('member_web_id',$member_web_id)->where('favorite','T')->get();
		return $dt;
	}

	function addFavoriteByUser($momtip_id,$member_web_id) {
     	try {
	        $update["favorite"] = "T";
			$update["favorite_date"] = date("Y-m-d H:i:s");
			$dtBookMark = DB::table("bookmark")->select('trick_mom_id')->where('trick_mom_id', $momtip_id)->where('member_web_id', $member_web_id)->update($update);
			if($dtBookMark > 0){
				return $dtBookMark;
			}
	        if($dtBookMark){
	        	return 1;
	        }else{
	        	return 0;
	        }
        }catch(\Exception $e){
        	$this->error_message = $e->getMessage();
			return -1;
		}
    }

    function removeFavoriteByUser($momtip_id,$member_web_id){
		try {
			$update["favorite"] = "F";
			$update["favorite_date"] = date("Y-m-d H:i:s");
			$dtBookMark = DB::table("bookmark")->select('trick_mom_id')->where('trick_mom_id', $momtip_id)->where('member_web_id', $member_web_id)->update($update);
			if($dtBookMark > 0){
				return $dtBookMark;
			}
		}catch(\Exception $e){
			$this->error_message = $e->getMessage();
			return [];
		}
	}

	function updateLogin($email,$login_count,$type = 'email') {
    	$dt = [];
	 	try {
	 		$dateTime = date("Y-m-d H:i:s");
	 		if($type == 'facebook'){
	 			$dt = DB::table('member_web')->where('member_web_id',$email)->update(['last_login' => $dateTime,'login_count' => $login_count,'last_login_success' => $dateTime,'member_status' => 1]);
		 		$dt = DB::table('member_web_draft')->where('member_web_id',$email)->update(['last_login' => $dateTime,'login_count' => $login_count,'last_login_success' => $dateTime,'member_status' => 1]);
	 		}else{
		 		$dt = DB::table('member_web')->where('email',$email)->update(['last_login' => $dateTime,'login_count' => $login_count,'last_login_success' => $dateTime,'member_status' => 1]);
		 		$dt = DB::table('member_web_draft')->where('email',$email)->update(['last_login' => $dateTime,'login_count' => $login_count,'last_login_success' => $dateTime,'member_status' => 1]);
	 		}
	 		\Illuminate\Support\Facades\Session::set("webMemberLastLogin",$dateTime);
	 		if(count($dt) > 0){
        		return $dt;
	 		}else{
	 			$this->error_message = "Empty data";
	 		}
	 	 }catch(\Exception $e){
	 	 	$this->error_message = $e->getMessage();
		}
        return $dt;
    }

    function updateMemberStatus($email) {
        $update_data["member_status"] = 2;
        $res = DB::table("member_web")->where("email",$email)->where("member_type",1)->update($update_data);
        $res2 = DB::table("member_web_draft")->where("email",$email)->where("member_type",1)->update($update_data);
        if ($res && $res2) {
            return true;
        } else {
            return false;
        }
    }


}
