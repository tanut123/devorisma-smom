<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;

class Brain extends Model {

	function loadlist($start,$limit)
	{
		$dt = DB::select("SELECT * FROM brain_ignite_tips WHERE obj_lang='".Lng::db()."' AND valid_date <= '" . date("Y-m-d H:i:s") . "' ORDER BY valid_date DESC LIMIT ".$start.",".$limit);
		return $dt;
	}

	function loadlistMedia()
	{
		$dt = DB::select("SELECT * FROM brain_media WHERE obj_lang='".Lng::db()."' AND valid_date <= '" . date("Y-m-d H:i:s") . "' ORDER BY valid_date DESC");
		return $dt;
	}

	public function load_detail($slug)
	{
		$dt = DB::table('brain_ignite_tips')->where('obj_lang', Lng::db())->where('slug',$slug)->where('valid_date','<=',date("Y-m-d H:i:s"))->get();

		return $dt;
	}
}