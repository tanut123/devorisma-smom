<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class SiteSetting extends Model {

	protected $table = '';

	function getSetting(){
		$dt = DB::table('site_settings')->get();
		if (count($dt) > 0) {
			foreach($dt as $row) {
				$name = $row->name;
				$value = $row->text;
				$settings[$name] = $value;
			}
		}
		return $settings;
	}
	function getSettingLastModStatic(){
		$dt = DB::table('site_settings')->select("obj_published_date")->where("name","SITEMAP_STATIC_PAGE")->get();
		return $dt[0]->obj_published_date;
	}

}
