<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;

class Subscribe extends Model {


	function getExitsEmail($email){
		$dt = DB::table("subscribe")->where("email",$email)->get();
		return $dt;
	}
    function insertEmail($email) {
        $reg_date = date("Y-m-d H:i:s");
        $data['subscribe_id'] = strtotime($reg_date).rand(100,999);
        $data['email'] = $email;
        $data['register_date'] = $reg_date;
        $data['obj_lang'] = "MAS";
        $data['obj_published_date'] = $reg_date;
        $data['obj_published_user_id'] = "";

        $data_draft['subscribe_id'] = strtotime($reg_date).rand(100,999);
        $data_draft['email'] = $email;
        $data_draft['register_date'] = $reg_date;
        $data_draft['obj_lang'] = "MAS";
        $data_draft['obj_rev'] = "0";
        $data_draft['obj_status'] = "active";
        $data_draft['obj_state'] = "published";
        $data_draft['obj_created_date'] = $reg_date;
        $data_draft['obj_created_user_id'] = "";
        $data_draft['obj_modified_date'] = "";
        $data_draft['obj_modified_user_id'] = "";
        $data_draft['obj_published_date'] = $reg_date;
        $data_draft['obj_published_user_id'] = "";

        DB::table("subscribe")->insert($data);
        DB::table("subscribe_draft")->insert($data_draft);
    }
}
