<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use DateTime;

class Momtip extends Model {

	protected $table = '';

	function insertRating($dataRating){
		$submit_date = date("Y-m-d H:i:s");
		$unique_id = $this->generateUUID();
        $page_uid = $dataRating['page_uid'];
		$rate = $dataRating['rate'];
		$trip_mom_dt = DB::select("SELECT title,type_mom,sub_category FROM trick_mom WHERE trick_mom_id=".$page_uid ." AND obj_lang = '" . Lng::db() ."'");
		$trip_mom_array = (array)$trip_mom_dt[0];
		if($rate == 1){
			$data_draft["answer_first"] = 1;
		}else if($rate == 2){
			$data_draft["answer_second"] = 1;
		}else if($rate == 3){
			$data_draft["answer_third"] = 1;
		}else if($rate == 4){
			$data_draft["answer_fourth"] = 1;
		}else if($rate == 5){
			$data_draft["answer_fifth"] = 1;
		}
		$data_draft['data_id'] = $page_uid;
        $data_draft['submit_date'] = $submit_date;
        $data_draft['data_title'] = $trip_mom_array["title"];
        $data_draft['data_type'] = $trip_mom_array["type_mom"];
        $data_draft['data_category'] = $trip_mom_array["sub_category"];
        $data_draft['unique_id'] = $unique_id;
        $data_draft['status'] = "active";
        $data_draft['obj_lang'] = Lng::db();
        $data_draft['obj_rev'] = "0";
        $data_draft['obj_status'] = "active";
        $data_draft['obj_state'] = "published";
        $data_draft['obj_created_date'] = $submit_date;
        $data_draft['obj_created_user_id'] = 0;
        $data_draft['obj_modified_date'] = $submit_date;
        $data_draft['obj_modified_user_id'] = 0;
        $data_draft['obj_published_date'] = $submit_date;
        $data_draft['obj_published_user_id'] = 0;

        $dtMemberDraft = DB::table("rating_draft")->insert($data_draft);
	        if($dtMemberDraft){
	        	return true;
	        }
        	return false;
	}

	function generateUUID() {
        $date = new DateTime();
        return (($date->format('U') * 1000) + mt_rand(0,999));
    }

	function getTrickMomListAll(){
		$dt = DB::table('trick_mom')->select('trick_mom_id','type_mom','obj_lang','slug', 'sub_category')->orderBy("sort_order","DESC")->orderBy("article_date","DESC")->get();
		return $dt;
	}

	function getTrickMomLastMod(){
		$dt = DB::table('trick_mom')->select('obj_published_date')->orderBy("obj_published_date","DESC")->take(1)->get();
		return $dt[0]->obj_published_date;
	}

	function getCountTrickMomAll($lang){
		$dt = DB::table('trick_mom')->select('trick_mom_id','type_mom','obj_lang','slug')->where("obj_lang", $lang)->orderBy("sort_order","DESC")->orderBy("article_date","DESC")->count();
		return $dt;
	}

	function getTrickMomListAllLimit($page, $limit, $lang){
		$dt = DB::table('trick_mom')->select('trick_mom_id','type_mom','obj_lang', 'obj_published_date','slug')->where("obj_lang", $lang)->orderBy("article_date","DESC")->skip($page)->take($limit)->get();
		return $dt;
	}

	function getTrickMomList($type, $page, $limit){
		$dt = DB::table('trick_mom')->select('trick_mom_id', 'title', 'description', 'image', 'image_gen', 'type_mom', 'file_mp4', 'youtube_source', 'article_date','slug')->where('type_mom', $type)->where('obj_lang', Lng::db())->orderBy("sort_order","DESC")->orderBy("article_date","DESC")->skip($page)->take($limit)->get();
		return $dt;
	}

	function getTrickMomListRelate($type, $slug, $sub_cat){
		// DB::connection()->enableQueryLog();
		$dt = DB::table('trick_mom')->select('trick_mom_id', 'title', 'description', 'image', 'image_gen', 'type_mom', 'file_mp4', 'youtube_source', 'article_date','slug')->where('type_mom', $type)->where('sub_category', $sub_cat)->where('obj_lang', Lng::db())->where('slug', '<>' ,  $slug)->where('article_date','<=', date("Y-m-d H:i:s"))->orderBy("sort_order","DESC")->orderBy("article_date","DESC")->take(12)->get();
		// var_dump(DB::getQueryLog());
		return $dt;
	}

	function getTrickMomDetail($type, $slug){
		$dt = DB::table('trick_mom')->select('trick_mom_id', 'title', 'description', 'image', 'image_gen', 'type_mom', 'file_mp4_gen', 'file_mp4', 'youtube_source', 'article_date','detail', 'pageviews', 'image_detail', 'image_detail_gen', 'slug','sub_category')->where('type_mom', $type)->where('obj_lang', Lng::db())->where('slug', $slug)->get();
		return $dt;
	}

	function updateTrickMomViews($id, $lang, $views){
		$dataUpdate["pageviews"] = $views;
		$dataUpdate["pageviews_lastupdate"] = date("Y-m-d H:i:s");
		$dt = DB::table('trick_mom')->where('trick_mom_id', '=' ,  $id)->where('obj_lang', '=' ,  $lang)->update($dataUpdate);
		return $dt;
	}
	function updateViews($id, $lang){

		$dataUpdate["pageviews_lastupdate"] = date("Y-m-d H:i:s");
		$dt = DB::table('trick_mom')->where('trick_mom_id', '=' ,  $id)->where('obj_lang', '=' ,  $lang)
			->increment('pageviews' ,1,$dataUpdate);
		return $dt;
	}

	function momtipPage($type){
		return str_replace(["pregnancy-mom","lactating-mom","toddler-mom"],[1,2,3],$type);
	}

	function getSubcat($type){
		$dt = DB::select("SELECT * FROM sub_category_child WHERE obj_parent_id = (SELECT trick_mom_subcat_id FROM trick_mom_subcat WHERE category_code = '".$type."') ORDER BY sort_order ASC");
		return $dt;
	}
	function getListBySubCat($subcat_id){
		$sql = "SELECT p.total as pageview ,t.* FROM trick_mom t left join page_view p on(p.`module` = 'trick_mom' and p.`ref_id` = t.`trick_mom_id` and p.obj_lang = t.obj_lang)  WHERE t.sub_category = ".$subcat_id." AND t.obj_lang = '" . Lng::db() . "' AND t.article_date <= '".date("Y-m-d H:i:s")."' ORDER BY t.sort_order DESC";
		// var_dump($sql);
		$dt = DB::select($sql);
		return $dt;
	}

}