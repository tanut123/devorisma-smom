<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App\Core\SettingManagement;


class Visitor extends Model {

	function getVisitor(){
		$dt = DB::select("SELECT SUM(total) as allview FROM page_view WHERE module = 'site'");
		if(isset($dt[0])){
			return $dt[0]->allview;
		}
		return 0;
	}

}
