<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;
use DateTime;

class Mommygadget extends Model {

	function loadlist($start,$limit)
	{
		$dt = DB::table('mommygadget')
				->leftJoin('page_view', function($join){
				$join->on('page_view.ref_id', '=', 'mommygadget.mommygadget_id')->where('page_view.module', '=', "mommygadget")->where("page_view.obj_lang","=","THA");
				})->where('valid_date','<=', date("Y-m-d H:i:s"))
				->orderBy('priority','ASC')->orderBy("valid_date","DESC")
				->limit($limit)->offset($start)->get();
		return $dt;
	}

	public function load_detail($slug){
		$dt = DB::table('mommygadget')->where('slug',$slug)->where('valid_date','<=', date("Y-m-d H:i:s"))->get();
		return $dt;
	}

	function insertRating($dataRating){
		$submit_date = date("Y-m-d H:i:s");
		$unique_id = $this->generateUUID();
        $page_uid = $dataRating['page_uid'];
		$rate = $dataRating['rate'];
		$mommygadget_list_dt = DB::select("SELECT title FROM mommygadget WHERE mommygadget_id=".$page_uid ." ");
		$mommygadget_list_array = (array)$mommygadget_list_dt[0];
		if($rate == 1){
			$data_draft["answer_first"] = 1;
		}else if($rate == 2){
			$data_draft["answer_second"] = 1;
		}else if($rate == 3){
			$data_draft["answer_third"] = 1;
		}else if($rate == 4){
			$data_draft["answer_fourth"] = 1;
		}else if($rate == 5){
			$data_draft["answer_fifth"] = 1;
		}
		$data_draft['data_id'] = $page_uid;
        $data_draft['submit_date'] = $submit_date;
        $data_draft['data_title'] = $mommygadget_list_array["title"];
        $data_draft['unique_id'] = $unique_id;
        $data_draft['status'] = "active";
        $data_draft['obj_lang'] = Lng::db();
        $data_draft['obj_rev'] = "0";
        $data_draft['obj_status'] = "active";
        $data_draft['obj_state'] = "published";
        $data_draft['obj_created_date'] = $submit_date;
        $data_draft['obj_created_user_id'] = 0;
        $data_draft['obj_modified_date'] = $submit_date;
        $data_draft['obj_modified_user_id'] = 0;
        $data_draft['obj_published_date'] = $submit_date;
        $data_draft['obj_published_user_id'] = 0;

        $dtMemberDraft = DB::table("rating_mommygadget_draft")->insert($data_draft);
	        if($dtMemberDraft){
	        	return true;
	        }
        	return false;
	}

	function generateUUID() {
        $date = new DateTime();
        return (($date->format('U') * 1000) + mt_rand(0,999));
    }

    function getRelatedData($id){
    	$dt = DB::table('mommygadget')->where('mommygadget_id','!=',$id)->where('valid_date','<=', date("Y-m-d H:i:s"))->orderBy('priority','ASC')->orderBy("valid_date","DESC")->get();
    	return $dt;
    }

}