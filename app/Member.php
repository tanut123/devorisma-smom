<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use DateTime;

class Member extends Model {


	function getMemberExist($email,$mobile){
		$dt = DB::table("member");

		if($email != "" && $mobile != ""){
			$dt->where("email",$email)->orWhere("mobile",$mobile);
		}else{
			if($email != ""){
				$dt->where("email",$email);
			}else if($mobile != ""){
				$dt->where("mobile",$mobile);
			}
		}

		return $dt->get();
	}
    function insertMember($dataMember) {
        $reg_date = date("Y-m-d H:i:s");
        $member_id = $this->generateUUID();
        $data['member_id'] = $member_id;
        $data['firstname'] = $dataMember["firstname"];
        $data['lastname'] = $dataMember["lastname"];
        $data['mobile'] = $dataMember["mobile"];
        $data['status_mom'] = $dataMember["status"];
        $data['eating_choice'] = (isset($dataMember["eating_choice"]) ? $dataMember["eating_choice"] :null);
        $data['eating_text'] = (isset($dataMember["eating_text"]) ? $dataMember["eating_text"] :null);
        $data['type_register'] = $dataMember["type_register"];
        $data['email'] = $dataMember["email"];
        $data['child_date'] = (isset($dataMember["child_date"]) && $dataMember["child_date"] != "")? $this->changeDateFormat($dataMember["child_date"]) : null;
        $data['antenatal_place'] = (isset($dataMember["antenatal_place"]) && $dataMember["antenatal_place"] != "")? $dataMember["antenatal_place"] : "-";
        $data['register_date'] = $reg_date;
        $data['obj_lang'] = "MAS";
        $data['obj_published_date'] = $reg_date;
        $data['obj_published_user_id'] = 0;

        $data_draft['member_id'] = $member_id;
        $data_draft['firstname'] = $dataMember["firstname"];
        $data_draft['lastname'] = $dataMember["lastname"];
        $data_draft['mobile'] = $dataMember["mobile"];
        $data_draft['status_mom'] = $dataMember["status"];
        $data_draft['eating_choice'] = (isset($dataMember["eating_choice"]) ? $dataMember["eating_choice"] :null);
        $data_draft['eating_text'] = (isset($dataMember["eating_text"]) ? $dataMember["eating_text"] :null);
        $data_draft['type_register'] = $dataMember["type_register"];
        $data_draft['email'] = $dataMember["email"];
        $data_draft['register_date'] = $reg_date;
        $data_draft['child_date'] = (isset($dataMember["child_date"]) && $dataMember["child_date"] != "")? $this->changeDateFormat($dataMember["child_date"]) : null;
        $data_draft['antenatal_place'] = (isset($dataMember["antenatal_place"]) && $dataMember["antenatal_place"] != "")? $dataMember["antenatal_place"] : "-";
        $data_draft['obj_lang'] = "MAS";
        $data_draft['obj_rev'] = "0";
        $data_draft['obj_status'] = "active";
        $data_draft['obj_state'] = "published";
        $data_draft['obj_created_date'] = $reg_date;
        $data_draft['obj_created_user_id'] = 0;
        $data_draft['obj_modified_date'] = $reg_date;
        $data_draft['obj_modified_user_id'] = 0;
        $data_draft['obj_published_date'] = $reg_date;
        $data_draft['obj_published_user_id'] = 0;

        $dtMember = DB::table("member")->insert($data);
        $dtMemberDraft = DB::table("member_draft")->insert($data_draft);
        if($dtMember && $dtMemberDraft){
        	return true;
        }
        return false;
    }
    function generateUUID() {
        $date = new DateTime();
        return (($date->format('U') * 1000) + mt_rand(0,999));
    }
    function changeDateFormat($date){
    	return implode("-", array_reverse(explode("/",$date)));
    }
}
