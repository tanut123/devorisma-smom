<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;

class Promotion extends Model {

	function loadListAll()
	{
		$dt = DB::select("SELECT promotion_id, obj_lang, slug FROM promotion  ORDER BY valid_date DESC ");
		return $dt;
	}

	function loadLastMod()
	{
		$dt = DB::select("SELECT obj_published_date FROM promotion  ORDER BY obj_published_date DESC LIMIT 1");
		return $dt[0]->obj_published_date;
	}

	function countListAll($lang)
	{
		$dt = DB::select("SELECT count(promotion_id) As countData FROM promotion WHERE obj_lang = ?  ORDER BY valid_date DESC ", [$lang]);
		return $dt;
	}

	function loadListAllLimit($start, $limit, $lang)
	{
		$dt = DB::select("SELECT promotion_id, obj_lang , obj_published_date, slug FROM promotion WHERE obj_lang = ?   ORDER BY valid_date DESC LIMIT ".$start.",".$limit, [$lang]);
		return $dt;
	}

	function loadlist($start,$limit)
	{
		$dt = DB::select("SELECT * FROM promotion WHERE obj_lang='".Lng::db()."' AND valid_date <= '" . date("Y-m-d H:i:s") . "' ORDER BY valid_date DESC LIMIT ".$start.",".$limit);
		return $dt;
	}
	public function load_detail($slug)
	{
		$dt = DB::table('promotion')->where('obj_lang', Lng::db())->where('slug',$slug)->where('valid_date','<=',date("Y-m-d H:i:s"))->get();

		return $dt;
	}

	function updatePromotionViews($id, $lang, $views){
		$dataUpdate["pageviews"] = $views;
		$dataUpdate["pageviews_lastupdate"] = date("Y-m-d H:i:s");
		$dt = DB::table('promotion')->where('promotion_id', '=' ,  $id)->where('obj_lang', '=' ,  $lang)->update($dataUpdate);
		return $dt;
	}
}