<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App\Core\SettingManagement;


class Media extends Model {

	protected $table = '';

	function getMedia($module, $uuname, $filename){
		$this->table = $module . "_media_file";
		$filename = str_replace(" ", "_", $filename);
		if($this->checkMp4Extension($filename)){
			$dt = DB::select("SELECT data, content_type FROM ". $this->table . " WHERE uuname = ?", array($uuname));
		}else{
			$dt = DB::select("SELECT data, content_type FROM ". $this->table . " WHERE uuname = ? AND original_name like ?", array($uuname, $filename));
		}
		if(isset($dt[0])){
			return $dt[0];
		}
		return null;
	}

	function getAllMedia(){
		$setting = new SettingManagement();

		$dt = DB::select("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE IN ('BASE TABLE','VIEW') AND TABLE_SCHEMA='".$setting->get("DATABASE_NAME")."' and TABLE_NAME like '%_media_file'");
		return $dt;
	}

	function mediaDelete($table_name){
		$ds = DB::table($table_name)->where("upload_type","=","DELETE")->orWhere("ref_id" , "0")->delete();
		return $ds;
	}

	function getJoinMedia($table_name){
		$dt = DB::select("SELECT uuname FROM ". $table_name ."_media_file m LEFT JOIN  ". $table_name ."_draft d ON (m.ref_id = d.". $table_name ."_id) where d.obj_status='deleted' OR d.obj_state='deleted'");
		return $dt;
	}

	function mediaDeleteByUUName($table_name, $uuname){
		$ds = DB::table($table_name)->where("uuname","=",$uuname)->delete();

		return $ds;
	}

	function tableSelect($tblname){
		$ds = DB::table($tblname."_draft")->select($tblname . "_id")->where("obj_status", "=", 'active')->get();
		return $ds;
	}

	function checkMp4Extension($fo_name){
		$arr_file = explode(".",$fo_name);
		if(end($arr_file) == "mp4"){
			return true;
		}
		return false;
	}

}
