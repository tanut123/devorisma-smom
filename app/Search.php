<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;

class Search extends Model {

	protected $table = '';

	function getSearchList($word, $page, $limit){
		$dt = array();
		$dt = DB::table('search_index')->select('ref_id','module','raw_data','url')->orderBy("pub_date","DESC");
		if(count($word) > 1){
			foreach ($word as $key => $valWord) {
				// if($key > 0){
				// 	$dt->where("raw_data", "like", "%" . $valWord . "%");
				// }else{
					$dt->orWhere("raw_data", "like", "%" . $valWord . "%");
				// }
			}
		}else{
			$dt->where("raw_data", "like", "%" . $word[0] . "%");
		}
		return $dt->where('obj_lang', Lng::db())->where('pub_date',"<=",date("Y-m-d H:i:s"))->skip($page)->take($limit)->get();
	}

	function listData($data){
		$dt = array();
		$wordRequest = $this->getSearchText($data["word"]);
        $dt['listData'] = $this->getSearchList($wordRequest, $data["page"], $data["limit"]);

        $html = "";
        if(count($dt['listData']) > 0){
			foreach($dt['listData'] as $key => $dataList){
				$dataArr = $this->getRawData($dataList->raw_data);

				$html .= '<li class="FXregular">';
				$html .= '	<span class="list-title color-gold"><a href="' . $this->getUrl($dataList->url) . '"">'. $this->setHighlightHtml($wordRequest, $dataArr["title"]) .'</a></span>';
				if($dataArr["detail"] != ""){
					$html .= '	<span class="list-detail FLighter color-gray-lighter">';
					$html .= 	 $this->setHighlightHtml($wordRequest, $dataArr["detail"], true);
					$html .= '	</span>';
				}
				$moduleGenerate = $this->generateModule($dataList->module);
				if($dataList->module != "" && $moduleGenerate != ""){
					$html .= '	<span class="list-tag color-gold">' . $moduleGenerate . '</span>';
				}
				$html .= '</li>';
			}
		}
		$returnData["list"] = $html;
		$pageNext = $data["page"]+$data["limit"];
        if($data["page"] == 0){
        	$pageNext = $data["limit"];
        }
        $returnData["nextpage"] = count($this->getSearchList($wordRequest, $pageNext, $data["limit"]));

		return $returnData;
	}

	function setHighlightHtml($data,$handle,$trimWithDot = false){

		$pos = 0;

		foreach ($data as $key => $value) {
			$pos = @mb_strpos(strtolower($handle), strtolower($value), 1);
			if($pos > 0){
				break;
			}
		}

		$str = "";
		$res = "";
		$highlight_a = array();

		$len = 190;
		if($pos < $len){
			$start = 0;
			$end = $pos;
		} else {
			$res .= $trimWithDot ? "... ":"";
			$start = $pos - $len;
			$end = $len;
		}

		$detail_res = mb_substr($handle, $start , $end).mb_substr($handle, $pos,$len);

		if(!empty($data)){
			$res .= $this->str_highlight($detail_res, $data,1);
		}

		if($trimWithDot && mb_strlen($handle) > $len){
			$new_handle = substr($handle, 0,intval($start+$end+mb_strlen(implode("", $data))));
			$handle_balance = str_replace($new_handle,"",$handle);
			if(mb_strlen($handle_balance) > 0){
				$res .= " ...";
			}
		}

		return $res;
	}

	function getUrl($url){
		$urlFull = url() . $url;
		$reg_exUrl = "/^(http|https)+/";
		if(preg_match($reg_exUrl, $url)){
			$urlFull = $url;
		}
		return $urlFull;
	}

	function str_highlight($text, $needle, $options = null, $highlight = null)
	{
	    // Default highlighting
	    if ($highlight === null) {
	        $highlight = '<span class="hilight">\1</span>';
	    }

	    // Select pattern to use
	    if ($options & @STR_HIGHLIGHT_SIMPLE) {
	        $pattern = '#(%s)#';
	        $sl_pattern = '#(%s)#';
	    } else {
	        $pattern = '#(?!<.*?)(%s)(?![^<>]*?>)#';
	        $sl_pattern = '#<a\s(?:.*?)>(%s)</a>#';
	    }

	    // Case sensitivity
	    if (!($options & @STR_HIGHLIGHT_CASESENS)) {
	        $pattern .= 'i';
	        $sl_pattern .= 'i';
	    }

	    $needle = (array) $needle;
	    foreach ($needle as $needle_s) {
	        $needle_s = preg_quote($needle_s);

	        // Escape needle with optional whole word check
	        if ($options & @STR_HIGHLIGHT_WHOLEWD) {
	            $needle_s = '\b' . $needle_s . '\b';
	        }

	        // Strip links
	        if ($options & @STR_HIGHLIGHT_STRIPLINKS) {
	            $sl_regex = sprintf($sl_pattern, $needle_s);
	            $text = preg_replace($sl_regex, '\1', $text);
	        }

	        $regex = sprintf($pattern, $needle_s);
	        $text = preg_replace($regex, $highlight, $text);
	    }

	    return $text;
	}

	function getSearchText($q){

			$search = trim($q);
			$search_res = array();
			preg_match_all('/(?:([\'"]).*?\1|[^\s,\'"]+)(?=[\s,]|$)/', $search, $m);

			$search = array();
			if(!empty($m[0])){
				foreach ($m[0] as $key => $value) {
					$search[$key] = str_replace(array("'","\""),array("",""),trim($value));
				}
			}

			foreach ($search as $key => $value) {
				if(!in_array($value, $search_res) && $value != ""){
					array_push($search_res, $value);
				}
			}
			return $search_res;
	}

    private function highlightWord($raw_data, $arrWord, $title_replace = false){
    	$data = "";
    	foreach ($arrWord as $valWord) {
    		if($title_replace){
    			$class = "FBold";
    		}else{
    			$class = "FXregular color-blue";
    		}
    		$raw_data = str_replace($valWord, "<span class='" . $class . "'>" . $valWord . "</span>", $raw_data);
    	}

    	return $raw_data;
    }

    private function getRawData($raw_data){
    	$arrData = explode("|:|", $raw_data);

    	$arr["title"] = "";
    	$arr["detail"] = "";
    	if(isset($arrData[0])){
    		$arr["title"] = $arrData[0];
    	}
    	if(isset($arrData[1])){
    		$arr["detail"] = $arrData[1];
    	}
    	return $arr;
    }

    private function generateModule($module){
    	$moduleName = "";
    	if($module == "trick_mom"){
    		$moduleName = trans("search.SEARCH_MODULE_MOMTRIPS");
    	}else if($module == "1000-days"){
    		$moduleName = trans("search.SEARCH_MODULE_1000_DAY");
    	}else if($module == "promotion"){
    		$moduleName = trans("search.SEARCH_MODULE_PROMOTION");
    	}else if($module == "knowledge_list"){
    		$moduleName = trans("search.SEARCH_MODULE_KNOWLEDGE");
    	}else{
    		$moduleName = "";
    	}
    	return $moduleName;
    }

    public function checkCharactor($q){
    	$message = array();
		$res_str = array();

		$res_str = getSearchText($q);

		if(!empty($res_str)){
			foreach ($res_str as $res_str_key => $res_str_value) {

				if(mb_strlen($res_str_value) <= 2){
					$message["status"] = "F";
					echo json_encode($message);
					die;
				}
			}
		}
		$message["status"] = "T";
		echo json_encode($message);
		die;
    }
}
