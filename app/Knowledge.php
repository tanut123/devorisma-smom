<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;
use DateTime;

class Knowledge extends Model {

	function loadListAll()
	{
		$dt = DB::select("SELECT knowledge_list_id, obj_lang, slug FROM knowledge_list  ORDER BY valid_date DESC ");
		return $dt;
	}

	function countListAll($lang)
	{
		$dt = DB::select("SELECT count(knowledge_list_id) As countData FROM knowledge_list WHERE obj_lang = ?  ORDER BY valid_date DESC ", [$lang]);
		return $dt;
	}

	function loadLastMod()
	{
		$dt = DB::select("SELECT obj_published_date FROM knowledge_list  ORDER BY obj_published_date DESC LIMIT 1");
		return $dt[0]->obj_published_date;
	}

	function loadListAllLimit($start, $limit, $lang)
	{
		$dt = DB::select("SELECT knowledge_list_id, obj_lang , obj_published_date, slug FROM knowledge_list WHERE obj_lang = ? AND valid_date <='".date("Y-m-d H:i:s")."'  ORDER BY valid_date DESC LIMIT ".$start.",".$limit, [$lang]);
		return $dt;
	}

	function loadlist($know_type_db,$know_sub_type_db,$start,$limit)
	{
		$dt = DB::select("SELECT * FROM knowledge_list WHERE obj_lang='".Lng::db()."' AND valid_date <='".date("Y-m-d H:i:s")."' AND knowledge_category = '".$know_type_db."' AND knowledge_subcategory = '".$know_sub_type_db."' ORDER BY valid_date DESC LIMIT ".$start.",".$limit);
		return $dt;
	}

	public function load_detail($knowledge_category,$knowledge_subcategory,$slug)
	{
		$dt = DB::table('knowledge_list')->where('obj_lang', Lng::db())->where('slug',$slug)->where('knowledge_category',$knowledge_category)->where('knowledge_subcategory',$knowledge_subcategory)->get();

		return $dt;
	}

	public function load_detail_old($slug)
	{
		$dt = DB::table('knowledge_list')->where('obj_lang', Lng::db())->where('slug',$slug)->get();
		return $dt;
	}

	function updateKnowledgeViews($id, $lang, $views){
		$dataUpdate["pageviews"] = $views;
		$dataUpdate["pageviews_lastupdate"] = date("Y-m-d H:i:s");
		$dt = DB::table('knowledge_list')->where('knowledge_list_id', '=' ,  $id)->where('obj_lang', '=' ,  $lang)->update($dataUpdate);
		return $dt;
	}

	function insertRating($dataRating){
		$submit_date = date("Y-m-d H:i:s");
		$unique_id = $this->generateUUID();
        $page_uid = $dataRating['page_uid'];
		$rate = $dataRating['rate'];
		$knowledge_list_dt = DB::select("SELECT title FROM knowledge_list WHERE knowledge_list_id=".$page_uid ." AND obj_lang = '" . Lng::db() ."'");
		$knowledge_list_array = (array)$knowledge_list_dt[0];
		if($rate == 1){
			$data_draft["answer_first"] = 1;
		}else if($rate == 2){
			$data_draft["answer_second"] = 1;
		}else if($rate == 3){
			$data_draft["answer_third"] = 1;
		}else if($rate == 4){
			$data_draft["answer_fourth"] = 1;
		}else if($rate == 5){
			$data_draft["answer_fifth"] = 1;
		}
		$data_draft['data_id'] = $page_uid;
        $data_draft['submit_date'] = $submit_date;
        $data_draft['data_title'] = $knowledge_list_array["title"];
        $data_draft['unique_id'] = $unique_id;
        $data_draft['status'] = "active";
        $data_draft['obj_lang'] = Lng::db();
        $data_draft['obj_rev'] = "0";
        $data_draft['obj_status'] = "active";
        $data_draft['obj_state'] = "published";
        $data_draft['obj_created_date'] = $submit_date;
        $data_draft['obj_created_user_id'] = 0;
        $data_draft['obj_modified_date'] = $submit_date;
        $data_draft['obj_modified_user_id'] = 0;
        $data_draft['obj_published_date'] = $submit_date;
        $data_draft['obj_published_user_id'] = 0;

        $dtMemberDraft = DB::table("rating_knowledge_draft")->insert($data_draft);
	        if($dtMemberDraft){
	        	return true;
	        }
        	return false;
	}

	function generateUUID() {
        $date = new DateTime();
        return (($date->format('U') * 1000) + mt_rand(0,999));
    }

    function getSubCategory($know_type_db){
    	$dt = DB::table('knowledge_list')->select('knowledge_subcategory')->distinct('knowledge_subcategory')->where('obj_lang', Lng::db())->where('knowledge_category',$know_type_db)->orderBy("knowledge_subcategory","ASC")->get();
		return $dt;
    }

    function getCategory($know_sub_category){
    	$dt = DB::table('knowledge_list')->where('obj_lang', Lng::db())->where('knowledge_subcategory',$know_sub_category)->where('obj_lang',Lng::db())->where('valid_date','<=',date("Y-m-d H:i:s"))->orderBy("knowledge_subcategory","ASC")->orderBy("valid_date","DESC")->limit(10)->get();
		return $dt;
    }

    function getRelatedData($mom_type){
    	$dt = DB::table('trick_mom')->where('type_mom',$mom_type)->where('obj_lang',Lng::db())->orderBy("obj_published_date","DESC")->get();
    	return $dt;
    }

    function getRelatedQuestionData($qa_type){
    	$dt = DB::table('question_article')->where('question_type',$qa_type)->where('valid_date','<=',date("Y-m-d H:i:s"))->orderBy('priority','ASC')->orderBy("valid_date","DESC")->limit(6)->get();
    	return $dt;
    }

}