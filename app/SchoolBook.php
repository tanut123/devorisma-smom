<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;

class SchoolBook extends Model {

	protected $table = '';

	function game(){
		$dt = DB::table('school_book_game')->select('title','image','image_gen','file_pdf','file_pdf_gen')->where('obj_lang', Lng::db())->orderBy("priority","asc")->get();
		return $dt;
	}
	function game_single($id){
		$dt = DB::table('school_book_game')->select('title','image','image_gen','file_pdf','file_pdf_gen')->where('obj_lang', Lng::db())->where("school_book_game_id",$id)->get();
		return $dt;
	}

	function diy(){
		$dt = DB::table('school_book_diy')->select('title','image','image_gen','file_pdf','file_pdf_gen')->where('obj_lang', Lng::db())->orderBy("priority","asc")->get();
		return $dt;
	}
	function diy_single($id){
		$dt = DB::table('school_book_diy')->select('title','image','image_gen','file_pdf','file_pdf_gen')->where('obj_lang', Lng::db())->where("school_book_diy_id",$id)->get();
		return $dt;
	}

	function story(){
		$dt = DB::table('school_book_story')->select('title','image','image_gen','file_pdf','file_pdf_gen')->where('obj_lang', Lng::db())->orderBy("priority","asc")->get();
		return $dt;
	}
	function story_single($id){
		$dt = DB::table('school_book_story')->select('title','image','image_gen','file_pdf','file_pdf_gen')->where('obj_lang', Lng::db())->where("school_book_story_id",$id)->get();
		return $dt;
	}

    function music() {
        $dt = DB::table('school_book_song')->select('song_title_'.Lng::url(),'file_mp3','file_mp3_gen')->orderBy("priority","asc")->get();
        return $dt;
    }
    function music_single($id) {
        $dt = DB::table('school_book_song')->select('song_title_'.Lng::url(),'file_mp3','file_mp3_gen')->where("school_book_song_id",$id)->get();
        return $dt;
    }

    function getMusicMediaFile($uuid) {
        $dt = DB::table("school_book_song_media_file")->where("uuname",$uuid)->get();
        return $dt;
    }
}
