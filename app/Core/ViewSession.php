<?php

namespace App\Core;
class ViewSession {
    public static $search = array();
    public static $replace = array();
    public static function set($search,$replace) {
        self::$search[] = "[[".$search."]]";
        self::$replace[] = $replace;
    }
    public static function make($view_data) {
        return str_replace(self::$search,self::$replace,$view_data);
    }
    public static function viewSessionGlobal() {
        self::set("csrf_token",csrf_token());
        #echo (\Illuminate\Support\Facades\Cookie::get("member_signup") == 1 ? "true" : "false")
        $sess_data = \Illuminate\Support\Facades\Session::get("memberSignup");
        $current_path = str_replace(url(),"",\Illuminate\Support\Facades\URL::current());
        if (isset($sess_data[$current_path]) && $sess_data[$current_path] == 2) {
            self::set("signup_active_pic","img_deactive");
            self::set("signup_active_panel","false");
        } else {
            self::set("signup_active_pic","");
            self::set("signup_active_panel","true");
        }
        //$content['show_breastfeeding_popup'] = Session::get('breastfeedingPopup') ;

        if (\Illuminate\Support\Facades\Session::get('breastfeedingPopup') == false) {
            $bf_popup = 'if(localStorage.getItem("memberPopup") == null){
					        createdimPopup(popupAlert());
					    }';
            self::set("breastfeeding_popup",$bf_popup);
        } else {
            self::set("breastfeeding_popup","");
        }

        $browser_data = $_SERVER['HTTP_USER_AGENT'];
        $is_ie8 = false;
        if (preg_match("/MSIE 8/",$browser_data)){
            $is_ie8 = true;
        }
        if ($is_ie8 == true) {
            self::set("img_type","png");
        } else {
            self::set("img_type","svg");
        }
        $BASE_LANG = url()."/".\App\Core\Lng::url()."/";
        $svg_prefix = "";
        $browser_data = $_SERVER['HTTP_USER_AGENT'];
        if (!preg_match("/MSIE 9/",$browser_data)){
            $svg_prefix = $BASE_LANG."home";

        }
        self::set("svg_prefix",$svg_prefix);
        $sign_in_panel_html = "";
        $webMemberId = \Illuminate\Support\Facades\Session::get("webMemberId");
        if ($webMemberId == "") {
            $sign_in_panel_html = \App\Core\LoginPanelGen::genLoginForm();
        } else {
            $sign_in_panel_html = \App\Core\LoginPanelGen::genMemberForm(\Illuminate\Support\Facades\Session::get("webMemberName"),DateLang::formatDateLang(\Illuminate\Support\Facades\Session::get("webMemberLastLogin"),"%d %b %Y %H:%M"));
        }
        $member_web_display_name = \Illuminate\Support\Facades\Session::get("web_member_display_name");
        $member_web_email = \Illuminate\Support\Facades\Session::get("web_member_email");
        if (\Illuminate\Support\Facades\Session::get("new_pass_mode") == "expire") {
            self::set("password_change_header",trans("password.PASSWORD_EXPIRE"));
        } else if(\Illuminate\Support\Facades\Session::get("new_pass_mode") == "reset") {
            self::set("password_change_header",trans("password.PASSWORD_CHANGE"));
        }
        self::set("member_web_display_name",(strlen($member_web_display_name ) > 0 ? $member_web_display_name  :""));
        self::set("member_web_email",(strlen($member_web_email ) > 0 ? $member_web_email  :""));
        self::set("login_panel",$sign_in_panel_html);
        self::set("captcha_date",date("YmdHis"));
    }
}