<?php
namespace App\core;
use PHPMailer;

class OMMail extends PHPMailer
{
	public $Subject;
	public $FromName;
	public $From;
	public $Body;
	public $AltBody;
	public $ErrorInfo;
	public $CharSet;

	public $MessageId;
	public $TransactionalGroupName;
	public $ToName;
	public $To;
	public $ReplyName;
	public $Reply;
	public $TemplateKey;
	public $ResponMessageId;

	public $Host;
	public $Username;
	public $Password;
	public $Port;
	private $smarty = null;
	public function __construct($exceptions = false) {
		if(env('ENABLE_TAXIMAIL_API') == true){
			$this->Host = env('MAIL_HOST');
			$this->Username = env('MAIL_USERNAME');
			$this->Password = env('MAIL_PASSWORD');
			$this->Port = env('MAIL_PORT');
			$this->CharSet = "UTF-8";

			$this->MessageId = "";
			$this->TransactionalGroupName = "";
			$this->Subject = "";
			$this->To = "";
			$this->ToName = "";

		}else{
			$this->exceptions = ($exceptions == true);
			$this->IsSMTP();

			$this->Host = env('MAIL_HOST'); 			// specify main and backup server
			$this->Username =  env('MAIL_USERNAME');  // SMTP username
			if($this->Username == ""){
				$this->SMTPAuth = false;     		// turn on SMTP authentication
			}else{
				$this->SMTPAuth = true;     		// turn on SMTP authentication
			}
			$this->Password =  env('MAIL_PASSWORD'); 	// SMTP password
			$this->Port = env('MAIL_PORT'); 			// SMTP password
			$this->Timeout = 300;
			$this->SMTPKeepAlive = true;		// use with $this->SmtpClose();
			$this->Mailer   = "smtp";
			$this->CharSet = "UTF-8";
		}
	}
	private function getTaximailSessionID(){
		$set_curl = array(CURLOPT_RETURNTRANSFER=>1, CURLOPT_POST=>TRUE,CURLOPT_ENCODING, $this->CharSet);
		$url = $this->Host . "/v2/user/login";
		$params = array();
		$params['username'] = $this->Username;
		$params['password'] = $this->Password;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if (!strncmp($url, 'https', 5)){
			curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
			curl_setopt($ch, CURLOPT_PORT, 443);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		}
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params) );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
		$server_output = curl_exec ($ch);
		curl_close ($ch);
		return json_decode($server_output, true);
	}

	private function sendTaximail(){
		$ResultLogin = $this->getTaximailSessionID();
		$ParamsMail = array();
		$ParamsMail["session_id"] = (isset($ResultLogin["data"]["session_id"]) && $ResultLogin["data"]["session_id"]!= "")?$ResultLogin["data"]["session_id"]:"";
		if(count($this->attachment) > 0){
			foreach ($this->attachment as $key => $value) {
				$path = array();
				$path[0]["filename"] = $value[2];
				$path[0]["path"] = env('BASE_URL').str_replace(ROOT_DIR,"", $value[0]);
				$ParamsMail["attachment"] = json_encode($path);
			}
		}
		$ParamsMail["message_id"] = $this->MessageId;
		$ParamsMail["transactional_group_name"] = !empty($this->TransactionalGroupName)?$this->TransactionalGroupName:$this->Subject;
		$ParamsMail["subject"] = $this->Subject;
		$ParamsMail["to_name"] = $this->ToName;
		$ParamsMail["to_email"] = $this->To;
		$ParamsMail["from_name"] = $this->FromName;
		// $ParamsMail["from_email"] = $this->From;
		$ParamsMail["from_email"] = env('MAIL_USERNAME');
		if(!empty($this->Reply)){
			$ParamsMail["reply_name"] = $this->ReplyName;
			$ParamsMail["reply_email"] = $this->Reply;
		}
		if(!empty($this->TemplateKey)){
			$ParamsMail["template_key"] = $this->TemplateKey;
		}
		$ParamsMail["content_html"] = !empty($this->Body)?$this->Body:'';
		$ParamsMail["content_plain"] = !empty($this->AltBody)?$this->AltBody:'';

		$set_curl = array(CURLOPT_RETURNTRANSFER=>1, CURLOPT_POST=>TRUE,CURLOPT_ENCODING, 'UTF-8');
		$url = $this->Host . "/v2/transactional";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if (!strncmp($url, 'https', 5)){
			curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
			curl_setopt($ch, CURLOPT_PORT, 443);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		}
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($ParamsMail) );
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);
		curl_close ($ch);

		return json_decode($server_output, true);
	}

	public function sendMail(){
		if(env('ENABLE_TAXIMAIL_API') == true){
			$res = false;
			$error = array();
			if(count($this->to) > 0){
				foreach ($this->to as $key => $value) {
					$this->To = isset($value[0])?$value[0]:"";
					$this->ToName = isset($value[1])?$value[1]:"";

					$status_send = $this->sendTaximail();
					if(isset($status_send["status"]) && $status_send["code"] == 202 && $status_send["status"] == "success"){
						$res = true;
					}
					if(isset($status_send["data"]) && isset($status_send["data"]["message_id"])){
						$ResponMessageId = $status_send["data"]["message_id"];
					}
					if(isset($status_send["err_msg"])){
						$this->ErrorInfo = $status_send["err_msg"];
						$res = false;
					}
				}
			}

			return $res;
		}else{
			return $this->Send();
		}

	}
	public function getResponMessageID(){
		return $this->ResponMessageId;
	}
}