<?php
namespace App\Core;

class LoginPanelGen {
    public static function genLoginForm() {
        $html = "<div class='case1'>
				<div class='left_side'>
				</div>
				<div class='left_side_mobile'>
				</div>
				<div class='right_side'>
						<span class='sign_in FBold anti_alias fs22' id='sign_in_case'>
                ".trans("login.SIGN_IN")."
            </span>
				</div>
			</div>
			<div class='case2'>
				<div class='input_panel textinline'>
					<div class='FBold fs22 anti_alias textinline text4'>
				".trans("login.TEXT4")."
					</div>
					<div class='FX fs20 email_panel anti_alias textinline'>
						<input type='text' class='email_inp b_inp' placeholder='".trans("login.EMAIL_PLACEHOLDER")."' id='email_inp' tabindex='1'>
					</div>
					<div class='FX fs20 pwd_panel anti_alias textinline'>
						<input type='password' class='pwd_inp b_inp' placeholder='".trans("login.PASSWORD_PLACEHOLDER")."' id='pwd_inp' tabindex='2' autocomplete='off'>
					</div>
					<div class='FX fs20 forget_box anti_alias textinline oneline'>
				<a href='".url()."/".Lng::url()."/password/forgot'>".trans("login.FORGOT_PASSWORD")."</a>
					</div>
					<div class='textinline oneline' id='sign_in_btn'>
					<input type='button' class='FX login_btn fs20 anti_alias' id='sign_in_btn' value='".trans("login.SIGN_IN")."' tabindex='3'/>
					</div>
            	</div>
            	<div class='facebook_panel textinline'>
					<div class='FX fs20 or_box anti_alias textinline'>
				".trans("login.OR_TEXT")."
					</div>
					<div class='facebook textinline'>

					</div>
				</div>
			</div>";
        return $html;
    }
    public static function genMemberForm($display_name,$last_login) {
		$member_controller = new \App\MemberWeb();
		$bookmarkCount = (array)$member_controller->countBookmark();
		//$bookmarkCount["count"] = 8;
/*		var_dump($bookmarkCount);
		exit();*/
        $html = "<div class='case3'>
				<div class='left_side user_display'>
					<div class='icon'>

					</div>
					<div class='detail fs22 FX anti_alias' title='". trans("login.GREATING_WORD") ." ". htmlentities($display_name, ENT_QUOTES, "UTF-8") ." (เข้าสู่ระบบครั้งสุดท้ายเมื่อ ". $last_login . ")'>
            			 <span class='header-name-title'>". trans("login.GREATING_WORD") ." ". htmlentities($display_name, ENT_QUOTES, "UTF-8") ."</span> <span class='last-login-text'>(เข้าสู่ระบบครั้งสุดท้ายเมื่อ ". $last_login . ")</span>
            		</div>
				</div>
				<div class='right_side'>
					<div class='textinline profile_btn arrow'>
						<a class='login' href='".url()."/".Lng::url()."/register/form/profile'>
							<div class='icon'>

							</div>
							<div class='detail fs22 FX anti_alias'>
					".trans("login.PROFILE")."
							</div>
						</a>
					</div>
					<div class='line textinline'>
					</div>
					<div class='textinline bookmark_btn arrow'>
						<a class='login' href='".url()."/".Lng::url()."/bookmark'>
							<div class='icon'>

							</div>
							<div class='detail fs22 FX anti_alias'>
					".trans("login.BOOKMARK")." <span class='FBold anti_alias' id='bookmark_count'>(".$bookmarkCount["count"].")</span>
							</div>
						</a>
					</div>
					<div class='line textinline'>
					</div>
					<div class='logout_panel textinline' id='signout_btn'>
						<div class='icon'>

						</div>
						<div class='detail fs20 FX anti_alias'>
				".trans("login.SIGN_OUT")."
						</div>
					</div>
				</div>
			</div>";
        return $html;
    }
}