<?php namespace App\Core;


Class DateLang{

	public static function getMonth($monthy,$abbr = false){
		$month = array();
		if(!$abbr){
			$month["tha"][1]='มกราคม';
			$month["tha"][2]='กุมภาพันธ์';
			$month["tha"][3]='มีนาคม';
			$month["tha"][4]='เมษายน';
			$month["tha"][5]='พฤษภาคม';
			$month["tha"][6]='มิถุนายน';
			$month["tha"][7]='กรกฏาคม';
			$month["tha"][8]='สิงหาคม';
			$month["tha"][9]='กันยายน';
			$month["tha"][10]='ตุลาคม';
			$month["tha"][11]='พฤศจิกายน';
			$month["tha"][12]='ธันวาคม';
		}else{
			$month["tha"][1]='ม.ค.';
			$month["tha"][2]='ก.พ.';
			$month["tha"][3]='มี.ค.';
			$month["tha"][4]='เม.ย.';
			$month["tha"][5]='พ.ค.';
			$month["tha"][6]='มิ.ย.';
			$month["tha"][7]='ก.ค.';
			$month["tha"][8]='ส.ค.';
			$month["tha"][9]='ก.ย.';
			$month["tha"][10]='ต.ค.';
			$month["tha"][11]='พ.ย.';
			$month["tha"][12]='ธ.ค.';
		}

		$month["jap"][1]='いちがつ';
		$month["jap"][2]='にがつ';
		$month["jap"][3]='さんがつ';
		$month["jap"][4]='しがつ';
		$month["jap"][5]='ごがつ';
		$month["jap"][6]='ろくがつ';
		$month["jap"][7]='しちがつ';
		$month["jap"][8]='はちがつ';
		$month["jap"][9]='くがつ';
		$month["jap"][10]='じゅうがつ';
		$month["jap"][11]='じゅういちがつ';
		$month["jap"][12]='じゅうにがつ';

		$month["chn"][1]='一月';
		$month["chn"][2]='二月';
		$month["chn"][3]='三月';
		$month["chn"][4]='四月';
		$month["chn"][5]='五月';
		$month["chn"][6]='六月';
		$month["chn"][7]='七月';
		$month["chn"][8]='八月';
		$month["chn"][9]='九月';
		$month["chn"][10]='十月';
		$month["chn"][11]='十一月';
		$month["chn"][12]='十二月';

		if(self::getLang() == "eng"){
			if(!$abbr){
				return date('F', mktime(0, 0, 0, $monthy, 10));
			}else{
				return date('M', mktime(0, 0, 0, $monthy, 10));
			}
		}else{
			return $month[self::getLang()][$monthy];
		}
	}

	public static function getWeekly($weekly,$abbr = false){
		$weekday = array();

		if(!$abbr){
			$weekday["tha"][0]='อาทิตย์';
			$weekday["tha"][1]='จันทร์';
			$weekday["tha"][2]='อังคาร';
			$weekday["tha"][3]='พุธ';
			$weekday["tha"][4]='พฤหัสบดี';
			$weekday["tha"][5]='ศุกร์';
			$weekday["tha"][6]='เสาร์';

			$weekday["eng"][0]='Sunday';
			$weekday["eng"][1]='Monday';
			$weekday["eng"][2]='Tuesday';
			$weekday["eng"][3]='Wednesday';
			$weekday["eng"][4]='Thursday';
			$weekday["eng"][5]='Friday';
			$weekday["eng"][6]='Saturday';

			$weekday["jap"][0]='日曜日';
			$weekday["jap"][1]='月曜日';
			$weekday["jap"][2]='火曜日';
			$weekday["jap"][3]='水曜日';
			$weekday["jap"][4]='木曜日';
			$weekday["jap"][5]='金曜日';
			$weekday["jap"][6]='土曜日';

			$weekday["chn"][0]='星期天';
			$weekday["chn"][1]='星期一';
			$weekday["chn"][2]='星期二';
			$weekday["chn"][3]='星期三';
			$weekday["chn"][4]='星期四';
			$weekday["chn"][5]='星期五';
			$weekday["chn"][6]='星期六';

		}else{
			$weekday["tha"][0]='อา';
			$weekday["tha"][1]='จ';
			$weekday["tha"][2]='อ';
			$weekday["tha"][3]='พ';
			$weekday["tha"][4]='พฤ';
			$weekday["tha"][5]='ศ';
			$weekday["tha"][6]='ส';

			$weekday["eng"][0]='S';
			$weekday["eng"][1]='M';
			$weekday["eng"][2]='T';
			$weekday["eng"][3]='W';
			$weekday["eng"][4]='T';
			$weekday["eng"][5]='F';
			$weekday["eng"][6]='S';

			$weekday["jap"][0]='日';
			$weekday["jap"][1]='月';
			$weekday["jap"][2]='火';
			$weekday["jap"][3]='水';
			$weekday["jap"][4]='木';
			$weekday["jap"][5]='金';
			$weekday["jap"][6]='土';

			$weekday["chn"][0]='日';
			$weekday["chn"][1]='一';
			$weekday["chn"][2]='二';
			$weekday["chn"][3]='三';
			$weekday["chn"][4]='四';
			$weekday["chn"][5]='五';
			$weekday["chn"][6]='六';
		}

		return $weekday[self::getLang()][$weekly];
	}

	public static function formatDateLang($date_db,$pattern="%d %b %Y"){
		if (strtotime($date_db) == -1){
			return "";
		}
			$day = strftime("%d", strtotime($date_db));
			$month = intval(strftime("%m", strtotime($date_db)));
			$year = intval(strftime("%Y", strtotime($date_db)));
			if(self::getLang() == "tha"){
				$year += 543;
			}
			$weekday = intval(strftime("%w", strtotime($date_db)));
			$pattern_tmp = $pattern;
			$i=0;

			while(strpos($pattern_tmp,"%") !== false){
				$pattern_array[$i] = substr($pattern_tmp,strpos($pattern_tmp,"%"),2);
				$pattern_tmp = str_replace($pattern_array[$i],"", $pattern_tmp);
				$i++;
			}
			//var_dump($pattern_array);
			foreach($pattern_array as $value){
				if($value == '%a'){
					$value_return = self::getWeekly((int)$weekday,true);
				}else if($value == '%A'){
					$value_return = self::getWeekly((int)$weekday);
				}else if($value == '%b'){
					$value_return = self::getMonth((int)$month,true);
				}else if($value == '%B'){
					$value_return = self::getMonth((int)$month);
				}else if($value == '%y'){
					$value_return = substr($year,-2);
				}else if($value == '%Y'){
					$value_return = $year;
				}else{
					$value_return = strftime($value, strtotime($date_db));
				}
				// var_dump($value_return);
				$pattern = str_replace($value, $value_return, $pattern);
			}
			return $pattern;
		// }
	}

	private static function getLang(){
		return Lng::db(true);
	}
}




?>