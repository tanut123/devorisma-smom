<?php
namespace App\core;

class OMPage {
	public $breadcrumb = "";
	public $page_title = "";
	public $activeMenu = "";
	public $sharedImage;
	public $var = array();
	function getVar($name) {
		if(!isset($this->var[$name])){
			$this->var[$name] = OMSiteSetting::val($name);
			if(!isset($this->var[$name])){
				return $name;
			}
		}
		return $this->var[$name];
	}

	function setBreadcrumb($breadcrumb) {
		$i =0 ;
		$arrVal = array();
		foreach ($breadcrumb as $key => $item) {
			if($i != 0){
				$this->breadcrumb .= " / ";
				$arrVal[] = $key;
			}
			if($i == count($breadcrumb)-1){
				$this->breadcrumb .= "<span>".OM::TrimWithDot($key,100)."</span>";
			}else{
				$this->breadcrumb .= "<a href=".$item." class='link-hover'>".OM::TrimWithDot($key,100)."</a>";
			}
			$i++;
		}
		krsort($arrVal);
		$this->setTitle($arrVal);
	}
	function getTitle() {
		$page_default = $this->getVar("window_title");
		if($this->page_title != ""){
			$title = $this->page_title.$page_default;
		}else{
			$title = $page_default;
		}
		return $title;
	}

	function setTitle($title) {
		if(is_array($title) && $title != ""){
			$this->page_title = "";
			foreach ($title as $key => $valTitle) {
				$this->page_title .= $valTitle;
				if($key == 0){
					$this->page_title .= " | ";
				}else{
					$this->page_title .= " / ";
				}
			}
		}
	}

	function setActiveMenu($title) {
		$this->activeMenu = $title;
	}

	function addSharedImage($path) {
		$this->sharedImage[] = $path;
	}

}