<?php
namespace App\Core;
class SmallCssPrint {
    function __construct() {

    }
    public static function loadCss($file) {
        $file_array = explode(",",$file);
        $r_data = "";
        foreach ($file_array as $value) {
            if (file_exists(public_path()."/css_scr/".$value.".css")) {
                $r_data .= file_get_contents(public_path()."/css_scr/".$value.".css");
            } else {
                return "CSS File Not Found.";
            }
        }
        $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $r_data);
        $buffer = preg_replace('/\s+/'," ",$buffer);
        $buffer = str_replace(': ', ':', $buffer);
        $buffer = str_replace(array("\n","\r\n","\t"), '', $buffer);

        return "<style>".$buffer."</style>";
    }
}