<?php namespace App\core;

use App;
use Cache;
use App\Core\SettingManagement;
use App\Core\TranslateManagement;

class ClearCache {

	public static function clear(){
		Cache::flush();
        $setting = new SettingManagement();
		$value = $setting->flush();

		$manage = new TranslateManagement();
		$value = $manage->writeTranslate();
		if($value){
        	return  true;
		}
	}


}
?>