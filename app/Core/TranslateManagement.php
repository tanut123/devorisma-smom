<?php namespace App\core;

use App\Translate;

class TranslateManagement {

	static function getTranslate(){
		$ds = new Translate();
		$dt = $ds->getTranslate();
		$translate = array();
		if (count($dt)>=0) {
			foreach($dt as $row) {
				$scope = trim($row->translate_scope);
				if ($scope == '') {
					$scope = 'core';
				}
				$lang = strtolower($row->obj_lang);
				$name = $row->translate_name;
				$value = $row->translate_text;

				$translate[$lang][$scope][$name] = $value;
			}
		}
		return $translate;
	}

	static function writeTranslate(){
		$dataTranslate = self::getTranslate();
		$httpdocs = base_path() . "/resources/lang/";
		foreach ($dataTranslate as $key => $valTranslate) {
			$langFolder = self::checkLang($key);
			$rootLang = $httpdocs.$langFolder;
			self::createFolder($rootLang);
			if(is_array($valTranslate)){
				foreach ($valTranslate as $keyName =>  $valData) {
					$filepath = $rootLang . "/" . $keyName . ".php";
					self::unlinkFile($filepath);
					$dataPhp = "<?php\n\n";
					$dataPhp .= "return [\n";
					$dataPhp .= self::createListTranslate($valData);
					$dataPhp .= "];";
					self::createFile($filepath,$dataPhp);
					$arrPath[] = $filepath;
				}
			}else{
				return null;
			}
		}
		return $arrPath;
	}

	static function createListTranslate($valData){
		if(is_array($valData)){
			$data = "";
			foreach ($valData as $key => $val) {
				$data .= "\t'$key' => '". htmlentities($val, ENT_QUOTES) . "',\n";
			}
			return $data;
		}
		return "";
	}

	static function checkLang($key){
		$langFolder = "th";
		if($key == "eng"){
			$langFolder = "en";
		}
		return $langFolder;
	}

	static function unlinkFile($path){
		if(is_file($path)){
		 	unlink($path);
		}
	}

	static function createFolder($name){
		if(!is_dir($name)){
			mkdir($name, 0777, true);
			return true;
		}
		return false;
	}

	static function createFile($path, $data){
		$fp = fopen($path, "w");
        fwrite($fp, $data);
        fclose ($fp);
	}
}
?>