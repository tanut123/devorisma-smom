<?php namespace App\Core;

use Meta;
use App;
use View;
use Config;
use App\SiteSetting;
use App\Core\SettingManagement;
use App\Visitor;
use Cache;
use Request;

class Controller extends App\Http\Controllers\Controller {
	public $BASE_LANG = "";

	public function __construct()
    {
    	$setting = new SettingManagement();
    	$visitor = new Visitor();
        # Default META
        ViewSession::set("VISITOR",number_format($visitor->getVisitor()));

        Meta::title($setting->get("TITLE"));
        Meta::meta('robots', 'index,follow');
        Meta::meta('keywords', $setting->get("KEYWORDS"));
        Meta::meta('description', $setting->get("DESCRIPTION"));
        Meta::meta('site_name', $setting->get("SITE_NAME"));
        Meta::meta('image', asset(env('BASE_CDN', '') . '/images/locked-logo.jpg'));

        # BASE LANG
        $this->BASE_LANG = url() . "/" . App::getLocale(). "/";
        View::share('BASE_LANG', $this->BASE_LANG);
        View::share('BASE_CDN', env('BASE_CDN', ''));
        View::share('BASE_LANG_TH', $this->urlLang('th'));
        View::share('BASE_LANG_EN', $this->urlLang('en'));
        View::share('ANALYTIC_CODE', $setting->get("ANALYTIC_CODE"));
        View::share('FACEBOOK_APPID' , $setting->get("FACEBOOK_APPID"));


    }

    function urlLang($lang){
  		//$string = Request::path();
		// $pattern = '/(th|en)/';
		// $replacement = $lang;
		// return url() . "/" . preg_replace($pattern, $replacement, $string);
		return url() . "/" . $lang . "/home";
    }




}
