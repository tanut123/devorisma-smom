<?php namespace App\Core;

use App;

class Lng {

	public static  function url(){
    	return App::getLocale();
    }
    public static  function db($lower = false){
    	$lang = "";

    	$locales = Config('app.locales');
    	$lang = $locales[self::url()];

    	if($lower){
    		$lang = strtolower($lang);
    	}

    	return $lang;
    }


}
