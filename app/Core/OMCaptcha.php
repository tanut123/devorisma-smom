<?php
namespace App\Core;
/*$random_text = random_text(4);
session_security_update($random_text,$session_id);
header('Content-type: image/png');
$im = make_image($random_text);
imagepng($im);
imagedestroy($im);*/
class OMCaptcha {
    public $background = "";
    public $size = array();
    public $text;
    public $image;
    public $font_file;
    public $font_size = 15;
    public $bg_color = array(200,200,200);
    function __construct($width,$height,$font_file,$font_size,$length) {
        $size[0] = $width;
        $size[1] = $height;
        $this->size = $size;
        $this->font_file = $font_file;
        $this->text = $this->randomText($length);
        $this->font_size = $font_size;
    }
    public function setBackground($background) {
        $this->background = $background;
    }
    public function setBackgroundColor($color) {
        $hex = str_replace("#", "", $color);

        if(strlen($hex) == 3) {
            $r = hexdec(substr($hex,0,1).substr($hex,0,1));
            $g = hexdec(substr($hex,1,1).substr($hex,1,1));
            $b = hexdec(substr($hex,2,1).substr($hex,2,1));
        } else {
            $r = hexdec(substr($hex,0,2));
            $g = hexdec(substr($hex,2,2));
            $b = hexdec(substr($hex,4,2));
        }
        $rgb = array($r, $g, $b);
        $this->bg_color = $rgb;
    }
    public function makeImage(){
        $im = imagecreate($this->size[0], $this->size[1]);
        $bg = imagecolorallocate($im, $this->bg_color[0], $this->bg_color[1], $this->bg_color[2]);
        $textcolor = imagecolorallocate($im, 0, 0, 255);

        $grey = imagecolorallocate($im, rand(0,150), rand(0,150), rand(0,150));
        //imagestring($im,20,30, 12, 'FGDS', $textcolor);
        $rand_rot = rand(-10,10);
        if (!file_exists($this->font_file)) {
            echo "Error Font File not found:".$this->font_file;
            return false;
        }
        //$rand_rot = -1;
        $text_box = imagettfbbox($this->font_size,$rand_rot,$this->font_file,$this->text);

        //print_r($text_box);
        $y_pos[] = $text_box[1];
        $y_pos[] = $text_box[3];
        $y_pos[] = $text_box[5];
        $y_pos[] = $text_box[7];

        $x_pos[] = $text_box[0];
        $x_pos[] = $text_box[2];
        $x_pos[] = $text_box[4];
        $x_pos[] = $text_box[6];

        $lower = abs(min($y_pos));

        $upper = abs(max($y_pos));

        $left = abs(min($x_pos));

        $right = abs(max($x_pos));
        $box_size_width = $left + $right;
        $box_size_height  = $lower + $upper;
/*        var_dump($lower,$upper);
        exit();*/
        $offset = $box_size_height;
        if ($rand_rot < 0) {
            $offset = ($box_size_height-($text_box[5]-$text_box[7]));
        }
        $x = ($this->size[0] - $box_size_width) / 2;
        $y = (($this->size[1] - $box_size_height) / 2) + $offset;
        //var_dump($box_size_width,$box_size_height,$x,$y,$this->size[1]);
        //exit();
        if ($this->background != "") {
            if (!file_exists($this->background)) {
                echo "Error Font File not found:".$this->background;
                return false;
            }
            $src = imagecreatefrompng($this->background);
            //$transparent_color = imagecolorallocate($src, 0, 0, 0);
            //imagecolortransparent($src, $transparent_color);
            imagecopyresampled($im, $src,0, 0, rand(127,0), rand(50,0), 227, 75, 227, 75);
        }
        imagettftext($im, $this->font_size, $rand_rot,$x,$y, $grey, $this->font_file, $this->text);
        return $im;
    }
    public function randomText($lenght) {
        $textArray = array("2","3","4","5","6","7","8","9","B","C","D","F","G","H","J","K","M","N","P","Q","R","S","T","V","W","X","Y","Z");
        $r_str = "";
        $i = 0;
        while ($i < $lenght) {
            $rand_num = rand(0,count($textArray)-1);
            $r_str.= $textArray[$rand_num];
            $i++;
        }
        return $r_str;
    }
}

?>