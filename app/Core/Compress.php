<?php
namespace App\Core;
use Log;
use Patchwork\JSqueeze;
use Illuminate\Http\Request;

class Compress {
    public static $debug_mode = true;
    public static $css_list = array();
    public static $js_list = array();
    public static $css_index = array();
    public static $js_index = array();
    function __construct() {

    }
    public static function read_css($file,$sequence_load="") {
        $file_data = explode(",",$file);
        if (count($file_data) > 0) {
            foreach ($file_data as $val) {
                self::check_and_add_css($val,$sequence_load);
            }
        } else {
            //Log::error("Compress CSS Zero argument.");
            self::writeLog("error","Compress CSS Zero argument.");
        }
    }
    public static function check_and_add_css($file,$sequence_load="") {
        if (isset(self::$css_index[$file])) {
            foreach (self::$css_list as $key1 => $val1) {
                foreach ($val1 as $key2 => $val2) {
                    if ($val2 == $file && ($key1 == 999 || $key1 > $sequence_load)) {
                        unset(self::$css_list[$key1][$key2]);
                        unset(self::$css_index[$file]);
                    }
                }
            }
        }
        if (empty(self::$css_index[$file])) {
            if (file_exists(base_path()."/resources/assets/css/".$file.".css")) {
                if (@$sequence_load != "") {
                    self::$css_list[$sequence_load][] = $file;
                }else {
                    self::$css_list[999][] = $file;
                }
                self::$css_index[$file] = true;
                //self::$css_list[] = $file;
            } else {
                //Log::error("Compress CSS file not exists: ".$file);
                if ($file != "") {
                    self::writeLog("error","Compress CSS file not exists: ".$file);
                }
            }
            self::$css_index[$file] = true;
        } else {
            //Log::warning("Duplicate load CSS file: ".$file);
            self::writeLog("warning","Duplicate load CSS file: ".$file);
        }
    }
    public static function read_js($file,$sequence_load="") {
        $file_data = explode(",",$file);
        if (count($file_data) > 0) {
            foreach ($file_data as $val) {
                self::check_and_add_js($val,$sequence_load);
            }
        } else {
            //Log::error("Compress JS Zero argument.");
            self::writeLog("error","Compress JS Zero argument.");
        }
    }
    public static function check_and_add_js($file,$sequence_load="") {
        if (isset(self::$js_index[$file])) {
            foreach (self::$js_list as $key1 => $val1) {
                foreach ($val1 as $key2 => $val2) {
                    if ($val2 == $file && ($key1 == 999 || $key1 > $sequence_load)) {
                        unset(self::$js_list[$key1][$key2]);
                        unset(self::$js_index[$file]);
                    }
                }
            }
        }
        if (empty(self::$js_index[$file]) || $sequence_load != "") {
            if (file_exists(base_path()."/resources/assets/js/".$file.".js")) {
                if (@$sequence_load != "") {
                    self::$js_list[$sequence_load][] = $file;
                }else {
                    self::$js_list[999][] = $file;
                }
                self::$js_index[$file] = true;
            } else {
                if ($file != "") {
                    //Log::error("Compress JS file not exists: ".$file);
                    self::writeLog("error", "Compress JS file not exists: " . $file);
                }
            }
        } else {
            //Log::warning("Duplicate load JS file: ".$file);
            self::writeLog("error","Duplicate load JS file: ".$file);
        }
    }
    public static function write_css() {
        $file_str = url()."/css/";
        if (count(self::$css_list) > 0) {
            $file_css = "";
            ksort(self::$css_list);
//            $test = "";
            foreach (self::$css_list as $key => $val) {
//                $test .= ">".$key;
                if (is_array($val)) {
                    if (isset($bg_flag)) {
                        $file_css.= ",";
                    }
                    $file_css.= implode(",",$val);
                    $bg_flag = true;
                } else {
                    if (strlen($val) > 0) {
                        if (isset($bg_flag)) {
                            $file_css.= ",";
                        }
                        $file_css.= $val;
                        $bg_flag = true;
                    }
                }
            }
            /*            echo $test;
                        exit();*/
            $file_name = self::genFile("css",$file_css);
            return $file_str.$file_name;
        } else {
            //Log::error("Zero JS file Compress");
            return $file_str."blank_file.css";
            //self::writeLog("error","Zero CSS file Compress");
        }
/*        foreach (self::$css_list as $file_data) {
            if (count(self::$css_list) > 0) {
                $file_css = implode(",",self::$css_list);
                return $file_str.$file_css.".css";
            } else{
                //Log::error("Zero CSS file Compress");
                self::writeLog("error","Zero CSS file Compress");
            }
        }*/
    }
    public static function write_js($script_tag = false) {
        $file_str = url()."/js/";
        if (count(self::$js_list) > 0) {
            $file_js = "";
            ksort(self::$js_list);
//            $test = "";
            foreach (self::$js_list as $key => $val) {
//                $test .= ">".$key;
                if (is_array($val)) {
                    if (isset($bg_flag)) {
                        $file_js.= ",";
                    }
                    $file_js.= implode(",",$val);
                    $bg_flag = true;
                } else {
                    if (strlen($val) > 0) {
                        if (isset($bg_flag)) {
                            $file_js.= ",";
                        }
                        $file_js.= $val;
                        $bg_flag = true;
                    }
                }
            }
            $file_name = self::genFile("js",$file_js);
            if ($script_tag == false) {
                return $file_str.$file_name;
            } else {
                return "<script src='".$file_str.$file_js.".js' type='text/javascript'></script>";
            }
        } else {
            //Log::error("Zero JS file Compress");
            //self::writeLog("error","Zero JS file Compress");
        }
    }
    public static function genFile($type,$data_input,$debug_mode=false) {
        if (self::$debug_mode == true) {
            $debug_mode = true;
        }
        $file_explode = explode(",",$data_input);
        $source_path = base_path()."/resources/assets";
        $stock_path = public_path();
        $ext = "";
        if ($type == "js") {
            $source_path .= "/js/";
            $stock_path .= "/js";
            $ext = ".js";
        } else {
            $source_path .= "/css/";
            $stock_path .= "/css";
            $ext = ".css";
        }
        //$file_name_md5 = md5($data_input);
        $file_name = $data_input.$ext;
        $file_name = str_replace("/","-",$file_name);
        if (!file_exists($stock_path."/".$file_name) || $debug_mode == true) {
            foreach ($file_explode as $val ) {

                if (file_exists($source_path.$val.'.'.$type)) {
                    $buffer = file_get_contents($source_path.$val.'.'.$type);
                } else {
                    self::writeLog("error","File Not Found :".$source_path.$val.'.'.$type);
                    return abort(404);
                }
                if ($type=='js') {
                    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
                    if (!preg_match("/.min/",$val)) {
                        $buffer = str_replace("  ","",$buffer);
                        $minify = new JSqueeze();
                        $buffer = $minify->squeeze($buffer,true,false,false);
                    }
                }
                if ($type=='css') {
                    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
                    $buffer = preg_replace('/\s+/'," ",$buffer);
                    $buffer = str_replace(': ', ':', $buffer);
                    $buffer = str_replace(array("\n","\r\n","\t"), '', $buffer);
                }

                $print_out_data[$val.'.'.$type] = trim($buffer);
            }
            $minify_data = "";
            if ($type=='js') {
                if ($debug_mode == true) {
                    foreach ($print_out_data as $key2 => $val2) {
                        $minify_data .= "/*".$key2."*/".$val2."\n";
                    }
                } else {
                    foreach ($print_out_data as $key2 => $val2) {
                        $minify_data .= $val2;
                    }
                }
            } else {
                $minify_data = implode(" ",$print_out_data);
            }
            if (!file_exists($stock_path)) {
                mkdir($stock_path,0777);
            }
/*            $file_list = scandir($stock_path);
            foreach ($file_list as $val) {
                if (is_file($stock_path."/".$val)) {
                    if (preg_match("/^".$file_name_md5."/",$val)) {
                        unlink($stock_path."/".$val);
                    }
                }
            }*/

            $base_cdn = env('BASE_CDN', '');
	       if($base_cdn != ''){
		        $array_search = ["/(\.\.\/)+(images)/"];
			    $array_match = [$base_cdn."/images"];
			    $minify_data = preg_replace($array_search, $array_match, $minify_data);
	       }
            if (file_put_contents($stock_path."/".$file_name,$minify_data)) {
                chmod($stock_path."/".$file_name, 0644);
                return $file_name;
            } else {
                self::writeLog("error","Write file Error :".$type."[".$data_input."]");
            }
        } else {
            return $file_name;
        }

    }
    public static function loadFile($type,$data_input){
        if ($data_input == "blank_file") {
            return "";
        }
        $file_explode = explode(",",$data_input);
        //self::setlastModified($file_explode,$type);
        $modify_data = "";
        $source_path = base_path()."/resources/assets";
        $stock_path = public_path();
        $ext = "";
        if ($type == "js") {
            $source_path .= "/js/";
            $stock_path .= "/js";
            $ext = ".js";
        } else {
            $source_path .= "/css/";
            $stock_path .= "/css";
            $ext = ".css";
        }
        //$file_name = "";
        //$temp_last_mod = "";
        /*foreach ($file_explode as $val) {
            if (file_exists($source_path.$val.$ext)) {
                $get_modify_date = filemtime($source_path.$val.$ext);
                $modify_data .= $get_modify_date;
                if (!isset($temp_last_mod) || $temp_last_mod < $get_modify_date) {
                    $temp_last_mod = $get_modify_date;
                }
            }
        }*/
        //echo Request::server('HTTP_IF_MODIFIED_SINCE');
        /*if (isset($last_modify_date) && strtotime($last_modify_date) >= $temp_last_mod) {
            header('HTTP/1.0 304 Not Modified');
            exit();
        }*/
        $file_name_md5 = md5($data_input);
        //$modify_data_md5 = md5($modify_data);
        $file_name = $file_name_md5.$ext;

        if (!file_exists($stock_path."/".$file_name) || env("ENV_MODE") == "DEV") {
            foreach ($file_explode as $val ) {


                if (file_exists($source_path.$val.'.'.$type)) {
                    $buffer = file_get_contents($source_path.$val.'.'.$type);
                } else {

                   // echo "File Not Found :".$source_path.$val.'.'.$type;
                   self::writeLog("error","File Not Found :".$source_path.$val.'.'.$type);
                    return abort(404);
                }

                if ($type=='js') {

                    /*$bg_crow = substr_count($buffer,"{");
                    $en_crow = substr_count($buffer,"}");
                    echo "console.log(".$bg_crow.",".$en_crow.",'".$val."');";*/

                    /*Cut Comment*/
                    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);

                    //$buffer = preg_replace('/\n\s+\/\/\s.*[\n|\r\n]/', '', $buffer);

                    if (!preg_match("/.min/",$val)) {
                        /*
                         $search_pattern = array('/\t/','/;\s*\n/','/{\s*\n/','/\n\s*}/','/,\s*\n/','/\n\s*\n/','/}\s*\n/','/\n\s*\./');
                        $replace_data = array("",";","{","}",",","\n","}",".");

                        $buffer = preg_replace($search_pattern,$replace_data,$buffer);
                        */

                        /*$buffer = preg_replace('/\t/',"",$buffer);
                        //$buffer = preg_replace('/;\s*\n/',";",$buffer);
                        $buffer = preg_replace('/{\s*\n/',"{",$buffer);
                        $buffer = preg_replace('/\n\s*}/',"}",$buffer);
                        $buffer = preg_replace('/,\s*\n/',",",$buffer);
                        $buffer = preg_replace('/\n\s*\n/',"\n",$buffer);
                        $buffer = preg_replace('/}\s*\n/',"}",$buffer);
                        $buffer = preg_replace('/\n\s*\./',".",$buffer);*/

                        $buffer = str_replace("  ","",$buffer);
                        $minify = new JSqueeze();

                        $buffer = $minify->squeeze($buffer,true,false,false);

                    }
                }
                if ($type=='css') {
                    $buffer = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $buffer);
                    $buffer = preg_replace('/\s+/'," ",$buffer);
                    $buffer = str_replace(': ', ':', $buffer);
                    $buffer = str_replace(array("\n","\r\n","\t"), '', $buffer);
                }

                $print_out_data[$val.'.'.$type] = trim($buffer);
            }
            $minify_data = "";
            if ($type=='js') {
                foreach ($print_out_data as $key2 => $val2) {
                    $minify_data .= "/*".$key2."*/".$val2."\n";
                }
            } else {
                $minify_data = implode(" ",$print_out_data);
            }
            if (!file_exists($stock_path)) {
                mkdir($stock_path,0777);
            }
/*            $file_list = scandir($stock_path);
            foreach ($file_list as $val) {
                if (is_file($stock_path."/".$val)) {
                    if (preg_match("/^".$file_name_md5."/",$val)) {
                        unlink($stock_path."/".$val);
                    }
                }
            }*/
            if (file_put_contents($stock_path."/".$file_name,$minify_data)) {
                //echo "/*Minify Process*/";
                chmod($stock_path."/".$file_name, 0644);
                return $minify_data;
            } else {
                self::writeLog("error","Write file Error :".$type."[".$data_input."]");
            }
        } else {
            return file_get_contents($stock_path."/".$file_name);
            //echo "/*Load Cache Process*/";
        }
    }
    public static function getLastModified($files,$type = "js"){
        $time = 0;
        $lastModified = 0;

        foreach ($files as $val ) {
            $file = public_path().$type.'/'.$val.'.'.$type;
            if(is_file($file)){
                $time =filemtime($file);
                if( $time > $lastModified){
                    $lastModified = $time;
                }
            }
        }
       return $lastModified;
        // header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
    }
    public static function writeLog($type,$msg,$debug = "") {
        $data = json_encode($debug);
        Log::$type($msg." ".$data);
    }
    public static function instantLoad() {
        if (count(self::$css_list) > 0) {
            $file_css = "";
            ksort(self::$css_list);

            foreach (self::$css_list as $key => $val) {

                if (is_array($val)) {
                    if (isset($bg_flag)) {
                        $file_css.= ",";
                    }
                    $file_css.= implode(",",$val);
                    $bg_flag = true;
                } else {
                    if (strlen($val) > 0) {
                        if (isset($bg_flag)) {
                            $file_css.= ",";
                        }
                        $file_css.= $val;
                        $bg_flag = true;
                    }
                }
            }
            $css_data = self::loadFile("css",$file_css,"",true);
            return "<style type='text/css'>".$css_data."</style>";
        } else {
            self::writeLog("error","Zero CSS file Compress");
        }
    }
}
