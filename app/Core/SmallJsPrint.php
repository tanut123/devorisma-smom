<?php
namespace App\Core;
use Patchwork\JSqueeze;

class SmallJsPrint {
    function __construct() {

    }
    public static function loadJs($file) {
        $file_array = explode(",",$file);
        $r_data = "";
        foreach ($file_array as $value) {
            if (file_exists(base_path()."/resources/assets/js/".$value.".js")) {
                $r_data .= file_get_contents(base_path()."/resources/assets/js/".$value.".js");
            } else {
                return "Js File Not Found: ".$value;
            }
        }
        $minify = new JSqueeze();
        $buffer = $minify->squeeze($r_data,true,false,false);

        return "<script type='text/javascript'>".$buffer."</script>";
    }
}