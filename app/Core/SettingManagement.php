<?php namespace App\core;

use App\SiteSetting;
use Cache;
use Crypto;

class SettingManagement {

	public function flush(){
		if($this->check()){
			Cache::flush('setting');
		}
		return $this->set();
	}

	private function check(){
		$data = Cache::get('setting');
		if($data != null){
			return true;
		}
		return false;
	}

	private function set(){

		$value = Cache::rememberForever('setting', function()
		{
			$settingData = new SiteSetting();
			$serializeData = serialize($settingData->getSetting());
		    return Crypto::Encrypt($serializeData);
		});
		return $value;
	}

	public function get($key){
		if($this->check()){
			$decryptData = Crypto::Decrypt(Cache::get('setting'));
        	$value = unserialize($decryptData);
        	return isset($value[$key])? $value[$key] : $key;
    	}
    	return $key;
	}
}
?>