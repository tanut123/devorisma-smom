<?php
function compress_css($file) {
    App\Core\Compress::read_css($file);
}
function compress_js($file) {
    App\Core\Compress::read_js($file);
}
function write_css() {
    return App\Core\Compress::write_css();
}
function write_js() {
    return App\Core\Compress::write_js();
}
// function readFileName($uuname, $original_filename, $mode ,$module) {
//     return App\Core\OMImage::readFileName($uuname, $original_filename, $mode ,$module);
// }