<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Log;

class Sitemap extends Model {

	public $XML_ROW;
	public $promotion;
	public $knowledge;
	public $momtrip;
	public $settingData;
	public $arrGenModule;


	function __construct(){
		$this->XML_ROW = 50000;
		$this->promotion = new Promotion();
		$this->knowledge = new Knowledge();
		$this->momtrip = new Momtip();
		$this->settingData = new SiteSetting();
		$this->arrGenModule = ["momtip","knowledge","promotion","static","index"];
	}

	public function genSitemap($type){
		$this->removeSitemap($type);
		if($type == "all"){
			$rwData = $this->genXML();
		}else{
			$rwData = $this->genXML($type);
			if($type != "index"){
				$rwData += $this->genXML("index");
			}
		}

		return $rwData;
	}

	public function removeSitemap($type){
		if($type == "all"){
			$path = $this->getPathFile() . "*.xml.gz";
		}else{
			if($type == "index"){
				// $path = public_path() . "/sitemap.xml";
				return null;
			}else{
				$path = $this->getPathFile() . $type ."_*";
			}
		}
		$arrpath = glob($path);
		$dt = array_map('unlink', $arrpath);
		if($dt){
			Log::debug("Remove sitemap by type (" . $type . ") Complete", $arrpath);
		}
		return $arrpath;
	}


	public function getPathFile(){
		$path = public_path()."/sitemap/";
		if(!is_dir($path)){
			mkdir($path, 0755, true);
		}
		return $path;
	}

	public function genXML($type = ""){
		$arrGenModule = [$type];
		$filepath = $this->getPathFile();
		if($type == ""){
			$arrGenModule = $this->arrGenModule;
		}
		$countValid = 0;
		if(count($arrGenModule) > 0){
			foreach ($arrGenModule as $key => $valModule) {
				$func = $valModule."Data";
				$content = $this->$func();
				if($content > 0){
					$countValid++;
				}else{
					Log::info('Cannot Write ' . $valModule);
				}
			}
		}
		return $countValid;
	}

	public function getSitemap(){
		$path = $this->getPathFile();
		$arrSitemap = $this->listdir($path);
		return $arrSitemap;
	}


	public function indexData(){
		$genXml = false;
		$sitemap = $this->getSitemap();
		// LAST MODIFIED FROM sitemap.xml
		$lastModifiedDate = $this->getLastModiFiedXML();
		$path = public_path() . "/sitemap/";

		if(count($sitemap) > 0){
			$countValid = 0;
			$content = $this->xmlHeader("index");
			natsort($sitemap);
			foreach ($sitemap as $valSiteMap) {
				$page = $this->getFileFromPath($valSiteMap);
				$page = url() . "/sitemap/" . $page;
				// GET PAGE NAME
				$index_name = $this->getPageFromFilename($page);
				if($index_name == null){
					continue;
				}
				$dateXML = $lastModifiedDate[$index_name];

				// GET LAST MODIFINED FROM DB
				$funcLatMod = $index_name . "LastMod";
				$dateDB = $this->$funcLatMod();

				// CHECK DIFF DATE TO RE sitemap.xml
				if($this->checkToUpdate($dateXML, $dateDB)){
					$genXml = true;
				}
				$date_type = $dateDB;

				$content .= $this->xmlBody($page, $date_type, "" , "", "index");
			}
			$content .= $this->xmlFooter("index");
			// RE sitemap.xml when true
			if($genXml){
				if($this->writeFileXML($content, "sitemap.xml", $path)){
					$countValid ++;
					$this->pingGoogle();
				}else{
					Log::debug('Cannot write to file (' . $file_name . ')');
				}
				return $countValid;
			}else{
				Log::debug('Data not change');
			}
		}
		return 0;
	}

	public function getPageFromFilename($page){
		$arrModule = $this->arrGenModule;
		foreach ($arrModule as $val) {
			if(strpos($page, $val) !== false){
				return $val;
			}
		}
		return null;
	}

	public function checkToUpdate($dateXml, $dateDB){
		if($dateXml == $this->showAtomFormat($dateDB)){
			return false;
		}
		return true;
	}

	public function getFileFromPath($path){
		$arrFile = explode("//", $path);
		if(count($arrFile) > 0 && isset($arrFile[1])){
			return $arrFile[1];
		}
		return null;
	}

	public function getTrickMomCountData($lang){
		$datamomtip = $this->momtrip->getCountTrickMomAll($lang);
		return $datamomtip;
	}

	public function momtipLastMod(){
		$lastMod = $this->momtrip->getTrickMomLastMod();
		return $lastMod;
	}

	public function getTrickMomListData($page, $limit, $lang){
		$datamomtip = $this->momtrip->getTrickMomListAllLimit($page, $limit, $lang);
		return $datamomtip;
	}

	public function getKnowledgeCountData($lang){
		$datamomtip = $this->knowledge->countListAll($lang);
		return $datamomtip[0]->countData;
	}

	public function knowledgeLastMod(){
		$lastMod = $this->knowledge->loadLastMod();
		return $lastMod;
	}

	public function getKnowledgeListData($page, $limit, $lang){
		$datamomtip = $this->knowledge->loadListAllLimit($page, $limit, $lang);
		return $datamomtip;
	}

	public function getPromotionCountData($valLang){
		$datamomtip = $this->promotion->countListAll($valLang);
		return $datamomtip[0]->countData;
	}

	public function promotionLastMod(){
		$lastMod = $this->promotion->loadLastMod();
		return $lastMod;
	}

	public function getPromotionListData($page, $limit, $valLang){
		$datamomtip = $this->promotion->loadListAllLimit($page, $limit, $valLang);
		return $datamomtip;
	}

	public function staticLastMod(){
		$lastMod = $this->settingData->getSettingLastModStatic();
		return $lastMod;
	}

	public function momtipData(){
		$arrLang = $this->langSite();
		$countValid = 0;
		foreach ($arrLang as $valLang) {
			$dataAllRow = $this->getTrickMomCountData($this->langDB($valLang));
			if($dataAllRow > 0){
				$loop_count = $this->countRow($dataAllRow);
				$start_row = 0;
				for($round = 1; $round <= $loop_count; $round++){
					$limit = $this->getLimit($round, $start_row);
					$data = $this->getTrickMomListData($limit["start_row"], $limit["limit"], $this->langDB($valLang));
					$content = $this->xmlHeader();
					$file_name = "momtip_". $valLang . "_" . $round . ".xml";
					if(count($data) > 0){
						foreach ($data as $valPage) {
							$obj_lang = $valPage->obj_lang;
							$trick_mom_id = $valPage->trick_mom_id;
							$slug = $valPage->slug;
							$type_mom = $valPage->type_mom;
							$lang = $this->getLang($obj_lang);
							$date = $valPage->obj_published_date;
							$type_mom_array = array(1=>"pregnancy-mom", 2=>"lactating-mom", 3=>"toddler-mom");
							$path =  $lang . "/" . $type_mom_array[$type_mom] . "/" . $slug;
							$page = url() . "/" . $path;
							$content .= $this->xmlBody($page, $date, "0.5" , "daily");
						}
						$content .= $this->xmlFooter();
						if($this->writeFileXML($content, $file_name, $this->getPathFile(), "gz")){
							$countValid ++;
						}else{
							Log::debug('Cannot write to file (' . $file_name . ')');
						}
					}else{
						Log::debug('No data to write (' . $file_name . ')');
					}
				}
			}else{
				Log::debug('No data momtip with language (' . $valLang . ') to write');
			}
		}
		return $countValid;
	}

	public function knowledgeData(){
		$arrLang = $this->langSite();
		$countValid = 0;
		foreach ($arrLang as $valLang) {
			$dataAllRow = $this->getKnowledgeCountData($this->langDB($valLang));
			if($dataAllRow > 0){
				$loop_count = $this->countRow($dataAllRow);
				$start_row = 0;
				for($round = 1; $round <= $loop_count; $round++){
					$limit = $this->getLimit($round, $start_row);
					$data = $this->getKnowledgeListData($limit["start_row"], $limit["limit"], $this->langDB($valLang));
					$content = $this->xmlHeader();
					$file_name = "knowledge_". $valLang . "_" . $round . ".xml";
					if(count($data) > 0){
						foreach ($data as $valPage) {
							$obj_lang = $valPage->obj_lang;
							$knowledge_list_id = $valPage->knowledge_list_id;
							$slug = $valPage->slug;
							$lang = $this->getLang($obj_lang);
							$date = $valPage->obj_published_date;

							$path =  $lang . "/knowledge/" . $slug;
							$page = url() . "/" . $path;
							$content .= $this->xmlBody($page, $date, "0.5" , "daily");
						}
						$content .= $this->xmlFooter();
						if($this->writeFileXML($content, $file_name, $this->getPathFile(), "gz")){
							$countValid ++;
						}else{
							Log::debug('Cannot write to file (' . $file_name . ')');
						}
					}else{
						Log::debug('No data to write (' . $file_name . ')');
					}
				}
			}else{
				Log::debug('No data Knowledge with language (' . $valLang . ') to write');
			}
		}
		return $countValid;
	}

	public function promotionData(){
		$arrLang = $this->langSite();
		$countValid = 0;
		foreach ($arrLang as $valLang) {
			$dataAllRow = $this->getPromotionCountData($this->langDB($valLang));
			if($dataAllRow > 0){
				$loop_count = $this->countRow($dataAllRow);
				$start_row = 0;
				for($round = 1; $round <= $loop_count; $round++){
					$limit = $this->getLimit($round, $start_row);
					$data = $this->getPromotionListData($limit["start_row"], $limit["limit"], $this->langDB($valLang));
					$content = $this->xmlHeader();
					$file_name = "promotion_". $valLang . "_" . $round . ".xml";
					if(count($data) > 0){
						foreach ($data as $valPage) {
							$obj_lang = $valPage->obj_lang;
							$promotion_id = $valPage->promotion_id;
							$slug = $valPage->slug;
							$lang = $this->getLang($obj_lang);
							$date = $valPage->obj_published_date;

							$path =  $lang . "/promotion/" . $slug;
							$page = url() . "/" . $path;
							$content .= $this->xmlBody($page, $date, "0.5" , "daily");
						}
						$content .= $this->xmlFooter();
						if($this->writeFileXML($content, $file_name, $this->getPathFile(), "gz")){
							$countValid ++;
						}else{
							Log::debug('Cannot write to file (' . $file_name . ')');
						}
					}else{
						Log::debug('No data to write (' . $file_name . ')');
					}
				}
			}else{
				Log::debug('No data Promotion with language (' . $valLang . ') to write');
			}
		}
		return $countValid;
	}

	public function langSite(){
		return ["th","en"];
	}

	public function getStaticDataSiteSettings(){
		$setting = $this->settingData->getSetting();
		$sitemap = $setting["SITEMAP_STATIC_PAGE"];
		$sitemapGen = explode("|:|", $sitemap);
		if(end($sitemapGen) == ""){
			array_pop($sitemapGen);
		}
		foreach ($sitemapGen as $site) {
			list($page, $frequency, $priority) = explode("__", trim($site));
			$arrPage[$page]["data"] = [$frequency,$priority];
		}
		if(count($arrPage) > 0){
			return $arrPage;
		}
		return null;
	}

	public function staticData(){
		$arrayPage = $this->getStaticDataSiteSettings();
		$arrLang = $this->langSite();
		if(count($arrayPage) > 0){
			$countValid = 0;
			foreach ($arrLang as $valLang) {
				$content = $this->xmlHeader();
				foreach ($arrayPage as $keyPage => $valPage) {
					$page = url() . "/" . $valLang . "/" . $keyPage;
					$content .= $this->xmlBody($page, date("Y-m-d H:i:s"), $valPage["data"][1], $valPage["data"][0]);
				}
				$content .= $this->xmlFooter();
				if($this->writeFileXML($content, "static_" . $valLang . ".xml", $this->getPathFile(), "gz")){
					$countValid ++;
				}else{
					Log::debug('Cannot write to file (static_' . $valLang . '.xml)');
				}
			}
			return $countValid;
		}
		Log::debug('No data to write (static)');
		return 0;
	}

	public function listdir($dir='.') {
	    if (!is_dir($dir)) {
	        return false;
	    }

	    $files = array();
	    $this->listdiraux($dir, $files);

	    return $files;
	}

	public function listdiraux($dir, &$files) {
	    $handle = opendir($dir);
	    while (($file = readdir($handle)) !== false) {
	        if ($file == '.' || $file == '..' || $file == '.DS_Store') {
	            continue;
	        }
	        $filepath = $dir == '.' ? $file : $dir . '/' . $file;
	        if (is_link($filepath)){
	            continue;

	        }if (is_file($filepath)){
	            $files[] = $filepath;
	        }else if (is_dir($filepath)){
	            $this->listdiraux($filepath, $files);
	        }
	    }
	    closedir($handle);
	}

	public function xmlHeader($type = ""){
		$content_head = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
		if($type == "index"){
			$content_head .= '<sitemapindex xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/siteindex.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'."\n";
		}else{
			$content_head .= "<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n";
		}
		return $content_head;
	}

	public function xmlFooter($type = ""){
		if($type == "index"){
			$content_foot = '</sitemapindex>';
		}else{
			$content_foot = "</urlset>";
		}
		return $content_foot;
	}

	public function xmlBody($site, $date, $priority = "",$frequency = "",$type = ""){
		if($type == "index"){
			$content_body = "	<sitemap>\n";
		}else{
			$content_body = "	<url>\n";
		}
		$content_body .= "		<loc>" . $site . "</loc>\n";
		$content_body .= "		<lastmod>".$this->showAtomFormat($date)."</lastmod>\n";

		if($frequency != ""){
			$content_body .= "		<changefreq>" . $frequency . "</changefreq>\n";
		}

		if($priority != ""){
			$content_body .="		<priority>" . $priority . "</priority>\n";
		}

		if($type == "index"){
			$content_body .= "	</sitemap>\n";
		}else{
			$content_body .="	</url> \n";
		}

		return $content_body;
	}

	public function writeFileXML($somecontent,$filename="test.xml",$filepath="",$type =""){
		try {
			if($type == "gz"){
				$gz = gzopen($filepath. $filename.".gz",'w9');
				gzwrite($gz, $somecontent);
				@chmod($filepath.$filename, 0666);
				gzclose($gz);
				Log::debug('Success, wrote  to file ('. $filename . ')');
				return true;
			} else {
				$handle = fopen($filepath. $filename, 'w');
				fwrite($handle, $somecontent);
				@chmod($filepath. $filename, 0666);
				fclose($handle);
				Log::debug('Success, wrote  to file ('. $filename . ')');
				return true;
			}
		} catch (Exception $e) {
			$ErrorMessage = $e -> getMessage();
			Log::debug('Cannot write to file ('. $filename . ') (' . $ErrorMessage . ')');
			return false;
		}
	}

	public function pingGoogle(){
		$url_ping = "http://www.google.com/webmasters/tools/ping?sitemap=" . urlencode(url() . "/sitemap/sitemap.xml");
		Log::debug('PING GOOGLE START');
		$res = $this->curl($url_ping);
		Log::debug('URL ' . $url_ping);
		Log::debug(htmlentities($res));
		Log::debug('PING GOOGLE DONE');
		return $res;
	}

	public function curl($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		if (!strncmp($url, 'https', 5)){
			curl_setopt($ch, CURLOPT_AUTOREFERER, 1);
			curl_setopt($ch, CURLOPT_PORT, 443);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		}
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
	    $response = curl_exec($ch);
		curl_close($ch);
		return $response;
    }

	public function getLastModiFiedXML(){
		$xml_file = public_path() . "/sitemap/sitemap.xml";

		$arrLastModified["momtip"] = "";
		$arrLastModified["promotion"] = "";
		$arrLastModified["knowledge"] = "";
		$arrLastModified["static"] = "";
		if(is_file($xml_file)){
			$xmlstr = (array)simplexml_load_file($xml_file);
			if(isset($xmlstr["sitemap"])){
				foreach ($xmlstr["sitemap"] as $key => $val) {
					$val = (array)$val;
					if(strpos($val["loc"], "momtip") !== false){
						$arrLastModified["momtip"] = $val["lastmod"];
					}else if(strpos($val["loc"], "promotion") !== false){
						$arrLastModified["promotion"] = $val["lastmod"];
					}else if(strpos($val["loc"], "knowledge") !== false){
						$arrLastModified["knowledge"] = $val["lastmod"];
					}else if(strpos($val["loc"], "static") !== false){
						$arrLastModified["static"] = $val["lastmod"];
					}
				}
			}
		}
		return $arrLastModified;
	}

	public function countRow($rows_all){
		return ceil($rows_all/$this->XML_ROW);
	}

	public function getLimit($round, $start_row){
		if($round == 1){
			$start_row = 0;
		}
		$round -= 1;
		$start_row = $round * $this->XML_ROW;
		$end_row = $this->XML_ROW;
		$limit["start_row"] = $start_row;
		$limit["limit"] = $end_row;
		return $limit;
	}

	public function getLang($lang){
		$langUrl = "th";
		if($lang == "ENG"){
			$langUrl = "en";
		}
		return $langUrl;
	}

	public function langDB($lang){
		$langDb = "THA";
		if($lang == "en"){
			$langDb = "ENG";
		}
		return $langDb;
	}

	public function showAtomFormat($date){
		return date(DATE_ATOM, strtotime($date));
	}

}
