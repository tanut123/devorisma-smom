<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;

class CustomPage extends Model {

	protected $table = '';

	function getCustomPage($slug){
		$dt = DB::table('custom_page')->select('custom_page_id', 'title', 'slug', 'css_file','css_file_gen', 'detail')->where('slug', trim($slug))->where('obj_lang', Lng::db())->get();
		return $dt;
	}

    function loadCustomCss($gen) {
        $dt = DB::table('custom_page_media_file')->select()->where('uuname',$gen)->get();
        return $dt;
    }
}
