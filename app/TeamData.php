<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;

class TeamData extends Model {

	protected $table = '';

	function getAllPerson(){
		$dt = DB::table('team_person')
            ->select("team_person_id","name_th","name_en","position_th","position_en","person_pic","person_pic_gen")
            ->orderBy("priority","asc")->get();
		return $dt;
	}
    function loadPersonData($person_id) {
        $dt = DB::table('team_person')
            ->where("team_person_id",$person_id)->get();
        return $dt;
    }
}
