<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use PHPImageOptim\Tools\Png\AdvPng;
use PHPImageOptim\Tools\Png\OptiPng;
use PHPImageOptim\Tools\Png\PngCrush;
use PHPImageOptim\Tools\Png\PngOut;
use PHPImageOptim\Tools\Png\PngQuant;
use PHPImageOptim\Tools\Jpeg\JpegOptim;
use PHPImageOptim\Tools\Jpeg\JpegTran;
use PHPImageOptim\Tools\Gif\Gifsicle;
use PHPImageOptim\PHPImageOptim;

class IMGOptim extends Model {

	private $imageOp;

	function __construct(){
		$this->imageOp = new PHPImageOptim();
	}

	function optimize($pathfile){
		if(is_file($pathfile)){
			$this->imageOp->setImage($pathfile);

		    $ext = $this->getExtension($pathfile);
		    if($ext == "png"){
		    	$this->pngOt();
		    }else if($ext == "jpg" || $ext == "jpeg"){
		    	$this->jpegOt();
		    }else if($ext == "gif"){
		    	$this->gifOt();
		    }else{
		    	return false;
		    }
		    return $this->imageOp->optimise();
		}else{
			return false;
		}
	}

	function getExtension($pathfile){
		return pathinfo($pathfile, PATHINFO_EXTENSION);
	}

	function pngOt(){
		// $advPng = new AdvPng();
	 //    $advPng->setBinaryPath('/usr/bin/advpng');

	    $optiPng = new OptiPng();
	    $optiPng->setBinaryPath('/usr/bin/optipng');

	    $pngCrush = new PngCrush();
	    $pngCrush->setBinaryPath('/usr/bin/pngcrush');

	    $this->imageOp
	    	//->chainCommand($advPng)
	            ->chainCommand($optiPng)
	            ->chainCommand($pngCrush);
	}

	function jpegOt(){
	    $jpegTran = new JpegTran();
	    $jpegTran->setBinaryPath('/usr/bin/jpegtran');

	    $jpegOptim = new JpegOptim();
	    $jpegOptim->setBinaryPath('/usr/bin/jpegoptim');

		$this->imageOp->chainCommand($jpegTran)
	            ->chainCommand($jpegOptim);
	}

	function gifOt(){
		$gifSicle = new Gifsicle();
	    $gifSicle->setBinaryPath('/usr/bin/gifsicle');

	    $this->imageOp->chainCommand($gifSicle);
	}

}
