<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core\Lng;
use App;
use DateTime;

class Syntom extends Model {

	public function load_detail($slug){
		$dt = DB::table('syntom')->where('slug',$slug)->get();
		return $dt;
	}

}