<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Core;
use Illuminate\Support\Facades\Lang;

class PowerOfSleep extends Model {

	protected $table = '';

	function getBannerTop(){
		$dt = DB::table('power_of_sleep_banner')->where("section","top")->orderBy("priority","asc")->orderBy("create_date","desc")->get();
		return $dt;
	}

	function getBannerBottom(){
		$dt = DB::table('power_of_sleep_banner')->where("section","bottom")->orderBy("priority","asc")->orderBy("create_date","desc")->get();
		return $dt;
	}

	function getVideo() {
		$dt = DB::table('power_of_sleep_video')->select("*","title_th AS title")->orderBy("priority","asc")->orderBy("create_date","desc")->get();
		return $dt;
	}
}
