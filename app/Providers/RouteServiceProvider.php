<?php namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;
use Illuminate\Http\Request;

class RouteServiceProvider extends ServiceProvider {

	/**
	 * This namespace is applied to the controller routes in your routes file.
	 *
	 * In addition, it is set as the URL generator's root namespace.
	 *
	 * @var string
	 */
	protected $namespace = 'App\Http\Controllers';

	/**
	 * Define your route model bindings, pattern filters, etc.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function boot(Router $router)
	{
		parent::boot($router);

		//
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router, Request $request)
	{
		    $locale = $request->segment(1);
		    $locale = ($locale == 'en')?'en':'th';
	    	$this->app->setLocale($locale);


		    $router->group(['namespace' => $this->namespace, 'prefix' => 'th'], function($router) {
		        require app_path('Http/routes_lang.php');
		    });
		    // $router->group(['namespace' => $this->namespace, 'prefix' => 'en'], function($router) {
		    //     require app_path('Http/routes_lang.php');
		    // });
		// }else if (in_array($locale, ['js','css','stocks','setting','translate','download_media','custom_css','custom_css_wcm','xml-sitemap'])) {
		// 	 $router->group(['namespace' => $this->namespace], function($router) {
		//         require app_path('Http/routes_resource.php');
		//     });

			 $router->group(['namespace' => $this->namespace], function($router) {
		        require app_path('Http/routes_resource.php');
		    });

	}

}
