@extends('template.master')
@compressCss("slick,slick-theme,online_shopping")
@section('content')
	<div class="online-panel FX lazyload" data-bgset="{{ asset($BASE_CDN . '/images/online_shopping/bg-mobile_new.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/online_shopping/bg-desktop_new.jpg') }}" data-expand="+10">
		<div class="online-panel-inner">
			<div class="panel-title">
				<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/online_shopping/text-title_new.png') }}" data-expand="+10" alt="text-title">
				<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/online_shopping/text-title-mobile_new.png') }}" data-expand="+10" alt="text-title-mobile">
			</div>
			<div class="panel-shopping">
				<div class="shopping-title">
					<img class="lazyload" data-src="{{ asset($BASE_CDN . '/images/online_shopping/text-shopping_new.png') }}" alt="text-shopping" data-expand="+10">
				</div>
				<div class="panel-shopping-inner">
					<div class="shopping-list">
						<div class="shopping-product">
							<img class="product01 lazyload" data-src="{{ asset($BASE_CDN . '/images/online_shopping/product01_new.png') }}" data-expand="+10"/>
						</div>
						<div class="shopping-box">
							<div class="shopping-name">
								<span class="text1">S-26 โกลด์ โปรเกรส</span> 
							</div>
							<div class="shopping-desc">
								ขนาด 600 กรัม
							</div>
							<div class="btn-shopping FMonXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/online_shopping/bg-btn.svg') }}" data-expand="+10" onclick="openLightbox(0);">
								Shop Online
							</div>
						</div>
					</div>
					<?php /* <div class="shopping-list">
						<div class="shopping-product">
							<img class="product02" src="{{ asset($BASE_CDN . '/images/online_shopping/product02.png') }}" />
						</div>
						<div class="shopping-box">
							<div class="shopping-name">
								<span class="text1">S-26 โปรเกรส</span> <span class="text2">โกลด์</span>
							</div>
							<div class="shopping-desc">
								ขนาด 1,800 กรัม
							</div>
							<div class="btn-shopping FMonXMed" onclick="openLightbox(1);">
								Shop Online
							</div>
						</div>
					</div> */ ?>
					<div class="shopping-list">
						<div class="shopping-product product03">
							<img class="product03 lazyload" data-src="{{ asset($BASE_CDN . '/images/online_shopping/product03_new.png') }}" data-expand="+10"/>
						</div>
						<div class="shopping-box">
							<div class="shopping-name">
								<span class="text1">S-26 โกลด์ โปรเกรส</span> 
							</div>
							<div class="shopping-desc">
								ขนาด 2,400 กรัม
							</div>
							<div class="btn-shopping FMonXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/online_shopping/bg-btn.svg') }}" data-expand="+10" onclick="openLightbox(2);">
								Shop Online
							</div>
						</div>
					</div>
					<div class="shopping-list">
						<div class="shopping-product">
							<img class="product04 lazyload" data-src="{{ asset($BASE_CDN . '/images/online_shopping/product04_new.png') }}" data-expand="+10"/>
						</div>
						<div class="shopping-box">
							<div class="shopping-name">
								<span class="text1">S-26 โกลด์ โปรเกรส</span>
							</div>
							<div class="shopping-desc">
								ขนาด 3,000 กรัม
							</div>
							<div class="btn-shopping FMonXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/online_shopping/bg-btn.svg') }}" data-expand="+10" onclick="openLightbox(3);">
								Shop Online
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="online-lightbox-panel FX">
		<div class="online-lightbox">
			<div class="btn-close"></div>
			<div class="lightbox-title">

			</div>
			<div class="lightbox-body">

			</div>
		</div>
	</div>
	<script>
		window.lazySizesConfig = window.lazySizesConfig || {};
		window.lazySizesConfig.customMedia = {
	    	'--mb': '(max-width: 767px)'
		};
	</script>
@endsection
@compressJs("lazy.bgset.min,lazysizes.min,slick.min,online_shopping")