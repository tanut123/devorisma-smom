@extends('template.master')

@compressCss("slick,slick-theme,thepowerofsleeping,bigvideo,home.banner")

@section('content')
<div id="product-panel">
	<div id="product-inner-panel">
		<article id="banner-mom" data-ratio="1.7">
			<div id="banner-slick">
				<div class="banner-list" data-delay='5'>
					<div class='image-loader-video' data-type='image' data-image_desktop="{{  asset($BASE_CDN . '/images/product/pe_gold/pe-gold-banner.jpg') }}" data-image_mobile="{{  asset($BASE_CDN . '/images/the_power_of_sleeping/cover-mp4.jpg') }}"  style='display:none;'></div>
                    <div id='btnVideoPlay' class='btnPlayVideo slide-banner-list' data-type='video' data-source="{{  asset($BASE_CDN . '/images/the_power_of_sleeping/S26_SLEEP_20160721.mp4') }}"></div>
                        <div class='play-control visible-xs' onClick='playPauseVideo();'>
                    </div>
				</div>
			</div>
		</article>
		@include('thepowerofsleeping.banner')
		@include('thepowerofsleeping.video')

	</div>
</div>
<div class="clearfix"></div>
<div id='video_light_box'>
	<div class='video_align'>
		<div class='video_panel'>

		</div>
		<div class='close_video'>

		</div>
	</div>
</div>
@endsection

@compressJs("slick.min,the_power_of_sleeping,video_plugin,bigvideo,jquery.tubular.1.0,home.banner")