@extends('template.master')

@compressCss("day-and-night")

@section('content')
<div class="wapcontent-panel">
	<div class="panel-row1">
		<img class="m-bg-obj visible-xs" src="{{ $BASE_CDN }}/images/day-and-night/m_bg-row1-object.jpg" alt="">
		<div class="panel-bg-obj">
			<div class="wap-content">
				<div class="bg-obj">
					<h1>เผยความลับ<span>สมองลูก</span></h1>
					<h3>ที่คุณแม่อาจจะยังไม่เคยรู้</h3>
					<h4>สมองลูกมีการเรียนรู้ทุกเวลาทั้งกลางวัน และกลางคืน</h4>
					<div class="txt-content">
						ในยุคสมัยที่โลกเปลี่ยนไว  แม่ทุกคนต้องการให้ลูกเรียนรู้และพัฒนาได้ไวทันโลก โดยเฉพาะอย่างยิ่งพัฒนาการทางด้านสมองของลูก เพราะสมองเป็นศูนย์กลาง ของทุกอย่างเช่น การคิด วางแผน เรียนรู้ เป็นต้น ซึ่งคุณแม่ทุกคนให้ความสำคัญ โดยการทุ่มเทที่จะทำทุกอย่างให้ลูกพร้อมรับและเรียนรู้ให้ไวทันโลก
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="panel-row2">
		<div class="panel-bg-obj">
			<div class="wap-content">
				<div class="panel-left">
					<img class="img-content img-responsive" src="{{ $BASE_CDN }}/images/day-and-night/img-row2.png" alt="">
				</div>
				<div class="panel-right">
					<div class="bg-obj">
						<h1>ปัจจัยหลัก</h1>
						<h4>ในการพัฒนาสมองลูกมี 3 ปัจจัยดังนี้ </h4>
						<ul class="txt-list">
							<li>พันธุกรรม </li>
							<li>สิ่งแวดล้อม และการเลี้ยงดู</li>
							<li>โภชนาการ</li>
						</ul>
						<div class="txt-content">
							เรื่องพันธุกรรมเป็นสิ่งที่ปรับเปลี่ยนยาก  ดังนั้นการเลี้ยงดูเอาใจใส่ และการ
							ดูแลเรื่องโภชนาการ จึงเป็นสิ่งที่สำคัญที่จะช่วยสร้างสมองลูก  โดยเฉพาะ ในช่วง 1,000 วันแรกของชีวิตที่สมองเจริญ และพัฒนาได้รวดเร็ว และมาก
							ถึง 80% ของสมองผู้ใหญ่
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="panel-row3">
		<div class="panel-bg-obj">
			<div class="wap-content">
				<div class="panel-left">
					<img class="img-content img-responsive visible-xs" src="{{ $BASE_CDN }}/images/day-and-night/img-row3.png" alt="">
					<div class="bg-obj">
						<h1>เผยความลับ…</h1>
						<h4>สมองเรียนรู้ทั้งกลางวัน และกลางคืนได้อย่างไร</h4>
						<div class="txt-content">
							คุณแม่หลายท่านอาจจะยังไม่ทราบว่าโดยความเป็นจริงแล้ว สมองลูกสามารถเรียนรู้ และพัฒนา ได้ตลอดเวลาทั้งกลางวัน และกลางคืน  เด็กส่วนใหญ่จะนอนหลับในเวลากลางคืน  ซึ่งการนอนหลับ นอกจากเป็นการพักผ่อนแล้ว ยังส่งเสริมให้สมองมีการเรียนรู้โดยการนอนทำให้
						</div>
						<ul class="txt-list">
							<li>ตัวเซลล์ประสาทเจริญเต็มที่ <span class="txt-normal">เพื่อเตรียมพร้อมเรียนรู้ในขณะตื่น</span></li>
							<li>ขณะหลับ <span class="txt-normal">เด็กจะนำเอาประสบการณ์ที่ได้ในขณะตื่น มาประมวลเป็นความจำ</span></li>
						</ul>
						<div class="txt-content">ในเวลากลางวัน การเรียนรู้ของเด็กผ่านทางประสาทสัมผัสทั้งห้าคือ ตาดู หูฟัง มือสัมผัส จมูกได้กลิ่น และลิ้นลิ้มรส</div>
					</div>
				</div>
				<div class="panel-right">
					<img class="img-content img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/day-and-night/img-row3.png" alt="">
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<div class="panel-row4">
		<div class="panel-bg-obj">
			<div class="wap-content">
				<div class="panel-left">
					<img class="img-content img-responsive visible-xs" src="{{ $BASE_CDN }}/images/day-and-night/img-row4.png" alt="">
					<div class="bg-obj">
						<h1>เทคนิคกาส่งเสริม</h1>
						<h4>การเรียนรู้ และพัฒนาการสมอง</h4>
						<div class="txt-content">
							คุณพ่อคุณแม่ควรเสริมการเรียนร ู้และพัฒนาสมองตามวัยของเด็ก รวมทั้งเข้าใจลูก ว่าเด็กอาจมีความแตกต่างกันในพื้นอารมณ์ ความถนัด แต่สิ่งหนึ่งที่คุณพ่อคุณแม่ทุกคนสามารถทำได้ และได้ดีที่สุดคือ การเลือกอาหารที่ดี สำหรับลูกเพื่อให้สมองได้มีการเรียนรู้ และร่างกายแข็งแรง
						</div>
						<div class="txt-brown">สารอาหารช่วยเสริมสร้างสมองให้เรียนรู้ และพัฒนาที่สำคัญได้แก่</div>
						<ul class="txt-list">
							<li>ดีเอชเอ ลูทีน โคลีน <span class="txt-normal">เป็นสารอาหารที่มีบทบาทสำคัญในการพัฒนา และเสริมสร้าง การทำงานของสมอง  เด็กๆจึงควรได้รับสารอาหารเหล่านี้ในชีวิตประจำวันเพื่อเสริมสร้าง พัฒนาการ และการทำงานของสมอง</span></li>
							<li>ดีเอชเอ <span class="txt-normal">เป็นกรดไขมันที่พบมากใน โครงสร้างของสมอตัวอ่อนตั้งแต่เริ่มตั้งครรค์ไปจน ตัวอ่อนพัฒนาเป็นทารกใกล้คลอด นอกจากนี้ ดีเอชเอยังมีส่วนสำคัญในการพัฒนา สายตา และการมองเห็นนอกจากนี้ยังมีบทบาทสำคัญในในการเรียนรู้ที่เกี่ยวกับการสังเกตุ  และกระบวนการเรียนรู้ </span></li>
							<li>ลูทีน <span class="txt-normal"> ช่วยส่งเสริมพัฒนาการการมองเห็น และส่งเสริมการพัฒนาของสมอง </span></li>
							<li>โคลีน <span class="txt-normal"> เป็นส่วนประกอบของหารที่สำคัญต่อการทำงานของเซลล์โคลีนทำให้การสื่อสาร ของเซลล์ประสาท การทำงานของสมอง และการพัฒนาความจำเป็นไปอย่างมีประสิทธิภาพ</span></li>
						</ul>
					</div>
				</div>
				<div class="panel-right">
					<div class="bg-obj">
						<img style="padding-left:20px;" class="img-content img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/day-and-night/img-row4.png" alt="">
						<ul class="txt-list">
							<li>แอลฟา-แล็คตัลบูมิน <span class="txt-normal">เป็นโปรตีนคุณภาพสูง พบในน้ำนมแม่ ย่อยง่าย และให้ กรดอะมิโนจำเป็น ที่ช่วยในการเจริญเติบโต และพัฒนาสมอง  “ทริบโตเฟน” เป็นหนึ่ง ในกรดอะมิโนจำเป็น ที่ช่วยในการนอน โดยร่นระยะเวลานอน ให้หลับเร็วขึ้น เมื่อ ร่างกายได้รับแอลฟา-แล็คตัลบูมินเข้าไป จะให้กรดอะมิโนชื่อ ทริบโตเฟนนอกจากนี้ ยังมีสารอาหารอื่นๆที่สำคัญสำหรับสมองอีก ได้แก่ ไอโอดีน ธาตุเหล็ก เป็นต้น</span></li>
						</ul>
						<div class="txt-normal multi-normal">
							จะเห็นได้ว่า สมองลูกต้องการสารอาหารที่หลากหลาย คุณแม่ควรเน้นสารอาหารที่ดี มีประโยชน์ ไม่ว่าจะเป็น ดีเอชเอ แอลฟา-แล็คตัลบูมิน โคลีน ลูทีน เพื่อช่วยเสริมสร้าง การทำงาน และพัฒนาการของสมองลูก ให้เกิดการเรียนรู้ได้ตลอดเวลาทั้งกลางวัน และกลางคืน เพื่อให้ลูกพร้อมรับและก้าวไวทันโลก
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
@endsection