@extends('template.master')

@compressCss("slick,slick-theme,question_article_detail")

@section('content')
	<div id="wapDetail">
		<div class="detail-panel">
			<div id="detail-content">
				<div id="img-detail">
				</div>
				<div id="detail-title" class="FXregular">{{ $data["title"]}}</div>
				<div id="detail-bar">
					<!-- <span id="detail-date" class="FXregular">{{ $data["datestring"] }}</span> -->
					<script type="text/javascript">
						var page_uid = "{{$data['question_article_id']}}";
					</script>
					<span id="detail-views" class="FXregular" style="visibility:hidden;">
					<span class="icon-view"></span>
					<span id="total_view"> 0 </span> Views
					</span>
				</div>
				<div class="detail-wapper FX">
					@replaceStockUrl($data["detail"])
					<div class="clearfix"></div>
				</div>
				<a href='{{ url() . "/หมอเด็ก/" .$data["category"] }}'>
					<div class="btn-back FX">ย้อนกลับ</div>
				</a>
			</div>
		</div>
	</div>
	<div id="bottom-panel">
		<div id="recommend">
			<div class="recommend-inner">
				<div class="recommend-panel">
					<div class="recommend-panel-text">
						<div class="recommend-text FLighter">{{ trans('momtip.MOMTIP_SHARE_TEXT') }}</div>
					</div>
					<div class="btn-share">
						<a id="facebook" class="share-link" data-url="{{ url() }}/หมอเด็ก/{{ $data['category'] }}/{{ $data['slug'] }}">
							<div class="icon-share facebook"></div><span class="text-share FXregular">Share on Facebook</span>
						</a>
					</div>
					<div class="btn-share">
						<a id="twitter" class="share-link" data-url="{{ url() }}/หมอเด็ก/{{ $data['category'] }}/{{ $data['slug'] }}">
							<div class="icon-share twitter"></div><span class="text-share FXregular">Share on Twitter</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div id="rating">
				<div class="rating-inner">
					<div class="rating-panel FThin">
						<div class="rating-panel-text">
							<div class="rating-text">{{ trans('momtip.MOMTIP_RATING_TEXT') }}</div>
						</div>
						<div class="rating-panel-choice">
							<div class="rating-panel-choice-inner">
								<div data-val="1" class="rating-number rate1"><span class="rate-text-number FXregular">1</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<div data-val="2" class="rating-number rate2"><span class="rate-text-number FXregular">2</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<div data-val="3" class="rating-number rate3"><span class="rate-text-number FXregular">3</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<div data-val="4" class="rating-number rate4"><span class="rate-text-number FXregular">4</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<div data-val="5" class="rating-number rate5"><span class="rate-text-number FXregular">5</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<input type="hidden" name="rating" id="inp-rating">
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
	<?php if(count($data["relate_momtip"]) >= 1 && App::getLocale() == "th" ){ ?>
	<div id="relate-momtip-panel">
		<div id="relate-momtip-content">
			<div class="relate-title FXMed">
				<?php $title_trans = str_replace([1,2,3],["PREGNANCY","LACTATING","TODDLER"], $data["title_related"]);
					echo trans("title.TITLE_MOMTIP_".$title_trans);
				?>
			</div>
			<div class="relate-momtip-panel-list">
			<ul id="relate-momtip-list">
				<?php foreach($data["relate_momtip"] as $item){
					$type_mom_data = str_replace([1,2,3],["pregnancy-mom","lactating-mom","toddler-mom"], $item->type_mom);
				?>
				<li data-id="{{ $item->trick_mom_id }}" class="relate-item list-item-momtip">
					<span class="relate-item-image">
						<a href="{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $item->slug }}">
							<img src="@readFileName($item->image_gen,$item->image,'c255x136','trick_mom')" alt="{{ $item->title }}" width="255" height="136" />
						</a>
					</span>
					<span class="relate-item-detail">
						<span class="relate-item-detail-title FXregular">
							@trimWithDot($item->title, 50)
						</span>
					</span>
					<?php
						$bookmark = "F";
						if(in_array($item->trick_mom_id, $data["bookmarkList"])){
				        	$bookmark = "T";
				        }
				        if($data["member_web_id"] == NULL || $bookmark == "T"){
					?>
						<a href="{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $item->slug }}" class="relate-item-detail-link">
							<img class="icon-link" width="30px" height="30px" src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-link.png') }}" alt="icon-link">
						</a>
					<?php }else{ ?>
						<a href="javascript:void(0);" onClick="javascript:addBookmark({{ $item->trick_mom_id }}, this)" data-url='{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $item->slug }}' class="list-body-link-bookmark">
							<img class='list-body-logo' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAABAklEQVQ4T92TsUoDQRRFz0sTQf/BsRQbK79DJJ0guwELBTGlFjuuY2EnCZZKJk0+IHY2foCVneWO+AUWakTwSSQLy5ospjRT3nfPhXd5I0x5NjYXoAdKbe/MZ9eTbFIBZ4ABBs6HzVnhACz/AzjdX1vSt9dLFUZdNIBF4Bm4E3g59eGwuPuvwpKmORLlvFTQZ02kkXazm0p4NCwFfAFbzodBuXFJInMrQh2VvutlV7khiY0VOEF0x3Wf+rluo5VdRLdV+RAbGx0POs6HVjE9jcx62gsPRc3Gpg387F4JTzqMOYMfgftpH6WgbwCreWHvwMIfoLJlKEnTtMYXNUvAUIXjb9UseSHfehsXAAAAAElFTkSuQmCC' width="15" height="17">
							<span class='FXMed text-bookmark'>จัดเก็บ</span>
						</a>
					<?php } ?>
				</li>
				<?php } ?>
			</ul>
			</div>
		</div>
	</div>
	<?php } ?>
	<?php if(count($data["relate"]) >= 1){ ?>
	<div id="relate-panel">
		<div id="relate-content">
			<div class="relate-title FXMed">
				<div class="relate-title-inner f_left">
					<img src="{{ asset($BASE_CDN ."/images/wyeth/wyeth_logo_r_3.png")}}" class="relate-title-img img-responsive" width="310" height="98" />
				</div>
				<div class="relate-title-inner f_right">
					<span class="relate-title-text FXregular">คุณแม่ถาม-คุณหมอตอบ</span>
					<div class="clearfix"></div>
					<a href="{{ url() }}/หมอเด็ก/{{ $data["category"] }}" class="btn-all FX">ดูทั้งหมด</a>
				</div>
				<div class="clearfix"></div>
			</div>
			<ul id="relate-list">
				<?php
				  $d_th_month = array(
              "",
            "ม.ค.",
            "ก.พ.",
            "มี.ค.",
            "เม.ย.",
            "พ.ค.",
            "มิ.ย.",
            "ก.ค.",
            "ส.ค.",
            "ก.ย.",
            "ต.ค.",
            "พ.ย.",
            "ธ.ค."
               );
				foreach($data["relate"] as $item){
					$d_day = date('d', strtotime($item->valid_date));
					$d_month = date('m', strtotime($item->valid_date));
				?>
				<li data-id="{{ $item->question_article_id }}" class="relate-item list-item-momtip">
					<span class="relate-item-image">
						<a href="{{ url() }}/หมอเด็ก/{{ $data["category"] }}/{{ $item->slug }}">
							<img src="@readFileName($item->image_gen,$item->image,'c176x177','question_article')" alt="{{ $item->title }}" class="hidden-xs" width="176" height="177"/>
							<img src="@readFileName($item->image_gen,$item->image,'c255x136','question_article')" alt="{{ $item->title }}" class="visible-xs" width="255" height="136"/>
						</a>
						<span class="badge-list">
							<span class="txt-day"><?=$d_day?></span>
							<span class="txt-month"><?=$d_th_month[intval($d_month)]?></span>
						</span>
					</span>
					<span class="relate-item-detail">
						<span class="relate-item-detail-title FXregular">
							@trimWithDot($item->title, 50)
						</span>
					</span>
						<a href="{{ url() }}/หมอเด็ก/{{ $data["category"] }}/{{ $item->slug }}" class="relate-item-detail-link FBold">
							อ่านต่อ <span class="icon-more"></span>
						</a>
				</li>
				<?php } ?>
			</ul>
		</div>
	</div>

	<?php } ?>

<script type="text/javascript">
	var CONFIRM_RATING = "{{ trans("momtip.CONFIRM_RATING") }}";
</script>

@endsection

@compressJs("slick.min,question_article_detail,favorite")
