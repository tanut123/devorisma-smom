@extends('template.master')
@compressCss("slick,slick-theme,question")
@section('content')
	<div id="wapKnowledge">
		<div class="list-content-panel">
			<div class="header-content">
				<div class="header-left">
					<div class="image-title"></div>
				</div>
				<div class="header-right">
					<div class="header-title FXregular">คุณแม่ถาม-คุณหมอตอบ</div>
					<div class="header-subtitle FXMed">ตอบจริงทุกคำถาม...จากคุณแม่</div>
					<div class="header-desc FXregular">จากประสบการณ์จริงของคุณแม่ตั้งครรภ์<br class="hidden-xs"/>ให้นมบุตรและคุณแม่เลี้ยงลูกวัย 1-3 ปี</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<?php
			$count = 1;
			foreach ($question as $key => $value) {
			?>
				<div class="category-panel panel-<?=$count++;?>">
					<div class="category-panel-inner">
						<div class="category-bar">
							<div class="category-name FX">
								<div class="category-title FXMed">
									<?php
										if($key == "1"){
											echo "<span class='txt-red'>คุณแม่ตั้งครรภ์</span>";
										}else if($key == "2"){
											echo "<span class='txt-red'>คุณแม่ให้นมบุตร</span>";
										}else if($key == "3"){
											echo "<span class='txt-red'>คุณแม่เลี้ยงลูกวัยเตาะแตะ</span>";
										}
									?>
								</div>
								<div class="category-desc FXBold">
									<?php
										if($key == "1"){
											echo "รวมบทความดีๆ จากหมอ...สู่เเม่";
										}else if($key == "2"){
											echo "สารพัดบทความดีๆ จากกุมารแพทย์…สู่แม่";
										}else if($key == "3"){
											echo "นานาสาระประโยชน์จากกุมารแพทย์…สู่แม่";
										}
									?>
								</div>
							</div>
						</div>
						<ul class="list-panel-sub" id="list_panel_<?=$key;?>">
						<?php   foreach($value as $key2 => $value2){
								echo $value2;
						} ?>
						</ul>
						<div class="clearfix"></div>
						<?php if(count($value) > 3){ ?>
								<a href="{{  url().'/หมอเด็ก/'.$key }}">
									<div class="category-viewall FX">
										ดูทั้งหมด
									</div>
								</a>
						<?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>

@endsection
@compressJs("slick.min,question")
