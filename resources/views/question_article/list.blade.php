@extends('template.master')

@compressCss("question_article_list")

@section('content')

	<input type="hidden" name="category" id="category" value="<?=$type?>"/>

	<div id="wapQa" class="{{ (App::getLocale() == 'en')? 'kl_en':'' }}">
		<div class="list-content-panel list-all-item">
			<div class="header-content sub_category">
				<h2 class="f_left">
					<img src="{{ asset($BASE_CDN . "/images/wyeth/wyeth_logo_r_4.png")}}" id="header-content-img" class="img-responsive" width="500" height="159">
				</h2>
				<h1 class="f_right FX"><?=$type_title?></h1>
				<span class="clearfix"></span>
			</div>
			<ul class="list-panel">

			</ul>
			<div class="clearfix"></div>
			<div class="btn-bar">
				<a href="{{ url() }}/หมอเด็ก">
					<div class="btn-style btn-back FX">ย้อนกลับ</div>
				</a>
				<div class="btn-style load-more FX">
					{{ trans('loadmore.LOADMORE') }}
				</div>
			</div>
		</div>
	</div>

@endsection

@compressJs("question_article_list")
