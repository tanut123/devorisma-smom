@extends('template.master')

@compressCss("important-notice")

@section('content')
<div class="content-panel">
	<div class="content-bg">
		<div class="stage-content">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/important-notice/img_content.png" alt="สิ่งสำคัญที่คุณแม่ควรทราบ">
		</div>
	</div>
</div>
@endsection

