@extends('template.master')

@compressCss("slick,slick-theme,momtip.detail")

@section('content')

<div id="detail-panel">
	<div id="detail-box" class="lazyload" data-bgset="{{ asset($BASE_CDN . '/images/knowledge/m_bg_object_promotion_new.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/promotion/bg_object_promotion_new.jpg') }}" data-expand="+10">
	<script type="text/javascript">
		var page_uid = "{{$content[0]->trick_mom_id}}";
	</script>
		<div id="detail-content">
			@if(count($content) > 0)
			<?php
				$type_mom_data_selected = str_replace([1,2,3],["pregnancy-mom","lactating-mom","toddler-mom"], $content[0]->type_mom);
			?>

			<h1 id="detail-title" class="FXregular">{{ $content[0]->title }}</h1>
			<div id="detail-bar">
				<!-- <span id="detail-date" class="FXregular">@dateLang($content[0]->article_date,"%d %b %Y")</span> -->
				<span id="detail-views" class="FXregular" style="visibility:hidden;">
					<img class="icon-view lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-view.png') }}" data-expand="+10" alt="icon-view">
					<span id="total_view"> {{ $content[0]->pageviews }} </span> Views
				</span><span id="detail-share">
					@if($member_web_id != "")
					<a id="manage-favorite" class="FXregular " data-type="add" href="javascript:manageFavorite('{{$content[0]->trick_mom_id}}');"><span class="img">
						<img id='icon-favorite-add' class='lazyload' data-src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAOCAYAAADwikbvAAACBUlEQVQoU4WSPWhTURiGny83SZNYEeI/CpYOkpsUFxWLNELB6iA6OHRQUQtKXIz/op06VfC33i4GhaqoQwcHfwaNUDBBCuoiTVMcSgXF3xbEmrQ39+bIKY3Y1NJvfM953nP43lcAch0x/1i4fBY4BQSAB76y0d54fOBrf1fD8pLH7QT2ABPAlfCY52KsI2fLFLi4PIxilTb6dwTVqZD2ah3hU3jUUy/Z7sg5peSCQt0Xr+9o0PCPF+zCCUESKFWPyLBCpUL+0LWia9cqp9QtyF4RdV4yljkKhB3bXdp8+v2PyivPLq1bEPK78YJtZLafefe7ovddXrvE6ze+A2MaLgHezyvy3tZW3FlfrBJ6ezFWfjEdwJGsZT5RsAPUrnhy6PF8cMaK7AR5JPBUMl3RTXhUP/Ainsy3zA+baWArZWkUfTlz3XyOoMG78WT+wFwGGcu8A+xHkY4fy2+bgl9dXR10vbUjIMsEbjUl84erDbKWeVPBIVDfDGe8bvPJj8UpWM/0Fj8AIUFuNyUH2ypnWSvao1AHgYJju2sqqfyF9cXpNg3q6HTuW5JD+15akXs6Vx2Nr2xEdesqpjNgLaZT9YsCdk1uunEPgd26URP+yVhLYvjnzAb+Zzt9PXUB76/ga6ABGHAWFjc2t43oXlfVd47Vvkmt9xUnCzeCNaEjGxJvdZFmzR99hMFXQ74dsQAAAABJRU5ErkJggg==' alt='Favorite remove' data-expand="+10">
					</span>
						Favorite
					</a>
					@endif
					@if($member_web_id != "" && $bookmark == "F")
					<a href="javascript:void(0);" onClick="javascript:addBookmark('{{$content[0]->trick_mom_id}}',this)" data-url="" class="list-body-link-bookmark">
						<img class='list-body-logo' class='lazyload' data-src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAABAklEQVQ4T92TsUoDQRRFz0sTQf/BsRQbK79DJJ0guwELBTGlFjuuY2EnCZZKJk0+IHY2foCVneWO+AUWakTwSSQLy5ospjRT3nfPhXd5I0x5NjYXoAdKbe/MZ9eTbFIBZ4ABBs6HzVnhACz/AzjdX1vSt9dLFUZdNIBF4Bm4E3g59eGwuPuvwpKmORLlvFTQZ02kkXazm0p4NCwFfAFbzodBuXFJInMrQh2VvutlV7khiY0VOEF0x3Wf+rluo5VdRLdV+RAbGx0POs6HVjE9jcx62gsPRc3Gpg387F4JTzqMOYMfgftpH6WgbwCreWHvwMIfoLJlKEnTtMYXNUvAUIXjb9UseSHfehsXAAAAAElFTkSuQmCC' data-expand="+10">
						<span class='FXMed text-bookmark'>จัดเก็บ</span>
					</a>
					@endif
				</span>
			</div>
			<div id="detail-wrapper">
				@if($content[0]->youtube_source != '' || $content[0]->file_mp4 != '')
					<div id="video-panel">
						@if($content[0]->youtube_source != '')
							<div id="youtube-player">
								<iframe width="100%" height="100%" src="https://www.youtube.com/embed/<?=$content[0]->youtube_source ?>" frameborder="0" allowfullscreen></iframe>

							</div>
						@else
							<video id="video{{$content[0]->trick_mom_id}}" data-id="{{$content[0]->trick_mom_id}}" class="video-player" controls>
								<source src="@readFileName($content[0]->file_mp4_gen,$content[0]->file_mp4,'o0x0','trick_mom')" type="video/mp4">
							</video>
						@endif
					</div>
				@endif
				<div id="img-mobile" class="visible-xs">
					<img data-src="@readFileName($content[0]->image_detail_gen,$content[0]->image_detail,'w423x426','trick_mom')" class="img-responsive lazyload" alt="" data-expand="+10">
				</div>
				<div id="detail-box-content" class="<?php echo ($content[0]->image_detail_gen == "")? 'display-full': '' ?>">
					@replaceStockUrl($content[0]->detail)
				</div>
				@if($content[0]->image_detail_gen != "")
				<div id="img-desktop" class="hidden-xs">
					<img data-src="@readFileName($content[0]->image_detail_gen,$content[0]->image_detail,'w423x426','trick_mom')" alt="" class="lazyload" data-expand="+10">
				</div>
				@endif
				<div class="clearfix"></div>
			</div>
			@else
			<div class="data-not-found FXregular">
				{{ trans('core.DATA_NOT_FOUND') }}
			</div>
			@endif
		</div>
	</div>
</div>
<div id="bottom-panel">
	<div id="recommend" class="lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/detail/bg-share.jpg') }}" data-expand="+10">
		<div class="recommend-inner">
			<div class="recommend-panel">
				<div class="recommend-panel-text">
					<div class="recommend-text FLighter">{{ trans('momtip.MOMTIP_SHARE_TEXT') }}</div>
				</div>
				<div class="btn-share">
					<a id="facebook" class="share-link" data-url="{{ $BASE_LANG }}{{ $type_mom_data_selected }}/{{ $content[0]->slug }}">
						<div class="icon-share facebook lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/detail/icon-share.png') }}" data-expand="+10"></div><span class="text-share FXregular">Share on Facebook</span>
					</a>
				</div>
				<div class="btn-share">
					<a id="twitter" class="share-link" data-url="{{ $BASE_LANG }}{{ $type_mom_data_selected }}/{{ $content[0]->slug }}">
						<div class="icon-share twitter lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/detail/icon-share.png') }}" data-expand="+10"></div><span class="text-share FXregular">Share on Twitter</span>
					</a>
				</div>
			</div>
		</div>
	</div>
	<div id="rating">
		<div class="rating-inner">
			<div class="rating-panel FThin">
				<div class="rating-panel-text">
					<div class="rating-text">{{ trans('momtip.MOMTIP_RATING_TEXT') }}</div>
				</div>
				<div class="rating-panel-choice">
					<div class="rating-panel-choice-inner">
						<div data-val="1" class="rating-number rate1 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/detail/rating-unactive.png') }}" data-expand="+10"><span class="rate-text-number FXregular">1</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
						<div data-val="2" class="rating-number rate2 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/detail/rating-unactive.png') }}" data-expand="+10"><span class="rate-text-number FXregular">2</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
						<div data-val="3" class="rating-number rate3 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/detail/rating-unactive.png') }}" data-expand="+10"><span class="rate-text-number FXregular">3</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
						<div data-val="4" class="rating-number rate4 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/detail/rating-unactive.png') }}" data-expand="+10"><span class="rate-text-number FXregular">4</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
						<div data-val="5" class="rating-number rate5 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/detail/rating-unactive.png') }}" data-expand="+10"><span class="rate-text-number FXregular">5</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
						<input type="hidden" name="rating" id="inp-rating">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@if(count($relate) > 0)
<div id="relate-panel">
	<div id="relate-content" class="lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/detail/bg-relate-mobile_new.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/momtip/detail/bg-relate_new.jpg') }}" data-expand="+10">
		<ul id="relate-list">
			@if(count($relate) >= 1)
				@foreach($relate as $item)
				<?php
					$type_mom_data = str_replace([1,2,3],["pregnancy-mom","lactating-mom","toddler-mom"], $item->type_mom);
				?>
				<li data-id="{{ $item->trick_mom_id }}" class="relate-item list-item-momtip">
					<span class="relate-item-image">
						<a href="{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $item->slug }}">
							<img class="hidden-xs lazyload" data-src="@readFileName($item->image_gen,$item->image,'c255x136','trick_mom')" alt="{{ $item->title }}" data-expand="+10"/>
							<img class="visible-xs lazyload" data-src="@readFileName($item->image_gen,$item->image,'c255x136','trick_mom')" alt="{{ $item->title }}" data-expand="+10"/>
						</a>
					</span>
					<span class="relate-item-detail">
						<span class="relate-item-detail-title FXregular">
							@trimWithDot($item->title, 50)
						</span>
						{{-- <span class="relate-item-detail-description FThin hidden-xs">
							@trimWithDot($item->description, 40)
						</span>
						<span class="relate-item-detail-description FThin visible-xs">
							@trimWithDot($item->description, 100)
						</span> --}}
					</span>
					<?php
						$bookmark = "F";
				        if(in_array($item->trick_mom_id, $bookmarkList)){
				        	$bookmark = "T";
				        }
					?>
					@if($member_web_id == "" || $bookmark == "T")
						<a href="{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $item->slug }}" class="relate-item-detail-link">
							<img class="icon-link lazyload" width="30px" data-src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-link.png') }}" alt="icon-link" data-expand="+10">
						</a>
					@else
						<a href="javascript:void(0);" onClick="javascript:addBookmark({{ $item->trick_mom_id }}, this)" data-url='{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $item->slug }}' class="list-body-link-bookmark">
						<img class='list-body-logo lazyload' data-src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAARCAYAAAACCvahAAABAklEQVQ4T92TsUoDQRRFz0sTQf/BsRQbK79DJJ0guwELBTGlFjuuY2EnCZZKJk0+IHY2foCVneWO+AUWakTwSSQLy5ospjRT3nfPhXd5I0x5NjYXoAdKbe/MZ9eTbFIBZ4ABBs6HzVnhACz/AzjdX1vSt9dLFUZdNIBF4Bm4E3g59eGwuPuvwpKmORLlvFTQZ02kkXazm0p4NCwFfAFbzodBuXFJInMrQh2VvutlV7khiY0VOEF0x3Wf+rluo5VdRLdV+RAbGx0POs6HVjE9jcx62gsPRc3Gpg387F4JTzqMOYMfgftpH6WgbwCreWHvwMIfoLJlKEnTtMYXNUvAUIXjb9UseSHfehsXAAAAAElFTkSuQmCC' data-expand="+10">
						<span class='FXMed text-bookmark'>จัดเก็บ</span>
					</a>

					@endif
				</li>
				@endforeach
			@endif
		</ul>
	</div>
</div>
@endif
<script type="text/javascript">
	var CONFIRM_RATING = "{{ trans("momtip.CONFIRM_RATING") }}";
</script>
<script>
	window.lazySizesConfig = window.lazySizesConfig || {};
	window.lazySizesConfig.customMedia = {
    	'--mb': '(max-width: 767px)'
	};
</script>
@endsection

@compressJs("lazy.bgset.min,lazysizes.min,slick.min,momtip.detail,favorite")
