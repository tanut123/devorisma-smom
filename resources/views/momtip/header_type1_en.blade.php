<div id="header-momtip">
	<div id="header-momtip-panel" class="content-en">
		<div id="header-momtip-wrapper">
			<section id="header-momtip-content">
				<h1 class="FXregular">
					<img class="logo-mom" src="{{ asset($BASE_CDN . '/images/momtip/logo-mom.png') }}" width='74' height='74'>
					Pregnancy <span class="FThin">Stage</span>
				</h1>
				<h2 class="FThin">The miracle of life</h2>
				<h3 class="FThin">The more you prepare for pregnancy <br class="hidden-xs">care,
					<span class="color-brown">the more complete for child’s<br class="hidden-xs">
development</span>
				</h3>
				<article class="FThin">
					<p>The most important thing to rely on for pregnancy stage is <br class="hidden-xs">credible source of information and change management; <br class="hidden-xs">both mentally and physically for the every best to your <br class="hidden-xs">child</p>
				</article>
			</section>
			<img class="img-responsive mom" src="{{ asset($BASE_CDN . '/images/momtip/mom.png') }}">
		</div>
	</div>
</div>