<div id="header-momtip">
	<div id="header-momtip-panel">
		<div id="header-momtip-wrapper">
			<section id="header-momtip-content">
				<h1 class="FXregular">
					<img class="logo-mom lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/logo-mom-2.png') }}" data-expand="+10" width='74' height='74'>
					คุณแม่<span class="FThin">ให้นมบุตร</span>
				</h1>
				<h2 class="FThin">Breastfeeding is the Best</h2>
				<h3 class="FThin">นมแม่ดีที่สุด
				</h3>
				<article class="FThin">
					<p>มีคุณค่าทางโภชนาการสูงสุด และยังมีภูมิคุ้มกันโรคต่างๆ องค์การอนามัยโลก หรือ WHO แนะนำให้ทารกทานนมแม่เพียงอย่างเดียว 6 เดือน และทานนมแม่ต่อเนื่องไปควบคู่กับอาหารที่เหมาะสมตามวัยจนอายุ 2 ปี หรือมากกว่า S-MomClub ขอร่วมสนับสนุนและส่งเสริมการเลี้ยงลูกด้วยนมแม่</p>
				</article>
			</section>
			<img id="mom-2" class="img-responsive mom lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/mom-2.png') }}">
		</div>
	</div>
</div>