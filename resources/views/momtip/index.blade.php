@extends('template.master')

@compressCss("slick,slick-theme,momtip")

@section('content')
	@include('momtip.header_type'.$type.'_'.trans('core.CORE_LANG'))
	<div class="panel_cat" data-type="{{ $type }}">

	</div>
	<script type="text/javascript">
		var DATA_NOT_FOUND = '{{ trans('core.DATA_NOT_FOUND') }}';
	</script>
	<script>
		window.lazySizesConfig = window.lazySizesConfig || {};
		window.lazySizesConfig.customMedia = {
	    	'--mb': '(max-width: 767px)'
		};
	</script>
@endsection

@compressJs("lazy.bgset.min,lazysizes.min,slick.min,momtip,favorite")
