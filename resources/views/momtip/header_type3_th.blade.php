<div id="header-momtip">
	<div id="header-momtip-panel">
		<div id="header-momtip-wrapper">
			<section id="header-momtip-content">
				<h1 class="FXregular">
					<img class="logo-mom lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/logo-mom-3.png') }}" data-expand="+10" width='74' height='74'>
					ลูกน้อย<span class="FThin">วัยเตาะแตะ</span>
				</h1>
				<h2 class="FThin">พัฒนาการสร้างได้ ด้วยความรัก ความเข้าใจ จากคุณแม่สู่ลูกน้อยวัยเตาะแตะ</h2>
				<h3 class="FThin" style="margin-top:10px;">ธรรมชาติของลูกน้อยวัยเตาะแตะ
					<span class="color-brown">เป็นช่วงวัยที่เด็กเริ่มมีการพัฒนา<br class="dtnl">ทักษะต่างๆ มากมาย ช่างคิด ช่างถาม และช่างเล่น</span>
				</h3>
				<article class="FThin">
					<p>เตรียมพร้อมด้านโภชนาการ รวบรวมทุกเคล็ดลับเพื่อส่งเสริมพัฒนาการของ<br class="hidden-xs" />ลูกน้อยวัยเตาะแตะที่คุณแม่..ต้องรู้! อย่างครบถ้วนกับ S-MomClub เพื่อลูกรัก<br class="dtnl" />เติบโต มีการเรียนรู้ และพัฒนาการเต็มศักยภาพ  ก้าวทันโลกที่เปลี่ยนแปลงอย่าง<br class="dtnl" />รวดเร็วได้อย่างชาญฉลาด </p>
				</article>
			</section>
			<img id="mom-3" class="img-responsive mom lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/mom-3.png') }}">
		</div>
	</div>
</div>