<div id="header-momtip">
	<div id="header-momtip-panel" class="content-en">
		<div id="header-momtip-wrapper">
			<section id="header-momtip-content">
				<h1 class="FXregular">
					<img class="logo-mom" src="{{ asset($BASE_CDN . '/images/momtip/logo-mom-2.png') }}" width='74' height='74'>
					Lactating <span class="FThin">Stage</span>
				</h1>
				<h2 class="FThin">Breastfeeding is the Best</h2>
				<article class="FThin">
					<p>WHO recommends mothers worldwide to exclusively breastfeed infants for the child's first six months to achieve optimal growth, development and health. Thereafter, they should be given nutritious complementary foods and continue breastfeeding up to the age of two years or beyond. S-MomClub strictly follows such statement and fully support breastfeeding policy</p>
				</article>
			</section>
			<img id="mom-2" class="img-responsive mom" src="{{ asset($BASE_CDN . '/images/momtip/mom-2.png') }}">
		</div>
	</div>
</div>