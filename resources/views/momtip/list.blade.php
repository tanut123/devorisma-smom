<?php
    $svg_prefix = "";
    $browser_data = $_SERVER['HTTP_USER_AGENT'];
    if (!preg_match("/MSIE 9/",$browser_data)){
        $svg_prefix = $BASE_LANG . str_replace([1,2,3],["pregnancy-mom","lactating-mom","toddler-mom"], $type);;
    }
?>
@if(count($listData) > 0)
	<?php $count = 0;?>
	@foreach($listData as $key => $data)

	<?php
		$count++;
		$type_mom_data = str_replace([1,2,3],["pregnancy-mom","lactating-mom","toddler-mom"], $data->type_mom);
	?>
	<li data-id="{{ $data->trick_mom_id }}" class="list-item {{ (($count) % 2 == 0) ? 'list-item-center': '' }}">
		<a href="{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $data->slug }}" class="list-body-img-panel">
			<img src="#" alt="">
			@if ($data->youtube_source != "" || $data->file_mp4 != "")
				<span class="cover-video">
					<img src="{{ asset($BASE_CDN . '/images/home/trick-mom/icon-video.png') }}">
				</span>
			@endif
		</a>
		<div class="list-body-content-panel">
			<a href="{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $data->slug }}" class="list-body-title">
				<strong class="FXregular">@trimWithDot($data->title, 56)</strong>
			</a>
			<a href="{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $data->slug }}" class="list-body-description FThin">
				@trimWithDot($data->description, 70)

			</a>
		</div>
		<a href="{{ $BASE_LANG }}{{ $type_mom_data }}/{{ $data->slug }}" class="list-body-link">
			<img class="list-body-logo" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABsklEQVRIib2WvW4CMQzHLXgIRsTWxO5asXRiY6/UnYUFnuEeo7qJgZytLrwGEr3DfgIWtk4sSEgn0QGq0g84rq3y3yLZ+SXxRwxQoXnWaWnmRyo0U8aVCpUmtFeh8rieaeZH86zTqtrrrIrg2sY4UaFShdbGmBrToGDfy6euW7DvGdPAGFMVWqtQaYyTIrh2LZCyH6rQ1pgWy+D6SQKNS/ZJAo1lcH1jWqjQVtkPKyHPD9A0xtQEd8Z+XAX5CWrsxwd/TJ8foHnW2BhTZdzk7O/rQL4qZ3+vjBtjTH80UPZDE9z9FXQKNMHdtyctgmsfYuTH55w14J0KPV18mi8y9mMV2n5KGmOcGNPiUoxepje3yvhqjOFa4CGGtDDGCQAc60ioXAbXr3L+DXAZXF+FynnWacGxYNfXZl5dYJJAQ4XWmvkRqNDsbNb8E/BY+DNQxpUxDerA6gKNaaCMK1ChsmDfqwurAyzY91SoBBPa51PX/Q0MAMACPprQ/lKLyqeua0L7uDeLHbOI2RizzqJ2EICIvREgctcHiPiffZwk0k8NEHkGeVeU6epU0ebGU/3nRPwGRm3+AjGDpggAAAAASUVORK5CYII=">
		</a>
	</li>
	<?php if($count % 3 == 0){  echo '<li class="clearfix"></li>'; $count = 0;} else{ echo ''; }?>
	@endforeach
@endif