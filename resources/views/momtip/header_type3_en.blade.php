<div id="header-momtip">
	<div id="header-momtip-panel">
		<div id="header-momtip-wrapper">
			<section id="header-momtip-content" class="content-en">
				<h1 class="FXregular">
					<img class="logo-mom" src="{{ asset($BASE_CDN . '/images/momtip/logo-mom-3.png') }}" width='74' height='74'>
					Toddler <span class="FThin">Stage</span>
				</h1>
				<h2 class="FThin" style="font-size: 47px;">Child’s development is <br class="hidden-xs">formed by love and <br class="hidden-xs">understanding</h2>
				<h3 class="FThin">
					<span class="color-brown"></span>
				</h3>
				<article class="FThin">
					<p>Toddler stage is the beginning of child’s development<br class="hidden-xs"> that need to be focused so nutrition plays the important <br class="hidden-xs">part for every aspect of development</p>
				</article>
			</section>
			<img id="mom-3" class="img-responsive mom" src="{{ asset($BASE_CDN . '/images/momtip/mom-3.png') }}">
		</div>
	</div>
</div>