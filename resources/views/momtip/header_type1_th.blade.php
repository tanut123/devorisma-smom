<div id="header-momtip">
	<div id="header-momtip-panel" class="lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/bg-mobile.png') }} [--mb] | {{ asset($BASE_CDN . '/images/momtip/bg-main.jpg') }}" data-expand="+10">
		<div id="header-momtip-wrapper">
			<section id="header-momtip-content">
				<h1 class="FXregular">
					<img class="logo-mom lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/logo-mom.png') }}" width='74' height='74' data-expand="+10">
					คุณแม่<span class="FThin">ตั้งครรภ์</span>
				</h1>
				<h2 class="FThin lazyload" data-bgset="{{ asset($BASE_CDN . '/images/momtip/quote.png') }}" data-expand="+10">ความมหัศจรรย์ของชีวิต<br class="mbnl">ในแต่ละช่วงเวลา</h2>
				<h3 class="FThin">การดูแลรอบด้านของคุณแม่ตั้งครรภ์ หมายถึง
					<span class="color-brown">พัฒนาการที่สมบูรณ์<br class="dtnl">ของลูกน้อยในครรภ์</span>
				</h3>
				<article class="FThin">
					<p>ดังนั้น สิ่งหนึ่งที่คุณแม่ตั้งครรภ์ควรให้ความสนใจเป็นพิเศษ คือ การหาข้อมูลเกี่ยวกับการดูแลขณะตั้งครรภ์ เตรียมพร้อมรับมือกับการเปลี่ยนแปลงต่างๆ ที่จะเกิดขึ้น เพื่อที่จะได้มอบสิ่งที่ดีที่สุดให้กับชีวิตน้อยๆ ที่กำลังเติบโตขึ้นในครรภ์</p>

					<p>เมื่อคุณแม่ตั้งครรภ์มีโภชนาการที่ดี ก็จะส่งผลต่อทารกในครรภ์ให้มีสุขภาพที่แข็งแรง . . . เรียนรู้โภชนาการและเคล็ดลับการดูแลสุขภาพที่คุณแม่ตั้งครรภ์ . . ต้องรู้! อย่างครบถ้วนกับ <br class="dtnl"> S-MomClub เพื่อให้ลูกรักเติบโตมีพัฒนาการเต็มศักยภาพตั้งแต่ในครรภ์</p>
				</article>
			</section>
			<img class="img-responsive mom lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/mom.png') }}">
		</div>
	</div>
</div>