@extends('template.master')
@compressCss("everyonecanbethestar")
@compressCss("app")

@section('content')
	<div id="everyonecanbethestar">
		<div style="position: absolute;top:0;left:-100%;height: 1px;width: 1px;text-indent: -9999px;">
				<h1>เมื่อ “พรสวรรค์” ไม่ใช่สำหรับแค่คนเพียงหนึ่งในล้านอีกต่อไป </h1>
				<p>
					เมื่อพูดถึงคำว่า “พรสวรรค์” หรือ “คนที่มีพรสวรรค์” คุณพ่อคุณแม่หลายๆ คน อาจจะนึกถึงภาพนักวิทยาศาสตร์อัจฉริยะ, นักประดิษฐ์ที่มีชื่อเสียง, นักกีฬาเหรียญทองโอลิมปิค, นักธุรกิจที่ประสบความสำเร็จ หรือคนที่ได้รับรางวัลระดับประเทศระดับโลก และอาจจะคิดต่อไปอีกว่า คงจะมีเด็กเพียงไม่กี่คน ที่มีพรสวรรค์ติดตัวมาตั้งแต่เกิด สามารถสร้างความสำเร็จที่ยิ่งใหญ่แบบนั้นได้

				</p>
				<p>
					ในความเป็นจริงแล้วความสำเร็จของเด็กคนนั้นมีติดตัวมาตามธรรมชาติตั้งแต่เกิด(Nature)นั้นจึงเป็นเพียงส่วนหนึ่ง แต่ การเลี้ยงดูของพ่อแม่ (Nurture) ด้วยโภชนาการที่ถูกต้องครบถ้วนตามช่วงวัย, สิ่งแวดล้อม และการเรียนรู้ในสิ่งที่เค้ารัก เป็นส่วนสำคัญยิ่งใหญ่กว่า ที่จะนำไปสู่ความสำเร็จให้พวกเขาได้ ที่ทำให้ลูกประสบความสำเร็จได้ในอนาคต.. 
				</p>
				<p>
					ดังนั้นนิยามคำว่า “คนที่มีพรวรรค์” จึงไม่ได้จำกัดอยู่เพียงกับคนไม่กี่คน ที่ฟ้าประทานความสามารถพิเศษติดตัวมาให้ตั้งแต่เกิดเท่านั้น.. แต่จริงๆ แล้ว เด็กทุกคน ก็สามารถเป็น คนที่มีพรสวรรค์ในแบบของเค้าและประสบความสำเร็จในอนาคตได้เช่นกัน ด้วยการเลี้ยงดูของพ่อแม่ 
				</p>

				<h2>“พรสวรรค์” สร้างได้ ด้วยการเรียนรู้สิ่งที่เค้ารัก</h2>
				<p>เมื่อหันกลับมามองลูกของเรา คุณพ่อคุณแม่หลายคน อาจมีคำถามว่า ลูกของฉันมีพรสวรรค์อะไรกับเขาบ้างไหม? พรสวรรค์สร้างได้จริงหรือ? เราจะสร้างลูกของเราให้มีพรสวรรค์ได้อย่างไร? ซึ่งเป็นคำถามในใจของพ่อแม่ยุคนี้.. </p>
				<p>หรือในบางกรณี อาจจะมีคุณพ่อคุณแม่จำนวนหนึ่ง ที่พยายามผลักดันปลุกปั้นทุกวิถีทางให้ลูกได้เป็นในสิ่งที่อยากให้เป็น เพราะคิดว่า นั่นคือสิ่งที่ดีสำหรับลูกในอนาคต  แต่อาจจะมองข้ามไปว่า แล้วลูกของคุณมีความสุขในการทำสิ่งนั้นอยู่หรือเปล่า และจริงๆ เขาสนใจด้านไหนกันแน่?</p>
				<p>
					เพราะเด็กแต่ละคนแตกต่างกัน มีความชอบ ความถนัด ความสนใจแตกต่างกัน ที่สามารถสร้างเป็น พรสวรรค์สู่ความสำเร็จได้.. ดังนั้น บทบาทของคุณพ่อคุณแม่ คือ 
				</p>
				<p>1.การเลี้ยงดูลูกรักด้วยความใกล้ชิด </p>
				<p>2.มั่นสังเกตความสนใจของลูกตั้งแต่ยังเล็ก </p>
				<p>3.ค้นหาความชอบความถนัดของลูกไปตามช่วงวัย </p>
				<p>4.และที่สำคัญ สนับสนุนส่งเสริมให้เขาเรียนรู้และทำในสิ่งที่เขารัก โดยไม่ปิดกั้นหรือผลักดันให้เขาทำในสิ่งที่เขาไม่ชอบ</p>
				<p>หากเด็กทุกคนได้เรียนรู้ในสิ่งที่เขารัก ตรงกับศักยภาพที่แท้จริงของเขาแล้ว.. เขาจะมีความสุข สนุกกับการเรียนรู้ได้อย่างไร้ขีดจำกัด ทำให้มีความคิดสร้างสรรค์และจินตนาการ สามารถนำไปปรับใช้ได้ไม่ว่าโลกอนาคตจะเป็นอย่างไร โลกที่เขาไม่เพียงแค่ต้องแข่งขันกับคนวัยเดียวกันเท่านั้น แต่ยังต้องแข่งขันกับความก้าวหน้าทางเทคโนโลยี เช่น เทคโนโลยี AI ที่จะมาทดแทนแรงงานมนุษย์ในหลายๆ สาขาอาชีพอีกด้วย.. </p>
				<p>และในวันนั้น คุณพ่อคุณแม่จะมั่นใจและภูมิใจได้ว่า คุณพ่อคุณแม่ได้ส่งมอบสิ่งที่ดีที่สุด เพื่อให้ลูกรักได้ใช้ชีวิตอย่างมีความสุขในแบบของเค้า ไม่ว่าในโลกอนาคตจะเปลี่ยนแปลงไปอย่างไรก็ตาม นั่นเอง</p>
				<p>S-26 Gold Progress เชื่อว่า เด็กแต่ละคนมีความชอบ ความถนัดที่แตกต่างกัน
ศักยภาพของพวกเขา สามารถสร้างเป็น “พรสวรรค์” สู่ความสำเร็จในอนาคตได้
ด้วยการสนับสนุนส่งเสริมจากพ่อแม่ ให้เขาได้เรียนรู้ในสิ่งที่เค้ารักอย่างแท้จริง
ทำให้มีความคิดสร้างสรรค์และจินตนาการ สามารถปรับใช้กับการเปลี่ยนแปลงของโลกอนาคต
เพื่อให้เขาได้ใช้ชีวิตอย่างมั่นคง มีความสุขจากการได้ทำในสิ่งที่เค้ารักได้อย่างแท้จริง
</p>
				<p>#พรสวรรค์ในแบบของเค้า</p>
				<p>#เด็กล้านคนก็ล้านพรสวรรค์</p>
		</div>
		 <div class="everyVideo">
		 	<div class="wrapvideo">
			 	<div class="video">
			 		<div class="embed-responsive embed-responsive-16by9" id="video-box">
	                   <iframe class="embed-responsive-item" id="autoplay-video" src="https://www.youtube.com/embed/NvDB4bPRdvw?rel=0&amp;enablejsapi=1" allowfullscreen></iframe>
	                </div> 
			 	</div>
			 </div>
		 </div>
		 <div class="hidden-xs">
		 	<div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/desktop_02.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
	        <div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/desktop_03.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
	        <div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/desktop_04.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
	        <div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/desktop_05.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
	        <div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/desktop_06.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
		 </div>
		  <div class="visible-xs">
		 	<div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/mobile_02.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
	        <div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/mobile_03.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
	        <div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/mobile_04.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
	        <div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/mobile_05.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
	        <div>
	            <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/everyonecanbethestar/mobile_06.jpg') }}" style="width: 100%" data-expand="+10" />
	        </div>
		 </div>
	</div>
	<script>
	window.lazySizesConfig = window.lazySizesConfig || {};
	window.lazySizesConfig.customMedia = {
    	'--mb': '(max-width: 767px)'
	};
</script>
@endsection


@compressJs("lazy.bgset.min,lazysizes.min")

@section('scripts')

@endsection




