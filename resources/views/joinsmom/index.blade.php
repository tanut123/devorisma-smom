@extends('template.master')
@compressCss("jquery-ui,selectize.custom,joinsmom")
@compressJs("datepicker,selectize,msform")
@section('content')
<div id="joinsmom">
	<div class="title-joinsmom font-xregular">
		<span class="font-xregular">สมัครสมาชิก</span><span class="font-x_li">S-MomClub</span>
	</div>
	<div class="banner">
		<img src="{{ url() }}/images/joinsmom/nurse-team.jpg" alt="ทีมพยาบาล">
	</div>
	<div class="progressbar-title">
		<div id = "progressbar">
			<div class="slide-bar-out">
				<div class="now-progressbar">
					<div class="box">
						<div class="text-box font-xregular">1/3</div>
					</div>
					<div class="arrow-down"></div>
					<div class="slide-bar-in"></div>
				</div>
				<div class="progressbar-active"></div>
			</div>
		</div>
		<form id="msform">
				<input type="hidden" id="trigle" value="0">
				<input type="hidden" id="status_mom" value="">
				<input type="hidden" id="storagezipcode" value="">
				<input type="hidden" id="storagezipcodeSc" value="">
				<fieldset class="show" id="frist-fieldset"> <!--ใช้ร่วมกันทั้งสองสถานะของแม่-->
				<span class="fs-title font-xregular"><i class="status-mom-icon"></i>สถานะคุณแม่</span>
				<div class="inline-radio">
					<input class="radio-position validation-input" type="radio" name="status_mom" id="status_mom1" value="1">
					<label class="label-position font-xregular" for="status_mom1"><span></span>คุณแม่ตั้งครรภ์</label>
				</div>
				<div class="inline-radio">
					<input class="radio-position validation-input" type="radio" name="status_mom" id="status_mom2" value="2">
					<label class="label-position font-xregular" for="status_mom2"><span></span>คุณแม่มีบุตร (ไม่เกิน 2 ขวบ)</label>
				</div>
				<span class="fs-title font-xregular fs-title-sm">กรอกข้อมูลของคุณแม่</span>
				<div class="from-inputtext">
					<label for="firstname" class="label-position-input font-xregular">ชื่อจริง ภาษาไทย</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="firstname" name="firstname" placeholder="กรุณาระบุชื่อจริง">
					<span class="help-block" id="help-block-firstname">กรุณาระบุชื่อจริง</span>
				</div>
				<div class="from-inputtext">
					<label for="lastname" class="label-position-input font-xregular">นามสกุล ภาษาไทย</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="lastname" name="lastname" placeholder="กรุณาระบุนามสกุล">
					<span class="help-block" id="help-block-lastname">กรุณาระบุนามสกุล</span>
				</div>
				<!-- <div class="from-inputtext">
					<label for="birthday" class="label-position-input font-xregular">วันเกิดของผู้สมัคร</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="birthday" name="birthday" placeholder="กรุณาระบุวันเกิด" style="cursor: pointer;" readonly>
					<span class="help-block" id="help-block-birthday">กรุณาระบุวันเกิด</span>
					<div class="stydate-icon"></div>
				</div> -->
				<div class="from-inputtext">
					<label for="mobile" class="label-position-input font-xregular">เบอร์โทรศัพท์มือถือ</label>
					<input type="text" class="text-input-position font-xregular validation-input number-only" id="mobile" name="mobile" placeholder="กรุณาระบุเบอร์โทรศัพท์มือถือ" maxlength="10">
					<span class="help-block" id="help-block-mobile">กรุณาระบุเบอร์โทรศัพท์มือถือ</span>
				</div>
				<div class="from-inputtext">
					<label for="email" class="label-position-input font-xregular">อีเมล</label>
					<input type="email" class="text-input-position font-xregular validation-input" id="email" name="email" placeholder="กรุณาระบุอีเมล">
					<span class="help-block" id="help-block-email">กรุณาระบุอีเมล</span>
				</div>
				<div class="from-inputtext">
					<label for="line_id" class="label-position-input font-xregular">ไลน์ไอดี</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="line_id" name="line_id" placeholder="กรุณาระบุไลน์ไอดี">
					<span class="help-block" id="help-block-line_id">กรุณาระบุไลน์ไอดี</span>
				</div>
				<div class="from-inputtext hidden-xs"></div>
				<div class="from-inputtext">
					<label for="" class="label-position-input font-xregular" style="margin-bottom: 15px;">คุณแม่ทราบข้อมูลการสมัครสมาชิก จากช่องทางใด</label>
					<div class="inline-add-view">
						<input class="radio-position validation-input" type="radio" name="advertisement" id="advertisement_one" value="1">
						<label class="label-position new-bullet font-xregular" for="advertisement_one"><span class="pos-text" style="font-size: 21px;">Facebook</span></label>
						<!-- <label class="label-position new-bullet" for="advertisement_one"></label>
						<label class="title-label font-xregular" style="font-size: 21px;" for="advertisement_one">facebook</label> -->
					</div>
					<div class="inline-add-view">
						<input class="radio-position validation-input" type="radio" name="advertisement" id="advertisement_three" value="3">
						<label class="label-position new-bullet font-xregular" for="advertisement_three"><span class="pos-text" style="font-size: 21px;">ร้านค้า / ตัวแทนจำหน่าย</span></label>
						<!-- <label class="label-position new-bullet" for="advertisement_three"></label>
						<label class="title-label font-xregular" style="font-size: 21px;" for="advertisement_three">โรงพยาบาล</label> -->
					</div>
					<div class="inline-add-view">
						<input class="radio-position validation-input" type="radio" name="advertisement" id="advertisement_two" value="2">
						<label class="label-position new-bullet font-xregular" for="advertisement_two"><span class="pos-text" style="font-size: 21px;">คลินิก</span></label>
						<!-- <label class="label-position new-bullet" for="advertisement_two"></label>
						<label class="title-label font-xregular" style="font-size: 21px;" for="advertisement_two">งาน Mother Class</label> -->
					</div>
					<!-- <div class="inline-add-view">
						<input class="radio-position validation-input" type="radio" name="advertisement" id="advertisement_four" value="4">
						<label class="label-position new-bullet font-xregular" for="advertisement_four"><span class="pos-text" style="font-size: 21px;">งานสัมมนาแม่ท้อง</span></label> -->
						<!-- <label class="label-position new-bullet" for="advertisement_four"></label>
						<label class="title-label font-xregular" style="font-size: 21px;" for="advertisement_four">งาน Event</label> -->
					<!-- </div> -->
					<!-- <div class="inline-add-view">
						<input class="radio-position validation-input" type="radio" name="advertisement" id="advertisement_five" value="5">
						<label class="label-position new-bullet font-xregular" for="advertisement_five"><span class="pos-text" style="font-size: 21px;">งานอีเวนท์ แม่และเด็ก</span></label> -->
						<!-- <label class="label-position new-bullet" for="advertisement_five"></label>
						<label class="title-label font-xregular" style="font-size: 21px;" for="advertisement_five">คลินิค</label> -->
					<!-- </div> -->
					<!-- <div class="inline-add-view">
						<input class="radio-position validation-input" type="radio" name="advertisement" id="advertisement_six" value="6">
						<label class="label-position new-bullet font-xregular" for="advertisement_six"><span class="pos-text" style="font-size: 21px;">โรงพยาบาล</span></label> -->
						<!-- <label class="label-position new-bullet font-xregular" for="advertisement_six"></label>
						<label class="title-label font-xregular" style="font-size: 21px;" for="advertisement_six">สื่ออื่น ๆ</label> -->
					<!-- </div> -->
					<div class="inline-add-view">
						<input class="radio-position validation-input" type="radio" name="advertisement" id="advertisement_seven" value="7">
						<label class="label-position new-bullet font-xregular" for="advertisement_seven"><span class="pos-text" style="font-size: 21px;">อื่น ๆ</span></label>
						<!-- <label class="label-position new-bullet font-xregular" for="advertisement_six"></label>
						<label class="title-label font-xregular" style="font-size: 21px;" for="advertisement_six">สื่ออื่น ๆ</label> -->
					</div>
				</div>
 				<!-- <div class="from-inputtext" id="render-occupation">
					<label for="" class="label-position-input font-xregular">อาชีพ</label>
					<input type="text" class="text-input-position font-xregular validation-input" placeholder="กรุณาระบุอาชีพ" disabled>
					<span class="help-block" id="">กรุณาระบุอาชีพ</span>
				</div> -->
				<!-- <div class="from-inputtext">
					<label for="salary" class="label-position-input font-xregular">รายได้ครัวเรือน</label>
					<input type="text" class="text-input-position font-xregular validation-input number-only" id="salary" name="salary" placeholder="กรุณาระบุรายได้ครัวเรือน">
					<span class="help-block" id="help-block-salary">กรุณาระบุรายได้ครัวเรือน</span>
				</div> -->
				<div class="from-inputtext hidden-xs"></div>
				<div class="from-btn">
					<div class="md-btn">
						<button type="button" name="next" class="next btn-next font-xregular" value="Next"><span class="pageone">ถัดไป</span></button>
					</div>
				</div>
			</fieldset>

			<fieldset id="Pregnant_mothers1"> <!--ใช้สถานะของแม่ตั้งครรภ์-->
				<span class="fs-title font-xregular"><i class="status-mom2-icon"></i>สถานะครรภ์</span>
				<div class="from-inputtext">
					<label for="child_date1" class="label-position-input font-xregular">วันกำหนดคลอด</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="child_date1" name="child_date[]" placeholder="กรุณาระบุวันกำหนดคลอด" readonly>
					<span class="help-block" id="">กรุณาระบุวันกำหนดคลอด</span>
					<div class="stydate-icon"></div>
				</div>
				<div class="from-inputtext hidden-xs">
				</div>
				<div class="from-inputtext hidden-xs">
				</div>
				<div class="from-inputtext hidden-xs">
				</div>
				<div class="from-radio">
					<span class="fs-detail font-xregular fs-title-ssm">สถานที่ฝากครรภ์</span>
					<div class="inline-radio inline-radio-left">
						<input class="radio-position validation-input" type="radio" name="hospital_type" id="hospital_type1" value="T">
						<label class="label-position label-position-sm-font font-xregular" for="hospital_type1"><span></span>โรงพยาบาลรัฐบาล</label>
					</div>
					<div class="inline-radio inline-radio-left">
						<input class="radio-position validation-input" type="radio" name="hospital_type" id="hospital_type2" value="F">
						<label class="label-position label-position-sm-font font-xregular" for="hospital_type2"><span></span>โรงพยาบาลเอกชน</label>
					</div>
				</div>
				<div class="from-inputtext" id="treatment_rights">
				</div>
				<div class="from-inputtext hidden-xs">
				</div>
				<div class="from-inputtext" id="hospital_province">
				</div>
				<div class="from-inputtext" id="hospital_name">
					<label for="" class="label-position-input font-xregular">ชื่อโรงพยาบาล</label>
					<input type="text" class="text-input-position font-xregular validation-input" placeholder="กรุณาระบุชื่อโรงพยาบาล" disabled>
					<span class="help-block" id="">กรุณาระบุชื่อโรงพยาบาล</span>
				</div>
				<br>
				<div class="from-inputtext">
					<label for="address1" class="label-position-input font-xregular">ที่อยู่จัดส่งเอกสาร</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="address1" name="address" placeholder="กรุณาระบุที่อยู่(บ้านเลขที่)">
					<span class="help-block" id="">กรุณาระบุที่อยู่(บ้านเลขที่)</span>
				</div>
				<div class="from-inputtext">
					<label for="moo2" class="label-position-input font-xregular">หมู่</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="moo1" name="moo" placeholder="กรุณาระบุหมู่">
					<span class="help-block" id="">กรุณาระบุหมู่</span>
				</div>
				<div class="from-inputtext">
					<label for="alley1" class="label-position-input font-xregular">ตรอก/ซอย</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="alley1" name="alley" placeholder="กรุณาระบุตรอก/ซอย">
					<span class="help-block" id="">กรุณาระบุตรอก/ซอย</span>
				</div>
				<div class="from-inputtext">
					<label for="road1" class="label-position-input font-xregular">ถนน</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="road1" name="road" placeholder="กรุณาระบุถนน">
					<span class="help-block" id="">กรุณาระบุถนน</span>
				</div>
				<div class="from-inputtext" id="province_address">
				</div>
				<div class="from-inputtext" id="district_address">
					<label for="" class="label-position-input font-xregular">อำเภอ/เขต</label>
					<input type="text" class="text-input-position font-xregular validation-input" placeholder="กรุณาระบุอำเภอ/เขต" disabled>
					<span class="help-block" id="">กรุณาระบุอำเภอ/เขต</span>
				</div>
				<div class="from-inputtext" id="subdistrict">
					<label for="" class="label-position-input font-xregular">ตำบล/แขวง</label>
					<input type="text" class="text-input-position font-xregular validation-input" placeholder="กรุณาระบุตำบล/แขวง" disabled>
					<span class="help-block" id="">กรุณาระบุตำบล/แขวง</span>
				</div>
				<div class="from-inputtext">
					<label for="zipcode1" class="label-position-input font-xregular">รหัสไปรษณีย์</label>
					<input type="text" class="text-input-position font-xregular validation-input number-only" id="zipcode1" name="zipcode" placeholder="กรุณาระบุรหัสไปรษณีย์">
					<span class="help-block" id="">กรุณาระบุรหัสไปรษณีย์</span>
				</div>
				<div class="from-btn">
					<div class="md-btn">
						<button type="button" name="next" class="previous btn-prev font-xregular" value="previous"><span class="pagetwo-prev">ย้อนกลับ</span></button>
					</div>
				</div>
				<div class="from-btn">
					<div class="md-btn">
						<button type="button" name="next" class="next btn-next font-xregular" value="Next"><span class="pagetwo">ถัดไป</span></button>
					</div>
				</div>
			</fieldset>

			<fieldset id="Pregnant_mothers2">
				<span class="fs-title font-xregular"><i class="productmom-icon"></i>ข้อมูลการใช้ผลิตภัณฑ์</span>
				<div class="from-inputtext" id="currentproduct"></div>
				<div class="from-inputtext"></div>
				<!-- <div class="from-inputtext">
					<label for="web_page1" class="label-position-input font-xregular">คุณแม่เข้าใช้งานเว็บไซด์หรือเพจไหนบ่อยที่สุด</label>
					<input type="text" class="text-input-position add-width-input font-xregular validation-input" id="web_page1" name="web_page" placeholder="ระบุชื่อเว็บไซด์ หรือ เฟสบุ๊คเพจ">
					<span class="help-block" id="">ระบุชื่อเว็บไซด์ หรือ เฟสบุ๊คเพจ</span>
				</div> -->
				<div class="from-inputtext"></div>
				<div class="from-inputtext form-full last">
					<label class="label-position-input font-xregular">คุณคิดว่า S-Mom Club คือศูนย์ลูกค้าสัมพันธ์ของ ผลิตภัณฑ์นมยี่ห้อใด (ตอบเพียง 1 ข้อ)</label>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name11" value="1">
						<label class="label-position font-xregular" for="product_name11"><span></span>S-26</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name12" value="2">
						<label class="label-position font-xregular" for="product_name12"><span></span>เอนฟา</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name13" value="3">
						<label class="label-position font-xregular" for="product_name13"><span></span>แอนมัม</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name14" value="4">
						<label class="label-position font-xregular" for="product_name14"><span></span>ไฮคิว</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name15" value="5">
						<label class="label-position font-xregular" for="product_name15"><span></span>ดูเม็กซ์</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name16" value="10" data-tiger="1">
						<label class="label-position font-xregular" for="product_name16"><span></span>อื่น ๆ</label>
						<span class="other-elm1"></span>
					</div>
					<!-- <div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name17" value="7">
						<label class="label-position font-xregular" for="product_name17"><span></span>พีเดียชัวร์</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name18" value="8">
						<label class="label-position font-xregular" for="product_name18"><span></span>ซิมิแลค</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name19" value="9">
						<label class="label-position font-xregular" for="product_name19"><span></span>ดีจี</label>
					</div> -->
				</div>
				<div class="from-inputtext"></div>
				<br>
				<div class="from-btn">
					<div class="md-btn">
						<button type="button" name="submit" class="btn-next font-xregular submit" value="submit"><span class="pagetree-sb">ยืนยันการสมัคร</span></button>
					</div>
				</div>
			</fieldset>
			<fieldset id="Status_of_childs1">
				<span class="fs-title font-xregular"><i class="status-mom3-icon"></i>สถานะการมีบุตร</span>
				<span class="fs-title font-xregular fs-title-sm">กรุณากรอกข้อมูลสถานะของบุตร</span>
				<span class="fs-title font-xregular fs-title-smx">มีบุตรคนเดียวใช่หรือไม่</span>
				<div class="inline-radio">
					<input class="radio-position validation-input" type="radio" name="has_one_child" id="has_one_child2" value="T">
					<label class="label-position label-position-sm-font font-xregular" for="has_one_child2"><span></span>ใช่</label>
				</div>
				<div class="inline-radio">
					<input class="radio-position validation-input" type="radio" name="has_one_child" id="has_one_child3" value="F">
					<label class="label-position label-position-sm-font font-xregular" for="has_one_child3"><span></span>ไม่ใช่ มีบุตรจำนวน</label>
					<div class="inline-box-select font-xregular validation-input">
						<select id="quarity_boy" class="radio-text-select-position validation-input" name="quarity_boy" style="cursor: pointer;">
							<option value="">เลือก</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="4">4</option>
							<option value="5">5</option>
						</select>
						<span class="fa-sort-desc-radio"></span>
					</div>
					<label class="label-position label-position-sm-font font-xregular"> คน</label>
				</div>
				<div class="hr-line">
					<hr class="hr-custom-color">
				</div>
				<div class="clone-form"></div>
				<div class="from-inputtext">
					<label for="address2" class="label-position-input font-xregular">ที่อยู่จัดส่งเอกสาร</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="address2" name="address" placeholder="กรุณาระบุที่อยู่(บ้านเลขที่)">
					<span class="help-block" id="">กรุณาระบุที่อยู่(บ้านเลขที่)</span>
				</div>
				<div class="from-inputtext">
					<label for="moo2" class="label-position-input font-xregular">หมู่</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="moo2" name="moo" placeholder="กรุณาระบุหมู่">
					<span class="help-block" id="">กรุณาระบุหมู่</span>
				</div>
				<div class="from-inputtext">
					<label for="alley2" class="label-position-input font-xregular">ตรอก/ซอย</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="alley2" name="alley" placeholder="กรุณาระบุตรอก/ซอย">
					<span class="help-block" id="">กรุณาระบุตรอก/ซอย</span>
				</div>
				<div class="from-inputtext">
					<label for="road2" class="label-position-input font-xregular">ถนน</label>
					<input type="text" class="text-input-position font-xregular validation-input" id="road2" name="road" placeholder="กรุณาระบุถนน">
					<span class="help-block" id="">กรุณาระบุถนน</span>
				</div>
				<div class="from-inputtext" id="provinceSc_address">
				</div>
				<div class="from-inputtext" id="districtSc_address">
					<label for="" class="label-position-input font-xregular">อำเภอ/เขต</label>
					<input type="text" class="text-input-position font-xregular validation-input" placeholder="กรุณาระบุอำเภอ/เขต" disabled>
					<span class="help-block" id="">กรุณาระบุอำเภอ/เขต</span>
				</div>
				<div class="from-inputtext" id="subdistrictSc">
					<label for="" class="label-position-input font-xregular">ตำบล/แขวง</label>
					<input type="text" class="text-input-position font-xregular validation-input" placeholder="กรุณาระบุตำบล/แขวง" disabled>
					<span class="help-block" id="">กรุณาระบุตำบล/แขวง</span>
				</div>
				<div class="from-inputtext">
					<label for="zipcode2" class="label-position-input font-xregular">รหัสไปรษณีย์</label>
					<input type="text" class="text-input-position font-xregular validation-input number-only" id="zipcode2" name="zipcode" placeholder="กรุณาระบุรหัสไปรษณีย์">
					<span class="help-block" id="">กรุณาระบุรหัสไปรษณีย์</span>
				</div>
				<div class="from-btn">
					<div class="md-btn">
						<button type="button" name="next" class="previous btn-prev font-xregular" value="previous"><span class="pagetwo-prevs">ย้อนกลับ</span></button>
					</div>
				</div>
				<div class="from-btn">
					<div class="md-btn">
						<button type="button" name="next" class="next btn-next font-xregular" value="Next"><span class="pagetwo">ถัดไป</span></button>
					</div>
				</div>
			</fieldset>
			<fieldset id="Status_of_childs2">
				<span class="fs-title font-xregular"><i class="productmom-icon"></i>ข้อมูลการใช้ผลิตภัณฑ์</span>
				<div class="from-inputtext" id="currentproductSc"></div>
				<div class="from-inputtext" id="previousproduct"></div>
				<div class="from-inputtext" id="reasonSc"></div>
				<div class="from-inputtext"></div>
				<!-- <div class="from-inputtext">
					<label for="web_page2" class="label-position-input font-xregular">คุณแม่เข้าใช้งานเว็บไซด์หรือเพจไหนบ่อยที่สุด</label>
					<input type="text" class="text-input-position add-width-input font-xregular validation-input" id="web_page2" name="web_page" placeholder="ระบุชื่อเว็บไซด์ หรือ เฟสบุ๊คเพจ">
					<span class="help-block" id="">ระบุชื่อเว็บไซด์ หรือ เฟสบุ๊คเพจ</span>
				</div> -->
				<div class="from-inputtext"></div>
				<div class="from-inputtext form-full last">
					<label class="label-position-input font-xregular">คุณคิดว่า S-Mom Club คือศูนย์ลูกค้าสัมพันธ์ของ ผลิตภัณฑ์นมยี่ห้อใด (ตอบเพียง 1 ข้อ)</label>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name21" value="1">
						<label class="label-position font-xregular" for="product_name21"><span></span>S-26</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name22" value="2">
						<label class="label-position font-xregular" for="product_name22"><span></span>เอนฟา</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name23" value="3">
						<label class="label-position font-xregular" for="product_name23"><span></span>แอนมัม</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name24" value="4">
						<label class="label-position font-xregular" for="product_name24"><span></span>ไฮคิว</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name25" value="5">
						<label class="label-position font-xregular" for="product_name25"><span></span>ดูเม็กซ์</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name26" value="10" data-tiger="2">
						<label class="label-position font-xregular" for="product_name26"><span></span>อื่น ๆ</label>
						<span class="other-elm2"></span>
					</div>
					<!-- <div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name26" value="6">
						<label class="label-position font-xregular" for="product_name26"><span></span>นมตราหมี</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name27" value="7">
						<label class="label-position font-xregular" for="product_name27"><span></span>พีเดียชัวร์</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name28" value="8">
						<label class="label-position font-xregular" for="product_name28"><span></span>ซิมิแลค</label>
					</div>
					<div class="row-radio">
						<input class="radio-position validation-input" type="radio" name="product_name" id="product_name29" value="9">
						<label class="label-position font-xregular" for="product_name29"><span></span>ดีจี</label>
					</div> -->
				</div>
				<div class="from-inputtext"></div>
				<br>
				<div class="from-btn">
					<div class="md-btn">
						<button type="button" name="submit" class="btn-next font-xregular submit" value="submit"><span class="pagetree-sb">ยืนยันการสมัคร</span></button>
					</div>
				</div>
			</fieldset>
		</form>
	</div>
	<div id="varlidate-status-mom" class="modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="from-checkbox">
						<input class="checkbox-position" type="checkbox" name="checkStatus1" id="checkStatus1">
						<label class="label-position label-position-ssm-font font-xregular" for="checkStatus1"><span></span><div class="checkbox-aling-text">ข้าพเจ้าอายุเกิน 13 ปี และประสงค์ที่จะรับข้อมูลเกี่ยวกับการส่งเสริมการขาย ข้อเสนอแนะและส่วนลดจาก Nestle รวมถึงการติดต่อสื่อสารจากทางชื่อทางการค้าอื่นๆของ Nestle</div></label>
					</div>
					<div class="from-checkbox">
						<input class="checkbox-position" type="checkbox" name="checkStatus2" id="checkStatus2">
						<label class="label-position label-position-ssm-font font-xregular" for="checkStatus2"><span></span><div class="checkbox-aling-text">ข้าพเจ้ายอมรับว่า Nestle จะประมวลผลข้อมูลส่วนบุคคลของข้าพเจ้าตามประกาศที่เกี่ยวกับ<a href="{{ $BASE_LANG }}page/privacy" target="_blank">ความเป็นส่วนตัวของ Nestle</a></div></label>
					</div>
					<div class="from-checkbox">
						<input class="checkbox-position" type="checkbox" name="checkStatus3" id="checkStatus3">
						<label class="label-position label-position-ssm-font font-xregular" for="checkStatus3"><span></span><div class="checkbox-aling-text">หากต้องการแจ้งเปลี่ยนแปลงข้อมูลสมาชิก หรือยกเลิก กรุณาติดต่อ <a href="tel:026402288">02 640 2288</a></div></label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="testsuccessregister" class="modal">
		<div class="modal-dialog registersuccess">
			<div class="modal-content delh">
				<div class="modal-body">
					<div style="text-align: center;">
					<div class='smum_club_logo'>
                        <img src="{{ url() }}/images/footer/smom_logo_new.png" width="124" height="98">
                    </div>
					</div>
					<div style="text-align: center; font-size: 30px; margin-bottom: 14px;">สมัครสมาชิกเรียบร้อยแล้วค่ะ</div>
					<div style="text-align: center; font-size: 24px;">ยินดีตอนรับเข้าสู่ครอบครัว S-MomClub</div>
					<div style="text-align: center; font-size: 24px;">คุณแม่สามารถรับคำปรึกษาจากทีมพยาบาลวิชาชีพ</div>
					<div style="text-align: center; font-size: 24px;">และผู้เชี่ยวชาญ ตลอด 24 ชม.</div>
					<div style="text-align: center; font-size: 24px;"><a href="tel:026402288" style="color:unset;">โทร. 0-2640-2288</a></div>
					<div style="text-align: center; font-size: 24px;"><a href="http://line.me/ti/p/@S-MomClub" target="_blank" style="color:unset;">Line : @S-MomClub</a> และ <a href="https://www.facebook.com/Smomclub" target="_blank" style="color:unset;">Facebook : SMomClub</a></div>
				</div>
				<div class="modal-footer" style="border-top: unset; text-align: center;">
					<!-- <button type="button" class="btn btn-default btnSuccess">ตกลง</button> -->
					<a class="btn btn-default btnSuccess" href="{{ $BASE_LANG }}home"  style="width: 28px;overflow: visible;">ตกลง</a>
				</div>
			</div>
		</div>
	</div>
	<div id="case_one" class="modal">
		<div class="modal-dialog registersuccess">
			<div class="modal-content delh">
				<div class="modal-body">
					<div style="text-align: center;">
					<div class='smum_club_logo'>
                        <img src="{{ url() }}/images/footer/smom_logo_new.png" width="124" height="98">
                    </div>
					</div>
					<div style="text-align: center; font-size: 30px;">สมัครสมาชิกเรียบร้อยแล้วค่ะ</div>
					<div style="text-align: center; font-size: 22px; margin-bottom: 14px;">พิเศษสำหรับคุณแม่ ที่เป็นสมาชิก S-MomClub</div>
					<div style="text-align: center; font-size: 24px;">ขอเชิญคุณแม่เข้ากรุ๊ป Line "ห้องแม่ท้อง"</div>
					<div style="text-align: center; font-size: 24px;">คุณแม่สามารถรับคำปรึกษาจากทีมพยาบาลวิชาชีพ</div>
					<div style="text-align: center; font-size: 24px;">และผู้เชี่ยวชาญ ตลอด 24 ชม.</div>
					<div style="text-align: center; font-size: 24px;">Click เพื่อเข้าห้อง ได้ที่นี้เลยค่ะ</div>
					<div class="hide-block-info-mobile btnSuccess" style="text-align: center; font-size: 24px;">
						<a href="https://line.me/R/ti/g/_SIEa38OiX" target="_blank" style="color:unset;">
							<img src="{{ url() }}/images/footer/image001.png" width="150" height="150">
						</a>
					</div>
				</div>
				<div class="modal-footer" style="border-top: unset; text-align: center;">
					<button class="btn btn-default btnSuccess hide-block-info-desktop btn-mb-ct" onclick="window.open('https://line.me/R/ti/g/_SIEa38OiX', '_blank');">ตกลง</button>
					<a class="btn btn-default linkOut hide-block-info-mobile" href="https://line.me/R/ti/g/_SIEa38OiX"  style="width: 28px;overflow: visible;">ตกลง</a>
				</div>
			</div>
		</div>
	</div>
	<div id="case_two" class="modal">
		<div class="modal-dialog registersuccess">
			<div class="modal-content delh">
				<div class="modal-body">
					<div style="text-align: center;">
					<div class='smum_club_logo'>
                        <img src="{{ url() }}/images/footer/smom_logo_new.png" width="124" height="98">
                    </div>
					</div>
					<div style="text-align: center; font-size: 30px;">สมัครสมาชิกเรียบร้อยแล้วค่ะ</div>
					<div style="text-align: center; font-size: 22px; margin-bottom: 14px;">พิเศษสำหรับคุณแม่ ที่เป็นสมาชิก S-MomClub</div>
					<div style="text-align: center; font-size: 24px;">ขอเชิญคุณแม่เข้ากรุ๊ป Line "ห้องนมแม่"</div>
					<div style="text-align: center; font-size: 24px;">คุณแม่สามารถรับคำปรึกษาจากทีมพยาบาลวิชาชีพ</div>
					<div style="text-align: center; font-size: 24px;">และผู้เชี่ยวชาญ ตลอด 24 ชม.</div>
					<div style="text-align: center; font-size: 24px;">Click เพื่อเข้าห้อง ได้ที่นี้เลยค่ะ</div>
					<div class="hide-block-info-mobile btnSuccess" style="text-align: center; font-size: 24px;">
						<a href="https://line.me/R/ti/g/9evYiCRM8y" target="_blank" style="color:unset;">
							<img src="{{ url() }}/images/footer/image002.png" width="150" height="150">
						</a>
					</div>
				</div>
				<div class="modal-footer" style="border-top: unset; text-align: center;">
					<button class="btn btn-default btnSuccess hide-block-info-desktop btn-mb-ct" onclick="window.open('https://line.me/R/ti/g/9evYiCRM8y', '_blank');">ตกลง</button>
					<a class="btn btn-default linkOut hide-block-info-mobile" href="https://line.me/R/ti/g/9evYiCRM8y"  style="width: 28px;overflow: visible;">ตกลง</a>
				</div>
			</div>
		</div>
	</div>
	<div id="case_three" class="modal">
		<div class="modal-dialog registersuccess">
			<div class="modal-content delh">
				<div class="modal-body">
					<div style="text-align: center;">
					<div class='smum_club_logo'>
                        <img src="{{ url() }}/images/footer/smom_logo_new.png" width="124" height="98">
                    </div>
					</div>
					<div style="text-align: center; font-size: 30px;">สมัครสมาชิกเรียบร้อยแล้วค่ะ</div>
					<div style="text-align: center; font-size: 22px; margin-bottom: 14px;">พิเศษสำหรับคุณแม่ ที่เป็นสมาชิก S-MomClub</div>
					<div style="text-align: center; font-size: 24px;">ขอเชิญคุณแม่เข้ากรุ๊ป Line "ห้องเรียนเด็ก"</div>
					<div style="text-align: center; font-size: 24px;">คุณแม่สามารถรับคำปรึกษาจากทีมพยาบาลวิชาชีพ</div>
					<div style="text-align: center; font-size: 24px;">และผู้เชี่ยวชาญ ตลอด 24 ชม.</div>
					<div style="text-align: center; font-size: 24px;">Click เพื่อเข้าห้อง ได้ที่นี้เลยค่ะ</div>
					<div class="hide-block-info-mobile btnSuccess" style="text-align: center; font-size: 24px;">
						<a href="https://line.me/R/ti/g/GT9eI7W4Dw" target="_blank" style="color:unset;">
							<img src="{{ url() }}/images/footer/image003.png" width="150" height="150">
						</a>
					</div>
				</div>
				<div class="modal-footer" style="border-top: unset; text-align: center;">
					<button class="btn btn-default btnSuccess hide-block-info-desktop btn-mb-ct" onclick="window.open('https://line.me/R/ti/g/GT9eI7W4Dw', '_blank');">ตกลง</button>
					<a class="btn btn-default linkOut hide-block-info-mobile" href="https://line.me/R/ti/g/GT9eI7W4Dw" style="width: 28px;overflow: visible;">ตกลง</a>
				</div>
			</div>
		</div>
	</div>
	<div id="case_four" class="modal">
		<div class="modal-dialog registersuccess">
			<div class="modal-content delh">
				<div class="modal-body">
					<div style="text-align: center;">
					<div class='smum_club_logo'>
                        <img src="{{ url() }}/images/footer/smom_logo_new.png" width="124" height="98">
                    </div>
					</div>
					<div style="text-align: center; font-size: 30px;">สมัครสมาชิกเรียบร้อยแล้วค่ะ</div>
					<div style="text-align: center; font-size: 22px; margin-bottom: 14px;">พิเศษสำหรับคุณแม่ ที่เป็นสมาชิก S-MomClub</div>
					<div style="text-align: center; font-size: 24px;">ขอเชิญคุณแม่เข้ากรุ๊ป Line "ห้องแม่ท้อง"</div>
					<div style="text-align: center; font-size: 24px;">คุณแม่สามารถรับคำปรึกษาจากทีมพยาบาลวิชาชีพ</div>
					<div style="text-align: center; font-size: 24px;">และผู้เชี่ยวชาญ ตลอด 24 ชม.</div>
					<div style="text-align: center; font-size: 24px;">Click เพื่อเข้าห้อง ได้ที่นี้เลยค่ะ</div>
					<div class="hide-block-info-mobile btnSuccess" style="text-align: center; font-size: 24px;">
						<a href="https://line.me/ti/g2/HIOVO7ODZ3" target="_blank" style="color:unset;">
							<img src="{{ url() }}/images/footer/image004.png" width="150" height="150">
						</a>
					</div>
				</div>
				<div class="modal-footer" style="border-top: unset; text-align: center;">
					<button class="btn btn-default btnSuccess hide-block-info-desktop btn-mb-ct" onclick="window.open('https://line.me/ti/g2/HIOVO7ODZ3', '_blank');">ตกลง</button>
					<a class="btn btn-default linkOut hide-block-info-mobile" href="https://line.me/ti/g2/HIOVO7ODZ3" style="width: 28px;overflow: visible;">ตกลง</a>
				</div>
			</div>
		</div>
	</div>
	<div id="case_five" class="modal">
		<div class="modal-dialog registersuccess">
			<div class="modal-content delh">
				<div class="modal-body">
					<div style="text-align: center;">
					<div class='smum_club_logo'>
                        <img src="{{ url() }}/images/footer/smom_logo_new.png" width="124" height="98">
                    </div>
					</div>
					<div style="text-align: center; font-size: 30px;">สมัครสมาชิกเรียบร้อยแล้วค่ะ</div>
					<div style="text-align: center; font-size: 22px; margin-bottom: 14px;">พิเศษสำหรับคุณแม่ ที่เป็นสมาชิก S-MomClub</div>
					<div style="text-align: center; font-size: 24px;">ขอเชิญคุณแม่เข้ากรุ๊ป Line "ห้องนมแม่"</div>
					<div style="text-align: center; font-size: 24px;">คุณแม่สามารถรับคำปรึกษาจากทีมพยาบาลวิชาชีพ</div>
					<div style="text-align: center; font-size: 24px;">และผู้เชี่ยวชาญ ตลอด 24 ชม.</div>
					<div style="text-align: center; font-size: 24px;">Click เพื่อเข้าห้อง ได้ที่นี้เลยค่ะ</div>
					<div class="hide-block-info-mobile btnSuccess" style="text-align: center; font-size: 24px;">
						<a href="https://line.me/ti/g2/APWGE1L3C2" target="_blank" style="color:unset;">
							<img src="{{ url() }}/images/footer/image005.png" width="150" height="150">
						</a>
					</div>
				</div>
				<div class="modal-footer" style="border-top: unset; text-align: center;">
					<button class="btn btn-default btnSuccess hide-block-info-desktop btn-mb-ct" onclick="window.open('https://line.me/ti/g2/APWGE1L3C2', '_blank');">ตกลง</button>
					<a class="btn btn-default linkOut hide-block-info-mobile" href="https://line.me/ti/g2/APWGE1L3C2" style="width: 28px;overflow: visible;">ตกลง</a>
				</div>
			</div>
		</div>
	</div>
	<div id="case_six" class="modal">
		<div class="modal-dialog registersuccess">
			<div class="modal-content delh">
				<div class="modal-body">
					<div style="text-align: center;">
					<div class='smum_club_logo'>
                        <img src="{{ url() }}/images/footer/smom_logo_new.png" width="124" height="98">
                    </div>
					</div>
					<div style="text-align: center; font-size: 30px;">สมัครสมาชิกเรียบร้อยแล้วค่ะ</div>
					<div style="text-align: center; font-size: 22px; margin-bottom: 14px;">พิเศษสำหรับคุณแม่ ที่เป็นสมาชิก S-MomClub</div>
					<div style="text-align: center; font-size: 24px;">ขอเชิญคุณแม่เข้ากรุ๊ป Line "ห้องแม่ Organic"</div>
					<div style="text-align: center; font-size: 24px;">คุณแม่สามารถรับคำปรึกษาจากทีมพยาบาลวิชาชีพ</div>
					<div style="text-align: center; font-size: 24px;">และผู้เชี่ยวชาญ ตลอด 24 ชม.</div>
					<div style="text-align: center; font-size: 24px;">Click เพื่อเข้าห้อง ได้ที่นี้เลยค่ะ</div>
					<div class="hide-block-info-mobile btnSuccess" style="text-align: center; font-size: 24px;">
						<a href="https://line.me/R/ti/g/eDfQEjYqQG" target="_blank" style="color:unset;">
							<img src="{{ url() }}/images/footer/case_six.jpg" width="150" height="150">
						</a>
					</div>
				</div>
				<div class="modal-footer" style="border-top: unset; text-align: center;">
					<button class="btn btn-default btnSuccess hide-block-info-desktop btn-mb-ct" onclick="window.open('https://line.me/R/ti/g/eDfQEjYqQG', '_blank');">ตกลง</button>
					<a class="btn btn-default linkOut hide-block-info-mobile" href="https://line.me/R/ti/g/eDfQEjYqQG" style="width: 28px;overflow: visible;">ตกลง</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection