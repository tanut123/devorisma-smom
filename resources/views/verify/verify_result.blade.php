@extends('template.master')
@compressCss("verify")
@section('content')
    <div id='verify_result'>
        <div class='panel_align'>
            <div class='data_panel'>
                <div class='FX header anti_alias'>
                    {{trans("verify.VERIFY_HEADER")}}
                </div>
                <div class='FBold detail1 anti_alias'>
                    {{ $result_status }}
                </div>
            </div>
        </div>
    </div>
@endsection