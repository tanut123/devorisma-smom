@extends('sphingomyelin.detail.template')

@section('content_detail_sphingomyelin')
	<div class="sphingomyelin-detail-content detail-1">
		<div class="detail-content">
			<img class="detail-image hidden-xs lazyload" data-src="{{ asset('/images/sphingomyelin/detail/index/img01.jpg') }}" data-expand="+10"/>
			<img class="detail-image-index visible-xs lazyload" data-src="{{ asset('/images/sphingomyelin/detail/index/img01-mobile.jpg') }}" data-expand="+10"/>
			<img class="detail-title-image hidden-xs lazyload" data-src="{{ asset('/images/sphingomyelin/detail/index/title.png') }}" data-expand="+10"/>
			<img class="detail-title-image visible-xs lazyload" data-src="{{ asset('/images/sphingomyelin/detail/index/title-mobile.png') }}" data-expand="+10"/>
			<div class="detail-box title">
				<span class="FXBold color-blue">ช่วงอายุ 1-3 ปี ควรเน้นการกระตุ้นวงจรประสาทของภาษา</span>
				เช่น การชี้อวัยวะบนใบหน้า การเรียกชื่อสิ่งของที่ใช้บ่อยในชีวิตประจำวัน การร้องเพลงหรือการเล่านิทานให้ลูกฟัง เพื่อเพิ่มการสร้างปลอกไมอีลินในวงจรประสาทภาษา
			</div>
			<div class="detail-box title">
				<span class="FXBold color-blue">นอกจากนี้ยังควรฝึกทักษะของกล้ามเนื้อมัดเล็ก</span> อันได้เเก่ การต่อของเล่น การใช้ช้อนในการตักอาหาร เพื่อช่วยการสร้างปลอกไมอีลินในวงจรประสาทในการทำงานระหว่างกล้ามเนื้อมือเเละการมองเห็น อันจะเป็นพื้นฐานของการเขียนหนังสือเเละทักษะทางมือต่อไป
			</div>
			<div class="detail-box title">
				<span class="FXBold color-blue">รวมไปถึงการฝึกการเรียนรู้ขั้นพื้นฐาน</span> ได้เเก่ การจับคู่สิ่งของที่มีคุณสมบัติเหมือนกัน เช่น ของที่รูปทรงเหมือนกันหรือสีเหมือนกันหรือการส่งเสริมให้เด็กเล่นเลียนเเบบพฤติกรรมของผู้ใหญ่ที่ตัวเขาเห็นในชีวิตประจำวัน ก็จะช่วยให้มีการสร้างปลอกไมอีลินในวงจรประสาทในการเรียนรู้ที่ได้
			</div>
			<div class="detail-box title">
				<span class="FXBold color-blue">สำหรับเด็กที่โตขึ้น การฝึกให้เด็กได้รู้จักการเเก้ไขปัญหาที่เจอในชีวิตประจำวัน รวมไปถึงการสอนความเป็น "เหตุ" เเละ "ผล" ของสิ่งต่างๆ ที่มีความเกี่ยวข้องกัน รวมไปถึงการสอนให้รู้จักอดทนเเละรอคอย</span> ก็จะช่วยให้การสร้างปลอกไมอีลินในวงจรประสาทในส่วนของการทำงานขั้นสูงของสมองเกิดขึ้นได้อย่างมีประสิทธิภาพอันเป็นรากฐานของการเรียนรู้สิ่งที่เป็นนามธรรม การคิดวิเคราะห์ การวางเเผน การควบคุมตนเอง เเละการเเก้ไขปัญหาเฉพาะหน้า ซึ่งเป็นกระบวนการทางสติปัญญาที่จะมีการพัฒนาต่อไปในช่วงวัยเด็กโตเเละวัยรุ่น
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
@endsection