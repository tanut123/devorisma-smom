@extends('sphingomyelin.detail.template')

@section('content_detail_sphingomyelin')
	<div class="sphingomyelin-detail-content">
		<img class="hidden-xs title-image lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/txt-title.png') }}" data-expand="+10"/>
		<img class="visible-xs title-image lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/txt-title-mobile.png') }}" data-expand="+10"/>
		<div class="detail-doctor-name FXBold">
			ข้อมูลบทความโดย ผศ.นพ.วรสิทธิ์ ศิริพรพาณิชย์
		</div>
		<div class="detail-content">
			<img class="detail-banner hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/banner.jpg') }}" data-expand="+10"/>
			<img class="detail-banner visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/banner-mobile.jpg') }}" data-expand="+10"/>
			<div class="detail-box FXBold color-blue space-bottom">
				เชื่อว่าคุณพ่อคุณแม่ทุกคนคงอยากให้ลูกน้อยของตัวเองเป็นเด็กฉลาดและมีสติปัญญาที่ดี เพื่อปูทางสู่อนาคตที่ดียิ่งขึ้น <br class="hidden-xs"/>และสิ่งที่จะสร้างความเฉลียวฉลาดให้กับลูกน้อยของเราได้นั้น ก็คือการพัฒนาของสมองอย่างมีประสิทธิภาพนั่นเอง
			</div>
			<div class="detail-box FXBold color-blue title">
				การพัฒนาของสมองกับความฉลาด
			</div>
			<div class="detail-box">
				ถึงแม้ความเข้าใจเกี่ยวกับการทำงานของสมองที่มีผลต่อความเฉลียวฉลาดของคนเรานั้นยังไม่เป็นที่ทราบกันชัดเจนเท่าไรนัก <br class="hidden-xs"/>แต่ก็เป็นที่รับรู้กันโดยทั่วไปว่า สมองเป็นอวัยวะที่มีบทบาทสำคัญต่อการพัฒนาด้านสติปัญญา โดยเฉพาะความเฉลียวฉลาด <br class="hidden-xs"/>ดังนั้น การพัฒนาสมองจะนำไปสู่ความสามารถทางสติปัญญาและความเฉลียวฉลาดของเด็กที่เพิ่มขึ้นตามวัย
			</div>
			<div class="detail-box">
				ไม่ว่าจะเป็น ความสามารถในการจดจำ การมีสมาธิ หรือโฟกัสกับเรื่องใดเรื่องหนึ่ง การเข้าใจความหมายของสีหน้าและท่าทางของผู้อื่น การควบคุมตนเอง ความสามารถในการนึกถึงและคาดเดาผลลัพธ์ที่เกิดจากการกระทำ การวางแผนล่วงหน้า การตัดสินใจในสถานการณ์ฉุกเฉิน การวิเคราะห์ปัญหาที่ยุ่งยากและหาทางแก้ไข หรือแม้แต่ความสามารถในการมองเห็นและการเคลื่อนไหวของมือ หรือกล้ามเนื้อมัดเล็ก ฯลฯ พัฒนาการเหล่านี้ล้วนเป็นสิ่งที่ต้องอาศัยการพัฒนาของสมองอย่างต่อเนื่องตั้งแต่วัยเด็กไปจนถึงวัยผู้ใหญ่
			</div>
			<img class="detail-image hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/img01.png') }}" data-expand="+10"/>
			<img class="detail-image hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/img02.png') }}" data-expand="+10"/>
			<div class="detail-box space-bottom">
				และสิ่งที่คุณพ่อคุณแม่อยากรู้ตามมา ก็คือ แล้วสมองของลูกจะพร้อมสำหรับการพัฒนาตั้งแต่เมื่อไหร่ เราจะได้เตรียมตัวกันได้ทัน
			</div>
			<div class="detail-box FXBold color-blue title">
				แค่เริ่มปฏิสนธิ สมองก็สร้างแล้ว
			</div>
			<img class="detail-image half-left visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/img01-mobile.png') }}" data-expand="+10"/>
			<img class="detail-image half-right visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/img02-mobile.png') }}" data-expand="+10"/>
			<div class="clearfix visible-xs"></div>
			<div class="detail-box space-bottom">
				ใช่แล้วครับ สมองของคนเราเริ่มสร้างและพัฒนาตั้งแต่ไม่กี่สัปดาห์ภายหลังการปฏิสนธิระหว่างไข่และสเปิร์มเลยทีเดียว (เร็วมาก!) โดยโครงสร้างพื้นฐานของระบบประสาทจะมีลักษณะเป็นท่อยาวๆ เรียกว่า หลอดประสาท (Neural tube) ต่อมาส่วนหน้าสุดของหลอดประสาทจะขยายตัวจนมีใหญ่ขึ้น พัฒนาไปเป็นสมอง ในขณะที่หลอดประสาทส่วนที่เหลือจะพัฒนาต่อโดยไม่ได้เปลี่ยนแปลงโครงสร้างมากนัก กลายเป็นไขสันหลังต่อไป
			</div>
			<div class="detail-box FXBold color-blue title">
				สเต็มเซลล์ (Stem Cells) <br class="hidden-xs"/>จุดเริ่มต้นที่สำคัญของการสร้างสมอง
			</div>
			<div class="detail-box space-bottom">
				กระบวนการสร้างสมอง ที่เกิดจากการขยายตัวของหลอดประสาทส่วนหน้านั้น จะเกิดพร้อมๆ ไปกับการสร้างเซลล์ใหม่ขึ้นเป็นจำนวนมาก ซึ่งเซลล์เหล่านี้จะกลายเป็นเซลล์ตั้งต้น หรือที่เราคุ้นหูกันในชื่อว่า สเต็มเซลล์ ของระบบประสาท ซึ่งสเต็มเซลล์เหล่านี้ จะพัฒนาไปเป็นเซลล์ประสาทชนิดต่างๆ และเป็นเซลล์สนับสนุนเซลล์ประสาทอีกหลายชนิด ตามระยะการพัฒนาของสมองต่อไป
			</div>
			<div class="detail-box FXBold color-blue title">
				สมองที่ดีเกิดจากการทำงานร่วมกันของ<br class="hidden-xs"/>เซลล์ประสาทที่มีประสิทธิภาพ
			</div>
			<div class="detail-box">
				ดูเหมือนขั้นตอนการสร้างสมองจะจบแล้ว แต่เดี๋ยวก่อน! มันยังไม่จบแค่นั้น เพราะแม้จะสร้างเซลล์สำเร็จแล้ว แต่สมองก็ยังไม่อาจเป็นสมองได้อยู่ดี ถ้าเซลล์ประสาทเป็นร้อยเป็นล้านเซลล์นั้นไม่ได้ประสาน เชื่อมต่อ และรับส่งสัญญาณหากันอย่างมีประสิทธิภาพ ซึ่งตัวชี้วัดประสิทธิภาพนั้นก็คือ
			</div>
			<div class="detail-box FXBold color-blue title">
				1. วงจรประสาทเชื่อมโยงกันได้ดี<br/>
				2. การส่งสัญญาณประสาทเกิดขึ้นอย่างรวดเร็ว
			</div>
			<div class="detail-box">
				มาถึงตรงนี้ คุณพ่อคุณแม่ก็คงพอจะเห็นภาพแล้วว่า ที่จริงแล้ว สมองของลูกนั้นเริ่มพัฒนาได้ตั้งแต่ปฏิสนธิ เติบโตอยู่ในครรภ์ ซึ่งเป็นช่วงเวลาที่สมองจะพัฒนาเพื่อเตรียมตัวให้พร้อมสำหรับการมีประสาทสัมผัสทั้งหก คือ การมองเห็น การได้ยิน การได้กลิ่น การรับรสชาติ การรับความรู้สึกสัมผัส และการรับรู้ตำแหน่งของร่างกายหรือการทรงตัว อันเป็นทักษะที่จำเป็นต่อการมีชีวิตอยู่หลังจากที่ทารกคลอดออกมา เพื่อรอรับการพัฒนาอย่างต่อเนื่องต่อไปในช่วงวัยต่างๆ
			</div>
			<div class="detail-box">
				และคงจะดีกว่า ถ้าลูกของเราจะได้รับการพัฒนาสมองอย่างเต็มที่ตั้งแต่ก่อนคลอด เพื่อเป็นพื้นฐานให้สมองพร้อมรับการพัฒนาอย่างต่อเนื่องผ่านขวบวัยที่เติบโตขึ้นมาอย่างมีประสิทธิภาพ
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="text-desc desc2">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/txt-desc.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/02/txt-desc-mobile.png') }}" data-expand="+10"/>
		</div>
	</div>
@endsection