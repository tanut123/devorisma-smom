@extends($page_current == "brain_fuction" ? 'sphingomyelin.brain_fuction' : 'sphingomyelin.first_1000_days.1000_days')

<?php 
	$urlPath = "";
	if($page_current == "brain_fuction"){
		$urlPath = "การทำงานของสมอง";
	}else{
		$urlPath = "ความสำคัญของ1000วันแรก/1000วัน";
	}
?>

@section('template_detail_sphingomyelin')
	<div class="sphingomyelin-detail-template">
		@if ($page_current !=  "brain_fuction")
			@include('sphingomyelin/detail/template_menu')
		@endif
		<div class="sphingomyelin-detail-panel">
			@yield('content_detail_sphingomyelin')
		</div>
	</div>
@endsection