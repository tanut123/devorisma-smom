@extends('sphingomyelin.detail.template')

@section('content_detail_sphingomyelin')
	<div class="sphingomyelin-detail-content">
		<img class="hidden-xs title-image lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/txt-title.png') }}" data-expand="+10"/>
		<img class="visible-xs title-image lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/txt-title-mobile.png') }}" data-expand="+10"/>
		<div class="detail-doctor-name FXBold">
			ข้อมูลบทความโดย ผศ.นพ.วรสิทธิ์ ศิริพรพาณิชย์
		</div>
		<div class="detail-content">
			<img class="detail-banner hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/banner.jpg') }}" data-expand="+10"/>
			<img class="detail-banner visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/banner-mobile.jpg') }}" data-expand="+10"/>
			<div class="detail-box FXBold color-blue">
				เวลาที่เราได้เจอคนเก่ง หรือเด็กฉลาดที่ทำข้อสอบได้คะแนนเต็ม ยกมือตอบคำถามด้วยท่าทีฉลาดว่องไว บางทีก็อด<br class="hidden-xs"/>ที่จะอยากรู้ไม่ได้ว่า สมองของคนเหล่านั้นจะรูปร่างหน้าตาเป็นอย่างไร เหมือนหรือต่างจากสมองของคนอย่างเราแค่ไหน
			</div>
			<div class="detail-box">
				คิดแล้วก็อยากจะลองขอดูสมองของเขาสักหน่อย แต่เอ๊ะ มันก็จะดูน่ากลัวเกินไปหรือเปล่า
			</div>
			<div class="detail-box">
				แต่! สิ่งที่เราคิดนั้นไม่ใช่เรื่องแปลกเกินไปหรอกนะ เพราะนักวิทยาศาสตร์เองก็เคยสงสัยแบบเรานี่ล่ะ (แสดงว่าเราก็มีความ<br class="hidden-xs"/>เป็นนักวิทยาศาสตร์ในตัวเหมือนกันนะเนี่ย) จึงได้มีความพยายามทำให้การสำรวจวิจัยสมองเพื่อตอบสิ่งที่สงสัยมานานว่า <br class="hidden-xs"/>สมองของคนฉลาดหรืออัจฉริยะนั้นจะเหมือนหรือแตกต่างจากสมองของคนทั่วไปอย่างไร
			</div>
			<div class="detail-box">
				พอตั้งคำถามอย่างนี้แล้ว คงจะไม่มีสมองของใครที่น่าจะสำรวจและตอบคำถามนี้ได้ดีไปกว่า สมองของอัลเบิร์ต ไอน์สไตน์ <br class="hidden-xs"/>(Albert Einstein) อัจฉริยะระดับโลกที่ทุกคนให้การยอมรับ
			</div>
			<div class="detail-box">
				30 ปีหลังการเสียชีวิตของไอน์สไตน์ นักวิทยาศาสตร์ได้นำเอาสมองที่เก็บรักษาไว้อย่างดีมาผ่าสำรวจ (เอิ่ม น่ากลัวไปมั้ย) <br class="hidden-xs"/>เพื่อค้นหาความลับในสมองก้อนนี้ ว่าความอัจฉริยะของไอน์สไตน์นั้นเกิดขึ้นได้อย่างไร และสมองของไอน์สไตน์นั้นมีความพิเศษ<br class="hidden-xs"/>อย่างไร ก่อนจะพบข้อเท็จจริงที่ว่า...
			</div>
			<img class="detail-image hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/img01.png') }}" data-expand="+10"/>
			<img class="detail-image last hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/img02.png') }}" data-expand="+10"/>
			<div class="detail-box">
				สมองของไอน์สไตน์ก็ไม่ได้พิเศษหรือแตกต่างไปจากสมองของคนทั่วไปนักหรอก (อ้าว…)
			</div>
			<div class="detail-box">
				เรามักจะชอบคิดว่า คนที่เก่งหรือฉลาดนั้นจะ ‘หัวโต’ เพราะว่ามีขนาดสมองใหญ่กว่าคนทั่วไป แต่ขนาดสมองของไอน์สไตน์ก็ได้บอกเราแล้วว่า ความคิดนั้นไม่ใช่เรื่องจริง ขนาดโดยรวมของทั้งก้อนสมองไม่ได้เป็นเครื่องกำหนดหรือเกี่ยวข้องกับความฉลาดของคนเราสักเท่าไหร่
			</div>
			<img class="detail-image half-left visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/img01-mobile.png') }}" data-expand="+10"/>
			<img class="detail-image half-right visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/img02-mobile.png') }}" data-expand="+10"/>
			<div class="clearfix visible-xs"></div>
			<div class="detail-box">
				แต่ถึงอย่างนั้น เมื่อดูเข้าไปถึงรายละเอียด นักวิทยาศาสตร์พบว่า สมองส่วน พาเรียทัล (Parietal lobe) ที่อยู่ทางตอนล่างซีกซ้ายมีขนาดใหญ่กว่าคนทั่วไปโดย<span class="FXBold color-blue">สมองส่วนพาเรียทัลนั้นมีบทบาทสำคัญด้านการคำนวณ วิเคราะห์ และจัดการกับตัวเลข</span> ซึ่งก็สอดคล้องกับอัจฉริยภาพด้านการคำนวณของไอน์สไตน์ที่ทุกคนต่างก็รู้กันดี
			</div>
			<div class="detail-box">
				เท่านั้นยังไม่พอ นักวิทยาศาสตร์ ยังพบว่า <span class="FXBold color-blue">สมองส่วนฮิปโปแคมปัส (hippocampus) ที่เกี่ยวข้องกับการเรียนรู้และความจำ</span> ที่สมองซีกซ้ายของไอน์สไตน์ยังมีเซลล์ประสาทที่ขนาดใหญ่กว่าเซลล์ประสาทชนิดเดียวกันที่อยู่ในสมองซีกขวา ซึ่งขนาดของเซลล์ประสาทภายในสมองส่วนนี้ ในคนทั่วไปจะไม่ค่อยพบความแตกต่างมากนัก
			</div>
			<div class="detail-box">
				พูดให้เห็นภาพง่ายๆ ก็เหมือนกับการที่คนเราออกกำลังกาย ถ้ามีการออกกำลังกายและใช้งานกล้ามเนื้อส่วนไหนเยอะๆ กล้ามเนื้อส่วนนั้นก็จะแข็งแรง มีขนาดและรูปร่างสวยงาม สมองของคนก็เช่นเดียวกัน เมื่อสมองได้รับการใช้งานอย่างสม่ำเสมอ เซลล์ประสาทในสมองก็จะส่งสัญญาณหากันตลอดเวลา เหมือนการออกกำลังของเซลล์ประสาท จนทำให้สมองส่วนนั้นมีขนาดใหญ่และมีเซลล์ประสาทที่ซับซ้อนยิ่งขึ้นตามลำดับ
			</div>
			<div class="detail-box FXBold color-blue">
				แม้จะยังไม่สามารถสรุปฟันธงลงไปได้ แต่เราก็พอจะมองเห็นภาพว่า สมองของอัจฉริยะนั้นไม่ได้มีโครงสร้างโดยรวมที่ได้เปรียบหรือแตกต่างจากคนทั่วไปเลย เราได้สมองขนาดเท่ากันมาตั้งแต่เกิด แต่วงจรประสาทภายในสมองของคนเก่งที่หนากว่าคนทั่วไป ก็อาจเป็นไปได้ว่ามันเกิดจากการฝึกฝนและใช้งานอย่างสม่ำเสมอ
			</div>
			<div class="detail-box">
				และถ้าเป็นอย่างนั้นจริง ก็แปลว่า เราสามารถพัฒนาสมองธรรมดาๆ ให้กลายเป็นอัจฉริยะได้น่ะสิ!
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="text-desc desc1">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/txt-desc.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail/01/txt-desc-mobile.png') }}" data-expand="+10"/>
		</div>
	</div>
@endsection