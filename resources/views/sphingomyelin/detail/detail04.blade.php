@extends('sphingomyelin.detail.template')

@section('content_detail_sphingomyelin')
	<div class="sphingomyelin-detail-content">
		<img class="hidden-xs title-image lazyload" data-expand="+10" data-src="{{ asset('/images/sphingomyelin/detail/04/txt-title.png') }}" />
		<img class="visible-xs title-image lazyload" data-expand="+10" data-src="{{ asset('/images/sphingomyelin/detail/04/txt-title-mobile.png') }}" />
		<div class="detail-doctor-name FXBold">
			ข้อมูลบทความโดย ผศ.นพ.วรสิทธิ์ ศิริพรพาณิชย์
		</div>
		<div class="detail-content">
			<img class="detail-banner hidden-xs lazyload" data-expand="+10" data-src="{{ asset('/images/sphingomyelin/detail/04/banner.jpg')}}" />
			<img class="detail-banner visible-xs lazyload" data-expand="+10" data-src="{{ asset('/images/sphingomyelin/detail/04/banner-mobile.jpg')}}" />
			<div class="detail-box FXBold color-blue">
				หลังจากทารกคลอดและลืมตาดูโลกแล้ว นอกจากพัฒนาการทางร่างกายที่สังเกตเห็นได้ด้วยตาเปล่า คุณพ่อคุณแม่อาจจะสงสัยว่า แล้วพัฒนาการทางสมองของลูกจะเป็นไปอย่างไร และคุณพ่อคุณแม่จะทำอย่างไรให้สมองของลูกทำงานอย่างเต็มประสิทธิภาพที่สุด
			</div>
			<div class="detail-box">
				ก่อนอื่น ต้องเข้าใจว่า ที่เราเคยเห็นภาพสมองของคนเราเป็นเหมือนก้อนยางลบทรงกลมรีที่มีรอยหยักมากมายเต็มไปหมดนั้น ภายในสมองประกอบด้วยเซลล์ประสาทจำนวนมาก ซึ่งเจ้าเซลล์ประสาทหน้าตาเหมือนเส้นใยที่ยาวและแตกแขนงออกตรงปลายนั่นเอง ที่จะคอยเชื่อมต่อถึงกัน จนเกิดเป็นวงจร เรียกว่า วงจรประสาท และวงจรประสาทในสมองของคนเรานี่เองที่ทำหน้าที่เชื่อมต่อและส่งข้อมูลระหว่างสมองแต่ละส่วน
			</div>
			<div class="detail-box">
				 ในสมองของเด็กเล็กหรือเด็กในช่วงวัยที่กำลังมีการพัฒนาการนั้น หากเปรียบเทียบให้เห็นภาพง่ายๆ ก็เหมือนการค่อยๆ สร้างถนนภายในเมืองใหญ่  ถนนแต่ละเส้นก็คือแขนงของเซลล์ประสาทที่ทำหน้าที่เชื่อมต่อกันเป็นเส้นทางสำหรับรับส่งข้อมูลจากจุดหนึ่งไปอีกจุดหนึ่งภายในสมองของเรา
			</div>
			<div class="detail-box">
				ดังนั้นการสร้างวงจรประสาทที่ดี ก็จะทำให้สมองของคนเราสามารถรับส่งข้อมูลถึงกันได้อย่างมีประสิทธิภาพ ซึ่งการที่สมองสามารถรับส่งข้อมูลอย่างมีประสิทธิภาพนี่เอง เป็นปัจจัยหลักที่มีผลต่อการสร้างพัฒนาการและการเรียนรู้ที่ดีสำหรับลูกน้อยตามมา
			</div>
			<div class="detail-box">
				 แต่การสร้างถนนหรือการพัฒนาวงจรประสาทให้มีประสิทธิภาพนั้น ไม่ได้เกิดขึ้นหลังจากที่ทารกคลอดออกมาแล้วเท่านั้น เพราะสมองของลูกจะเริ่มมีการพัฒนาทีละเล็กละน้อยตั้งแต่ช่วงที่ทารกเจริญเติบโตอยู่ในครรภ์ของคุณแม่ และพัฒนาการทางสมองของลูกที่เกิดขึ้นในครรภ์นั้นจะเกี่ยวข้องกับพัฒนาการด้านการมองเห็น การรับสัมผัส และการได้ยิน ซึ่งเป็นพัฒนาการพื้นฐานสำหรับการอยู่รอดของทารกแรกเกิดโดยทั่วไปนั่นเอง
			</div>
			<img class="detail-image last hidden-xs lazyload" data-expand="+10" data-src="{{ asset('/images/sphingomyelin/detail/04/img01.png') }}" />
			<div class="detail-box">
				ความแตกต่างของพัฒนาการทางสมองในเด็กแต่ละคน จึงเริ่มเห็นได้ชัดหลังคลอด หรือในช่วงอายุตั้งแต่ขวบปีแรกไปจนสามขวบ เพราะเป็นช่วงอายุที่สมองของเด็กจะพัฒนาส่วนที่เกี่ยวข้องกับการสื่อสารและภาษา รวมไปถึงการทำงานของกล้ามเนื้อมัดเล็ก หมายถึงการใช้มือและหยิบจับสิ่งของต่างๆ ได้
			</div>
			<img class="detail-image visible-xs lazyload" data-expand="+10" data-src="{{ asset('/images/sphingomyelin/detail/04/img01-mobile.png') }}" />
			<div class="clearfix visible-xs"></div>
			<div class="detail-box FXBold color-blue title">
				คุณพ่อคุณแม่อาจจะสังเกตพัฒนาการในแต่ละช่วงวัยของลูกน้อยได้ ดังนี้
			</div>
			<div class="detail-box title">
				<div class="detail-inline-box">
					<div class="detail-inline title-detail FXBold color-blue">3 เดือนแรก:</div><div class="detail-inline">ลูกจะเริ่มใช้กล้ามเนื้อเพื่อชันคอขึ้นได้เอง</div>
				</div>
				<div class="detail-inline-box">
					<div class="detail-inline title-detail FXBold color-blue">อายุ 6 เดือน:</div><div class="detail-inline">ลูกสามารถใช้มือคว้าสิ่งของได้</div>
				</div>
				<div class="detail-inline-box">
					<div class="detail-inline title-detail FXBold color-blue">อายุ 9 เดือน:</div><div class="detail-inline">เริ่มส่งเสียงและทักทายได้</div>
				</div>
				<div class="detail-inline-box">
					<div class="detail-inline title-detail FXBold color-blue">อายุ 12 เดือน:</div><div class="detail-inline">ยืน เตรียมพร้อมที่จะเดิน และโบกมือทักทายได้</div>
				</div>
				<div class="detail-inline-box">
					<div class="detail-inline title-detail FXBold color-blue">อายุ 18 เดือน:</div><div class="detail-inline">เดินได้ พูดเป็นคำได้ ใช้ช้อนส้อมตักอาหารเองได้</div>
				</div>
				<div class="detail-inline-box">
					<div class="detail-inline title-detail FXBold color-blue">อายุ 24 เดือน:</div><div class="detail-inline">ขึ้นลงบันได พูดได้เป็นคำติดต่อกันได้ตั้งแต่สองคำขึ้นไป</div>
				</div>
				<div class="detail-inline-box">
					<div class="detail-inline title-detail FXBold color-blue">อายุ 30 เดือน:</div><div class="detail-inline">เคลื่อนไหวได้อย่างมีประสิทธิภาพและแสดงอารมณ์ชัดเจน</div>
				</div>
				<div class="detail-inline-box">
					<div class="detail-inline title-detail FXBold color-blue">อายุ 36 เดือน:</div><div class="detail-inline last">เข้าใจในเหตุผลมากขึ้น เริ่มใช้เหตุผลในการตัดสินใจ</div>
				</div>
			</div>
			<div class="detail-box">
				ดังนั้น เด็กเล็กในช่วงอายุ 1-3 ขวบ จึงต้องการกระตุ้นพัฒนาการทางสมองเพื่อให้เกิดพัฒนาการด้านการสื่อสารและการรับสัมผัสอย่างดีที่สุด
			</div>
			<div class="detail-box">
				คุณพ่อคุณแม่จึงสามารถกระตุ้นการพัฒนาสมองของเด็กในวัยดังกล่าว ได้ด้วยการส่งเสริมทักษะแต่ละด้านของลูกให้ถูกต้อง เช่น เสริมสร้างพัฒนาการทางด้านการสื่อสาร ด้วยการพูดคุยกับลูกบ่อยๆ เรียกชื่อลูก ชื่อสิ่งของ ร้องเพลง หรือเล่านิทานให้ลูกฟัง ส่วนการกระตุ้นการทำงานของกล้ามเนื้อมัดเล็ก ก็สามารถทำได้โดยการให้เด็กต่อของเล่น หรือฝึกการใช้ช้อนส้อม เป็นต้น
			</div>
			<div class="detail-box">
				จะเห็นว่า สมองของเด็กทารกตั้งแต่แรกเกิดจนถึงสามขวบนั้น ต้องการการกระตุ้นให้เกิดการพัฒนาที่ดี เพราะเป็นช่วงเวลาที่สมองของเด็กกำลังสร้างวงจรประสาทที่เชื่อมต่อกันอย่างมีประสิทธิภาพนั่นเอง
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="text-desc desc4">
			<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset('/images/sphingomyelin/detail/04/txt-desc.png') }}" />
			<img class="visible-xs lazyload" data-expand="+10" data-src="{{ asset('/images/sphingomyelin/detail/04/txt-desc-mobile.png') }}" />
		</div>
	</div>
@endsection