@extends('sphingomyelin.template')

@section('content_sphingomyelin')
	<div class="sphingomyelin-content home">
		<div class="content-text">
			<div class="text-margin">
				<span class="text-line">คุณแม่รู้มั้ยว่า...</span><img class="text-line" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/01_txt.png') }}" /><span class="text-line">เป็นช่วงที่สมองสร้างไวที่สุด ซึ่งเป็นช่วงเวลาสำคัญ</span><br class="hidden-xs"/>
				<span class="text-line">ในการสร้างเซลล์ประสาท (Neuron) และการเชื่อมต่อของสมอง (Synap & Myelin)<sup>1</sup></span>
			</div>
			<div class="text-margin">
				<img class="text-line" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/02_txt.png') }}" /><span class="text-line">สร้างมากตั้งแต่ในท้อง ในช่วง 3 เดือนก่อนคลอด และต่อเนื่องในช่วงวัยเด็ก...</span><br class="hidden-xs"/>
				<span class="text-line">ดังนั้นการสร้างไมอีลินจึงสำคัญมากในช่วงวัยเด็ก<sup>2</sup></span>
			</div>
			<div>
				<img class="text-line" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/03_txt.png') }}" /><span class="text-line">หนึ่งในสารอาหารสำคัญในการสร้างไมอีลิน<sup>3</sup></span>
			</div>
		</div>
		<div class="liner"></div>
		<div class="content-1000days">
			<img class="hidden-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/04_txt.png') }}" />
			<img class="visible-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/04_txt_mobile.png') }}" />
			<div class="text-box">
				<div class="text">
					1,000 วันแรกของการเลี้ยงลูกอย่างใส่ใจและทุ่มเทของคุณแม่
				</div>
				<div class="subtext">
					ตั้งแต่ช่วงตั้งครรภ์ (270 วัน) จนถึงลูกอายุ 2 ขวบปี (730 วันต่อมา)
				</div>
				<div class="text">
					จะเป็นตัวแปรสำคัญตัวหนึ่งที่จะบอกถึง<br class="hidden-xs"/>
					ขีดความสามารถ ศักยภาพ และความสำเร็จของลูกในอนาคต
				</div>
			</div>
		</div>
		<div class="align-center">
			<img class="title-center hidden-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/05_txt.png') }}" />
			<img class="title-center visible-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/05_txt_mobile.png') }}" />
		</div>
		<div class="liner"></div>
		<img class="title-content-middle hidden-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/06_txt.png') }}" />
		<img class="title-content-middle visible-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/06_txt_mobile.png') }}" />
		<img class="content-middle hidden-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/07_box.png') }}" />
		<img class="content-middle visible-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/07_box_mobile.png') }}" />
		<div class="liner"></div>
		<img class="title-content-bottom hidden-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/08_txt.png') }}" />
		<img class="title-content-bottom visible-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/08_txt_mobile.png') }}" />
		<div class="content-bottom">
			<div class="content-box">
				<div class="content-type FXBold">
					<div class="content-type-inner">
						สฟิงโก<br/>ไมอีลิน
					</div>
				</div><div class="content-desc">
					<div class="content-desc-inner">
						เป็นไขมันฟอสโฟไลปิด พบมากในน้ำนมแม่<sup>6</sup>
						ทั้งยังเป็นสารอาหารที่สำคัญในการสร้าง<br class="hidden-xs"/>
						ไมอีลิน<sup>7</sup> พบได้ในไข่ ครีม ชีส นม รวมถึง<br class="hidden-xs"/>
						ผลิตภัณฑ์นม<sup>8</sup>
					</div>
				</div>
			</div><div class="content-box">
				<div class="content-type FXBold">
					<div class="content-type-inner">
						แอลฟา<br/>แล็คตัลบูมิน
					</div>
				</div><div class="content-desc">
					<div class="content-desc-inner">
						(Alpha-lactalbumin) เป็นเวย์โปรตีน<br class="hidden-xs"/>
						คุณภาพสูง พบมากในนมแม่ มีปริมาณ<br class="hidden-xs"/>
						มากถึง 28% ของโปรตีนทั้งหมด มี<br class="hidden-xs"/>
						บทบาทสำคัญในการช่วยพัฒนาสมอง<br class="hidden-xs"/>
						และร่างกายของเด็ก<sup>9</sup>
					</div>
				</div>
			</div><div class="content-box">
				<div class="content-type FXBold">
					<div class="content-type-inner">
						ไขมัน
					</div>
				</div><div class="content-desc">
					<div class="content-desc-inner">
						รู้มั้ยว่า สมองประกอบไปด้วยไขมัน<br class="hidden-xs"/>
						ถึง 60% (ในกรณีที่ตัดน้ำออก) <br class="hidden-xs"/>
						ซึ่งไขมันเป็นส่วนประกอบหลักของ<br class="hidden-xs"/>
						ไมอีลินในสมอง<sup>10</sup>
					</div>
				</div>
			</div><div class="content-box">
				<div class="content-type FXBold">
					<div class="content-type-inner">
						ดีเอชเอ <br/>และโคลีน
					</div>
				</div><div class="content-desc">
					<div class="content-desc-inner">
						ทำงานร่วมกันเพื่อช่วยพัฒนา<br class="hidden-xs"/>
						เรื่องการเรียนรู้และความจำ<br class="hidden-xs"/>
						ของทารกให้ดีขึ้นอีกด้วย
					</div>
				</div>
			</div><div class="content-box">
				<div class="content-type FXBold">
					<div class="content-type-inner">
						วิตามิน<br/>บี 12
					</div>
				</div><div class="content-desc">
					<div class="content-desc-inner">
						มีส่วนช่วยในการทำงานของ<br class="hidden-xs"/>ระบบประสาทและสมอง
					</div>
				</div>
			</div><div class="content-box">
				<div class="content-type FXBold">
					<div class="content-type-inner">
						ธาตุเหล็ก
					</div>
				</div><div class="content-desc">
					<div class="content-desc-inner">
						เป็นส่วนประกอบสำคัญของ<br class="hidden-xs"/>
						ฮีโมโกลบิลในเม็ดเลือดแดง
					</div>
				</div>
			</div><div class="content-box">
				<div class="content-type FXBold">
					<div class="content-type-inner">
						สังกะสี
					</div>
				</div><div class="content-desc">
					<div class="content-desc-inner">
						ช่วยในการเจริญเติบโตของร่างกาย
					</div>
				</div>
			</div><div class="content-box">
				<div class="content-type FXBold">
					<div class="content-type-inner">
						ไอโอดีน
					</div>
				</div><div class="content-desc">
					<div class="content-desc-inner">
						เป็นส่วนประกอบหลักที่สำคัญของฮอร์โมน<br class="hidden-xs"/>
						ไทรอยด์ ซึ่งมีหน้าที่ควบคุมการ<br class="hidden-xs"/>
						เจริญเติบโต และการพัฒนาของ<br class="hidden-xs"/>
						ร่างกายและสมอง
					</div>
				</div>
			</div>

			<div class="content-bottom-text">
				คุณแม่ควรให้ลูกได้รับสารอาหารหลากหลาย ครบทั้ง 5 หมู่ ในปริมาณที่เพียงพอเพื่อพัฒนาการที่ดีทั้งด้านร่างกายและสมองของลูกน้อย
			</div>
		</div>
		<img class="hidden-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/09_txt.png') }}" />
		<img class="text-bottom visible-xs" src="{{ asset($BASE_CDN . '/images/sphingomyelin/main-page/09_txt_mobile.png') }}" />
	</div>
@endsection