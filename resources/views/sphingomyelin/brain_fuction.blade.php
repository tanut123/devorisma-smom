@extends('sphingomyelin.template')

@section('content_sphingomyelin')
	<div class="sphingomyelin-content page-1">
		<div class="liner"></div>
		@if ($detail_menu == "")
		<div class="main-title-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/header-menu.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/header-menu-mobile.png') }}" data-expand="+10"/>
			<div class="main-title-text FXMed">
				กับการทำงานของสมอง
			</div>
		</div>
		@endif
		<div class="sphingomyelin-detail-template">
			@include('sphingomyelin/detail/template_menu')
		</div>
		@if ($detail_menu == "")
		<div class="title-middle-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page01/01_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page01/01_txt_mobile.png') }}" data-expand="+10"/>
			<div class="title-middle-text hidden-xs">
				จะมีการส่งสัญญาณประสาท เร็วกว่าที่ไม่มีไมอีลินถึง 60 เท่า<sup>1</sup>
			</div>
			<div class="title-middle-text visible-xs">
				จะมีการ<span class="highlight FXMed">ส่งสัญญาณประสาท</span>
			</div>
			<img class="mobile visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page01/03_txt.png') }}" data-expand="+10"/>
		</div>
		<div class="title-bottom-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/header-menu.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/header-menu-mobile.png') }}" data-expand="+10"/>
			<div class="title-bottom-text hidden-xs">
				เป็นหนึ่งในสารอาหารสำคัญในการสร้างไมอีลิน<sup>2</sup>
			</div>
			<div class="title-bottom-text visible-xs">
				เป็นหนึ่งใน<span class="highlight FXMed">สารอาหารสำคัญ</span>ในการ
			</div>
			<img class="mobile visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page01/04_txt.png') }}" data-expand="+10"/>
		</div>
		<div class="desc-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page01/02_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page01/02_txt_mobile.png') }}" data-expand="+10"/>
		</div>
		<div class="liner"></div>
		@endif
		@yield('template_detail_sphingomyelin')
	</div>
@endsection