@extends('sphingomyelin.first_1000_days.template')

@section('content_1000_days')
	<div class="sphingomyelin-subcontent page-2">
		<div class="main-desc-box">
			<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/01_txt.png') }}" />
			<img class="visible-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/01_txt_mobile.png') }}" />
			<div class="main-desc-text">
				เป็นช่วงที่สมองของลูกพัฒนาอย่างรวดเร็วและต่อเนื่อง ลูกควรจะได้รับนมแม่ให้นานที่สุด <br class="hidden-xs"/>เพราะนมแม่มีสารอาหารหลัก คือไขมัน <br class="visible-401-432"/>โปรตีนและแลคโตส<sup>1</sup> ทั้งยังอุดมด้วย<br class="visible-401-432"/>สารอาหารสำคัญๆ <br class="hidden-xs"/>ที่ช่วยให้ร่างกายและสมองของลูกน้อยเจริญเติบโตได้ดี เช่น
			</div>
		</div>
		<div class="main-bottom-box">
			<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/02_txt.png') }}" />
			<img class="visible-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/02_txt_mobile.png') }}" />
			<div class="main-bottom-text">
				กับการพัฒนากายและสมอง
			</div>
		</div>
		<div class="liner"></div>
		<div class="content-mike-box content1">
			<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/03_txt.png') }}" />
			<img class="visible-xs lazyload " data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/03_txt_mobile.png') }}" />
			<div class="content-mike-text">
				ไขมันชนิดฟอสโฟไลปิด พบในน้ำนมแม่<sup>2</sup>
			</div>
		</div>
		<div class="content-mike-box content2">
			<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/04_txt.png') }}" />
			<img class="visible-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/04_txt_mobile.png') }}" />
			<div class="content-mike-text">
				เป็นเวย์โปรตีนคุณภาพสูง พบมากในนมแม่ มีปริมาณมากถึง 28% ของโปรตีนทั้งหมด <br class="hidden-xs"/>มีบทบาทสำคัญในการช่วยพัฒนาสมองและร่างกายของเด็ก<sup>3</sup>
			</div>
		</div>
		<div class="content-mike-box content3">
			<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/05_txt.png') }}" />
			<img class="visible-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/05_txt_mobile.png') }}" />
			<div class="content-mike-text">
				ทำงานร่วมกันเพื่อช่วยพัฒนาเรื่องการเรียนรู้และความจำ<br class="hidden-xs"/>ของทารกให้ดีขึ้นอีกด้วย
			</div>
		</div>
		<div class="content-mike-box content4">
			<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/06_txt.png') }}" />
			<img class="visible-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/06_txt_mobile.png') }}" />
			<div class="content-mike-text">
				เพื่อสร้างกระดูกและอวัยวะต่างๆ ของร่ายกายโดยเฉพาะสมองของลูกให้เจริญเติบโตอย่างมีคุณภาพ
			</div>
		</div>
		<div class="liner"></div>
		<div class="img-bottom-box">
			<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/07_txt.png') }}" />
			<img class="visible-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/07_txt_mobile.png') }}" />
		</div>
	</div>
@endsection