@extends('sphingomyelin.template')

@section('content_sphingomyelin')
	<div class="sphingomyelin-content page-3">
		<div class="liner"></div>
		<div class="subtitle-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/header-menu.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/header-menu-mobile.png') }}" data-expand="+10"/>
			<div class="subtitle-text">
				ความสำคัญของ 1,000 วันแรกกับการพัฒนาสมอง
			</div>
		</div>
		<div class="submenu-bar">
			<a href="{{ url() }}/สฟิงโกไมอีลิน/ความสำคัญของ1000วันแรก/270วัน" class="submenu-list submenu1 <?=($view_subpage=='270')?'active':''?>">
			</a><a href="{{ url() }}/สฟิงโกไมอีลิน/ความสำคัญของ1000วันแรก/635วัน" class="submenu-list submenu2 <?=($view_subpage=='635')?'active':''?>">
			</a><a href="{{ url() }}/สฟิงโกไมอีลิน/ความสำคัญของ1000วันแรก/1000วัน" class="submenu-list submenu3 <?=($view_subpage=='1000')?'active':''?>"></a>
		</div>
		<div class="sphingomyelin-subcontent-panel">
			@yield('content_1000_days')
		</div>
	</div>
@endsection