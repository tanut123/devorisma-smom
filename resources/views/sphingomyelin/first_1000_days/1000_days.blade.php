@extends('sphingomyelin.first_1000_days.template')

@section('content_1000_days')
	<div class="sphingomyelin-subcontent page-3">
		<div class="subcontent-title-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage03/01_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage03/01_txt_mobile.png') }}" data-expand="+10"/>
			<div class="subcontent-title-text">
				เป็นอีกหนึ่งช่วงที่สมองของลูกกำลังพัฒนาแบบก้าวกระโดดอย่างต่อเนื่อง จึงเป็นช่วงเวลาสำคัญสำหรับพัฒนาการหลายๆ ด้านของลูก ไม่ว่าจะเป็นด้านร่างกาย สมอง และความเป็นตัวตน <br class="hidden-xs"/>คุณแม่ควรต้องให้ความสำคัญกับเรื่องสารอาหารและกระตุ้นพัฒนาการที่ช่วยเสริมสร้างสมอง <br class="hidden-xs"/>ด้วยการทำกิจกรรมที่หลากหลาย รวมถึงการให้ความรัก และการเอาใจใส่ดูแลลูกอย่างเต็มที่
			</div>
		</div>
		<div class="liner"></div>
		@yield('template_detail_sphingomyelin')
	</div>
@endsection