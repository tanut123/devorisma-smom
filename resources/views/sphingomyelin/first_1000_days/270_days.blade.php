@extends('sphingomyelin.first_1000_days.template')

@section('content_1000_days')
	<div class="sphingomyelin-subcontent page-1">
		<div class="subcontent-title-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage01/01_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage01/01_txt_mobile.png') }}" data-expand="+10"/>
			<div class="subcontent-title-text">
				270 วันแรก ในระหว่างตั้งครรภ์ เป็นช่วงเวลาของการสร้างร่างกายและสมองลูกคุณแม่ควรได้รับสารอาหารหลักทั้ง 5 หมู่ รวมทั้งสารอาหารต่างๆ อย่างพอเพียง
			</div>
		</div>
		<div class="subcontent-subtitle-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/02_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/02_txt_mobile.png') }}" data-expand="+10"/>
			<div class="subcontent-subtitle-text">
				กับการพัฒนากายและสมอง
			</div>
		</div>
		<div class="liner"></div>
		<div class="subcontent-box content1">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage01/03_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage01/03_txt_mobile.png') }}" data-expand="+10"/>
			<div class="subcontent-text">
				ไขมันชนิดฟอสโฟไลปิด พบในน้ำนมแม่<sup>1</sup>
			</div>
		</div>
		<div class="subcontent-box content2">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage01/04_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage01/04_txt_mobile.png') }}" data-expand="+10"/>
			<div class="subcontent-text">
				ควรรับประทานให้เพียงพอต่อความต้องการในแต่ละวัน เพื่อสร้างกระดูกและอวัยวะต่างๆ <br class="hidden-xs"/>ของร่ายกายโดยเฉพาะสมองของลูกให้เจริญเติบโตอย่างมีคุณภาพ
			</div>
			<div class="subcontent-text2">
				นอกจากนี้ กิจกรรมต่างๆ รวมถึงอารมณ์ และความรู้สึกของคุณแม่ก็สำคัญไม่แพ้กัน<br class="hidden-xs"/>ที่จะช่วยหล่อหลอมให้ลูกรักของคุณกลายเป็นเด็กฉลาด แข็งแรง และมีพัฒนาการที่ดี
			</div>
		</div>
		<div class="subcontent-bottom-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage01/05_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/subpage01/05_txt_mobile.png') }}" data-expand="+10"/>
		</div>
	</div>
@endsection