@extends('sphingomyelin.first_1000_days.template')

@section('content_1000_days')
	<div class="sphingomyelin-subcontent main">
		<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/detail3.png') }}" data-expand="+10"/>
		<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page03/detail3-mobile.png') }}" data-expand="+10"/>
	</div>
@endsection