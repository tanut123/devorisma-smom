@extends('sphingomyelin.template')

@section('content_sphingomyelin')
	<div class="sphingomyelin-content home">
			<div style="position: absolute;top:0;left:-100%;height: 1px;width: 1px;text-indent: -9999px;">
				<h1>สฟิงโกไมอีลิน</h1>
				<h2>สฟิงโกไมอีลิน หนึ่งในสารอาหารสำคัญต่อสมองลูกน้อยที่คุณแม่ต้องรู้</h2>
				<p>
					“สฟิงโกไมอีลิน” สารอาหารที่พบมากในนมแม่ นมและผลิตภัณฑ์นม ช่วยในการสร้างปลอกไมอีลิน ซึ่งมีผลต่อการทำงานของสมอง

				</p>

				<p>การเชื่อมโยงการทำงานของเซลล์ประสาทในสมองแต่ละส่วน (brain connection) ซึ่งคือกลไกที่สำคัญที่สุดในการทำงานของสมอง ซึ่งต้องเกิดขึ้นอย่างรวดเร็วการทำงานของ สมองจึงจะมีประสิทธิภาพ หรือเปรียบเสมือนกับการใช้อินเทอร์เน็ต 5G
</p>
<p>การเพิ่มความเร็วในการส่งสัญญาณของสมองต้องอาศัยการสร้างปลอกไมอีลิน เริ่มสร้างตั้งแต่อยู่ในครรภ์และสร้างอย่างต่อเนื่องอย่างรวดเร็วภายใน 2 ปีแรกหลังคลอด และถึงวัยผู้ใหญ่ตอนต้น </p>

				
				<p>คุณพ่อคุณแม่รู้มั้ยว่า...
ความสามารถของลูกรักของคุณนั้นไม่ว่าจะเป็นการรับรู้ การควบคุมการเคลื่อนไหว หรือการคิดวิเคราะห์และ

</p>
			<p>การวางแผนเมื่อเติบโตขึ้นไปนั้นล้วนเกิดขึ้นจากการทำงานของสมอง แต่สมองเพียงส่วนใดส่วนหนึ่งไม่สามารถที่จะสร้างสิ่งมหัศจรรย์เหล่านี้ได้โดยลำพัง แต่ต้องอาศัย</p>
			<p>การเชื่อมโยงการทำงานของเซลล์ประสาทในสมองแต่ละส่วน (brain connection) ซึ่งคือกลไกที่สำคัญที่สุดในการทำงานของสมอง ซึ่งต้องเกิดขึ้นอย่างรวดเร็วการทำงานของสมองจึงจะมีประสิทธิภาพ หรือเปรียบเสมือนกับการใช้อินเทอร์เน็ต 5G</p>

			<p>เพราะสามารถส่งสัญญาณได้เร็วกว่า จึงมีประสิทธิภาพมากกว่านั่นเอง และนี่คือความลับของสมองที่สามารถทำสิ่งที่เหลือเชื่อ ไม่ว่าจะเป็นการคลาน เดิน การพูด การวิ่งเล่น หรือหัวเราะ ร้องไห้ของลูกรักนั้น เป็นเพราะการส่งสัญญาณหากันด้วยความรวดเร็วของสมองแต่ละส่วนนั่นเอง</p>

			<p>โดยวัตถุดิบในการสร้างไมอีลินจะเป็นสารอาหารกลุ่มไขมัน และไขมันที่มีความจำเพาะต่อไมอีลินโดยเฉพาะนั่นคือ “สฟิงโกไมอีลิน (sphingomyelin)” นั่นเอง</p>


			<h3>“สฟิงโกไมอีลิน” หนึ่งในสารอาหารสำคัญ ในการสร้างไมอีลิน</h3>
			<p>
การได้รับสฟิงโกไมอีลินในปริมาณที่เหมาะสมจึงเป็นวัตถุดิบสำคัญต่อการสร้างไมอีลินในสมอง ซึ่งช่วยให้การทำงานของสมองเกิดขึ้นได้อย่างรวดเร็ว โดยสฟิงโกไมอีลินจะพบได้มากในนมแม่ และผลิตภัณฑ์นม เช่น ครีม ชีส ดังนั้นทารกจึงควรได้รับนมแม่อย่างเต็มที่ เพราะนมแม่เป็นแหล่งของไขมันชั้นดี ซึ่งรวมถึงสฟิงโกไมอีลินด้วย 
</p>

			<p>นอกจากนี้ยังมีรายงานการวิจัยที่แสดงให้เห็นว่าทารกที่ได้รับนมแม่เพียงอย่างเดียวเป็นระยะเวลาอย่างน้อย 3 เดือน จะมีปริมาณไมอีลินในสมองเพิ่มขึ้นและมีความสัมพันธ์กับผลการประเมินการเรียนรู้ที่ดีขึ้นอีกด้วย</p>

			<p>โดยสรุปการพัฒนาของสมองเป็นกระบวนการที่เกิดขึ้นอย่างรวดเร็วตั้งแต่ช่วงที่อยู่ในครรภ์มารดาไปจนถึง 2 ปีแรก การส่งเสริมการพัฒนาของสมองสามารถกระทำได้โดยการให้สารอาหารอย่างครบถ้วน และให้เด็กได้มีโอกาสเรียนรู้สิ่งใหม่ๆ </p>
			<p>นอกจากนี้การทำงานอย่างมีประสิทธิภาพของสมองยังต้องอาศัยการเชื่อมต่อสัญญาณประสาทที่มีความรวดเร็ว ซึ่งการสร้างจะเป็นกลไกสำคัญในการเพิ่มความเร็วในการส่งสัญญาณประสาท เปรียบเสมือนการสร้างทางด่วนให้กับสมอง โดยสารอาหารในกลุ่มไขมันโดยเฉพาะ “สฟิงโกไมอีลิน” จะเป็นวัตถุดิบสำคัญในการสร้างไมอีลินในสมอง</p>

			<p>ดังนั้นจะเห็นได้ว่านอกเหนือจากพันธุกรรมและการเลี้ยงดูแล้วจึงอยากให้คุณพ่อคุณแม่ตระหนักถึงโภชนาการสำหรับลูกรักเพื่อพัฒนาการทั้งทางร่ายกาย และสมองให้เหมาะสมสำหรับแต่ละช่วงวัย</p>

			<p>นพ.วรสิทธิ์ ศิริพรพาณิชย์</p>


				

			</div>
		<div class="hidden-xs">
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-d_01.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-d_02.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-d_03.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-d_04.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-d_05.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-d_06.png') }}" data-expand="+10" />
			</div>
		</div>
		<div class="visible-xs">
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-m_01.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-m_02.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-m_03.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-m_04.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-m_05.png') }}" data-expand="+10" />
			</div>
			<div>
				<img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/detail2/detail-m_06.png') }}" data-expand="+10" />
			</div>
		</div>
            
	</div>
@endsection