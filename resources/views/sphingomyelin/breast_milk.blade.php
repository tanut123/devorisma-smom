@extends('sphingomyelin.template')

@section('content_sphingomyelin')
	<div class="sphingomyelin-content page-2">
		<div class="liner"></div>
		<div class="main-title-box">
			<div class="main-title-text FXMed">
				นมแม่และ
			</div>
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/header-menu.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/header-menu-mobile.png') }}" data-expand="+10"/>
		</div>
		<div class="main-desc-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/01_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/01_txt_mobile.png') }}" data-expand="+10"/>
			<div class="main-desc-text">
				เป็นช่วงที่สมองของลูกพัฒนาอย่างรวดเร็วและต่อเนื่อง ลูกควรจะได้รับนมแม่ให้นานที่สุด เพราะนมแม่มีสารอาหารหลัก คือไขมัน โปรตีนและแลคโตส<sup>1</sup> ทั้งยังอุดมด้วยสารอาหารสำคัญๆ ที่ช่วยให้ร่างกายและสมองของลูกน้อยเจริญเติบโตได้ดี เช่น
			</div>
		</div>
		<div class="main-bottom-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/02_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/02_txt_mobile.png') }}" data-expand="+10"/>
			<div class="main-bottom-text">
				กับการพัฒนากายและสมอง
			</div>
		</div>
		<div class="liner"></div>
		<div class="content-mike-box content1">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/03_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/03_txt_mobile.png') }}" data-expand="+10"/>
			<div class="content-mike-text">
				ไขมันชนิดฟอสโฟไลปิด พบในน้ำนมแม่<sup>2</sup>
			</div>
		</div>
		<div class="content-mike-box content2">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/04_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/04_txt_mobile.png') }}" data-expand="+10"/>
			<div class="content-mike-text">
				เป็นเวย์โปรตีนคุณภาพสูง พบมากในนมแม่ มีปริมาณมากถึง 28% ของโปรตีนทั้งหมด <br class="hidden-xs"/>มีบทบาทสำคัญในการช่วยพัฒนาสมองและร่างกายของเด็ก<sup>3</sup>
			</div>
		</div>
		<div class="content-mike-box content3">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/05_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/05_txt_mobile.png') }}" data-expand="+10"/>
			<div class="content-mike-text">
				ทำงานร่วมกันเพื่อช่วยพัฒนาเรื่องการเรียนรู้และความจำ<br class="hidden-xs"/>ของทารกให้ดีขึ้นอีกด้วย
			</div>
		</div>
		<div class="content-mike-box content4">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/06_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/06_txt_mobile.png') }}" data-expand="+10"/>
			<div class="content-mike-text">
				เพื่อสร้างกระดูกและอวัยวะต่างๆ ของร่ายกาย<br class="hidden-xs"/>โดยเฉพาะสมองของลูกให้เจริญเติบโตอย่างมีคุณภาพ
			</div>
		</div>
		<div class="liner"></div>
		<div class="img-bottom-box">
			<img class="hidden-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/07_txt.png') }}" data-expand="+10"/>
			<img class="visible-xs lazyload" data-src="{{ asset($BASE_CDN . '/images/sphingomyelin/page02/07_txt_mobile.png') }}" data-expand="+10"/>
		</div>
	</div>
@endsection