@if(count($game) > 0)
<ul id="game-list-panel">
	<?php $count = 0; ?>
	@foreach ($game as $data)
	<li class="game-list {{ ($count == 1) ? 'game-list-center-left': '' }} {{ ($count == 2) ? 'game-list-center-right': '' }}">
		<a class="game-list-link" target="_blank" href="@readFileName($data->file_pdf_gen,$data->file_pdf,'o0x0','school_book_game')">
			<img class="game-list-img" src="@readFileName($data->image_gen,$data->image,'c210x210','school_book_game')">
			<span class="game-list-title FXregular">{{ $data->title }}</span>
		</a>
	</li>
	<?php $count++; ?>
	<?php ($count == 4) ? $count = 0: '' ?>
	@endforeach
</ul>
<div class="clearfix"></div>
@endif