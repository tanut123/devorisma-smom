@extends('template.master')

@compressCss("slick,slick-theme,media.school_book_template")

@section('content')
<div class="wapContent">
	@include('media.school_book.template_header')
	@yield('content_school_book')
</div>
<script type="text/javascript">
	var DATA_NOT_FOUND = '{{ trans('core.DATA_NOT_FOUND') }}';
</script>
@endsection
@compressJs("slick.min,media.school_book",9)