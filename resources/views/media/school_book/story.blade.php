@if(count($story) > 0)
<ul id="story-list-panel">
	<?php $count = 0; ?>
	@foreach ($story as $key => $data)
	<li class="story-list {{ ($count == 1) ? 'story-list-center': '' }}">
		<span class="story-list-link" target="_blank" href="">
			<img class="story-list-img" src="@readFileName($data->image_gen,$data->image,'c231x231','school_book_story')"/>
			<span class="story-list-detail">
				<span class="story-list-title FXregular">{{ $data->title }}</span>
				<a target="_blank" href="@readFileName($data->file_pdf_gen,$data->file_pdf,'o0x0','school_book_story')" class="story-link-download FXregular"><?=trans('school_book.SCHOOL_BOOK_DOWNLOAD_FREE')?></a>
			</span>
		</span>
	</li>
	<?php $count++; ?>
	<?php ($count == 2) ? $count = 0: '' ?>
	@endforeach
</ul>
<div class="clearfix"></div>
@endif