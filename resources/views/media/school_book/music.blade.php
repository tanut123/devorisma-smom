<div id='media_music'>
    <div class='align_panel'>
        <div class='control_panel'>
            <canvas id="soundControlPanel" width="216" height="216"></canvas>
            <div id='volumControl'>
                <div class='current'>

                </div>
                <div class='minus_time'>

                </div>
                <canvas id="volumControlPanel" height='30' width="216"></canvas>
            </div>
        </div>
        <div class='music_list_panel'>
            <audio id='media_music_player' preload="auto" src='<?php echo $first_song ?>'></audio>
            <?php echo $music ?>
        </div>
    </div>
</div>