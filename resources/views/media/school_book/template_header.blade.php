<div id="school-book-header">
	<div id="school-book-header-panel">
		<div id="school-book-header-content-panel">
			<h1 class="text-left FThin"><span class="FXregular">สื่อ</span>เพื่อลูกน้อย</h1>
			<h2 class="text-right FThin">
				<span class="text-top">คุณแม่สามารถใช้สื่อ<br class="visible-xs"/>หลากหลายเหล่านี้</span>
				<span class="text-under">เพื่อเสริมสร้างพัฒนาการทางสมอง<br class="visible-xs"/>และกระตุ้นการรับรู้ต่างๆ ของลูกน้อยได้</span>
			</h2>
			<div class="clearfix"></div>
			<div id="list-menu-top">
				<a id="list-menu-item1" data-name="game" class="list-menu-item">
					{{--<img class="img-list-normal" src="{{ asset($BASE_CDN . '/images/media/school_book/item-game.png') }}" alt="item-game">
					<img class="img-list-active" src="{{ asset($BASE_CDN . '/images/media/school_book/item-game-active.png') }}" alt="item-game">--}}
					<div class='item_game'></div>
					<span class="text-title FXregular">เกมพัฒนาสมอง</span>
					<span class="active-arrow">
						<img src="{{ asset($BASE_CDN . '/images/media/school_book/game-active.png') }}" alt="game">
					</span>
				</a>
				<a id="list-menu-item2" data-name="diy" class="list-menu-item">
					{{--<img class="img-list-normal" src="{{ asset($BASE_CDN . '/images/media/school_book/item-diy.png') }}" alt="item-diy">
					<img class="img-list-active" src="{{ asset($BASE_CDN . '/images/media/school_book/item-diy-active.png') }}" alt="item-diy">--}}
					<div class='item_diy'></div>
					<span class="text-title FXregular">D.I.Y.</span>
					<span class="active-arrow">
						<img src="{{ asset($BASE_CDN . '/images/media/school_book/diy-active.png') }}" alt="diy">
					</span>
				</a>
				<a id="list-menu-item3" data-name="story" class="list-menu-item">
					{{--<img class="img-list-normal" src="{{ asset($BASE_CDN . '/images/media/school_book/item-story.png') }}" alt="item-story">
					<img class="img-list-active" src="{{ asset($BASE_CDN . '/images/media/school_book/item-story-active.png') }}" alt="item-story">--}}
					<div class='item_story'></div>
					<span class="text-title FXregular">นิทานก่อนนอน</span>
					<span class="active-arrow">
						<img src="{{ asset($BASE_CDN . '/images/media/school_book/story-active.png') }}" alt="story">
					</span>
				</a>
				{{-- <a id="list-menu-item4" data-name="music" class="list-menu-item">
					<img class="img-list-normal" src="{{ asset($BASE_CDN . '/images/media/school_book/item-music.png') }}" alt="item-music">
					<img class="img-list-active" src="{{ asset($BASE_CDN . '/images/media/school_book/item-music-active.png') }}" alt="item-game">
					<span class="text-title FXregular">เพลงนี้เพื่อหนู</span>
					<span class="active-arrow">
						<img src="{{ asset($BASE_CDN . '/images/media/school_book/music-active.png') }}" alt="music">
					</span>
				</a> --}}
			</div>
		</div>
	</div>
</div>