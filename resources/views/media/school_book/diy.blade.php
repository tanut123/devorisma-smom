<?php
    $svg_prefix = "";
    $browser_data = $_SERVER['HTTP_USER_AGENT'];
    if (!preg_match("/MSIE 9/",$browser_data)){
        $svg_prefix = $BASE_LANG."school_book";
    }
?>
@if(count($diy) > 0)
<ul id="diy-list-panel">
	<?php $count = 0; ?>
	@foreach ($diy as $key => $data)
	<li class="diy-list {{ ($count == 1) ? 'diy-list-center': '' }}">
		<span class="diy-list-link" target="_blank" href="">
			<svg class="diy-list-img" version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 width="280.18px" height="300.176px" viewBox="0 0 280.18 300.176" enable-background="new 0 0 280.18 300.176"
				 xml:space="preserve">
				 <defs>
			        <pattern id="img_{{$key}}" patternUnits="userSpaceOnUse" width="280" height="300">
			            <image xlink:href="@readFileName($data->image_gen,$data->image,'c280x300','school_book_diy')" x="0" y="0" width="280" height="300" />
			        </pattern>
			    </defs>
			<path fill-rule="evenodd" fill="url({{ $svg_prefix }}#img_{{$key}})" clip-rule="evenodd" d="M140.079,262.07c-15.663,0-31.327,0.022-46.99-0.008
				c-11.613-0.021-20.643-4.974-26.502-14.945c-15.915-27.086-31.679-54.261-47.378-81.473c-5.801-10.055-5.681-20.361,0.116-30.392
				c15.635-27.057,31.318-54.087,47.134-81.038c5.957-10.15,15.077-15.151,26.898-15.147c31.16,0.009,62.321,0.008,93.481,0
				c11.824-0.003,20.936,5.016,26.891,15.164c15.814,26.952,31.496,53.981,47.129,81.04c5.796,10.031,5.9,20.336,0.1,30.389
				c-15.701,27.211-31.462,54.388-47.379,81.472c-5.859,9.971-14.896,14.913-26.509,14.932
				C171.406,262.09,155.743,262.07,140.079,262.07z"/>
			</svg>
			<span class="diy-list-title FXregular">{{ $data->title }}</span>
			<a target="_blank" href="@readFileName($data->file_pdf_gen,$data->file_pdf,'o0x0','school_book_diy')" class="diy-link-download FXregular"><?=trans('school_book.SCHOOL_BOOK_DOWNLOAD_FREE')?></a>
		</span>
	</li>
	<?php $count++; ?>
	<?php ($count == 3) ? $count = 0: '' ?>
	@endforeach
</ul>
<div class="clearfix"></div>
@endif