@extends('media.school_book.template')

@compressCss("media.school_book_game,media.school_book_diy,media.school_book_story,media.school_book_music",10)

@section('content_school_book')
<div class="container-wrapper">
	<span class="active-arrow">
		<img class="visible-xs" src="{{ asset( $BASE_CDN . '/images/media/school_book/game-active.png') }}" alt="game">
	</span>
	<div class="container-wrapper-detail">
		<div class="container-wrapper-top"></div>
		<div class="container-detail">
		</div>
		<div class="container-wrapper-bottom"></div>
	</div>
</div>
@endsection

@compressCss('media.school_book_music,perfect-scrollbar.min')
@compressJs('media.school_book.music,perfect-scrollbar.jquery.min')
