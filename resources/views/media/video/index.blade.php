@extends('template.master')

@compressCss("slick,slick-theme,video")

@section('content')
<div class="wapContent">
	<?php echo $video;?>
</div>
<div id='video_light_box'>
    <div class='video_align'>
        <div class='video_panel'>

        </div>
        <div class='close_video'>

        </div>
    </div>
</div>

@endsection
@compressJs("slick.min,video")
