<div id='team_header'>
    <div class='team_header_bg lazyload' data-bgset="{{ asset($BASE_CDN . '/images/team/team_header/bg.jpg') }}" data-expand="+10">
        <div class='team_header_align'>
             <div class='team_all'>
                 <img data-src='{{ asset($BASE_CDN . '/images/team/team_header/team_pic.png') }}' class='team_pic lazyload' data-expand="+10">
             </div>
        </div>
    </div>
    <div class='page_detail_bg lazyload' data-bgset="{{ asset($BASE_CDN . '/images/team/team_header/team_header_detail_bg.jpg') }}" data-expand="+10">
        <div class='page_detail_align'>
             <div class='page_detail'>
                 <img data-src='{{ asset($BASE_CDN . '/images/team/team_header/detail_header.png') }}' class='lazyload' data-expand="+10">
				 <?=(App::getLocale()=="en")?
				     "<div class='header_text'>
	                    <span class='thin'>We are gratefully to be a part of your child’s development</span>
	                  </div>
	                  <div class='detail'>
	                  	<p>With all the questions from you, we are really appreciate your trust and will always stand right  beside you, take care of you. <br>Our expert team including nutritionist, health education specialist and professional nurse are available and open for any questions <br>about pregnancy to toddler stage to make sure that you are well took care by professional.</p>
	                  	<p>Our care call service is served by specialist to ensure your child’s proper nutrition and development, both mentally and physically. Additionally, Healthcare magazine and developmental book will be sent to you for the best of every aspect of your child’s development as we considered you our family.</p>
	                  	<p>Please kindly contact us and we’re looking forward to hearing from you</p>
	                  </div>"
				 :
	                 "<div class='header_text'>
	                     <span class='bold'>ขอเป็นส่วนหนึ่ง เพื่อสร้างศักยภาพที่แข็งแกร่งให้กับลูกน้อย</span>
	                  </div>
	                  <div class='detail'>
	                     <p>หลากหลายเรื่องราวร้อยพันคำถามที่เกี่ยวกับลูกน้อยของคุณ ผู้เชี่ยวชาญ S-MomClub ยินดีเป็นที่ปรึกษาให้กับคุณพ่อคุณแม่<br>
	                     ด้วยผู้เชี่ยวชาญในด้านต่างๆ ทั้งนักโภชนาการ นักสุขศึกษา รวมทั้งทีมพยาบาล ที่พร้อมจะให้การดูแลแก่ครอบครัว S-MomClub<br>
	                     ในทุกๆ ช่วง ตั้งแต่คุณแม่เริ่มตั้งครรภ์ ไปจนถึงลูกน้อยวัยเรียนรู้ เพื่อให้คุณมั่นใจได้ว่า ทุกช่วงเวลาของลูกคุณจะมีผู้เชี่ยวชาญ S-MomClub<br>
	                     คอยอยู่เคียงข้างเพื่อให้คำปรึกษา และส่งเสริมศักยภาพของลูกน้อยได้อย่างเต็มที่</p>
	                     <p>นอกจากนี้เรายังมีทีมงานผู้เชี่ยวชาญให้บริการทางโทรศัพท์เยี่ยมเยียนสุขภาพบุตรเพื่อให้คำแนะนำด้านโภชนาการ และพัฒนาการต่างๆ<br>
	                     ให้แก่ครอบครัว S-MomClub พร้อมจัดส่งนิตยสาร และคู่มือเสริมสร้างพัฒนาการลูกน้อยสำหรับสมาชิก S-MomClub อีกด้วยนะคะ</p>
	                     <p>วันนี้คุณพ่อคุณแม่ สามารถเลือกช่องทางการติดต่อกับ S-MomClub<br>
	                     ได้หลายช่องทางเรายินดีเป็นที่ปรึกษาของคุณพ่อคุณแม่ในทุกเรื่องราวของลูกน้อยเสมอ</p>
	                  </div>"
                 ?>
             </div>
        </div>
    </div>
</div>