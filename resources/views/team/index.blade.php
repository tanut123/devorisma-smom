@extends('template.master')

@compressCss("slick,slick-theme,team_header,team_person")

@section('style')
@endsection

@section('content')
	<div class="wapContent">
	    @include('team.header')
	    @include('team.person_list')
	</div>
	<script>
		window.lazySizesConfig = window.lazySizesConfig || {};
		window.lazySizesConfig.customMedia = {
	    	'--mb': '(max-width: 767px)'
		};
	</script>
@endsection

@compressJs("lazy.bgset.min,lazysizes.min,slick.min,team.team_person")

@section('scripts')

@endsection