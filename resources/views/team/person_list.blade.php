<?php
    $svg_prefix = "";
    $browser_data = $_SERVER['HTTP_USER_AGENT'];
    if (!preg_match("/MSIE 9/",$browser_data)){
        $svg_prefix = $BASE_LANG."team";
    }

?>

<div id='team_person'>
    <div class='team_personr_bg lazyload' data-bgset="{{ asset($BASE_CDN . '/images/team/team_person/team_person_bg.png') }}" data-expand="+10">
        <div class='team_person_align'>
            <div class='team_person_header'>
            <?=(App::getLocale()=="en")?
            	"<span class='thin'>We, expert team, are always available for you.</span>"
            :
            	"<span class='bold'>ทีมงานและผู้เชี่ยวชาญของเราพร้อมเสมอที่จะให้คำแนะนำคุณแม่</span>"
            ?>
            </div>
            <div class='person_panel'>
                @foreach($person_data as $key => $data)
                    <div class='person_list'>
                        <div class='person_detail' person_id='{{$data['team_person_id']}}'>
                            <div class='personal_picture_scale'>
                                <div class='person_picture'>
                                    <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         width="249px" height="228px" viewBox="0 0 249 228" enable-background="new 0 0 249 228" xml:space="preserve">
                                         @if ($key == "0") {
                                            <linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="160.582" y1="-4.0166" x2="88.4195" y2="232.0165">
                                                <stop  offset="0" style="stop-color:#BA8C26"/>
                                                <stop  offset="0.2447" style="stop-color:#FFE79F"/>
                                                <stop  offset="0.5378" style="stop-color:#D8AF56"/>
                                                <stop  offset="0.8095" style="stop-color:#FFE79F"/>
                                                <stop  offset="1" style="stop-color:#DEA832"/>
                                            </linearGradient>
                                         @endif
                                         <defs>
                                            <pattern id="img{{$key}}" patternUnits="userSpaceOnUse" width="245" height="225">
                                                <image xlink:href="{{$data['image_url']}}" scr="{{$data['image_url']}}" x="0" y="0" width="249" height="228" />
                                            </pattern>
                                        </defs>
                                        {{--<path fill-rule="evenodd" clip-rule="evenodd" fill="url({{ $BASE_LANG }}team#img{{$key}})" stroke="url({{$BASE_LANG}}team#SVGID_1_)" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M242.735,96.047L199.006,19.46C192.658,8.346,180.933,1.5,168.239,1.5H80.762C68.078,1.5,56.347,8.346,50,19.46
                                        L6.264,96.047c-6.352,11.109-6.352,24.8,0,35.906L50,208.546c6.347,11.111,18.078,17.954,30.762,17.954h87.478
                                        c12.693,0,24.419-6.843,30.767-17.954l43.729-76.593C249.088,120.847,249.088,107.156,242.735,96.047z"/>--}}
                                        <g>
                                            <path fill-rule="evenodd" clip-rule="evenodd" fill="url({{ $svg_prefix }}#img{{$key}})" d="M80.762,224.5c-11.933,0-23.055-6.493-29.025-16.946L8,130.961C2.018,120.5,2.018,107.502,8,97.04l43.737-76.588
                                                C57.708,9.996,68.83,3.5,80.762,3.5h87.478c11.935,0,23.058,6.496,29.03,16.952l43.729,76.587c5.983,10.464,5.983,23.461,0,33.921
                                                l-43.73,76.595c-5.971,10.452-17.095,16.945-29.029,16.945H80.762z"/>
                                            <path fill="#FFFFFF" d="M168.239,5.5c11.219,0,21.677,6.109,27.293,15.943l43.73,76.589c5.633,9.851,5.633,22.087-0.001,31.938
                                                l-43.729,76.592c-5.616,9.831-16.074,15.938-27.293,15.938H80.762c-11.217,0-21.673-6.107-27.288-15.938L9.736,129.968
                                                c-5.631-9.849-5.631-22.085,0.001-31.938l43.736-76.587C59.089,11.609,69.545,5.5,80.762,5.5H168.239 M168.239,1.5H80.762
                                                C68.078,1.5,56.347,8.346,50,19.46L6.264,96.047c-6.352,11.109-6.352,24.8,0,35.906L50,208.546
                                                c6.347,11.111,18.078,17.954,30.762,17.954h87.478c12.693,0,24.419-6.843,30.767-17.954l43.729-76.593
                                                c6.353-11.106,6.353-24.797,0-35.906L199.006,19.46C192.658,8.346,180.933,1.5,168.239,1.5L168.239,1.5z"/>

                                                <path fill="none" stroke="url({{$svg_prefix}}#SVGID_1_)" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                                                M242.735,96.047L199.006,19.46C192.658,8.346,180.933,1.5,168.239,1.5H80.762C68.078,1.5,56.347,8.346,50,19.46L6.264,96.047
                                                c-6.352,11.109-6.352,24.8,0,35.906L50,208.546c6.347,11.111,18.078,17.954,30.762,17.954h87.478
                                                c12.693,0,24.419-6.843,30.767-17.954l43.729-76.593C249.088,120.847,249.088,107.156,242.735,96.047z"/>
                                        </g>
                                    </svg>
                                </div>
                            </div>
                            <div class='person_name'>
                                <?php echo $data['name'];?>
                            </div>
                            <div class='person_position'>
                                <?php echo$data['position'];?>
                            </div>
                            <div class='hover_icon'>

                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div id='team_person_detail'>
    <div class='cover_panel'>
        <div class='cover_panel2'>
            <div class='detail_panel'>
                <div class='person_pic_cover'>
                    <div class='person_pic'>
                        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                             width="249px" height="228px" viewBox="0 0 249 228" enable-background="new 0 0 249 228" xml:space="preserve">

                             <defs>
                                <pattern id="img_p" patternUnits="userSpaceOnUse" width="245" height="225">
                                    <image id='popup_img' xlink:href="{{ asset($BASE_CDN . '/images/team/team_person/team_blank.png') }}" x="0" y="0" width="249" height="228" />
                                </pattern>
                            </defs>
                            {{--<path fill-rule="evenodd" clip-rule="evenodd" fill="url({{ $BASE_LANG }}team#img{{$key}})" stroke="url({{$BASE_LANG}}team#SVGID_1_)" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M242.735,96.047L199.006,19.46C192.658,8.346,180.933,1.5,168.239,1.5H80.762C68.078,1.5,56.347,8.346,50,19.46
                            L6.264,96.047c-6.352,11.109-6.352,24.8,0,35.906L50,208.546c6.347,11.111,18.078,17.954,30.762,17.954h87.478
                            c12.693,0,24.419-6.843,30.767-17.954l43.729-76.593C249.088,120.847,249.088,107.156,242.735,96.047z"/>--}}
                            <g>
                                <path fill-rule="evenodd" clip-rule="evenodd" fill="url({{ $svg_prefix }}#img_p)" d="M80.762,224.5c-11.933,0-23.055-6.493-29.025-16.946L8,130.961C2.018,120.5,2.018,107.502,8,97.04l43.737-76.588
                                    C57.708,9.996,68.83,3.5,80.762,3.5h87.478c11.935,0,23.058,6.496,29.03,16.952l43.729,76.587c5.983,10.464,5.983,23.461,0,33.921
                                    l-43.73,76.595c-5.971,10.452-17.095,16.945-29.029,16.945H80.762z"/>
                                <path fill="#FFFFFF" d="M168.239,5.5c11.219,0,21.677,6.109,27.293,15.943l43.73,76.589c5.633,9.851,5.633,22.087-0.001,31.938
                                    l-43.729,76.592c-5.616,9.831-16.074,15.938-27.293,15.938H80.762c-11.217,0-21.673-6.107-27.288-15.938L9.736,129.968
                                    c-5.631-9.849-5.631-22.085,0.001-31.938l43.736-76.587C59.089,11.609,69.545,5.5,80.762,5.5H168.239 M168.239,1.5H80.762
                                    C68.078,1.5,56.347,8.346,50,19.46L6.264,96.047c-6.352,11.109-6.352,24.8,0,35.906L50,208.546
                                    c6.347,11.111,18.078,17.954,30.762,17.954h87.478c12.693,0,24.419-6.843,30.767-17.954l43.729-76.593
                                    c6.353-11.106,6.353-24.797,0-35.906L199.006,19.46C192.658,8.346,180.933,1.5,168.239,1.5L168.239,1.5z"/>

                                    <path fill="none" stroke="url({{$svg_prefix}}#SVGID_1_)" stroke-width="3" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="
                                    M242.735,96.047L199.006,19.46C192.658,8.346,180.933,1.5,168.239,1.5H80.762C68.078,1.5,56.347,8.346,50,19.46L6.264,96.047
                                    c-6.352,11.109-6.352,24.8,0,35.906L50,208.546c6.347,11.111,18.078,17.954,30.762,17.954h87.478
                                    c12.693,0,24.419-6.843,30.767-17.954l43.729-76.593C249.088,120.847,249.088,107.156,242.735,96.047z"/>
                            </g>
                       </svg>
                    </div>
                </div>
                <div class='detail_panel2'>
                    <div class='name'>

                    </div>
                    <div class='position'>

                    </div>
                    <div class='personal_detail_header'>
						 <?=(App::getLocale()=="en")?'Impression':'ความรู้สึกประทับใจ' ?>
                    </div>
                    <div class='personal_detail'>

                    </div>
                </div>
                <div class='close_popup_btn'>

                </div>
            </div>
        </div>
    </div>
</div>