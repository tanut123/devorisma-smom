@extends('template.master')

{{--@if (isset($custom_page[0]->css_file_string))
@compressCss($custom_page[0]->css_file_string)
@endif--}}
<?php
if (isset($custom_page[0]->css_file)) {
    $custom_css = $custom_page[0]->css_file;
    $custom_css_gen = $custom_page[0]->css_file_gen;
}
?>
@compressCss("custom_page")

@section('content')
<div id="detail-panel" class="wapContent">
	<div id="detail-box">
		<div id="detail-content">
			@replaceStockUrl($custom_page[0]->detail)
		</div>
	</div>
</div>
@endsection

@compressJs("custom_page")