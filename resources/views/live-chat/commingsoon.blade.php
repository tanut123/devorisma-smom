@extends('template.master')

@compressCss(",live-chat")

@section('style')
@endsection

@section('content')
	<div class="wapContent">
		<div class="inner-panel">
			<div class="content-top">
				<h1 class="text-title color-gold"><span class="FXregular">COMING</span> <span class="FThin">SOON</span></h1>
				<span class="text-description FLighter color-gold">
					ขอขอบคุณที่ทุกท่านให้ความสนใจบริการ คุยสด กับผู้เชี่ยวชาญ ซึ่งจะเริ่มเปิดให้บริการเร็วๆ นี้ อดใจรออีกนิดนะคะ<br class="hidden-xs"/> โดยในระหว่างนี้ท่านสามารถขอคำปรึกษาได้จากทีมงานผู้เชี่ยวชาญของ
					<img class="img-logo" src="{{ asset($BASE_CDN . '/images/live-chat/logo-smom-club.png') }}" alt="logo-smomclub">
				</span>
			</div>
			<div class="content-bottom">
				<img src="{{ asset($BASE_CDN . '/images/live-chat/team.png') }}" class="img-responsive img-team" alt="team">
			</div>
		</div>
	</div>
@endsection

@section('scripts')

@endsection