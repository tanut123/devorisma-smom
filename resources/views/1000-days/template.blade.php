@extends('template.master')

@compressCss("growup.growup_template")

@section('content')
<div class="wapContent">
	@include('1000-days.template_header')
	@yield('content_growup')
</div>
@endsection

@compressJs("growup.growup_template")