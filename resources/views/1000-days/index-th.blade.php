<div class="container-detail">
	<div class="container-detail-table">
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
			<div class="section">
				<span class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular"><img class="text-1000-days" src="{{ $BASE_CDN }}/images/1000-days/1000-days-text.png" alt="1000-days-text"></span> ของชีวิตลูก</span>
				<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/growup-brain-knowleadge.png" alt="first-270-days-menu">
				<span class="text-description col-lg-10 col-md-10 col-sm-10 col-xs-12 col-no-padding FLighter">1,000 วันแรกของการเลี้ยงดูอย่างใส่ใจและทุ่มเท ของคุณแม่ ตั้งแต่ช่วง ตั้งครรภ์ (270 วัน) จนถึงลูก อายุ 2 ขวบปี (730 วันต่อมา) จะเป็นตัวกำหนด สำคัญตัวหนึ่งที่จะบอกถึงขีดความสามารถ
			ศักยภาพ และความสำเร็จของลูกในอนาคต
				</span>
			</div>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
			<div class="section">
				<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/growup-brain-knowleadge.png" alt="first-270-days-menu">
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="container-detail-table">
		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
			<div class="section">
				<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/1000-days-growup.png" alt="1000-days-growup" width='' height=''>
			</div>
		</div>
		<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
			<div class="section">
				<span class="right">
					<span class="text-title col-xs-12 col-no-padding FThin"><span class="FThin"><img class="text-1000-days img-display" src="{{ $BASE_CDN }}/images/1000-days/1000-days-text.png" alt="1000-days-text">
					<span class="text-display">กับการ<br/>พัฒนาการสมอง</span>
					<span class="clearfix"></span>
					</span></span>

					<span class="text-description col-lg-10 col-md-10 col-sm-10 col-xs-12 col-no-padding FLighter">1,000 วันแรกของชีวิต สมองของลูกพัฒนาอย่างรวดเร็วที่สุด โดยเริ่มตั้งแต่ในครรภ์ มีการเพิ่มเซลล์สมองควบคู่กับการสร้าง เส้นใยประสาทอย่างรวดเร็ว ทารกแรกคลอดเมื่อครบ 1,000 วัน สมองจะมีขนาดประมาณ 80% ของสมองผู้ใหญ่ จำนวนเซลล์ สมองมากถึงแสนล้านเซลล์รวมทั้งการสร้างเส้นใยประสาทอย่างสมบูรณ์ซึ่งเส้นใยประสาทมีหน้าที่ช่วยในการทำงานและเพิ่ม ประสิทธิภาพการสื่อสารระหว่างเซลล์สมอง ดังนั้น สารสื่อ ประสาทจึงมีบทบาทสำคัญในช่วงเวลานี้ เพราะเซลล์ประสาท ต้องอาศัย “สารสื่อประสาท” ซึ่งเป็นสาร เคมีที่ทำหน้าที่ส่งต่อ ข้อมูลระหว่างเซลล์ประสาททำให้เกิดการทำงานของสมองสร้างความทรงจำ การเรียนรู้ ส่งผลต่อความคิดการ วิเคราะห์จาก งานวิจัยพบว่าร่างกายเด็กต้องการสารตั้งต้นสำหรับการสร้าง สารสื่อประสาทสำคัญในปริมาณที่สูงกว่าผู้ใหญ่อย่างมาก โดยเด็กเล็กต้องการสารสื่อประสาทมากกว่าผู้ใหญ่ถึง 6 เท่า
					</span>
				</span>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="container-detail-table">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
			<div class="section">
				<span class="text-title col-xs-12 col-no-padding FThin">สารอาหารสำคัญที่สร้างสมองลูกใน <img class="text-1000-days img-1000-mobile visible-xs" src="{{ $BASE_CDN }}/images/1000-days/1000-days-text.png" alt="1000-days-text">
				<span class="FXregular"></span></span>
				<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/brain.png" alt="brain">
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					<ul class="list-inner-item">
						<li>
							<span class="list-title">การสร้างเส้นใยประสาท</span>
							<span class="list-description">ส่วนประกอบหลักของเส้นใยประสาท คือ โปรตีน และไขมัน ดังนั้น ใน 1,000 วันแรก เด็กควรได้รับปริมาณโปรตีน และ ไขมันอย่าง เพียงพอ สารอาหารอย่าง แอลฟา-แล็คตัลบูมิน เป็นแหล่งของ โปรตีนคุณภาพ ที่จะมีส่วนช่วยสร้างเส้นใยประสาทของลูก
							</span>
						</li>
						<li>
							<span class="list-title">การสร้างสารสื่อประสาท</span>
							<span class="list-description">ร่างกายเด็กเล็กไม่สามารถสร้างสารสื่อประสาทขึ้นเองได้ จึง จำเป็นต้องได้รับสารอาหารจำพวกโปรตีนเพื่อใช้เป็นสารตั้งต้น ในการสร้างสารสื่อประสาทโดยคุณภาพโปรตีนที่แตกต่างกัน จะส่งผลโดยตรงกับการสร้างสารสื่อประสาท ในสมองของเด็ก ที่ต่างกัน สารอาหารอย่าง แอลฟา-แล็คตัลบูมิน เป็นสารอาหาร ประเภทโปรตีน คุณภาพ ที่จะสามารถช่วยสร้างสารสื่อประสาท ช่วยในการทำงานของสมอง
							</span>
						</li>
					</ul>
				</span>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
			<div class="section">
				<img class="text-1000-days img-1000 hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/1000-days-text.png" alt="1000-days-text">
				<img class="img-responsive hidden-xs img-brain" src="{{ $BASE_CDN }}/images/1000-days/brain.png" alt="brain">
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
