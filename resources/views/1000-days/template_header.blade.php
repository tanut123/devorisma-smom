<header id="growup-header-panel">
	<div id="growup-header-content-panel">
		<div id="growup-header-content">
			<h1 class="FThin">
				<img id="logo-1000-days-mobile" class="visible-xs" src="{{ $BASE_CDN }}/images/1000-days/1000-days-logo.png">
				<span class="FXregular">สร้างลูก…</span>สมองไว ใน <img class="text-1000-days-title" src="{{ $BASE_CDN }}/images/1000-days/1000-days-text.png" alt="1000-days-text">
			</h1>
			<div id="growup-header-content-menu">
				<span id="logo-1000-days-desktop" class="hidden-xs">
					<img src="{{ $BASE_CDN }}/images/1000-days/1000-days-logo.png">
				</span>
				<span id="menu-1000-days">
					<a href="{{ $BASE_LANG }}1000-days/first-270-days" id="menu-1000-days1" class="menu-1000-days {{ (isset($page) && $page == "first-270-days") ? 'menu-1000-days-active' : '' }} ">
						<img class="menu-normal" src="{{ $BASE_CDN }}/images/1000-days/first-270-days-menu.png" alt="first-270-days-menu">
						<img class="menu-normal-active" src="{{ $BASE_CDN }}/images/1000-days/first-270-days-menu-active.png" alt="first-270-days">
					</a>
					<a href="{{ $BASE_LANG }}1000-days/second-365-days" id="menu-1000-days2" class="menu-1000-days {{ (isset($page) && $page == "second-365-days") ? 'menu-1000-days-active' : '' }}">
						<img class="menu-normal" src="{{ $BASE_CDN }}/images/1000-days/second-365-days-menu.png" alt="second-365-days">
						<img class="menu-normal-active" src="{{ $BASE_CDN }}/images/1000-days/second-365-days-menu-active.png" alt="second-365-days">
					</a>
					<a href="{{ $BASE_LANG }}1000-days/final-365-days" id="menu-1000-days3" class="menu-1000-days {{ (isset($page) && $page == "final-365-days") ? 'menu-1000-days-active' : '' }}">
						<img class="menu-normal" src="{{ $BASE_CDN }}/images/1000-days/final-365-days-menu.png" alt="final-365-days">
						<img class="menu-normal-active" src="{{ $BASE_CDN }}/images/1000-days/final-365-days-menu-active.png" alt="final-365-days">
					</a>
				</span>
			</div>
		</div>
	</div>
</header>