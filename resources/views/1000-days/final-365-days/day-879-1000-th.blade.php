<div class="container-detail-table">
	<div class="title-top FThin color-gold">
		<span class="FXregular">วันที่ 879 - 1,000</span> ถ่ายทอดความรักอันยิ่งใหญ่ของแม่<br/>
		<span class="FXregular">DAY 879 - 1,000</span> Always show Your Love
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-879-1000-01.jpg" alt="mom-879-1000-01">
			<span id="day-1-90-title-box2" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">หมั่น</span>แสดงความรัก</span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FThin">
				ด้วยการกอด หอมแก้ม และบอกคำว่า “รัก” กับลูกเสมอ เพื่อเสริมสร้างระบบประสาท ความจำ และยังทำให้ลูกรู้สึกปลอดภัย มีความมั่นใจในตัวเองส่งผลให้ลูกเป็นเด็กอารมณ์ดี
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-879-1000-01.jpg" alt="mom-879-1000-01">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-879-1000-02.jpg" alt="mom-879-1000-02">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
		<div class="section">
			<span class="right">
				<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">หมั่น</span>ชมเชย</span>
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FThin">
					การชม หรือให้กำลังใจเมื่อลูกสามารถทำสิ่งต่างๆ ได้เป็นเหมือนเครื่องมือในการสร้างตัวตนของลูก คำชมเหล่านั้นจะทำให้ลูกรู้สึกภูมิใจในตัวเอง มีกำลังใจที่จะพัฒนาตัวเองให้เก่งขึ้น และเมื่อต้องพบเจอกับปัญหา ก็จะทำให้ลูกสามารถข้ามผ่านอุปสรรคไปได้
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>