@extends('1000-days.template')

@section('content_growup')

@include('1000-days.final-365-days.template_top_menu')
<div class="container-detail">
	@yield('content_final_365_days')
</div>
@endsection
