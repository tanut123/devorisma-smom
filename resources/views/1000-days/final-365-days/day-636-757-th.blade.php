<div class="container-detail-table">
	<div class="title-top FThin color-gold">
		<span class="FXregular">วันที่ 636 - 757</span> สู่เส้นทางการเป็นเด็กฉลาด<br/>
		<span class="FXregular">DAY 636 - 757</span> Time to raise a Smart kid
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-636-757-01.jpg" style="width:175px;" alt="mom-636-757-01">
			<span id="day-1-90-title-box2" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">โภชนาการ</span>สร้างสมอง</span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
				คุณแม่ควรดูแลอาหารของลูกน้อยให้ได้รับสารอาหารทั้ง 5 หมู่ โดยเฉพาะโปรตีนยังคงมีบทบาทที่สำคัญในช่วงนี้ เพื่อสร้างสมองที่พร้อมสมบูรณ์ที่สุด อีกทั้งแอลฟา-แล็คตัลบูมิน ยังช่วยสร้างเส้นใยประสาท และสารสื่อประสาทช่วยในการทำงานของสมองให้สมบูรณ์แบบ ตัวอย่างเมนูแนะนำสำหรับลูกน้อย เช่น เมนูไข่ สำหรับอาหารเช้า, เมนูปลาทะเล อย่างน้อยสัปดาห์ละ 2 ครั้ง, ผักหลากสีสลับกันไปในแต่ละมื้อ และผลไม้เป็นอาหารว่าง เสริมด้วยนม 2-3 แก้วต่อวัน
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" style="width:175px;" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-636-757-01.jpg" alt="mom-636-757-01">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-636-757-02.jpg" alt="mom-636-757-02">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
		<div class="section">
			<span class="right">
				<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">เตรียมพร้อม</span>กระตุ้นพัฒนาการแต่ละช่วงเวลา </span>
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					ลูกวัย 13-18 เดือน ช่วงพัฒนาความจำลูกจะชอบทำอะไร “ซ้ำๆ” เพื่อทบทวนความจำ การเรียนรู้สิ่งซ้ำๆ จะช่วยให้เซลล์ประสาทของลูกเชื่อมโยงกันแข็งแรงขึ้น เมื่อลูกทำสิ่งเดิมๆ ได้ดีแล้ว
				</span>
				<span id="day-1-90-title-box5" class="text-title col-xs-12 col-no-padding FThin"> <span class="FXregular">คุณแม่ควร</span>ค่อยๆแนะนำสิ่งใหม่ๆ ที่ซับซ้อนมากขึ้น</span>
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					ลูกวัย 15-18 เดือน ช่วงพัฒนาด้านภาษา ช่วงนี้ลูกพยายามจะสื่อสารมากขึ้น แต่ยังไม่สามารถพูดได้ชัด หรือพูดประโยคยาวๆ ได้ ดังนั้นคุณแม่ควรย้ำคำพูดของลูกด้วยการออกเสียงที่ถูกต้อง และชัดเจนทุกครั้ง หรืออาจเล่นกับลูกด้วยเกมภาษา จะทำให้ลูกพัฒนาภาษาได้อย่างรวดเร็ว
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-636-757-03.jpg" style="width:350px;" alt="mom-636-757-03">
			<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"> <span class="FXregular">ลูกวัย</span> 18 เดือน - 2 ขวบ </span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
				ช่วงพัฒนาความเป็นตัวตน ลูกวัยนี้จะไม่สามารถควบคุมความต้องการของตนเองได้ อาจชอบแย่งของเล่น หรือไม่ยอมให้ใครยืม คุณแม่ควรค่อยๆ สอนให้ลูกเรียนรู้เกี่ยวกับการ “แบ่งปัน” โดยอธิบายอย่างอ่อนโยน และเป็นตัวอย่างที่ดีให้กับลูกเพราะนั่นจะเป็นการปลูกฝังนิสัยดีๆ ให้กับลูกน้อยของคุณ
				</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-636-757-03.jpg" style="width:350px;" alt="mom-636-757-03">
		</div>
	</div>
	<div class="clearfix"></div>
</div>