<div class="container-detail-table">
	<div class="title-top FThin color-gold">
		<span class="FXregular">วันที่ 758 - 878</span> เปิดประสบการณ์สู่โลกกว้างให้กับลูกน้อย<br/>
		<span class="FXregular">DAY 758 - 878</span> Openness to new Experiences
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-758-878-01.jpg" alt="mom-758-878-01">
			<span id="day-1-90-title-box2" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">ไป</span>สถานที่ใหม่ๆ</span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
				เป็นการเปิดโอกาสให้ลูกได้มีประสบการณ์ และเห็นในสิ่งที่แตกต่าง และหลากหลายลองให้ลูกได้สำรวจไปรอบๆ เขาจะเกิดการทดลองทำสิ่งต่างๆ และพลิกแพลงวิธีการเล่นตามแบบของตัวเอง รวมทั้งลูกยังได้เรียนรู้
				และสังเกตปฏิกิริยาของสิ่งเหล่านั้นอีกด้วย เพื่อเป็นการฝึกให้ลูกใช้สมองในการคิดวิเคราะห์ ฝึกสายตาในการมองเพื่อเรียนรู้สิ่งต่างๆ ที่เกิดขึ้น แต่คุณแม่ควรดูแลความปลอดภัยของลูกอยู่ใกล้ๆ เสมอ
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-758-878-01.jpg" alt="mom-758-878-01">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-758-878-02.jpg" alt="mom-758-878-02">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
		<div class="section">
			<span class="right">
				<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">ทำ</span>กิจกรรมใหม่ๆ</span>
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					เปิดโลกกว้างแห่งการเรียนรู้ด้วยการส่งเสริมกิจกรรมที่หลากหลายให้กับลูกไม่ว่าจะเป็นระบายสี ร้องเพลง เต้นเข้าจังหวะ อ่านนิทานเกมฝึกทักษะต่างๆ ล้วนแล้วแต่เป็นสิ่งที่ทำให้ลูกสนุกที่จะเรียนรู้
					พร้อมที่จะจดจำ คิดต่อยอด มีจินตนาการ และพัฒนาการที่ดีขึ้นด้วยแต่คุณแม่ไม่ควรบังคับ หรือเคี่ยวเข็ญให้ลูกต้องทำควรให้ลูกรู้สึกสนุก และอยากทำด้วยตัวของเขาเอง
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-758-878-03.jpg" alt="mom-758-878-03">
			<span id="day-1-90-title-box5" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พบ</span>เจอคนใหม่ๆ</span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
				การพาลูกเข้าสังคมเพื่อพบเจอเพื่อนใหม่ๆ จะช่วยให้ลูกรู้จักที่จะวางตัว และอยู่ร่วมกับผู้อื่นในสังคม นอกจากนี้การที่ลูกได้เล่นกับเด็กคนอื่นๆ จะทำให้ลูกได้ออกกำลังกาย ซึ่งจะช่วยพัฒนาการทางด้านกล้ามเนื้อของลูกอีกด้วย
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/mom-758-878-03.jpg" alt="mom-758-878-03">
		</div>
	</div>
	<div class="clearfix"></div>
</div>