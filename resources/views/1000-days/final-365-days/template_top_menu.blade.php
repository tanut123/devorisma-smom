<div id="top-menu-panel">
	<h2 class="FXregular"><img src="{{ asset($BASE_CDN . '/images/1000-days/final-365-days/final-365-days.png') }}" alt="final-365-days" class="img-responsive img-center"> </h2>
	<h3 class="FThin"><span class="FXregular">มหัศจรรย์ 365 วัน </span>ขวบปีที่สอง ช่วงเวลาแห่งพัฒนาการของลูกรัก</h3>
	<div id="menu-list-top-panel" class="row">
		<a href="{{ $BASE_LANG }}1000-days/final-365-days/day-636-757" class="menu-list-top-normal {{ isset($sub_page) && $sub_page == "day-636-757" ? 'menu-list-top-normal-active' : '' }} col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-padding">
			<span class="button-list">
				<span class="list-text">
					<span class="text-top-title1 FXregular">Day 636 - 757</span>
					<span class="text-top-title2 FThin">สู่เส้นทางเป็นเด็กฉลาด</span>
				</span>
			</span>
			<span class='text_view'><span class='image_view'></span>[[final-365-days_7]] Views</span>
		</a>
		<a href="{{ $BASE_LANG }}1000-days/final-365-days/day-758-878" class="menu-list-top-normal {{ isset($sub_page) && $sub_page == "day-758-878" ? 'menu-list-top-normal-active' : '' }} col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-padding">
			<span class="button-list">
				<span class="list-text-normal">
					<span class="text-top-title1 FXregular">Day 758 - 878</span>
					<span class="text-top-title2 FThin">เปิดประสบการณ์สู่โลกกว้าง<br/>ให้กับลูกน้อย</span>
				</span>
			</span>
			<span class='text_view'><span class='image_view'></span>[[final-365-days_8]] Views</span>
		</a>
		<a href="{{ $BASE_LANG }}1000-days/final-365-days/day-879-1000" class="menu-list-top-normal {{ isset($sub_page) && $sub_page == "day-879-1000" ? 'menu-list-top-normal-active' : '' }} col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-padding">
			<span class="button-list">
				<span class="list-text">
					<span class="text-top-title1 FXregular">Day 879 - 1,000</span>
					<span class="text-top-title2 FThin">ถ่ายทอดความรักอันยิ่งใหญ่ของแม่</span>
				</span>
			</span>
			<span class='text_view'><span class='image_view'></span>[[final-365-days_9]] Views</span>
		</a>
	</div>
</div>