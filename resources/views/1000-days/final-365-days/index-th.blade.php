@extends('1000-days.final-365-days.template')

@section('content_final_365_days')
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding margin-bottom-10">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/final-365-days/logo-main.png" alt="logo-main">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding margin-bottom-10">
		<div class="section">
			<span class="right">
				<span class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular main-title">ลูก </span><span class="main-sub-title">วัย 13 - 24 เดือน</span></span>
				<span class="text-description col-lg-10 col-md-10 col-sm-10 col-xs-12 col-no-padding FLighter">ช่วงที่ 3 ของ 1,000 วันแรก เป็นอีกหนึ่งช่วงที่สมองของลูกกำลังพัฒนาแบบก้าวกระโดดอย่างต่อเนื่อง จึงเป็นช่วงเวลาสำคัญสำหรับพัฒนาการหลายๆ ด้านของลูก ไม่ว่าจะเป็นด้านร่างกาย สมอง และความเป็นตัวตน คุณแม่ควรต้องให้ความสำคัญกับเรื่องสารอาหาร และการกระตุ้นพัฒนาการที่ช่วยเสริมสร้างสมอง ด้วยการทำกิจกรรมที่หลากหลาย รวมถึงการให้ความรัก และการเอาใจใส่ดูแลลูก
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
@endsection
