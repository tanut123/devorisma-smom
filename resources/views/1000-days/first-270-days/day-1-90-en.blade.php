<div class="container-detail-table">
	<div class="title-top FThin color-gold">
		<span class="FXregular">ตั้งครรภ์วันที่ 1 - 90</span>  ช่วงเริ่มสร้างอวัยวะของลูกน้อย<br/>
		<span class="FXregular">DAY 1 - 90</span> All Major Organs Begin to Form
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-1-90-01.png" alt="mom-1-90-01">
			<span id="day-1-90-title-box2" class="text-title col-xs-12 col-no-padding FThin">
				<span class="text-big"><span class="FXregular">ตั้งครรภ์</span> ไตรมาส 1<br/></span>
				<span class="FXregular">โฟเลต DHA</span> และ <span class="FXregular">แอลฟา-แล็คตัลบูมิน</span>
			</span>
			<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">คุณแม่ตั้งครรภ์จำเป็นต้องได้รับโฟเลตเพื่อช่วยในการสร้างหลอดประสาท และสมองที่สมบูรณ์ของทารก DHA ที่มีส่วนช่วยในการพัฒนาระบบสมอง และสายตาของลูก นอกจากนี้สารอาหารอย่าง แอลฟา-แล็คตัลบูมิน ซึ่งเป็นโปรตีนคุณภาพสูง จะถูกนำไปใช้เป็นสารตั้งต้นในการสร้างสารสื่อประสาทช่วยในการทำงานของสมองลูกน้อยในครรภ์
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-1-90-01.png" alt="mom-1-90-01">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-1-90-02.png" alt="mom-1-90-02">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
		<div class="section">
			<span class="right">
				<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin">ออก<span class="FXregular">กำลังกาย</span>เบาๆ</span>

				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">นอกจากจะช่วยให้คุณแม่รู้สึกผ่อนคลาย นอนหลับได้ดีขึ้น ลดการบวม หรือปวดหลังแล้ว ยังส่งผลให้ลูกน้อยในครรภ์ของคุณมีพัฒนาการทางด้านสมอง และร่างกายที่ดีขึ้นอีกด้วย การออกกำลังกายที่ปลอดภัย ได้แก่ เดิน ว่ายน้ำ และโยคะ โดยอาจเริ่มต้นจากออกกำลังกายครั้งละ 15 นาที 2-3 ครั้งต่อสัปดาห์ จากนั้นค่อยๆ เพิ่มเวลาเป็น 20-30 นาที และทำให้บ่อยครั้งขึ้นในหนึ่งสัปดาห์
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-1-90-03.png" alt="mom-1-90-03">
			<span id="day-1-90-title-box4" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">อารมณ์ของแม่</span>ส่งผลต่อลูก</span>
			<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
				อารมณ์ความรู้สึกของคุณแม่จะส่งผลต่อพัฒนาการทางด้านจิตใจ และสมองของลูกน้อยในครรภ์ ดังนั้นตลอดการตั้งครรภ์ คุณแม่ควรมีอารมณ์ที่ดีอยู่เสมอ หรืออาจหากิจกรรมที่ปลอดภัยทำ เพื่อให้คุณแม่รู้สึกผ่อนคลาย
			</span>
			<span id="day-1-90-title-box5" class="text-title col-xs-12 col-no-padding FThin">เริ่มต้นด้วย<span class="FXregular">การสัมผัส</span></span>
			<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
				การสัมผัสท้องด้วยการตบเบาๆ หรือลูบท้องเป็นจังหวะ เป็นการเริ่มต้นสื่อสารของคุณแม่กับลูกน้อยในครรภ์ คุณแม่สามารถเริ่มทำได้ ตลอดเวลาโดยเฉพาะเมื่อลูกมีอายุ 8 สัปดาห์ขึ้นไป เป็นช่วงที่ลูกเริ่มมีความไวต่อการสัมผัส และเมื่อลูกในครรภ์เริ่มโตมากขึ้น เขาจะตอบสนองต่อการสัมผัสนี้ด้วยการเตะ ผลัก หรือดันกลับ
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-1-90-03.png" alt="mom-1-90-03">
		</div>
	</div>
	<div class="clearfix"></div>
</div>