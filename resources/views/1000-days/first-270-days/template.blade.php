@extends('1000-days.template')

@compressCss("growup.growup_first_270_days")

@section('content_growup')

@include('1000-days.first-270-days.template_top_menu')
<div class="container-detail">
	@yield('content_first_270_days')
</div>
@endsection
