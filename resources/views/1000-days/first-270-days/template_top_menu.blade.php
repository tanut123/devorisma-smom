<div id="top-menu-panel">
	<h2 class="FXregular"><img src="{{ asset($BASE_CDN . '/images/1000-days/first-270-days/first-270-days.png') }}" alt="first-270-days" class="img-responsive img-center"></h2>
	<h3 class="FThin"><span class="FXregular">มหัศจรรย์ 270 วันแรก</span>เพื่อลูกน้อยในครรภ์ของคุณ</h3>
	<div id="menu-list-top-panel" class="row">
		<a href="{{ $BASE_LANG }}1000-days/first-270-days/day-1-90" class="menu-list-top-normal {{ isset($sub_page) && $sub_page == "day-1-90" ? 'menu-list-top-normal-active' : '' }} col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-padding">
			<span class="button-list">
				<span class="list-text">
					<span class="text-top-title1 FXregular">Day 1 - 90</span>
					<span class="text-top-title2 FThin">ตั้งครรภ์ไตรมาส 1</span>
				</span>
			</span>
			<span class='text_view'><span class='image_view'></span>[[first-270-days_1]] Views</span>
		</a>
		<a href="{{ $BASE_LANG }}1000-days/first-270-days/day-91-180" class="menu-list-top-normal {{ isset($sub_page) && $sub_page == "day-91-180" ? 'menu-list-top-normal-active' : '' }} col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-padding">
			<span class="button-list">
				<span class="list-text">
					<span class="text-top-title1 FXregular">Day 91 - 180</span>
					<span class="text-top-title2 FThin">ตั้งครรภ์ไตรมาส 2</span>
				</span>
			</span>
			<span class='text_view'><span class='image_view'></span>[[first-270-days_2]] Views</span>
		</a>
		<a href="{{ $BASE_LANG }}1000-days/first-270-days/day-181-270" class="menu-list-top-normal {{ isset($sub_page) && $sub_page == "day-181-270" ? 'menu-list-top-normal-active' : '' }} col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-padding">
			<span class="button-list">
				<span class="list-text">
					<span class="text-top-title1 FXregular">Day 181 - 270</span>
					<span class="text-top-title2 FThin">ตั้งครรภ์ไตรมาส 3</span>
				</span>
			</span>
			<span class='text_view'><span class='image_view'></span>[[first-270-days_3]] Views</span>
		</a>
	</div>
</div>