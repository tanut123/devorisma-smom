<div class="container-detail-table">
	<div class="title-top FThin color-gold">
		<span class="FXregular">ตั้งครรภ์วันที่ 91 - 180</span>  ช่วงอวัยวะลูกขยายขนาด<br/>
		<span class="FXregular">DAY 91 - 180</span> Organs Continue Developing
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-91-180-01.jpg" alt="mom-91-180-01">
			<span id="day-1-90-title-box2" class="text-title col-xs-12 col-no-padding FThin">
				<span class="text-big"><span class="FXregular">ตั้งครรภ์</span> ไตรมาส 2<br/></span>
				<span class="FXregular">DHA แอลฟา</span>-แล็คตัลบูมิน และแคลเซียม
			</span>
			<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">นอกจากการทานอาหารให้ครบ 5 หมู่แล้ว คุณแม่ตั้งครรภ์ควรได้รับ DHA แอลฟา-แล็คตัลบูมิน แคลเซียม รวมถึง ธาตุเหล็ก ไอโอดีน และวิตามินต่างๆ ให้เพียงพอต่อความต้องการในแต่ละวัน เพื่อสร้างกระดูก และอวัยวะต่างๆ ของร่างกาย โดยเฉพาะสมองของลูกในครรภ์ให้เจริญเติบโตอย่างมีคุณภาพ
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-91-180-01.jpg" alt="mom-91-180-01">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-91-180-02.jpg" alt="mom-91-180-02">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
		<div class="section">
			<span class="right">
				<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">ฟัง</span>เพลง</span>

				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
				เมื่อลูกน้อยในครรภ์ของคุณอายุได้ประมาณ 18 สัปดาห์ หู และสมองของลูกจะเริ่มได้รับรู้ถึงเสียง และประมาณสัปดาห์ที่ 24 หูของลูกจะเจริญเติบโตอย่างเต็มที่ ทำให้ลูกสามารถได้ยินเสียงจากสิ่งแวดล้อมภายนอกครรภ์
				ดังนั้นการเปิดเพลงให้ลูกฟังไม่ว่าจะเป็นเพลงกล่อมเด็ก เพลงคลาสสิก หรือแม้แต่เพลงฟังสบายๆ ที่คุณแม่ชื่นชอบ นอกจากจะทำให้คุณแม่รู้สึกผ่อนคลายแล้ว ยังเป็นส่วนสำคัญส่วนหนึ่ง
				ที่จะช่วยในการพัฒนาระบบประสาท และสมองที่ควบคุมการได้ยินของลูกน้อย และส่งผลต่อพัฒนาการทางด้านภาษา และอารมณ์ของลูกภายหลังคลอดอีกด้วย
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-91-180-03.jpg" alt="mom-91-180-03">
			<span id="day-1-90-title-box5" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พูดคุย</span>กับลูก</span>
			<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
				เสียงของคุณแม่จะส่งผลต่อลูกมากที่สุด เพราะลูกสามารถได้ยินเสียงของแม่ในระยะใกล้ และชัดเจนที่สุด ลูกจะสนใจฟังในสิ่งที่คุณแม่พูด หรือคุยกับเขาคุณแม่อาจลองต้ังชื่อลูกเมื่อทราบเพศแล้ว และสื่อสารกับลูกน้อยในครรภ์ด้วยเรียกชื่อลูกบ่อยๆ เล่าเรื่องต่างๆ ให้ลูกฟังด้วยน้ำเสียงที่อ่อนโยน
				หรือแม้แต่การอ่านนิทาน และโคลงกลอนให้ฟัง ล้วนแล้วแต่จะช่วยพัฒนาองค์ความรู้ และทักษะในการ ใช้ภาษาให้กับลูกน้อยต่อๆ ไป
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-91-180-03.jpg" alt="mom-91-180-03">
		</div>
	</div>
	<div class="clearfix"></div>
</div>