<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding margin-bottom-10">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/logo-main.png" alt="logo-main">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding margin-bottom-10">
		<div class="section">
			<span class="right">
				<span class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular main-title">จากแม่</span><br/> <span class="main-sub-title">สู่ลูกน้อยในครรภ์</span></span>
				<span class="text-description col-lg-10 col-md-10 col-sm-10 col-xs-12 col-no-padding FLighter">270 วันแรกในระหว่างตั้งครรภ์ เป็นช่วงเวลาของการสร้างสมองลูก คุณแม่ควรได้รับสารอาหารต่างๆ อย่างเพียงพอ โดยเฉพาะโปรตีน ที่มีผลโดยตรงต่อการสร้าง และพัฒนาสมองของลูก นอกจากนี้ กิจกรรมต่างๆ รวมถึงอารมณ์ และความรู้สึกของคุณแม่ก็สำคัญไม่แพ้กัน ที่จะช่วยหล่อหลอมให้ลูกรักของคุณกลายเป็นเด็กฉลาด แข็งแรง และมีพัฒนาการที่ดี
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>