<div class="container-detail-table">
	<div class="title-top FThin color-gold">
		<span class="FXregular">ตั้งครรภ์วันที่ 181 - 270</span>  ช่วงลูกเจริญเติบโต<br/>
		<span class="FXregular">DAY 181 - 270</span> Baby is...growing
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-181-270-01.jpg" alt="mom-181-270-01">
			<span id="day-1-90-title-box2" class="text-title col-xs-12 col-no-padding FThin">
				<span class="text-big"><span class="FXregular">ตั้งครรภ์</span> ไตรมาส 3<br/></span>
				<span class="FXregular">DHA แอลฟา</span>-แล็คตัลบูมิน แคลเซียม ธาตุเหล็ก และวิตามินต่างๆ
			</span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
				สำหรับไตรมาสนี้เป็นช่วงเวลาสำคัญของการพัฒนาสมอง และระบบประสาทของลูก โดยเฉพาะอย่างยิ่งในช่วงอายุครรภ์ 6 - 7 เดือน การทานแอลฟา-แล็คตัลบูมินจึงมีความสำคัญเป็นอย่างมาก นอกจากนี้คุณแม่ควรรับประทานธาตุเหล็กเพื่อเตรียมตัวช่วงคลอดที่ต้องเสียเลือดมาก
				วิตามินเค ที่ช่วยป้องกันเลือดออกตามอวัยวะต่างๆ ของทารกแรกเกิด และทานอาหารที่มีรสร้อน เช่น ยำหัวปลี ไก่ผัดใบกระเพรา หมูผัดขิง เพื่อช่วยเพิ่มน้ำนมให้คุณแม่หลังคลอด และการทาน DHA แอลฟา-แล็คตัลบูมิน แคลเซียม ในปริมาณที่เพียงพอ จะทำให้ร่างกายคุณแม่สะสมสารอาหารเหล่านี้ ไปใช้ผลิตน้ำนมที่มีคุณภาพสูง สำหรับเลี้ยงลูกหลังคลอดอีกด้วย
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-181-270-01.jpg" alt="mom-181-270-01">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/first-270-days/mom-181-270-02.jpg" alt="mom-181-270-02">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
		<div class="section">
			<span class="right">
				<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">เล่น<span class="FThin">ส่องไฟ</span></span></span>

				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
				ลูกน้อยในครรภ์ของคุณเติบโตขึ้นมากจนสามารถมองเห็น การเล่นส่องไฟที่หน้าท้องจะช่วยให้ลูกเรียนรู้ความความแตกต่างของความมืด และความสว่างได้ เป็นการกระตุ้นให้เซลล์สมอง และเส้นประสาทส่วนการรับภาพ และการมองเห็นของลูกน้อยมีพัฒนาการที่ดี
				</span>
				<span id="day-1-90-title-box5" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">อยู่ในท้อง<span class="FThin">จนครบ 39-40 สัปดาห์</span></span></span>
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					ยิ่งลูกอยู่ในท้องนานเท่าไหร่ สมองของลูกจะยิ่งมีพัฒนาการที่ดีมากขึ้นเท่านั้น เพราะสมองของลูกน้อยในครรภ์
					จะพัฒนาอย่างเต็มที่เมื่ออายุประมาณ 39-40 สัปดาห์ ดังนั้นคุณแม่ควรดูแลตัวเองให้ดี ไม่ควรให้มีโรคแทรกซ้อน
					และทำจิตใจให้แจ่มใสผ่อนคลายอยู่เสมอ เพื่อรอเวลาที่เหมาะสมสำหรับลูกน้อยคนเก่งของคุณ
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>