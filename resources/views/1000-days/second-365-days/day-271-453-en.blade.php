<div class="container-detail-table">
	<div class="title-top FThin color-gold">
		<span class="FXregular">วันที่ 271 - 453</span>  มหัศจรรย์น้ำนมแม่<br/>
		<span class="FXregular">DAY 271 - 453</span> Miracle of breast milk
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-271-453-01.jpg" alt="mom-271-453-01">
			<span id="day-1-90-title-box2" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">มหัศจรรย์น้ำนมแม่<span class="FThin">กระตุ้นพัฒนาการของลูกน้อยวัย 6 เดือนแรก</span></span></span>
			<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
				ระยะหกเดือนแรกหลังคลอด เป็นช่วงที่อวัยวะต่างๆ ในร่างกายลูกยังพัฒนา และโตไม่เต็มที่ แลัวยังเป็นช่วงที่มีความสำคัญในการสร้างรากฐานการเจริญเติบโตของสมองลูกน้อย
				<p>ดังนั้นอาหารที่ดีที่สุดของลูกวัยนี้ ก็คือ “นมแม่” เพราะอุดมด้วยสารอาหารสำคัญๆ เช่น โปรตีน, คาร์โบไฮเดรต, ไขมัน และเกลือแร่ ที่ช่วยในการเจริญเติบโต และยังมีสารเอนไซม์และฮอร์โมน ซึ่งเป็นตัวช่วยสร้างเซลล์สมองของลูก ทำให้เป็นเด็กฉลาด</p>
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-271-453-01.jpg" alt="mom-271-453-01">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-271-453-02.jpg" alt="mom-271-453-02">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
		<div class="section">
			<span class="right">
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					<p>ในน้ำนมแม่ยังมี “แอลฟา-แล็คตัลบูมิน” ซึ่งเป็นเวย์โปรตีน มีคุณค่าทางโภชนาการสูง เป็นโปรตีนย่อยง่าย ลูกน้อยจึงสามารถดูดซึมสารอาหารไปใช้ได้ดี และยังช่วยลดปัญหาเกี่ยวกับระบบทางเดินอาหาร และอาการท้องผูก หรือแหวะนมอีกด้วย อีกหนึ่งคุณสมบัติที่สำคัญของแอลฟา-แล็คตัลบูมิน คือ ช่วยในกระบวนการสร้างน้ำนมเพื่อให้ได้น้ำนมปริมาณเพียงพอสำหรับการเลี้ยงลูก</p>
					<p>หัวน้ำนม หรือคอเลสตรัม เป็นนมที่มีประโยชน์ที่สุด หัวน้ำนมสีเหลืองเข้มนั้น มีสารเบต้าแคโรทีนที่ช่วยในการสร้างภูมิต้านทานโรค และเป็นยาระบายอ่อนๆ ช่วยให้เด็กตัวเหลืองลดลง การกินนมแม่เป็นการช่วยกระตุ้นประสาทสัมผัสต่างๆ ของลูก ด้วยการมองประสานสายตากันระหว่างคุณแม่และลูก
						การได้ยินเสียงหัวใจเต้นของแม่ ขณะที่หูของลูกแนบอยู่บริเวณหน้าอกมือของลูกได้สัมผัสร่างกาย และเสื้อผ้าของคุณแม่ การได้รับรู้รสชาติของน้ำนม และการได้กลิ่นในระหว่างที่คุณแม่โอบอุ้มลูก แต่อีกหนึ่งสัมผัสที่สำคัญที่สุด ก็คือสายใยรัก และความผูกพันทางด้านจิตใจระหว่างคุณแม่กับลูก</p>
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>