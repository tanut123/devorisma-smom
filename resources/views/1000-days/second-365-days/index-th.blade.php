<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding margin-bottom-10">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/logo-main.png" alt="logo-main">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding margin-bottom-10">
		<div class="section">
			<span class="right">
				<span class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular main-title">นมแม่</span><br/> <span class="main-sub-title">อาหารที่ดีที่สุดของทารก</span></span>
				<span class="text-description col-lg-10 col-md-10 col-sm-10 col-xs-12 col-no-padding FLighter">ช่วง 365 วันนี้ หรือขวบปีแรก เป็นช่วงที่สมองของลูกพัฒนาอย่างรวดเร็วต่อเนื่อง ลูกควรจะได้รับนมแม่ให้นานที่สุด เพราะนมแม่อุดมด้วยสารอาหารสำคัญๆ ที่ช่วยให้ร่างกาย และสมองของลูกน้อยเจริญเติบโตได้ดี โดยเฉพาะ “แอลฟา-แล็คตัลบูมิน” เป็นโปรตีนคุณภาพสูง เพื่อที่จะช่วยสร้างการสมอง และสร้างพัฒนาการที่ดีของลูก
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>