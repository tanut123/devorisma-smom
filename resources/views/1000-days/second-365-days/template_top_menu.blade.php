<div id="top-menu-panel">
	<h2 class="FXregular"><img src="{{ asset($BASE_CDN. '/images/1000-days/second-365-days/second-365-days.png') }}" alt="second-365-days" class="img-responsive img-center"></h2>
	<h3 class="FThin"><span class="FXregular">มหัศจรรย์ 365 วันแรก</span> ขวบปีแรก นมแม่ดีที่สุด</h3>
	<div id="menu-list-top-panel" class="row">
		<a href="{{ $BASE_LANG }}1000-days/second-365-days/day-271-453" class="menu-list-top-normal {{ isset($sub_page) && $sub_page == "day-271-453" ? 'menu-list-top-normal-active' : '' }} col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-padding">
			<span class="button-list">
				<span class="list-text">
					<span class="text-top-title1 FXregular">Day 271 - 453</span>
					<span class="text-top-title2 FThin">มหัศจรรย์น้ำนมแม่</span>
				</span>
			</span>
			<span class='text_view'><span class='image_view'></span>[[second-365-days_4]] Views</span>
		</a>
		<a href="{{ $BASE_LANG }}1000-days/second-365-days/day-454-544" class="menu-list-top-normal {{ isset($sub_page) && $sub_page == "day-454-544" ? 'menu-list-top-normal-active' : '' }} col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-padding">
			<span class="button-list">
				<span class="list-text">
					<span class="text-top-title1 FXregular">Day 454 - 544</span>
					<span class="text-top-title2 FThin">เสริมสร้างพัฒนาการของลูกน้อย</span>
				</span>
			</span>
			<span class='text_view'><span class='image_view'></span>[[second-365-days_5]] Views</span>
		</a>
		<a href="{{ $BASE_LANG }}1000-days/second-365-days/day-545-635" class="menu-list-top-normal {{ isset($sub_page) && $sub_page == "day-545-635" ? 'menu-list-top-normal-active' : '' }} col-lg-4 col-md-4 col-sm-4 col-xs-12 col-no-padding">
			<span class="button-list">
				<span class="list-text-normal">
					<span class="text-top-title1 FXregular">Day 545 - 635</span>
					<span class="text-top-title2 FThin">ส่งเสริมพัฒนาการของลูกน้อย<br/>อย่างต่อเนื่อง</span>
				</span>
			</span>
			<span class='text_view'><span class='image_view'></span>[[second-365-days_6]] Views</span>
		</a>
	</div>
</div>