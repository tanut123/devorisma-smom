@extends('1000-days.template')

@section('content_growup')

@include('1000-days.second-365-days.template_top_menu')
<div class="container-detail">
	@yield('content_second_365_days')
</div>
@endsection
