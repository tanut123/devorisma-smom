<div class="container-detail-table">
	<div class="title-top FThin color-gold">
		<span class="FXregular">วันที่ 454 - 544</span> เสริมสร้างพัฒนาการของลูกน้อย<br/>
		<span class="FXregular">DAY 454 - 544</span> Enhance your baby’s skills
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-454-544-01.jpg" alt="mom-454-544-01">
			<span class="text-big FThin color-gold"><span class="FXregular">กระตุ้นพัฒนาการ<br/></span><span class="text-big-small">รอบด้านของลูกน้อยวัย 7-9 เดือน</span><br/></span>
			<span id="day-1-90-title-box2" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการ<span class="FThin">เกี่ยวกับการกิน</span></span></span></span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
				กินนมแม่และเสริมอาหารแทนนมแม่ 1-2 มื้อ ลูกเริ่มคันเหงือก และฟันกำลังขึ้น สามารถบดและกลืนอาหารได้ดีขึ้น คุณแม่ควรเริ่มให้ลูกได้ลองอาหารบดละเอียด ซึ่งแต่งรสชาติอาหารได้นิดหน่อย เพื่อให้ลูกมีพัฒนาการเรื่องการบดเคี้ยว และเรียนรู้รสชาติใหม่ๆ ของอาหาร
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-454-544-01.jpg" alt="mom-454-544-01">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-454-544-02.jpg" alt="mom-454-544-02">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
		<div class="section">
			<span class="right">
				<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการกล้าม</span>เนื้อมัดเล็ก<br/></span>
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					ฝึกมองตามของตก คุณแม่ลองใช้ลูกบอลผ้าสีสันสดใส ยิ่งมีเสียงได้ยิ่งดี มาถือไว้ตรงหน้าลูก
					ห่างประมาณ 1 ฟุต แล้วเคลื่อนขึ้นและลงช้าๆ เพื่อให้ลูกจับจ้องอยู่ที่ลูกบอล
					จากนั้นปล่อยลูกบอลให้ตกลงกับพื้น เพื่อกระตุ้นให้ลูกมองตามและไขว่คว้าหาลูกบอล
				</span>
				<span id="day-1-90-title-box5" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการกล้าม</span>เนื้อมัดใหญ่<br/></span>
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					หัดนั่งทรงตัวได้เอง ให้ลูกนั่งบนพื้น โดยมีคุณแม่ช่วยประคองอยู่ข้างๆ แล้วชวนลูกคุยเล่น หรือให้ลูกเล่นของเล่น
					เพื่อไม่ให้เขารู้สึกตัวเวลาที่คุณแม่ค่อยๆ ปล่อยมือที่ใช้ประคองออก ทำวันละนิด แต่ทำบ่อยๆ จนกระทั่งลูกของคุณสามารถนั่งได้เอง
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>

<div class="container-detail-table">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-454-544-03.jpg" alt="mom-454-544-03">
			<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการ</span>ทางสังคม<br/></span>
				<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
					เล่นจ๊ะเอ๋ ช่วงเวลาที่ลูกกำลังมองหน้าคุณแม่ ให้คุณแม่ใช้มือปิดหน้าตัวเอง และเมื่อเปิดหน้า
					ให้พูดคำว่า “จ๊ะเอ๋” เป็นการเสริมสร้างความสัมพันธ์ที่ดีระหว่างลูกน้อยกับคุณแม่เพราะในขณะที่เล่น มีการสบตากัน ยิ้ม หัวเราะ และมองหน้าซึ่งกันและกัน
				</span>
			<span id="day-1-90-title-box5" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการ</span>ทางภาษา<br/></span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
				ฝึกพูดพยางค์เดียว เริ่มจากคุณแม่ทำเสียงที่ไม่เป็นคำให้ลูกเลียนเสียง เช่น กระดกลิ้น หรือทำเสียง
				“ปา” “บา” “มา” ควรเว้นระยะเวลาให้ลูกทำเสียงตามคุณแม่ในแต่ละเสียง แล้วค่อยๆ เริ่มพูดคำที่มีความหมายในชีวิตประจำวัน เช่น “หม่ำ” “พ่อ” “แม่” โดยพูดช้าๆ ชัดๆ และชี้ไปที่สิ่งที่พูดถึง เพื่อให้ลูกเข้าใจความหมาย
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-454-544-03.jpg" alt="mom-454-544-03">
		</div>
	</div>
	<div class="clearfix"></div>
</div>