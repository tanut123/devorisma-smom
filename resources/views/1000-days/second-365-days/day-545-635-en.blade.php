<div class="container-detail-table">
	<div class="title-top FThin color-gold">
		<span class="FXregular">วันที่ 545 - 635</span> ส่งเสริมพัฒนาการของลูกอย่างต่อเนื่อง<br/>
		<span class="FXregular">DAY 545 - 635</span> Enhance your baby’s skills
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-545-635-01.jpg" alt="mom-545-635-01">
			<span class="text-big FThin color-gold"><span class="FXregular">กระตุ้นพัฒนาการ<br/></span><span class="text-big-small">รอบด้านของลูกน้อยวัย 10-12 เดือน</span><br/></span>
			<span id="day-1-90-title-box2" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการ</span>เกี่ยวกับการกิน</span></span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
				กินนมแม่ และเสริมอาหารแทนนมแม่ 3มื้อ ช่วงนี้ลูกมีฟันขึ้นหลายซี่แล้ว ทำให้บดเคี้ยวได้ดีขึ้น ดังนั้นจึงควรให้อาหารแบบอ่อนนุ่ม หรือบดหยาบๆ ที่มีรสชาติอาหารใกล้เคียงกับอาหารของผู้ใหญ่ และควรเริ่มให้ลูกกำช้อนป้อนเพื่อตัวเองบ้าง
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-545-635-01.jpg" alt="mom-545-635-01">
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-545-635-02.jpg" alt="mom-545-635-02">
		</div>
	</div>
	<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 col-no-padding">
		<div class="section">
			<span class="right">
				<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการ</span>กล้ามเนื้อมัดเล็ก</span>
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					ฝึกหยิบสิ่งของเล็กๆ โดยใช้ปลายนิ้วหัวแม่มือ และนิ้วชี้ อาจเป็นของที่กินได้ เช่น ขนมปัง ผลไม้ที่
					นิ่ม และหั่นเป็นชิ้นเล็กๆ เพราะลูกวัยนี้มักชอบหยิบของเข้าปาก จากนั้นให้ลองทำท่าขอของจากลูก เพื่อเป็นการฝึกให้ลูกรู้จักปล่อยของจากมือด้วย
				</span>
				<span id="day-1-90-title-box5" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการ</span>กล้ามเนื้อมัดใหญ่</span>
				<span class="text-description col-lg-12 col-md-12 col-sm-12 col-xs-12 col-no-padding FLighter">
					ฝึกลูกให้ตั้งไข่ เริ่มต้นจากการฝึกลูกให้เกาะยืน โดยจับให้เขานั่งในท่ายองๆ ใกล้กับเก้าอี้ แล้วกระตุ้นให้ลูกยืนด้วยของเล่น
					คุณแม่ควรคอยช่วยจับ จนแน่ใจว่าลูกสามารถจับเก้าอี้ยืนเองได้ เมื่อลูกทำได้คล่องแล้ว ลองพยุงให้ลูกยืนโดยไม่เกาะ และเมื่อเห็นว่าลูกสามารถทรงตัวได้เอง จึงค่อยๆ ปล่อยมือ
				</span>
			</span>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<div class="container-detail-table">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive visible-xs img-mobile" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-545-635-03.jpg" alt="mom-545-635-03">
			<span id="day-1-90-title-box3" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการ</span>ทางสังคม</span>
				<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
					เลียนแบบท่าทาง ลองกระตุ้นให้ลูกทำท่าทางต่างๆ ตามสถานการณ์ เช่น โบกมือ พยักหน้า ส่ายหน้า รับโทรศัพท์ โดยคุณแม่เป็นคนแสดงให้ดู และพูดคำที่เข้ากับท่าทางนั้นๆ จะทำให้ลูกเรียนรู้ว่าเขาจะต้องทำอย่างไรในสถานการณ์ต่างๆ
				</span>
			<span id="day-1-90-title-box5" class="text-title col-xs-12 col-no-padding FThin"><span class="FXregular">พัฒนาการ</span>ทางภาษา</span>
			<span class="text-description col-lg-11 col-md-11 col-sm-11 col-xs-12 col-no-padding FLighter">
				หัดพูดคำสองพยางค์ คุณแม่ควรเริ่มพูดคำที่มีเสียง 2 พยางค์ โดยเป็นคำที่มีเสียงซ้ำๆ เพื่อให้ลูกเลียนเสียงได้ง่าย เช่น หม่ำหม่ำ จ้าจ๊ะ มามา
				ปาปา คุณแม่อาจหาเพลงหรือหนังสือภาพที่มีคำซ้ำๆ เยอะๆ มาเปิดหรืออ่านให้ลูกฟัง เพื่อกระตุ้นให้ลูกรู้สึกสนุกและอยากเลียนเสียงมากขึ้น
			</span>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 col-no-padding">
		<div class="section">
			<img class="img-responsive hidden-xs" src="{{ $BASE_CDN }}/images/1000-days/second-365-days/mom-545-635-03.jpg" alt="mom-545-635-03">
		</div>
	</div>
	<div class="clearfix"></div>
</div>