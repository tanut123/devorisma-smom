<section id="detail-panel1" class="detail-panel">
	<article class="detail-box">
		<div class="detail-content">
			<img class="img-banner-1 hidden-xs" src="{{ asset($BASE_CDN . '/images/app/img-banner-1.png') }}" alt="img-banner-1" />
			<img class="img-banner-1 visible-xs" src="{{ asset($BASE_CDN . '/images/app/img-banner-1-mobile.png') }}" alt="img-banner-1-mobile" />
			<div class="title-banner-box">
				<img class="title-banner hidden-xs" src="{{ asset($BASE_CDN . '/images/app/title-banner-1.png') }}" alt="title-banner-1" />
				<img class="title-banner visible-xs" src="{{ asset($BASE_CDN . '/images/app/title-banner-1-mobile.png') }}" alt="title-banner-1-mobile" />
				<div class="title-banner-text-box">
					<div class="btn-box">
					<a class="btn-app-link" href="https://itunes.apple.com/app/id908937273" target="_blank">
						<img class="btn-app ios" src="{{ asset($BASE_CDN . '/images/app/btn-banner-app-ios.png') }}" alt="btn-app-ios" />
					</a><a class="btn-app-link" href="https://play.google.com/store/apps/details?id=com.nestle.smomclub.phone&hl=en" target="_blank">
						<img class="btn-app andriod" src="{{ asset($BASE_CDN . '/images/app/btn-banner-app-andriod.png') }}" alt="btn-app-andriod" />
					</a>
					</div>
					<div class="title-banner-text FMonX">
						<ul>
							<li>สำรวจโดย Mindshare (Thailand) ข้อมูล ณ วันที่ 24 ตุลาคม 2560</li>
							<li>เฉพาะแม่ตั้งครรภ์</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>
<section id="detail-panel2" class="detail-panel">
	<article class="detail-box">
		<div class="detail-content">
			<div class="panel-content panel-main">
				<img  class="title-banner-2 hidden-xs" src="{{ asset($BASE_CDN . '/images/app/title-banner-2.png') }}" alt="title-banner-2" />
				<img  class="title-banner-2 visible-xs" src="{{ asset($BASE_CDN . '/images/app/title-banner-2-mobile.png') }}" alt="title-banner-2-mobile" />
				<div class="subtitle-banner-2 FMonX">
					สุดยอดนวัตกรรมแห่งแอพพลิเคชั่นหนึ่งเดียวที่ตอบโจทย์ทุกความต้องการของคนเป็นแม่
				</div>
				<img class="img-banner-2 hidden-xs" src="{{ asset($BASE_CDN . '/images/app/img-banner-2.png') }}" alt="img-banner-2" />
				<img class="img-banner-2 visible-xs" src="{{ asset($BASE_CDN . '/images/app/img-banner-2-mobile.png') }}" alt="img-banner-2-mobile" />
			</div>
			<div class="panel-content panel-1">
				<div class="panel-image left">
					<img class="img-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-1.png') }}" alt="img-menu-2-1" />
					<img class="img-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-1-mobile.png') }}" alt="img-menu-2-1-mobile" />
				</div>
				<div class="panel-text right">
					<div class="text-menu-2-1">
						<img class="title-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-1.png') }}" alt="title-menu-2-1" />
						<img class="title-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-1-mobile.png') }}" alt="title-menu-2-1-mobile" />
						<div class="text-menu FMonX">
							ครั้งแรกกับอุปกรณ์ฟังเสียงหัวใจลูกบน<br class="hidden-xs"/>
							Mobile application ฟังเสียงของจังหวะชีวิต<br class="hidden-xs"/>
							น้อยๆ ในครรภ์ของคุณแม่ได้ง่ายๆ เพียง<br class="hidden-xs"/>
							ปลายนิ้วคลิก
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-content panel-2">
				<div class="panel-image right">
					<img class="img-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-2.png') }}" alt="img-menu-2-2" />
					<img class="img-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-2-mobile.png') }}" alt="img-menu-2-2-mobile" />
				</div>
				<div class="panel-text left">
					<div class="text-menu-2-2">
						<img class="title-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-2.png') }}" alt="title-menu-2-2" />
						<img class="title-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-2-mobile.png') }}" alt="title-menu-2-2-mobile" />
						<div class="text-menu FMonX">
							Community แม่ท้องแห่งเดียวในประเทศไทย<br class="hidden-xs"/>
							บน Mobile Application ให้คุณแม่เม้าท์มอย<br class="hidden-xs"/>
							ประสาแม่ท้อง พร้อมพูดคุยปรึกษาปัญหาหรือ<br class="hidden-xs"/>
							ความกังวลใจกับพยาบาลตลอด 24 ชั่วโมง
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-content panel-3">
				<div class="panel-image left">
					<img class="img-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-3.png') }}" alt="img-menu-2-3" />
					<img class="img-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-3-mobile.png') }}" alt="img-menu-2-3-mobile" />
				</div>
				<div class="panel-text right">
					<div class="text-menu-2-3">
						<img class="title-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-3.png') }}" alt="title-menu-2-3" />
						<img class="title-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-3-mobile.png') }}" alt="title-menu-2-3-mobile" />
						<div class="text-menu FMonX">
							ทันทุกพัฒนาการของลูกน้อยในครรภ์ด้วยฟังก์ชั่น<br class="hidden-xs"/>
							แจ้งเตือนตามอายุครรภ์แบบ Exclusive และข้อมูล<br class="hidden-xs"/>
							ที่เที่ยงตรงแม่นยำ ส่งตรงเพื่อคุณแม่โดยเฉพาะ
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-content panel-4">
				<div class="panel-image right">
					<img class="img-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-4.png') }}" alt="img-menu-2-4" />
					<img class="img-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-4-mobile.png') }}" alt="img-menu-2-4-mobile" />
				</div>
				<div class="panel-text left">
					<div class="text-menu-2-4">
						<img class="title-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-4.png') }}" alt="title-menu-2-4" />
						<img class="title-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-4-mobile.png') }}" alt="title-menu-2-4-mobile" />
						<div class="text-menu FMonX">
							ไม่ว่าจะท้องไหน ท้องที่เท่าไหร่ อวดท้องสวยๆได้ทุกวัน<br class="hidden-xs"/>
							 ทุกสัปดาห์ แชร์ไปให้โลกรู้ พร้อมฟังก์ชั่นแต่งภาพสวยๆ<br class="hidden-xs"/>
							 ให้คุณแม่เล่นสนุกกับลูกน้อยได้ตั้งแต่ในครรภ์
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-content panel-5">
				<div class="panel-image left">
					<img class="img-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-5.png') }}" alt="img-menu-2-5" />
					<img class="img-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/img-menu-2-5-mobile.png') }}" alt="img-menu-2-5-mobile" />
				</div>
				<div class="panel-text right">
					<div class="text-menu-2-5">
						<img class="title-menu hidden-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-5.png') }}" alt="title-menu-2-5" />
						<img class="title-menu visible-xs" src="{{ asset($BASE_CDN . '/images/app/title-menu-2-5-mobile.png') }}" alt="title-menu-2-5-mobile" />
						<div class="text-menu FMonX">
							บันทึกและตั้งเตือนนัดหมายสำคัญ ให้คุณแม่<br class="hidden-xs"/>
							ไม่พลาดทุกนัดหมายกับคุณหมอตลอดระยะเวลา<br class="hidden-xs"/>
							ตั้งครรภ์สุดพิเศษนี้
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</article>
</section>
<section id="detail-panel3" class="detail-panel">
	<article class="detail-box">
		<div class="detail-content">
			<div class="detail-content-inner">
			<div class="content-row">
				<div class="content-box">
					<img class="img-content" src="{{ asset($BASE_CDN . '/images/app/img-menu-3-1.png') }}" alt="img-menu-3-1" />
					<div class="title-content-box">
						<img class="title-content" src="{{ asset($BASE_CDN . '/images/app/title-menu-3-1.png') }}" alt="title-menu-3-1" />
					</div>
					<div class="text-content FMonX">
						อัพเดตทุกเทคโนโลยีเพื่อแม่ท้องทันสมัยเช่นคุณ
					</div>
				</div>
				<div class="content-box">
					<img class="img-content" src="{{ asset($BASE_CDN . '/images/app/img-menu-3-2.png') }}" alt="img-menu-3-2" />
					<div class="title-content-box">
						<img class="title-content" src="{{ asset($BASE_CDN . '/images/app/title-menu-3-2.png') }}" alt="title-menu-3-2" />
					</div>
					<div class="text-content FMonX">
						ค้นหาชื่อดี ชื่อเด่น เป็นมงคลให้ลูกรัก
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-row">
				<div class="content-box">
					<img class="img-content" src="{{ asset($BASE_CDN . '/images/app/img-menu-3-3.png') }}" alt="img-menu-3-3" />
					<div class="title-content-box">
						<img class="title-content" src="{{ asset($BASE_CDN . '/images/app/title-menu-3-3.png') }}" alt="title-menu-3-3" />
					</div>
					<div class="text-content FMonX">
						ตอบสารพัดปัญหาเรื่องนอยด์กังวลใจแบบเจาะลึก<br/>ทุก insight ของแม่ท้อง
					</div>
				</div>
				<div class="content-box">
					<img class="img-content" src="{{ asset($BASE_CDN . '/images/app/img-menu-3-4.png') }}" alt="img-menu-3-4" />
					<div class="title-content-box">
						<img class="title-content" src="{{ asset($BASE_CDN . '/images/app/title-menu-3-4.png') }}" alt="title-menu-3-4" />
					</div>
					<div class="text-content FMonX">
						ติดตามและเข้าถึงทุกกิจกรรมดีๆ สำหรับ<br/>คุณแม่ตั้งครรภ์
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			</div>
		</div>
	</article>
</section>
<section id="detail-panel-app" class="detail-panel">
	<article class="detail-box">
		<div class="detail-content">
			<div class="text-app-box">
				<img class="text-app hidden-xs" src="{{ asset($BASE_CDN . '/images/app/text-app.png') }}" alt="text-app" /><img class="text-app visible-xs" src="{{ asset($BASE_CDN . '/images/app/text-app-mobile.png') }}" alt="text-app-mobile" />
				<div class="btn-app-bar">
					<a class="btn-app-link" href="https://itunes.apple.com/app/id908937273" target="_blank">
						<img class="btn-app ios" src="{{ asset($BASE_CDN . '/images/app/btn-ios.png') }}" alt="btn-app-ios" />
					</a><a class="btn-app-link" href="https://play.google.com/store/apps/details?id=com.nestle.smomclub.phone&hl=en" target="_blank">
						<img class="btn-app andriod" src="{{ asset($BASE_CDN . '/images/app/btn-andriod.png') }}" alt="btn-app-andriod" />
					</a>
				</div>
			</div>
			<img class="img-hand hidden-xs" src="{{ asset($BASE_CDN . '/images/app/hand-desktop.png') }}" alt="hand-desktop" />
			<img class="img-hand visible-xs" src="{{ asset($BASE_CDN . '/images/app/hand-mobile.png') }}" alt="hand-mobile" />
		</div>
	</article>
</section>