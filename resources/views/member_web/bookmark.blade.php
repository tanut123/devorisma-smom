@extends('template.master')

@compressCss("slick,slick-theme,member_web.bookmark")

@section('content')
	@if($html != "")
	<div id="last-article">
		<div id="last-article-panel">
			<div id="last-article-panel-top">
				<h1 id="last-article-panel-top-title" class="FLighter color-gold">
					{{ trans('member_web_bookmark.BOOKMARK_LASTEST_ARTICLE') }}
				</h1>
			</div>
			<div id="last-article-panel-content">
				<ul id="last-article-panel-content-list">
					<?=$html ?>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	@endif
	<div id="bookmark-panel">
		<div id="bookmark-panel-inner">
			<div id="top-panel">
				<h1 id="main-bookmark-title" class="FLighter color-gold">
					{{ trans('member_web_bookmark.BOOKMARK_MY_ARTICLE') }}
				</h1>
				<a id="btn-manage-bookmark">
					<span class="icon-bookmark icon-pen"></span>
					<span class="text-normal FXMed color-gold">
						{{ trans('member_web_bookmark.BOOKMARK_MANAGE_ARTICLE') }}
					</span>
				</a>
			</div>
			<div id="content-panel">
				<ul id="list-panel">
				</ul>
				<div id="loadmore">
					{{ trans('core.CORE_LOADMORE') }}
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		var DATA_NOT_FOUND = '{{ trans('core.DATA_NOT_FOUND') }}';
		var BOOKMARK_CONFIRM_REMOVE = '{{ trans('member_web_bookmark.BOOKMARK_CONFIRM_REMOVE') }}';
		var BOOKMARK_ALERT_NOT_COMPLETE = '{{ trans('member_web_bookmark.BOOKMARK_ALERT_NOT_COMPLETE') }}';
	</script>
@endsection

@compressJs("slick.min,member_web.bookmark,favorite")
