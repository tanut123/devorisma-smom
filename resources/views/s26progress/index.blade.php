@extends('template.master')
@compressCss("slick,slick-theme")
@compressCss("app")
@compressCss("s26progress")
<!-- @compressCss("smartbrain")
 -->
@section('content')

 <div class="s-mom-site">
 	<section class="section-cover">
 		<div class="border"><div class="layer"></div></div>
 		<div class="headline-text-new">ใหม่</div>
 		<div class="headline-text-uht">ครั้งแรกในรูปแบบ UHT</div>
 		<div class="logo">
 			<img src="{{ asset($BASE_CDN . '/images/cover/logo.png') }}" class="i_png" >
 			<img src="{{ asset($BASE_CDN . '/images/cover/logo.gif') }}" class="i_gif" >
 		</div>
 		<div class="sub-headline">เอส-26 โกลด์ โปรเกรส ยูเอชที</div>
 		<div class="product set-anim anim-product">
 			<div class="product-wrap">
 				<div class="lion set-anim anim-lion"></div>
 				<div class="choline set-anim anim-choline anim-float">Choline 12 mg</div>
 				<div class="omega set-anim anim-omega anim-float">Omega 3, 6, 9</div>
 				<div class="dha set-anim anim-dha anim-float">DHA High Calcium</div>
 				<div class="lutein set-anim anim-lutein anim-float">Lutein 34 mcg</div>
 				<div class="fiber set-anim anim-fiber anim-float">ผสมใยอาหารจากธรรมชาติ</div>
 			</div>
 		</div>
 		<div class="code">ฆอ. 2331/2562</div>
 	</section>
 	<section class="section-media">
 		<div class="container">
 			<div class="boxVideo">
                    <div class="wrapVideoD">
                      <div class="embed-responsive embed-responsive-16by9">
                        <video id="videoMainD" src="../video/desktop_v3.mp4" loop muted autoplay playsinline> </video>
                      </div>
                    </div>
                    <div class="wrapVideoM">
                      <div class="embed-responsive embed-responsive-100">
                        <video id="videoMainM" src="../video/mobile_v3.mp4" loop muted autoplay playsinline> </video>
                      </div>
                    </div>
                 </div>
 		</div>
 	</section>
 	<section class="section-nutrients">
 		<div class="container">
 			<div class="head">
 				<h3>นมเอส-26 โกลด์ โปรเกรส ยูเอชที มีอะไร ?</h3>
 			</div>
 			<div class="body">
 				<div class="text" style="text-align: center;">
 					<p>การเรียนรู้ของลูกหลากหลาย ทั้งในบ้านและนอกบ้าน ต้องเรียนรู้สิ่งใหม่ๆ ตลอดเวลา </p>
 				</div>
 				

 				<div class="future">
 					<div class="lion">
 						เรียนรู้สิ่งที่ชอบเพื่ออนาคตที่ใช่
 					</div>
 					<div class="lion-text">
 						
 					</div>
 				</div>
 				<div class="recomment">
 					<h3>* นม 1 กล่อง ให้สารอาหารดังนี้ โอเมก้า 3 142 มก., โอเมก้า 6 884 มก., โอเมก้า 9 2,127 มก., ลูทีน 34 มคก., โคลีน 12 มก., ดีเอชเอ 8 มก., <br/>ผสมใยอาหารจากธรรมชาติ</h3>
 					<h4>ควรกินอาหารหลากหลายครบ 5 หมู่ ในสัดส่วนที่เหมาะสมเป็นประจำ</h4>
 				</div>
 				<div class="code2">ฆอ. 2331/2562</div>
 				<!-- <div class="lion-mini">
 					มีให้เลือก 2 สูตร ที่เหมาะกับเด็กแต่ละช่วงวัย ได้แก่
 				</div> -->
 				<div class="nutrients-footer"></div>
 				<div class="code">ฆอ. 2331/2562</div>
 			</div>
 		</div>
 	</section>

 	<section class="section-info-steps">
 		<div class="container">

 			<div class="info-steps-box">
 				<div class="border show-desktop"><div class="layer"></div></div>

		 		<div class="step-1">
		 			<div class="wrap-box">
		 				<h1>เอส-26 โกลด์ โปรเกรส ยูเอชที</h1>
		 				<p>เชื่อว่าเด็กทุกคนมีความแตกต่างกัน และมีพรสวรรค์ในแบบของเค้า</p>
		 			</div>
		 		</div>
		 		<div class="lion-1"></div>
		 		<div class="pointer-1"></div>

		 		<div class="step-2">
		 			<div class="wrap-box">
		 				<p>ให้เค้าได้เรียนรู้ในสิ่งที่รัก จะเป็นการ ส่งเสริมพรสวรรค์ ในตัวไปอีกขั้น</p>
		 			</div>
		 		</div>
		 		<div class="lion-2"></div>
		 		<div class="pointer-2"></div>

		 		<div class="step-3">
		 			<div class="wrap-box">
		 				<p>เพื่อต่อยอดสู่อนาคตในแบบที่เค้าชอบ</p>
		 			</div>
		 		</div>
		 		<div class="lion-3"></div>
		 		<div class="lion-4"></div>
		 		<div class="text-end">นอกจากการส่งเสริมการเรียนรู้แล้ว <span class="br-1">คุณแม่อย่าลืมเลือกเสริมนมที่มีสารอาหาร</span> <span class="br-2">ให้เค้าระหว่างมื้อด้วยนะคะ</span></div>
		 		<div class="code">ควรกินอาหารหลากหลายครบ 5 หมู่ ในสัดส่วนที่เหมาะสมเป็นประจำ<br/>ฆอ. 2240/2562</div>
 			</div>
	 		
 		</div>
 	</section>
 	<section class="section-shop">
 		<div class="container">
 			<div class="wrap-shop">
		 		<div class="shop-box">
		 			<h3>สั่งซื้อออนไลน์</h3>
		 			<div class="link-box">
		 				<a href="http://bit.ly/s26golduht" target="_blank" style="background-image: url({{ asset($BASE_CDN . '/images/shop/online-lazada.png') }})"></a>
		 				<a style="display: none;" href="" style="background-image: url({{ asset($BASE_CDN . '/images/shop/online-jd.png') }})"></a>
		 				<a style="display: none;" href="" style="background-image: url({{ asset($BASE_CDN . '/images/shop/online-top.png') }})"></a>
		 				<a style="display: none;" href="" style="background-image: url({{ asset($BASE_CDN . '/images/shop/online-bigc.png') }})"></a>
		 			</div>
		 		</div>
		 		<div class="shop-box" style="display: none;">
		 			<h3>สถานที่จำหน่าย</h3>
		 			<div class="link-box">
		 				<a href="" style="background-image: url({{ asset($BASE_CDN . '/images/shop/shop1.png') }});"></a>
		 				<a href="" style="background-image: url({{ asset($BASE_CDN . '/images/shop/shop2.png') }});"></a>
		 				<a href="" style="background-image: url({{ asset($BASE_CDN . '/images/shop/shop3.png') }});"></a>
		 				<a href="" style="background-image: url({{ asset($BASE_CDN . '/images/shop/shop4.png') }});"></a>
		 				<a href="" style="background-image: url({{ asset($BASE_CDN . '/images/shop/shop5.png') }});"></a>
		 				<a href="" style="background-image: url({{ asset($BASE_CDN . '/images/shop/shop6.png') }});"></a>
		 			</div>
		 		</div>
	 		</div>
 		</div>
 	</section>
 </div>

	<script>
	window.lazySizesConfig = window.lazySizesConfig || {};
	window.lazySizesConfig.customMedia = {
    	'--mb': '(max-width: 767px)'
	};
</script>
@endsection


@compressJs("lazy.bgset.min,lazysizes.min,slick.min,iphone-inline-video.min,anime.min,web,ani_progress")
@section('scripts')

@endsection



