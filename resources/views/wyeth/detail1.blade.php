@extends('template.master')
@compressCss("wyeth_detail")
@section('content')
<div class="wyeth-detail-panel">
	<div class="wyeth-detail-panel-inner">
		<div class="head-panel">
			<img src="{{ asset($BASE_CDN . '/images/wyeth/detail/logo_wyeth.png') }}" class="logo_wyeth" width="558" height="107" />
		</div>
		<div class="title-panel FMonXBold">
			<div class="title">
				ด้วยความทุ่มเทเพื่อความสมบูรณ์แบบ กับงานวิจัยมากว่า 100 ปี <br class="hidden-xs"/>
				จาก ไวเอท นิวทริชั่น และยังวิจัยอย่างต่อเนื่อง เพื่อเสริมศักยภาพ<br class="hidden-xs"/>
				สมองของลูกน้อย
			</div>
			<div class="subtitle FMonXMed">
				ไวเอท นิวทริชั่น ค้นคว้าและวิจัยในเรื่องสารอาหารสำหรับเสริมการพัฒนาสมองที่เด็ก<br class="hidden-xs"/>
				ต้องการตั้งแต่ 1,000 วันแรกของชีวิต
			</div>
		</div>
		<div class="content-panel FX">
			Wyeth Nutrition เป็นผู้คิดค้นและพัฒนาสูตรนมผงที่เหมาะสำหรับทารก และไม่เคยหยุดยั้งในการพัฒนา<br class="hidden-xs"/>สารอาหารมากว่า 100 ปี เพื่อให้แม่มั่นใจในสารอาหารเพื่อทุกพัฒนาการที่ก้าวไกลของลูก
			<div class="content-row">
				<div class="content-box">
					<div class="text-year-box Waseem">
						1915
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-1915.svg') }}" class="icon-year y1915" width="49" height="69" /> -->
					</div>
					<div class="text-detail FX">
						พัฒนานมผง Modern Infant Formula
					</div>
				</div>
				<div class="content-box">
					<div class="text-year-box Waseem">
						1933
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-1933.svg') }}" class="icon-year y1933" width="53" height="78" /> -->
					</div>
					<div class="text-detail FX">
						นมผงดัดแปลงสูตรแรกที่เสริมเบต้าแคโรทีน<br class="hidden-xs"/>
						อันเป็นสารตั้งต้นของวิตามินเอ
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-row">
				<div class="content-box">
					<div class="text-year-box Waseem">
						1956
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-1956.svg') }}" class="icon-year y1956" width="55" height="81" /> -->
					</div>
					<div class="text-detail FX">
						นมผงดัดแปลงสูตรแรกที่ระบุวันหมดอายุ<br class="hidden-xs"/>
						เพื่อให้มั่นใจความสดใหม่
					</div>
				</div>
				<div class="content-box">
					<div class="text-year-box Waseem">
						1961
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-1961.svg') }}" class="icon-year y1961" width="104" height="87" /> -->
					</div>
					<div class="text-detail FX">
						ค้นพบวิธีการสกัดเอาเกลือแร่ ออกจาก<br class="hidden-xs"/>
						เวย์โปรตีนที่ได้จากนมวัวถึง 95%
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-row">
				<div class="content-box">
					<div class="text-year-box Waseem">
						1989
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-1989.svg') }}" class="icon-year y1989" width="99" height="111" /> -->
					</div>
					<div class="text-detail FX">
						ผู้ผลิตนมผงดัดแปลง นานาชาติรายแรก<br class="hidden-xs"/>
						ของโลกที่เสริม 5 นิวคลีโอไทด์สำคัญ
					</div>
				</div>
				<div class="content-box">
					<div class="text-year-box Waseem">
						1996
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-1996.svg') }}" class="icon-year y1996" width="93" height="100" /> -->
					</div>
					<div class="text-detail FX">
						ร่วมมือกับบริษัท Martek Bioscience Corporation <br class="hidden-xs"/>
						ในการค้นหาแหล่งของ AA และ DHA ที่เหมาะสม<br class="hidden-xs"/>
						สำหรับนมผงดัดแปลงสำหรับทารก ซึ่งช่วยให้นม <br class="hidden-xs"/>
						S-26® สำหรับทารกคลอดก่อนกำหนด มี AA <br class="hidden-xs"/>
						และ DHA จากเชื้อราและสาหร่าย เพื่อช่วยเสริม<br class="hidden-xs"/>
						พัฒนาการทางสมองและการมองเห็น
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-row">
				<div class="content-box">
					<div class="text-year-box Waseem">
						2002
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-2002.svg') }}" class="icon-year y2002" width="102" height="85" /> -->
					</div>
					<div class="text-detail FX">
						เป็นครั้งแรกที่สูตรนมผงเสริมด้วยแอลฟา<br class="hidden-xs"/>
						แล็คตัลบูมินซึ่งเป็นโปรตีนเวย์คุณภาพสูง
					</div>
				</div>
				<div class="content-box">
					<div class="text-year-box Waseem">
						2006
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-2006-2.svg') }}" class="icon-year y2006" width="134" height="96" /> -->
					</div>
					<div class="text-detail FX">
						สูตรแรกของนมผงที่เพิ่มลูทีนซึ่งมีส่วนช่วย<br class="hidden-xs"/>
						เสริมพัฒนาการของดวงตา
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-row">
				<div class="content-box">
					<div class="text-year-box Waseem">
						2010
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-2010.svg') }}" class="icon-year y2010" width="88" height="57" /> -->
					</div>
					<div class="text-detail FX">
						ปรับสูตรนม S-26® GOLD® ให้มีปริมาณ<br class="hidden-xs"/>
						โปรตีนให้ใกล้เคียงกับปริมาณโปรตีนที่พบ<br class="hidden-xs"/>
						ในนมแม่
					</div>
				</div>
				<div class="content-box">
					<div class="text-year-box Waseem">
						2015
						<!-- <img src="{{ asset($BASE_CDN . '/images/wyeth/legacy/img-2015.svg') }}" class="icon-year y2015" width="61" height="61" /> -->
					</div>
					<div class="text-detail FX">
						สนับสนุนทุนวิจัยให้กับสถาบันสุขภาพแห่งชาติของประเทศ<br class="hidden-xs"/>
						สหรัฐอเมริกา (FNIH) ศึกษาการทำงานของสมองเด็กที่<br class="hidden-xs"/>
						สุขภาพดีตั้งแต่แรกเกิดจนอายุ 5 ปี โดยหาปัจจัยการพัฒนา<br class="hidden-xs"/>
						สมองเด็กนอกเหนือจากปัจจัยด้านพันธุกรรมและสิ่งแวดล้อม<br class="hidden-xs"/>
						อื่นๆ เพื่อช่วยให้นักวิจัยสามารถถอดรหัสสมองของเด็กได้
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<a class="btn-back FX" href="{{ url() }}/wyethnutrition#wyeth-article">ย้อนกลับ</a>
	</div>
</div>
@endsection