@extends('template.master')
@compressCss("wyeth_index")
@section('content')
<div class="wyeth-panel">
	<div class="wyeth-panel-inner FX">
		<div class="wyeth-main-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/bg-wyeth-main-mobile.jpg')}}  [--mb] | {{ asset($BASE_CDN . '/images/wyeth/bg-wyeth-main.jpg')}}" data-expand="-10">
            <div class="background_mb lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/kidmom.png')}}  [--mb] | " data-expand="-10">
            </div>
			<div class="wyeth-main-panel-inner">
				<div class="logo lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_4.png')}}" data-expand="-10">

				</div>
				<div class="text-panel">
					ไวเอท นิวทริชั่น ผู้คิดค้นสูตรนมผงที่เหมาะสมสำหรับทารก <br class="hidden-xs"/>และไม่เคยหยุดยั้งในการพัฒนาสารอาหารมากกว่า 100 ปี จากความเข้าใจแม่
				</div>
				<div class="button-box">
					<a href="/wyethnutrition#wyeth-article" class="button-link">
						<div class="icon article lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-10">
						</div><div class="text article">Article</div>
					</a>
					<a href="/wyethnutrition#wyeth-legacy" class="button-link">
						<div class="icon legacy lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-10">
						</div><div class="text legacy">Legacy</div>
					</a>
					<a href="/wyethnutrition#wyeth-contact" class="button-link">
						<div class="icon contact lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-10">
						</div><div class="text">Contact us</div>
					</a>
				</div>
			</div>
		</div>
		<div class="wyeth-menubar" id="wyeth-article">
		</div>
		<div class="wyeth-article-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/bg-wyeth-article-mobile.jpg')}} [--mb] | {{ asset($BASE_CDN . '/images/wyeth/bg-wyeth-article.jpg')}}" data-expand="-10">
			<div class="wyeth-article-panel-inner">
				<div class="title-box">
					<div class="title-icon article lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-10"></div>
					<div class="title-text"><span class="FXBold">บทความจากไวเอท</span></div>
				</div>
				<div class="article-panel">
					<div class="article-row" >
						<a class="article-box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/bg-article.jpg')}}" data-expand="-10" href="{{ url() }}/wyethnutrition/detail1">
							<div class="text-box">
								<div class="title FMonXBold"><span class="top">100 ปี </span><br/>จาก ไวเอท นิวทริชั่น</div>
								<div class="desc FX">ค้นคว้าและวิจัยในเรื่องสารอาหารสำหรับเสริมพัฒนาการสมองที่เด็กต้องการตั้งแต่ 1,000 วันแรกของชีวิต</div>
							</div>
							<div class="btn-arrow lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-2"></div>
						</a>
						<a class="article-box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/bg-article.jpg')}}" data-expand="-10"  href="{{ url() }}/wyethnutrition/detail2">
							<div class="text-box">
								<div class="title FMonXBold"><span class="top">What is</span><br/>Homogenization?</div>
								<div class="desc FX">ความทุ่มเทนำมาซึ่งกระบวนการผลิตที่สมบูรณ์แบบ</div>
							</div>
							<div class="btn-arrow lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-2"></div>
						</a>
						<div class="clearfix"></div>
					</div>
					<div class="article-row">
						<a class="article-box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/bg-article.jpg')}}" data-expand="-10" href="{{ url() }}/wyethnutrition/detail3">
							<div class="text-box">
								<div class="title FMonXBold"><span class="top">ทุ่มเท</span><br/>สมบูรณ์แบบ</div>
								<div class="desc FX">ตามแบบฉบับสวิตเซอร์แลนด์</div>
							</div>
							<div class="btn-arrow lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-2"></div>
						</a>
						<a class="article-box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/bg-article.jpg')}}" data-expand="-10" href="{{ url() }}/wyethnutrition/detail4">
							<div class="text-box">
								<div class="title FMonXBold"><span class="top">ด้วยความทุ่มเท</span><br/>ค้นคว้า วิจัยที่ไม่เคยหยุดยั้ง</div>
								<div class="desc FX">กับการสนับสนุนทุนวิจัยให้กับสถาบัน</div>
							</div>
							<div class="btn-arrow lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-2"></div>
						</a>
						<div class="clearfix"></div>
					</div>
				</div>
				<img data-src="{{ asset($BASE_CDN . '/images/wyeth/img-kid.png') }}" data-sizes="auto" class="img-article lazyload" width="555" height="625" data-expand="+10"/>
			</div>
		</div>
		<div class="wyeth-menubar" id="wyeth-legacy">
		</div>
		<div class="wyeth-legacy-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/legacy_background.jpg')}} [--mb] | {{ asset($BASE_CDN . '/images/wyeth/bg-wyeth-legacy.jpg')}}" data-expand="-10">
            <div class="legacy_bg lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/legacy_doctor.png')}} [--mb]" data-expand="-10">

            </div>
			<div class="wyeth-legacy-panel-inner">
				<div class="title-box">
					<div class="title-icon legacy"></div>
					<div class="title-text"><span class="FXBold">มากกว่า 1 ศตวรรษ</span> ที่ผ่านมาเราได้<br class='mbnl'>บรรลุจุดหมายสำคัญ<br class="dtnl"/>ทางโภชนาการ<br class='mbnl'>หลายประการ</div>
				</div>
				<div class="arrow">
					<div class="left_arrow lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/arrow.png')}}" data-expand="-10"></div>
					<div class="right_arrow lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/arrow.png')}}" data-expand="-10"></div>
				</div>
				<div class="slide_panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/line.png')}}" data-expand="-10" id="slide_panel">
					<div class="data_show_panel">
						<div class="data_table">
							<div class="data_row">
								<div class="data_cell data1">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1915.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon new product1"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product1 lazyload" width="159" height="43" data-expand="-10"/>
											<div class="detail" >
												พัฒนานมผงดัดแปลงสำหรับทารกขึ้นเป็นครั้งแรก
											</div>
										</div>
									</div>
									<div class="history_dot" data="1" scroll="0">
										<div class="year year1 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data2">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1920.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon new product2"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product2 lazyload" width="159" height="43" data-expand="-10"/>
											<div class="detail">
												นม SMA<sup>®</sup> ออกสู่ตลาดเป็นครั้งแรก
											</div>
										</div>
									</div>
									<div class="history_dot" data="2" scroll="56">
										<div class="year year2 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data3">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1933.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon product3"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product3 lazyload" width="159" height="43" data-expand="-10"/>
											<div class="detail">
												นมผงดัดแปลงสูตรแรกที่เสริมเบต้าแคโรทีนอันเป็นสารตั้งต้นของวิตามินเอในปริมาณเทียบเท่ากับนมแม่
											</div>
										</div>
									</div>
									<div class="history_dot" data="3" scroll="196">
										<div class="year year3 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data4">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1947.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon product4"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product4 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												นมผงดัดแปลงสูตรแรกที่เสริมวิตามินซีชนิดเสถียร
											</div>
										</div>
									</div>
									<div class="history_dot" data="4" scroll="290">
										<div class="year year4 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data5">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1951.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon new product5"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product5 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												เริ่มใช้โลหะเคลือบกระป๋อง เป็นครั้งแรก นับเป็นความก้าวหน้าครั้งสำคัญทั้งด้านสุขภาพและความปลอดภัย
											</div>
										</div>
									</div>
									<div class="history_dot" data="5" scroll="420">
										<div class="year year5 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data6">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1956.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon new product6"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product6 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												นมผงดัดแปลงสูตรแรกที่ระบุวันหมดอายุเพื่อให้มั่นใจความสดใหม่
											</div>
										</div>
									</div>
									<div class="history_dot" data="6" scroll="529">
										<div class="year year6 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data7">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1961.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon new product7"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product7 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												ค้นพบวิธีการสกัดเอาเกลือแร่ ออกจาก เวย์โปรตีนที่ได้จากนมวัวถึง 95%
												และเปิดตัวนม S-26<sup>®</sup> อันเป็นนมผงดัดแปลงสำหรับทารกสูตรแรกที่มีเวย์โปรตีนเด่น
											</div>
										</div>
									</div>
									<div class="history_dot" data="7" scroll="644">
										<div class="year year7 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data8">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1989.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon product8"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product8 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												ผู้ผลิตนมผงดัดแปลง นานาชาติรายแรกของโลกที่เสริม 5 นิวคลีโอไทด์สำคัญในปริมาณเทียบเท่านมแม่ในนมผงดัดแปลงสำหรับทารก
											</div>
										</div>
									</div>
									<div class="history_dot" data="8" scroll="738">
										<div class="year year8 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data9">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1996.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon product9"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product9 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												ร่วมมือกับบริษัท Martek Bioscience Corporation ในการค้นหาแหล่งของ AA และ DHA ที่เหมาะสมสำหรับนมผง ดัดแปลงสำหรับทารก ซึ่งช่วยให้นม S-26<sup>®</sup> สำหรับทารกคลอดก่อนกำหนด มี AA และ DHA จากเชื้อราและสาหร่ายในปริมาณเทียบเท่านมแม่เพื่อช่วยเสริมพัฒนาการทางสมองและการมองเห็น
											</div>
										</div>
									</div>
									<div class="history_dot" data="9" scroll="886">
										<div class="year year9 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data10">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/1997.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon product10"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product10 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												ครั้งแรกในการพัฒนานมผงดัดแปลง สูตรเวย์โปรตีนเด่นที่ไม่มีน้ำตาลแลคโตสสำหรับทารกที่มีภาวะไม่ทนต่อน้ำตาลแลคโตสในนม
											</div>
										</div>
									</div>
									<div class="history_dot" data="10" scroll="936">
										<div class="year year10 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data11">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/2002.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon product11"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product11 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												เป็นครั้งแรกที่สูตรนมผงเสริมด้วยแอลฟา-แล็คตัลบูมินซึ่งเป็นโปรตีนเวย์คุณภาพสูง
											</div>
										</div>
									</div>
									<div class="history_dot" data="11" scroll="1005">
										<div class="year year11 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data12">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/2006.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon product12"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product12 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												สูตรแรกของนมผงที่เพิ่มลูทีนซึ่งช่วยเสริมพัฒนาการของดวงตา
											</div>
										</div>
									</div>
									<div class="history_dot" data="12" scroll="1099">
										<div class="year year12 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data13">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/2010.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon origin product13"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product13 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												ปรับสูตรนม S-26<sup>®</sup> GOLD<sup>®</sup> ให้มีปริมาณโปรตีนให้ใกล้เคียงกับนมแม่และให้ 100% ของสารอาหารสำคัญ 5 ชนิดที่เสี่ยงขาดในเด็กที่ได้สารอาหารน้อยกว่าเกณฑ์
											</div>
										</div>
									</div>
									<div class="history_dot" data="13" scroll="1250">
										<div class="year year13 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
								<div class="line">
									<div class="line_dash"></div>
								</div>
								<div class="data_cell data14">
									<div class="data">
										<div class="detail_box lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/background_circle.jpg')}}" data-expand="-10">
											<div class="year lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_big/2011.png')}}" data-expand="-10"></div>
											<!-- <div class="product-icon origin product14"></div> -->
											<img data-src="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png') }}" class="wyeth_product product14 lazyload" width="159" height="43" data-expand="-10" />
											<div class="detail">
												เปิดตัวนม ILLUMA<sup>®</sup> สูตร NUTRISORB™/ Human Affinity อันเป็นนมผงดัดแปลงสำหรับทารกสูตรแรกที่เข้าใจถึงประโยชน์ของนมแม่ถึงระดับโมเลกุล
											</div>
										</div>
									</div>
									<div class="history_dot" data="14" scroll="1323">
										<div class="year year14 lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/year_small.png')}}" data-expand="-10"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
                <div class="mobile_detail_panel">
                </div>
                <a href="/wyethnutrition-legacy" target="_blank"><div class="readmore FXMed">Read more</div></a>
			</div>
		</div>
		<div class="wyeth-menubar" id="wyeth-contact">
		</div>
		<div class="wyeth-contact-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/contact_background.jpg')}} [--mb] | {{ asset($BASE_CDN . '/images/wyeth/bg-wyeth-contact.jpg')}}" data-expand="-10">
            <div class="contact_bg lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/contact_mom.png')}} [--mb]" data-expand="-10">

            </div>
			<div class="wyeth-contact-panel-inner">
			<div class="title-box">
				<div class="title-icon contact"></div>
				<div class="title-text gold">
					<div class="header">
						<span class="FXBold">ติดต่อสอบถาม</span>
						ข้อมูลเพิ่มเติม <br/>หรือขอคำแนะนำจากเรา
					</div>
					<div class="address">
						<div class='FXBold f34'>Wyeth Nutrition</div>
						<div>
							999/9 Rama 1 Rd. Pathumwan<br>
							Bangkok 10330
						</div>
					</div>
					<div class="telephone">
						Tel. <a href='tel:026402288' class='phone'>0-2640-2288</a><br>
						{{--Fax +66 2657 8880<br>
						Email <a href='mailto:info@wyeth.com' class='mail'>info@wyeth.com</a>--}}
					</div>
				</div>

			</div>
			</div>
		</div>
	</div>
	<div class="wyeth_nav_bar">
		<div class="align">
			<div class="logo lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_3.png')}}" data-expand="-10"></div>
			<div class="button_panel">
				<a href="/wyethnutrition#wyeth-article">
					<div class="wyeth_btn wyeth_btn1">
						<div class="wyeth-icon article lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-10"></div><div class="wyeth-text FX">Article</div>
					</div>
				</a>
				<a href="/wyethnutrition#wyeth-legacy">
					<div class="wyeth_btn wyeth_btn2">
						<div class="wyeth-icon legacy lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-10"></div><div class="wyeth-text FX">Legacy</div>
					</div>
				</a>
				<a href="/wyethnutrition#wyeth-contact">
					<div class="wyeth_btn wyeth_btn3">
						<div class="wyeth-icon contact lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}" data-expand="-10"></div><div class="wyeth-text FX">Contact us</div>
					</div>
				</a>
			</div>
		</div>
	</div>
</div>
<script>
	window.lazySizesConfig = window.lazySizesConfig || {};
	window.lazySizesConfig.customMedia = {
    	'--mb': '(max-width: 767px)'
	};
</script>
@endsection
@compressJs("lazy.bgset.min,lazysizes.min,wyeth_index")