@extends('template.master')
@compressCss("wyeth_legacy")
@section('content')
<div class="legacy-panel">
	<div class="legacy-panel-inner">
		<div class="panel-title">
			<div class="wyeth-title FMonX">
				<span class="title FMonXBold">The History of Wyeth Nutrition | ไวเอท นิวทริชั่น</span><br/>
				<span class="subtitle">ไวเอท นิวทริชั่น ผู้คิดค้นและพัฒนาสูตรนมผงที่เหมาะสำหรับทารกและไม่เคยหยุดยั้ง<br class="hidden-xs">ในการพัฒนาสารอาหารมากกว่า 100 ปี</span>
			</div>
			<div class="wyeth-logo"></div>
		</div>
		<div class="panel-content">
			<div class="content-box content-1">
				<div class="content-side left">
					<div class="content-img img1"></div>
				</div>
				<div class="content-side right">
					<div class="content-text">
						<div class="content-year year1860"></div>
						<div class="content-title FMonXBold">จอห์น ไวเอท <br class="visible-xs">และ แฟรงค์ ไวเอท</div>
						<div class="content-desc FX">ได้เปิดกิจการร้านขายยา<br class="visible-xs">ทีมีห้องแล็บ<br class="hidden-xs">ที่ใช้เป็นที่<br class="visible-xs">คิดค้นพัฒนาสูตรนมผง</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-2">
				<div class="content-side left">
					<div class="content-text">
						<div class="content-year year1915"></div>
						<div class="content-title FMonXBold">พัฒนานมผง<br class="visible-xs">ดัดแปลง</div>
						<div class="content-desc FX">สำหรับทารกขึ้นเป็น</div>
						<div class="content-highlight FMonXBold">ครั้งแรก</div>
					</div>
				</div>
				<div class="content-side right">
					<div class="content-img img2"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-3">
				<div class="content-side left">
					<div class="content-img img3"></div>
				</div>
				<div class="content-side right">
					<div class="content-text">
						<div class="content-year year1915"></div>
						<div class="content-title FMonXBold">ครั้งแรกในการ<br class="visible-xs">พัฒนาสูตร</div>
						<div class="content-desc FX">ปราศจาก น้ำตาลแลคโตส</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-4">
				<div class="content-side left">
					<div class="content-text">
						<div class="content-year year1933"></div>
						<div class="content-title plus FMonXBold">Beta-<br class="hidden-xs">Carotene</div>
					</div>
				</div>
				<div class="content-side right">
					<div class="content-img img4"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-5">
				<div class="content-side left">
					<div class="content-img img5"></div>
				</div>
				<div class="content-side right">
					<div class="content-text">
						<div class="content-year year1947"></div>
						<div class="content-title plus FMonXBold">Vitamin C</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-6">
				<div class="content-side left">
					<div class="content-text">
						<div class="content-year year1951"></div>
						<div class="content-desc FX">เริ่มใช้โลหะเคลือบกระป๋อง</div>
						<div class="content-highlight FMonXBold">เป็นครั้งแรก</div>
					</div>
				</div>
				<div class="content-side right">
					<div class="content-img img6"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-7">
				<div class="content-side left">
					<div class="content-img img7"></div>
				</div>
				<div class="content-side right">
					<div class="content-text">
						<div class="content-year year1956"></div>
						<div class="content-title FMonXBold">ครั้งแรกที่มี<br class="visible-xs">การระบุ</div>
						<div class="content-desc FX">หมดอายุ</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-8">
				<div class="content-side left">
					<div class="content-text">
						<div class="content-year year1961"></div>
						<div class="content-highlight FMonXBold">ครั้งแรก</div>
						<div class="content-desc FX">ของสูตรที่มีโปรตีนเวย์<br />เป็นส่วนผสมหลัก</div>
					</div>
				</div>
				<div class="content-side right">
					<div class="content-img img8"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-9">
				<div class="content-side left">
					<div class="content-img img9"></div>
				</div>
				<div class="content-side right">
					<div class="content-text">
						<div class="content-year year1989"></div>
						<div class="content-title FMonXBold">ครั้งแรกของโลก<br class="visible-xs">ที่เสริม</div>
						<div class="content-desc FX">5 นิวคลีโอไทด์ที่สำคัญ</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-10">
				<div class="content-side left">
					<div class="content-text">
						<div class="content-year year1996"></div>
						<div class="content-title FMonXBold">ครั้งแรกที่มี<br class="visible-xs">การเสริม<br class="hidden-xs">AA <br class="visible-xs">และ DHA</div>
					</div>
				</div>
				<div class="content-side right">
					<div class="content-img img10"></div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-11">
				<div class="content-side left">
					<div class="content-img img11">&nbsp;</div>
				</div>
				<div class="content-side right">
					<div class="content-text">
						<div class="content-year year2002"></div>
						<div class="content-title FMonXBold">เสริมแอลฟา<br class="visible-xs">แลคตัลบูมิน</div>
						<div class="content-desc FX">โปรตีนเวย์คุณภาพสูง</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="content-box content-12">
				<div class="content-side left">
					<div class="content-text">
						<div class="content-year year2006"></div>
						<div class="content-title FMonXBold">ครั้งแรกที่มีการ<br class="visible-xs">เสริมลูทีน</div>
						<div class="content-desc FX">ช่วยเสริมพัฒนาการ<br />ของดวงตา</div>
					</div>
				</div>
				<div class="content-side right">
					<div class="content-img img12"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- <div class="popup-panel"><div class="popup-panel-inner"><div class="popup-panel-box"><div class="popup-close"></div><div class="promotion-panel"><img class="promotion-img" src="{{ $BASE_CDN }}/images/wyeth/legacy/popup/coupon-tops.jpg"></div></div></div></div></div> -->
</div>
@endsection
@compressJs("wyeth_legacy")