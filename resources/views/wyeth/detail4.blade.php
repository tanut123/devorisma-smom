@extends('template.master')
@compressCss("wyeth_detail")
@section('content')
<div class="wyeth-detail-panel">
	<div class="wyeth-detail-panel-inner">
		<div class="head-panel">
			<img src="{{ asset($BASE_CDN . '/images/wyeth/detail/logo_wyeth.png') }}" class="logo_wyeth" width="558" height="107" />
		</div>
		<div class="title-panel FMonXBold">
			<div class="title">
				ด้วยความทุ่มเท ค้นคว้า วิจัย ที่ไม่เคยหยุดยั้ง <br class="hidden-xs"/>
				กับการสนับสนุนทุนวิจัยให้กับสถาบัน
			</div>
			<div class="subtitle FMonXMed">
				ไวเอท นิวทริชั่น ในฐานะผู้นำคิดค้น พัฒนาผลิตภัณฑ์ด้านโภชนาการและสารอาหารสำหรับเด็ก
			</div>
		</div>
		<div class="content-panel FX">
			<div class="content-paragraft">
				ไวเอท นิวทริชั่น สนับสนุนทุนการวิจัยให้กับสถาบันสุขภาพแห่งชาติของประเทศสหรัฐอเมริกา (National Institute of Health) ซึ่งเป็นองค์กรในของกระทรวงสาธารณสุขและบริการมนุษยชนแห่งสหรัฐอเมริกา มุ่งเน้นการวิจัยและค้นคว้า<br class="hidden-xs"/>
				องค์ความรู้และนวัตกรรมใหม่ๆ ที่เพิ่มคุณภาพชีวิตและช่วยชีวิตมนุษย์ ภายใต้โครงการวิจัยชื่อ Baby Connectome Project (BCP) อันเป็นส่วนหนึ่งของ NIH Human Connectome Project ที่ทำการศึกษาถึงการทำงานของสมองมนุษย์ ซึ่งนับเป็นครั้งสำคัญที่เราจะทราบถึงข้อมูลเชิงลึกลงไปถึงแผนผังโครงสร้าง การเชื่อมต่อ วงจร และระบบการทำงาน<br class="hidden-xs"/>ของเซลล์ประสาทในสมอง
			</div>
			<div class="content-paragraft">
				Baby Connectome Project นั้นครอบคลุมระยะเวลาการวิจัยนาน 5 ปี โดยเริ่มตั้งแต่ปี 2558 เพื่อศึกษาการพัฒนา<br class="hidden-xs"/>ของสมองและปัจจัยที่มีผลต่อการพัฒนาสมองของเด็กปกติตั้งแต่แรกเกิดจนถึงอายุ 5 ปี การศึกษานี้จะช่วยให้เราเข้าใจ<br class="hidden-xs"/>การเชื่อมต่อของสมองเด็ก รวมถึงแผนผัง วงจร และการทำงานของสมองแต่ละวิถีประสาทสั่งการ ที่เกี่ยวข้องกับ<br class="hidden-xs"/>การแสดงออก ทักษะ และพฤติกรรมของเด็ก
			</div>
			<div class="content-paragraft">
				ผลจากการวิจัยจะช่วยให้เราเข้าถึงองค์ความรู้และกระบวนการทำงานของสมองเด็กอย่างละเอียด ซึ่งเป็นประโยชน์<br class="hidden-xs"/>ต่อนักวิจัยทั่วโลก ที่จะนำข้อมูลนี้ไปต่อยอดการวิจัยที่เกี่ยวกับสมองเด็ก และ ไวเอท นิวทริชั่น ในฐานะผู้นำคิดค้น พัฒนาผลิตภัณฑ์ด้านโภชนาการและสารอาหารสำหรับเด็ก ก็มุ่งมั่นที่จะพัฒนา วิเคราะห์หาสารอาหารสำคัญที่จำเป็นต่อ<br class="hidden-xs"/>การพัฒนาสมองที่ส่งผลต่อศักยภาพของเด็กต่อไปอย่างไม่หยุดยั้ง
			</div>
		</div>
		<a class="btn-back FX" href="{{ url() }}/wyethnutrition#wyeth-article">ย้อนกลับ</a>
	</div>
</div>
@endsection