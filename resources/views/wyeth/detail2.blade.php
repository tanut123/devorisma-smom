@extends('template.master')
@compressCss("wyeth_detail")
@section('content')
<div class="wyeth-detail-panel">
	<div class="wyeth-detail-panel-inner">
		<div class="head-panel">
			<img src="{{ asset($BASE_CDN . '/images/wyeth/detail/logo_wyeth.png') }}" class="logo_wyeth" width="558" height="107" />
		</div>
		<div class="title-panel FMonXBold">
			<div class="title">
				ความทุ่มเทนำมาซึ่งกระบวนการผลิต<br class="hidden-xs"/>ที่สมบูรณ์แบบ / What is Homogenization?
			</div>
			<div class="subtitle FMonXMed">
				ผลิตภัณท์คุณภาพ ด้วยการวิจัยมาตรฐานสวิตเซอร์แลนด์ จากไวเอท นิวทริชั่น เป็นเครื่อง<br class="hidden-xs"/>
				พิสูจน์ให้เห็นถึงความทุ่มเทของทีมนักวิจัย ก่อนจะมาเป็นนมผงคุณภาพ
			</div>
		</div>
		<div class="content-panel FX">
			<div class="content-paragraft">
				โดยมีขั้นตอนการผลิตที่สำคัญเรียกว่า Homogenization คือการทำให้เป็นเนื้อเดียวกันผ่านกระบวนการทำละลาย <br class="hidden-xs"/>
				โดยนำส่วนผสมทั้งหมดตามปริมาณสัดส่วนที่กำหนดมาทำละลายให้เป็นของเหลวเนื้อเดียวกัน แล้วจึงนำเข้าสู่<br class="hidden-xs"/>
				กระบวนการฉีดเป็นละออง (Spray dry) ก่อนทำให้แห้งด้วยความร้อนจนแปรสภาพเป็นผง ก่อนเข้าสู่ขั้นตอนการบรรจุ <br class="hidden-xs"/>
				เนื่องจากกระบวนการ Homogenization เป็นกระบวนการผลิตที่ทำให้ส่วนผสมต่างๆ ผสมเป็นเนื้อเดียวกัน จึงทำให้<br class="hidden-xs"/>
				ผลิตภัณฑ์มีคุณภาพเท่าเทียมกันทุกการผลิต ซึ่งโรงงานผลิตได้การรับรองมาตรฐานคุณภาพการผลิต ISO <br class="hidden-xs"/>
				22000:2005* / FSSC22000:2013** Food Safety Standard
			</div>
			<div class="content-paragraft">
				การคัดเลือกส่วนผสม บรรจุภัณฑ์ จึงอยู่ภายใต้กฎระเบียบที่เข้มงวด มีการตรวจสอบคุณภาพมากกว่า 700 ครั้งต่อหนึ่งชุดการผลิต ควบคุมมาตรฐานการผลิตโดยผู้เชี่ยวชาญ เพื่อให้คุณแม่มั่นใจได้ว่า ลูกน้อยจะได้รับสารอาหารเหมาะสมในทุกๆ ช้อนจากนมผงของ ไวเอท นิวทริชั่น เพื่อให้ทุกโอกาสในอนาคตสร้างได้ด้วยตัวเค้าเอง
			</div>
		</div>
		<a class="btn-back FX" href="{{ url() }}/wyethnutrition#wyeth-article">ย้อนกลับ</a>
	</div>
</div>
@endsection