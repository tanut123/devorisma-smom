@extends('template.master')

@compressCss("jquery-ui,validationEngine.jquery,member")

@section('fb_header')
		<!-- Facebook Conversion Code for Register - Wyeth S26 -->
	<script>(function() {
			var _fbq = window._fbq || (window._fbq = []);
			if (!_fbq.loaded) {
				var fbds = document.createElement('script');
				fbds.async = true;
				fbds.src = '//connect.facebook.net/en_US/fbds.js';
				var s = document.getElementsByTagName('script')[0];
				s.parentNode.insertBefore(fbds, s);
				_fbq.loaded = true;
			}
		})();
		window._fbq = window._fbq || [];
		window._fbq.push(['track', '6028679578042', {'value':'0.00','currency':'USD'}]);
	</script>
@endsection
@section('fb_footer')
		<!-- Facebook Conversion Code for Register - Wyeth S26 -->
	<noscript>
		<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?ev=6028679578042&amp;cd[value]=0.00&amp;cd[currency]=USD&amp;noscript=1" />
	</noscript>
@endsection
@section('content')
@include('member.content-'.trans('core.CORE_LANG'))

<div id="fb-root"></div>

<script type="text/javascript">
	var DATA_NOT_FOUND = '{{ trans('core.DATA_NOT_FOUND') }}';
	var LANG = '{{ trans('core.CORE_LANG') }}';
	var MEMBER_STATUS = '{{ trans('member.MEMBER_STATUS') }}';
	var MEMBER_FIRSTNAME_REQUIRED = '{{ trans('member.MEMBER_FIRSTNAME_REQUIRED') }}'
	var MEMBER_FIRSTNAME_VALID = '{{ trans('member.MEMBER_FIRSTNAME_VALID') }}'
	var MEMBER_LASTNAME_REQUIRED = '{{ trans('member.MEMBER_LASTNAME_REQUIRED') }}'
	var MEMBER_LASTNAME_VALID = '{{ trans('member.MEMBER_LASTNAME_VALID') }}'
	var MEMBER_EMAIL_REQUIRED = '{{ trans('member.MEMBER_EMAIL_REQUIRED') }}'
	var MEMBER_EMAIL_VALID = '{{ trans('member.MEMBER_EMAIL_VALID') }}'
	var MEMBER_STATUS_REQUIRED = '{{ trans('member.MEMBER_STATUS_REQUIRED') }}'
	var MEMBER_MOBILE_REQUIRED = '{{ trans('member.MEMBER_MOBILE_REQUIRED') }}'
	var MEMBER_MOBILE_VALID = '{{ trans('member.MEMBER_MOBILE_VALID') }}'
	var MEMBER_AGE_REQUIRED = '{{ trans('member.MEMBER_AGE_REQUIRED') }}'
	var MEMBER_ACCEPT_REQUIRED = '{{ trans('member.MEMBER_ACCEPT_REQUIRED') }}'
	var EMAIL_MOBILE_SELECT = '{{ trans('member.EMAIL_MOBILE_SELECT') }}'
</script>
@endsection
@compressJs("datepicker,jquery.validationEngine,languages/jquery.validationEngine-".trans('core.CORE_LANG').",member")
