<div id="content-panel">
	<section id="section-1">
		<div class="section-panel">
			<div class="section-inner-panel">
				<div class="content-left col-xs-12 col-sm-5 col-md-5 col-lg-5 col-no-padding">
					<div class="wrap-inner">
						<img class="img1" src="{{ asset($BASE_CDN . '/images/member/s-momclub-register.png') }}" alt="s-momclub-register">
					</div>
				</div>
				<div class="content-right col-xs-12 col-sm-7 col-md-7 col-lg-7 col-no-padding">
					<div class="wrap-inner">
						<span class="text-1 FLighter color-gold">ยินดีต้อนรับสมาชิกใหม่</span>
						<span class="text-2 FXMed color-gold">
							สู่ครอบครัว <img src="{{ asset($BASE_CDN . '/images/member/s-momclub-text-' . App::getLocale() . '.png') }}" alt="s-momclub-register">
						</span>
						<span class="text-3 FLighter color-gold">
							ด้วย <span class="text-4 FBold">S-MomClub Welcome Set</span> <br class="visible-xs"/>สำหรับคนพิเศษของเรา
						</span>
						<span class="text-5 FLighter color-gray">
							<span class="text-6 FBold color-blue">เพียงสมัครสมาชิกวันนี้ !!</span>
							เราขอมอบ S-MomClub Welcome Set ที่มาพร้อมกับ <span class="text-7 FBold">“จดหมายต้อนรับ”</span><br/>
							ที่บอกถึงสิทธิประโยชน์และบริการของเราเพื่อคุณแม่และลูกน้อยคนพิเศษ
						</span>
						<span class="text-8 FLighter">
							<a href="{{ $BASE_LANG }}register#section-3" class="hidden-xs color-gold">
								สมัครสมาชิก
							</a>
							<a href="{{ $BASE_LANG }}register#section-3-link" class="visible-xs color-gold">
								สมัครสมาชิก
							</a>
						</span>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</section>
	<section id="section-2">
		<a id="privilege-to" name="privilege-to"></a>
		<div class="section-panel">
			<div class="section-inner-panel">
				<div class="title-panel">
					<span class="FLighter text-1 color-white">สิทธิประโยชน์<span class="FThin">และบริการ</span></span>
					<span class="FThin text-2 color-white">เพื่อคุณแม่และลูกน้อยคนพิเศษ</span>
				</div>
				<div id="content-list-panel">
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon1.png') }}" class="img img-responsive" alt="img-hexagon1">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">มอบของขวัญที่ระลึก</span><br/>
								<span class="text2 FLighter">สำหรับสมาชิกใหม่</span>
							</span>
						</div>
					</div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon2.png') }}" class="img img-responsive" alt="img-hexagon2">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">บัตรสมาชิก S-MomClub</span>
							</span>
						</div>
					</div>
					<div class="clearfix visible-xs"></div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon3.png') }}" class="img img-responsive" alt="img-hexagon3">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">บริการให้คำปรึกษา</span><br/>
								<span class="text2 FLighter">ผ่านทางเว็บด้านโภชนาการ <br/>และพัฒนาการเด็ก</span><br/>
								<span class="text1 FXMed">S-MomClub.com</span>
							</span>
						</div>
					</div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon4.png') }}" class="img img-responsive" alt="img-hexagon4">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">ให้คำปรึกษาคุณแม่ตั้งครรภ์</span><br/>
								<span class="text2 FLighter">และเยี่ยมเยียนสุขภาพ <br/>บุตร 1 ขวบปี ทาง <br/> โทรศัพท์</span>
								<span class="text1 FXMed">02-640-2288</span>
							</span>
						</div>
					</div>
					<div class="clearfix visible-xs"></div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon5.png') }}" class="img img-responsive" alt="img-hexagon5">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">จัดส่ง  SMS เคล็ดลับ</span><br/>
								<span class="text2 FLighter">การเลี้ยงดูและส่งเสริม<br/>พัฒนาการเด็ก</span>
							</span>
						</div>
					</div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon6.png') }}" class="img img-responsive" alt="img-hexagon6">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">Free Application</span><br/>
								<span class="text2 FLighter">S-MomClub สู่เส้นทาง <br/>การเป็นอัจฉริยะของลูกน้อย</span>
							</span>
						</div>
					</div>
					<div class="clearfix visible-xs"></div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon7.png') }}" class="img img-responsive" alt="img-hexagon7">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">Facebook/SmomClub</span><br/>
								<span class="text2 FLighter">พร้อมเสริมข้อมูลความรู้<br/>
								เพื่อศักยภาพที่ครบ<br/>
								รอบด้านของลูกน้อย</span>
							</span>
						</div>
					</div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon8.png') }}" class="img img-responsive" alt="img-hexagon8">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">รับนิตยสารด้านโภชนาการ</span><br/>
								<span class="text2 FLighter">และพัฒนาการเด็กพร้อมจัด<br/>
								ส่งคู่มือส่งเสริมพัฒนาการ<br/>
								ตามช่วงวัยของลูกน้อย</span>
							</span>
						</div>
					</div>
					<div class="clearfix visible-xs"></div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon9.png') }}" class="img img-responsive" alt="img-hexagon9">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">มอบของขวัญพิเศษ</span>
								<span class="text2 FLighter"><br/>ครบรอบ 1 ปี</span>
							</span>
						</div>
					</div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon10.png') }}" class="img img-responsive" alt="img-hexagon10">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">สิทธิพิเศษเข้าร่วมกิจกรรม</span>
								<span class="text2 FLighter"><br/>สานสัมพันธ์แห่งรักกับเพื่อนๆ <br/>ครอบครัว S-MomClub</span>
							</span>
						</div>
					</div>
					<div class="clearfix visible-xs"></div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon11.png') }}" class="img img-responsive" alt="img-hexagon11">
							<span class="text-panel color-gray">
								<a href="http://line.me/ti/p/@S-MomClub" alt="S-MomClub" target="_blank">
									<span class="text-panel color-gray">
											<span class="text1 FXMed">ID: @S-MomClub</span>
									</span>
								</a>
							</span>
						</div>
					</div>
					<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
						<div class="content-list-wrapper">
							<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon12.png') }}" class="img img-responsive" alt="img-hexagon12">
							<span class="text-panel color-gray">
								<span class="text1 FXMed">S-MomClub Mobile App</span>
							</span>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</section>
	<a href="#" name="section-3-link" class="offset-position visible-xs"></a>
	<section id="section-3">
		<a id="register-to" name="register-to"></a>
		<div class="section-panel">
			<div class="section-inner-panel">
				<div class="title-panel color-gold">
					<span class="FXregular">ช่องทาง<span class="FThin">การสมัครสมาชิก</span></span>
				</div>
				<div id="menu-list-panel">
					<div id="facebook-register" data-type="facebook" class="menu-list-panel ">
						<span class="register-btn">
							<img src="{{ asset($BASE_CDN . '/images/member/icon-facebook.png') }}" class="img-facebook" alt="icon-facebook">
							<img src="{{ asset($BASE_CDN . '/images/member/icon-facebook-active.png') }}" class="img-facebook-active" alt="icon-facebook-active">
							<span class="text-btn FLighter">สมัครด้วย <span class='bold'>Facebook Account</span></span>
							<div class='checkbox_box_panel'>
								<label class="checkbox_panel color-gold">
									<input type="checkbox" id='chk1'>
									<span class="text-over FLighter fb_accept_condition">{{trans("condition.ALL_CONDITION_ACCEPT")}} <a class='more_condition FLighter' href="{{ $BASE_LANG }}page/terms-and-conditions" target='_blank'>{{trans("condition.READ_MORE_CONDITION")}}</a></span>
								</label>
							</div>
							<img src="{{ asset($BASE_CDN . '/images/member/btn-facebook.png') }}" class="btn-facebook" alt="btn-facebook">
							<img src="{{ asset($BASE_CDN . '/images/member/btn-facebook-active.png') }}" class="btn-facebook-active" alt="btn-facebook-active">
						</span>
						<span class="arrow-active">
							<img src="{{ asset($BASE_CDN . '/images/member/arrow-facebook-active.png') }}" class="arrow-facebook-active" alt="arrow-facebook-active">
						</span>
					</div>
					<div class="clearfix visible-xs"></div>
					<div id="facebook-register-mobile-form" class="form-panel form-panel-mobile">
						<span class="text-title-form FXregular color-gold">กรอกข้อมูลสมาชิกในแบบฟอร์มด้านล่าง</span>
						<span class="text-description-form FLighter color-gold">เพื่อสิทธิ์ในการได้รับข่าวสาร โปรโมชั่น และเข้าร่วมกิจกรรมต่างๆ ซึ่งเป็นประโยชน์ในการจะช่วย<br class="hidden-xs"/>ส่งเสริมศักยภาพรอบด้านให้กับลูกของคุณ </span>
						<form id="facebook-mobile" method="POST" action="" class="form-register">
							<input type="hidden" name="type_register" value="1">
							<span class="box-form">
								<span class="input-panel">
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter" for="">ชื่อ*</label>
										<input type="text" name="firstname" class="firstname inp-text FLighter validate[required, custom[onlyLetterNumberCus]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter inp-text-even" for="">นามสกุล*</label>
										<input type="text" name="lastname" class="lastname inp-text-even inp-text FLighter validate[required, custom[onlyLetterNumberCus]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter" for="">โทรศัพท์มือถือ*</label>
										<input type="text" name="mobile" class="mobile inp-text FLighter validate[groupRequired[mobileEmail],custom[phone]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter inp-text-even" for="">อีเมล*</label>
										<input type="text" name="email" class="email inp-text-even inp-text FLighter validate[groupRequired[mobileEmail],custom[email]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<span class="label-list-text color-gold FLighter" for=""><span>สถานะคุณแม่</span>
											<div class="status-list-panel color-gold FLighter">
												<div data-value="1" class="status-list color-gold">• คุณแม่ตั้งครรภ์</div>
												<div data-value="2" class="status-list color-gold">• คุณแม่ให้นมบุตร</div>
												<div data-value="3" class="status-list color-gold">• ลูกน้อยวัยเตาะแตะ</div>
												<div class="arrow-top"></div>
											</div>
										</span>
										<input type="text" readonly name="status" class="status validate[required] status-mom">
									</span>
									<span class="clearfix"></span>
									<span class="box-child_date">
										<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
											<label class="label-placeholder FLighter" for="">กำหนดคลอด*</label>
											<input type="text" name="child_date" class="child_date datepicker inp-text FLighter validate[required]">
										</span>
										<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
											<label class="label-placeholder FLighter inp-text-even" for="">สถานที่ฝากครรภ์*</label>
											<input type="text" name="antenatal_place" class="antenatal_place inp-text-even inp-text FLighter validate[required]">
										</span>
									</span>
									<span class="clearfix"></span>
								</span>
								<span class="clearfix"></span>
								<span class="checkbox-box-panel">
									<span class="checkbox-panel color-gold">
										<span class="text-over FLighter">ข้าพเจ้าได้อ่านและยอมรับข้อตกลง ตลอดจนเงื่อนไขทั้งหมดสําหรับเว็บไซต์ของเนสท์เล่ <a href="{{ $BASE_LANG }}page/terms-and-conditions" target="_blank">ดูรายละเอียดเพิ่มเติมได้ที่นี่</a>่</span>
										<input type="text" readonly name="accept" class="accept validate[required, funcCall[acceptCheck]]">
									</span>
									<span class="checkbox-panel color-gold">
										<span class="text-over FLighter">ข้าพเจ้ามีอายุมากกว่า 13 ปี</span>
										<input type="text" readonly name="age" class="age validate[required, funcCall[ageCheck]]">
									</span>
								</span>
							</span>
							<a class="btn-register FXregular">
								สมัครสมาชิก
							</a>
						</form>
					</div>
					<div id="email-register" data-type="email" href="javascript:void(0);" class="menu-list-panel">
						<span class="register-btn">
							<img src="{{ asset($BASE_CDN . '/images/member/icon-mail.png') }}" class="img-mail" alt="icon-mail">
							<img src="{{ asset($BASE_CDN . '/images/member/icon-mail-active.png') }}" class="img-mail-active" alt="icon-mail-active">
							<span class="text-btn FLighter">สมัครด้วย <span class='bold'>Email Address</span></span>
							<div class='checkbox_box_panel'>
								<label class="checkbox_panel color-gold">
									<input type="checkbox" id='chk2'>
									<span class="text-over FLighter fb_accept_condition">{{trans("condition.ALL_CONDITION_ACCEPT")}} <a class='more_condition FLighter' href="{{ $BASE_LANG }}page/terms-and-conditions" target='_blank'>{{trans("condition.READ_MORE_CONDITION")}}</a></span>
								</label>
							</div>
							<img src="{{ asset($BASE_CDN . '/images/member/btn-mail.png') }}" class="btn-mail" alt="btn-mail">
							<img src="{{ asset($BASE_CDN . '/images/member/btn-mail-active.png') }}" class="btn-mail-active" alt="btn-mail-active">
						</span>
						<span class="arrow-active">
							<img src="{{ asset($BASE_CDN . '/images/member/arrow-mail-active.png') }}" class="arrow-mail-active" alt="arrow-mail-active">
						</span>
					</div>
					<div class="clearfix"></div>
					<div id="email-register-mobile-form" class="form-panel form-panel-mobile">
						<span class="text-title-form FXregular color-gold">กรอกข้อมูลสมาชิกในแบบฟอร์มด้านล่าง</span>
						<span class="text-description-form FLighter color-gold">เพื่อสิทธิ์ในการได้รับข่าวสาร โปรโมชั่น และเข้าร่วมกิจกรรมต่างๆ ซึ่งเป็นประโยชน์ในการจะช่วย<br class="hidden-xs"/>ส่งเสริมศักยภาพรอบด้านให้กับลูกของคุณ </span>
						<form id="mail-mobile" method="POST" action="" class="form-register">
							<input type="hidden" name="type_register" value="2">
							<span class="box-form">
								<span class="input-panel">
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter" for="">ชื่อ*</label>
										<input type="text" name="firstname" class="firstname inp-text FLighter validate[required, custom[onlyLetterNumberCus]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter inp-text-even" for="">นามสกุล*</label>
										<input type="text" name="lastname" class="lastname inp-text-even inp-text FLighter validate[required, custom[onlyLetterNumberCus]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter" for="">โทรศัพท์มือถือ*</label>
										<input type="text" name="mobile" class="mobile inp-text FLighter validate[groupRequired[mobileEmail],custom[phone]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter inp-text-even" for="">อีเมล์*</label>
										<input type="text" name="email" class="email inp-text-even inp-text FLighter validate[groupRequired[mobileEmail],custom[email]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<span class="label-list-text color-gold FLighter" for=""><span>สถานะคุณแม่</span>
											<div class="status-list-panel color-gold FLighter">
												<div data-value="1" class="status-list color-gold">• คุณแม่ตั้งครรภ์</div>
												<div data-value="2" class="status-list color-gold">• คุณแม่ให้นมบุตร</div>
												<div data-value="3" class="status-list color-gold">• ลูกน้อยวัยเตาะแตะ</div>
												<div class="arrow-top"></div>
											</div>
										</span>
										<input type="text" readonly name="status" class="status validate[required] status-mom">
									</span>
									<span class="clearfix"></span>
									<span class="box-child_date">
										<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
											<label class="label-placeholder FLighter" for="">กำหนดคลอด*</label>
											<input type="text" name="child_date" class="child_date datepicker inp-text FLighter validate[required]">
										</span>
										<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
											<label class="label-placeholder FLighter inp-text-even" for="">สถานที่ฝากครรภ์*</label>
											<input type="text" name="antenatal_place" class="antenatal_place inp-text-even inp-text FLighter validate[required]">
										</span>
									</span>
									<span class="clearfix"></span>
								</span>
								<span class="clearfix"></span>
								<span class="checkbox-box-panel">
									<span class="checkbox-panel color-gold">
										<span class="text-over FLighter">ข้าพเจ้าได้อ่านและยอมรับข้อตกลง ตลอดจนเงื่อนไขทั้งหมดสําหรับเว็บไซต์ของเนสท์เล่ <a href="{{ $BASE_LANG }}page/terms-and-conditions" target="_blank">ดูรายละเอียดเพิ่มเติมได้ที่นี่</a>่</span>
										<input type="text" readonly name="accept" class="accept validate[required, funcCall[acceptCheck]]">
									</span>
									<span class="checkbox-panel color-gold">
										<span class="text-over FLighter">ข้าพเจ้ามีอายุมากกว่า 13 ปี</span>
										<input type="text" readonly name="age" class="age validate[required, funcCall[ageCheck]]">
									</span>
								</span>
							</span>
							<a class="btn-register FXregular">
								สมัครสมาชิก
							</a>
						</form>
					</div>
				</div>
				<div class="form-desktop hidden-xs">
					<div id="facebook-register-form" class="form-panel form-panel-desktop">
						<span class="text-title-form FXregular color-gold">กรอกข้อมูลสมาชิกในแบบฟอร์มด้านล่าง</span>
						<span class="text-description-form FLighter color-gold">เพื่อสิทธิ์ในการได้รับข่าวสาร โปรโมชั่น และเข้าร่วมกิจกรรมต่างๆ ซึ่งเป็นประโยชน์ในการจะช่วย<br class="hidden-xs"/>ส่งเสริมศักยภาพรอบด้านให้กับลูกของคุณ </span>
						<form id="facebook-desktop" method="POST" action="" class="form-register">
							<input type="hidden" name="type_register" value="1">
							<span class="box-form">
								<span class="input-panel">
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter" for="">ชื่อ*</label>
										<input type="text" name="firstname" class="firstname inp-text FLighter validate[required, custom[onlyLetterNumberCus]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter inp-text-even" for="">นามสกุล*</label>
										<input type="text" name="lastname" class="lastname inp-text-even inp-text FLighter validate[required, custom[onlyLetterNumberCus]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter" for="">โทรศัพท์มือถือ*</label>
										<input type="text" name="mobile" class="mobile inp-text FLighter validate[groupRequired[mobileEmail],custom[phone]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter inp-text-even" for="">อีเมล*</label>
										<input type="text" name="email" class="email inp-text-even inp-text FLighter validate[groupRequired[mobileEmail],custom[email]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<span class="label-list-text color-gold FLighter" for=""><span>สถานะคุณแม่</span>
											<div class="status-list-panel color-gold FLighter">
												<div data-value="1" class="status-list color-gold">• คุณแม่ตั้งครรภ์</div>
												<div data-value="2" class="status-list color-gold">• คุณแม่ให้นมบุตร</div>
												<div data-value="3" class="status-list color-gold">• ลูกน้อยวัยเตาะแตะ</div>
												<div class="arrow-top"></div>
											</div>
										</span>
										<input type="text" readonly name="status" class="status validate[required] status-mom">
									</span>
									<span class="clearfix"></span>
									<span class="box-child_date">
										<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding content-list-last">
											<label class="label-placeholder FLighter" for="">กำหนดคลอด*</label>
											<input type="text" name="child_date" class="child_date datepicker inp-text FLighter validate[required]]">
										</span>
										<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding content-list-last">
											<label class="label-placeholder FLighter inp-text-even" for="">สถานที่ฝากครรภ์*</label>
											<input type="text" name="antenatal_place" class="antenatal_place inp-text-even inp-text FLighter validate[required]">
										</span>
									</span>
									<span class="clearfix"></span>
								</span>
								<span class="clearfix"></span>
								<span class="checkbox-box-panel">
									<span class="checkbox-panel color-gold">
										<span class="text-over FLighter">ข้าพเจ้าได้อ่านและยอมรับข้อตกลง ตลอดจนเงื่อนไขทั้งหมดสําหรับเว็บไซต์ของเนสท์เล่ <a href="{{ $BASE_LANG }}page/terms-and-conditions" target="_blank">ดูรายละเอียดเพิ่มเติมได้ที่นี่</a>่</span>
										<input type="text" readonly name="accept" class="accept validate[required, funcCall[acceptCheck]]">
									</span>
									<span class="checkbox-panel color-gold">
										<span class="text-over FLighter">ข้าพเจ้ามีอายุมากกว่า 13 ปี</span>
										<input type="text" readonly name="age" class="age validate[required, funcCall[ageCheck]]">
									</span>
								</span>
							</span>
							<a class="btn-register FXregular">
								สมัครสมาชิก
							</a>
						</form>
					</div>
					<div id="email-register-form" class="form-panel form-panel-desktop">
						<span class="text-title-form FXregular color-gold">กรอกข้อมูลสมาชิกในแบบฟอร์มด้านล่าง</span>
						<span class="text-description-form FLighter color-gold">เพื่อสิทธิ์ในการได้รับข่าวสาร โปรโมชั่น และเข้าร่วมกิจกรรมต่างๆ ซึ่งเป็นประโยชน์ในการจะช่วย<br class="hidden-xs"/>ส่งเสริมศักยภาพรอบด้านให้กับลูกของคุณ </span>
						<form id="mail-desktop" method="POST" action="" class="form-register">
							<input type="hidden" name="type_register" value="2">
							<span class="box-form">
								<span class="input-panel">
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter" for="">ชื่อ*</label>
										<input type="text" name="firstname" class="firstname inp-text FLighter validate[required, custom[onlyLetterNumberCus]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter inp-text-even" for="">นามสกุล*</label>
										<input type="text" name="lastname" class="lastname inp-text-even inp-text FLighter validate[required, custom[onlyLetterNumberCus]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter" for="">โทรศัพท์มือถือ*</label>
										<input type="text" name="mobile" class="mobile inp-text FLighter validate[groupRequired[mobileEmail],custom[phone]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<label class="label-placeholder FLighter inp-text-even" for="">อีเมล*</label>
										<input type="text" name="email" class="email inp-text-even inp-text FLighter validate[groupRequired[mobileEmail],custom[email]]">
									</span>
									<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
										<span class="label-list-text color-gold FLighter" for=""><span>สถานะคุณแม่</span>
											<div class="status-list-panel color-gold FLighter">
												<div data-value="1" class="status-list color-gold">• คุณแม่ตั้งครรภ์</div>
												<div data-value="2" class="status-list color-gold">• คุณแม่ให้นมบุตร</div>
												<div data-value="3" class="status-list color-gold">• ลูกน้อยวัยเตาะแตะ</div>
												<div class="arrow-top"></div>
											</div>
										</span>
										<input type="text" readonly name="status" class="status validate[required] status-mom">
									</span>
									<span class="clearfix"></span>
									<span class="box-child_date">
										<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding content-list-last">
											<label class="label-placeholder FLighter" for="">กำหนดคลอด*</label>
											<input type="text" name="child_date" class="child_date datepicker inp-text FLighter validate[required]]">
										</span>
										<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding content-list-last">
											<label class="label-placeholder FLighter inp-text-even" for="">สถานที่ฝากครรภ์*</label>
											<input type="text" name="antenatal_place" class="antenatal_place inp-text-even inp-text FLighter validate[required]">
										</span>
									</span>
									<span class="clearfix"></span>
								</span>
								<span class="clearfix"></span>
								<span class="checkbox-box-panel">
									<span class="checkbox-panel color-gold">
										<span class="text-over FLighter">ข้าพเจ้าได้อ่านและยอมรับข้อตกลง ตลอดจนเงื่อนไขทั้งหมดสําหรับเว็บไซต์ของเนสท์เล่ <a href="{{ $BASE_LANG }}page/terms-and-conditions" target="_blank">ดูรายละเอียดเพิ่มเติมได้ที่นี่</a>่</span>
										<input type="text" readonly name="accept" class="accept validate[required, funcCall[acceptCheck]]">
									</span>
									<span class="checkbox-panel color-gold">
										<span class="text-over FLighter">ข้าพเจ้ามีอายุมากกว่า 13 ปี</span>
										<input type="text" readonly name="age" class="age validate[required, funcCall[ageCheck]]">
									</span>
								</span>
							</span>
							<a class="btn-register FXregular">
								สมัครสมาชิก
							</a>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
