@extends('template.master')

@compressCss("promotion_detail")

@section('content')
	<div id="wapPromotionDetail">
		<div class="detail-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/knowledge/m_bg_object_promotion_new.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/promotion/bg_object_promotion_new.jpg') }}" data-expand="+10">
			<div id="detail-content">
				<h1 id="detail-title">{{ $data["title"]}}</h1>
				<div id="detail-bar">
					<!-- <span id="detail-date" class="FThin">{{ $data["datestring"] }}</span> -->
					<script type="text/javascript">
						var page_uid = "{{$data['promotion_id']}}";
					</script>
					<span id="detail-views" class="FThin" style="visibility:hidden;">
					<img class="icon-view lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-view.png') }}" alt="icon-view" data-expand="+10">
					<span id="total_view"> 0 </span> Views
					</span>
					<span id="detail-share">
						<a id="facebook" data-url="{{ $BASE_LANG }}promotion/{{ $data['slug'] }}">
							<img class="icon-share lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-fb.png') }}" alt="icon-share" data-expand="+10">
						</a>
						<a id="twitter" data-url="{{ $BASE_LANG }}promotion/{{ $data['slug'] }}">
							<img class="icon-twitter lazyload" data-src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-twitter.png') }}" alt="icon-twitter" data-expand="+10">
						</a>
					</span>
				</div>
				<div class="detail-wapper">
					@replaceStockUrl($data["detail"])
				</div>
			</div>
		</div>
	</div>
	<script>
	    window.lazySizesConfig = window.lazySizesConfig || {};
	    window.lazySizesConfig.customMedia = {
	        '--mb': '(max-width: 767px)'
	    };
	</script>
@endsection

@compressJs("lazy.bgset.min,lazysizes.min,promotion_detail")

