@extends('template.master')

@compressCss("promotion")

@section('content')
	<div id="wapPromotion" class="lazyload" data-bgset="{{ asset($BASE_CDN . '/images/knowledge/m_bg_knowledge.png') }} [--mb] | {{ asset($BASE_CDN . '/images/knowledge/bg_knowledge.png') }}" data-expand='+10'>
		<div class="list-content-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/knowledge/m_bg_object_knowledge.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/promotion/bg_object_promotion.jpg') }}" data-expand='+10'>
			<div class="header-content">
				<h1><span class="font-xregular">โปรโมชั่น &amp; กิจกรรม</span></h1>
			</div>
			<ul class="list-panel">

			</ul>
			<div class="clearfix"></div>
			<div class="load-more" style="display:none;">ดูเพิ่มเติม</div>
		</div>
	</div>
	<script>
	    window.lazySizesConfig = window.lazySizesConfig || {};
	    window.lazySizesConfig.customMedia = {
	        '--mb': '(max-width: 767px)'
	    };
	</script>

@endsection

@compressJs("lazy.bgset.min,lazysizes.min,promotion")