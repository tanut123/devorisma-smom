<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>404 Error, the page you're looking for cannot be found</title>

	<style type="text/css">
	body{
		background: rgba(255, 202, 76, 0.8);
	}
	#page404{
		background:white;
		padding:40px;
	}
	#page404 h1{
		font-size:24px;color:#A87914;
	}
	#page404 a{
		color:gray;
	}

	</style>
</head>
<body>
	<div id="page404" >
	<h1>404 Error, the page you're looking for cannot be found</h1>

    <p>It seems that the page you were trying to visit is no longer on my site. Please try to remain calm, these kinds of things happen all the time.</p>
	<br />
	<br />
	<br />
	<ul>
		<li>Hit the “back” button on your browser.</li>
		<li>Head on over to the <a href="/">home page</a>.</li>
	</ul>

	<p></p>

</div>

</body>
</html>