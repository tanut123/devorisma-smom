@if(count($listData) > 0)
	@foreach($listData as $key => $data)
		<li class="FXregular">
			<span class="list-title color-gold">{{ $data->title }}</span>
			<span class="list-detail FLighter color-gray-lighter">
				<?php
				$dataReplace = str_replace($search_word, "<span class='FBold color-blue'>" + $search_word + "</span>", $data->raw_data);
				echo $dataReplace;
				?>
			</span>
			<span class="list-tag color-gold">{{ $data->module }}</span>
		</li>
	@endforeach
@endif
