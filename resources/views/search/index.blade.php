@extends('template.master')

@compressCss("search")

@section('content')
<div id="search-page-panel">
	<section id="search-top-panel">
		<h1 id="text-h1" class="color-gold FXregular">{{ trans('core.CORE_SEARCH') }}</h1>
		<form action="{{ $BASE_LANG }}search/list" name="search" id="search-form" method='post'>
			<input type='hidden' name='csrf_token' value='[[csrf_token]]'>
			<div id="box-search-panel">
				<div class="input-search-panel">
					<input name="q" id="input-search" class="FXregular color-gold" value="{{ $search_word }}">
				</div>
				<span id="btn-submit-search" class="hidden-xs">
					<img src="{{ asset($BASE_CDN . '/images/search/icon-search-desktop.png') }}">
					<span class="FXregular color-white">{{ trans('core.CORE_SEARCH') }}</span>
					<span class="clearfix"></span>
				</span>
				<span id="btn-submit-search-mobile" class="visible-xs">
					<span class="img-panel-search">
						<img class="img-responsive" src="{{ asset($BASE_CDN . '/images/search/icon-search.png') }}">
					</span>
				</span>
				<div class="clearfix"></div>
			</div>
		</form>
	</section>
	<section id="search-detail-panel">
		<div id="result-panel">
			<div id="result-text">
				<span class="color-gold FLighter">
					ผลการค้นหาเกี่ยวกับ
					<span class="color-blue FXregular">
						“<span id="result-text-search">คุณแม่</span>”
					</span>
					พบทั้งหมด
					<span  class="color-blue FXregular">
						<span id="result-text-number">20 </span>
					รายการ</span>
				</span>
			</div>
		</div>
		<div id="list-result-panel">
			<ul id="list-result-inner-panel">

			</ul>
		</div>
		<div id="loadmore" class="color-gold">{{ trans('core.CORE_LOADMORE') }}</div>
	</section>
</div>
<script type="text/javascript">
	var DATA_NOT_FOUND = '{{ trans('core.DATA_NOT_FOUND') }}';
	var LANG = '{{ trans('core.CORE_LANG') }}';
	var SEARCH_LIMIT = '{{ $SEARCH_LIMIT }}';
</script>
@endsection
@compressJs("search")
