@extends('template.master')

@compressCss("forget_pass,register_form")

@section('content')
{{--	<div id="forgot_pass_panel">
		<div class='panel_align'>
			<div class='sub_panel'>
				<div class='header'>
					Header
				</div>
				<div class='input_panel'>
					<span class=''>E-mail:</span><input type='text' name='email'>
				</div>
				<div class='submit_panel'>
					<input type='button' name='submit_reset' value='Send Request for Reset Password' id='send_req'>
				</div>
			</div>
		</div>
	</div>
	--}}
	<div id="register-panel">
		<div id="register-panel-inner">
			<div class="register-panel-top">
				<div class="main-title-cheng-pass anti_alias FX">ลืมรหัสผ่าน</div>
				<div class="register-panel-top-left">
					<form id="register-form" onsubmit="return false" action='{{$BASE_LANG}}password/check_reset_request' method='post'>
						<div class="input-panel email_panel">
							<div class="input-text FLighter">อีเมล</div>
							<input class="input-data FLighter" type="text" name="email">
							<input name='csrf_token' type='hidden' value='[[csrf_token]]'>
						</div>
					</form>
				</div>
				<div class="register-panel-top-right">
					<div class="register-panel-top-right-inner">
						<div class="guide-title-bar">
							<div class="icon-key"></div><span class="guide-title FXMed">คำแนะนำ</span>
						</div>
						<span class="FLighter">ในการสมัครสมาชิกเว็บไซต์ เพื่อเข้าใช้งาน กรุณากรอกรายละเอียดด้านซ้ายมือให้ครบถ้วน เพื่อความสมบูรณ์ของข้อมูลผู้ใช้งาน</span>
						<ul class="guide-panel FLighter">
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">ชื่อผู้ใช้งาน</span><span class="subtitle-guide FLighter"> - กรอกชื่อผู้ใช้งานของคุณ 10 ตัวอักษร, ต้องมีตัวเลขอย่างน้อย 1 หมายเลข (0-9) และต้องมีอย่างน้อย 1 อักขระพิเศษ</span>
								</span>
							</li>
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">อีเมล์</span><span class="subtitle-guide FLighter"> - กรอกอีเมล์ของคุณ เช่น example@mail.com</span>
								</span>
							</li>
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">รหัสผ่าน</span><span class="subtitle-guide FLighter"> - กรุณาระบุรหัสผ่าน โดยต้องมีความยาวไม่ต่ำกว่า 8 ตัวอักษรต้องมีตัวเลขอย่างน้อย 1 หมายเลข (0-9) และต้องมีอย่างน้อย 1 อักขระพิเศษ</span>
								</span>
							</li>
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">ยืนยันรหัสผ่าน</span><span class="subtitle-guide FLighter"> - ระบุรหัสผ่านอีกครั้ง เพื่อยืนยันรหัสผ่าน</span>
								</span>
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-btn">
				<div class="btn-submit FLighter" id='send_req'>ยืนยัน</div>
			</div>
		</div>
	</div>

	<script language="javascript">
		var RESET_REQUEST_SENT = "{{trans('password.RESET_REQUEST_SENT')}}";
		var RESET_REQUEST_FAIL = "{{trans('password.RESET_REQUEST_FAIL')}}";
		var EMAIL_VALID_MESSAGE = "{{trans('subscribe.SUBSCRIBE_MISMATCH')}}";
	</script>
@endsection
@compressJs("forget_pass")
