@extends('template.master')
@compressCss("validationEngine.jquery,password_change,register_form")
@section('content')

	<div id="register-panel" class="{{@$type}}">
		<div id="register-panel-inner">
			<div class="register-panel-top">
				<div class="main-title-cheng-pass anti_alias FX">@if(@$type == "create") สร้างรหัสผ่าน @else [[password_change_header]] @endif</div>
				<div class="register-panel-top-left">
					<form id="register-form">
						<div class="input-panel display_name_panel">
							<div class="input-text FLighter">ชื่อผู้ใช้งาน</div>
							<input class="input-data FLighter" type="text" name="displayname" readonly value='[[member_web_display_name]]'>
						</div>
						<div class="input-panel email_panel">
							<div class="input-text FLighter">อีเมล</div>
							<input class="input-data FLighter" type="text" name="email" readonly value='[[member_web_email]]'>
						</div>
						<div class="input-panel password">
							<div class="input-text FLighter">@if(@$type == "create")รหัสผ่าน * (มีอายุการใช้งาน 90 วัน) @else รหัสผ่านใหม่ @endif</div>
							<input class="input-data FLighter validate[required, funcCall[checkValidatePassword]]" type="password" name="password" id="password" tabindex="3" autocomplete="off">
						</div>
						<div class="check-strength">
							<span class="text-strength FLighter">ระดับความปลอดภัยของรหัสผ่าน</span>
							<div class="clearfix"></div>
							<div id="strength-password">
								<div class="strength lv1"></div>
								<div class="strength lv2"></div>
								<div class="strength lv3"></div>
								<div class="strength lv4"></div>
								<div class="FLighter" id="checkpw"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="input-panel">
							<div class="input-text FLighter">@if(@$type == "create")ยืนยันรหัสผ่าน @else ยืนยันรหัสผ่านใหม่ @endif</div>
							<input class="input-data FLighter validate[required,equals[password]]" type="password" id="confirm-password" tabindex="4" autocomplete="off">
						</div>
						<span class="FLighter" id="checkmatch"></span>
					</form>
				</div>
				<div class="register-panel-top-right">
					<div class="register-panel-top-right-inner">
						<div class="guide-title-bar">
							<div class="icon-key"></div><span class="guide-title FXMed">คำแนะนำ</span>
						</div>
						<span class="FLighter description-list">
						@if(@$type == "create")
						ในการสมัครสมาชิกเว็บไซต์ เพื่อเข้าใช้งาน กรุณาสร้างรหัสผ่านตามคำแนะนำเพื่อความสมบูรณ์ของข้อมูลผู้ใช้งาน
						@else
							ในการสมัครสมาชิกเว็บไซต์ เพื่อเข้าใช้งาน กรุณากรอกรายละเอียดด้านซ้ายมือให้ครบถ้วน เพื่อความสมบูรณ์ของข้อมูลผู้ใช้งาน
						@endif

						</span>
						<ul class="guide-panel FLighter guide-panel-list">
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">รหัสผ่าน:</span><span class="subtitle-guide FLighter"> กรุณาระบุรหัสผ่าน โดยต้องมีความยาวไม่ต่ำกว่า 8 ตัวอักษรต้องมีตัวเลขอย่างน้อย 1 หมายเลข (0-9) และต้องมีอย่างน้อย 1 อักขระพิเศษ</span>
									@if(@$type == "create")<span class="hint">* (มีอายุการใช้งาน 90 วัน)</span>@endif
								</span>
							</li>
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">ยืนยันรหัสผ่าน:</span><span class="subtitle-guide FLighter"> ระบุรหัสผ่านอีกครั้ง เพื่อยืนยันรหัสผ่าน</span>
								</span>
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="panel-btn">
				<div class="btn-submit FLighter">ยืนยัน</div>
			</div>
		</div>
	</div>

@endsection
@compressJs("jquery.validationEngine,languages/jquery.validationEngine-th,password_check")