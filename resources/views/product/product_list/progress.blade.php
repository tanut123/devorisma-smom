<div class='footer_product'>
    <a href='{{$BASE_LANG}}product/progress'>
        <div class='footer_product_align progress lazyload' data-bgset="{{ $BASE_CDN }}/images/product/product_footer/footer_bg3.jpg" data-expand="+10">
            <div class='detail_panel'>
                <div class='footer_product_img_panel'>
                    <img class="lazyload" data-src="{{ $BASE_CDN }}/images/product/product_footer/s26_progress.png" data-expand="+10" width='122'>
                </div>
                <div class='footer_product_detail'>
                    <div class='footer_product_name'>

                        <span class='thin'>S-26</span> <span class='bold'>โปรเกรส</span>
                    </div>
                    <div class='footer_product_detail'>
                        สำหรับเด็ก 1 ขวบขึ้นไป
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>