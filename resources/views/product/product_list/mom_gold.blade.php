<div class='footer_product'>
    <a href='{{$BASE_LANG}}product/promama'>
        <div class='footer_product_align mom_gold lazyload' data-bgset="{{ $BASE_CDN }}/images/product/product_footer/footer_bg1.jpg" data-expand="+10">
            <div class='detail_panel'>
                <div class='footer_product_img_panel'>
                    <img class="lazyload" data-src="{{ $BASE_CDN }}/images/product/product_footer/footer_mom_gold.png" data-expand="+10" width='196'>
                </div>
                <div class='footer_product_detail'>
                    <div class='footer_product_name'>
                        <span class='thin'>S-26</span> <span class='bold'>โปรมาม่า</span>
                    </div>
                    <div class='footer_product_detail'>
                        สำหรับคุณแม่ตั้งครรภ์และให้นมบุตร
                    </div>
                </div>
            </div>
        </div>
    </a>
</div>