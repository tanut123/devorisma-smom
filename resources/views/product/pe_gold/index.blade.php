@extends('template.master')

@compressCss("slick,slick-theme,product.pe_gold,product.footer,bigvideo,home.banner")

@section('content')
<div id="product-panel">
	<div id="product-inner-panel">
		<article id="banner-mom" data-ratio="1.7">
			<div id="banner-slick">
				<div class="banner-list" data-delay='5'>
					<div class='image-loader-video' data-type='image' data-image_desktop="{{  asset($BASE_CDN . '/images/product/pe_gold/pe-gold-banner.jpg') }}" data-image_mobile="{{  asset($BASE_CDN . '/images/product/pe_gold/pe-gold-banner-mobile.jpg') }}"  style='display:none;'></div>
                    <div id='btnVideoPlay1' class='btnPlayVideo slide-banner-list' data-type='video' data-source="{{  asset($BASE_CDN . '/images/product/pe_gold/PE_GOLD.mp4') }}"></div>
                        <div class='play-control visible-xs' onClick='playPauseVideo();'>
                    </div>
				</div>
				{{--<div class='banner-list' data-delay='5'>
                    <div class='image-loader' data-type='image' data-image_desktop="{{  asset($BASE_CDN . '/images/product/pe_gold/pe-gold-banner.jpg') }}" data-image_mobile="{{  asset($BASE_CDN . '/images/product/pe_gold/pe-gold-banner-mobile.jpg') }}"  style='display:none;'></div>
				</div>--}}
				<div class="banner-list" data-delay='5'>
					<div class='image-loader-video' data-type='image' data-image_desktop="{{  asset($BASE_CDN . '/images/product/pe_gold/pe-gold-banner.jpg') }}" data-image_mobile="{{  asset($BASE_CDN . '/images/product/pe_gold/pe-gold-banner-mobile.jpg') }}"  style='display:none;'></div>
                    <div id='btnVideoPlay2' class='btnPlayVideo slide-banner-list' data-type='video' data-source="{{  asset($BASE_CDN . '/images/product/pe_gold/MASTER_S26_PEGold.mp4') }}"></div>
                        <div class='play-control visible-xs' onClick='playPauseVideo();'>
                    </div>
				</div>
			</div>
		</article>
		<article id="article-body">
			<article id="article-custom-page">
				<div class="article-custom-page-panel">
					<div class="custom-page-header">
						<img data-expand="+10" data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/title-custom-link-top.png') }}" class="img-top visible-xs lazyload">
						<img data-expand="+10" data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/title-custom-link-bottom.png') }}" class="visible-xs lazyload">
						<img data-expand="+10" data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/title-custom-link.png') }}" class="hidden-xs lazyload">
					</div>
					<div class="custom-page-body">
						<?php echo $custom_data_list; ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</article>
			<article id="article-momtip">
				<div class="article-momtip-panel">
					<div class="momtip-header">
						<img data-expand="+10" data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/title-momtip.png') }}" class="hidden-xs lazyload">
						<img data-expand="+10" data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/title-momtip-mobile.png') }}" class="visible-xs lazyload">
					</div>
					<div class="momtip-body">
							<?php echo $list_data; ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</article>
			<article id="article-grow">
				<div class="article-grow-panel">
					<div class="grow-header">
						<img data-expand="+10" data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/title-grow.png') }}" class="hidden-xs lazyload">
						<img data-expand="+10" data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/title-grow-mobile.png') }}" class="visible-xs lazyload">
					</div>
					<div class="grow-body">
						<img class="lazyload" data-expand="+10" data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/grow.png') }}">
					</div>
				</div>
			</article>
		</article>
		<article id="article-contact" class="lazyload" data-bgset="{{  asset($BASE_CDN . '/images/product/pe_gold/bg-article-contact-mobile.jpg') }} [--mb] | {{  asset($BASE_CDN . '/images/product/pe_gold/bg-article-contact.jpg') }}" data-expand="+10">
			<div class="article-contact-panel">
				<div class="main-title">
					<img data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/title-contact.png') }}" class="hidden-xs lazyload">
					<img data-expand="+10" data-src="{{  asset($BASE_CDN . '/images/product/pe_gold/title-contact-mobile.png') }}" class="visible-xs lazyload">
				</div>
				<div class="main-box">
					<div class="contact-box box-telphone">
						<a href="tel:026402288">
							<div class="contact-box-inner">
								<div class="icon telphone lazyload" data-bgset="{{ asset($BASE_CDN . '/images/product/pe_gold/icon-contact.png') }}" data-expand="+10"></div>
								<span class="contact-text FXregular">0 2640 2288 กด 1</span>
							</div>
						</a>
					</div>
					<div class="contact-box box-facebook">
						<a href="https://www.facebook.com/pickyeaterclub" target="_blank">
							<div class="contact-box-inner">
								<div class="icon facebook lazyload" data-bgset="{{ asset($BASE_CDN . '/images/product/pe_gold/icon-contact.png') }}" data-expand="+10"></div>
								<span class="contact-text FXregular">Facebook.com/<br class="visible-xs">PECLUB</span>
							</div>
						</a>
					</div>
					<div class="contact-box">
						<a href="http://line.me/ti/p/@PECLUB" target="_blank">
							<div class="contact-box-inner">
								<div class="icon line lazyload" data-bgset="{{ asset($BASE_CDN . '/images/product/pe_gold/icon-contact.png') }}" data-expand="+10"></div>
								<span class="contact-text FXregular">@PECLUB</span>
							</div>
						</a>
					</div>
				</div>
			</div>
		</article>
		<article id="article-information">
			<div class="article-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/product/pe_gold/bg-product-mobile.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/product/pe_gold/bg-product-main.jpg') }}">
				<div class="article-inner-panel">
					<ul id="menu-pe-gold">
						<li id="data1" class="menu-list">
							<div class="menu-list-text FXregular">
								ข้อมูลโภชนาการ
							</div>
						</li>
						<li id="data2" class="menu-list menu-list-middle">
							<div class="menu-list-text FXregular">
								วิธีเตรียมนม
							</div>
						</li>
						<li id="data3" class="menu-list">
							<div class="menu-list-text FXregular">
								การเก็บรักษา
							</div>
						</li>
						<li class="clearfix"></li>
					</ul>
					<div id="article-information-panel">
						<div class="panel-main">
							<img data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/pe_gold/img-product-main1.png') }}" class="img-main1 lazyload">
							<div class="title-box">
								<div class="title-txt">
									<span class="color-blue FXregular text1">S-26</span><span class="color-gold FLighter text2"> พีอี โกลด์</span><br><span class="color-blue FLighter text3">สูตร แอดวานซ์ 9+9</span>
								</div>
								<div class="subtitle-txt">
									<span class="color-gold FXregular">สำหรับเด็กอายุ1ปีขึ้นไปและทุกคนในครอบครัว<br>มี 9 สารอาหารสำคัญ + 9 สารอาหารเพิ่มเติม</span>
								</div>
								<img data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/pe_gold/img-product-main2.png') }}" class="img-main2 lazyload">
							</div>
							<div class="clearfix"></div>
							<img data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/pe_gold/img-product-main3.png') }}" class="img-main3 lazyload">
							<div class="desc-box">
								<span class="desc-txt color-gold FXregular">คุณแม่ระหว่างตั้งครรภ์และให้นมบุตร มีความต้องการ<br class="hidden-xs">ทางโภชนาการที่เพิ่มมากขึ้น จึงจำเป็นต้องได้รับอาหาร<br class="hidden-xs">และโภชนาการที่ดี เพื่อให้ลูกรักเติบโต มีพัฒนาการ<br class="hidden-xs">เต็มศักยภาพ คุณแม่จำเป็นต้องใส่ใจเลือกสรรอาหาร<br class="hidden-xs">ที่มีประโยชน์มีคุณค่าตามหลักโภชนาการครบถ้วน<br class="hidden-xs">สมบูรณ์เพื่อให้เพียงพอต่อตนเองและลูกรัก</span>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</article>
	</div>
</div>
<div class="clearfix"></div>
<script>
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.customMedia = {
        '--mb': '(max-width: 767px)'
    };
</script>
@endsection

@compressJs("lazy.bgset.min,lazysizes.min,slick.min,product,product.footer,favorite,video_plugin,bigvideo,jquery.tubular.1.0,home.banner")