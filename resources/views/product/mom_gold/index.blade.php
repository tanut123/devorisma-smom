@extends('template.master')

@compressCss("slick,slick-theme,product.momgold,product.footer")

@section('content')
	<div id="wapMomGold">
		
		<div style="position: absolute;top:0;left:-100%;height: 1px;width: 1px;text-indent: -9999px;">
				<h1>S-26 โปรมาม่า</h1>
				<h2>สูตรใหม่! ไขมันต่ำ ผสมใยอาหาร</h2>
				<p>
					สารอาหารของคุณแม่ในช่วงมีครรภ์และให้นมบุตรนั้น ส่งผลต่อสุขภาพของคุณแม่ โภชนาการที่ดีในระหว่างการให้นมบุตรนั้นมีผลต่อคุณภาพน้ำนมแม่ฆอ.671/2562

				</p>

				<h2>ข้อมูลโภชนาการ S-26 โปรมาม่า สูตรใหม่ ไขมันต่ำ</h2>
				<h3>ในนม 1 แก้ว (240มล.) มีสารอาหารดังนี้</h3>
				<p>สฟิงโกไมอีลิน 24.7 มก. | เพิ่มปริมาณดีเอชเอ 72%*แอลฟา-แล็คตัลบูมิน 528 มก. | โคลีน และโฟเลตสูง | ผสมใยอาหารจากธรรมชาติ</p>

				<h3>มี โอโอดีน</h3>
				<p>เป็นส่วนประกอบที่สำคัญของฮอร์โมนไทรอยด์ซึ่งมีหน้าที่ควบคุมการเจริญเติบโตและการพัฒนาของร่างกายและสมอง</p>

				<h3>มี แมกนีเซียม</h3>
				<p>เป็นส่วนประกอบของกระดูกและฟันและช่วยในการทำงานของระบบประสาทและกล้ามเนื้อ</p>

				<h3>แมงกานีส สูง</h3>
				<p>มีส่วนร่วมในการทำงานของเอนไซม์หลายกลุ่มในร่างกาย</p>

				<h3>สังกะสี สูง</h3>
				<p>ช่วยในการเจริญเติบโตของร่างกาย</p>

				<h3>คลอไรด์ สูง</h3>
				<p>ร่วมกับสารอื่นในการรักษาสมดุลของกรดด่างในร่างกาย</p>

				<h3>กรดแพนโทธินิค สูง</h3>
				<p>ช่วยในการใช้ประโยชน์ของไขมันและคาร์โบไฮเดรต</p>

				<h3>ไนอะซิน สูง</h3>
				<p>ช่วยให้เยื่อบุทางเดินอาหารและผิวหนังอยู่ในสภาพปกติ</p>

				<h3>มี โปรตีน</h3>
				<p>ให้กรดอะมิโนที่จำเป็นต่อการสร้างโปรตีนชนิดต่างๆ ในร่างกาย</p>

				
				<p>ควรกินอาหารหลากหลายครบ 5 หมู่ ในสัดส่วนที่เหมาะสมเป็นประจำ</p>
				<p>*ในผลิตภัณฑ์ 1 แก้ว สูตรเดิมมี ดีเอชเอ  19 มก. สูตรใหม่มี ดีเอชเอ 32.8 มก. มี สฟิงโกไมอีลิน 24.7 มก., แอลฟา-แล็คตัลบูมิน 528 มก., โคลีน 173.9 มก. และ ใยอาหารจากธรรมชาติ 2.87 ก.ฆอ.671/2562</p>
				<h2>วิธีเตรียมนม S-26 โปรมาม่า สูตรใหม่</h2>
				<p>ล้างมือ ล้างถ้วยหรือแก้ว ก่อนเตรียมผลิตภัณฑ์เตรียมน้ำต้มสุกอุ่น (ต้มน้ำให้เดือด 10 นาที แล้วทิ้งให้น้ำอุ่น)ใช้ช้อนตวงที่ให้มาในกล่องเท่านั้น ตวงผลิตภัณฑ์ให้มีปริมาณที่ถูกต้อง ปาดผลิตภัณฑ์ให้เรียบเสมอขอบช้อนตวง เทผลิตภัณฑ์ลงในถ้วยหรือแก้ว ตามสัดส่วนด้านล่าง</p>
				<p>ใช้ช้อนคนในถ้วยหรือแก้วให้ผลิตภัณฑ์ละลายจนหมด</p>
				<p>* 1 ช้อนตวง = 8.24 กรัม ผสมผลิตภัณฑ์ 6 ช้อนตวง ต่อนํ้าต้มสุกอุ่น 210 มิลลิลิตร จะได้นม 1 แก้ว ปริมาณ 240 มิลลิลิตร</p>
				<p>* นมที่เหลือจากการบริโภคในแต่ละครั้งต้องเททิ้งไป</p>

				<h3>การเก็บรักษานม S-26 โปรมาม่า</h3>
				<p>ถุงอลูมิเนียมที่ยังไม่ได้เปิดใช้ให้เก็บไว้ในที่แห้งและเย็น ถุงที่เปิดใช้แล้ว ต้องพับปากถุงให้ปิดสนิทหลังจากใช้ทุกครั้ง เพื่อให้นมผงใหม่เสมอ และเก็บไว้ที่แห้งและเย็น</p>
				<p>ควรใช้ให้หมดภายใน 4 สัปดาห์ หลังจากเปิดถุงแล้ว หลีกเลี่ยงการเก็บในที่อุณหภูมิร้อนจัด หรือเย็นจัดเป็นระยะเวลานาน</p>

			</div>
		    <div class="hidden-xs">
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-desktop_01.png') }}" data-expand="+10" />
	            </div>
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-desktop_02.png') }}" data-expand="+10" />
	            </div>
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-desktop_03.png') }}" data-expand="+10" />
	            </div>
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-desktop_04.png') }}" data-expand="+10" />
	            </div>
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-desktop_05.png') }}" data-expand="+10" />
	            </div>
	           
	        </div>
	        <div class="visible-xs">
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-mobile_01.jpg') }}" style="width: 100%" data-expand="+10" />
	            </div>
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-mobile_02.jpg') }}" style="width: 100%" data-expand="+10" />
	            </div>
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-mobile_03.jpg') }}" style="width: 100%" data-expand="+10" />
	            </div>
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-mobile_04.jpg') }}" style="width: 100%" data-expand="+10" />
	            </div>
	            <div>
	                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/mom_gold/detail/detail-mobile_05.jpg') }}" style="width: 100%" data-expand="+10" />
	            </div>
	          
	        </div>
		
		<article id="article-momtip" class="lazyload" data-bgset="{{ asset($BASE_CDN . '/images/home/trick-mom/bg-trick-mom.jpg') }}" data-expand="+10">
			<div class="article-momtip-panel">
				<div class="momtip-header">
					<span class="momtip-header-left">
						<span class="FXregular">เคล็ดลับ</span>คุณแม่
					</span>
					<span class="momtip-header-right FLighter">
						หลากหลายเคล็ดลับและความรู้จาก S-MomClub สำหรับคุณแม่ เพื่อดูแลลูกน้อย
						และรับมือกับปัญหาที่พบในระหว่างการเลี้ยงดู
					</span>
					<div class="clearfix"></div>
				</div>
				<div class="momtip-body">
						<?php echo $list_data; ?>
				</div>
			</div>
		</article>
	</div>
<div class="clearfix"></div>
<div class='product_footer_slick'>
@include('product.product_list.progress_gold')
@include('product.product_list.progress')
@include('product.product_list.organic')
</div>
<script>
	window.lazySizesConfig = window.lazySizesConfig || {};
	window.lazySizesConfig.customMedia = {
    	'--mb': '(max-width: 767px)'
	};
</script>

@endsection

@compressJs("lazy.bgset.min,lazysizes.min,slick.min,product.footer,favorite,product")