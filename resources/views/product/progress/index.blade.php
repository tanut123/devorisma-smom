@extends('template.master')

@compressCss("slick,slick-theme,product.progress,product.footer")

@section('content')
<div id="product-panel" class="FX">
	<div id="banner-product" class="-warp-banner lazyload" data-bgset="{{ asset($BASE_CDN . '/images/product/progress/bg-banner-xs.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/product/progress/bg-banner.jpg') }}" data-expand="+10">
		<div class="banner-inner">
			<div class="-txt-title">
				<img class="lazyload img-title" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/progress/txt_head.png') }}" alt="เติบโตแข็งแรง เพิ่มโอกาสเรียนรู้"/>
			</div>
			<div class="-image-product-hl">
				<img class="lazyload img-product-s26" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/progress/product-s26-progress.png') }}" alt="เอส 26 โปรเกรส"/>				
			</div>
			<div class="warp-small-desc hidden-xs">				
				<div class="small-desc">
					<div class="col-left">
						<p class="-txt-product-detail">
							เอส-26 โปรเกรส 360 สมาร์ท แคร์ 3 พลิตภัณฑ์นมกลิ่นวานิลลา รสจืด โภชนาการจาก <br class="hidden-xs">
							สารอาหารที่มีประโยชน์ต่อร่างกาย ควรกินอาหารหลากหลายครบ 5 หมู่ ในสัดส่วนที่เหมาะสมเป็นประจํา <br class="hidden-xs">
							*ดีเอชเอ 4.32 มก. และเออาร์เอ 6.24 มก. ต่อแก้ว โอเมก้า 3 134 มก., โอเมก้า 6 1,344 มก. และโอเมก้า 9 2,302 มก. <br class="hidden-xs">
							ต่อแก้ว นิวคลีโอไทด์ 5 ชนิด 6.24 มก. ต่อแก้ว ลูทีน 48 มคก. ต่อแก้ว โคลีน 48 มก. ต่อแก้ว กลุ่มแคโรทีน 50 มคก. ต่อแก้ว
						</p>
					</div>
					<div class="col-right">
						<p class="-txt-mog FXBold">
							ฆอ. 2587/2561
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="warp-small-desc visible-xs">				
		<div class="small-desc">
			<div class="col-left">
				<p class="-txt-product-detail">
					เอส-26 โปรเกรส 360 สมาร์ท แคร์ 3 พลิตภัณฑ์นมกลิ่นวานิลลา รสจืด โภชนาการจาก <br class="hidden-xs">
					สารอาหารที่มีประโยชน์ต่อร่างกาย ควรกินอาหารหลากหลายครบ 5 หมู่ ในสัดส่วนที่เหมาะสมเป็นประจํา <br class="hidden-xs">
					*ดีเอชเอ 4.32 มก. และเออาร์เอ 6.24 มก. ต่อแก้ว โอเมก้า 3 134 มก., โอเมก้า 6 1,344 มก. และโอเมก้า 9 2,302 มก. <br class="hidden-xs">
					ต่อแก้ว นิวคลีโอไทด์ 5 ชนิด 6.24 มก. ต่อแก้ว ลูทีน 48 มคก. ต่อแก้ว โคลีน 48 มก. ต่อแก้ว กลุ่มแคโรทีน 50 มคก. ต่อแก้ว
				</p>
			</div>
			<div class="col-right">
				<p class="-txt-mog FXBold">
					ฆอ. 2587/2561
				</p>
			</div>
		</div>
	</div>
	<div id="content-panel">
		<div class="content-inner">
			<article id="info">
				<div class="info-panel">
					<div class="info-images-desc">
						<img class="lazyload info-image hidden-xs" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/progress/S-26-progress-info.png') }}" alt="เอส-26 โปรเกรส 360 สมาร์ท แคร์ 3">
						<img class="lazyload info-image visible-xs" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/progress/S-26-progress-info-mobile.png') }}" alt="เอส-26 โปรเกรส 360 สมาร์ท แคร์ 3">
					</div>
					<div class="info-desc FXBold">
						นมพงปรุงแต่ง<br class="hidden-xs"/>
						กลิ่นวานิลลา รสจืด<br/>
						เครื่องหมายการค้า<br class="hidden-xs"/>
						เอส-26 <sup>&#174;</sup> โปรเกรส<br/>
						360&#176; สมาร์ท แคร์
					</div>
				</div>
			</article>
			<article id="article-btn">
				<div class="article-panel">
					<div class="article-inner-panel">
						<div class="title-box">
							<img class="lazyload title-image" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/progress/title-info.png') }}" alt="ผลิตภัณฑ์นมกลิ่นวานิลลา">
							<div class="title FXBold">
								ผลิตภัณฑ์นมกลิ่นวานิลลา รสจืด เอส-26<sup>&#174;</sup> โปรเกรส 360&#176; สมาร์ท แคร์ 3<br class="hidden-xs" />
								ให้คุณค่าทางโภชนาการจากสารอาหารที่มีประโยชน์ต่อร่างกาย
							</div>
							<div class="desc">
								เพราะลูกน้อยในวัยนี้กำลังเจริญเติบโต คุณพ่อคุณแม่จึงควรดูแลอย่างใกล้ชิด เพื่อให้ลูกได้รับสารอาหารที่มีคุณค่าทางโภชนาการอย่างเหมาะสม เพื่อช่วยเสริมสร้างพัฒนาการลูกน้อย
							</div>
						</div>
						<div id="menu-pe-gold">
							<div class="btn-list lazyload" data-bgset="{{ asset($BASE_CDN . '/images/product/mom_gold/bg-btn.png') }}" data-expand="+10">
								<a href="{{ $BASE_LANG }}product/progress#article-1" class="btn-inner FXBold">ข้อมูลโภชนาการ</a>
							</div>
							<div class="btn-list lazyload" data-bgset="{{ asset($BASE_CDN . '/images/product/mom_gold/bg-btn.png') }}" data-expand="+10">
								<a href="{{ $BASE_LANG }}product/progress#article-2" class="btn-inner FXBold">การเก็บรักษา</a>
							</div>
							<div class="btn-list lazyload" data-bgset="{{ asset($BASE_CDN . '/images/product/mom_gold/bg-btn.png') }}" data-expand="+10">
								<a href="{{ $BASE_LANG }}product/progress#article-momtip" class="btn-inner FXBold">เคล็ดลับคุณแม่</a>
							</div>
						</div>
					</div>
				</div>
			</article>
			<article id="article-1" class="content">
				<div class="inner">
					<div class="title-desc-product">
						<img class="lazyload title-desc" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/progress/title_detail.png') }}" alt="s-26 โปรเกรส สูตรใหม่">
					</div>
					<h2 class="-txt-sub-detail">ในนม 1 แก้ว (ปริมาณ 240 มล.) ให้สารอาหารดังนี้</h2>
					<div class="warp-table-detail">
						<div class="-table-detail">
							<div class="desc-left">
								<div class="box_content_desc">
									<p class="-warp-list nopadtop">
										<span class="-list">											
											<span class="-title-txt -inline FXBold">เออาร์เอ</span> 6.24 มิลลิกรัม
										</span>
										<span class="-list">											
											<span class="-title-txt -inline FXBold">โคลีน</span> 48 มิลลิกรัม
										</span>
										<span class="-list">											
											<span class="-title-txt -inline FXBold">ลูทีน</span> 48 ไมโครกรัม
										</span>
										<span class="-list">											
											<span class="-title-txt -inline FXBold">กลุ่มแคโรทีน</span> 50 ไมโครกรัม
										</span>
										<span class="-list">											
											<span class="-title-txt -inline FXBold">นิวคลีโอไทด์</span> 5 ชนิด 6.24 มิลลิกรัม
										</span>
										<span class="-list">											
											<span class="-title-txt -inline FXBold">ผสมใยอาหารจากธรรมชาติ</span>
										</span>
									</p>
								</div>
								<div class="box_content_desc">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">มีวิตามินเอ</span> ช่วยในการมองเห็น
										</span>										
									</p>
								</div>
								<div class="box_content_desc">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">วิตามินบี 12 สูง</span> 
											มีส่วนช่วยในการ <br class="hidden-xs">
											ทํางานของระบบประสาท <br class="hidden-xs">
											และสมอง
										</span>										
									</p>
								</div>
								<div class="box_content_desc">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">มีวิตามินบี 2</span> 
											ช่วยให้ร่างกายได้พลังงาน <br class="hidden-xs">
											จากคาร์บไฮเดรต โปรตีน <br class="hidden-xs">
											และไขมัน
										</span>										
									</p>
								</div>
								<div class="box_content_desc">
									<p class="-warp-list nopadbottom padding-xs">
										<span class="-list">											
											<span class="-title-txt FXBold">วิตามินซีสูง และมีวิตามินอี</span> 
											มีส่วนช่วยในกระบวนการต่อต้านอนุมูลอิสระ
										</span>										
									</p>
								</div>
								<div class="box_content_desc paddingLeft visible-xs">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">มีวิตามินเค</span> 
											ช่วยสร้างสารที่ทําให้เกิด <br class="hidden-xs">
											การแข็งตัวของเลือด
										</span>										
									</p>
								</div>
								<div class="box_content_desc paddingLeft visible-xs">
									<p class="-warp-list nopadbottom">
										<span class="-list">											
											<span class="-title-txt FXBold">มีไอโอดีน</span> 											 
											เป็นส่วนประกอบ <br class="hidden-xs">
											ที่สําคัญของฮอร์โมนไทรอยด์ <br class="hidden-xs">
											ซึ่งมีหน้าที่ควบคุมการเจริญเติบโต <br class="hidden-xs">
											และการพัฒนาของร่างกายและสมอง
										</span>										
									</p>
								</div>
							</div>
							<div class="desc-right">
								<div class="img-glass">
									<img class="lazyload glass-desc" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/progress/glass_milk.png') }}" alt="s-26">
								</div>
								<div class="box_content_desc paddingLeft hidden-xs">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">มีวิตามินเค</span> 
											ช่วยสร้างสารที่ทําให้เกิด <br class="hidden-xs">
											การแข็งตัวของเลือด
										</span>										
									</p>
								</div>
								<div class="box_content_desc paddingLeft hidden-xs">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">มีไอโอดีน</span> 											 
											เป็นส่วนประกอบ <br class="hidden-xs">
											ที่สําคัญของฮอร์โมนไทรอยด์ <br class="hidden-xs">
											ซึ่งมีหน้าที่ควบคุมการเจริญเติบโต <br class="hidden-xs">
											และการพัฒนาของร่างกายและสมอง
										</span>										
									</p>
								</div>
								<div class="box_content_desc">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">มีกรดแพนโทธินิค</span> 
											ช่วยในการใช้ประโยชน์ <br class="hidden-xs">
											ของไขมันและคาร์โบไฮเดรต
										</span>										
									</p>
								</div> 
								<div class="box_content_desc">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">โฟเลตสูง</span> 
											มีส่วนสําคัญในการสร้างเม็ดเลือดแดง
										</span>										
									</p>
								</div> 
								<div class="box_content_desc">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">มีเหล็ก</span> 
											เป็นส่วนประกอบสําคัญของ <br class="hidden-xs">
											ฮีโมโกลบินในเม็ดเลือดแดง
										</span>
									</p>
								</div>
								<div class="box_content_desc">
									<p class="-warp-list">
										<span class="-list">											
											<span class="-title-txt FXBold">แคลเซียมสูง</span> 
											และมีฟอสฟอรัส มีส่วนช่วยในกระบวนการ <br class="hidden-xs">
											สร้างกระดูกและฟันที่แข็งแรง
										</span>										
									</p>
								</div> 
								<div class="box_content_desc">
									<p class="-warp-list nopadbottom">
										<span class="-list">											
											<span class="-title-txt FXBold">วิตามินดีสูง</span> 
											ช่วยดูดซึมแคลเซียม และฟอสฟอรัส
										</span>										
									</p>
								</div> 
							</div>
						</div>
					</div>
					<div class="detail-bottom">
						<div class="detail-text left">
							ควรกินอาหารหลากหลายครบ 5 หมู่ ในสัดส่วนที่เหมาะสมเป็นประจํา
							<span class="bottom-list nonePadding-xs">
								<span class="bottom-column">*</span>
								<span class="bottom-column-r">สูตรเดิมไม่ได้พสมดีเอชเอ</span>
							</span>
							<span class="bottom-list nonePadding-xs">
								<span class="bottom-column">**</span>
								<span class="bottom-column-r">โอเมก้า3 134 มก., โอเมก้า6 1,344 มก.,</span>
							</span>
							<span class="bottom-list nonePadding-xs">
								<span class="bottom-column"></span>
								<span class="bottom-column-r">โอเมก้า9 2,302 มก. ต่อแก้ว (240 มล.)</span>
							</span>							
						</div>
						<div class="detail-text right FXBold">ฆอ. 2587/2561</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</article>
			<article id="article-2" class="content">
				<div class="inner">
					<div class="title-desc-save">
						<img class="lazyload title-save" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/progress/title_save.png') }}" alt="การเก็บรักษานม">
					</div>
					<div class="-warp-save">
						<div class="-table-save">
							<div class="-save-l">							
								<span class="bottom-list">
									<span class="bottom-column">•</span>
									<span class="bottom-column-r">ทุกครั้งหลังการใช พับปากของใหปดสนิท ใสในภาชนะ <br class="hidden-xs"/>
									ที่มีฝาปดสนิท เก็บในที่เเหงเเละเย็น</span>
								</span>
								<span class="bottom-list">
									<span class="bottom-column">•</span>
									<span class="bottom-column-r">ควรใชใหหมดภายใน 4 สัปดาห หลังจากเปดซองแลว</span>
								</span>	
								<span class="bottom-list">
									<span class="bottom-column">•</span>
									<span class="bottom-column-r">หลีกเลี่ยงการเก็บในที่อุณหภูมิรอนจัด หรือเย็นจัดเปนระยะ</span>
								</span>	
							</div>
							<div class="-save-r">
								<div class="img-save-milk">
									<img class="lazyload -save-milk-img" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/product/progress/save_milk.png') }}" alt="การเก็บรักษานม">
								</div>
							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
	</div>
	<article id="article-momtip">
		<div class="article-momtip-panel">
			<div class="momtip-header">
				<span class="momtip-header-left">
					<span class="FXregular">เคล็ดลับ</span>คุณแม่
				</span>
				<span class="momtip-header-right FLighter">
					หลากหลายเคล็ดลับและความรู้จาก S-MomClub สำหรับคุณแม่ เพื่อดูแลลูกน้อย
					และรับมือกับปัญหาที่พบในระหว่างการเลี้ยงดู
				</span>
				<div class="clearfix"></div>
			</div>
			<div class="momtip-body">
					<?php echo $list_data; ?>
			</div>
		</div>
	</article>
</div>
<div class="clearfix"></div>
<div class='product_footer_slick'>
@include('product.product_list.mom_gold')
@include('product.product_list.progress_gold')
@include('product.product_list.organic')
</div>
<script>
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.customMedia = {
        '--mb': '(max-width: 767px)'
    };
</script>
@endsection

@compressJs("lazy.bgset.min,lazysizes.min,slick.min,product.footer,favorite,product")