@extends('template.master')

@compressCss("slick,slick-theme,product.progress_gold,product.footer")

@section('content')
<div id='s26_progress_gold'>

    <div style="position: absolute;top:0;left:-100%;height: 1px;width: 1px;text-indent: -9999px;">
                <h1>S-26 โกลด์ โปรเกรส</h1>
                <h2>สูตรใหม่! มีฟอสโฟไลปิด เพิ่ม สฟิงโกไมอีลิน</h2>
                <p>
                    ในโลกปัจจุบันมีอะไรใหม่ใหม่ให้ลูกเรียนรู้ได้ตลอดเวลา คุณพ่อคุณแม่ควรสนับสนุนให้ลูกได้เรียนรู้จากสิ่งรอบตัวและให้ลูกได้รับสารอาหารที่มีคุณค่าทางโภชนาการ พร้อมออกกำลังกายและนอนหลับอย่างเหมาะสมตามช่วงวัย
ฆอ.629/2562


                </p>

                <h2>ข้อมูลโภชนาการ S-26 โกลด์ โปรเกรส</h2>
                <h3>สูตรใหม่ เพิ่มสฟิงโกไมอีลิน</h3>
                <p>[1] สฟิงโกไมอีลิน เป็นส่วนหนึ่งของฟอสโฟไลปิด
* ในนม 1 แก้ว; สูตรเดิมมี สฟิงโกไมอีลิน 11,400 มคก., แอลฟา-แล็คตัลบูมิน 270 มก., ดีเอชเอ 23.2 มก., 
โคลีน 57.5 มก., ลูทีน 67 มคก., ใยอาหารจากธรรมชาติ น้อยกว่า 1 ก. (0.72 ก.) ; 
</p>
<p>สูตรใหม่ มี ฟอสโฟไลปิด 72 มก., สฟิงโกไมอีลิน 14,400 มคก., แอลฟา-แล็คตัลบูมิน 326 มก., ดีเอชเอ 23.68 มก., โคลีน 58.6 มก., ลูทีน 68 มคก., ใยอาหารจากธรรมชาติ 1 ก.
ฆอ.629/2562</p>

                <h3>ในนม 1 แก้ว (240มล.) มีสารอาหารดังนี้</h3>
                <p>มี ฟอสโฟไลปิด | เพิ่ม สฟิงโกไมอีลินเพิ่ม แอลฟา-แล็คตัลบูมิน | เพิ่ม ดีเอชเอ เพิ่ม โคลีน | เพิ่ม ลูทีน
ผสมใยอาหารจากธรรมชาติ เพิ่มขึ้น
</p>

                <h3>มี วิตามินบี 1 </h3>
                <p>มีส่วนช่วยในการทำงานของระบบประสาทและกล้ามเนื้อ</p>

                <h3>มี ไอโอดีน</h3>
                <p>เป็นส่วนประกอบสำคัญvองฮีโมโกลบินในเม็ดเลือดแดง</p>

                <h3>มี ธาตุเหล็ก</h3>
                <p>ช่วยในการเจริญเติบโตของร่างกาย</p>

                <h3>แคลเซียม สูง และ </h3>
                <p>มี ฟอสฟอรัส มีส่วนช่วยในกระบวนการสร้างกระดูกและฟันที่แข็งแรง</p>

                <h3>กรดแพนโทธินิค สูง</h3>
                <p>ช่วยในการใช้ประโยชน์ของไขมันและคาร์โบไฮเดรต</p>

                <h3>โฟเลต สูง</h3>
                <p>มีส่วนสำคัญในการสร้างเม็ดเลือดแดง</p>

                <h3>วิตามินดี สูง </h3>
                <p>ช่วยดูดซึมแคลเซียมและฟอสฟอรัส</p>

                <h3>มี วิตามินเค </h3>
                <p>ช่วยลดการสลายแคลเซียมทำให้กระดูกแข็งแรง</p>

                <h3>มี วิตามินเอ ช่วยในการมองเห็น</h3>
                <p>วิตามินซี สูง และ มี วิตามินอีมีส่วนช่วยในกระบวนการต่อต้านอนุมูลอิสระ</p>
                <p>ควรกินอาหารหลากหลายครบ 5 หมู่ในสัดส่วนที่เหมาะสมเป็นประจำ
ฆอ.629/2562
</p>

                <h3>วิธีเตรียมนม S-26 โกลด์ โปรเกรส สูตรใหม่</h3>
                <p>1 ช้อน : น้ำ 1 ออนซ์ (30 มล.)</p>
                <p>
                    •ล้างมือ ล้างถ้วยหรือแก้ว ก่อนเตรียมผลิตภัณฑ์<br/>
                    •เตรียมน้ำต้มสุกอุ่น (ต้มน้ำให้เดือด 10 นาที แล้วทิ้งให้น้ำอุ่น)<br/>
                    •ใช้ช้อนตวงที่ให้มาในกล่องเท่านั้น<br/>
                    •ตวงผลิตภัณฑ์ให้มีปริมาณที่ถูกต้อง ปาดผลิตภัณฑ์ให้เรียบ เสมอขอบช้อนตวง<br/>
                    •เทผลิตภัณฑ์ลงในถ้วยหรือแก้ว ตามสัดส่วนด้านล่าง<br/>
                    •ใช้ช้อนคนในถ้วยหรือแก้วให้ผลิตภัณฑ์ละลายจนหมด<br/>
                </p>

                <h3>ข้อควรระวัง</h3>
                <p>
                    • ควรเตรียมผลิตภัณฑ์เพียงครั้งละ 1 แก้ว เพื่อดื่มทันที และ
                      ปฏิบัติตามอย่างเคร่งครัด<br/>
                    • หากเด็กดื่มผลิตภัณฑ์ที่ผสมแล้วไม่หมดให้ทิ้งผลิตภัณฑ์
                      ที่เหลือทั้งหมด

                </p>

                <p>• การปล่อยเด็กให้ดื่มผลิตภัณฑ์ตามลำพังอาจทําให้เด็กสําลักได้</p>
                <p>* 1 ช้อนตวง = 6 กรัม ผสมผลิตภัณฑ์ 7 ช้อนตวง ต่อนํ้าต้มสุกอุ่น 210 มิลลิลิตรจะได้นม 1 แก้ว ปริมาณ 240 มิลลิลิตร</p>
                <p>* การใช้ผลิตภัณฑ์มากหรือน้อยกว่าท่ีกําหนดจะทําให้เด็กเกิดภาวะการขาดน้ำ หรือได้รับสารอาหาร ไม่เพียงพอ ไม่ควรเปลี่ยนสัดส่วนการผสมผลิตภัณฑ์โดยมิได้รับคําแนะนําจากแพทย์</p>


                <h3>การเก็บรักษานม S-26 โกลด์ โปรเกรส</h3>
                <p>•ถุงที่เปิดใช้แล้ว ต้องพับปากถุงให้ปิดสนิทหลังจากใช้ทุกครั้ง เพื่อให้นมผงใหม่เสมอ และเก็บไว้ที่แห้งและเย็น</p>
                <p>•ควรใช้ให้หมดภายใน 4 สัปดาห์ หลังจากเปิดถุงแล้ว หลีกเลี่ยงการเก็บในที่อุณหภูมิร้อนจัด หรือเย็นจัดเป็นระยะเวลานาน</p>

            </div>

    <div class="hidden-xs">
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-d_01.png') }}" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-d_02.png') }}" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-d_03.png') }}" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-d_04.png') }}" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-d_05.png') }}" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-d_06.png') }}" data-expand="+10" />
            </div>
        </div>
        <div class="visible-xs">
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-m_01.png') }}" style="width: 100%" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-m_02.png') }}" style="width: 100%" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-m_03.png') }}" style="width: 100%" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-m_04.png') }}" style="width: 100%" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-m_05.png') }}" style="width: 100%" data-expand="+10" />
            </div>
            <div>
                <img class="img-responsive img-center lazyload" data-src="{{ asset($BASE_CDN . '/images/product/progress_gold/detail/detail-m_06.png') }}" style="width: 100%" data-expand="+10" />
            </div>
            
        </div>
  
        <article id="article-momtip" class="lazyload" data-expand="+10" data-bgset="{{ asset($BASE_CDN . '/images/home/trick-mom/bg-trick-mom.jpg') }}">
			<div class="article-momtip-panel">
				<div class="momtip-header">
					<span class="momtip-header-left">
						<span class="FXregular">เคล็ดลับ</span>คุณแม่
					</span>
					<span class="momtip-header-right FLighter">
						หลากหลายเคล็ดลับและความรู้จาก S-MomClub สำหรับคุณแม่ เพื่อดูแลลูกน้อย
						และรับมือกับปัญหาที่พบในระหว่างการเลี้ยงดู
					</span>
					<div class="clearfix"></div>
				</div>
				<div class="momtip-body">
						<?php echo $list_data; ?>
				</div>
			</div>
		</article>
    
                
                
                
                

</div>
<div class="clearfix"></div>
<div class='product_footer_slick'>
@include('product.product_list.mom_gold')
@include('product.product_list.progress')
@include('product.product_list.organic')
</div>

<script>
    window.lazySizesConfig = window.lazySizesConfig || {};
    window.lazySizesConfig.customMedia = {
        '--mb': '(max-width: 767px)'
    };
</script>
@endsection

@compressJs("lazy.bgset.min,lazysizes.min,slick.min,jquery.rwdImageMaps.min,product.footer,favorite,product,product.progress_gold")