@extends('template.mommysurvey')
@compressCss("layout_survey")
@section('content')
   <?php

   	// page 1 new
   	$status_user = array();
   	$status_user[0] = "ตั้งครรภ์";
   	$status_user[1] = "มีบุตร";

   	$age_user = array();
   	$age_user[0] = "13-17 ปี";
   	$age_user[1] = "18-24 ปี";
   	$age_user[2] = "25-34 ปี";
   	$age_user[3] = "35-44 ปี";
   	$age_user[4] = "มากกว่า 44 ปี";

   	$living_user = array();
   	$living_user[0] = "กรุงเทพและปริมณฑล";
   	$living_user[1] = "ภาคเหนือ";
   	$living_user[2] = "ภาคกลาง";
   	$living_user[3] = "ภาคใต้";
   	$living_user[4] = "ภาคตะวันออก";
   	$living_user[5] = "ภาคตะวันออกเฉียงเหนือ";
   	$living_user[6] = "ภาคตะวันตก";

	$information_from = array();
	$information_from[0] = "คุณหมอ, พยาบาล, บุคลากรทางการแพทย์";
	$information_from[1] = "เว็บไซต์, เฟซบุ๊ก, เพจต่างๆ";
	$information_from[2] = "โฆษณา, สื่อประชาสัมพันธ์";
	$information_from[3] = "พนักงานขาย (PC), ร้านค้า";
	$information_from[4] = "พ่อแม่, ญาติ, เพื่อน คนคุ้นเคย แนะนำ";

	srand( (double)microtime() * 10000000 );
	$n = 0;
    while ( $n < 5 ){
        while ( true ) {
            $c = rand() % 5;
            if ( empty( $ans[$c] ) ){

                $ans[$c] = $information_from[$n];

                $n++;
                break;
            };
        };
    };
    ksort($ans);



	//page 2

	$rating = array();
	$rating["pleased"]["title"] = "• คุณพึงพอใจในการให้บริการของ S-MomClub ระดับใด";
	$rating["credible"]["title"] = "• ความน่าเชื่อถือของบุคคลากรที่ให้ข้อมูล";
	$rating["conveniently"]["title"] = "• ช่องทางการให้ข้อมูลข่าวสาร สะดวก เข้าถึงง่าย รวดเร็ว";
	$rating["interest"]["title"] = "• ความน่าสนใจของเนื้อหา/ข้อมูลที่คุณได้รับ";
	$rating["useful"]["title"] = "• เนื้อหาและข้อมูลที่ได้รับมีประโยชน์กับคุณมากแค่ไหน";

	$social = array();
	$social[0] = "Facebook";
	$social[1] = "Line@";
	$social[2] = "Website";

	srand( (double)microtime() * 10000000 );
		$n = 0;
		    while ( $n < 3 ){
		        while ( true ) {
		            $c = rand() % 3;
		            if ( empty( $ans_social[$c] ) ){

		                $ans_social[$c] = $social[$n];

		                $n++;
		                break;
		            };
		        };
		    };
	ksort($ans_social);



	$like_club = array();
	$like_club[0] = "คลับของคุณแม่และลูกน้อย";
	$like_club[1] = "คลับของคุณแม่ตั้งครรภ์";
	$like_club[2] = "คลับเสริมความงามของคุณแม่";
	$like_club[3] = "คลับให้คำปรึกษาผู้มีบุตรยาก";

	srand( (double)microtime() * 10000000 );
		$n = 0;
		    while ( $n < 4 ){
		        while ( true ) {
		            $c = rand() % 4;
		            if ( empty( $ans_club[$c] ) ){

		                $ans_club[$c] = $like_club[$n];

		                $n++;
		                break;
		            };
		        };
		    };
	ksort($ans_club);

	//page 3

	$brand = array();
	$brand[0] = "เอนฟา";
	$brand[1] = "ไฮคิว";
	$brand[2] = "ดูเม็กซ์";
	$brand[3] = "S-26";
	$brand[4] = "พีเดียชัวร์";
	$brand[5] = "ซิมิแลค";
	$brand[6] = "ดีจี";
	$brand[7] = "ตราหมี";
	$brand[8] = "แอนมัม";

	srand( (double)microtime() * 10000000 );
	$n = 0;
	    while ( $n < 9 ){
	        while ( true ) {
	            $c = rand() % 9;
	            if ( empty( $ans_brand[$c] ) ){

	                $ans_brand[$c] = $brand[$n];

	                $n++;
	                break;
	            };
	        };
	    };
	ksort($ans_brand);
 ?>

<div id="page-intro-smom">
	<div id="page-intro">
		<div class="warp-intro">
			<div class="box-header font-face">
				<img src="{{ asset($BASE_CDN . '/images/survey/logo-header.svg') }}" width="347" height="157" alt="Mommy Survey" class="img-header">
			</div>
			<div class="detail-survey font-face-light">
				แบบสอบถามนี้มีวัตถุประสงค์ <br class="show-xs">
				เพื่อสำรวจความคิดเห็นของผู้ใช้บริการ<br>
				เพื่อใช้เป็นข้อมูลในการปรับปรุงคุณภาพ <br class="show-xs">
				การให้บริการและประเมินผลการให้บริการ <br class="show-xs">
			</div>
			<div class="detail-survey font-face">
				ดังนั้น จึงขอความร่วมมือจากท่าน <br class="show-xs">
				โปรดกรอกข้อมูลตามความเป็นจริง
			</div>
		</div>
	</div>
</div>
<div id="page-1-new">
	<div class="warp-content">
		<div class="box-img">
			<div class="box-number-mobile">
				<div class="page-current font-face">1</div>
				<div class="slaz-page"></div>
				<div class="page-total font-face">4</div>
			</div>
			<div class="bg-q1">
				<h2 class="title-img3 font-face-slim">
					Mommy <br>
					Survey
				</h2>
			</div>
			<div class="bx-img-mobile">
				<img src="{{ asset($BASE_CDN . '/images/survey/q1-mobile.jpg') }}" class="img-mom-moblie" width="640" height="596" alt="pregnancy">
			</div>
		</div>
		<div class="box-1-new">
			<div class="box-number-page">
				<div class="page-current font-face">1</div>
				<div class="slaz-page"></div>
				<div class="page-total font-face">4</div>
			</div>
			<div class="warp-1-new">
				<h3 class="header-question font-face-light top-header">
					ขณะนี้ ผู้ตอบแบบสอบถามอยู่ในสถานะใด?
				</h3>
				<?php $i=0; foreach ($status_user as $k_status => $v_status) { ?>
					<div class="choice-status">
						<div class="custom-radio">
							<input type="radio" name="quetion-status" id="quetion-status-<?=$i?>" onclick="checkPageOne('quetion-status');" value="<?=$v_status?>" class="radio-choice quetion-status">
							<div class="dot-in-radio"></div>
						</div>
						<label class="detail-choice font-face" onclick="checkPageOne('quetion-status');" for="quetion-status-<?=$i?>"><?=$v_status?></label>
					</div>
				<?php $i++; } ?>
				<div class="clear-both"></div>
			</div>
			<div class="warp-1-new">
				<h3 class="header-question font-face-light">
					อายุของผู้ตอบแบบสอบถาม?
				</h3>
				<?php  $i=0; foreach ($age_user as $k_age => $v_age) { ?>
					<div class="choice-status">
						<div class="custom-radio">
							<input type="radio" name="quetion-age"  id="quetion-age-<?=$i?>" onclick="checkPageOne('quetion-age');" value="<?=$v_age?>" class="radio-choice quetion-age">
							<div class="dot-in-radio"></div>
						</div>
						<label class="detail-choice font-face" onclick="checkPageOne('quetion-age');" for="quetion-age-<?=$i?>"><?=$v_age?></label>
					</div>
				<?php  $i++; } ?>
				<div class="clear-both"></div>
			</div>
			<div class="warp-1-new bottom-warp">
				<h3 class="header-question font-face-light">
					ผู้ตอบแบบสอบถามอาศัยอยู่ในภูมิภาคใด?
				</h3>
				<?php  $i=0; foreach ($living_user as $k_living => $v_living) { ?>
					<div class="choice-status">
						<div class="custom-radio">
							<input type="radio" name="quetion-living"  id="quetion-living-<?=$i?>" onclick="checkPageOne('quetion-living');" value="<?=$v_living?>" class="radio-choice quetion-living">
							<div class="dot-in-radio"></div>
						</div>
						<label class="detail-choice font-face" onclick="checkPageOne('quetion-living');" for="quetion-living-<?=$i?>"><?=$v_living?></label>
					</div>
				<?php $i++; } ?>
				<div class="clear-both"></div>
			</div>
		</div>
	</div>
</div>
<div id="page-1">
	<div class="box-img">
		<div class="img-mom">
			<h2 class="title-img font-face-slim">
				Mommy	<br>
				Survey
			</h2>
		</div>
		<div class="bx-img-mobile">
			<img src="{{ asset($BASE_CDN . '/images/survey/mobile-page1.jpg') }}" class="img-mom-mobile" width="639" height="472" alt="mommy gadget">
		</div>
	</div>
	<div class="box-question-1">
		<div class="warp-content">
			<div class="box-number-page">
				<div class="page-current font-face">2</div>
				<div class="slaz-page"></div>
				<div class="page-total font-face">4</div>
			</div>
			<h3 class="header-question font-face-light">
				คุณรู้จัก S-MomClub หรือไม่?
			</h3>
			<div class="ques1">
				<div class="custom-radio">
					<input type="radio" id="dont_know" name="answer-ques1" value="ไม่รู้จัก" class="radio-choice know_smom">
					<div class="dot-in-radio"></div>
				</div>
				<label for="dont_know" id="txt_dont_know" class="detail-choice font-face">ไม่รู้จัก </label>
			</div>
			<div class="ques1">
				<div class="custom-radio">
					<input type="radio" id="yes" name="answer-ques1" value="รู้จัก" class="radio-choice know_smom">
					<div class="dot-in-radio"></div>
				</div>
				<label for="yes" id="txt_yes" class="detail-choice font-face">รู้จัก</label>
			</div>
			<div class="all-sub-choice">
				<?php $i = 0 ; foreach ($ans as $key => $value) { ?>
					<div class="sub-choice">
						<div class="sub-custom-radio">
							<input type="radio" name="know-from" id="know-from-<?=$i?>" value="<?=$value?>" class="sub-radio-choice know_smom_from">
							<div class="center-radio"></div>
						</div>
						<label class="detail-sub-choice font-face" for="know-from-<?=$i?>"><?=$value?></label>
					</div>
				<?php $i++; } ?>
				<div class="sub-choice">
					<div class="sub-custom-radio">
						<input type="radio" name="know-from" id="other" value="other" class="sub-radio-choice know_smom_from">
						<div class="center-radio"></div>
					</div>
					<label class="detail-sub-choice font-face" for="other">อื่นๆ</label>
				</div>
				<div class="desc-other">
					<textarea  class="font-face area-desc-other" placeholder="พิมพ์ที่นี่..."> </textarea>
				</div>
			</div>
		<!-- 	<div class="ques1">
				<div class="custom-radio">
					<input type="radio" id="not-sure" name="answer-ques1" value="คุ้นๆ" class="radio-choice">
					<div class="dot-in-radio"></div>
				</div>
				<label for="not-sure" id="txt_not_sure" class="detail-choice font-face">คุ้นๆ</label>
			</div> -->
			<div class="submit">
				<div class="btn-next">
					<div class="txt-next font-face">ถัดไป</div>
					<div class="box-arrow">
						<div class="arrow-next"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="page-2">
	<div class="warp-page2">
	 	<div class="box-img">
	 		<div class="img-mom-q2">
	 			<div class="header-img2 font-face-light">
	 				นมแม่ <br>
	 				ดีที่สุด
	 			</div>
	 			<h2 class="title-img2 font-face-slim">
	 				Mommy <br>
	 				Survey
	 			</h2>
	 		</div>
	 		<div class="bx-img-mobile">
	 			<img src="{{ asset($BASE_CDN . '/images/survey/mobile-page2.jpg') }}" class="img-mom-mobile" width="640" height="625" alt="breastfeeding">
	 		</div>
	 		<h3 class="header-question font-face-light mobile">
				คุณเคยได้รับบริการจาก<br class="show-xs">S-MomClub หรือไม่ ?
			</h3>
	 	</div>
	 	<div class="box-question2">
	 		<div class="box-number-page">
				<div class="page-current font-face">3</div>
				<div class="slaz-page"></div>
				<div class="page-total font-face">4</div>
			</div>
	 		<div class="box-content">
				<h3 class="header-question font-face-light desktop">
					คุณเคยใช้บริการจาก S-MomClub หรือไม่
				</h3>
				<div class="choice-ques2">
					<div class="custom-radio">
						<input type="radio" name="choice-ques2" id="never" value="ไม่เคยใช้บริการ" class="radio-choice service_smom">
						<div class="dot-in-radio"></div>
					</div>
					<label for="never" id="txt_never" class="detail-choice font-face">ไม่เคยใช้บริการ</label>
				</div>
				<div class="choice-ques2">
					<div class="custom-radio">
						<input type="radio" name="choice-ques2" id="ever" value="เคยใช้บริการ" class="radio-choice service_smom">
						<div class="dot-in-radio"></div>
					</div>
					<label for="ever" id="txt_ever" class="detail-choice font-face">เคยใช้บริการ</label>
				</div>
				<div class="warp-all-choice">
					<div class="all-sub">
						<div class="sub-question font-face">
							• ช่องทางที่คุณใช้บริการบ่อยที่สุดของ S-MomClub เรียง 3 อันดับจากมากไปน้อย
						</div>
						<div class="question-mark font-face">
							( 1 หมายถึง บ่อยที่สุด )
						</div>
						<div class="all-answer-social">
							<?php foreach ($ans_social as $key => $value) {
								$line = str_replace("@", "", $value);
								$value2 = $value;
								$value = strtolower($line);
								?>
								<div class="answer-social">
									<div class="answer-ques1 font-face">
										<?=$value2?>
									</div>
									<div class="social-radio <?=$value?>-social-radio">
										<input type="radio" name="<?=$value?>_answer" value="1" class="radio-answer <?=$value?>-radio-answer validate-radio">
										<div class="number-center <?=$value?>-number-center font-face">1</div>
									</div>
									<div class="social-radio <?=$value?>-social-radio margin-child2">
										<input type="radio" name="<?=$value?>_answer" value="2" class="radio-answer <?=$value?>-radio-answer validate-radio">
										<div class="number-center <?=$value?>-number-center font-face">2</div>
									</div>
									<div class="social-radio <?=$value?>-social-radio margin-child3">
										<input type="radio" name="<?=$value?>_answer" value="3" class="radio-answer <?=$value?>-radio-answer validate-radio">
										<div class="number-center <?=$value?>-number-center font-face">3</div>
									</div>
								</div>
							<?php } ?>
						</div>
						<div class="question-mark2 font-face">
							กดจำนวนดาวตามระดับที่ท่านพึงพอใจ (5 ดาว หมายถึงพึงพอใจมากที่สุด)
						</div>
					</div>
					<div class="all-sub">
					<?php foreach ($rating as $k => $v) { ?>
						<div class="sub-question font-face">
							<?=$v['title']?>
						</div>
						<div class="answer-rating">
						<fieldset class="rating">
					        <input type="radio" class="<?=$k?>_rating validate-radio" id="<?=$k?>_star5" name="<?=$k?>_rating" value="5" />
					        <label class="full" for="<?=$k?>_star5" title="Awesome - 5 stars"></label>

					        <input type="radio" class="<?=$k?>_rating validate-radio" id="<?=$k?>_star4" name="<?=$k?>_rating" value="4" />
					        <label class="full" for="<?=$k?>_star4" title="Pretty good - 4 stars"></label>

					        <input type="radio" class="<?=$k?>_rating validate-radio" id="<?=$k?>_star3" name="<?=$k?>_rating" value="3" />
					        <label class="full" for="<?=$k?>_star3" title="Meh - 3 stars"></label>

					        <input type="radio" class="<?=$k?>_rating validate-radio" id="<?=$k?>_star2" name="<?=$k?>_rating" value="2" />
					        <label class="full" for="<?=$k?>_star2" title="Kinda bad - 2 stars"></label>

					        <input type="radio" class="<?=$k?>_rating validate-radio" id="<?=$k?>_star1" name="<?=$k?>_rating" value="1" />
					        <label class="full" for="<?=$k?>_star1" title="Sucks big time - 1 star"></label>
						</fieldset>
						</div>
					<?php } ?>
					</div>
					<div class="all-sub">
						<div class="sub-question font-face pading-chioce">
							• ข้อความใดต่อไปนี้ที่คุณคิดว่ามีความใกล้เคียงกับ S-MomClub มากที่สุด
						</div>
						<?php $i = 0; foreach ($ans_club as $k_club => $v_club) { ?>
							<div class="chioce-like-club">
								<div class="sub-custom-radio">
									<input type="radio" name="like_club" id="club-<?=$i?>" value="<?=$v_club?>" class="sub-radio-choice ans-like-club">
									<div class="center-radio"></div>
								</div>
								<label class="detail-sub-choice font-face" for="club-<?=$i?>"><?=$v_club?></label>
							</div>
						<?php $i++; } ?>
					</div>
				</div>
			</div>
			<!-- <div class="submit">
				<div class="btn-next font-face">
					<div class="txt-next">ถัดไป </div>
					<div class="box-arrow">
						<div class="arrow-next"></div>
					</div>
			    </div>
			</div> -->
	 	</div>
	</div>
</div>

<div id="page-3">
	<div class="warp-content">
		<div class="box-img">
			<div class="img-mom-q3">
				<h2 class="title-img3 font-face-slim">
					Mommy <br>
					Survey
				</h2>
			</div>
			<div class="bx-img-mobile">
				<img src="{{ asset($BASE_CDN . '/images/survey/mobile-page3.jpg') }}" class="img-mom-moblie" width="640" height="596" alt=" mommy reading">
			</div>
		</div>
		<div class="box-number-page">
			<div class="page-current font-face">4</div>
			<div class="slaz-page"></div>
			<div class="page-total font-face">4</div>
		</div>
		<div class="box-question3">
			<div class="warp-question3">
				<h3 class="header-question font-face-light">
					ท่านคิดว่า S-MomClub คือศูนย์<br class="show-xs">ลูกค้าสัมพันธ์ของผลิตภัณฑ์ใด (ตอบเพียง 1 ช่อง)
				</h3>
				<?php foreach ($ans_brand as $key => $value) {
					$value_brand = "";
					if ($value == "เอนฟา") {
						$value_brand = "เอนฟา (Enfa)";
					}elseif ($value == "ไฮคิว") {
						$value_brand = "ไฮคิว (Hi-Q)";
					}elseif ($value == "ดูเม็กซ์") {
						$value_brand = "ดูเม็กกซ์ (Dumex)";
					}elseif ($value == "S-26") {
						$value_brand = "เอส-26 (S-26)";
					}elseif ($value == "พีเดียชัวร์") {
						$value_brand = "พีเดียชัวร์ (Pediasure)";
					}elseif ($value == "ซิมิแลค") {
						$value_brand = "ซิมิแลค (Similac)";
					}elseif ($value == "ดีจี") {
						$value_brand = "ดีจี (DG)";
					}elseif ($value == "ตราหมี") {
						$value_brand = "ตราหมี (Bear Brand)";
					}else {
						$value_brand = "แอนมัม (Anmum)";
					}

					?>
					<div class="choice-ques3">
						<div class="custom-radio">
							<input type="radio" name="choice-ques3" id="brand-<?=$key?>" value="<?=$value?>" class="radio-choice click-choice">
							<div class="dot-in-radio"></div>
						</div>
						<label class="detail-choice font-face" for="brand-<?=$key?>"><?=$value_brand;?></label>
					</div>
				<?php } ?>
				<div class="clear-both"></div>
			</div>
		</div>
	</div>
</div>

<div id="page-thank-you">
	<div class="page-content">
		<div class="logo">
			<img src="{{ asset($BASE_CDN . '/images/survey/logo-smom.svg') }}" alt="logo S-MomClub" width="363" height="72" class="box-logo">
		</div>
		<div class="header">
			<div class="font-face-med">ขอบคุณคุณแม่</div>
			<div class="font-face-light">ที่ร่วมแสดงความคิดเห็น </div>
		</div>
	</div>
	<div class="box-img-nurse">
		<img src="{{ asset($BASE_CDN . '/images/survey/nurse.png') }}" width="707" height="449" alt="nurse" class="img-nurse">
	</div>
	<div class="dim-nurse">
		<img src="{{ asset($BASE_CDN . '/images/survey/dim-bg.png') }}" width="1280" height="297" alt="dim-bg" class="dim-bg">
	</div>
</div>


<div id="modal-varlidate" class="modal fade" role="dialog">
	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
		    <div class="modal-body">
		        <p>กรุณาตอบแบบสอบถามให้ครบถ้วน</p>
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>
	    </div>

	</div>
</div>

<div id="varlidate-value" class="modal fade" role="dialog">
	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
		    <div class="modal-body">
		        <p>กรุณาเรียง 3 อันดับจากมากไปน้อย ( 1 หมายถึง บ่อยที่สุด )</p>
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>
	    </div>

	</div>
</div>

<div id="varlidate-other" class="modal fade" role="dialog">
	<div class="modal-dialog">

	    <!-- Modal content-->
	    <div class="modal-content">
		    <div class="modal-body">
		        <p>กรุณากรอกรายละเอียดอื่นๆ</p>
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    </div>
	    </div>

	</div>
</div>

@endsection
@compressJs("question-survey")