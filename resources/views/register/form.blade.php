@extends('template.master')
@compressCss("register_form,ddown")
@section('content')

	<div id="register-panel">
		<div id="register-panel-inner">
			<div class="register-panel-top">
				<div class="main-title"><span class="FXregular">สมัคร</span><span class="FLighter">สมาชิก</span></div>
				<div class="register-panel-top-left">
					<div class="input-panel">
						<div class="input-text FLighter">ชื่อผู้ใช้งาน</div>
						<input class="input-data FLighter" type="text" name="displayname">
					</div>
					<div class="input-panel">
						<div class="input-text FLighter">อีเมล</div>
						<input class="input-data FLighter" type="text" name="email">
					</div>
					<div class="input-panel">
						<div class="input-text FLighter">รหัสผ่าน</div>
						<input class="input-data FLighter" type="password" name="password" id="password" autocomplete="off">
					</div>
					<div class="check-strength">
						<span class="text-strength FLighter">ระดับความปลอดภัยของรหัสผ่าน</span>
						<div class="strength lv1"></div>
						<div class="strength lv2"></div>
						<div class="strength lv3"></div>
						<div class="strength lv4"></div>
						<div class="FLighter" id="checkpw"></div>
						<div class="clearfix"></div>
					</div>
					<div class="input-panel">
						<div class="input-text FLighter">ยืนยันรหัสผ่าน</div>
						<input class="input-data FLighter" type="password" id="confirm-password" autocomplete="off">
					</div>
					<span class="FLighter" id="checkmatch"></span>
				</div>
				<div class="register-panel-top-right">
					<div class="register-panel-top-right-inner">
						<div class="guide-title-bar">
							<div class="icon-key"></div><span class="guide-title FXMed">คำแนะนำ</span>
						</div>
						<span class="FLighter">ในการสมัครสมาชิกเว็บไซต์ เพื่อเข้าใช้งาน กรุณากรอกรายละเอียดด้านซ้ายมือให้ครบถ้วน เพื่อความสมบูรณ์ของข้อมูลผู้ใช้งาน</span>
						<ul class="guide-panel FLighter">
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">ชื่อผู้ใช้งาน</span><span class="subtitle-guide FLighter"> - กรอกชื่อผู้ใช้งานของคุณ 10 ตัวอักษร, ต้องมีตัวเลขอย่างน้อย 1 หมายเลข (0-9) และต้องมีอย่างน้อย 1 อักขระพิเศษ</span>
								</span>
							</li>
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">อีเมล์</span><span class="subtitle-guide FLighter"> - กรอกอีเมล์ของคุณ เช่น example@mail.com</span>
								</span>
							</li>
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">รหัสผ่าน</span><span class="subtitle-guide FLighter"> - กรุณาระบุรหัสผ่าน โดยต้องมีความยาวไม่ต่ำกว่า 8 ตัวอักษรต้องมีตัวเลขอย่างน้อย 1 หมายเลข (0-9) และต้องมีอย่างน้อย 1 อักขระพิเศษ</span>
								</span>
							</li>
							<li class="guide-box">
								<span class="guide-text">
									<span class="title-guide FLighter">ยืนยันรหัสผ่าน</span><span class="subtitle-guide FLighter"> - ระบุรหัสผ่านอีกครั้ง เพื่อยืนยันรหัสผ่าน</span>
								</span>
							</li>
						</ul>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="register-panel-middle">
				<div class="title">
					<div class="img-icon icon-form"></div><span class="text-title FLighter">ข้อมูลส่วนตัว</span>
				</div>
				<div class="register-panel-middle-inner">
					<div class="input-panel-middle">
						<div class="input-panel-middle-inner">
							<div class="input-text FLighter">คำนำหน้าชื่อ:</div>
							<div class="radio-panel">
								<div class="radio-box">
									<input class="input-radio" type="radio" name="title" id="t1" value="1"><label for="t1" class="text-radio FLighter"><span></span>นาย</label>
								</div>
								<div class="radio-box">
									<input class="input-radio" type="radio" name="title" id="t2" value="2"><label for="t2" class="text-radio FLighter"><span></span>นาง</label>
								</div>
								<div class="radio-box">
									<input class="input-radio" type="radio" name="title" id="t3" value="3"><label for="t3" class="text-radio FLighter"><span></span>นางสาว</label>
								</div>
							</div>
						</div>
						<div class="input-panel-middle-inner">
							<div class="input-text FLighter">เพศ:</div>
							<div class="radio-panel">
								<div class="radio-box">
									<input class="input-radio" type="radio" name="gendar" id="g1" value="1"><label for="g1" class="text-radio FLighter"><span></span>ชาย</label>
								</div>
								<div class="radio-box">
									<input class="input-radio" type="radio" name="gendar" id="g2" value="2"><label for="g2" class="text-radio FLighter"><span></span>หญิง</label>
								</div>
							</div>
						</div>
					</div>
					<div class="input-panel-middle">
						<div class="input-panel-middle-inner">
							<div class="input-text FLighter">ชื่อ</div>
							<input class="input-data FLighter" type="text" name="firstname">
						</div>
						<div class="input-panel-middle-inner">
							<div class="input-text FLighter">หมายเลขโทรศัพท์</div>
							<input class="input-data FLighter" type="text" name="mobile">
						</div>
					</div>
					<div class="input-panel-middle">
						<div class="input-panel-middle-inner">
							<div class="input-text FLighter">สกุล</div>
							<input class="input-data FLighter" type="text" name="lastname">
						</div>
						<div class="input-panel-middle-inner">
							<div class="input-text FLighter">วันเดือนปีเกิด</div>
						<div class="dropdown" >
							<div class="dropdown-list" id="date">
								<span class="dropdown-list-text FLighter">วัน</span>
								<span class="btn-list">
									<span class="btn-img"></span>
								</span>
							</div>
							<div class="dropdown-list-data">
								<ul id="list-subject" class="dropdown-list-data-inner">
									<li data-val="1">
										<span class="FLighter">1</span>
									</li>
									<li data-val="2">
										<span class="FLighter">2</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="dropdown">
							<div class="dropdown-list" id="month">
								<span class="dropdown-list-text FLighter">เดือน</span>
								<span class="btn-list">
									<span class="btn-img"></span>
								</span>
							</div>
							<div class="dropdown-list-data">
								<ul id="list-subject" class="dropdown-list-data-inner">
									<li data-val="1">
										<span class="FLighter">1</span>
									</li>
									<li data-val="2">
										<span class="FLighter">2</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="dropdown">
							<div class="dropdown-list" id="year" >
								<span class="dropdown-list-text FLighter">ปี</span>
								<span class="btn-list">
									<span class="btn-img"></span>
								</span>
							</div>
							<div class="dropdown-list-data">
								<ul id="list-subject" class="dropdown-list-data-inner">
									<li data-val="1">
										<span class="FLighter">1</span>
									</li>
									<li data-val="2">
										<span class="FLighter">2</span>
									</li>
								</ul>
							</div>
						</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="register-panel-bottom">
				<div class="title">
					<div class="img-icon icon-kid"></div><span class="text-title FLighter">ข้อมูลลูกน้อย</span>
				</div>
				<div class="register-panel-bottom-inner">
					<div class="panel-information-kid">
						<div class="panel-information-kid-left">
							<div class="radio-panel">
								<div class="radio-box">
									<input class="input-radio" type="radio" name="test" id="d1" value="d1"><label for="d1" class="text-radio FLighter"><span></span>ตั้งครรภ์</label>
								</div>
								<div class="radio-box">
									<input class="input-radio" type="radio" name="test" id="d2" value="d2"><label for="d2" class="text-radio FLighter"><span></span>คลอดแล้ว</label>
								</div>
							</div>
						</div>
						<div class="panel-information-kid-right">
							<div class="dropdown" >
							<div class="dropdown-list" id="date">
								<span class="dropdown-list-text FLighter">วัน</span>
								<span class="btn-list">
									<span class="btn-img"></span>
								</span>
							</div>
							<div class="dropdown-list-data">
								<ul id="list-subject" class="dropdown-list-data-inner">
									<li data-val="1">
										<span class="FLighter">1</span>
									</li>
									<li data-val="2">
										<span class="FLighter">2</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="dropdown">
							<div class="dropdown-list" id="month">
								<span class="dropdown-list-text FLighter">เดือน</span>
								<span class="btn-list">
									<span class="btn-img"></span>
								</span>
							</div>
							<div class="dropdown-list-data">
								<ul id="list-subject" class="dropdown-list-data-inner">
									<li data-val="1">
										<span class="FLighter">1</span>
									</li>
									<li data-val="2">
										<span class="FLighter">2</span>
									</li>
								</ul>
							</div>
						</div>
						<div class="dropdown">
							<div class="dropdown-list" id="year" >
								<span class="dropdown-list-text FLighter">ปี</span>
								<span class="btn-list">
									<span class="btn-img"></span>
								</span>
							</div>
							<div class="dropdown-list-data">
								<ul id="list-subject" class="dropdown-list-data-inner">
									<li data-val="1">
										<span class="FLighter">1</span>
									</li>
									<li data-val="2">
										<span class="FLighter">2</span>
									</li>
								</ul>
							</div>
						</div>
						</div>
					</div>
				</div>
			</div>
			<div class="panel-btn">
				<div class="btn-submit FLighter">ยืนยันs</div>
			</div>
		</div>
	</div>

@endsection
@compressJs("ddown,register_form")