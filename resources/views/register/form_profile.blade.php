@extends('template.master')
@compressCss("ddown,validationEngine.jquery,jquery-ui,register_form")
@section('content')
<?php
	$PAGE_LANG = trans('core.CORE_LANG');
?>
	<div id="register-panel">
		<div id="register-panel-inner">
			<form id="register-form">
				<input type="hidden" name="mode" value="edit">
				<input type="hidden" name="member_type" value="{{ $member_type }}">
				<input type="hidden" name="member_web_id" value="{{ $member_web_id }}">
				<div class="main-title"><span class="FXregular">ข้อมูล</span><span class="FLighter">ส่วนตัว</span></div>
				<div id="status-mom" class="register-panel-box fix-line">
					<div class="title">
						<div class="img-icon icon-mom"></div><span class="text-title FLighter">{{ trans("register.MOM_STATUS") }}</span>
					</div>
					<div class="register-panel-bottom-inner">
						<div class="panel-information">
							<span class="data-left FLighter">
								{{ trans("register.CHOOSE_MOM_STATUS") }}
							</span>
							<span class="data-right">
								<div class="radio-box">
									<input class="input-radio" type="radio" name="status-mom-choice" id="status-mom-choice1" value="1" {{ $status_mom == "1" ?  'checked ' : '' }} disabled><label for="status-mom-choice1" class="text-radio FLighter" ><span></span>{{ trans("register.PREGNANCY_MOM") }}</label>
								</div>
								<div class="radio-box">
									<input class="input-radio" type="radio" name="status-mom-choice" id="status-mom-choice2" value="2" {{ $status_mom == "2" ?  'checked ' : '' }} disabled><label for="status-mom-choice2" class="text-radio FLighter" ><span></span>{{ trans("register.LACTATING_MOM") }}</label>
								</div>
								<div class="radio-box">
									<input class="input-radio" type="radio" name="status-mom-choice" id="status-mom-choice3" value="3" {{ $status_mom == "3" ?  'checked ' : '' }} disabled><label for="status-mom-choice3" class="text-radio FLighter" ><span></span>{{ trans("register.TODDLER_MOM") }}</label>
								</div>
								<input type="text" class="hidden-value validate[required]" name="status_mom" id="status_mom" tabindex="6" value="{{ $status_mom != "" && $status_mom != 0 ?  $status_mom : '' }}">
							</span>
							<span class="clearfix"></span>
							<div class="panel-eating">
								@if($status_mom == "1" || $status_mom == "2" )
									<div class="panel-eating-choice">
										<span class="data-left FLighter">คุณแม่ทานนมบำรุงครรภ์หรือไม่ :</span>
										<span class="data-right">
											<div class="radio-box ">
												<input class="input-radio" type="radio" name="eating-choice" id="eat1" value="T" {{ $eating_choice != "" && $eating_choice == "T" ?  'checked' : '' }}><label for="eat1" class="text-radio FLighter" ><span></span>ทาน</label>
											</div>
											<div class="radio-box ">
												<input class="input-radio" type="radio" name="eating-choice" id="eat2" value="F" {{ $eating_choice != "" && $eating_choice == "F" ?  'checked' : '' }}><label for="eat2" class="text-radio FLighter" ><span></span>ไม่ทาน</label>
											</div>
											<input type="text" class="hidden-value validate[required]" name="eating" id="eating" value="{{ $eating_choice != "" ?  $eating_choice : '' }}">
										</span>
										<span class="clearfix"></span>
									</div>
									<div class="panel-eating-text">
										@if($eating_choice != "" && $eating_choice == "T")
										<span class="data-left FLighter">ยี่ห้อนมบำรุงครรภ์ :</span>
										<span class="data-right">
											<input class="input-data FLighter validate[required]" type="text" id="eating_text" name="eating_text" value="{{ $eating_text }}">
										</span>
										@endif
									</div>
								@endif
							</div>
							<span class="clearfix"></span>
						</div>
					</div>
				</div>
				<div class="register-panel-bottom fix-line">
					<div class="title">
						<div class="img-icon icon-kid"></div><span class="text-title FLighter">{{ trans("register.CHILD_DATA") }}</span>
					</div>
					<div class="countChild">
						<span class="FLighter">{{ trans("register.CHILD_AMOUNT") }}</span>
						<div class="ddChild">
							<div class="dropdown-list">
									<?php
										$DATAT_UNIT = ($PAGE_LANG == "th")? "คน" : "person" ;
									?>
									<span class="dropdown-list-text FLighter">{{ count($child) > 0 ? count($child) ." ".  $DATAT_UNIT :  trans("register.SELECT_CHILD_AMOUNT")}}</span>
									<span class="btn-list">
										<span class="btn-img"></span>
									</span>
							</div>
							<div class="dropdown-list-data">
								<ul class="dropdown-list-data-inner">
									<li data-val="1" class="FLighter">1 {{ ($PAGE_LANG == "th")? "คน" : "person" }}</li>
									<li data-val="2" class="FLighter">2 {{ ($PAGE_LANG == "th")? "คน" : "persons" }}</li>
									<li data-val="3" class="FLighter">3 {{ ($PAGE_LANG == "th")? "คน" : "persons" }}</li>
									<li data-val="4" class="FLighter">4 {{ ($PAGE_LANG == "th")? "คน" : "persons" }}</li>
									<li data-val="5" class="FLighter">5 {{ ($PAGE_LANG == "th")? "คน" : "persons" }}</li>
								</ul>
							</div>
						</div>
						<input type="text" class="hidden-value validate[required]" id="check-ddown" tabindex="12" readonly value="{{ count($child) > 0 ? count($child) : ""}}">
					</div>
					<div id="register-panel-bottom-child" class="register-panel-bottom-inner">
					@if (count($child) > 0)
					<?php
						$running_number = 1;
						$running_number_tabIndex = 13;
					?>
					@foreach ($child as $keyChild => $valChild)
						<div class="panel-information-kid {{{ ($valChild->mom_status == 1)? 'pregnant' : 'lactating' }}}">
							<div class="panel-information-kid-left">
								<div class="panel-number FLighter">{{++$keyChild}}.</div>
								<div class="radio-panel">
									<div class="radio-box">
										<input class="input-radio list-check list-mom-type" type="radio" name="mom_status_choice[{{ $keyChild }}]" id="d{{ $running_number }}"  value="1" {{ $valChild->mom_status == 1 ? 'checked' : '' }}><label for="d{{ $running_number++ }}" class="text-radio FLighter"><span></span>{{ trans("register.PREGNANT") }}</label>
									</div>
									<div class="radio-box">
										<input class="input-radio list-check list-mom-type" type="radio" name="mom_status_choice[{{ $keyChild }}]" id="d{{ $running_number }}" value="2" {{ $valChild->mom_status == 2 ? 'checked' : '' }}><label for="d{{ $running_number++ }}" class="text-radio FLighter"><span></span>{{ trans("register.GIVE_BIRTH") }}</label>
									</div>
								</div>
								<input type="text" class="mom_status hidden-value validate[required]" name="mom_status[{{ $keyChild }}]" value="{{ $valChild->mom_status != "" ? $valChild->mom_status : '' }}" tabindex="{{ $running_number_tabIndex++ }}">
							</div>
							<div class="panel-information-kid-right">
								<input type="text" name="child_date[{{$keyChild}}]" class="validate[required] input-data calendar FLighter {{{ ($valChild->mom_status == 1)? 'pregnant' : 'lactating' }}}" readonly value="{{ date("d/m/Y", strtotime($valChild->child_date)) }}" tabindex="{{ $running_number_tabIndex }}">
								<div class="text-date-child FLighter">
									@if($valChild->mom_status == 1)
									{{ trans("register.PREGNANT_DATE") }}
									@else
									{{ trans("register.CHILD_BIRTHDATE") }}
									@endif
								</div>
								<div class="location-box {{{ ($valChild->mom_status == 1)? 'pregnant' : '' }}}"><input type="text" class="input-data location validate[required] FLighter" name="antenatal_place[{{$keyChild}}]" tabindex="{{ $running_number_tabIndex }}" placeholder="ตัวอย่าง รพ.บำรุงราษฎร์" value="{{ $valChild->antenatal_place }}"><div class="text-location-child FLighter">สถานที่ฝากครรภ์</div></div>
							</div>
						</div>
					@endforeach
					@endif
					</div>
				</div>
				<div class="register-panel-top">
					<div class="title">
						<div class="img-icon icon-setting"></div><span class="text-title FLighter">{{ trans("register.SETTING_ACCOUNT") }}</span>
					</div>
					<div class="register-panel-top-left">
						<div class="input-panel">
							<input class="input-data FLighter validate[required]" type="hidden" id="displayname" name="displayname" value="{{ $displayname }}" tabindex="1">
						</div>
						<div class="input-panel">
							<div class="input-text FLighter">{{ trans("register.EMAIL") }}</div>
							<input class="input-data FLighter validate[required, custom[email]]" type="text" id="email" name="email" {{ $email != "" ? ' readonly' : '' }} value="{{ $email }}" tabindex="2">
						</div>
						<div class="input-panel">
							<div class="input-text FLighter">{{ trans("register.OLD_PASSWORD") }}</div>
							<input class="input-data FLighter validate[required]" type="password" name="old_password" id="old-password" maxlength="20" tabindex="3" autocomplete="off">
						</div>
						<div class="input-panel password">
							<div class="input-text FLighter">{{ trans("register.PASSWORD") }}</div>
							<input class="input-data FLighter validate[required, funcCall[checkValidatePassword]]" type="password" name="password" id="password" maxlength="20" tabindex="3" autocomplete="off">
						</div>
						<div class="check-strength">
							<span class="text-strength FLighter">{{ trans("register.STRENGTH_TEXT") }}</span>
							<div class="clearfix"></div>
							<div id="strength-password">
								<div class="strength lv1"></div>
								<div class="strength lv2"></div>
								<div class="strength lv3"></div>
								<div class="strength lv4"></div>
								<div class="FLighter" id="checkpw"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="input-panel">
							<div class="input-text FLighter">{{ trans("register.CONFIRM_PASSWORD") }}</div>
							<input class="input-data FLighter validate[required,equals[password]]" type="password" id="confirm-password" maxlength="20" tabindex="4" autocomplete="off">
						</div>
						<span class="FLighter" id="checkmatch"></span>
					</div>
					<div class="register-panel-top-right">
						<div class="register-panel-top-right-inner">
							<div class="guide-title-bar">
								<div class="icon-key"></div><span class="guide-title FXMed">คำแนะนำ</span>
							</div>
							<span class="FLighter">ในการสมัครสมาชิกเว็บไซต์ เพื่อเข้าใช้งาน กรุณากรอกรายละเอียดด้านซ้ายมือให้ครบถ้วน เพื่อความสมบูรณ์ของข้อมูลผู้ใช้งาน</span>
							<ul class="guide-panel FLighter">
								<li class="guide-box">
									<span class="guide-text">
										<span class="title-guide FLighter">อีเมล</span><span class="subtitle-guide FLighter"> - กรอกอีเมลของคุณ เช่น example@mail.com</span>
									</span>
								</li>
								<li class="guide-box">
									<span class="guide-text">
										<span class="title-guide FLighter">รหัสผ่าน</span><span class="subtitle-guide FLighter"> - กรุณาระบุรหัสผ่าน โดยต้องมีความยาวไม่ต่ำกว่า 10 ตัวอักษร แต่ไม่เกิน 20 ตัวอักษร ตัวอักษรพิมพ์เล็กอย่างน้อย 1 ตัวอักษร ตัวอักษรพิมพ์ใหญ่อย่างน้อย 1 ตัวอักษร ต้องมีตัวเลขอย่างน้อย 1 หมายเลข (0-9) และต้องมีอย่างน้อย 1 อักขระพิเศษ</span>
									</span>
								</li>
								<li class="guide-box">
									<span class="guide-text">
										<span class="title-guide FLighter">ยืนยันรหัสผ่าน</span><span class="subtitle-guide FLighter"> - ระบุรหัสผ่านอีกครั้ง เพื่อยืนยันรหัสผ่าน</span>
									</span>
								</li>
							</ul>
						</div>
					</div>
					<div class="clearfix" id="bfAllow"></div>
					<div class="box-allow">
						<div class="icon-allow {{ $member_card ? ' allow active' : '' }}"></div>
						<input type="hidden" id="allow" tabindex="5" name="member_card_register" {{ $member_card ? ' value=2 readonly' : '' }} >
						<div class="panel-text-allow">
							<span class="text-allow FLighter">{{ trans("register.REGISTER_SMOM_CLUB") }}</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="register-panel-middle">
					<div class="title">
						<div class="img-icon icon-form"></div><span class="text-title FLighter">{{ trans("register.PROFILE") }}</span>
					</div>
					<div class="register-panel-middle-inner">
						<div class="input-panel-middle">
							<div class="input-panel-middle-inner">
								<div class="input-text FLighter">{{ trans("register.TITLE") }}</div>
								<div class="radio-panel">
									<div class="radio-box">
										<input class="input-radio" type="radio" name="title-choice" id="t1" value="1" {{ $title == "1" ?  'checked ' : '' }} ><label for="t1" class="text-radio FLighter" ><span></span>{{ trans("register.TITLE_MR") }}</label>
									</div>
									<div class="radio-box">
										<input class="input-radio" type="radio" name="title-choice" id="t2" value="2" {{ $title == "2" ?  'checked ' : '' }}><label for="t2" class="text-radio FLighter"><span></span>{{ trans("register.TITLE_MRS") }}</label>
									</div>
									<div class="radio-box">
										<input class="input-radio" type="radio" name="title-choice" id="t3" value="3" {{ $title == "3" ?  'checked ' : '' }}><label for="t3" class="text-radio FLighter"><span></span>{{ trans("register.TITLE_MS") }}</label>
									</div>
									<input type="text" class="hidden-value validate[required]" name="title" id="title" tabindex="6" value="{{ $title != "" && $title != 0 ?  $title : '' }}">
								</div>
							</div>
							<div class="input-panel-middle-inner">
								<div class="input-text FLighter">{{ trans("register.GENDAR") }}</div>
								<div class="radio-panel">
									<div class="radio-box">
										<input class="input-radio" type="radio" name="gendar-choice" id="g1" value="1" {{ $gendar == "1" ?  'checked ' : '' }}><label for="g1" class="text-radio FLighter"><span></span>{{ trans("register.GENDAR_MALE") }}</label>
									</div>
									<div class="radio-box">
										<input class="input-radio" type="radio" name="gendar-choice" id="g2" value="2" {{ $gendar == "2" ?  'checked ' : '' }}><label for="g2" class="text-radio FLighter"><span></span>{{ trans("register.GENDAR_FEMALE") }}</label>
									</div>
									<input type="text" class="hidden-value validate[required]" name="gendar" id="gendar" tabindex="9" value="{{ $gendar != "" && $gendar != 0 ?  $gendar : '' }}">
								</div>
							</div>
						</div>
						<div class="input-panel-middle">
							<div class="input-panel-middle-inner" id="box-fname">
								<div class="input-text FLighter">{{ trans("register.FIRSTNAME") }}</div>
								<input class="input-data FLighter validate[required, custom[onlyLetterNumberCus]]" type="text" id="firstname" name="firstname" value="{{ $firstname }}" tabindex="7">
							</div>
							<div class="input-panel-middle-inner" id="box-mobile">
								<div class="input-text FLighter">{{ trans("register.TELEPHONE_NUMBER") }}</div>
								<input class="input-data FLighter validate[required, custom[phone]]" type="text" id="mobile" name="mobile" value="{{ $mobile }}" tabindex="10" maxlength="10">
							</div>
						</div>
						<div class="input-panel-middle">
							<div class="input-panel-middle-inner" id="box-lname">
								<div class="input-text FLighter">{{ trans("register.LASTNAME") }}</div>
								<input class="input-data FLighter validate[required, custom[onlyLetterNumberCus]]" type="text" id="lastname" name="lastname" value="{{ $lastname }}" tabindex="8">
							</div>
							<div class="input-panel-middle-inner" id="box-birthday">
								<div class="input-text FLighter">{{ trans("register.BIRTH_DATE") }}</div>
								<input type="text" class="input-date input-data FLighter validate[required] datepicker" id="birthday" name="birthday" value="{{ ($birthday == null) ? '' : date("d/m/Y", strtotime($birthday)) }}" tabindex="11" readonly>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="register-panel-box fix-line">
					<div class="title">
						<div class="img-icon icon-home"></div><span class="text-title FLighter">{{ trans('register.REGISTER_ADDRESS') }}</span>
					</div>
					<div class="register-panel-bottom-inner">
						<div class="panel-information">
							<span class="input-box input-box-short-left">
								<span class="input-text FLighter">{{ trans('register.ADDRESS_HOUSE_NO') }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_number" value="{{ $address_number }}" tabindex="18">
							</span>
							<span class="input-box input-box-short-right">
								<span class="input-text FLighter">{{ trans('register.ADDRESS_BUILDING') }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_building" value="{{ $address_building }}" tabindex="19">
							</span>
							<span class="input-box input-box-short-left">
								<span class="input-text FLighter">{{ trans('register.ADDRESS_FLOOR') }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_floor" value="{{ $address_floor }}" tabindex="20">
							</span>
							<span class="input-box input-box-short-right">
								<span class="input-text FLighter">{{ trans('register.ADDRESS_ROOM') }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_room" value="{{ $address_room }}" tabindex="21">
							</span>
							<span class="input-box input-box-no-margin input-box-long">
								<span class="input-text FLighter">{{ trans('register.ADDRESS_VILLAGE') }}</span>
								<input class="input-data FLighter" type="text" name="address_village" value="{{ $address_village }}" tabindex="22">
							</span>
							<span class="input-box input-box-short-left" id="box-soi">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_ALLEY") }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_alley" value="{{ $address_alley }}" tabindex="23">
							</span>
							<span class="input-box input-box-long" id="box-road">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_ROAD") }}</span>
								<input class="input-data input-259 FLighter" type="text" name="address_road" value="{{ $address_road }}" tabindex="24">
							</span>
							<span class="input-box input-box-short-right" id="box-moo">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_VILLAGE_NO") }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_village_no" value="{{ $address_village_no }}" tabindex="25">
							</span>
							<span class="input-box input-box-no-margin input-box-long">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_SUB_DISTRICT") }}</span>
								<input class="input-data  FLighter" type="text" name="address_sub_district" value="{{ $address_sub_district }}" tabindex="26">
							</span>
							<span class="input-box input-box-long">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_DISTRICT") }}</span>
								<input class="input-data  FLighter" type="text" name="address_district" value="{{ $address_district }}" tabindex="27">
							</span>
							<span class="input-box input-box-long">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_PROVINCE") }}</span>
								<input class="input-data input-259  FLighter" type="text" name="address_province" value="{{ $address_province }}" tabindex="28">
							</span>
							<span class="input-box input-box-short-left">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_ZIP_CODE") }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_zip_code" value="{{ $address_zip_code }}" tabindex="29">
							</span>
							<span class="input-box input-box-no-margin input-box-short-right">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_PHONE") }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_phone" value="{{ $address_phone }}" tabindex="30">
							</span>
							<span class="input-box input-box-short-left">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_FAX") }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_fax" value="{{ $address_fax }}" tabindex="31">
							</span>
							<span class="input-box input-box-no-margin input-box-short-right">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_MOBILE") }}</span>
								<input class="input-data input-small FLighter" type="text" name="address_mobile" value="{{ $address_mobile }}" tabindex="32">
							</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="register-panel-box">
					<div class="title">
						<div class="img-icon icon-condo"></div><span class="text-title FLighter">{{ trans("register.OFFICE") }}</span>
					</div>
					<div class="register-panel-bottom-inner">
						<div class="panel-information">
							<span class="input-box input-box-long">
								<span class="input-text FLighter">{{ trans("register.OFFICE_NAME") }}</span>
								<input class="input-data input-long FLighter" type="text" name="office_name" value="{{ $office_name }}" tabindex="33">
							</span>
							<span class="input-box input-box-short-left">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_BUILDING") }}</span>
								<input class="input-data input-small FLighter" type="text" name="office_building" value="{{ $office_building }}" tabindex="34">
							</span>
							<span class="input-box input-box-short-right">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_FLOOR") }}</span>
								<input class="input-data input-small FLighter" type="text" name="office_floor" value="{{ $office_floor }}" tabindex="35">
							</span>
							<span class="input-box input-box-no-margin input-box-short-left">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_ROOM") }}</span>
								<input class="input-data input-small FLighter" type="text" name="office_room" value="{{ $office_room }}" tabindex="36">
							</span>
							<span class="input-box input-box-short-right">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_VILLAGE") }}</span>
								<input class="input-data FLighter" type="text" name="office_village" value="{{ $office_village }}" tabindex="37">
							</span>
							<span class="input-box input-box-short-left" id="box-soi2">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_ALLEY") }}</span>
								<input class="input-data input-small FLighter" type="text" name="office_alley" value="{{ $office_alley }}" tabindex="38">
							</span>
							<span class="input-box input-box-long" id="box-road2">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_ROAD") }}</span>
								<input class="input-data input-259 FLighter" type="text" name="office_road" value="{{ $office_road }}" tabindex="39">
							</span>
							<span class="input-box input-box-no-margin input-box-short-right" id="box-moo2">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_VILLAGE_NO") }}</span>
								<input class="input-data input-small FLighter" type="text" name="office_village_no" value="{{ $office_village_no }}" tabindex="40">
							</span>
							<span class="input-box input-box-long">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_SUB_DISTRICT") }}</span>
								<input class="input-data  FLighter" type="text" name="office_sub_district" value="{{ $office_sub_district }}" tabindex="41">
							</span>
							<span class="input-box input-box-long">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_DISTRICT") }}</span>
								<input class="input-data  FLighter" type="text" name="office_district" value="{{ $office_district }}" tabindex="42">
							</span>
							<span class="input-box input-box-no-margin input-box-long">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_PROVINCE") }}</span>
								<input class="input-data input-259  FLighter" type="text" name="office_province" value="{{ $office_province }}" tabindex="43">
							</span>
							<span class="input-box input-box-short-left">
								<span class="input-text FLighter">{{ trans("register.ADDRESS_ZIP_CODE") }}</span>
								<input class="input-data input-small FLighter" type="text" name="office_zip_code" value="{{ $office_zip_code }}" tabindex="44">
							</span>
							<span class="input-box input-box-short-right">
								<span class="input-text FLighter">{{ trans("register.OFFICE_PHONE") }}</span>
								<input class="input-data input-small FLighter" type="text" name="office_phone" value="{{ $office_phone }}" tabindex="45">
							</span>
							<span class="input-box input-box-short-left">
								<span class="input-text FLighter">{{ trans("register.OFFICE_FAX") }}</span>
								<input class="input-data input-small FLighter" type="text" name="office_fax" value="{{ $office_fax }}" tabindex="46">
							</span>
							<span class="input-box input-box-no-margin input-box-short-right">
								<span class="input-text FLighter">{{ trans("register.OFFICE_MOBILE") }}</span>
								<input class="input-data input-small FLighter" type="text" name="office_mobile" value="{{ $office_mobile }}" tabindex="47">
							</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="panel-btn">
					<div class="btn-submit FLighter">{{ trans("core.CORE_CONFIRM_BUTTON") }}</div>
				</div>
			</div>
		</form>
	</div>
</div>

	<script type="text/javascript">
		var MEMBER_DISPLAYNAME_REQUIRED = "{{ trans("register.DISPLAYNAME_REQUIRED_MESSAGE") }}";
		var MEMBER_EMAIL_REQUIRED = " {{ trans("register.EMAIL_REQUIRED_MESSAGE") }} ";
		var MEMBER_EMAIL_VALID = " {{ trans("register.EMAIL_VALID_MESSAGE") }} ";
		var MEMBER_PASSWORD_REQUIRED = " {{ trans("register.PASSWORD_REQUIRED_MESSAGE") }} ";
		var MEMBER_PASSWORD_VALID = " {{ trans("register.PASSWORD_VALID_MESSAGE") }} ";
		var MEMBER_CONFIRM_PASSWORD_VALID = " {{ trans("register.CONFIRM_PASSWORD_VALID_MESSAGE") }} ";
		var MEMBER_TITLE_REQUIRED = " {{ trans("register.TITLE_REQUIRED_MESSAGE") }} ";
		var MEMBER_FIRSTNAME_REQUIRED = " {{ trans("register.FIRSTNAME_REQUIRED_MESSAGE") }} ";
		var MEMBER_FIRSTNAME_VALID = " {{ trans("register.CHARACTER_VALID_MESSAGE") }} ";
		var MEMBER_LASTNAME_REQUIRED = " {{ trans("register.LASTNAME_REQUIRED_MESSAGE") }} ";
		var MEMBER_LASTNAME_VALID = " {{ trans("register.CHARACTER_VALID_MESSAGE") }} ";
		var MEMBER_GENDAR_REQUIRED = " {{ trans("register.GENDAR_REQUIRED_MESSAGE") }} ";
		var MEMBER_MOBILE_REQUIRED = " {{ trans("register.MOBILE_REQUIRED_MESSAGE") }} ";
		var MEMBER_MOBILE_VALID = " {{ trans("register.MOBILE_VALID_MESSAGE") }} ";
		var MEMBER_DATE_REQUIRED = " {{ trans("register.DATE_REQUIRED_MESSAGE") }} ";
		var MEMBER_CHILD_REQUIRED = " {{ trans("register.COUNT_CHILD_REQUIRED_MESSAGE") }} ";
		var MEMBER_CHILD_TYPE_REQUIRED = " {{ trans("register.CHILD_TYPE_REQUIRED_MESSAGE") }} ";
		var MEMBER_CAPTCHA_REQUIRED = " {{ trans("register.CAPTCHA_CODE_REQUIRED_MESSAGE") }} ";
		var MEMBER_MOM_STATUS_REQUIRED = " {{ trans("register.MOM_STATUS_REQUIRED_MESSAGE") }} ";
		var MESSAGE_DUPLICATE = "{{ trans("register.REGISTER_DUPLICATE_EMAIL") }}";
		var REGISTER_COMPLETE_MESSAGE = "{{ trans("register.REGISTER_COMPLETE_MESSAGE") }}";
		var REGISTER_EDIT_COMPLETE_MESSAGE = "{{ trans("register.REGISTER_EDIT_COMPLETE_MESSAGE") }}";
		var REGISTER_BUTTON_BACK_TO_HOME = "{{ trans("register.REGISTER_BUTTON_BACK_TO_HOME") }}";
		var REGISTER_BUTTON_EDIT = "{{ trans("register.REGISTER_BUTTON_EDIT") }}";
		var REGISTER_EDIT_SUCCESS = "{{ trans("register.REGISTER_EDIT_SUCCESS") }}";
		var REGISTER_BUTTON_FORGOT_PASSWORD = "{{ trans("register.REGISTER_BUTTON_FORGOT_PASSWORD") }}";
		var PREGNANT = "{{ trans("register.PREGNANT") }}";
		var PREGNANT_DATE = "{{ trans("register.PREGNANT_DATE") }}";
		var GIVE_BIRTH = "{{ trans("register.GIVE_BIRTH") }}";
		var CHILD_BIRTHDATE = "{{ trans("register.CHILD_BIRTHDATE") }}";
	</script>

@endsection
@compressJs("ddown,datepicker,jquery.validationEngine,languages/jquery.validationEngine-th,register_form")