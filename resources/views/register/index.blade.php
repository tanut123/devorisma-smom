@extends('template.master')

@compressCss("register_index")

@section('content')
	<div id="content-panel">
		<section id="section-gift">
			<div class="section-panel">
				<div class="section-panel-inner">
					<div class="panel-img"></div>
					<div class="panel-text">
						<div class="panel-text-inner">
						<div class="title FMonXBold">
							<div class="title-text"><span class="color-red text-free">ฟรี!</span> ของขวัญสำหรับ</div>
							<div class="subtitle-text">คุณแม่ตั้งครรภ์</div>
						</div>
						<div class="desc FMonX">
							<span class="FMonXBold">สมัครสมาชิกฟรี...ง่ายๆ</span> เพียงสแกน<br/><span class="FMonXBold">QR Code</span> หรือแอดไลน์ <a href="http://line.me/ti/p/@S-MomClub" alt="S-MomClub" target="_blank" class="FMonXBold">@S-MomClub</a><br/>และพิมพ์ <span class="FMonXBold color-red">“ยืนยัน”</span> พร้อมชื่อ-สกุล<br/> เบอร์โทรศัพท์ เพื่อรับของพรีเมี่ยม
						</div>
						<div class="remark-text FMonXMed">*ตามเงื่อนไขที่กำหนด</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</section>
		<section id="section-momtip">
			<div class="section-panel">
				<div class="section-panel-inner">
					<div class="panel-img"></div>
					<div class="panel-text">
						<div class="panel-text-inner">
						<div class="title FMonXBold">
							<div class="title-text">สมัครสมาชิก<span class="color-red">ฟรี</span>...<span class="FMonX">ง่ายๆ</span></div>
						</div>
						<div class="desc FMonX">
							เพียงสแกน <span class="FMonXBold">QR Code</span> หรือแอดไลน์ <br/><a href="http://line.me/ti/p/@S-MomClub" alt="S-MomClub" target="_blank" class="FMonXBold">@S-MomClub</a> และพิมพ์ <span class="color-red FMonXBold">“ยืนยัน”</span> <br/>พร้อมชื่อ-สกุล  เบอร์โทรศัพท์ <br/>เพื่อรับของพรีเมี่ยม
						</div>
						<div class="remark-text FMonXMed">*ตามเงื่อนไขที่กำหนด</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</section>
		<section id="section-1">
			<div class="section-panel">
				<div class="section-inner-panel">
					<div class="content-left">
						<div class="wrap-inner">
							<img class="img1 desktop" src="{{ asset($BASE_CDN . '/images/register/index/img-welcome.png') }}?v=2" width="930" height="508" alt="s-momclub-register">
							<img class="img1 mobile" src="{{ asset($BASE_CDN . '/images/register/index/img-welcome-mobile.png') }}?v=2" width="640" height="622" alt="s-momclub-register">
						</div>
					</div>
					<div class="content-right">
						<div class="wrap-inner">
							<span class="text-1 FLighter">ยินดีต้อนรับสมาชิกใหม่</span>
							<span class="text-2 FLighter">
								สู่ครอบครัว <img src="{{ asset($BASE_CDN . '/images/register/index/logo.png') }}" width="233" height="38">
							</span>
							<div class="highlight-box">
								<span class="title FMonXBold">
									สมัครสมาชิก<span class="color-red">ฟรี</span><span class="FMonX">...ง่ายๆ</span>
								</span><br/>
								<span class="subtitle FMonXMed">เพียงสแกน QR code หรือแอดไลน์ <br class="visible-xs"><a alt="S-MomClub" href="http://line.me/ti/p/@S-MomClub" alt="S-MomClub" target="_blank">@S-MomClub</a></span>
							</div>
							<div class="desc FX">
								กดเพิ่มเพื่อนและพิมพ์ <span class="color-red">“ยืนยัน”</span> พร้อม <br class="visible-365" />ชื่อ-สกุล เบอร์โทรศัพท์ <br class="hidden-xs"/>เพื่อรับของพรีเมี่ยม
							</div>
							<div class="remark-text FX">*ตามเงื่อนไขที่กำหนด</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</section>
		<section id="section-register">
			<div id="register" class="section-panel">
				<div class="section-panel-inner">
					<div class="panel-text FMonX">
						<div class="title"></div>
						<div class="subtitle FMonXMed">เพียงสแกน QR code หรือแอดไลน์ <br class="visible-xs"><a alt="S-MomClub" href="http://line.me/ti/p/@S-MomClub" alt="S-MomClub" target="_blank">@S-MomClub</a></div>
						<div class="desc">
							กดเพิ่มเพื่อนและพิมพ์ <span class="color-red">“ยืนยัน”</span> <br class="visible-365" />พร้อม ชื่อ-สกุล <br class="hidden-xs"/>เบอร์โทรศัพท์ <br class="visible-365" />เพื่อรับของพรีเมี่ยม
						</div>
						<div class="remark-text">*ตามเงื่อนไขที่กำหนด</div>
						<span class="btn-register FLighter">
							<a alt="S-MomClub" href="http://line.me/ti/p/@S-MomClub" alt="S-MomClub" target="_blank">
								สมัครสมาชิก
							</a>
						</span>
					</div>
					<div class="panel-img"></div>
				</div>
			</div>
		</section>
		<section id="section-2">
			<div id="privilege"></div>
			<div class="section-panel">
				<div class="section-inner-panel">
					<div class="title-panel">
						<span class="FLighter text-1 color-white">สิทธิประโยชน์<span class="FThin">และบริการ</span></span>
						<span class="FThin text-2 color-white">เพื่อคุณแม่คนพิเศษ</span>
					</div>
					<div id="content-list-panel">
						<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
							<div class="content-list-wrapper">
								<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon1.png') }}" class="img img-responsive" alt="img-hexagon1" width="160" height="142">
								<span class="text-panel color-gray">
									<span class="text1 FXMed">มอบของขวัญที่ระลึก</span><br/>
									<span class="text2 FLighter">สำหรับสมาชิกใหม่</span>
								</span>
							</div>
						</div>
						<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
							<div class="content-list-wrapper">
								<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon2.png') }}" class="img img-responsive" alt="img-hexagon2" width="159" height="142">
								<span class="text-panel color-gray">
									<span class="text1 FXMed">Line@ , Live Chat</span><br/>
									<span class="text2 FLighter">พูดคุยปรึกษากับพยาบาลและ<br/>ผู้เชี่ยวชาญตลอด <div class="img-24hr"></div></span>
								</span>
							</div>
						</div>
						<div class="clearfix visible-xs"></div>
						<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
							<div class="content-list-wrapper">
								<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon3.png') }}" class="img img-responsive" alt="img-hexagon3" width="161" height="142">
								<span class="text-panel color-gray">
									<span class="text1 FXMed">บริการให้คำปรึกษา</span><br/>
									<span class="text2 FLighter">ผ่านทางเว็บด้านโภชนาการ <br/>และพัฒนาการเด็ก</span><br/>
									<span class="text1 FXMed">S-MomClub.com</span>
								</span>
							</div>
						</div>
						<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
							<div class="content-list-wrapper">
								<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon4.png') }}" class="img img-responsive" alt="img-hexagon4" width="161" height="141">
								<span class="text-panel color-gray">
									<span class="text1 FXMed">ให้คำปรึกษาคุณแม่ตั้งครรภ์</span><br/>
									<span class="text2 FLighter">โทรศัพท์</span>
									<span class="text1 FXMed">0-26402288</span>
								</span>
							</div>
						</div>
						<div class="clearfix visible-xs"></div>
						<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
							<div class="content-list-wrapper">
								<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon5.png') }}" class="img img-responsive" alt="img-hexagon5" width="160" height="142">
								<span class="text-panel color-gray">
									<span class="text1 FXMed">จัดส่ง  SMS เคล็ดลับ</span><br/>
									<span class="text2 FLighter">การเลี้ยงดูและส่งเสริม<br/>พัฒนาการเด็ก</span>
								</span>
							</div>
						</div>
						<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
							<div class="content-list-wrapper">
								<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon7.png') }}" class="img img-responsive" alt="img-hexagon7" width="160" height="142">
								<span class="text-panel color-gray">
									<span class="text1 FXMed">Facebook/SmomClub</span><br/>
									<span class="text2 FLighter">พร้อมเสริมข้อมูลความรู้<br/>
									เพื่อศักยภาพที่ครบรอบ<br/>
									ด้านของลูกน้อย</span>
								</span>
							</div>
						</div>
						<div class="clearfix visible-xs"></div>
						<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
							<div class="content-list-wrapper">
								<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon9.png') }}" class="img img-responsive" alt="img-hexagon9" width="159" height="141">
								<span class="text-panel color-gray">
									<span class="text1 FXMed">มอบของขวัญพิเศษ</span>
									<span class="text2 FLighter"><br/>ครบรอบ 1 ปี</span>
								</span>
							</div>
						</div>
						<div class="content-list col-xs-6 col-sm-3 col-md-3 col-lg-3 col-no-padding">
							<div class="content-list-wrapper">
								<img src="{{ asset($BASE_CDN . '/images/member/img-hexagon10.png') }}" class="img img-responsive" alt="img-hexagon10" width="159" height="141">
								<span class="text-panel color-gray">
									<span class="text1 FXMed">สิทธิพิเศษเข้าร่วมกิจกรรม</span>
									<span class="text2 FLighter"><br/>สานสัมพันธ์แห่งรักกับเพื่อนๆ<br/>ครอบครัว S-MomClub</span>
								</span>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</section>
	</div>

<script type="text/javascript">

</script>
@endsection

@compressJs("register_index")