@extends('template.master')
@compressCss("ddown,validationEngine.jquery,jquery-ui,register_form")
@section('content')
<?php $PAGE_LANG = trans('core.CORE_LANG'); ?>
	<div id="register-panel">
		<div id="register-panel-inner">
			<form id="register-form">
				<input type="hidden" name="mode" value="add">
				<input type="hidden" name="member_type" value="1">
				<div class="main-title"><span class="FXregular">สมัคร</span><span class="FLighter">สมาชิกเว็บไซต์</span></div>
				<div id="status-mom" class="register-panel-box fix-line">
					<div class="title">
						<div class="img-icon icon-mom"></div><span class="text-title FLighter">{{ trans("register.MOM_STATUS") }}</span>
					</div>
					<div class="register-panel-bottom-inner">
						<div class="panel-information">
							<span class="data-left FLighter">
								{{ trans("register.CHOOSE_MOM_STATUS") }}
							</span>
							<span class="data-right">
								<div class="radio-box ">
									<input class="input-radio" type="radio" name="status-mom-choice" id="status-mom-choice1" value="1"><label for="status-mom-choice1" class="text-radio FLighter" ><span></span>{{ trans("register.PREGNANCY_MOM") }}</label>
								</div>
								<div class="radio-box ">
									<input class="input-radio" type="radio" name="status-mom-choice" id="status-mom-choice2" value="2"><label for="status-mom-choice2" class="text-radio FLighter" ><span></span>{{ trans("register.LACTATING_MOM") }}</label>
								</div>
								<div class="radio-box ">
									<input class="input-radio" type="radio" name="status-mom-choice" id="status-mom-choice3" value="3"><label for="status-mom-choice3" class="text-radio FLighter" ><span></span>{{ trans("register.TODDLER_MOM") }}</label>
								</div>
								<input type="text" class="hidden-value validate[required]" name="status_mom" id="status_mom" tabindex="6" value="">
							</span>
							<span class="clearfix"></span>
							<div class="panel-eating"></div>
							<span class="clearfix"></span>
						</div>
					</div>
				</div>
				<div class="register-panel-bottom fix-line">
					<div class="title">
						<div class="img-icon icon-kid"></div><span class="text-title FLighter">{{ trans("register.CHILD_DATA") }}</span>
					</div>
					<div class="countChild">
						<span class="FLighter">{{ trans("register.CHILD_AMOUNT") }}</span>
						<div class="ddChild">
							<div class="dropdown-list">
									<span class="dropdown-list-text FLighter">{{ trans("register.SELECT_CHILD_AMOUNT") }}</span>
									<span class="btn-list">
										<span class="btn-img"></span>
									</span>
							</div>
							<div class="dropdown-list-data">
								<ul class="dropdown-list-data-inner">
									<li data-val="1" class="FLighter">1 {{ ($PAGE_LANG=="th")?"คน":"person" }}</li>
									<li data-val="2" class="FLighter">2 {{ ($PAGE_LANG=="th")?"คน":"persons" }}</li>
									<li data-val="3" class="FLighter">3 {{ ($PAGE_LANG=="th")?"คน":"persons" }}</li>
									<li data-val="4" class="FLighter">4 {{ ($PAGE_LANG=="th")?"คน":"persons" }}</li>
									<li data-val="5" class="FLighter">5 {{ ($PAGE_LANG=="th")?"คน":"persons" }}</li>
								</ul>
							</div>
						</div>
						<input type="text" class="hidden-value validate[required]" id="check-ddown" tabindex="11" readonly>
					</div>
					<div id="register-panel-bottom-child">
					</div>
				</div>
				<div class="register-panel-top">
					<div class="title">
						<div class="img-icon icon-setting"></div><span class="text-title FLighter">{{ trans("register.SETTING_ACCOUNT") }}</span>
					</div>
					<div class="register-panel-top-left">
						<div class="input-panel">
							<div class="input-text FLighter">{{ trans("register.DISPLAYNAME") }}</div>
							<input class="input-data FLighter validate[required]" type="text" id="displayname" name="displayname" tabindex="1">
						</div>
						<div class="input-panel">
							<div class="input-text FLighter">{{ trans("register.EMAIL") }}</div>
							<input class="input-data FLighter validate[required, custom[email]]" type="text" name="email" id="email" tabindex="2">
							<input type="hidden" name="email_old" tabindex="2">
						</div>
						<div class="input-panel password">
							<div class="input-text FLighter">{{ trans("register.PASSWORD") }}</div>
							<input class="input-data FLighter validate[required, funcCall[checkValidatePassword]]" type="password" name="password" id="password" maxlength="20" tabindex="3" autocomplete="off">
						</div>
						<div class="check-strength">
							<span class="text-strength FLighter">{{ trans("register.STRENGTH_TEXT") }}</span>
							<div class="clearfix"></div>
							<div id="strength-password">
								<div class="strength lv1"></div>
								<div class="strength lv2"></div>
								<div class="strength lv3"></div>
								<div class="strength lv4"></div>
								<div class="FLighter" id="checkpw"></div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="input-panel">
							<div class="input-text FLighter">{{ trans("register.CONFIRM_PASSWORD") }}</div>
							<input class="input-data FLighter validate[required,equals[password]]" type="password" id="confirm-password" maxlength="20" tabindex="4" autocomplete="off">
						</div>
						<span class="FLighter" id="checkmatch"></span>
					</div>
					<div class="register-panel-top-right">
						<div class="register-panel-top-right-inner">
							<div class="guide-title-bar">
								<div class="icon-key"></div><span class="guide-title FXMed">คำแนะนำ</span>
							</div>
							<span class="FLighter">ในการสมัครสมาชิกเว็บไซต์ เพื่อเข้าใช้งาน กรุณากรอกรายละเอียดด้านซ้ายมือให้ครบถ้วน เพื่อความสมบูรณ์ของข้อมูลผู้ใช้งาน</span>
							<ul class="guide-panel FLighter">
								<li class="guide-box">
									<span class="guide-text">
										<span class="title-guide FLighter">อีเมล</span><span class="subtitle-guide FLighter"> - กรอกอีเมลของคุณ เช่น example@mail.com</span>
									</span>
								</li>
								<li class="guide-box">
									<span class="guide-text">
										<span class="title-guide FLighter">รหัสผ่าน</span><span class="subtitle-guide FLighter"> - กรุณาระบุรหัสผ่าน โดยต้องมีความยาวไม่ต่ำกว่า 10 ตัวอักษร แต่ไม่เกิน 20 ตัวอักษร ตัวอักษรพิมพ์เล็กอย่างน้อย 1 ตัวอักษร ตัวอักษรพิมพ์ใหญ่อย่างน้อย 1 ตัวอักษร ต้องมีตัวเลขอย่างน้อย 1 หมายเลข (0-9) และต้องมีอย่างน้อย 1 อักขระพิเศษ</span>
									</span>
								</li>
								<li class="guide-box">
									<span class="guide-text">
										<span class="title-guide FLighter">ยืนยันรหัสผ่าน</span><span class="subtitle-guide FLighter"> - ระบุรหัสผ่านอีกครั้ง เพื่อยืนยันรหัสผ่าน</span>
									</span>
								</li>
							</ul>
						</div>
					</div>
					<div class="clearfix" id="bfAllow"></div>
					<div class="box-allow">
						<div class="icon-allow allow"></div>
						<input type="hidden" id="allow" name="member_card_register" value="1">
						<div class="panel-text-allow">
							<span class="text-allow FLighter">{{ trans("register.REGISTER_SMOM_CLUB") }}</span>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="register-panel-middle">
					<div class="title">
						<div class="img-icon icon-form"></div><span class="text-title FLighter">{{ trans("register.PROFILE") }}</span>
					</div>
					<div class="register-panel-middle-inner">
						<div class="input-panel-middle">
							<div class="input-panel-middle-inner">
								<div class="input-text FLighter">{{ trans("register.TITLE") }}</div>
								<div class="radio-panel">
									<div class="radio-box">
										<input class="input-radio" type="radio" name="title-choice" id="t1" value="1"><label for="t1" class="text-radio FLighter"><span></span>{{ trans("register.TITLE_MR") }}</label>
									</div>
									<div class="radio-box">
										<input class="input-radio" type="radio" name="title-choice" id="t2" value="2"><label for="t2" class="text-radio FLighter"><span></span>{{ trans("register.TITLE_MRS") }}</label>
									</div>
									<div class="radio-box">
										<input class="input-radio" type="radio" name="title-choice" id="t3" value="3"><label for="t3" class="text-radio FLighter"><span></span>{{ trans("register.TITLE_MS") }}</label>
									</div>
									<input type="text" class="hidden-value validate[required]" name="title" id="title" tabindex="5" >
								</div>
							</div>
							<div class="input-panel-middle-inner">
								<div class="input-text FLighter">{{ trans("register.GENDAR") }}</div>
								<div class="radio-panel">
									<div class="radio-box">
										<input class="input-radio" type="radio" name="gendar-choice" id="g1" value="1"><label for="g1" class="text-radio FLighter"><span></span>{{ trans("register.GENDAR_MALE") }}</label>
									</div>
									<div class="radio-box">
										<input class="input-radio" type="radio" name="gendar-choice" id="g2" value="2"><label for="g2" class="text-radio FLighter"><span></span>{{ trans("register.GENDAR_FEMALE") }}</label>
									</div>
									<input type="text" class="hidden-value validate[required]" name="gendar" id="gendar" tabindex="8" >
								</div>
							</div>
						</div>
						<div class="input-panel-middle">
							<div class="input-panel-middle-inner" id="box-fname">
								<div class="input-text FLighter">{{ trans("register.FIRSTNAME") }}</div>
								<input class="input-data FLighter validate[required, custom[onlyLetterNumberCus]]" type="text" id="firstname" name="firstname" tabindex="6">
							</div>
							<div class="input-panel-middle-inner" id="box-mobile">
								<div class="input-text FLighter">{{ trans("register.TELEPHONE_NUMBER") }}</div>
								<input class="input-data FLighter validate[required, custom[phone]]" type="text" id="mobile" name="mobile" tabindex="9" maxlength="10">
							</div>
						</div>
						<div class="input-panel-middle">
							<div class="input-panel-middle-inner" id="box-lname">
								<div class="input-text FLighter">{{ trans("register.LASTNAME") }}</div>
								<input class="input-data FLighter validate[required, custom[onlyLetterNumberCus]]" type="text" id="lastname" name="lastname" tabindex="7">
							</div>
							<div class="input-panel-middle-inner" id="box-birthday">
								<div class="input-text FLighter">{{ trans("register.BIRTH_DATE") }}</div>
								<input type="text" class="input-date input-data FLighter validate[required] datepicker" name="birthday" id="birthday" tabindex="10" readonly>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class='captcha_panel'>
					<div class="captcha-text FLighter">{{ trans("register.CAPTCHA_TEXT") }}</div>
					<div class='captcha_image'>
						<img src='{{url()}}/captcha/[[captcha_date]]' id='captcha_img'><img src='{{ asset($BASE_CDN . '/images/register/capcha_refresh.svg') }}' class='captcha_refresh'>
					</div>
					<input class="input-data FLighter capcha_code validate[required]" type="text" name="captcha_code" id="captcha_code" tabindex="22">
				</div>
				<div class="panel-btn">
					<div class="btn-submit FLighter">ยืนยัน</div>
				</div>
			</form>
		</div>
	</div>

		<script type="text/javascript">
		var MEMBER_DISPLAYNAME_REQUIRED = "{{ trans("register.DISPLAYNAME_REQUIRED_MESSAGE") }}";
		var MEMBER_EMAIL_REQUIRED = " {{ trans("register.EMAIL_REQUIRED_MESSAGE") }} ";
		var MEMBER_EMAIL_VALID = " {{ trans("register.EMAIL_VALID_MESSAGE") }} ";
		var MEMBER_PASSWORD_REQUIRED = " {{ trans("register.PASSWORD_REQUIRED_MESSAGE") }} ";
		var MEMBER_PASSWORD_VALID = " {{ trans("register.PASSWORD_VALID_MESSAGE") }} ";
		var MEMBER_CONFIRM_PASSWORD_VALID = " {{ trans("register.CONFIRM_PASSWORD_VALID_MESSAGE") }} ";
		var MEMBER_TITLE_REQUIRED = " {{ trans("register.TITLE_REQUIRED_MESSAGE") }} ";
		var MEMBER_FIRSTNAME_REQUIRED = " {{ trans("register.FIRSTNAME_REQUIRED_MESSAGE") }} ";
		var MEMBER_FIRSTNAME_VALID = " {{ trans("register.CHARACTER_VALID_MESSAGE") }} ";
		var MEMBER_LASTNAME_REQUIRED = " {{ trans("register.LASTNAME_REQUIRED_MESSAGE") }} ";
		var MEMBER_LASTNAME_VALID = " {{ trans("register.CHARACTER_VALID_MESSAGE") }} ";
		var MEMBER_GENDAR_REQUIRED = " {{ trans("register.GENDAR_REQUIRED_MESSAGE") }} ";
		var MEMBER_MOBILE_REQUIRED = " {{ trans("register.MOBILE_REQUIRED_MESSAGE") }} ";
		var MEMBER_MOBILE_VALID = " {{ trans("register.MOBILE_VALID_MESSAGE") }} ";
		var MEMBER_DATE_REQUIRED = " {{ trans("register.DATE_REQUIRED_MESSAGE") }} ";
		var MEMBER_CHILD_REQUIRED = " {{ trans("register.COUNT_CHILD_REQUIRED_MESSAGE") }} ";
		var MEMBER_CHILD_TYPE_REQUIRED = " {{ trans("register.CHILD_TYPE_REQUIRED_MESSAGE") }} ";
		var MEMBER_CAPTCHA_REQUIRED = " {{ trans("register.CAPTCHA_CODE_REQUIRED_MESSAGE") }} ";
		var MEMBER_MOM_STATUS_REQUIRED = " {{ trans("register.MOM_STATUS_REQUIRED_MESSAGE") }} ";
		var MESSAGE_DUPLICATE = "{{ trans("register.REGISTER_DUPLICATE_EMAIL") }}";
		var REGISTER_COMPLETE_MESSAGE = "{{ trans("register.REGISTER_COMPLETE_MESSAGE") }}";
		var REGISTER_EDIT_COMPLETE_MESSAGE = "{{ trans("register.REGISTER_EDIT_COMPLETE_MESSAGE") }}";
		var REGISTER_BUTTON_FORGOT_PASSWORD = "{{ trans("register.REGISTER_BUTTON_FORGOT_PASSWORD") }}";
		var PREGNANT = "{{ trans("register.PREGNANT") }}";
		var PREGNANT_DATE = "{{ trans("register.PREGNANT_DATE") }}";
		var GIVE_BIRTH = "{{ trans("register.GIVE_BIRTH") }}";
		var CHILD_BIRTHDATE = "{{ trans("register.CHILD_BIRTHDATE") }}";
	</script>


@endsection
@compressJs("ddown,datepicker,jquery.validationEngine,languages/jquery.validationEngine-th,register_form")