@extends('template.master')

@compressCss("register_policy")

@section('content')
<div id="main-policy-panel">
	<div id="main-policy-panel-inner">
		<div id="panel-box">
			<div id="panel-box-inner">
				<h1 id="main-policy-title" class="color-title FLighter">
					ข้อตกลงการใช้บริการเว็บไซต์ สำหรับบุคคลทั่วไป
				</h1>
				<h2 id="main-policy-description" class="color-description FLighter">
					ข้อตกลงการใช้บริการเว็บไซต์ สำหรับบุคคลทั่วไป
					<span class="color-title">
						ข้อตกลงการใช้บริการเว็บไซต์ สำหรับบุคคลทั่วไป
					</span>
				</h2>
				<ul id="list-policy" class="list-panel FLighter">
					<li class="list color-descritpion">
						<span class="list-title color-title">
						1. ข้อตกลงการใช้บริการเว็บไซต์ สำหรับบุคคลทั่วไป
						</span>
						ข้อตกลงการใช้บริการเว็บไซต์ สำหรับบุคคลทั่วไป
					</li>
					<li class="list color-descritpion">
						<span class="list-title color-title">
						2. ข้อตกลงการใช้บริการเว็บไซต์ สำหรับบุคคลทั่วไป
						</span>
						ข้อตกลงการใช้บริการเว็บไซต์ สำหรับบุคคลทั่วไป
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>
@endsection
