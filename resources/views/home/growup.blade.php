@if(App::getLocale() == 'th')
<div id="growup-panel" class="{{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
	<div id="growup-content1-panel" class="lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/bg-growup-content1_new.png [--mb] | {{ $BASE_CDN }}/images/home/growup/bg-growup-content1_new.png" data-expand="+10">

		<!-- <div id="growup-content1" class="lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/bg-growup-content1-mobile-top.png [--mb]"" data-expand="+10">	 -->
			<div id="growup-content1">
			<div id="growup-content1-header">
				<span id="growup-content1-header-text" class="FThin">
					<span class="FXregular">สร้างลูก…</span>สมองไว ใน
				</span>
				<div id="growup-content1-header-img" class="lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/growupImageSprite_new.png" data-expand="+10">&nbsp;</div>
				{{--<img id="growup-content1-header-img" src="{{ $BASE_CDN }}/images/home/growup/1000-days.png" alt="1000-days">--}}
			</div>

			<div id="growup-content1-body">
				<div id="growup-content1-body-description-mobile" class="visible-xs">
					{{--<img class="line-top"  src="{{ $BASE_CDN }}/images/home/growup/line-mobile.png" alt="Line">--}}
                    <div class='line_active line-top'></div>
					<div id="mobile-mom-step1" class="list-mobile-mom list-mobile-mom-active">
						{{--<img class="img-description"  src="{{ $BASE_CDN }}/images/home/growup/growup-mom1-mobile.png" alt="growup-mom1">--}}
						<div class="img-description img-m1 lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/growupImageSprite_new.png" data-expand="+10">&nbsp;</div>
						<div class="text-box-mobile-panel">
							<span class="FXregular">ช่วงตั้งครรภ์</span>
							<br class="visible-xs"/>
							<span class="FThin">First 270 Days</span>
						</div>
					</div>
                    <div class='line_active line-bottom'></div>
					{{--<img class="line-bottom"  src="{{ $BASE_CDN }}/images/home/growup/line-mobile.png" alt="Line">--}}
					<div id="mobile-mom-step2" class="list-mobile-mom ">
						{{--<img class="img-description"  src="{{ $BASE_CDN }}/images/home/growup/growup-mom2-mobile.png" alt="growup-mom2">--}}
						<div class="img-description img-m2 lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/growupImageSprite_new.png [--mb]" data-expand="+10">&nbsp;</div>
						<div class="text-box-mobile-panel">
							<span class="FXregular">ช่วงแรกเกิด - 1 ขวบ</span>
							<br class="visible-xs"/>
							<span class="FThin">Second 365 Days</span>
						</div>
					</div>
					<div id="mobile-mom-step3" class="list-mobile-mom ">
						{{--<img class="img-description"  src="{{ $BASE_CDN }}/images/home/growup/growup-mom3-mobile.png" alt="growup-mom3">--}}
						<div class="img-description img-m3 lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/growupImageSprite_new.png [--mb]" data-expand="+10">&nbsp;</div>
						<div class="text-box-mobile-panel">
							<span class="FXregular">ช่วง 1 - 2 ขวบ</span>
							<br class="visible-xs"/>
							<span class="FThin">Final 365 Days</span>
						</div>
					</div>
					<div id="active-content" class="visible-xs">
                        <div class='active-mobile'></div>
						{{--<img width="32"  src="{{ $BASE_CDN }}/images/home/growup/active-mobile.png" alt="Active">--}}
					</div>
				</div>
				<div id="growup-content1-body-description" class="hidden-xs">
					{{--<img id="growup-content1-body-description-line" class="img-description"  src="{{ $BASE_CDN }}/images/home/growup/dot-line.png" alt="dot-line">--}}
					<div id="growup-content1-body-description-line" class="img-description"></div>
					<div id="growup-content1-body-description-img1" class="img-description">
						<div  class="img-description img1 lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/growupImageSprite_new.png"></div>
						<div class="growup-content1-body-description-panel" >
							<div class="growup-content1-body-description-panel-stronger FXregular">ช่วงตั้งครรภ์ </div>
							<div class="growup-content1-body-description-panel-lighter FThin">First 270 Days</div>
						</div>
					</div>
					<div id="growup-content1-body-description-img2" class="img-description">
						{{--<img  src="{{ $BASE_CDN }}/images/home/growup/growup-mom2.png" alt="growup-mom2">--}}
						<div  class="img-description img2 lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/growupImageSprite_new.png"></div>
						<div class="growup-content1-body-description-panel">
							<div class="growup-content1-body-description-panel-stronger FXregular">ช่วงแรกเกิด - 1 ขวบ</div>
							<div class="growup-content1-body-description-panel-lighter FThin">Second 365 Days</div>
						</div>
					</div>
					<div id="growup-content1-body-description-img3" class="img-description">
						{{--<img class="img-description"  src="{{ $BASE_CDN }}/images/home/growup/growup-mom3.png" alt="growup-mom3">--}}
						<div  class="img-description img3 lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/growupImageSprite_new.png"></div>
						<div class="growup-content1-body-description-panel">
							<div class="growup-content1-body-description-panel-stronger FXregular">ช่วง 1 - 2 ขวบ</div>
							<div class="growup-content1-body-description-panel-lighter FThin">Final 365 Days</div>
						</div>
					</div>
				</div>
				<div id="growup-content1-body-timeline" class="hidden-xs lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/growupImageSprite_new.png" data-expand="+30">
					<div id="line-panel"></div>
					<span id="ball-growup-active" class="ball-active"></span>
					<span id="growup1" class="ball-list big-ball"></span>
					<span id="growup2" class="ball-list small-ball"></span>
					<span id="growup3" class="ball-list small-ball"></span>
					<span id="growup4" class="ball-list small-ball"></span>
					<span id="growup5" class="ball-list big-ball"></span>
					<span id="growup6" class="ball-list small-ball"></span>
					<span id="growup7" class="ball-list small-ball"></span>
					<span id="growup8" class="ball-list small-ball"></span>
					<span id="growup9" class="ball-list big-ball"></span>
					<span id="growup10" class="ball-list small-ball"></span>
					<span id="growup11" class="ball-list small-ball"></span>
					<span id="growup12" class="ball-list small-ball"></span>
				</div>
			</div>
		</div>
	</div>
	<div id="growup-content2-panel" class="lazyload" data-bgset="{{ $BASE_CDN }}/images/home/growup/bg-growup-content2_new.jpg" data-expand="+10">
		<div id="growup-content2">
			<ul id="growup-content2-list">
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" class="lazyload" data-expand="+10" alt="logo-270.png" >
							<div class="list-text-left">
								<span class="FXregular title1 title-list">First</span>
								<span class="FThin title2 title-list">270 <span>Days</span></span>
								<span class="FThin title3 title-list">จากแม่สู่ลูกน้อยในครรภ์</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/first-270-days" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('270 วันแรกในระหว่างการตั้งครรภ์ เป็นช่วงเวลาของการสร้างสมองลูก คุณแม่ควรได้รับสารอาหารต่างๆ อย่างเพียงพอ โดยเฉพาะโปรตีนที่มีผลโดยตรงต่อการสร้างและพัฒนาสมองของลูก',200)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/first-270-days" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png" class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">First</span>
								<span class="FThin title2 title-list">1 - 90  <span>Days</span></span>
								<span class="FThin title3 title-list">ช่วงเริ่มสร้างอวัยวะของลูกน้อย</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/first-270-days/day-1-90" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('ตั้งครรภ์วันที่ 1 - 90 ช่วงเริ่มสร้างอวัยวะของลูกน้อย  คุณแม่ตั้งครรภ์จำเป็นต้องได้รับโฟเลตเพื่อช่วยในการสร้างหลอดประสาท และสมองที่สมบูรณ์',200)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/first-270-days/day-1-90" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png"  class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">First</span>
								<span class="FThin title2 title-list">91 - 180  <span>Days</span></span>
								<span class="FThin title3 title-list">ช่วงอวัยวะลูกขยายขนาด</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/first-270-days/day-91-180" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('ตั้งครรภ์วันที่ 91 - 180 ช่วงอวัยวะลูกขยายขนาด  คุณแม่ตั้งครรภ์ควรได้รับ DHA แอลฟา-แล็คตัลบูมิน แคลเซียม รวมถึง ธาตุเหล็ก ไอโอดีน และวิตามินต่างๆ ให้เพียงพอ',200)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/first-270-days/day-91-180" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png"  class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">First</span>
								<span class="FThin title2 title-list">181 - 270 <span>Days</span></span>
								<span class="FThin title3 title-list">ช่วงลูกเจริญเติบโต</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/first-270-days/day-181-270" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('ตั้งครรภ์วันที่ 181 - 270 ช่วงลูกเจริญเติบโต  สำหรับไตรมาสนี้เป็นช่วงเวลาสำคัญของการพัฒนาสมอง และระบบประสาทของลูก',130)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/first-270-days/day-181-270" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png"  class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">Second</span>
								<span class="FThin title2 title-list">365 <span>Days</span></span>
								<span class="FThin title3 title-list">ขวบปีแรก นมแม่ดีที่สุด</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/second-365-days" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('เป็นช่วงที่สมองของลูกพัฒนาอย่างรวดเร็วต่อเนื่อง ลูกควรได้รับนมแม่ให้นานที่สุด เพราะนมแม่อุดมด้วยสารอาหารสาคัญๆ ที่ช่วยให้ร่างกายและสมองของลูกน้อยเจริญเติบโตได้ดี โดยเฉพาะ “แอลฟา-แล็คตัลบูมิน”',200)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/second-365-days" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png" class="lazyload" data-expand="+10"/>
							<div class="list-text-left">
								<span class="FXregular title1 title-list">Second</span>
								<span class="FThin title2 title-list">271 - 453  <span>Days</span></span>
								<span class="FThin title3 title-list">มหัศจรรย์น้ำนมแม่</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/second-365-days/day-271-453" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('วันที่ 271 - 453 มหัศจรรย์น้ำนมแม่  กระตุ้นพัฒนาการของลูกน้อยวัย 6 เดือนแรก',130)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/second-365-days/day-271-453" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png"  class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">Second</span>
								<span class="FThin title2 title-list">454 - 544 <span>Days</span></span>
								<span class="FThin title3 title-list">เสริมสร้างพัฒนาการของลูกน้อย</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/second-365-days/day-454-544" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('วันที่ 454 - 544 เสริมสร้างพัฒนาการของลูกน้อย  กระตุ้นพัฒนาการรอบด้านของลูกน้อยวัย 7-9 เดือน',130)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/second-365-days/day-454-544" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png"  class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">Second</span>
								<span class="FThin title2 title-list">545 - 635 <span>Days</span></span>
								<span class="FThin title3 title-list">ส่งเสริมพัฒนาการของลูกอย่างต่อเนื่อง</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/second-365-days/day-545-635" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('วันที่ 545 - 635 ส่งเสริมพัฒนาการของลูกอย่างต่อเนื่อง  กระตุ้นพัฒนาการรอบด้านของลูกน้อยวัย 10-12 เดือน',130)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/second-365-days/day-545-635" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png"  class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">Final</span>
								<span class="FThin title2 title-list">365 <span>Days</span></span>
								<span class="FThin title3 title-list">ขวบปีที่สองช่วงเวลาแห่งพัฒนาการของลูกรัก</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/final-365-days" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('ช่วงที่ 3 ของ 1,000 วันแรก เป็นอีกหนึ่งช่วงเวลาทองที่สมองของลูกกำลังพัฒนาแบบก้าวกระโดดอย่างต่อเนื่อง จึงเป็นช่วงเวลาสำคัญสำหรับพัฒนาการหลายๆ ด้านของลูก ไม่ว่าจะเป็นด้านร่างกาย สมอง และความเป็นตัวตน',200)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/final-365-days" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png"  class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">Final</span>
								<span class="FThin title2 title-list">636 - 757 <span>Days</span></span>
								<span class="FThin title3 title-list">สู่เส้นทางการเป็นเด็กฉลาด</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/final-365-days/day-636-757" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('วันที่ 636 - 757 สู่เส้นทางการเป็นเด็กฉลาด  เตรียมพร้อมกระตุ้นพัฒนาการแต่ละช่วงเวลา',130)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/final-365-days/day-636-757" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png" class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">Final</span>
								<span class="FThin title2 title-list">758 - 878 <span>Days</span></span>
								<span class="FThin title3 title-list">เปิดประสบการณ์สู่โลกกว้างให้กับลูกน้อย</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/final-365-days/day-758-878" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('วันที่ 758 - 878 เปิดประสบการณ์สู่โลกกว้างให้กับลูกน้อย  เป็นการเปิดโอกาสให้ลูกได้มีประสบการณ์ และเห็นในสิ่งที่แตกต่าง ',130)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/final-365-days/day-758-878" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
				<li class="growup-list">
					<span class="growup-list-panel">
						<span class="growup-list-left">
							<img data-src="{{ $BASE_CDN }}/images/home/growup/logo-270.png" alt="logo-270.png" class="lazyload" data-expand="+10">
							<div class="list-text-left">
								<span class="FXregular title1 title-list">Final</span>
								<span class="FThin title2 title-list" style="font-size: 33px;">879 - 1,000 <span>Days</span></span>
								<span class="FThin title3 title-list">ถ่ายทอดความรักอันยิ่งใหญ่ของแม่</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/final-365-days/day-879-1000" class="button-growup FThin visible-xs">อ่านต่อ</a>--}}
							</div>
						</span>
						<span class="growup-list-right hidden-xs">
							<span class="text-wrap">
								<span class="FThin text-des">@trimWithDot('วันที่ 879 - 1,000 ถ่ายทอดความรักอันยิ่งใหญ่ของแม่  ด้วยการหมั่นแสดงความรัก และชมเชย',130)</span>
								{{--<a href="{{ $BASE_LANG }}1000-days/final-365-days/day-879-1000" class="button-growup FThin">อ่านต่อ</a>--}}
							</span>
						</span>
						<span class="clearfix"></span>
					</span>
				</li>
			</ul>
		</div>
	</div>
</div>
@endif