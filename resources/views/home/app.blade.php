<section id="detail-panel-app" class="detail-panel">
    <article class="detail-box">
        <div class="detail-content">
            <div class="text-app-box">
                <img class="text-app hidden-xs" src="{{ asset($BASE_CDN . '/images/app/text-app.png') }}" alt="text-app" /><img class="text-app visible-xs" src="{{ asset($BASE_CDN . '/images/app/text-app-mobile.png') }}" alt="text-app-mobile" />
                <div class="btn-app-bar">
                    <a class="btn-app-link" href="https://itunes.apple.com/app/id908937273" target="_blank">
                        <img class="btn-app ios" src="{{ asset($BASE_CDN . '/images/app/btn-ios.png') }}" alt="btn-app-ios" />
                    </a><a class="btn-app-link" href="https://play.google.com/store/apps/details?id=com.nestle.smomclub.phone&hl=en" target="_blank">
                        <img class="btn-app andriod" src="{{ asset($BASE_CDN . '/images/app/btn-andriod.png') }}" alt="btn-app-andriod" />
                    </a>
                </div>
            </div>
            <img class="img-hand hidden-xs" src="{{ asset($BASE_CDN . '/images/app/hand-desktop.png') }}" alt="hand-desktop" />
            <img class="img-hand visible-xs" src="{{ asset($BASE_CDN . '/images/app/hand-mobile.png') }}" alt="hand-mobile" />
        </div>
    </article>
</section>