<div id="trick-mom-panel" class="lazyload" data-bgset="{{ $BASE_CDN }}/images/home/trick-mom/bg-trick-mom.jpg" data-expand='+10'>
	<div id="trick-mom-content">
		<div id="trick-mom-header">
			<span id="trick-mom-header-left">
				<span class="FXregular">{{ (App::getLocale()=="en")?'Mom’s ':'เคล็ดลับ' }}{{ (App::getLocale()=="en")?'Tip':'คุณแม่' }}</span>
			</span>
			<span id="trick-mom-header-right">{{ (App::getLocale()=="en")?'Varieties of tips for being a new mom; pregnancy, baby care and development,
			breastfeeding and etc., available for you by professional S-MomClub team':'หลากหลายเคล็ดลับและความรู้จาก S-MomClub สำหรับคุณแม่ เริ่มตั้งแต่การเตรียมตัวเป็นคุณแม่มือใหม่ การตั้งท้อง พัฒนาการลูกน้อยในแต่ละช่วงวัย การให้นมแม่ และเคล็ดลับการเลี้ยงดูลูกน้อย' }}
			</span>
			<div class="clearfix"></div>
		</div>
		<div id="trick-mom-body">
			<?php echo $trick_mom?>
			<div class="clearfix"></div>
		</div>
		<div id="trick-mom-footer">
			<ul id="trick-mom-footer-list">
				<li id="list-footer-mom1" class="list-footer lazyload" data-bgset="{{ $BASE_CDN }}/images/home/trick-mom/mom1-mobile.jpg [--mb] | {{ $BASE_CDN }}/images/home/trick-mom/mom1.jpg" data-expand="+10">
					<a href="{{ $BASE_LANG }}pregnancy-mom" class="list-footer-link">
						{{--<img class="list-footer-img list-footer-img-desktop hidden-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/mom1.jpg">--}}
						{{--<img class="list-footer-img visible-xs" src="{{ urhttps://ais-callcenter.orisma.alpha/l() }}/images/home/trick-mom/mom1-mobile.jpg">--}}
						<span class="list-footer-text-panel">
							<span class="list-footer-text">
								{{ trans('menu.MENU_PREGNANCY_STAGE') }}
							</span>
							{{--<img class="img-link hidden-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/link-footer.png">--}}
							<div class="img-link hidden-xs img_link_footer lazyload" data-bgset="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABOUlEQVRIib2WPY6DMBCFreQQlD7BtiuaVOk4wfY0NOE8K1c0W3MQDkFDt9U2kSIhfVvkRUIk/JgkQ4MYz7wP28PDzi1cQAKcgBpogZ7r1eu51niypDUH8UAl0Q4IQA4cgVT3XPFOeRXgY0EFcAYaIAN2C/k75TWqK9ZA9nrTC1AuQSagpeoDsJ9LDsAfcIiBPNA5SCdMJRR6o6dAI+DlbknVDGegnCn+BL5nl+a+ppSuHwYrbe7kHgEfwC/wsxaoPWyA6hZI1LbZiuItwEz6idMH2a3tvFigZtcBJycHeNw1rwMGoHaynDwGFguU07RO63mMhcUAZW29k6mmW2AS+pLGpEXJSzGfmememXaj6Xdm5yAK2Hijgh4r19eAzf9skGDzp1aS3RlkUPT+09VIwGNxbhxBX3Yi/gdG5GZg/jMyRQAAAABJRU5ErkJggg==" data-expand="+10"></div>
							{{--<img class="img-link visible-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/link-footer-mobile.png" width="30">--}}
							<div class="img-link visible-xs img_link_footer_mobile lazyload" data-bgset="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAA9CAYAAAAeYmHpAAADOUlEQVRogeWbv04jMRDGVxdBpIjyaiQ6Cui4gvYqXiAddPS0SNTp6EBKmyolyhNAiZQHSBckKoqTkhSnIEVBvys81g6+/IO1dyPN16Cs7fm+Yb32eGxnWSIANeAEuALawBPwAkyAOQ5z+f0i5W2pfwLUUmmLCqAONIEHYEQxjMROE6hX7dt/APaBe3lry/ABDIE+8Aj05G9fnn+saDsR+/tV+5oBB0CHvLtqvErZBXAE7K6xtSv1LqTd6wKbcyk7KMtHLbAO3ADTQNQY912eRuI5FXvjgGcq/OV0e+AYGAQi3oBrYC8R557Yfwt4B8BxCk5Nfgm8K9IZ0AIaSYlz/obwzZSGd+AyBVkNuAv+y8/AYXSyzfQcCr/GHbGmOdwA0w0IboGdKATf17UjOjS6rBkwNzFcCxyeAeeRdEcBcB50926hNx506SlwFlFvNABnfJ5J7r5r6DJ4w1vpsIc4rt/41wY33LSkR+mt6tLLIF3d451NpzNc4KHn4dsIYooNLl/j0oPbgE0CGFyk4/FMwVEa+I0LKdMGETnfDp+ns5t1DQ7UgDCj4DwM/FL2/pTo+KH6vqesitVxwbxHKwJ5A7eSogLHW4q3s6zSPvlq6Y1IoWVVjguvj9XnLFqW4tarHtcJBFTh+LXivA8L6+QJgDEJVktVOI5bnfll6QQ9kuNSMh7thCKqcLyt+Jq64EEVREkArBBRquO4RITHg39YI0/ivaYiD4SU7bhPPY2AWoZLt3p0UhEvEFKa43yeik8yXJ7Z4yIF6QoxpTiOSzZ6XIUf+lFswg0EJXccl2X1aGe4nQVweefSFgaBqKSO47I/Pq/+lOG2VACGsUi+KSy140Ox+5KRByX9WAQFhCVzHLeTAjDJyOPtxxjGiyKV48rmPFPGezFExwDwE/irtA2AHwVt9rwxs2/a5DdtcvR+kh+m5mmTEZnJ2NvkKsveeloe2sqcSIHJHJm9bKhUsJX3lgr2djikoq29LKlob9dSGtjan5ZG9k4iSENbZ06UAVuni5QhW+fIxJi9E4Ni1NbZUEVg6xRwQGbnvHdAbOtkvxJh6w5HIMrObZ0Fgu3cywqBpRt4i8AW37X8B42D5L+Ku4BOAAAAAElFTkSuQmCC" data-expand="+10"></div>
						</span>
					</a>
				</li>
				<li id="list-footer-mom2" class="list-footer lazyload" data-bgset="{{ $BASE_CDN }}/images/home/trick-mom/mom2-mobile.jpg [--mb] | {{ $BASE_CDN }}/images/home/trick-mom/mom2.jpg" data-expand="+10">
					<a href="{{ $BASE_LANG }}lactating-mom" class="list-footer-link">
						{{--<img class="list-footer-img list-footer-img-desktop hidden-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/mom2.jpg">--}}
						{{--<img class="list-footer-img visible-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/mom2-mobile.jpg">--}}
						<span class="list-footer-text-panel">
							<span class="list-footer-text">
								{{ trans('menu.MENU_LACTATING_STAGE') }}
							</span>
							{{--<img class="img-link hidden-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/link-footer.png">--}}
							<div class="img-link hidden-xs img_link_footer lazyload" data-bgset="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABOUlEQVRIib2WPY6DMBCFreQQlD7BtiuaVOk4wfY0NOE8K1c0W3MQDkFDt9U2kSIhfVvkRUIk/JgkQ4MYz7wP28PDzi1cQAKcgBpogZ7r1eu51niypDUH8UAl0Q4IQA4cgVT3XPFOeRXgY0EFcAYaIAN2C/k75TWqK9ZA9nrTC1AuQSagpeoDsJ9LDsAfcIiBPNA5SCdMJRR6o6dAI+DlbknVDGegnCn+BL5nl+a+ppSuHwYrbe7kHgEfwC/wsxaoPWyA6hZI1LbZiuItwEz6idMH2a3tvFigZtcBJycHeNw1rwMGoHaynDwGFguU07RO63mMhcUAZW29k6mmW2AS+pLGpEXJSzGfmememXaj6Xdm5yAK2Hijgh4r19eAzf9skGDzp1aS3RlkUPT+09VIwGNxbhxBX3Yi/gdG5GZg/jMyRQAAAABJRU5ErkJggg==" data-expand="+10"></div>
							{{--<img class="img-link visible-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/link-footer-mobile.png" width="30">--}}
							<div class="img-link visible-xs img_link_footer_mobile lazyload" data-bgset="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAA9CAYAAAAeYmHpAAADOUlEQVRogeWbv04jMRDGVxdBpIjyaiQ6Cui4gvYqXiAddPS0SNTp6EBKmyolyhNAiZQHSBckKoqTkhSnIEVBvys81g6+/IO1dyPN16Cs7fm+Yb32eGxnWSIANeAEuALawBPwAkyAOQ5z+f0i5W2pfwLUUmmLCqAONIEHYEQxjMROE6hX7dt/APaBe3lry/ABDIE+8Aj05G9fnn+saDsR+/tV+5oBB0CHvLtqvErZBXAE7K6xtSv1LqTd6wKbcyk7KMtHLbAO3ADTQNQY912eRuI5FXvjgGcq/OV0e+AYGAQi3oBrYC8R557Yfwt4B8BxCk5Nfgm8K9IZ0AIaSYlz/obwzZSGd+AyBVkNuAv+y8/AYXSyzfQcCr/GHbGmOdwA0w0IboGdKATf17UjOjS6rBkwNzFcCxyeAeeRdEcBcB50926hNx506SlwFlFvNABnfJ5J7r5r6DJ4w1vpsIc4rt/41wY33LSkR+mt6tLLIF3d451NpzNc4KHn4dsIYooNLl/j0oPbgE0CGFyk4/FMwVEa+I0LKdMGETnfDp+ns5t1DQ7UgDCj4DwM/FL2/pTo+KH6vqesitVxwbxHKwJ5A7eSogLHW4q3s6zSPvlq6Y1IoWVVjguvj9XnLFqW4tarHtcJBFTh+LXivA8L6+QJgDEJVktVOI5bnfll6QQ9kuNSMh7thCKqcLyt+Jq64EEVREkArBBRquO4RITHg39YI0/ivaYiD4SU7bhPPY2AWoZLt3p0UhEvEFKa43yeik8yXJ7Z4yIF6QoxpTiOSzZ6XIUf+lFswg0EJXccl2X1aGe4nQVweefSFgaBqKSO47I/Pq/+lOG2VACGsUi+KSy140Ox+5KRByX9WAQFhCVzHLeTAjDJyOPtxxjGiyKV48rmPFPGezFExwDwE/irtA2AHwVt9rwxs2/a5DdtcvR+kh+m5mmTEZnJ2NvkKsveeloe2sqcSIHJHJm9bKhUsJX3lgr2djikoq29LKlob9dSGtjan5ZG9k4iSENbZ06UAVuni5QhW+fIxJi9E4Ni1NbZUEVg6xRwQGbnvHdAbOtkvxJh6w5HIMrObZ0Fgu3cywqBpRt4i8AW37X8B42D5L+Ku4BOAAAAAElFTkSuQmCC" data-expand="+10"></div>
						</span>
					</a>
				</li>
				<li id="list-footer-mom3" class="list-footer lazyload" data-bgset="{{ $BASE_CDN }}/images/home/trick-mom/mom3-mobile.jpg [--mb] | {{ $BASE_CDN }}/images/home/trick-mom/mom3.jpg" data-expand="+10">
					<a href="{{ $BASE_LANG }}toddler-mom" class="list-footer-link">
						{{--<img class="list-footer-img list-footer-img-desktop hidden-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/mom3.jpg">--}}
						{{--<img class="list-footer-img visible-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/mom3-mobile.jpg">--}}
						<span class="list-footer-text-panel">
							<span class="list-footer-text">
								{{ trans('menu.MENU_TODDLER_STAGE') }}
							</span>
							{{--<img class="img-link hidden-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/link-footer.png">--}}
							<div class="img-link hidden-xs img_link_footer lazyload" data-bgset="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABOUlEQVRIib2WPY6DMBCFreQQlD7BtiuaVOk4wfY0NOE8K1c0W3MQDkFDt9U2kSIhfVvkRUIk/JgkQ4MYz7wP28PDzi1cQAKcgBpogZ7r1eu51niypDUH8UAl0Q4IQA4cgVT3XPFOeRXgY0EFcAYaIAN2C/k75TWqK9ZA9nrTC1AuQSagpeoDsJ9LDsAfcIiBPNA5SCdMJRR6o6dAI+DlbknVDGegnCn+BL5nl+a+ppSuHwYrbe7kHgEfwC/wsxaoPWyA6hZI1LbZiuItwEz6idMH2a3tvFigZtcBJycHeNw1rwMGoHaynDwGFguU07RO63mMhcUAZW29k6mmW2AS+pLGpEXJSzGfmememXaj6Xdm5yAK2Hijgh4r19eAzf9skGDzp1aS3RlkUPT+09VIwGNxbhxBX3Yi/gdG5GZg/jMyRQAAAABJRU5ErkJggg==" data-expand="+10"></div>
							{{--<img class="img-link visible-xs" src="{{ $BASE_CDN }}/images/home/trick-mom/link-footer-mobile.png" width="30">--}}
							<div class="img-link visible-xs img_link_footer_mobile lazyload" data-bgset="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAA9CAYAAAAeYmHpAAADOUlEQVRogeWbv04jMRDGVxdBpIjyaiQ6Cui4gvYqXiAddPS0SNTp6EBKmyolyhNAiZQHSBckKoqTkhSnIEVBvys81g6+/IO1dyPN16Cs7fm+Yb32eGxnWSIANeAEuALawBPwAkyAOQ5z+f0i5W2pfwLUUmmLCqAONIEHYEQxjMROE6hX7dt/APaBe3lry/ABDIE+8Aj05G9fnn+saDsR+/tV+5oBB0CHvLtqvErZBXAE7K6xtSv1LqTd6wKbcyk7KMtHLbAO3ADTQNQY912eRuI5FXvjgGcq/OV0e+AYGAQi3oBrYC8R557Yfwt4B8BxCk5Nfgm8K9IZ0AIaSYlz/obwzZSGd+AyBVkNuAv+y8/AYXSyzfQcCr/GHbGmOdwA0w0IboGdKATf17UjOjS6rBkwNzFcCxyeAeeRdEcBcB50926hNx506SlwFlFvNABnfJ5J7r5r6DJ4w1vpsIc4rt/41wY33LSkR+mt6tLLIF3d451NpzNc4KHn4dsIYooNLl/j0oPbgE0CGFyk4/FMwVEa+I0LKdMGETnfDp+ns5t1DQ7UgDCj4DwM/FL2/pTo+KH6vqesitVxwbxHKwJ5A7eSogLHW4q3s6zSPvlq6Y1IoWVVjguvj9XnLFqW4tarHtcJBFTh+LXivA8L6+QJgDEJVktVOI5bnfll6QQ9kuNSMh7thCKqcLyt+Jq64EEVREkArBBRquO4RITHg39YI0/ivaYiD4SU7bhPPY2AWoZLt3p0UhEvEFKa43yeik8yXJ7Z4yIF6QoxpTiOSzZ6XIUf+lFswg0EJXccl2X1aGe4nQVweefSFgaBqKSO47I/Pq/+lOG2VACGsUi+KSy140Ox+5KRByX9WAQFhCVzHLeTAjDJyOPtxxjGiyKV48rmPFPGezFExwDwE/irtA2AHwVt9rwxs2/a5DdtcvR+kh+m5mmTEZnJ2NvkKsveeloe2sqcSIHJHJm9bKhUsJX3lgr2djikoq29LKlob9dSGtjan5ZG9k4iSENbZ06UAVuni5QhW+fIxJi9E4Ni1NbZUEVg6xRwQGbnvHdAbOtkvxJh6w5HIMrObZ0Fgu3cywqBpRt4i8AW37X8B42D5L+Ku4BOAAAAAElFTkSuQmCC" data-expand="+10"></div>
						</span>
					</a>
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
	</div>
</div>