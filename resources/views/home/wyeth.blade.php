<div class="{{ (strlen($home_banner) > 0)?'wyeth-panel':'wyeth-panel wyeth-panel-no-banner'}}">
    <div class="wyeth-panel-inner FX">
        <div class="wyeth-main-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/bg-wyeth-main-mobile.jpg')}} [--mb] | {{ asset($BASE_CDN . '/images/wyeth/bg-wyeth-main.jpg')}}"  data-expand="-10">
            <div class="background_mb lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/kidmom.png')}} [--mb]"  data-expand="-10">

            </div>
            <div class="wyeth-main-panel-inner">
                <div class="logo lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/wyeth_logo_w_4.png')}}"  data-expand="-10">

                </div>
                <div class="text-panel">
                   	ไวเอท นิวทริชั่น ผู้คิดค้นสูตรนมผงที่เหมาะสมสำหรับทารก <br class="hidden-xs">และไม่เคยหยุดยั้งในการพัฒนาสารอาหารมากกว่า 100 ปี จากความเข้าใจแม่
                </div>
                <div class="button-box">
                    <a href="/wyethnutrition#wyeth-article" class="button-link">
                        <div class="icon article lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}"  data-expand="-2">
                        </div><div class="text article">Article</div>
                    </a>
                    <a href="/wyethnutrition#wyeth-legacy" class="button-link">
                        <div class="icon legacy lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}"  data-expand="-2">
                        </div><div class="text legacy">Legacy</div>
                    </a>
                    <a href="/wyethnutrition#wyeth-contact" class="button-link">
                        <div class="icon contact lazyload" data-bgset="{{ asset($BASE_CDN . '/images/wyeth/icon.svg')}}"  data-expand="-2">
                        </div><div class="text">Contact us</div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>