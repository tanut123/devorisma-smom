<div class="question-article-panel">
	<div class="question-article-panel-inner">
		<div class="question-title"></div>
		<div class="question-panel-content">
			<div class="question-content-text">
				<div class="question-text-title FX">
					<span class="main FBold">ตอบจริงทุกคำถาม </span>
					<br>ไขทุกข้อสงสัย…หลากหลายคำถามคาใจของคุณแม่
					<br class="hidden-xs">ตั้งครรภ์ และพัฒนาการลูกน้อยในทุกช่วงวัย
				</div>
				<div class="question-text-desc">
					<ul class="text-profile">
						<li class="block-name">
							<div class="name FBold">นายแพทย์อติคุณ คณุตม์วงศ์</div>
							<div class="position FX">อาจารย์แพทย์ผู้เชี่ยวชาญด้านสูตินรีเวช</div>
						</li>
						<li class="block-name">
							<div class="name FBold">นายแพทย์วิทยา เพ็ชรดาชัย</div>
							<div class="position FX">กุมารแพทย์ผู้เชี่ยวชาญด้านกุมารเวชศาสตร์<br/>และเวชศาสตร์ครอบครัว</div>
						</li>
						<li class="block-name">
							<div class="name FBold">แพทย์หญิงเกศินี โอวาสิทธิ์</div>
							<div class="position FX">กุมารแพทย์ผู้เชี่ยวชาญด้านพัฒนาการ<br class="visible-mb">และพฤติกรรม</div>
						</li>
						<li class="block-name">
							<div class="name FBold">ทันตแพทย์ชัยรัตน์ ศรีสวรรค์</div>
							<div class="position FX">ทันตแพทย์ผู้เชี่ยวชาญด้านทันตกรรม</div>
						</li>
						<li class="block-name">
							<div class="name FBold">น.อ. แพทย์หญิง นวภรณ์ ออรุ่งโรจน์</div>
							<div class="position FX">แพทย์ผู้เชี่ยวชาญด้านสูตินรีเวช<br/>และเวชศาสตร์มารดาและทารกในครรภ์</div>
						</li>
					</ul>
					<a href="{{ url() }}/หมอเด็ก" class="button-link">
						<div class="button-page">
							<span class="button-text FXMed">ดูทั้งหมด</span>
						</div>
					</a>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="question-content-img"></div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>