<div id='kl_banner'>
    <div class='kl_panel {{ (App::getLocale()=="en")?"kl_panel_en":"" }}'>
        <div class='head_panel'>
            <div class='head_text'>
                <span class="header-left"><span class='bold'>{{ (App::getLocale()=="en")?'Reversal ':'พลิก' }}</span><span class='thin'>{{ (App::getLocale()=="en")?'Theory':'ทุกทฤษฎี' }}</span></span>
                <span class="header-right"><?=(App::getLocale()=="en")?'Meet the can’t-miss knowledge and secret know-how bringing<br>the best to your child':'รวมเกร็ดความรู้ดีๆ มากมาย ข่าวสารสุขภาพใหม่ๆ อัพเดทผลวิจัยทางการแพทย์ที่คุณพ่อและคุณแม่ไม่ควรพลาด เพื่อสร้างศักยภาพลูกน้อยสมองไว' ?></span>
                <span class="clearfix"></span>
            </div>
        </div>
        <div class='banner_panel'>
            <div class='left_banner'>
                <?php echo $kl_left ?>
            </div>
            <div class='right_banner'>
                <div class='right_top_banner'>
                    <div class='right_top_1'>
                        <?php echo $kl_right1 ?>
                    </div>
                    <div class='right_top_2'>
                        <?php echo $kl_right2 ?>
                    </div>
                </div>
                <div class='right_bottom_banner'>
                      <?php echo $kl_rightb ?>
                </div>
            </div>
        </div>
    </div>
</div>
@compressCss('home.kw_banner')
@compressJs('home.kw_banner',2)
