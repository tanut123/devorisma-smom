@extends('template.master')

@compressCss("slick,slick-theme,bigvideo,home.banner,home.growup,home.trick_mom,home.app,home.knowledge_list,home.question_article,home_wyeth")

@section('content')
<div class="wapContent">
	@include('home.banner')
	{{--@include('home.knowledge_banner')--}}
	@if(App::getLocale() == 'th')
		@include('home.wyeth')
	@endif
    <div class="home-middle">        
        {{--@include('home.knowledge_list')--}}
        @include('home.trick_mom')
    </div>
	@include('home.growup')
	{{--@include('home.app')--}}

</div>
<script>
	window.lazySizesConfig = window.lazySizesConfig || {};
	window.lazySizesConfig.customMedia = {
    	'--mb': '(max-width: 767px)'
	};
</script>
@endsection
@compressJs("slick.min",1)
@compressJs("lazy.bgset.min,lazysizes.min,jquery.textfill.min,video_plugin,bigvideo,jquery.tubular.1.0,home.knowledge_list,home.growup,home.trick_mom,home.banner,favorite,home.wyeth")
