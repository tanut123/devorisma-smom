<div class='knowledge-list-panel lazyload' data-bgset="{{ asset($BASE_CDN . '/images/home/knowledge_list/bg.jpg')}}" data-expand="-10">
	<div class='knowledge-list-panel-inner'>
		<div class="knowledge-list-header">
			<span class="knowledge-list-header-left">
				<span class="FXregular">{{ (App::getLocale()=="en")?'Secret ':'นานา' }}{{ (App::getLocale()=="en")?'Know-How':'สาระ' }}</span>
			</span>
			<span class="knowledge-list-header-right {{ (App::getLocale() == 'en')? 'kl_en':'' }}">
				{{ (App::getLocale() == 'en')? 'Miscellaneous Know-How ':'หลากหลายข้อมูลสาระดีๆ' }}
				<div class="txt-desc">
					{{ (App::getLocale() == 'en')? 'When a new member comes, your life will never be the same!':'สำหรับเจ้าตัวน้อยของแม่ ตั้งแต่วินาทีที่ลูกน้อยของคุณออกมาลืมตาดูโลก คุณจะรู้ว่าชีวิตของคุณแม่ จะไม่เหมือนเดิมอีกต่อไปแล้ว' }}
				</div>
			</span>
			<div class="clearfix"></div>
		</div>
		<div class="knowledge-list-body">
			<div class="knowledge-list-body-inner">
				<ul class="list-panel">
					 <?php echo $knowledge_list ?>
				</ul>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>