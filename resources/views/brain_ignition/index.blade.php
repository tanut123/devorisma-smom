@extends('template.master')
@compressCss("slick,slick-theme,bigvideo,brain_ignition")
@section('content')
	<div class="wapContent FX">
		<div class="inner-panel">
			<div class="static-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/bg-static-mobile.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/brain_ignition/bg-static.jpg') }}" data-expand="+10">
				<div class="static-panel-inner">
					<div class="title-panel">
						<div class="text-title">
							<img class="lazyload" data-src="{{ asset($BASE_CDN . '/images/brain_ignition/text-title.svg') }}" alt="" data-expand="+10">
						</div>
						<div class="text-subtitle line1 FXBold">
							<span>ลูกสมองไว คุณแม่สร้างได้ มาร่วมค้นหาศักยภาพความถนัดของลูก<br class="hidden-xs"/>
							ผ่านเช็คลิสต์พร้อมคำแนะนำเพื่อค้นหาและสนับสนุนศักยภาพลูกให้ถูกทางจาก</span>
						</div>
						<div class="text-subtitle line2">
							<span><span class="highlight-color FXBold">ผู้ช่วยศาสตราจารย์ ดร.สุธาวัลย์ หาญขจรสุข</span><br/>ผู้เชี่ยวชาญด้านพัฒนาอัจฉริยภาพ คณะศึกษาศาสตร์ มหาวิทยาลัยศรีนครินทรวิโรฒ</span>
						</div>
						<div class="text-subtitle line3">
							<span>เพราะพ่อแม่มีบทบาทสำคัญที่จะ <span class="highlight-color FXBold">“ปลุกแววลูก ให้เปลี่ยนโลก”</span></span>
						</div>
					</div>
					<div class="slide-panel">
						<?php echo $banner?>
					</div>
					<div class="button-panel">
						<a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก/แบบวัดแวว" class="button brain-test lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/btn-ignition.svg') }}" data-expand="+10">
							<div class="icon-btn-box">
								<div class="icon-btn brain-test lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/icon.svg') }}" data-expand="+10"></div>
							</div><div class="text-btn">
								เเบบทดสอบวัดเเวว
							</div>
						</a><div class="button brain-tip lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/btn-ignition.svg') }}" data-expand="+10">
							<div class="icon-btn-box">
								<div class="icon-btn brain-tip lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/icon.svg') }}" data-expand="+10"></div>
							</div><div class="text-btn">
								เคล็ดลับ Ignite ไม่ Ignore
							</div>
							<div class="arrow-selected"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="dynamic-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/bg-dynamic.png') }}" data-expand="+10">
				<div class="dynamic-panel-inner">
					<div class="dynamic-title FXMed">
						เคล็ดลับ Ignite ไม่ Ignore
					</div>
					<ul class="data-panel">

					</ul>
					<div class="clearfix"></div>
					<div class="button-loadmore FMonXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/btn-ignition.svg') }}" data-expand="+10" style="display:none;">Load more</div>
					{{-- <div class="brain-lab-panel">
						<div class="brain-lab-panel-inner">
							<div class="brain-lab-title FXMed">
								Brain Ignition Lab
							</div>
							<div class="brain-lab-subtitle FXMed">
								จุดประกายฉายแวว…แบบทดสอบ<br class="visible-xs"/>วัดศักยภาพความถนัดลูก
							</div>
							<div class="brain-lab-btn-panel">
								<div class="brain-lab-btn FXMed map">
									Brain Map
								</div><div class="brain-lab-btn FXMed briefed">
									To be briefed
								</div>
							</div>
						</div>
					</div> --}}
				</div>
			</div>
		</div>
	</div>
	<script>
		window.lazySizesConfig = window.lazySizesConfig || {};
		window.lazySizesConfig.customMedia = {
	    	'--mb': '(max-width: 767px)'
		};
	</script>
@endsection
@compressJs("lazy.bgset.min,lazysizes.min,slick.min,video_plugin,bigvideo,jquery.tubular.1.0,brain_ignition")