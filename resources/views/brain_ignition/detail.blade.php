@extends('template.master')
@compressCss("knowledge_detail")
@section('content')
	<div id="wapKnowledgeDetail">
		<div class="detail-panel">
			<div id="detail-content">
				<h1 id="detail-title">{{ $data["title"]}}</h1>
				<div id="detail-bar">
					<!-- <span id="detail-date" class="FThin">{{ $data["datestring"] }}</span> -->
					<script type="text/javascript">
						var page_uid = "{{$data['brain_ignite_tips_id']}}";
					</script>
					<span id="detail-views" class="FThin" style="visibility:hidden;">
					<img class="icon-view" src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-view.png') }}" alt="icon-view" width="25" height="14">
					<span id="total_view"> 0 </span> Views
					</span>
				</div>
				<div class="detail-wapper">
					@replaceStockUrl($data["detail"])
					<div class="clearfix"></div>
				</div>
				<a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก">
					<div class="btn-back">ย้อนกลับ</div>
				</a>
			</div>
		</div>
	</div>
@endsection
@compressJs("brain_ignition_detail")
