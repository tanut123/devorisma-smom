<?php
	if (!file_exists('stocks/certificate')) {
	    mkdir('stocks/certificate', 0777, true);
	}

	$upload_dir = "stocks/certificate/";
	$img = $_POST['imgData'];
	$imgName = $_POST['imgName'];
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);
	$data = base64_decode($img);
	$file = $upload_dir . $imgName . ".png";
	$success = file_put_contents($file, $data);

?>