@extends('template.master')
@compressCss("brain_ignition_certificate")
@section('content')
	<div id="panel-cer" class="panel">
		<div class="panel-inner FMonX">
			<input type="hidden" name="mode" value="<?=$mode;?>"/>
			<div class="certificate-box-panel">
			<div class="certificate-box">
				<div class="certificate-name">น้อง</div>
				<div class="certificate-type FMonXBold"><?=$mode;?></div>
				<div class="certificate-level FMonXBold"><?=$level;?></div>
			</div>
			</div>
			<canvas id="certificate-canvas" width="786" height="537" style="display: none;"></canvas>
			<div class="input-name-panel">
				<div class="input-name-title FMonXMed">
					“คุณแม่ใส่ชื่อเล่นน้องได้เลยค่ะ”
				</div>
				<div class="input-name-panel-inner">
					<div class="input-box">
						<input type="text" name="kid-name" id="kid-name" required/>
						<div class="input-placeholder">คุณแม่ใส่ชื่อเล่นน้องได้เลยค่ะ</div>
					</div>
				</div>
			</div>
			<div class="share-panel">
				<div class="btn-share facebook FMonXMed">
					แชร์ด้วย Facebook
				</div>
			</div>
		</div>
	</div>
@endsection
@compressJs("brain_ignition_certificate")
