@extends('template.master')
@compressCss("slick,slick-theme,brain_ignition_question")
@section('content')
	<div class="wapContent FX">
		<div class="inner-panel lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/question/bg-question-mobile.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/brain_ignition/question/bg-question.jpg') }}" data-expand="+10">
			<div class="static-panel">
				<div class="static-panel-inner">
					<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/brain_ignition/question/question-banner.svg') }}" />
					<img class="visible-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/brain_ignition/question/question-banner-mobile.svg') }}" />
					<div class="question-panel-desc FXBold">ขอสงวนสิทธิ์แบบทดสอบสำหรับคุณแม่ที่มีบุตรอายุ 1-5 ปี</div>
				</div>
			</div>
			<div class="question-panel">
				<div class="question-panel-inner">
					<div class="question-panel-title-box">
						<img class="hidden-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/brain_ignition/question/question-title.svg') }}" />
						<img class="visible-xs lazyload" data-expand="+10" data-src="{{ asset($BASE_CDN . '/images/brain_ignition/question/question-title-mobile.svg') }}" />
					</div>
					<div class="question-box">
						<a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก/แบบวัดแวว/ภาษา" class="question-box-list language">
							<div class="question-img language"></div>
							<div class="question-box-inner">
								<div class="question-text">
									<div class="question-title FMonXBold">การสื่อสาร<br/>และภาษา</div>
								</div>
							</div>
							<div class="btn-start FXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/question/btn-start.svg') }}" data-expand="+10">เริ่มทดสอบ</div>
						</a><a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก/แบบวัดแวว/ศิลปะ" class="question-box-list art">
							<div class="question-img art"></div>
							<div class="question-box-inner">
								<div class="question-text">
									<div class="question-title FMonXBold">ศิลปะ</div>
								</div>
							</div>
							<div class="btn-start FXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/question/btn-start.svg') }}" data-expand="+10">เริ่มทดสอบ</div>
						</a><a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก/แบบวัดแวว/ดนตรี" class="question-box-list music">
							<div class="question-img music"></div>
							<div class="question-box-inner">
								<div class="question-text">
									<div class="question-title FMonXBold">ดนตรี</div>
								</div>
							</div>
							<div class="btn-start FXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/question/btn-start.svg') }}" data-expand="+10">เริ่มทดสอบ</div>
						</a><a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก/แบบวัดแวว/คณิตศาสตร์" class="question-box-list math">
							<div class="question-img math"></div>
							<div class="question-box-inner">
								<div class="question-text">
									<div class="question-title FMonXBold">คณิตศาสตร์</div>
								</div>
							</div>
							<div class="btn-start FXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/question/btn-start.svg') }}" data-expand="+10">เริ่มทดสอบ</div>
						</a><a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก/แบบวัดแวว/วิทยาศาสตร์" class="question-box-list science">
							<div class="question-img science"></div>
							<div class="question-box-inner">
								<div class="question-text">
									<div class="question-title FMonXBold">วิทยาศาสตร์<br/>และนักประดิษฐ์์</div>
								</div>
							</div>
							<div class="btn-start FXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/question/btn-start.svg') }}" data-expand="+10">เริ่มทดสอบ</div>
						</a><a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก/แบบวัดแวว/ความเป็นผู้นำ" class="question-box-list leader">
							<div class="question-img leader"></div>
							<div class="question-box-inner">
								<div class="question-text">
									<div class="question-title FMonXBold">ความเป็นผู้นำ</div>
								</div>
							</div>
							<div class="btn-start FXMed lazyload" data-bgset="{{ asset($BASE_CDN . '/images/brain_ignition/question/btn-start.svg') }}" data-expand="+10">เริ่มทดสอบ</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		window.lazySizesConfig = window.lazySizesConfig || {};
		window.lazySizesConfig.customMedia = {
	    	'--mb': '(max-width: 767px)'
		};
	</script>
@endsection
@compressJs("lazy.bgset.min,lazysizes.min,slick.min,brain_ignition_question")