@extends('template.master')


@compressCss("custom_page,miracle2")

@section('content')
<div id="detail-panel" class="wapContent">
	<div id="detail-box">
		<div id="detail-content1">
			<div id="detail-inner-content1">
				<h1>
					<img src="{{ asset($BASE_CDN . '/images/miracle2/the-miracle-of-sleeping-1.png') }}" alt="The Miracle of sleeping" title="The Miracle of sleeping" class="img-responsive img-title">
				</h1>
			</div>
		</div>
		<div id="detail-content2">
			<div id="detail-inner-content2">
				<div id="paragraph-1">
					<span class="paragraph-1-1 FXregular">
						พลิกทุกทฤษฎี คลื่นสมองทารกวิ่งปรู๊ดแม้ตอนนอน
					</span>
					<span class="paragraph-1-2 FXregular">
						ดูดนมแม่ มีแอลฟา-แล็คตัลบูมิน ช่วยลูกหลับนานขึ้น เรียนรู้ดีขึ้นเมื่อตื่น
					</span>
				</div>
				<div id="paragraph-2">
					<span class="paragraph-2-1 FXregular">
						อเมซิ่งสมองของลูกสามารถเรียนรู้และพัฒนาได้ตลอดเวลาทั้งกลางวันและกลางคืน เพราะนอกจากเด็กจะเรียนรู้และมี
พัฒนาการผ่านการทำกิจกรรมในเวลากลางวันแล้ว ช่วงเวลากลางคืนไม่เพียงแต่เป็นเวลาพักผ่อนแต่คลื่นสมองทารก
แรกคลอด 10 – 20 สัปดาห์ ไม่เคยอยู่นิ่งระหว่างนอน ทางการแพทย์เชื่อว่า การนอนหลับในช่วงแรกของทารกที่เรียก
ว่า REM (Rapid Eye Movement) เป็นช่วงที่สมองทารกมีการพัฒนา จากการศึกษาพบว่า เพียงครึ่งชั่วโมงแรกของ
การนอนกลางวัน ทารกจะขยับตัวเคลื่อนไหวเฉลี่ย 10.5 นาที และเพิ่มมากเป็น 24.4 นาที หากเป็นการนอนช่วงกลางคืน
นอกจากนี้งานวิจัยยังชี้ว่า ถ้าให้ทารกดื่มนมจากเต้านานขึ้นกว่าเดิม จะสามารถหลับได้นานขึ้น ส่งผลให้เรียนรู้ได้ดีขึ้นใน
เวลากลางวัน เพราะนมแม่มีแอลฟา-แล็คตัลบูมิน เป็นตัวช่วยสำคัญ
					</span>
				</div>
				<div id="paragraph-3">
					<div class="text-normal color-black FXregular">
						<span class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
						<p>“การนอนหลับ” ของทารกตัวน้อยไม่ใช่แค่การ
พักผ่อนทั่วไปเหมือนผู้ใหญ่ แต่ยังเป็นช่วงเวลาที่สมอง
ของลูกเก็บประสบการณ์ที่ได้เรียนรู้ในแต่ละวัน บันทึกไว้
เป็นความทรงจำระยะยาว ทำให้เด็กสามารถจดจำสิ่งต่างๆ
ได้ดีขึ้นด้วย มีงานวิจัยจากต่างประเทศพบว่า คลื่นสมอง
ของทารกระหว่างตื่นนอนและยามหลับมีลักษณะคล้ายคลึง
กันมาก โดยสมองของเด็กจะตื่นตัวและพร้อมเรียนรู้ตลอด
เวลาแม้ยามหลับ นักวิจัยได้ทำการวัดคลื่นสมองของทารก
แรกคลอด อายุ 10 - 20 สัปดาห์ ระหว่างนอนหลับพบว่า
คลื่นสมองจะไม่อยู่นิ่งเลย หากเป็นการนอนหลับในช่วง
กลางวัน ภายในครึ่งชั่วโมงทารกตัวน้อยจะมีการเคลื่อนไหว
ร่างกายเฉลี่ยแล้ว 10.5 นาที และหากเป็นช่วงกลางคืน
ทารกจะเคลื่อนไหวถึง 24.4 นาทีเลยทีเดียว</p>
						</span>
						<span class="col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">
							<img src="{{ asset($BASE_CDN . '/images/miracle2/baby-1.png') }}" alt="baby" title="baby" class="img-responsive img-baby hidden-xs">
							<img src="{{ asset($BASE_CDN . '/images/miracle2/baby-1-mobile.png') }}" alt="baby" title="baby" class="img-responsive img-baby visible-xs">
						</span>
						<div class="clearfix"></div>
					</div>
				</div>
				<div id="paragraph-4">
					<span class="paragraph-4-1 text-normal color-black FXregular">
								<p>การเคลื่อนไหวในช่วงแรกที่ลูกนอนหลับ หรือที่เราเรียกว่า REM (Rapid Eye Movement) นั้น เป็นช่วงเวลาที่
สมองของทารกจะมีการนำเอาประสบการณ์ที่ได้ในขณะตื่นมา ประมวลเป็นความจำ<sup>1</sup> และในช่วง REM นี้เอง ที่เลือดจะขึ้น
ไปเลี้ยงสมองเป็นจำนวนมาก ซึ่งจะเป็นผลดีต่อความสามารถในการเรียนรู้ของทารก ทำให้ในขณะตื่นเด็กจะตื่นตัว เกิด
ความสามารถในการเรียนรู้ และจัดการกับข้อมูลต่างๆ ที่ได้รับตลอดทั้งวัน มีความรู้สึก และไหวพริบปฏิภาณดี</p>
					</span>
				</div>
				<div id="paragraph-5" class="text-normal color-black FXregular">
					<p><img src="{{ asset($BASE_CDN . '/images/miracle2/alpha-lactalbumin.png') }}" alt="alpha-lactalbumin" title="alpha-lactalbumin" class="img-lactalbumin">โดยปกติ ทารกวัยแรกเกิด – 6 เดือนแรก จะมีการนอนช่วง REM ในตอนต้น จาก
						นั้นจะค่อยๆ เข้าสู่การหลับลึกหรือที่เรียกว่า Non-REM และทุกๆ 20 นาที ก็จะสลับกลับไปเป็น
						ช่วง REM หรืออาจจะเรียกได้ว่า ทารกวัยนี้มักจะมีการตื่นบ่อย ทุกๆ 20 นาที ซึ่งคุณพ่อ
						คุณแม่สามารถลดการตื่นบ่อย เพิ่มการนอนหลับให้นานขึ้นได้ เพียงแค่ให้ลูกได้ทานนมแม่นาน
						ขึ้นก่อนนอนนั่นเอง เพราะนมแม่มี แอลฟา-แล็คตัลบูมิน ซึ่งเป็นโปรตีนคุณภาพสูงที่ให้กรด
อะมิโนจำเป็น ชื่อ “ทริปโตแฟน” ที่เป็นสารตั้งต้นของสารสื่อประสาท ช่วยในการสื่อสารของเซลล์ประสาทและการทำงาน
ของสมอง และยังมีส่วนช่วยในควบคุมการนอนหลับ โดยร่นระยะเวาลานอน ให้หลับเร็วขึ้น<sup>2</sup><img src="{{ asset($BASE_CDN . '/images/miracle2/choline.png') }}" alt="choline" title="choline" class="img-choline"></p>
				</div>
				<div id="paragraph-6" class="text-normal color-black FXregular">
					<p>ทั้งนี้ มีงานวิจัยศึกษาพฤติกรรมการนอนของทารกวัย 3 เดือนที่ดื่มนมแม่ จำนวนทั้งสิ้น 16 คน แบ่งเป็น 2 กลุ่ม
กลุ่มแรกกินนมแม่อย่างเดียว กลุ่มที่ 2 กินนมผสมอย่างเดียว พบว่า เด็กที่ทานนมแม่จะมีการหลับได้ดีกว่า โดยสามารถ
ใช้เวลาเพียง 30 นาทีก็หลับได้ ขณะที่เด็กกินนมผสมจะใช้เวลากว่าจะหลับนานกว่า<sup>3</sup> นอกจากนี้ American Journal of
Clinical Nutrition ยังเคยตีพิมพ์ด้วยว่า จากการศึกษาการทำงานของแอลฟา-แล็คตัลบูมินในผู้ใหญ่ พบว่า แอลฟา-
แล็คตัลบูมิน สามารถช่วยเรื่องความจำ และลดปัญหาการนอนอย่างมีนัยสำคัญทางสถิติ</p>
				</div>
				<div id="paragraph-7">
					<img src="{{ asset($BASE_CDN . '/images/miracle2/baby-play.png') }}" alt="baby-play" title="baby-play" class="img-responsive img-baby-play hidden-xs">
					<img src="{{ asset($BASE_CDN . '/images/miracle2/baby-play-mobile.png') }}" alt="baby-play" title="baby-play" class="img-responsive img-baby-play visible-xs">
				</div>
				<div id="paragraph-8">
					<span class="paragraph-8-1 text-normal FXregular">เคล็ดลับคุณพ่อคุณแม่มือใหม่</span>
					<span class="text-normal color-black FXregular">
						<p>ช่วงกลางวัน โดยในช่วงเวลาที่ลูกน้อยตื่น คุณพ่อคุณแม่สามารถพัฒนาสมองผ่านการกระตุ้นประสาทสัมผัสทั้ง 5
ขณะเดียวกัน การกล่อมลูกนอนกลางวัน หรือกลางคืน ของคุณพ่อคุณแม่ ไม่เพียงแต่ช่วยให้ลูกน้อยได้พักผ่อนอย่าง
เต็มอิ่ม แต่ขณะเดียวกันยังอาจส่งเสริมให้เกิดการเรียนรู้ที่ดีขึ้นเมื่อตื่นนอนอีกด้วย</p>
					</span>
				</div>
				<div id="paragraph-9" class="FXregular">
					<div class="list-ref"><sup>1</sup>Tarullo AR, Balsam PD, and Fifer WP. Sleep and Infant Learning. Infant and Child Development 2011: 20; 35-46</div>
					<div class="list-ref"><sup>2</sup>Steiberg LA, O’Connell NC, Hatch TF, Picciano MF, Birch LL. Tryptophan intakes influences infants’ sleep latency 1992. J. Nutr: 122(9); 1781-91</div>
					<div class="list-ref"><sup>3</sup>J. Cubero1, V. Valero1, J. Sánchez2, M. Rivero3, H. Parvez1, A. B. Rodríguez1 & C. Barriga, The circadian rhythm of tryptophan in breast milk affects the rhythms of
 6-sulfatoxymelatonin and sleep in newborn 2005</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

