@extends('template.master')


@compressCss("custom_page")

@section('content')
<div id="detail-panel" class="wapContent">
	<div id="detail-box">
		<div id="detail-content">
			<div class="FLighter" style="font-size: 25px;">
			<img class="img-responsive hidden-xs" style="float: right;" src="stocks/trick_mom/o0x0/cx/um/e44scxumea/kid.png" alt="kid" />
            <p class="p1 img-responsive hidden-xs" style="text-indent: 50px;padding-top:80px;padding-bottom:80px;">เชิญชวนคุณพ่อคุณแม่ตรวจเช็คแผนการสร้างภูมิคุ้มกันโรคของกระทรวงสาธารณสุขตั้งแต่เด็กแรกเกิดจนถึง 6 ขวบกันค่ะ</p>
            <p class="p1 img-responsive visible-xs" style="text-indent: 50px;">เชิญชวนคุณพ่อคุณแม่ตรวจเช็คแผนการสร้างภูมิคุ้มกันโรคของกระทรวงสาธารณสุขตั้งแต่เด็กแรกเกิดจนถึง 6 ขวบกันค่ะ</p>
			<img class="img-responsive visible-xs" style='margin:0 auto;margin-bottom:20px' src="stocks/trick_mom/o0x0/cx/um/e44scxumea/kid.png" alt="kid" />
            <table style='border-spacing: 2px;border-collapse: separate;'>
            <tbody>
            <tr>
            <th style="background:#666;color:#FFF;border-radius:3px;text-align: center;width:100px;min-width: 60px;">อายุ</th>
            <th style="background:#666;color:#FFF;border-radius:3px;text-align: center;">วัคซีนที่จำเป็นต้องให้กับเด็กทุกคน กระทรวงสาธารณสุข</th>
            <th style="background:#666;color:#FFF;border-radius:3px;text-align: center;">วัคซีนอื่นๆ ที่อาจให้เสริม หรือทดแทน กระทรวงสาธารณสุข</th>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top;text-align: center;'>แรกเกิด</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันวัณโรค บี ซี จี (BCG) <br />&bull; ฉีดวัคซีนป้องกันตับอักเสบ บี ครั้งที่ 1 (HBV1)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>&nbsp;</td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">1 เดือน</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันตับอักเสบ บี ครั้งที่ 2 (HBV2)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>&nbsp;</td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">2 เดือน</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ-บาดทะยัก-ไอกรน ชนิดทั้งเซลล์ ครั้งที่ 1 (DTwP-HB1)<br />&bull; วัคซีนโปลิโอชนิดกิน ครั้งที่ 1 (OPV1 หรือ IPV1)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ-บาดทะยัก-ไอกรน ชนิดไร้เซลล์ ครั้งที่ 1 (DTaP1)<br />&bull; วัคซีนโปลิโอชนิดฉีด ครั้งที่ 1 (IPV 1)<br />&bull; ฉีดวัคซีนป้องกันเชื้อ ฮิบ ครั้งที่ 1 (Hib1)<br />&bull; ฉีดวัคซีนป้องกันเชื้อนิวโมคอคคัสชนิดคอนจูเกต ครั้งที่ 1 (PCV1)<br />&bull; หยอดวัคซีนโรต้า ครั้งที่ 1 (Rota1)
            </td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">4 เดือน</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ-บาดทะยัก-ไอกรน ชนิดทั้งเซลล์ ครั้งที่ 2 (DTwP-HB2)<br />&bull; วัคซีนโปลิโอชนิดกิน ครั้งที่ 2 (OPV2 หรือ IPV2)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ-บาดทะยัก-ไอกรน ชนิดไร้เซลล์ ครั้งที่ 2 (DTaP2)<br />&bull; วัคซีนโปลิโอชนิดฉีด ครั้งที่ 2 (IPV2)<br />&bull; ฉีดวัคซีนป้องกันเชื้อ ฮิบ ครั้งที่ 2 (Hib2)<br />&bull; ฉีดวัคซีนป้องกันเชื้อนิวโมคอคคัสชนิดคอนจูเกต ครั้งที่ 2 (PCV2)<br />&bull; หยอดวัคซีนโรต้า ครั้งที่ 2 (Rota1)
            </td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">6 เดือน</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ-บาดทะยัก-ไอกรน ชนิดทั้งเซลล์ ครั้งที่ 3 (DTwP-HB3)<br />&bull; วัคซีนโปลิโอชนิดกิน ครั้งที่ 3 (OPV3)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ-บาดทะยัก-ไอกรน ชนิดไร้เซลล์ ครั้งที่ 3 (DTaP3)<br />&bull; วัคซีนโปลิโอชนิดฉีด ครั้งที่ 3 (IPV3)<br />&bull; ฉีดวัคซีนป้องกันเชื้อ ฮิบ ครั้งที่ 3 (Hib3)<br />&bull; ฉีดวัคซีนป้องกันเชื้อนิวโมคอคคัสชนิดคอนจูเกต ครั้งที่ 3 (PCV3)<br />&bull; หยอดวัคซีนโรต้า ครั้งที่ 3 (Rota3 เฉพาะ Pentavalent)<br />วัคซีนป้องกันโรคไข้หวัดใหญ่ (Influenza)*<br />*ให้ปีละครั้งช่วงอายุ 6 เดือน &ndash; 18 ปี (เน้นในอายุ 6 &ndash; 24 เดือน) ในปีแกรฉีด 2 เข็มห่างกัน 4 สัปดาห์
            </td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">9 - 12 เดือน</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันหัด-หัดเยอรมัน-คางทูม ครั้งที่ 1 (MMR1)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            วัคซีนไข้สมองอักเสบเจอี (Live JE) ครั้งที่ 1 และ 2
            </td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">9 - 18 เดือน</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันไข้สมองอักเสบเจอี ครั้งที่ 1 และ 2 ห่างกัน 1 &ndash; 4 สัปดาห์ (MBV JE1, JE2 หรือ Live JE1)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันไข้สมองอักเสบเจอี ครั้งที่ 1 (Live JE1)*<br />* เริ่มฉีดเข็มแรกที่อายุ 9 &ndash; 12 เดือน และเข็มที่สอง อีก 3 &ndash; 12 เดือนต่อมา
            </td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">12 - 18 เดือน</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>&nbsp;</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            วัคซีนป้องกันโรคตับอักเสบเอ (HAV) ให้ 2 ครั้ง ห่างกัน 6 &ndash; 12 เดือน<br />&bull; ฉีดวัคซีนป้องกันโรคอีสุกอีใส ครั้งที่ 1 (VZV1) หรือวัคซีนรวมหัด-หัดเยอรมัน-คางทูม-อีสุกอีใส ครั้งที่ 1 (MMRV1)<br />&bull; ฉีดวัคซีนป้องกันเชื้อนิวโมคอคคัสชนิดคอนจูเกต ครั้งที่ 4 (PCV4)
            </td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">18 เดือน</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ บาดทะยัก ไอกรน ชนิดทั้งเซลล์ กระตุ้น ครั้งที่ 1 (DTwP กระตุ้น 1)<br />&bull; วัคซีนโปลิโอชนิดกิน กระตุ้น ครั้งที่ 1 (OPV กระตุ้น 1)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ บาดทะยัก ไอกรน ชนิดไร้เซลล์ กระตุ้น ครั้งที่ 1 (DTaP กระตุ้น 1)<br />&bull; วัคซีนโปลิโอชนิดฉีด ครั้งที่ 4 (IPV4)<br />&bull; ฉีดวัคซีนป้องกันเชื้อ ฮิบ ครั้งที่ 4 (Hib4)
            </td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">2 - 2<sup>1/2</sup> ปี</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันไข้สมองอักเสบเจอี ครั้งที่ 3 (MBV JE3 หรือ Live JE2)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันไข้สมองอักเสบเจอี ครั้งที่ 2 (Live JE2)
            </td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">2<sup>1/2</sup> -  ปี</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันหัด หัดเยอรมัน คางทูม ครั้งที่ 2 (MMR 2)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>&nbsp;</td>
            </tr>
            <tr>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top' align="center">4 -  ปี</td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ บาดทะยัก ไอกรน ชนิดทั้งเซลล์ กระตุ้น ครั้งที่ 2 (DTwP กระตุ้น 2)<br />&bull; วัคซีนโปลิโอชนิดกิน กระตุ้น ครั้งที่ 2 (OPV กระตุ้น 2)
            </td>
            <td style='padding:3px;border-radius:3px;background:rgba(0,0,0,0.1);vertical-align:top'>
            ฉีดวัคซีนป้องกันคอตีบ บาดทะยัก ไอกรน ชนิดไร้เซลล์ กระตุ้น ครั้งที่ 2 (DTaP กระตุ้น 2)<br />&bull; วัคซีนโปลิโอชนิดฉีด ครั้งที่ 5 (IPV5)<br />&bull; ฉีดวัคซีนป้องกันโรคอีสุกอีใส ครั้งที่ 2 (VZV2) หรือวัคซีนรวมหัด-หัดเยอรมัน-คางทูม-อีสุกอีใส ครั้งที่ 2 (MMRV2)
            </td>
            </tr>
            </tbody>
            </table>
            <p class="p2" style="margin-bottom: 0px;"><span class="FXMed">ขอขอบคุณข้อมูลจาก</span>
            <p class="p2" style="margin-top: 10px; text-indent: 50px;">&ldquo;ตารางการให้วัคซีนในเด็กไทยปกติ&rdquo; แนะนำโดย สมาคมโรคติดเชื้อในเด็กแห่งประเทศไทย 2558
            </div>

		</div>
	</div>
</div>
@endsection

