@extends('template.master')


@compressCss("custom_page,miracle4")

@section('content')
<div id="detail-panel" class="wapContent">
	<div id="detail-box">
		<div id="detail-content1">
			<div id="detail-inner-content1">
				<h1>
					<img src="{{ asset($BASE_CDN . '/images/miracle4/the_miracle_of_sleeping.png') }}" alt="The Miracle of sleeping" title="The Miracle of sleeping" class="img-responsive img-title">
				</h1>
			</div>
		</div>
		<div id="detail-content2">
			<div id="detail-inner-content2">
				<div id="paragraph-1">
					<span class="paragraph-1-1 FXregular">
						เผยความลับสมองลูก ที่คุณอาจไม่เคยรู้
					</span>
					<span class="paragraph-1-2 FXregular">
						รู้ไหม สมองลูกไม่เคยหลับ และมีการเรียนรู้ทุกเวลา ทั้งกลางวัน และกลางคืน
					</span>
				</div>
				<div id="paragraph-2">
					<p>
					<span class="paragraph-2-1 FXregular">
								ในยุคสมัยที่โลกเปลี่ยนแปลงอย่างรวดเร็ว คุณพ่อคุณแม่ต่างพยายามทำทุกอย่างเพื่อเตรียมลูกให้พร้อมที่จะ
พัฒนาทันโลก ไม่ว่าจะเป็นการจัดหากิจกรรมต่างๆ เพื่อพัฒนาสมองและเสริมสร้างการเรียนรู้ให้กับเด็ก ซึ่งโดยทั่วไปจะ
เน้นไปที่การทำกิจกรรมในช่วงเวลากลางวันที่เด็กตื่นนอน แต่คุณพ่อคุณแม่รู้ไหมว่า สมองของลูกสามารถเรียนรู้และ
พัฒนาได้ตลอดเวลาทั้งกลางวันและกลางคืน และหากพยายามให้เด็กทำกิจกรรมต่างๆ ในขณะที่เด็กยังพักผ่อนไม่เพียง
พอ อาจส่งผลต่อการเรียนรู้ การจดจำของเด็กๆ ไปจนถึงเป็นการปิดกั้นการสื่อสารระหว่างเซลล์สมองอีกด้วย
					</span>
					</p>
				</div>
				<div id="paragraph-3">
					<div class="text-normal color-black FXregular">
						<span class="color-blue-light">สมองพัฒนาทั้งกลางวันและกลางคืน</span><img src="{{ asset($BASE_CDN . '/images/miracle4/child-play.png') }}" alt="Child" title="child-play" class="img-child-play hidden-xs">
						<p>
							<span>		ปัจจุบันคุณพ่อคุณแม่พยายามศึกษาหาแนวทาง
ในการการเลี้ยงลูกให้เหมาะสมกับยุคสมัยที่เปลี่ยนไปอย่าง
รวดเร็ว โดยเน้นให้เด็กทำกิจกรรม และเปิดรับประสบการณ์
ต่างๆ รอบตัว ทุกอย่างเป็นสิ่งแปลกใหม่น่าสนใจ น่าเรียนรู้<sup>1</sup>
แต่ในขณะเดียวกัน คุณพ่อคุณแม่ต้องไม่ลืมว่าการนอน
หลับพักผ่อนของเด็กก็มีความสำคัญไม่แพ้กัน </span>
<img src="{{ asset($BASE_CDN . '/images/miracle4/child-play-mobile.png') }}" alt="Child" title="child-play" class="img-child-play img-responsive visible-xs">
</p>
<p>
							<span class="color-gold-light">				แพทย์หญิง เกศินี โอวาสิทธิ์ กุมารแพทย์ผู้
เชี่ยวชาญด้านพัฒนาการและพฤติกรรม ให้ความเห็นว่า
“คุณพ่อคุณแม่ส่วนใหญ่มักให้ความสำคัญกับการทำ
กิจกรรมและพัฒนาการของลูกในเวลากลางวัน แต่อันที่
จริงแล้ว สมองของลูกสามารถเรียนรู้และพัฒนาได้ตลอด
เวลาทั้งกลางวันและกลางคืน  โดยขณะที่เด็กนอนหลับใน
เวลากลางคืน นอกจากเป็นการพักผ่อนแล้ว กลางคืนยัง
เป็นช่วงเวลาที่สมองของลูกเก็บประสบการณ์ที่ได้เรียนรู้
ในแต่ละวัน บันทึกไว้เป็นความทรงจำระยะยาว ทำให้เด็ก
สามารถจดจำสิ่งต่างๆ ได้ดีขึ้นด้วย”</span>
							</p>
							<p>		ฉะนั้น การนอนหลับที่สนิทไม่เพียงแต่ส่งเสริมให้
							สมองมีการเรียนรู้ แต่การนอนยังทำให้เซลล์ประสาทเจริญ
							เต็มที่เพื่อเตรียมพร้อมเรียนรู้และทำกิจกรรมต่างๆ ในขณะ
							ตื่นในแต่ละวันได้อย่างมีประสิทธิภาพ</p>
							<span class="color-blue-light">เทคนิคการส่งเสริมการเรียนรู้ และพัฒนาการสมอง</span>
								<p class="p3-change">		คุณพ่อคุณแม่อาจทราบกันดีอยู่แล้วว่า ปัจจัยหลักในการพัฒนาสมองลูก มี 3 ปัจจัย ได้แก่ พันธุกรรม สภาพ
แวดล้อมซึ่งรวมไปถึงการเลี้ยงดู และโภชนาการ โดยปัจจัยด้านพันธุกรรมนั้นเป็นปัจจัยที่ปรับเปลี่ยนได้ยาก การเลี้ยงดู
เอาใจใส่ และการดูแลเรื่องโภชนาการ จึงเป็นปัจจัยสำคัญในการช่วยสร้างพัฒนาการทางสมองของลูกที่คุณพ่อคุณแม่
สามารถทำได้ โดยเฉพาะในช่วง 1,000 วันแรกของชีวิต ที่สมองเจริญเติบโตและพัฒนาได้รวดเร็วและมากถึง 80% ของ
สมองผู้ใหญ่</p>
					</span>
						<div class="clearfix"></div>
					</div>
				</div>

				<div id="paragraph-5" class="text-normal color-black FXregular">
					<p class="text-normal p-change FXregular"><span>สมองพัฒนาทั้งกลางวันและกลางคืน</span>		<img src="{{ asset($BASE_CDN . '/images/miracle4/child-1.png') }}" alt="Child" title="Child" class="img-child hidden-xs">เพื่อให้เด็กมีพัฒนาการทางสมองที่ดี และเรียนรู้
ได้ไว คุณพ่อคุณแม่ควรเสริมการเรียนรู้และพัฒนาสมอง
ตามวัยของเด็ก รวมทั้งเข้าใจลูก ว่าเด็กอาจมีความ
แตกต่างกันในพื้นฐานของอารมณ์ ความถนัด ซึ่งนอก
จากการชวนลูกทำกิจกรรมที่จะช่วยพัฒนาสมองทั้งสอง
ข้างแล้ว คุณพ่อคุณแม่ควรให้ความสำคัญกับการนอน
หลับที่เพียงพอของลูก ซึ่งการนอนโดยเฉลี่ยของเด็ก
ปฐมวัยนั้นอยู่ที่ราว 12-14 ชั่วโมงต่อวัน<sup>2<sup>
<img src="{{ asset($BASE_CDN . '/images/miracle4/child-1.png') }}" alt="Child" title="Child" class="img-child img-responsive visible-xs">
</p>
		<p><span>การเตรียมสภาพแวดล้อมเพื่อให้เกิดการนอน
หลับที่ดีจึงเป็นเรื่องที่สำคัญอย่างยิ่ง แต่ในระหว่างที่ลูก
นอนหลับ คุณพ่อคุณแม่ไม่สามารถเข้าไปช่วยในเรื่องการเรียนรู้หรือการพัฒนาสมองของลูกได้ สิ่งที่คุณพ่อ
คุณแม่สามารถทำให้ลูกน้อยได้ก็คือ การดูแลเอาใจใส่เรื่องโภชนาการที่เหมาะสมในช่วงเวลาที่ลูกตื่น เพื่อให้สมองลูก
สามารถนำสารอาหารมาใช้ในช่วงเวลาแห่งการเรียนรู้ และทำให้ร่างกายแข็งแรง  <span class="FBold">ซึ่งสารอาหารที่ช่วยเสริมสร้างสมอง
ให้เรียนรู้และพัฒนาที่สำคัญ ได้แก่</span></span>
</p>

				</div>
				<div id="paragraph-6" class="text-normal color-black FXregular">
					<span class="FBold">
						<img src="{{ asset($BASE_CDN . '/images/miracle4/dha-lutein.png') }}" alt="DHA" title="DHA" class="img-dha-lutein img-responsive visible-xs">
						<img src="{{ asset($BASE_CDN . '/images/miracle4/dha.png') }}" alt="DHA" title="DHA" class="img-dha hidden-xs"><img src="{{ asset($BASE_CDN . '/images/miracle4/lutein.png') }}" alt="Lutein" title="Lutein" class="img-lutein hidden-xs"><span>ดีเอชเอ ลูทีน โคลีน:</span></span> เป็นสารอาหารที่มีบทบาทสำคัญในการพัฒนา
						และเสริมสร้างการทำงานของสมอง  เด็กๆ จึงควรได้รับสารอาหาร
						เหล่านี้ ในชีวิตประจำวันเพื่อเสริมสร้างพัฒนาการและการทำงานของสมอง
				</div>
				<div id="paragraph-7">
					<span class="paragraph-7-1 text-normal FXregular">								<span class="FBold"><img src="{{ asset($BASE_CDN . '/images/miracle4/alpha-lactalbumin.png') }}" alt="Alpha-Lactalbumin" title="Alpha-Lactalbumin" class="img-alpha-lactalbumin">แอลฟา-แล็คตัลบูมิน:</span> เป็นโปรตีนคุณภาพสูง พบในน้ำนมแม่  ย่อยง่าย
										และให้กรดอะมิโนจำเป็น ที่ช่วยในการเจริญเติบโตและพัฒนาสมอง ซึ่ง
										“ทริบโตเฟน” เป็นหนึ่งในกรดอะมิโนจำเป็นที่เป็นสารตั้งต้นของสารสื่อ
										ประสาท คือ "เซราโตนิน" ซึ่งช่วยในการควบคุมการนอนหลับ และกระบวน
										การเรียนรู้และพัฒนาสติปัญญา เมื่อร่างกายได้รับแอลฟา-แล็คตัลบูมิน
เข้าไป จะให้กรดอะมิโนชื่อ ทริบโตเฟน เป็นหนึ่งในกรดอะมิโนจำเป็นที่ ช่วยในการนอน โดยร่นระยะเวาลานอน ให้หลับเร็วขึ้นนอกจากนี้ยังมีสารอาหารอื่นๆ ที่สำคัญสำหรับสมองอีก ได้แก่ ไอโอดีนและธาตุเหล็ก เป็นต้น</span>
					<p class="text-normal FXregular">เมื่อเด็กได้รับสารอาหารที่ดี และนอนหลับสนิทอย่างเพียงพอ เด็กจะสามารถตื่นขึ้นมาพร้อมสมองที่ปลอดโปร่ง
สดใส พร้อมรับมือกับทุกสถานการณ์  และสามารถก้าวทันโลกแห่งการเรียนรู้ที่เปลี่ยนแปลงอย่างรวดเร็วได้ทันที</p>
				</div>
				<div id="paragraph-8">
					<div class="paragraph-8-1 text-normal FXregular">
						<span><sup>1</sup>Kalliala, M. (2006). Play Culture in a changing world. Berkshire, England: Open University Press.</span>
						<span><sup>2</sup>Amanda R. Tarullo (2011). Sleep and Infant Learning. Infant Child Dev: National Institute of Health.</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

