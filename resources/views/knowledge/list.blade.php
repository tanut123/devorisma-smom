@extends('template.master')

@compressCss("knowledge")

@section('content')

	<input type="hidden" name="category" id="category" value="<?=$type?>"/>
	<input type="hidden" name="sub_category" id="sub_category" value="<?=$sub_type?>"/>

	<div id="wapKnowledge" class="lazyload {{ (App::getLocale() == 'en')? 'kl_en':'' }}" data-bgset="{{ asset($BASE_CDN . '/images/knowledge/m_bg_knowledge.png') }} [--mb] | {{ asset($BASE_CDN . '/images/knowledge/bg_knowledge.png') }}" data-expand="+10">
		<div class="list-content-panel list-all-item lazyload" data-bgset="{{ asset($BASE_CDN . '/images/knowledge/m_bg_object_knowledge_new.jpg') }} [--mb] | {{ asset($BASE_CDN . '/images/knowledge/bg_object_knowledge.jpg') }}" data-expand="+10">
			<?php if($sub_type_name=="lactating"){ ?>
				<div class="header-content lactating">
					<h1>{{ (App::getLocale() == 'en')? 'Secret ':'นานา' }}{{ (App::getLocale() == 'en')? 'Know-How ':'สาระ' }}</h1>
					<h3>
						{{ trans('subtitle.SUBTITLE_KNOWLEDGE_'.strtoupper($sub_type_name)) }}
						<div class="txt-desc"><?php echo nl2br(trans('desc.DESC_KNOWLEDGE_'.strtoupper($sub_type_name)));?></div>
					</h3>
				</div>
			<?php }else if($sub_type_name=="braindevelopment"){ ?>
				<div class='head_panel'>
				    <div class='head_text braindevelopment'>
				        <span class="header-left FX">
				        	<span>{{ (App::getLocale()=="en")?'Reversal ':'พลิก' }}{{ (App::getLocale()=="en")?'Theory':'ทุกทฤษฎี' }}</span>
				        </span>
				        <span class="header-right FX">
				        	<?php echo nl2br(trans('desc.DESC_KNOWLEDGE_'.strtoupper($sub_type_name)));?>
				        </span>
				        <span class="clearfix"></span>
				    </div>
				</div>
			<?php }else if($sub_type_name=="ลูกน้อยถ่ายสบาย"){ ?>
				<div class='head_panel'>
				    <div class='head_text excrete'>
				        <span class="header-left FX">
				        	<span><?php echo nl2br(trans('subtitle.SUBTITLE_KNOWLEDGE_EXCRETE'));?></span>
				        </span>
				        <span class="header-right FX">
				        	รู้ได้อย่างไรว่า เจ้าตัวเล็ก...ท้องผูก
				        </span>
				        <span class="clearfix"></span>
				    </div>
				</div>
			<?php }else{ ?>
				<div class="header-content sub_category">
					<h1>{{ trans('subtitle.SUBTITLE_KNOWLEDGE_'.strtoupper($sub_type_name)) }}</h1>
				</div>
			<?php } ?>
			<ul class="list-panel">

			</ul>
			<div class="clearfix"></div>
			<div class="btn-bar">
				<?php if($sub_type_name != "braindevelopment" && $sub_type_name != "lactating" && $sub_type_name != "ลูกน้อยถ่ายสบาย" ){ ?>
				<a href="{{ $BASE_LANG.$type }}">
					<div class="btn-style btn-back">ย้อนกลับ</div>
				</a>
				<?php } ?>
				<div class="btn-style load-more" style="display:none;">
					{{ trans('loadmore.LOADMORE') }}
				</div>
			</div>
		</div>
	</div>
	<script>
		window.lazySizesConfig = window.lazySizesConfig || {};
		window.lazySizesConfig.customMedia = {
	    	'--mb': '(max-width: 767px)'
		};
	</script>

@endsection

@compressJs("lazy.bgset.min,lazysizes.min,knowledge_list")
