@extends('template.master')

@compressCss("slick,slick-theme,knowledge")

@section('content')
	<div id="wapKnowledge" class="{{ (App::getLocale() == 'en')? 'kl_en':'' }}">
		<div class="list-content-panel">
			<div class="header-content">
				<h1>{{ (App::getLocale() == 'en')? 'Secret ':'นานา' }}{{ (App::getLocale() == 'en')? 'Know-How ':'สาระ' }}</h1>
				<h3>
					<?php echo $knowledge_title; ?>
					<div class="txt-desc"><?php echo $knowledge_desc; ?></div>
				</h3>
			</div>
			<?php
			$count = 1;
			foreach ($knowledge_subcategory as $key => $value) {
				$sublink = substr($key,1,1);
			?>
				<div class="category-panel panel-<?=$count++;?>">
					<div class="category-panel-inner">
						<div class="category-bar">
							<div class="category-name font-xregular">
								<?php echo nl2br(trans('subtitle.SUBTITLE_KNOWLEDGE_'.strtoupper($key))); ?>
							</div>
						</div>
						<ul class="list-panel-sub" id="list_panel_<?=$key;?>">
						<?php   foreach($value as $key2 => $value2){
								echo $value2;
						} ?>
						</ul>
						<div class="clearfix"></div>
						<?php if(count($value) > 3){ ?>
							<div class="category-viewall FBold">
								<a href="{{ $BASE_LANG.$know_category.'/'.$sublink }}">
									{{ trans('view.VIEWALL') }}
								</a>
							</div>
						<?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>

@endsection

@compressJs("slick.min,knowledge")
