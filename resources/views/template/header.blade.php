<?php
$browser_data = $_SERVER['HTTP_USER_AGENT'];
$is_ie8 = false;
if (preg_match("/MSIE 8/",$browser_data)){
	$is_ie8 = true;
}
?>

<div class="hidden-xs" >
	<nav id="TabNav" style="display:none;@if($is_ie8) background-color:#FFCA4C @endif">
		<div id="nav_bar">
			<div id="logo_site">
				<a href="{{ $BASE_LANG }}home">
					<img src="{{ $BASE_CDN }}/images/header/logo-smom_new.png" alt="S-MomClub" width="363" height="72">
				</a>
			</div>
			<div id="menu_page">
				<div id="menu-home" class="menu-list {{ (App::getLocale() == 'en')? 'visible-en': '' }}">
					<a href="{{ $BASE_LANG }}home">
						<div class="icon_list">

							<div class='icon home' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>
						</div>
						<span>{{ trans('menu.MENU_HOME') }}</span>
					</a>
				</div>
				<div id="menu1" data-id="submenu_momtip_dt" class="menu-list {{ (App::getLocale() == 'en')? 'visible-en': '' }}">
					<a>
						<div class="icon_list">

							<div class='icon mom' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>

						</div>
						<span>{{ trans('menu.MENU_MOMTIP') }}</span>
					</a>
				</div>
				<div id="submenu_momtip" class="list_submenu_hover list_submenu_enable">
					<ul>
						<li><a href="{{ $BASE_LANG }}pregnancy-mom">{{ trans('menu.MENU_PREGNANCY_STAGE') }}</a></li>
						<li><a href="{{ $BASE_LANG }}lactating-mom">{{ trans('menu.MENU_LACTATING_STAGE') }}</a></li>
						<li><a href="{{ $BASE_LANG }}toddler-mom">{{ trans('menu.MENU_TODDLER_STAGE') }}</a></li>
					</ul>
				</div>
				@if(App::getLocale() == 'th')
				<!-- <div id="menu9" class="menu-list {{ (App::getLocale() == 'en')? 'hidden-en': '' }} ">
					<a href="{{ url() }}/everyonecanbethestar">
						<div class="icon_list">
							<div class='icon l1000day' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>

						</div>
						<span>ล้านคนล้านพรสวรรค์</span>
					</a>
				</div> -->
				<div id="menu9" class="menu-list {{ (App::getLocale() == 'en')? 'hidden-en': '' }} ">
					<a href="{{ url() }}/สมองดีเรียนรู้ไว">
						<div class="icon_list">
							<div class='icon l1000day' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>

						</div>
						<span>สมองดี เรียนรู้ไว</span>
					</a>
				</div>
				<div id="menu2" class="menu-list {{ (App::getLocale() == 'en')? 'hidden-en': '' }} ">
					<a href="{{ url() }}/สฟิงโกไมอีลิน">
						<div class="icon_list">
							<div class='icon l1000day' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>

						</div>
						<span>สฟิงโกไมอีลิน</span>
					</a>
				</div>
				<!--<div id="menu8" class="menu-list {{ (App::getLocale() == 'en')? 'hidden-en': '' }} ">
					<a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก">
						<div class="icon_list">
							<div class='icon brain_ignition' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>

						</div>
						<span>Brain Ignition lab</span>
					</a>
				</div>-->
				@endif
				<div id="menu3" class="menu-list {{ (App::getLocale() == 'en')? 'visible-en': '' }}">
					<a href="{{ $BASE_LANG }}braindevelopment">
						<div class="icon_list">
							<div class='icon article' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>
						</div>
						<span>{{ trans('menu.MENU_KNOWLEDGE') }}</span>
					</a>
				</div>
				<div id="submenu_knowledge" class="list_submenu_hover list_submenu_enable">
					<ul>
						<li><a href="{{ $BASE_LANG }}braindevelopment">{{ trans('subtitle.SUBTITLE_KNOWLEDGE_BRAINDEVELOPMENT') }}</a></li>
					</ul>
				</div>
				@if(App::getLocale() == 'th')
				<div id="menu6" data-id="sub-menu-product-dt" class="menu-list {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
					<a>
						<div class="icon_list">
							<div class='icon product' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>

						</div>
						<span>ผลิตภัณฑ์</span>
					</a>
				</div>
				<div id="sub-menu-product" class="list_submenu_hover list_submenu_enable {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
					<ul class="listsub-menu">
						<li class="banner-product" style="width: 213px;height: 171px;">
							 <div class="txt-bn">
							    โภชนาการเพื่อลูกน้อย
							</div>
						</li>
						<li id="product1" class="m_product">
							<a href="{{ $BASE_LANG }}product/promama">
							    <div class='product_pic1'>&nbsp;</div>
								<div class="txt-pro-name">S-26 <span class="font-bold">โปรมาม่า</span></div>
								<div class="txt-pro-desc">สำหรับคุณแม่ตั้งครรภ์และให้นมบุตร</div>
							</a>
						</li>
						<li id="product2" class="m_product">
							<a href="{{ $BASE_LANG }}product/gold_progress">
							    <div class='product_pic2'>&nbsp;</div>
								<div class="txt-pro-name">S-26 <span class="font-bold">โกลด์ โปรเกรส</span></div>
								<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
							</a>
						</li>
						{{--<li id="product3" class="m_product">

							<a href="{{ $BASE_LANG }}product/pe_gold">
							    <div class='product_pic3'>&nbsp;</div>
								<div class="txt-pro-name">S-26 <span class="font-bold">พีอี โกลด์</span></div>
								<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
							</a>
						</li>--}}
						<li id="product4" class="m_product">
							<a href="{{ $BASE_LANG }}product/progress">
							    <div class='product_pic4'>&nbsp;</div>
								<div class="txt-pro-name">S-26 <span class="font-bold">โปรเกรส</span></div>
								<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
							</a>
						</li>
						<li id="product5" class="m_product">
							<a href="{{ url() }}/s-26organicprogress/home" target="_blank">
							    <div class='product_pic5'>&nbsp;</div>
								<div class="txt-pro-name">S-26 <span class="font-bold">ออร์แกนิค โปรเกรส</span></div>
								<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
							</a>
						</li>
						<li id="product6" class="m_product">
							<a href="{{ url() }}/S26GoldProgressUHT" target="_blank">
								<div class='product_pic6'>&nbsp;</div>
								<div class="txt-pro-name">S-26 <span class="font-bold">โกลด์ โปรเกรส ยูเอชที</span></div>

								<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
							</a>
						</li>

					</ul>
					<div class="clearfix"></div>
				</div>
				<div id="menu5" class="menu-list {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
					<a href="{{ $BASE_LANG }}promotion">
						<div class="icon_list" style="width:109px;">
							<div class='icon promotion' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>

						</div>
						<span>โปรโมชั่น &amp; กิจกรรม</span>
					</a>
				</div>
				@endif
				<div id="menu7" class="menu-list {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
					<a href="{{ url() }}/wyethnutrition">
						<div class="icon_list" style="width:109px;">
							<div class='icon wyethnutrition' style="background-image:url('{{ $BASE_CDN }}/images/header/headerSplite.[[img_type]]')">&nbsp;</div>

						</div>
						<span>Wyeth Nutrition</span>
					</a>
				</div>
				<div id="lang_search">
					<div id="page_lang">

					</div>
					<div id="search_page" style="display:none;">
						<form action="{{ $BASE_LANG }}search" id="search-desktop-form" method='get'>
							<label class="label-placeholder" for="">{{ trans('core.CORE_SEARCH') }}</label>
							<input id="globalsearch" name="q" type="text">
							<input type='hidden' value='[[csrf_token]]' name='csrf_token'>
							<span id="btn_search"></span>
						</form>
					</div>
				</div>
			</div>
			{{-- <div class='contact-tail'>

                <a href="tel:026402288" style='color:#7B5700'>
                <div class='img_call_center'></div>
                <span class='bold_call_center'>0-2640-2288</span>
                </a>
            </div> --}}
			{{-- <div class="view-panel">
				<div class="view-panel-inner">
					<span class="view-text">{{ trans('core.VISITOR_AMOUNT') }}</span>
					<span class="view-number-box">
						<img src="{{ $BASE_CDN }}/images/header/icon-view.png" alt="icon-view" width="33" height="26">
						<span class="view-number">[[VISITOR]]</span>
					</span>
					<span class="view-text">{{ trans('core.VISITOR_UNIT') }}</span>
				</div>
			</div> --}}
			@if(App::getLocale() == 'th')
			{{-- <div class="wyeth-panel {{ (App::getLocale() == 'en')? 'hidden-en': '' }} ">
				<a href="{{ url() }}/wyethnutrition">
					<div class="wyeth-panel-inner">
						<div class="wyeth-icon"></div>
						<div class="wyeth-text FBold">Wyeth Nutrition</div>
					</div>
				</a>
			</div>
			<div class="question-panel {{ (App::getLocale() == 'en')? 'hidden-en': '' }} ">
				<a href="{{ url() }}/หมอเด็ก">
					<div class="question-panel-inner">
						<div class="question-icon"></div>
						<div class="question-text FBold">คุณแม่ถาม-คุณหมอตอบ</div>
					</div>
				</a>
			</div> --}}
			<div class="joinmom-menu hidden-xs make">
				<a href="{{ $BASE_LANG }}recruitment">
					<div class="joinmom-menu-inner FXBold">
						<div class="joinmom-menu-icon"></div><div class="joinmom-menu-text FBold">Join S-Mom Club</div>
					</div>
				</a>
			</div>
			<div class="online-shopping-menu hidden-xs">
				<a href="{{ url() }}/online_shopping">
					<div class="online-shopping-menu-inner FXBold">
						<div class="online-shopping-menu-icon"></div><div class="online-shopping-menu-text FBold">Online Shopping</div>
					</div>
				</a>
			</div>
			@endif
			<div class="contact-panel hidden-xs">
				<a href="{{ $BASE_LANG }}team">
					<div class="contact-panel-inner">
						<div class="contact-icon"></div>
						<div class="contact-text FBold">{{ trans('menu.MENU_CONTACT_US') }}</div>
					</div>
				</a>
			</div>
		</div>
	</nav>
	<div class="bg-sub-hover" style="display:none;">
		<div class="content-header-fixed">
			<div id="submenu_momtip_dt" class="list_submenu_hover list_submenu_enable">
				<ul>
					<li><a href="{{ $BASE_LANG }}pregnancy-mom">{{ trans('menu.MENU_PREGNANCY_STAGE') }}</a></li>
					<li><a href="{{ $BASE_LANG }}lactating-mom">{{ trans('menu.MENU_LACTATING_STAGE') }}</a></li>
					<li><a href="{{ $BASE_LANG }}toddler-mom">{{ trans('menu.MENU_TODDLER_STAGE') }}</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="bg-sub-hover-product"  style="display:none;">
		<div class="bg-sub-hover2"></div>
		@if(App::getLocale() == 'th')
		<div id="sub-menu-product-dt" class="list_submenu_hover list_submenu_enable {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
			<ul class="listsub-menu">
				<li class="banner-product" style="width: 213px;height: 171px;">
					 <div class="txt-bn">
					    โภชนาการเพื่อลูกน้อย
					</div>
				</li>
				<li id="product1" class="m_product">
					<a href="{{ $BASE_LANG }}product/promama">
					    <div class='product_pic1'>&nbsp;</div>
						<div class="txt-pro-name">S-26 <span class="font-bold">โปรมาม่า</span></div>
						<div class="txt-pro-desc">สำหรับคุณแม่ตั้งครรภ์และให้นมบุตร</div>
					</a>
				</li>
				<li id="product2" class="m_product">
					<a href="{{ $BASE_LANG }}product/gold_progress">
					    <div class='product_pic2'>&nbsp;</div>
						<div class="txt-pro-name">S-26 <span class="font-bold">โกลด์ โปรเกรส</span></div>
						<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
					</a>
				</li>
				<!-- <li id="product3" class="m_product">
					<a href="{{ $BASE_LANG }}product/pe_gold">
					    <div class='product_pic3'>&nbsp;</div>
						<div class="txt-pro-name">S-26 <span class="font-bold">พีอี โกลด์</span></div>
						<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
					</a>
				</li> -->
				<li id="product4" class="m_product">
					<a href="{{ $BASE_LANG }}product/progress">
					    <div class='product_pic4'>&nbsp;</div>
						<div class="txt-pro-name">S-26 <span class="font-bold">โปรเกรส</span></div>
						<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
					</a>
				</li>
				<li id="product5" class="m_product">
					<a href="{{ url() }}/s-26organicprogress/home" target="_blank">
					    <div class='product_pic5'>&nbsp;</div>
						<div class="txt-pro-name">S-26 <span class="font-bold">ออร์แกนิค โปรเกรส</span></div>
						<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
					</a>
				</li>
				<li id="product6" class="m_product">
					<a href="{{ url() }}/S26GoldProgressUHT" target="_blank">
					    <div class='product_pic6'>&nbsp;</div>
						<div class="txt-pro-name">S-26 <span class="font-bold">โกลด์ โปรเกรส ยูเอชที</span></div>

						<div class="txt-pro-desc">สำหรับเด็ก 1 ขวบขึ้นไป</div>
					</a>
				</li>
			</ul>
			<div class="clearfix"></div>
		</div>
		@endif
	</div>
</div>
@if(App::getLocale() == 'th')
<div id='login_bar' class="{{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
	<div class='panel_align' id='login_case_panel'>
		[[login_panel]]
	</div>
</div>
@endif
