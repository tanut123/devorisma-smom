<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= Meta::meta('title'); ?></title>
     <?= Meta::tagMetaName('robots'); ?>
    <?= Meta::tagMetaName('keywords'); ?>
    <?= Meta::tag('site_name'); ?>
    <?= Meta::tagMetaProperty('url', Request::url()); ?>
    <?= Meta::tagMetaProperty('og:url', Request::url()); ?>
    <?= Meta::tag('title'); ?>
    <?= Meta::tag('description'); ?>
    <?= Meta::tag('image'); ?>
    <meta property="og:type" content="website"/>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@s-mom" />
    <meta name="twitter:creator" content="@s-mom" />
	<meta name="csrf-token" content="[[csrf_token]]" />
	<base href="<?=url()?>" />

	<link rel="shortcut icon" href="<?= asset('favicon.ico')?>" />
	<link rel="canonical" href="<?=  Request::url()?>" />
	<style>html { visibility : hidden }</style>
	<script type="text/javascript">
		if (self == top) {
			document.documentElement.style.visibility= "visible";
		}else{
			top.location= self.location;
		}
		var csrf_token= "[[csrf_token]]";
	</script>
    @loadJs("master_survey,nativeFileLoad,plainPreload")
	@yield('style')

</head>
<body class="{{ trans('core.CORE_LANG') }}">
@if(strpos(url(),"s-momclub.com") > 0 )
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TF9JNQ"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TF9JNQ');</script>
	<!-- End Google Tag Manager -->
@endif

	<script type="text/javascript">
		var BASE_URL = '{{ url() }}';
		var BASE_LANG = '{{ $BASE_LANG }}';

	</script>

	<script type='text/javascript'>
     {{--
        Update stable version  form http://research.insecurelabs.org/jquery/test/
        - jQuery v1.12.2
      --}}
		plainFade();
		initFileload(["{{url()}}/css/@genFile('css','site')","@writeCss()"],["{{url()}}/js/@genFile('js','jquery.min,bootstrap.min')","@writeJs()"],{callback:function(){
			fadeBodyIn();
			
			}
		});
	</script>
	<div id="fb-root"></div>

	@yield('content')

	@yield('scripts')
</body>
</html>

