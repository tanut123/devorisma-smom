<?php
    $img_type = "svg";
?>
<div class='clearfix'></div>
<script type="text/javascript">
	var LANG = '{{ trans('core.CORE_LANG') }}';
</script>
<div id='footer_panel'>
    <div class='footer_top_bg'>
        <div class='footer_align_top'>
            <div class='footer_top'>
                <div class='footer_top_header'>
                    <span class='header_1'>{{ (App::getLocale()=="en")?'Contact ':'ช่องทาง' }}{{ (App::getLocale()=="en")?'Channel':'การติดต่อ' }}</span>
                </div>
                <div class='footer_top_contact_panel {{ (App::getLocale() == "en")? "panel-en": "" }}'>
                    <div class='footer_contact'>
                        <a href='tel:026402288'>
                            <div class='footer_contact_icon'>
                                <div class='icon tel' style="background-image:url('{{ $BASE_CDN }}/images/footer/footerIconSplite_new.png')">&nbsp;</div>
                            </div>
                            <div class='footer_contact_header'>
                                {{ (App::getLocale()=="en")?'Tel.':'โทร.' }}
                            </div>
                            <div class='footer_contact_detail'>
                                0-2640-2288
                            </div>
                        </a>
                    </div>
                    <div class='footer_contact'>
                        <a href='https://www.facebook.com/Smomclub' target="_blank">
                            <div class='footer_contact_icon'>
                                <div class='icon facebook' style="background-image:url('{{ $BASE_CDN }}/images/footer/footerIconSplite_new.png')">&nbsp;</div>
                            </div>
                            <div class='footer_contact_header'>
                                Facebook.com/
                            </div>
                            <div class='footer_contact_detail'>
                                SmomClub
                            </div>
                        </a>
                    </div>
                    <div class='footer_contact'>
                        <a href='https://line.me/R/ti/p/@s-momclubgold' target="_blank">
                            <div class='footer_contact_icon'>
                                <div class='icon line' style="background-image:url('{{ $BASE_CDN }}/images/footer/footerIconSplite_new.png')">&nbsp;</div>
                            </div>
                            <div class='footer_contact_header'>
                                Search by ID
                            </div>
                            <div class='footer_contact_detail'>
                                @S-MomClubGold
                            </div>
                        </a>
                    </div>
                    <div class='footer_contact'>
                        <div class='footer_contact_icon'>
                            <div class='icon club' style="background-image:url('{{ $BASE_CDN }}/images/footer/footerIconSplite_new.png')">&nbsp;</div>
                        </div>
                        <div class='footer_contact_header'>
                            Mobile Application
                        </div>
                        <div class='footer_contact_detail'>
                            S-MomClub
                        </div>
                    </div>
                    <div class='footer_contact'>
                        <a href="mailto:info@S-MomClub.com">
                            <div class='footer_contact_icon'>
                                <div class='icon e-mail' style="background-image:url('{{ $BASE_CDN }}/images/footer/footerIconSplite_new.png')">&nbsp;</div>
                            </div>
                            <div class='footer_contact_header'>
                                E-mail
                            </div>
                            <div class='footer_contact_detail'>
                                info@S-MomClub.com
                            </div>
                        </a>
                    </div>
                    <div class='footer_contact'>
                        <div class='footer_contact_icon'>
                            <div class='icon mail' style="background-image:url('{{ $BASE_CDN }}/images/footer/footerIconSplite_new.png')">&nbsp;</div>
                        </div>
                        <div class='footer_contact_header'>
                            {{ (App::getLocale()=="en")?'P.O. Box 10 Makkasan':'ตู้ ปณ.10' }}
                        </div>
                        <div class='footer_contact_detail'>
                            {{ (App::getLocale()=="en")?'Bangkok 10402':'ปณฝ. มักกะสัน กทม. 10402' }}
                        </div>
                    </div>
                    @if(App::getLocale() == 'th')
                    <div class='footer_contact {{ (App::getLocale() == "en")? "hidden-en": "" }}'>
                    	<a href="{{ $BASE_LANG }}live-chat">
                    	<div class='footer_contact_icon'>
                    	    <div class='icon live_chat' style="background-image:url('{{ $BASE_CDN }}/images/footer/footerIconSplite_new.png')">&nbsp;</div>
                        </div>
                        <div class='footer_contact_header'>
                           S-MomClub
                        </div>
                        <div class='footer_contact_detail'>
                            LIVE CHAT
                        </div>
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class='footer_middle_bg'>
            <img class='middle_bg_img' src='{{ $BASE_CDN }}/images/footer/footer_middle_bg_desktop_new.png' width="1280" height="245">
            <div class='footer_middle_align'>
                <div class='footer_middle'>
                    <div class='smum_club_logo'>
                        <img src='{{ $BASE_CDN }}/images/footer/smom_logo_new.png' width="124">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class='footer_bottom_bg'>
        <div class='footer_align_bottom {{ (App::getLocale() == "en")? "footer-en": "" }}'>
            <div class='footer_bottom clearfix'>
                <div class='footer_bottom_fb_and_subscribe'>
                    <div class='subscribe_panel {{ (App::getLocale() == "en")? "subscribe-en": "" }}'>
                        <div class='subscribe_header'>
                            SUBSCRIBE TO OUR NEWSLETTER
                        </div>
                        <div class='subscribe_detail'>
                            {{trans("subscribe.SUBSCRIBE_DETAIL")}}
                        </div>
                        <form class='subscribe_form' method="post" action="{{ $BASE_LANG }}footer/subscribe">
                            <input type='text' class='email_input' autocomplete='off' name='subscribe_email' placeholder="Your Email Address">
                            <input type='button' class='email_submit' name='subscribe_email' value='Subscribe'>
                            <input type='hidden' value='[[csrf_token]]' name='csrf_token'>
                            <div class='mobile_submit'><div class='submit_img' alt="Submit"></div></div>
                        </form>
                        <div class='check_age_panel'>
                            <input id='check_age' type='checkbox' class='check_age'>
                            <span class='check_age_text'>{{trans("condition.ALL_CONDITION_ACCEPT")}}</span> <a href='{{$BASE_LANG}}page/terms-and-conditions' class='condition_link' target='_blank'>{{trans("condition.READ_MORE_CONDITION")}}</a>
                        </div>
                    </div>
                </div>
                <div class='footer_bottom_link'>
                    <div class='link_box {{ (App::getLocale() == "en")? "box-en": "" }}'>
                        <div class='link_parent has_child'>
                            <a class='footer_link_bold'>{{ trans('menu.MENU_MOMTIP') }}</a>
                        </div>
                        <div class='site_map_child'>
                            <div>
                                <a href='{{ $BASE_LANG }}pregnancy-mom' class='footer_link_thin'>{{ trans('menu.MENU_PREGNANCY_STAGE') }}</a>
                            </div>
                            <div>
                                <a href='{{ $BASE_LANG }}lactating-mom' class='footer_link_thin'>{{ trans('menu.MENU_LACTATING_STAGE') }}</a>
                            </div>
                            <div>
                                <a href='{{ $BASE_LANG }}toddler-mom' class='footer_link_thin'>{{ trans('menu.MENU_TODDLER_STAGE') }}</a>
                            </div>
                        </div>
                    </div>
                    @if(App::getLocale() == 'th')
                    <div class='link_box {{ (App::getLocale() == "en")? "hidden-en": "" }}'>
                        {{-- <div class='link_parent has_child'>
                            <a href='{{ $BASE_LANG }}1000-days' class='footer_link_bold'>1,000 วัน</a>
                        </div>
                        <div class='site_map_child'>
                            <div>
                                <a href='{{ $BASE_LANG }}1000-days/first-270-days' class='footer_link_thin'>First 270 Days</a>
                            </div>
                            <div>
                                <a href='{{ $BASE_LANG }}1000-days/second-365-days' class='footer_link_thin'>Second 365 Days</a>
                            </div>
                            <div>
                                <a href='{{ $BASE_LANG }}1000-days/final-365-days' class='footer_link_thin'>Final 365 Days</a>
                            </div>
                        </div> --}}
                        <div class='link_parent'>
                            <a href='{{ url() }}/สฟิงโกไมอีลิน' class='footer_link_bold'>สฟิงโกไมอีลิน</a>
                        </div>
                    </div>
                    @endif
                    <div class='link_box {{ (App::getLocale() == "en")? "box-en": "" }}'>
                        @if(App::getLocale() == 'th')
                        <div class='link_parent'>
                            <a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก" class='footer_link_bold'>Brain Ignition lab</a>
                        </div>
                        @endif
                        <div class='link_parent'>
                            <a href='{{ $BASE_LANG }}braindevelopment' class='footer_link_bold'>{{ trans('menu.MENU_KNOWLEDGE') }}</a>
                        </div>
                        <div class="{{ (App::getLocale() == 'en')? 'visible-en list_box_en': '' }}" style="display:none">
                            <a href="{{ $BASE_LANG }}team" class='footer_link_bold'>{{ trans('menu.MENU_CONTACT_US') }}</a>
                        </div>
                    </div>
                    @if(App::getLocale() == 'th')
                    <div class='link_box {{ (App::getLocale() == "en")? "hidden-en": "" }}'>
                    	<div class="{{ (App::getLocale() == 'en')? 'hidden-en': '' }} link_parent has_child">
                            <a class='footer_link_bold'>ผลิตภัณฑ์</a>
                        </div>
                        <div class='site_map_child hidden-sm hidden-md hidden-lg'>
                            <div>
                                <a href="{{ $BASE_LANG }}product/mom_gold" class="footer_link_thin">S-26 มัม โกลด์</a>
                            </div>
                            <div>
                                <a href="{{ $BASE_LANG }}product/progress_gold" class="footer_link_thin">S-26 โปรเกรส โกลด์</a>
                            </div>
                            <div>
                                <a href="{{ $BASE_LANG }}product/pe_gold" class="footer_link_thin">S-26 พีอี โกลด์</a>
                            </div>
                            <div>
                                <a href="{{ $BASE_LANG }}product/progress" class="footer_link_thin">S-26 โปรเกรส</a>
                            </div>
                            <div>
                                <a href="{{ $BASE_LANG }}/s-26organicprogress" class="footer_link_thin">S-26 ออร์แกนิค โปรเกรส</a>
                            </div>
                        </div>
                        <div class="{{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
                            <a href="{{ $BASE_LANG }}promotion" class='footer_link_bold'>โปรโมชั่น &amp; กิจกรรม</a>
                        </div>
                    </div>
                    <div class='link_box {{ (App::getLocale() == "en")? "hidden-en": "" }}'>
                        <div class='link_parent has_child'>
                            <a href='javascript:void(0);' class='footer_link_bold'>S-MomClub Member</a>
                        </div>
                        <div class='site_map_child'>
                            <div>
                                <a href='{{ $BASE_LANG }}recruitment' class='footer_link_thin'>สมัครสมาชิก</a>
                            </div>
                        </div>
                    </div>
                    <div class='link_box {{ (App::getLocale() == "en")? "hidden-en": "" }}'>
                        <div class='link_parent'>
                            <a href="{{ $BASE_LANG }}team" class='footer_link_bold'>{{ trans('menu.MENU_CONTACT_US') }}</a>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class='clearfix'></div>
            <div class='footer_tail'>
                <!-- <div class='footer_tail_left'>
                    <a href="tel:026402288" style='color:#7B5700'><span>S-MomClub</span>
                    <div class='img_call_center'></div>
                    <span class='bold_call_center'>0-2640-2288</span>
                    </a>
                </div> -->
                <div class="view-panel">
                    <div class="view-panel-inner">
                        <span class="view-text">{{ trans('core.VISITOR_AMOUNT') }}</span>
                        <span class="view-number-box">
                            <span class="view-number">[[VISITOR]]</span>
                        </span>
                        <span class="view-text">{{ trans('core.VISITOR_UNIT') }}</span>
                    </div>
                </div>
                <div class='footer_tail_right'>
                    <div class='tail_copy_right'>
                        © {{date("Y")}} Wyeth Nutrition All right reserved.
                    </div>
                    <div class='pipe_tail'> | </div>
                    <div class='policy_and_condition'>
                        <a href='{{ $BASE_LANG }}page/privacy' target='_blank'><span>
                            {{ trans('condition.CONDITION_PRIVACY_POLICY') }}
                        </span></a> |
                        <a href='{{ $BASE_LANG }}page/terms-and-conditions' target='_blank'><span>
                           {{ trans('condition.CONDITION_AND_TERM') }}
                        </span></a> |
                         <a href='{{ $BASE_LANG }}page/cookies-notice' target='_blank'><span>
                           ประกาศเกี่ยวกับคุกกี้
                        </span></a>
                        <br class='mbnl'>
                        @if(App::getLocale() == 'th')
                        <a href='{{ $BASE_LANG }}important-notice' class="{{ (App::getLocale() == 'en')? 'hidden-en': '' }}"><span>
                            สิ่งสำคัญที่คุณแม่ควรทราบ
                        </span></a>
                        @endif
                    </div>
                </div>
            </div>
            <div class='clearfix'></div>
        </div>
        <?php echo (isset($is_member_page) && $is_member_page == true ? "<div id='isMemberPage' data-val='T'></div>" : "") ?>
        <div id='show_member_signup' show_value='[[signup_active_panel]]'>
        </div>
        <div class='desktop_space'></div>
    </div>
</div>
<div class="cookie-policy-panel FX">
    <div class="panel">
    <div class="panel-inner">
        <div class="text">
            <div class="text-inner">
                เราใช้คุกกี้ในเว็บไซต์ของเราเพื่อวัตถุประสงค์ในการพัฒนาเว็บไซต์ และเพื่อการให้บริการที่ดีที่สุด สําหรับคุณ โปรดอ่านนโยบายการใช้คุกกี้ เมื่อคุณเข้าใช้เว็บไซต์นี้ เราถือว่าคุณยินยอมให้จัดเก็บและ เข้าถึงคุกกี้บนอุปกรณ์ของคุณ
            </div>
        </div><div class="btn-panel">
            <a href='{{ $BASE_LANG }}page/cookies-notice' target='_blank' class="btn-style btn-readmore">รายละเอียดเพิ่มเติม
            </a><a class="btn-style btn-confirm">ได้อ่านและยอมรับ</a>
        </div>
    </div>
    </div>
</div>
