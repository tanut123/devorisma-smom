<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?= Meta::meta('title'); ?></title>
     <?= Meta::tagMetaName('robots'); ?>
    <?= Meta::tagMetaName('keywords'); ?>
    <?= Meta::tag('site_name'); ?>
    <?= Meta::tag('url', Request::url()); ?>
    <?= Meta::tag('title'); ?>
    <?= Meta::tag('description'); ?>
    <?= Meta::tag('image'); ?>
    <meta property="og:type" content="website"/>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:site" content="@s-mom" />
    <meta name="twitter:creator" content="@s-mom" />
	<meta name="csrf-token" content="[[csrf_token]]" />
	<base href="<?=url()?>" />

	<link rel="shortcut icon" href="<?= asset('favicon.ico')?>" />
	<link rel="canonical" href="<?=  Request::url()?>" />
	<style>html { visibility : hidden }</style>
	<script type="text/javascript">
		if (self == top) {
			document.documentElement.style.visibility= "visible";
		}else{
			top.location= self.location;
		}
		var csrf_token= "[[csrf_token]]";
	</script>
    @loadJs("master,nativeFileLoad,plainPreload")

	@yield('fb_header')
	@yield('style')
	@if (isset($custom_css))
    <link href="{{url()}}/custom_css/{{$custom_css_gen}}&{{$custom_css}}?f=19" rel="stylesheet">
	@endif
	@if (isset($product_css))
    <link href="{{url()}}/product_css/{{$product_css_gen}}&{{$product_css}}?f=19" rel="stylesheet">
	@endif

</head>
<body class="{{ trans('core.CORE_LANG') }}">
@if(strpos(url(),"s-momclub.com") > 0 )
	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TF9JNQ"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-TF9JNQ');</script>
	<!-- End Google Tag Manager -->
@endif

	<script type="text/javascript">
		var BASE_URL = '{{ url() }}';
		var BASE_LANG = '{{ $BASE_LANG }}';
		var BASE_CDN = '{{ $BASE_CDN }}';
		var CORE_SEARCH_ALERT_NULL = "{{ trans('core.CORE_SEARCH_ALERT_NULL') }}";
		var CORE_SEARCH_ALERT_MIN_WORD = "{{ trans('core.CORE_SEARCH_ALERT_MIN_WORD') }}";
		var SUBSCRIBE_MISMATCH = "{{trans('subscribe.SUBSCRIBE_MISMATCH')}}";
		var SUBSCRIBE_CHECK_CONDITION = "{{trans('subscribe.SUBSCRIBE_CHECK_CONDITION')}}";
		var LOGIN_USERNAME_VALIDATE = "{{trans('login.USERNAME_VALIDATE')}}";
		var LOGIN_PASSWORD_VALIDATE = "{{trans('login.PASSWORD_VALIDATE')}}";
		var LOGIN_FAIL = "{{trans('login.LOGIN_FAIL')}}";
		var LOGIN_PLEASE_WAIT = "{{trans('login.LOGIN_PLEASE_WAIT')}}";
		var LOGIN_NEED_VERIFY = "{{trans('login.LOGIN_NEED_VERIFY')}}";
		var INVALID_PASSWORD_EMAIL = "{{trans('login.INVALID_PASSWORD_EMAIL')}}";
		var BOOKMARK_SAVE_SUCCESS = "{{trans('member_web_bookmark.BOOKMARK_SAVE_SUCCESS')}}";
		var BOOKMARK_CONNOT_SAVE = "{{trans('member_web_bookmark.BOOKMARK_CONNOT_SAVE')}}";
		var FAVORITE_SAVE_SUCCESS = "{{trans('member_web.FAVORITE_SAVE_SUCCESS')}}";
		var FAVORITE_CANNOT_SAVE = "{{trans('member_web.FAVORITE_CANNOT_SAVE')}}";
		var CHANGE_PASS_SUCCESS = "{{trans('password.CHANGE_SUCCESS')}}";
		var CHANGE_PASS_USING = "{{trans('password.CHANGE_PASS_USING')}}";
		var CHANGE_PASS_EVER_USING = "{{trans('password.CHANGE_PASS_EVER_USING')}}";
		var CORE_CONFIRM_BUTTON = "{{trans('core.CORE_CONFIRM_BUTTON')}}";
		var CORE_OK_BUTTON = "{{trans('core.CORE_OK_BUTTON')}}";
		var CORE_CANCEL_BUTTON = "{{trans('core.CORE_CANCEL_BUTTON')}}";
		var CORE_EDIT_BUTTON = "{{trans('core.CORE_EDIT_BUTTON')}}";
		var CORE_REGISTER_COMPLETE_MESSAGE = "{{trans('register.REGISTER_COMPLETE_MESSAGE')}}";

	</script>

	<script type='text/javascript'>
     {{--
        Update stable version  form http://research.insecurelabs.org/jquery/test/
        - jQuery v1.12.2
      --}}
		plainFade();
		initFileload(["{{url()}}/css/@genFile('css','site,header,footer')?f=19","@writeCss()?f=19"],["{{url()}}/js/@genFile('js','jquery.min,bootstrap.min,jquery.placeholder,moment,site,header,footer,sub_menu,JJJud')","@writeJs()"],{callback:function(){
			fadeBodyIn();
				[[breastfeeding_popup]]
			}
		});
	</script>
	<div id="fb-root"></div>

	@include('template.offcanvas')
	@include('template.header')
	@yield('content')
	@include('template.footer')

	<script>
		window.fbAsyncInit = function() {
		FB.init({
		    appId      : '{{ $FACEBOOK_APPID }}',
		    cookie     : true,  // enable cookies to allow the server to access
		                        // the session
		    xfbml      : true,  // parse social plugins on this page
		    version    : 'v2.3' // use version 2.2
		  });
		};

	(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/{{ App::getLocale()=="en"?"en_US":"th_TH" }}/sdk.js";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>



	@yield('scripts')
	@yield('fb_footer')
	<div id="floatpopup"></div>
</body>
</html>

