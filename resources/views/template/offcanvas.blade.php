<nav id="menu-mobile" class="visible-xs {{ (App::getLocale() == 'en')? 'en': '' }} ">
	<span id="header-logo-mobile">
		<a href="{{ $BASE_LANG }}home">
			<img  src="{{ asset($BASE_CDN.'/images/header/logo-smom_new.png') }}" alt="s-mom-club-logo" width="180" height="36">
		</a>
	</span>
	<a id="header-search-mobile-btn" class="">
		<div class='icon-normal' width="25">&nbsp;</div>
		<div class='icon-close' width="25">&nbsp;</div>
{{--		<img class="icon-normal" width="25" src="{{ asset($BASE_CDN.'/images/header/icon-search-menu.png') }}" alt="btn-mobile">
		<img class="icon-close" width="22" src="{{ asset($BASE_CDN.'/images/header/icon-close.png') }}" alt="btn-mobile">--}}
	</a>
	<a id="header-menu-mobile-btn">
		<div class='icon-normal' width="25">&nbsp;</div>
		<div class='icon-close' width="25">&nbsp;</div>
{{--		<img class="icon-normal" width="30" src="{{ asset($BASE_CDN.'/images/header/btn-mobile.png') }}" alt="btn-mobile">
		<img class="icon-close" width="22" src="{{ asset($BASE_CDN.'/images/header/icon-close.png') }}" alt="btn-mobile">--}}
	</a>
	{{--<div class='contact-tail'>
        <a href="tel:026402288" style='color:#7B5700'>
        <div class='img_call_center'></div>
        <span class='bold_call_center'>0-2640-2288</span>
        </a>
    </div>--}}
	{{--<div class="view-panel">
		<div class="view-panel-inner">
			<span class="view-text">{{ trans('core.VISITOR_AMOUNT') }}</span>
			<span class="view-number-box">
				<img src="{{ $BASE_CDN }}/images/header/icon-view.png" alt="icon-view" width="24" height="19">
				<span class="view-number">[[VISITOR]]</span>
			</span>
			<span class="view-text">{{ trans('core.VISITOR_UNIT') }}</span>
		</div>
	</div> --}}
</nav>
<nav id="menu-mobile-search" class="menu-list hidden-sm hidden-md hidden-lg">
	<span id="menu-mobile-search-panel">
		<form id="submit-search" name="submit-search" action="{{ $BASE_LANG }}search" method='get'>
			<label class="label-placeholder" for="">{{ trans('core.CORE_SEARCH') }}</label>
			<input name="q" id="inp-search">
			<input type='hidden' value='[[csrf_token]]' name='csrf_token'>
			<a id="icon-search" class="">
				<div class="icon-normal" width="25" alt="search"></div>
			</a>
		</form>
	</span>
</nav>
<nav id="menu-mobile-list" class="menu-list hidden-sm hidden-md hidden-lg">
	@if(App::getLocale() == 'th')
	{{-- <div class="wyeth-panel {{ (App::getLocale() == 'en')? 'hidden-en': '' }} ">
		<a href="{{ url() }}/wyethnutrition">
			<div class="wyeth-panel-inner">
				<div class="wyeth-icon"></div>
				<div class="wyeth-text FBold">Wyeth Nutrition</div>
			</div>
		</a>
	</div>
	<div class="question-panel {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
		<a href="{{ url() }}/หมอเด็ก">
			<div class="question-panel-inner">
				<div class="question-icon"></div>
				<div class="question-text FBold anti_alias">คุณแม่ถาม-คุณหมอตอบ</div>
			</div>
		</a>
	</div> --}}
	<div class="joinmom-menu">
		<a href="{{ $BASE_LANG }}recruitment">
			<div class="joinmom-menu-inner FXBold">
				<div class="joinmom-menu-icon"></div><div class="joinmom-menu-text FBold">Join S-Mom Club</div>
			</div>
		</a>
	</div>
	<div class="online-shopping-menu">
		<a href="{{ url() }}/online_shopping">
			<div class="online-shopping-menu-inner FXBold">
				<div class="online-shopping-menu-icon"></div><div class="online-shopping-menu-text FBold">Online Shopping</div>
			</div>
		</a>
	</div>
	@endif
	<ul id="menu-mobile-list-panel" class="{{ (App::getLocale() == 'en')? 'en': '' }}">
		<li class="menu-list-mobile first">
			<a href="javascript:void(0);" class="FXregular">{{ trans('menu.MENU_MOMTIP') }}
			{{--<img src="{{ asset($BASE_CDN.'/images/footer/footer_arrow_down.png') }}" class="arrow-normal" width="22">
			<img src="{{ asset($BASE_CDN.'/images/footer/footer_arrow_down.png') }}" class="arrow-active" width="22">--}}
				<div class='arrow-normal'></div>
				<div class='arrow-active'></div>
			</a>
		</li>
		<ul class="menu-mobile-list-child">
			<li><a href="{{ $BASE_LANG }}pregnancy-mom" class="FXregular">{{ trans('menu.MENU_PREGNANCY_STAGE') }}</a></li>
			<li><a href="{{ $BASE_LANG }}lactating-mom" class="FXregular">{{ trans('menu.MENU_LACTATING_STAGE') }}</a></li>
			<li><a href="{{ $BASE_LANG }}toddler-mom" class="FXregular">{{ trans('menu.MENU_TODDLER_STAGE') }}</a></li>
		</ul>
		@if(App::getLocale() == 'th')
		{{-- <li class="menu-list-mobile {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
			<a href="javascript:void(0);" class="FXregular">1,000 วัน
				<div class='arrow-normal'></div>
				<div class='arrow-active'></div>
			</a>
		</li>
		<ul class="menu-mobile-list-child {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
			<li><a href='{{ $BASE_LANG }}1000-days' class='FXregular'>1,000 Days</a></li>
			<li><a href='{{ $BASE_LANG }}1000-days/first-270-days' class='FXregular'>First 270 Days</a></li>
			<li><a href='{{ $BASE_LANG }}1000-days/second-365-days' class='FXregular'>Second 365 Days</a></li>
			<li><a href='{{ $BASE_LANG }}1000-days/final-365-days' class='FXregular'>Final 365 Days</a></li>
		</ul> --}}
		<li class="menu-list-mobile">
			<a href="{{ url() }}/everyonecanbethestar" class="FXregular">ล้านคนล้านพรสวรรค์
			</a>
		</li>
		<li class="menu-list-mobile">
			<a href="{{ url() }}/สฟิงโกไมอีลิน" class="FXregular">สฟิงโกไมอีลิน
			</a>
		</li>
		<!--<li class="menu-list-mobile">
			<a href="{{ url() }}/พรสวรรค์สร้างได้ใน1000วันแรก" class="FXregular">Brain Ignition lab
			</a>
		</li>
  -->
		@endif
		<li class="menu-list-mobile">
			<a href="{{ $BASE_LANG }}braindevelopment" class="FXregular">{{ trans('menu.MENU_KNOWLEDGE') }}
			</a>
		</li>
		@if(App::getLocale() == 'th')
		<li class="menu-list-mobile {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
			<a href="javascript:void(0);" class="FXregular">s-26 member
			{{--<img src="{{ asset($BASE_CDN.'/images/footer/footer_arrow_down.png') }}" class="arrow-normal" width="22">
			<img src="{{ asset($BASE_CDN.'/images/footer/footer_arrow_down.png') }}" class="arrow-active" width="22">--}}
				<div class='arrow-normal'></div>
				<div class='arrow-active'></div>
			</a>
		</li>
		<ul class="menu-mobile-list-child {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
			<li><a href="{{ $BASE_LANG }}register" class="FXregular">สมัครสมาชิก</a></li>
			<li><a href="{{ $BASE_LANG }}register#privilege" class="FXregular">สิทธิประโยชน์</a></li>
		</ul>
		<li class="menu-list-mobile {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
			<a href="javascript:void(0);" class="FXregular">ผลิตภัณฑ์
			{{--<img src="{{ asset($BASE_CDN.'/images/footer/footer_arrow_down.png') }}" class="arrow-normal" width="22">
			<img src="{{ asset($BASE_CDN.'/images/footer/footer_arrow_down.png') }}" class="arrow-active" width="22">--}}
				<div class='arrow-normal'></div>
				<div class='arrow-active'></div>
			</a>
		</li>
		<ul class="menu-mobile-list-child {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
			<li><a href="{{ $BASE_LANG }}product/promama" class="FXregular">S-26 โปรมาม่า</a></li>
			<li><a href="{{ $BASE_LANG }}product/gold_progress" class="FXregular">S-26 โกลด์ โปรเกรส</a></li>
			{{--<li><a href="{{ $BASE_LANG }}product/pe_gold" class="FXregular">S-26 พีอี โกลด์</a></li>--}}
			<li><a href="{{ $BASE_LANG }}product/progress" class="FXregular">S-26 โปรเกรส</a></li>
			<li><a href="{{ url() }}/s-26organicprogress/home" class="FXregular">S-26 ออร์แกนิค โปรเกรส</a></li>	
			<li><a href="{{ url() }}/S26GoldProgressUHT" class="FXregular">S-26 โกลด์ โปรเกรส ยูเอชที</a></li>			
		</ul>
		<li class="menu-list-mobile {{ (App::getLocale() == 'en')? 'hidden-en': '' }}">
			<a href="{{ $BASE_LANG }}promotion" class="FXregular">โปรโมชั่น & กิจกรรม
			</a>
		</li>
		@endif
		<li class="menu-list-mobile">
			<a href="{{ url() }}/wyethnutrition" class="FXregular">
			Wyeth Nutrition
			</a>
		</li>
		<li class="menu-list-mobile">
			<a href="{{ $BASE_LANG }}team" class="FXregular">{{ trans('menu.MENU_CONTACT_US') }}
			</a>
		</li>
		<li class="language-list-panel FThin">

		</li>
	</ul>
</nav>
