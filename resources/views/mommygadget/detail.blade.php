@extends('template.master')

@compressCss("slick,slick-theme,mommygadget_detail")

@section('content')
<script type="text/javascript">
	var page_uid = "{{$data['mommygadget_id']}}";
</script>
	<div id="wapKnowledgeDetail">
		<div class="detail-panel">
			<div id="detail-content">
				<h1 id="detail-title">{{ $data["title"]}}</h1>
				<div id="detail-bar">
					<!-- <span id="detail-date" class="FThin">{{ $data["datestring"] }}</span> -->
					<span id="detail-views" class="FThin" style="visibility:hidden;">
					<img class="icon-view" src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-view.png') }}" alt="icon-view" width="25" height="14">
					<span id="total_view"> 0 </span> Views
					</span>
				</div>
				<div class="detail-wapper">
					@replaceStockUrl($data["detail"])
					<div class="clearfix"></div>
				</div>
				<a href='{{ $BASE_LANG."mommygadget" }}'>
					<div class="btn-back">ย้อนกลับ</div>
				</a>
			</div>
		</div>
	</div>
	<div id="bottom-panel">
		<div id="recommend">
			<div class="recommend-inner">
				<div class="recommend-panel">
					<div class="recommend-panel-text">
						<div class="recommend-text FLighter">{{ trans('momtip.MOMTIP_SHARE_TEXT') }}</div>
					</div>
					<div class="btn-share">
						<a id="facebook" class="share-link" data-url="{{ $BASE_LANG }}mommygadget/{{ $data['slug'] }}">
							<div class="icon-share facebook"></div><span class="text-share FXregular">Share on Facebook</span>
						</a>
					</div>
					<div class="btn-share">
						<a id="twitter" class="share-link" data-url="{{ $BASE_LANG }}mommygadget/{{ $data['slug'] }}">
							<div class="icon-share twitter"></div><span class="text-share FXregular">Share on Twitter</span>
						</a>
					</div>
				</div>
			</div>
		</div>
		<div id="rating">
				<div class="rating-inner">
					<div class="rating-panel FThin">
						<div class="rating-panel-text">
							<div class="rating-text">{{ trans('momtip.MOMTIP_RATING_TEXT') }}</div>
						</div>
						<div class="rating-panel-choice">
							<div class="rating-panel-choice-inner">
								<div data-val="1" class="rating-number rate1"><span class="rate-text-number FXregular">1</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<div data-val="2" class="rating-number rate2"><span class="rate-text-number FXregular">2</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<div data-val="3" class="rating-number rate3"><span class="rate-text-number FXregular">3</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<div data-val="4" class="rating-number rate4"><span class="rate-text-number FXregular">4</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<div data-val="5" class="rating-number rate5"><span class="rate-text-number FXregular">5</span><span class="rate-text-desc FXthin">{{ trans('momtip.MOMTIP_RATING_POINT') }}</span></div>
								<input type="hidden" name="rating" id="inp-rating">
							</div>
						</div>
					</div>
				</div>
		</div>
	</div>
		<?php if(count($data["relate"]) >= 1){ ?>
	<div id="relate-panel">
		<div id="relate-content">
			<div class="relate-title FXMed">
				ทันทุกเทคโนโลยีทันสมัย ตอบโจทย์คุณแม่สมัยใหม่
			</div>
			<div class="relate-momtip-panel-list">
			<ul id="relate-list">
				<?php foreach($data["relate"] as $item){ ?>
				<li data-id="{{ $item->mommygadget_id }}" class="relate-item list-item-momtip">
					<span class="relate-item-image">
						<a href="{{ $BASE_LANG }}mommygadget/{{ $item->slug }}">
							<img src="@readFileName($item->image_gen,$item->image,'c255x136','mommygadget')" alt="{{ $item->title }}" width="255" height="136"/>
						</a>
					</span>
					<span class="relate-item-detail">
						<span class="relate-item-detail-title FXregular">
							@trimWithDot($item->title, 50)
						</span>
					</span>
					<a href="{{ $BASE_LANG }}mommygadget/{{ $item->slug }}" class="relate-item-detail-link">
						<img class="icon-link" width="30px" height="30" src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-link.png') }}" alt="icon-link">
					</a>
				</li>
				<?php } ?>
			</ul>
			</div>
		</div>
	</div>
	<?php } ?>

<script type="text/javascript">
	var CONFIRM_RATING = "{{ trans("momtip.CONFIRM_RATING") }}";
</script>

@endsection

@compressJs("slick.min,mommygadget_detail,favorite")
