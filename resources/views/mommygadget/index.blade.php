@extends('template.master')
@compressCss("mommygadget")
@section('content')
	<div id="wapKnowledge">
		<div class="list-content-panel list-all-item">
			<div class="header-content lactating">
				<div class="title FXMed">เทรนด์คุณแม่</div>
				<div class="txt-title FThin">
					รวมเรื่องทันสมัย อัพเดททุกเทคโนโลยี
					<div class="txt-desc FX">เตรียมความพร้อมให้กับคุณแม่ยุคใหม่</div>
				</div>
			</div>
			<ul class="list-panel">

			</ul>
			<div class="clearfix"></div>
			<div class="btn-bar">
				<div class="btn-style load-more" style="display:none;">
					{{ trans('loadmore.LOADMORE') }}
				</div>
			</div>
		</div>
	</div>
@endsection
@compressJs("mommygadget")
