@extends('template.master')
@compressCss("slick,slick-theme")
@compressCss("app")
@compressCss("smartbrain")

@section('content')
<div id="smartbrain">

	<div class="smart_banner">
		<div class="smart_banner_slide">
			<div>
				<a href="{{ url() }}/สมองดีเรียนรู้ไว/พรสวรรค์ในแบบของเค้า">
					<img src="{{ $BASE_CDN }}/images/smartbrand/banner/img01_mobile.jpg" class="img-responsive visible-xs">
					<img src="{{ $BASE_CDN }}/images/smartbrand/banner/img01.jpg" class="img-responsive hidden-xs">
				</a>
			</div>
			
		</div>
	</div>
	<div class="smart_foundation">
		<div class="smart_foundation_slide">
			<div>
				<img src="{{ $BASE_CDN }}/images/smartbrand/foundation/img01_mobile.jpg" class="img-responsive visible-xs">
				<img src="{{ $BASE_CDN }}/images/smartbrand/foundation/img01.jpg" class="img-responsive hidden-xs">
			</div>
			
		</div>
	</div>
	<div class="smart_video">
		<div class="smart_video_wrap">
			<img src="{{ $BASE_CDN }}/images/smartbrand/video/img_mobile_01.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/video/img_01.jpg" class="img-responsive hidden-xs">
			<div class="wrapvideo">
			 	<div class="video">
			 		<div class="embed-responsive embed-responsive-16by9" id="video-box">
	                   <iframe class="embed-responsive-item" id="autoplay-video" src="https://www.youtube.com/embed/NvDB4bPRdvw?rel=0&amp;enablejsapi=1" allowfullscreen></iframe>
	                </div> 
			 	</div>
			 </div>
		</div>
		<div>
			<a href="{{ url() }}/everyonecanbethestar">
			<img src="{{ $BASE_CDN }}/images/smartbrand/video/img_mobile_02.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/video/img_02.jpg" class="img-responsive hidden-xs">
			</a>
		</div>
		<div>
			
				<img src="{{ $BASE_CDN }}/images/smartbrand/video/img_mobile_03.jpg" class="img-responsive visible-xs">
				<img src="{{ $BASE_CDN }}/images/smartbrand/video/img_03.jpg" class="img-responsive hidden-xs">
			
		</div>
	</div>
	<div class="smart_doc">
		<div class="smart_doc_slide">
			<div>
				<a href="javascript:void(0);">
					<img src="{{ $BASE_CDN }}/images/smartbrand/doc/img_mobile01.jpg" class="img-responsive visible-xs">
					<img src="{{ $BASE_CDN }}/images/smartbrand/doc/img01.jpg" class="img-responsive hidden-xs">
				</a>
			</div>
			
		</div>
	</div>
</div>



	<script>
	window.lazySizesConfig = window.lazySizesConfig || {};
	window.lazySizesConfig.customMedia = {
    	'--mb': '(max-width: 767px)'
	};
</script>
@endsection


@compressJs("lazy.bgset.min,lazysizes.min,slick.min,web")
@section('scripts')


@endsection


