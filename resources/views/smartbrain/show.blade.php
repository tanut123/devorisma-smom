@extends('template.master')
@compressCss("slick,slick-theme")
@compressCss("app")
@compressCss("smartbrain")

@section('content')
<div id="smartbrain">

	<div class="smart_detail">
		<div>
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_01.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_01.jpg" class="img-responsive hidden-xs">
		</div>
		<div>
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_02.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_02.jpg" class="img-responsive hidden-xs">

		</div>
		<div>
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_03.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_03.jpg" class="img-responsive hidden-xs">
		</div>
		<div  class="boxSlide4">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_04.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_04.jpg" class="img-responsive hidden-xs">
			<div class="wrap_slide">
				<div class="smart_detail_slide">
					<div>
						<img src="{{ $BASE_CDN }}/images/smartbrand/detail/img01.png" class="img-responsive">
					</div>
					<div>
						<img src="{{ $BASE_CDN }}/images/smartbrand/detail/img02.png" class="img-responsive">
					</div>
					<div>
						<img src="{{ $BASE_CDN }}/images/smartbrand/detail/img03.png" class="img-responsive">
					</div>
					<div>
						<img src="{{ $BASE_CDN }}/images/smartbrand/detail/img02.png" class="img-responsive">
					</div>
					
				</div>
			</div>
		</div>
		<div>
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_05.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_05.jpg" class="img-responsive hidden-xs">
		</div>
		<div>
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_06.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_06.jpg" class="img-responsive hidden-xs">
		</div>
		<div>
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_07.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_07.jpg" class="img-responsive hidden-xs">
		</div>
		<div class="boxSlide8">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_08.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_08.jpg" class="img-responsive hidden-xs">
			<div class="wrap_slide">
				<div class="smart_detail_slide">
					<div>
						<img src="{{ $BASE_CDN }}/images/smartbrand/detail/img04.png" class="img-responsive">
					</div>
					<div>
						<img src="{{ $BASE_CDN }}/images/smartbrand/detail/img05.png" class="img-responsive">
					</div>
					<div>
						<img src="{{ $BASE_CDN }}/images/smartbrand/detail/img06.png" class="img-responsive">
					</div>
					<div>
						<img src="{{ $BASE_CDN }}/images/smartbrand/detail/img05.png" class="img-responsive">
					</div>
					
				</div>
			</div>
		</div>
		<div>
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_09.jpg" class="img-responsive visible-xs">
			<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_09.jpg" class="img-responsive hidden-xs">
		</div>
		<div>
			<a href="{{ url() }}/สมองดีเรียนรู้ไว">
				<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_mobile_10.jpg" class="img-responsive visible-xs">
				<img src="{{ $BASE_CDN }}/images/smartbrand/detail/detail_10.jpg" class="img-responsive hidden-xs">
			</a>
		</div>
	</div>
	<div class="smart_doc">
		<div class="smart_doc_slide">
			<div>
				<a href="javascript:void(0);">
					<img src="{{ $BASE_CDN }}/images/smartbrand/doc/img_mobile01.jpg" class="img-responsive visible-xs">
					<img src="{{ $BASE_CDN }}/images/smartbrand/doc/img01.jpg" class="img-responsive hidden-xs">
				</a>
			</div>
			
		</div>
	</div>
</div>


	<script>
	window.lazySizesConfig = window.lazySizesConfig || {};
	window.lazySizesConfig.customMedia = {
    	'--mb': '(max-width: 767px)'
	};
</script>
@endsection



@compressJs("lazy.bgset.min,lazysizes.min,slick.min,web")
@section('scripts')

@endsection
