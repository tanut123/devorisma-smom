@extends('template.master')

@compressCss("mommygadget_detail")

@section('content')
<script type="text/javascript">
	var page_uid = "{{$data['syntom_id']}}";
</script>
	<div id="wapKnowledgeDetail">
		<div class="detail-panel">
			<div id="detail-content">
				<h1 id="detail-title">{{ $data["title"]}}</h1>
				<div id="detail-bar">
					<!-- <span id="detail-date" class="FThin">{{ $data["datestring"] }}</span> -->
					<span id="detail-views" class="FThin" style="visibility:hidden;">
					<img class="icon-view" src="{{ asset($BASE_CDN . '/images/momtip/detail/icon-view.png') }}" alt="icon-view" width="25" height="14">
					<span id="total_view"> 0 </span> Views
					</span>
				</div>
				<div class="detail-wapper">
					@replaceStockUrl($data["detail"])
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

@endsection

@compressJs("syntom_detail")