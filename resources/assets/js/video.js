var tempWidth;
$(document).ready(function () {
	$('#video_list').slick({
		dots: false,
		slidesToShow: 4,
		slidesToScroll: 4,
		responsive: [
			{
				breakpoint: 767,
			    settings: {
			    	slidesToShow: 1,
			        slidesToScroll: 1,
			        dots: false
			    }
			}
		]
	});
    // //www.youtube.com/embed/-t2CR9qZRj0?autoplay=1&amp;wmode=transparent
    tempWidth = $(window).width();
    initVideoAction();
});
$(window).resize(function() {
    if ($(window).width() < 768 && tempWidth > 767) {
        tempWidth = $(window).width();
        setTimeout("initVideoAction()",1000);
    }
    if ($(window).width() > 767 && tempWidth < 768) {
        tempWidth = $(window).width();
        setTimeout("initVideoAction()",1000);
    }
});

function initVideoAction() {
    $(".close_video").click(function() {
        $(".video_iframe").attr("src","");
        $("#video_light_box").css("display","none");
    });
    $(".video_btn_play").click(function() {
        var video_data = $(this).attr("video_data");

        var hl_panel = $("#youtube_iframe");
        if (hl_panel.attr("video_type") == "youtube") {
            var hl_video = document.getElementById("youtube_iframe").contentWindow;
            hl_video.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
        } else {
            var hl_panel = document.getElementById("youtube_iframe");
            hl_panel.pause();
            //alert(video_data);
            //new_video.attr("src",video_data);

                /*
                 <video id='youtube_iframe' video_type='mp4' controls>
                 <source src="{{$hl_mp4_file}}" type="video/mp4">
                 </video>*/
        }
        if ($(this).attr("type") == "youtube") {
            var new_iframe = $("<iframe>",{allowfullscreen:"",frameborder:"0",src:"https://www.youtube.com/embed/"+video_data+"?enablejsapi=1&autoplay=1"}).addClass("video_iframe");
            $(".video_panel").html("");
            $(".video_panel").append(new_iframe);
            $("#video_light_box").css("display","table");
        } else {
            var new_video = $("<video>",{controls:"true",autoplay:"autoplay"}).addClass("video_iframe");
            var new_source = $("<source>",{type:"video/mp4"}).attr("src",video_data);
            new_video.append(new_source);
            $(".video_panel").html("");
            $(".video_panel").append(new_video);
            $("#video_light_box").css("display","table");
        }
        //<iframe class="video_iframe" id='video_light_box_iframe' allowfullscreen="" frameborder="0" src="https://www.youtube.com/embed/{{$dt_video_hl}}?enablejsapi=1&autoplay=1"></iframe>
    });
}