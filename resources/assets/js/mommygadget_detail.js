var tempRateClick = false;
$(document).ready(function () {
	$('#relate-list').slick({
		dots: false,
	  	infinite: false,
	  	slidesToShow: 2,
		slidesToScroll: 1,
		variableWidth: true,
		responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: false,
		        dots: false,
		        variableWidth: true
		      }
		    }
		 ]
	});

	$("#total_view").load(BASE_LANG + "page-view/mommygadget/"+page_uid,function(){
		$("#detail-views").css("visibility","visible");
	});

	var data_rate = localStorage.getItem("rating_"+page_uid);
	if(data_rate == null){
		hoverRating();
		clickRating();
	}else{
		$(".rate"+data_rate).addClass("active");
		$(".rate"+data_rate).append("<div class='rating-active'></div>");
	}

})

function hoverRating(){
	$(".rating-number").mouseover(function(){
		var nowSelect = $(this);
		var answerVal = nowSelect.data("val");
		var anserSelect = nowSelect.parent().find(".rating-number");
		nowSelect.append("<div class='rating-active'></div>");
		nowSelect.addClass("active");
	}).mouseleave(function(event) {
		if (tempRateClick == false){
			ratingClearHover();
		}
	});
}

function ratingClearHover(){
	$(".rating-number").removeClass("active");
	$(".rating-active").remove();
}

function clickRating(){
	$(".rating-number").bind("click touchstart",function(event){
		event.preventDefault();
		tempRateClick = true;
		var nowSelect = $(this);
		var answerVal = nowSelect.data("val");
		nowSelect.nextAll("input").val(answerVal);
		var anserSelect = nowSelect.parent().find(".rating-number");
		nowSelect.append("<div class='rating-active'></div>");
		alert(CONFIRM_RATING);
		setTimeout(function(){
			ratingSend();
			$('.modal').modal("hide");
			$(".rating-number").unbind( "click touchstart" );
			$(".rating-number").unbind("mouseover");
		}, 2000);
	});
}

function ratingSend(){
	if(checkValRating()){
		var rate = $('#inp-rating').val();
		var data = "&page_uid="+ page_uid + "&rate=" + rate;
		$.ajax({
			url: BASE_LANG + "mommygadget/rating",
			data: "mode=add" + data,
			type: "POST",
			cache: false,
			success: function(){
				localStorage.setItem("rating_"+page_uid,rate);
			}
		});
	}else{

	}
}

function checkValRating(){
	var answer = false;
	if($("#inp-rating").val() != ""){
		answer = true;
	}
	return answer;

}