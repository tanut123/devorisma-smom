var dataProduct = [
	{
		"name": "S-26 Progress GOLD 600g",
		"weight": "600 g.",
		"image": [
			BASE_CDN + "/images/online_shopping/600/1.png",
			BASE_CDN + "/images/online_shopping/600/2.png",
			BASE_CDN + "/images/online_shopping/600/3.png",
			BASE_CDN + "/images/online_shopping/600/4.png",
		],
		"retailer": [
			{
				"logo": "lazada",
				"pack_size": 1,
				"link": "http://bit.ly/2w9EpLI"
			},
			{
				"logo": "shopee",
				"pack_size": 1,
				"link": "http://bit.ly/2w5MFfG"
			},
			{
				"logo": "jdcentral",
				"pack_size": 1,
				"link": "http://bit.ly/2MvbThH"
			},
		]
	},
	{
		"name": "S-26 Progress GOLD 1800g",
		"weight": "1,800 g.",
		"image": [
			BASE_CDN + "/images/online_shopping/1800/1.png",
			BASE_CDN + "/images/online_shopping/1800/2.png",
			BASE_CDN + "/images/online_shopping/1800/3.png",
			BASE_CDN + "/images/online_shopping/1800/4.png",
		],
		"retailer": [
			{
				"logo": "tops",
				"pack_size": 1,
				"link": "http://www.tops.co.th/p/s26-progress-gold-milk-powder-180kg"
			},
		]
	},
	{
		"name": "S-26 Progress GOLD 2400g",
		"weight": "2,400 g.",
		"image": [
			BASE_CDN + "/images/online_shopping/2400/1.png",
			BASE_CDN + "/images/online_shopping/2400/2.png",
			BASE_CDN + "/images/online_shopping/2400/3.png",
			BASE_CDN + "/images/online_shopping/2400/4.png",
		],
		"retailer": [
			{
				"logo": "lazada",
				"pack_size": 1,
				"link": "http://bit.ly/2NPVEfN"
			},
			{
				"logo": "shopee",
				"pack_size": 1,
				"link": "http://bit.ly/2Mlv9Pp"
			},
			{
				"logo": "jdcentral",
				"pack_size": 1,
				"link": "http://bit.ly/2vPySdH"
			},
		]
	},
	{
		"name": "S-26 Progress GOLD 3000g",
		"weight": "3,000 g.",
		"image": [
			BASE_CDN + "/images/online_shopping/3000/1.png",
			BASE_CDN + "/images/online_shopping/3000/2.png",
			BASE_CDN + "/images/online_shopping/3000/3.png",
			BASE_CDN + "/images/online_shopping/3000/4.png",
		],
		"retailer": [
			{
				"logo": "lazada",
				"pack_size": 1,
				"link": "http://bit.ly/2w9cPOv"
			},
			{
				"logo": "shopee",
				"pack_size": 1,
				"link": "http://bit.ly/2nHw5yQ"
			},
			{
				"logo": "jdcentral",
				"pack_size": 1,
				"link": "http://bit.ly/2KWPQf2"
			},
		]
	},
];

var logoRetailer = {
	"tops": BASE_CDN + "/images/online_shopping/logo-tops.svg",
	"lazada": BASE_CDN + "/images/online_shopping/logo-lazada.svg",
	"shopee": BASE_CDN + "/images/online_shopping/logo-shopee.svg",
	"24shop": BASE_CDN + "/images/online_shopping/logo-24shop.svg",
	"jdcentral": BASE_CDN + "/images/online_shopping/logo-jdcentral.svg",
}

function openLightbox(code){
	var lb = $(".online-lightbox-panel");
	var btn = lb.find(".btn-close");
	var title = lb.find(".lightbox-title");
	var body = lb.find(".lightbox-body");
	
	var html = '<div class="lightbox-slide-panel"><div class="slide-box">';

				$.each(dataProduct[code]["image"],function(a,e){
					html += '<div class="slide-list"><img class="lazyload" data-src="'+e+'" data-expand="+10"/></div>';
				});
			
				html += '</div></div><div class="lightbox-retailer"><div class="list-panel">';
					html += '<div class="list">';
						html += '<div class="col1 FXMed">Retailer</div>';
						html += '<div class="col2 FXMed">Pack Size</div>';
						html += '<div class="col3 FXMed">Weight</div>';
						html += '<div class="col4 FXMed"></div>';
					html += '</div>';
				$.each(dataProduct[code]["retailer"],function(a,e){
					html += '<div class="list">';
						html += '<div class="col1">';
						html += '<img class="lazyload" data-src="'+logoRetailer[e.logo]+'" data-expand="+10" alt="'+e.logo+'" />';
						html += '</div>';
						html += '<div class="col2">'+e.pack_size+'</div>';
						html += '<div class="col3">'+dataProduct[code]["weight"]+'</div>';
						html += '<div class="col4">';
						html += '<a href="'+e.link+'" target="_blank" class="btn-buy FXMed"><span class="icon"></span><span class="text">ซื้อสินค้า</span></a>';
						html += '</div>'
					html +='</div>';
				});
					html += '</div><div class="term"><div class="title FXMed">Terms and conditions</div><div class="desc">You will be redirected to one of our partner sites. Please be sure to read their terms and conditions. Wyeth Nutrition is not liable for our partner sites.</div></div>';
				html += '</div>';
				
	title.html(dataProduct[code]["name"]);
	body.html(html);

	lb.addClass("active");

	setTimeout(function(){
		$(".lightbox-slide-panel .slide-box").slick({
			dots: true,
			infinite: true,
			arrow: false,
			autoplay: true,
			autoplaySpeed: 5000,
		});
		$(".lightbox-slide-panel").addClass("init");
	},100);

	btn.unbind("click");
	btn.on("click",function(){
		lb.removeClass("active");
		$(".lightbox-slide-panel .slide-box").slick('unslick');
		$(".lightbox-slide-panel").removeClass("init");
	});

}