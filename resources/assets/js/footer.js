var tempWidth;
var tempExpandElement;
var tempParentElement;
$(document).ready(function () {

    $(".event_penal").click(function(){
        //if ($(window).width() > 767) {
            var pathName = window.location.pathname;
            if ($("#footer_panel .footer_signup .data_panel .detail_panel").css("display") == "block") {
                $("#footer_panel .footer_signup .data_panel .detail_panel").slideUp();

                $("#footer_panel .footer_bottom_bg .footer_signup .data_panel .link_img").addClass("img_deactive");
                var date = new Date();
                var minutes = 5;
                date.setTime(date.getTime() + (minutes * 60 * 1000));
                //$.cookie("member_signup","0",{expires : date,path: pathName});
                setMemberSignUpPanel(2,pathName);
            } else {
                $("#footer_panel .footer_signup .data_panel .detail_panel").slideDown();

                $("#footer_panel .footer_bottom_bg .footer_signup .data_panel .link_img").removeClass("img_deactive");

                var date = new Date();
                var minutes = 5;
                date.setTime(date.getTime() + (minutes * 60 * 1000));
                //$.cookie("member_signup","1",{expires : date,path: pathName});
                //setMemberSignUpPanel(1,pathName);
            }
        /*} else {
            window.location = "google.com";
        }*/
    });
/*  if ($.cookie("member_signup") == "0") {
        console.log($.cookie("member_signup"));
        $("#footer_panel .footer_signup .data_panel .detail_panel").slideUp();
        $("#footer_panel .footer_bottom_bg .footer_signup .data_panel .link_img").addClass("img_deactive");
    } else {
        $("#footer_panel .footer_signup .data_panel .detail_panel").slideDown();
        $("#footer_panel .footer_bottom_bg .footer_signup .data_panel .link_img").removeClass("img_deactive");
    }*/
    $(window).scroll(function() {
        if ($("#footer_panel .footer_signup .data_panel .detail_panel").css("display") != "none") {
            $("#footer_panel .footer_signup .data_panel .detail_panel").slideUp();
            $("#footer_panel .footer_bottom_bg .footer_signup .data_panel .link_img").addClass("img_deactive");
        }
    });

    width_w = $(window).width();
	$(".has_child").after().unbind("click");
	if(width_w <= 767){
		linkFooterClick();
	}

    $(window).resize(function() {
        	win_width = $(this).width();
        if (win_width != tempWidth) {
            tempWidth = win_width;
            $(".site_map_child").removeAttr("style");
            $(".site_map_child").removeClass("visible");
            $(".has_child_active").addClass("has_child");
            $(".has_child_active").removeClass("has_child_active");
        }
		$(".has_child").after().unbind("click");
		if(win_width <= 767){
			linkFooterClick();
		}
    });
    $(".mobile_submit").click(function() {
        sendSubscribe();
    });
    $(".email_submit").click(function() {
        sendSubscribe();
    });
    tempWidth = $(window).width();
    if (tempWidth < 768) {
        $("#footer_panel .footer_bottom_bg .footer_signup .data_panel .link_img").addClass("img_deactive");
    }
    $(".email_input").keypress(function(event) {
        if (event.keyCode == 13) {
            sendSubscribe();
            return false;
        }
    });
    $(".check_age_text").click(function() {
        if (document.getElementById("check_age").checked == false) {
            document.getElementById("check_age").checked = true;
        } else {
            document.getElementById("check_age").checked = false;
        }
        //console.log(document.getElementById("check_age").checked);
    });
    setMemberSignup();

    if(LANG == "en"){
    	$('.footer_bottom .link_box').eq(0).before($('.subscribe_panel'));
    }

});

function linkFooterClick(){
	$(".has_child").after().click(function() {
		var menu = $(this);
		var menu_child = $(this).next();
		if($(".site_map_child").is(":animated")){
			return false;
		}
		if(menu_child.hasClass("site_map_child")){
			if(menu.hasClass("has_child_active")){
				menu.removeClass("has_child_active").addClass("has_child");
				menu_child.removeClass("visible").slideUp();
			}else{
				if($(".site_map_child").hasClass("visible")){
					$(".link_parent.has_child_active").removeClass("has_child_active").addClass("has_child");
					$(".site_map_child.visible").removeClass("visible").slideUp();
				}
				menu_child.addClass("visible").slideDown();
				menu.removeClass("has_child").addClass("has_child_active");
			}
		}
    });

    $(".footer_link_bold").click(function(){
    	var link = $(this);
    	var parent_link = link.parent();
    	if(parent_link.hasClass("has_child")){
    		parent_link.click();
    		return false;
    	}
    });

	// $(".footer_link_bold").click(function() {
 //        if ($(this).parent().hasClass("has_child_active") && tempWidth > 767) {
 //            return true;
 //        } else {
 //            if ($(this).parent().hasClass("has_child")) {
 //                var child_num = $(this).parent().parent().children().length;
 //                for (var i = 0;i<child_num;i++) {
 //                    var child_element = $($(this).parent().parent().children()[i]);
 //                    if (child_element.hasClass("site_map_child")) {
 //                        if (child_element.hasClass("visible")) {
 //                            child_element.removeClass("visible");
 //                            child_element.slideUp();
 //                            $(this).parent().removeClass("has_child_active");
 //                            $(this).parent().addClass("has_child");
 //                        } else {
 //                            if (tempExpandElement && tempParentElement ) {
 //                                tempParentElement.removeClass("has_child_active");
 //                                tempParentElement.addClass("has_child");
 //                                tempExpandElement.removeClass("visible");
 //                                tempExpandElement.slideUp();
 //                            }

 //                            tempParentElement = $(this).parent();
 //                            tempExpandElement = child_element;

 //                            child_element.addClass("visible");
 //                            child_element.slideDown();
 //                            $(this).parent().removeClass("has_child");
 //                            $(this).parent().addClass("has_child_active");
 //                        }
 //                    }
 //                }
 //                return false;
 //            } else {
 //                return true;
 //            }
 //        }
 //    });
}
function setMemberSignup() {
    if (tempWidth < 768) {
        $("#footer_panel .footer_signup .data_panel .detail_panel").css("display","none");
    } else {
        if ($("#show_member_signup").attr("show_value") == "true") {
            $("#footer_panel .footer_signup .data_panel .detail_panel").css("display","block");
        } else {
            $("#footer_panel .footer_signup .data_panel .detail_panel").css("display","none");
        }
    }
}


function sendSubscribe() {
    var email = $(".email_input").val();
    if (validateEmail(email) == true) {
        if (document.getElementById("check_age").checked == true) {
            $.ajax({
                url: BASE_LANG + "footer/subscribe",
                type: "POST",
                data: {"email" : email, "_token": csrf_token},
                dataType: "json"
            })
                .success(function (data) {
                    if (data.resp == "success") {
                        alert(data.msg);
                        $(".email_input").val("");
                        document.getElementById("check_age").checked = false;
                    } else {
                        alert(data.msg);
                    }
                })
        } else {
            alert(SUBSCRIBE_CHECK_CONDITION);
        }
    } else {
        alert(SUBSCRIBE_MISMATCH);
    }
}
function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}
function setMemberSignUpPanel(state,path) {
    $.ajax(
        {
            url: BASE_LANG + "footer/setMemberSignUpPanel",
            type: "POST",
            data: {p_status:state, s_path:path, _token:csrf_token},
            dataType: "json"

        }
    );
}
function setMemberBreastfeedingPanel(state,path) {
    $.ajax(
        {
            url: BASE_LANG + "footer/setPopupBreastfeeding",
            type: "POST",
            data: {p_status:state, s_path:path},
            dataType: "json"

        }
    );
}