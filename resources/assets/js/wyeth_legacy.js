$(document).ready(function() {
    var url = window.location.href;
    if (url.indexOf("tesco") > -1) {
        createPopupQR("tesco");
    }else if(url.indexOf("bigc") > -1){
        createPopupQR("bigc");
    }else if(url.indexOf("tops") > -1){
        createPopupQR("tops");
    }
});

function createPopupQR(product){
    $("body").addClass("hasPopup");

    var promotionText = "";

    if(product == "tesco"){
        // promotionText = 'ส่วนลด 100 บาท เมื่อซื้อ S-26 Progress Gold ขนาด 3,000 กรัม ที่ เทสโก้โลตัส';
    }else if(product == "bigc"){
        // promotionText = 'ส่วนลด 100 บาท เมื่อซื้อ S-26 Progress Gold ขนาด 3,000 กรัม ที่ บิ๊กซี';
    }else if(product == "tops"){
        // promotionText = 'ส่วนลด 50 บาท เมื่อซื้อผลิตภัณฑ์ S-26 ครบ 1,000 บาท';
    }

    // var htmlPopup = '<div class="popup-panel"><div class="popup-panel-inner"><div class="popup-panel-box"><div class="popup-close"></div><div class="title-panel"><div class="text-top FMonXMed"><span class="highlight FMonXBold color-red">ง่ายๆ</span> แค่สแกน <span class="highlight FMonXBold color-red">QR Code</span></div><div class="text-bottom FMonX">แล้วรับสิทธิ์ <span class="highlight FMonXBold color-red">E-Coupon</span> สุดคุ้มมากมาย</div></div><div class="qr-panel"><div class="qr-panel-inner"><div class="qr-image '+product+'"></div><div class="qr-text"><div class="text-free FMonXBold color-red">แจกฟรี!!</div><div class="text-coupon FMonXBold">E-coupon</div><div class="promotion-detail FX">'+promotionText+'</div></div></div></div></div></div></div>';
    var htmlPopup = '<div class="popup-panel"><div class="popup-panel-inner"><div class="popup-panel-box"><div class="popup-close"></div><div class="promotion-panel"><img class="promotion-img" src="'+BASE_CDN+'/images/wyeth/legacy/popup/coupon-tops.jpg"></div></div></div></div></div>';

    $(".legacy-panel").append(htmlPopup);

    $(".popup-close").click(function(){
        closePopupQR();
    });
}

function closePopupQR(){
    $("body").removeClass("hasPopup");
    $(".popup-panel").remove();
}