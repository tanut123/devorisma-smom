$(document).ready(function(){
    progressGold();
});

function progressGold(){
	$('#image-map-desktop[usemap]').rwdImageMaps();
	$('#image-map-mobile[usemap]').rwdImageMaps();

	var ww = $(this).width();
	var posY1 = 0;
	var posY2 = 0;
	var posY3 = 0;
	if(ww > 1280){
		posY1 = (ww - 1280) * 0.65;
		posY2 = (ww - 1280) * 1.08;
		posY3 = (ww - 1280) * 1.51;
	}else if(ww <= 1280 && ww > 767){
		posY1 = 0;
		posY2 = 0;
		posY3 = 0;
	}else{
		posY1 = (767 - ww) * -1.428;
		posY2 = (767 - ww) * -2.76;
		posY3 = (767 - ww) * -3.77;
	}
	$("#panel1").css("transform","translateY("+posY1+"px)");
	$("#panel2").css("transform","translateY("+posY2+"px)");
	$("#panel3").css("transform","translateY("+posY3+"px)");

	$(window).resize(function(){
		var ww = $(this).width();
		var posY1 = 0;
		var posY2 = 0;
		var posY3 = 0;
		if(ww > 1280){
			posY1 = (ww - 1280) * 0.65;
			posY2 = (ww - 1280) * 1.08;
			posY3 = (ww - 1280) * 1.51;
		}else if(ww <= 1280 && ww > 767){
			posY1 = 0;
			posY2 = 0;
			posY3 = 0;
		}else{
			posY1 = (767 - ww) * -1.428;
			posY2 = (767 - ww) * -2.76;
			posY3 = (767 - ww) * -3.77;
		}
		$("#panel1").css("transform","translateY("+posY1+"px)");
		$("#panel2").css("transform","translateY("+posY2+"px)");
		$("#panel3").css("transform","translateY("+posY3+"px)");
	});
}