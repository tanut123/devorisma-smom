DESKTOP_LIST = 6;
MOBILE_LIST = 3;
LIST = 6;

$(document).ready(function(){
	initList();
	loadMore();
	manageBookmark();
	initSlideArticle();
});

function loadMore(){
	$("#loadmore").click(function(){
		callAjaxList(getList(), LIST);
	})
}
function initList(){
	checkListLimit();
	callAjaxList(0,LIST);
}

function checkListLimit(){
	LIST = DESKTOP_LIST;
	if(isMobileDevice()){
		LIST = MOBILE_LIST;
	}
}

function initSlideArticle(){
	$("#last-article-panel-content-list").slick({
		dots: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        variableWidth: true,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
  					variableWidth: false
                }
            }
        ]
	});
	setHeightDescription();
}

function getList(){
	return $("#list-panel .list-item").length;
}

function callAjaxList(page, limit){
	$("#list-panel").append(loadingAjax());
	$.ajax({
		url: BASE_LANG + 'bookmark/list',
		type: 'POST',
		data: {  'page': page, 'limit': limit},
		dataType: 'json'
	})
	.success(function(d) {
		if(page == 0){
			$("#list-panel").html(d["html"]);
		}else{
			$("#list-panel").append(d["html"]);
		}
		$("#list-panel").find(".global-loading").remove();
		if(d["l"] == "y"){
			$("#loadmore").show();
		}else{
			$("#loadmore").hide();
		}
		$("#bookmark_count").html(d["count"])
		checkListLoadControl();
		callListFavorite();
		setHeightDescription();
	}).fail(function() {
		$("#list-panel").find(".global-loading").remove();
		$("#list-panel").html(dataNotFound());
		$("#list-content").hide();
	})
}

function getType(){
	return $(".panel_cat").data("type");
}

function manageBookmark(){
	$("#btn-manage-bookmark").click(function(){
		var btn = $(this);
		if(btn.hasClass("active")){
			btn.removeClass("active");
			removeCotrol();
		}else{
			btn.addClass("active");
			listShowControl();
		}
	})
}

function checkListLoadControl(){
	if($("#btn-manage-bookmark").hasClass("active")){
		listShowControl();
	}else{
		removeCotrol();
	}
}

function listShowControl(){
	$("#list-panel .list-item").each(function(){
		var nowElm = $(this);
		if(!nowElm.hasClass("control")){
			nowElm.append("<a class='icon-bookmark control-remove'></a>");
			removeClick();
		}
	});
}

function removeClick(){
	$(".control-remove").click(function(){
		var selecItem = $(this);
		var itemData = selecItem.parent();
		var footer = {foot: {}};
		footer.foot[CORE_CONFIRM_BUTTON] = function(){ removeBookmark(itemData.data("id")); };
		footer.foot[CORE_CANCEL_BUTTON] = close;
		alert(BOOKMARK_CONFIRM_REMOVE,"",footer);
	});
}

function removeBookmark(momtip_id){
	$.ajax({
		url: BASE_LANG + 'bookmark/remove',
		type: 'POST',
		data: {'momtip_id': momtip_id},
		dataType: 'json'
	})
	.success(function(d) {
		$(".modal").modal('hide');
		if(d["cmd"] == "y"){
			initList();
		}else{
			alert(BOOKMARK_ALERT_NOT_COMPLETE);
		}
	});
}

function removeCotrol(){
	$('.control-remove').remove();
}

function setHeightDescription(){
	$(".list-item-momtip").each(function(a,b){
		var this_item = $(b);
		var description = this_item.find(".list-body-title .FXregular");
		description.JJJud({"max_rows":2});
	});
}