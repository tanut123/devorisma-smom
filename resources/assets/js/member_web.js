

$(document).ready(function () {
	// popupMsg("th",VERIFY_TITLE,VERIFY_SUBTITLE);
	facebookRegister();
	emailRegister();
	// $(".condition-detail").click(function () {
	// 	window.open(BASE_LANG+"1000-days");
	// });
});
function popupMsg(lng,msg1,msg2) {
	var p_html = "<div id='panel-popup'>";
			p_html +="<div class='popup-maxwidth'>";
				p_html +="<div class='bg-popup'>";
					p_html +="<h1 class='FXregular'>สมัคร<span class='FLighter'>สมาชิก</span></h1>";
					p_html +="<h2 class='FXMed'>"+msg1+"</h2>";
					p_html +="<h3 class='FXregular'>"+msg2+"</h3>";
			p_html +="</div>";
			p_html +="</div>";
		p_html +="</div>";
	$( "#warpRegisterIndex").append(p_html);
}



function facebookRegister(){
	$("#btn-facebook").click(function(){
		if($("#facebook-accept").is(':checked')){
			$(this).addClass("active");
			$(this).append('<i class="fa fa-spinner fa-spin"></i>');
			memberWebcheckLoginState(function(info){
				callLoginMember(info,"register");
				$("#btn-facebook").removeClass("active");
				$("#btn-facebook .fa").remove();
			});
		}else{
			alert("กรุณายอมรับข้อตกลงก่อนสมัครสมาชิก");
		}
	})
}

function emailRegister(){
	$("#btn-email").click(function(){
		if($("#email-accept").is(':checked')){
			window.location.href = BASE_LANG + "register/form/add";
		}else{
			alert("กรุณายอมรับข้อตกลงก่อนสมัครสมาชิก");
		}
	})
}