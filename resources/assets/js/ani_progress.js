var can_resize = false;
var videoFrist = '';
var videoMain = $('#videoMain');

$(window).resize(function(){
  if(can_resize){
    var el = $('.ev-anim');
    if($('.set-anim')){
      el.removeClass('set-anim');
      el.removeAttr("style");
    }
  }
});

$(document).ready(function(){
	// loadVideo();

	var desktop_timeline = anime.timeline();

    desktop_timeline 
    .add({
    	targets: '.anim-product, .anim-lion',
    	opacity: ['0', '1'],
    	scale: ['0.8', '1'],
    	easing: 'easeOutQuad',
      
    	duration: 600

    })
    .add({
		targets: ['.anim-choline', '.anim-fiber'],
		opacity: ['0', '1'],
		scale: ['0.5', '1.2', '1'],
		translateY: 0,
		easing: 'easeOutQuad',
     
		duration: 600,


    })
    .add({
		targets: '.anim-omega, .anim-lutein',
		opacity: ['0', '1'],
		scale: ['0.5', '1.2', '1'],
		translateY: 0,
		easing: 'easeOutQuad',
      
		duration: 600

    })
    .add({
		targets: '.anim-dha',
		opacity: ['0', '1'],
		scale: ['0.8', '1.2', '1'],
		translateY: 0,
		easing: 'easeOutQuad',

		duration: 500,
		complete: function(anim) {
			can_resize = true;

			anime({
				targets: '.anim-float',
				translateY: ['0', '5', '0'],
				direction: 'normal',
				loop: true,
				easing: 'easeInOutQuad',
				duration: 3000,
				delay: anime.stagger(300, {start: 0})
			});
	
		}

    });
});


// var isMobile = {
// 	Android: function() {
// 		return navigator.userAgent.match(/Android/i);
// 	},
// 	BlackBerry: function() {
// 		return navigator.userAgent.match(/BlackBerry/i);
// 	},
// 	iOS: function() {
// 		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
// 	},
// 	Opera: function() {
// 		return navigator.userAgent.match(/Opera Mini/i);
// 	},
// 	Windows: function() {
// 		return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
// 	},
// 	any: function() {
// 		return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
// 	},
// };


// if(isMobile.any()){
// 	videoFrist = '../video/mobile.mp4';
// }else{
// 	videoFrist = '../video/desktop.mp4';
// }
// function loadVideo(){
// 	console.log(videoMain);
// 	videoMain.html('<source src="'+videoFrist+'" type="video/mp4"></source>' );
// 	document.getElementById("videoMain").play();
// }