var innerWidth = 0;
var innerHeight = 0;
var autoplaySpeedMSec = 2000;
var dataDealy = [];
var bannerSlickDotsIndex = 0;
var bannerSlickDotsCount = 1;
var statusVideoPlaying = false;
var isMobile = false;
var countVideo = 0;
var myPlayer = {}
var videoFullScreen = false;
var nowPlay;
var nowVideo;
var played = false;
var WIN_WIDTH_GLOBAL = 0;
var WIN_HEIGHT_GLOBAL = 0;
var is_firstlayout = true;


$(document).ready(function(){
	// window.addEventListener("message", receiveMessage, false);

	$('#banner-slick, #banner-mom').css({"height":$(window).height()});
	if(navigator.userAgent.match(/(iPod|iPhone)/)){
		$(".btnPlayVideo").css('background',"none");
	}
	isMobile = DetectMobile();
	if(isMobile){
		$("body").addClass("mobile-device");
	}

	$('#banner-slick').slick({
		dots:true,
	  	infinite: true,
	  	slidesToShow: 1,
		slidesToScroll: 1,
		fade: true
	}).on('init', function(slick){
	}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
		if(checkType() == "image" && !played){
			clearAutoPlay();
		}else{
			checkToPauseVideo();
		}
	}).on('afterChange', function(event, slick, currentSlide){
		if(checkType() == "image"){
			clearAutoPlay();
			playAuto();
		}else{
			checkToPlayVideo();
		}
	});
    // if ($("#breastfeeding_popup").attr("show_data") == "1") {
    //     createdim(popupAlert());
    // }
	initSlide();
});

// function receiveMessage(event)
// {
//   if (event.origin !== BASE_URL)
//     return;

// }

function initSlide(){
	is_firstlayout = islandscape();
	$(window).resize(function(){
		if(WIN_WIDTH_GLOBAL == $(this).width()){
			return false;
		}else{
			if(is_firstlayout != islandscape() ){
				window.location.reload();
			}
		}
		loaderImage();
		resizeBannerDisplay();
		resizeImages();
		WIN_WIDTH_GLOBAL = $(this).width();
		WIN_HEIGHT_GLOBAL = $(this).height();
		if(WIN_WIDTH_GLOBAL < 768){
			checkMobile = true;
			// resizePopup();
		}else{
			checkMobile = false;
			// resizePopup();
		}

		if(checkAndriod()){
			$("#banner-mom").removeClass("andriod-lanscape");
			if(islandscape()){
				$("#banner-mom").addClass("andriod-lanscape");
			}
		}
		if(isIpad()){
			$("#banner-mom").removeClass("ipad-lanscape");
			if(islandscape()){
				$("#banner-mom").addClass("ipad-lanscape");
			}
		}
	});
	var winWidth = $(window).width();
	var winHeight = $(window).height();
	WIN_WIDTH_GLOBAL = winWidth;
	WIN_HEIGHT_GLOBAL = winHeight;
	// resizePopup();
	loaderImage();

	resizeBannerDisplay();
	resizeImages();

	var elmActive = getNowElement().find(".slide-banner-list");
	var type = elmActive.data("type");
	if(type == "image" && !played){
		playAuto();
	}else{
		checkToPlayVideo();
	}
	genFontSize();
	$("body").css("background","#fff");
	//$(".global-loading").remove();
	if(checkAndriod()){
		$("#banner-mom").addClass("andriod-device");
		if(islandscape()){
			$("#banner-mom").addClass("andriod-lanscape");
		}
	}
	if(isIpad()){
		$("#banner-mom").addClass("ipad-device");
		if(islandscape()){
			$("#banner-mom").addClass("ipad-lanscape");
		}
	}
}

function loaderImage(){
	var load = "desktop";
	if(WIN_WIDTH_GLOBAL < WIN_HEIGHT_GLOBAL){
		load = "mobile";
	}
	$(".image-loader").each(function(){
		var elm = $(this);
		var image = elm.data("image_desktop");
		var classAdd = "images-list-lanscape";
		var type = elm.data("type");
		if(load == "mobile"){
			image = elm.data("image_mobile");
			classAdd = "images-list-portrait";
		}
		var imgTag = "<img data-src='" + image + "' class='images-list slide-banner-list lazyload " + classAdd + "' data-type='" + type + "' data-expand='-10'>";
		if(elm.nextAll("." + classAdd).length == 0){
			elm.after(imgTag);
		}
	})

	$(".image-loader-video").each(function(){
		var elm = $(this);
		var image = elm.data("image_desktop");
		var classAdd = "images-list-lanscape";
		var type = elm.data("type");
		if(load == "mobile"){
			image = elm.data("image_mobile");
			classAdd = "images-list-portrait";
		}
		var imgTag = "<img data-src='" + image + "' class='mobile-detect visible-xs lazyload " + classAdd + "' data-type='" + type + "' data-expand='-10'>";
		if(elm.nextAll("." + classAdd).length == 0){
			elm.after(imgTag);
		}
	})
}

function checkToPauseVideo(){
	// if(isMobile){
	// 	playAuto();
	// }else{
		if(checkType() == "video"){
			if(!isMobile || media == "tablet"){
				myPlayer[nowPlay][nowVideo].pause();
			}
		}else{
			getNowElement().find(".player_list").tubular('pause');
		}
	// }
}

function playPauseVideo(){
	var elm = getNowElement().find(".slide-banner-list");
	var video_id = "#" + elm.attr("id");
	// if(elm.parent().hasClass("playing")){
	// 	myPlayer["video"][video_id].pause();
	// 	played = true;
	// 	// elm.parent().removeClass("playing");
	// 	// clearAutoPlay();
	// 	// playAuto();
	// }else{
		elm.siblings(".mobile-detect").addClass("active");

		myPlayer["video"][video_id].play();
		played = false;
		elm.parent().addClass("playing");
		getNowElement().find("play-control").remove();
	// }
}

function checkToPlayVideo(){
	loadVideo(checkType(), getNowElement().find(".slide-banner-list"));
	if(checkType() == "video"){
		if(!isMobile){
			myPlayer[nowPlay][nowVideo].play();
			clearAutoPlay();
		}else{
			clearAutoPlay();
			playAuto();
		}
	}else if(checkType() == "youtube"){
		if(!isMobile){
			getNowElement().find(".player_list").tubular('play');
			clearAutoPlay();
		}else{
			clearAutoPlay();
			playAuto();
		}
	}
}

function moreContent(){
	return ($("#banner-mom").find(".banner-list").length > 1)? true: false;
}
function getNowElement(){
	return $("#banner-mom").find(".slick-active");
}
function checkType(){
	return getNowElement().find(".slide-banner-list").data("type");
}
function getDelay(){
	return (getNowElement().data("delay") == 0)? 1 : getNowElement().data("delay");
}

function slickNext(){
	$('#banner-slick').slick('slickNext');
}

var interval;
function playAuto(){
	var timeIn = getDelay();
	if(timeIn == 0){
		timeIn = 1;
	}
	interval = setInterval(function(){
		if(!played || checkType() == "image"){
			slickNext();
		}
	}, timeIn*1000);
}

function clearAutoPlay(){
	if(interval){
		clearInterval(interval);
	}
}

var timeoutDelay;
function clearTimeOutDelay(){ if (timeoutDelay) clearTimeout(timeoutDelay); }
function setTimeDelay(time){ if (timeoutDelay) clearTimeout(timeoutDelay); timeoutDelay = setTimeout(function(){ cycleNext(); },time); }

function destroyPlayer(ended){
    if (!moreContent() && !isMobile) return;
    if (myPlayer[nowPlay] && played) {
        if (nowPlay == "video") {
            if(typeof(myPlayer['video'][nowVideo].pause) == "function") {
                if(typeof(ended) == "undefined") myPlayer['video'][nowVideo].pause();
                // $('#slideshow .video_content').empty();
            }
        } else if (nowPlay == "youtube") {
            myPlayer['youtube'].destroy();
        }
        // myPlayer[nowPlay] = null;
        $('#slideshow .cover_video_content').show();
        if(!isMobile) $('#slideshow .video_content').css('opacity',1);
        else $('#slideshow .video_content').css('opacity',0);
    }
    played = false;
}

function loadVideo(type, elm) {
	var videoID = elm.attr("id");
    if (typeof(videoID) == "undefined") {
    	if (typeof(fadePlayBtn) != "undefined") { fadePlayBtn('fadeOut'); }
    	return;
     }
    if (videoID.indexOf('#') == -1) videoID = '#'+videoID;
    nowPlay = type;
    if (type == "video") {
        nowVideo = videoID;
        if ($(videoID).find('.big-video-wrap').size()>0) {
            if (!isMobile) {
                myPlayer[nowPlay][nowVideo].play();
            }
            return;
        }
	   	var source = elm.data("source");
	   	var banner_id = "#" + elm.parent().attr("id");
	    var BV = new $.BigVideo({ container : $(videoID), doLoop : (!moreContent() && !isMobile)   });
	    BV.init();
	    BV.show(source);
        // BV.show("http://video-js.zencoder.com/oceans-clip.mp4");
        if (typeof(myPlayer[type]) != "object") myPlayer[type] = {};
        myPlayer[type][videoID] = BV.getPlayer();
        countVideo++;
        myPlayer[type][videoID].ready(function(){
            myPlayer[type][videoID].on("play", function(){
                clearAutoPlay();
                getNowElement().find(".link-video").addClass("link-video-active");
                played = true;
                // mute sound
                this.volume(0);
            });
             myPlayer[type][videoID].on("pause", function(){
             	getNowElement().find(".link-video").removeClass("link-video-active");
             	if(isMobile){
	                played = false;
	                clearAutoPlay();
	                playAuto();
	                $(".mobile-detect.active").removeClass("active");
	                $(".playing").removeClass("playing");
             	}
            });

            if (!isMobile) {
                myPlayer[type][videoID].play();
            } else {
            }
            myPlayer[type][videoID].on("ended", function(){
            	getNowElement().find(".link-video").removeClass("link-video-active");
                destroyPlayer(0);
                slickNext();
            });

            // if (typeof(scrollingToStop) == "function") scrollingToStop();
        });
    } else if (type == "youtube") {
    	// var dataVideo = $(videoID).data("source");
        var code = $(videoID).data("source");

        var youtube_id = $(videoID).prev(".player_list").attr("id");
		var options = { videoId: code };
		if(getNowElement().find(".tubular-panel").length > 0){
			return ;
		}
		$(".banner-list").find(".player_list").each(function(){
			var id = $(this).attr("id");
			var codeID = $(this).data("source");
			var options_yt = {
							videoId: codeID,
							mute: true,
			 				onStateChangeApi: function(state){
			 					// end video
			 					if (state.data == 0) {
			 						getNowElement().find(".link-video").removeClass("link-video-active");
			                        slickNext();
			                        played = false;
			                    }
			                    // play video
			                    if (state.data == 1) {
			                    	if(isMobile || media == "tablet"){
			                    		clearAutoPlay();
			                    	}
			                    	getNowElement().find(".link-video").addClass("link-video-active");
			                        played = true;
			                        if($("#banner-mom").hasClass("andriod-device") || $("#banner-mom").hasClass("ipad-device")){
			                        	getNowElement().find("iframe").addClass("active");
			                        }
			                        // removeCoverPhoto(videoID);
			                        // $(videoID).parents('.wrap_slide_content').addClass('playing');
			                    }
			                     if (state.data == 2) {
			                     	getNowElement().find(".link-video").removeClass("link-video-active");
			                     	if(isMobile){
				                        played = false;
				                        clearAutoPlay();
				                        playAuto();
			                     	}
			                    }
			 				}
			 			};
			if(getNowElement().find(".player_list").attr("id") == id && !isMobile){
				 options_yt["onReadyApi"] = function(e){
									if(!isMobile){
										e.target.playVideo();
									}
								};
			}
			if(!moreContent()){
				options_yt["repeat"] = true;
			}
        	$('#' + id).tubular(options_yt);
		})
    }
}
function DetectMobile(){
	var status = false;
	if(navigator.userAgent.match(/iPad/i)){
	 	status = true;
	}

	if(navigator.userAgent.match(/(iPod|iPhone)/)){
	 	status = true;
	}

	if(navigator.userAgent.match(/Android/i)){
	 	status = true;
	}

	if(navigator.userAgent.match(/BlackBerry/i)){
	 	status = true;
	}

	if(navigator.userAgent.match(/webOS/i)){
	 	status = true;
	}

	return status;
}

function isIpad(){
	var status = false;
	if(navigator.userAgent.match(/iPad/i)){
	 	status = true;
	}
	return status;
}

function resizeBannerDisplay(){
	var browser_type = navigator.userAgent;

	innerWidth = $(window).width();
	innerHeight = $(window).height();

	$("#banner-mom,#banner-slick").css({
		"height":innerHeight
	});
}

function resizeImages() {
	var me = $(window);
    var wh = me.height();
    var ww = me.width();
    if(ww < 768){
    	var popupHeight = $(".event_penel").height();
    	wh += 40;
    	// fh += popupHeight;
    }


    var ratio = (islandscape()) ? (1280/750):(640/1095);
    var imgw = wh * ratio;
    var fh;
    var fw;
    if (imgw <= ww) {
        fh = ww / ratio;
        fw = ww;
    } else {
        fh = wh;
        fw = imgw;
    }


    if(islandscape()){
    	$(".images-list-lanscape").show();
    	$(".images-list-portrait").hide();
    	if(isMobile){
    		$(".images-list-lanscape").addClass("show-slide-image").removeClass("hide-slide-image");
    		$(".images-list-portrait").addClass("hide-slide-image").removeClass("show-slide-image");
    	}
    }else{
    	$(".images-list-lanscape").hide();
    	$(".images-list-portrait").show();
    	if(isMobile){
    		$(".images-list-lanscape").addClass("hide-slide-image").removeClass("show-slide-image");
    		$(".images-list-portrait").addClass("show-slide-image").removeClass("hide-slide-image");
    	}
    }

    // alert(fw+"__"+fh);
   	$(".images-list-lanscape").css({"height":fh,"width":fw});
    $(".images-list-portrait").css({"height":fh,"width":fw});

    $('#banner-slick, #banner-mom').css({"height":wh});

    if (fw >= ww) {
        $('#banner-mom .images-list,#banner-mom .mobile-detect').css('margin-left', - ((fw - ww) /2 ));
        $('#banner-mom .images-list,#banner-mom .mobile-detect').css('margin-top', -(fh - wh) / 2);
    }


}

function getCurrentVideo(){
	var video_id =  $("#banner-mom .slick-active").find("video").attr("id");
	return (typeof video_id !== "undefined")? video_id : "";
}
function resizeVideo() {
	var me = $('#banner-mom');
    var wh = me.height();
    var ww = me.width();
    if(ww < 768){
    	var popupHeight = $(".event_penel").height();
    	wh += 40;
    	// fh += popupHeight;
    }

    if(getCurrentVideo() != ""){

	    var vid = document.getElementById(getCurrentVideo());

	    var ratio = vid.videoWidth/vid.videoHeight;
	    var imgw = wh * ratio;
	    var fh;
	    var fw;
	    if (imgw <= ww) {
	        fh = ww / ratio;
	        fw = ww;
	    } else {
	        fh = wh;
	        fw = imgw;
	    }



	    $(".video-list").css({"height":fh,"width":fw});

	    $('#banner-slick, #banner-mom').css({"height":wh});


	    if (fw >= ww) {
	        $(".video-list").css('margin-left', - ((fw - ww) /2 )).css('margin-top', -(fh - wh) / 2);
	    }
	}


}

function genFontSize(){
	var maxFont = "55px";
	if(isMobile){
		maxFont = "20px";

    }
	$('.display-title-text').fitText(1.4, { minFontSize: '26px', maxFontSize: maxFont });
}

function islandscape(){
	innerWidth = $(window).width();
	innerHeight = $(window).height();
	return (innerHeight > innerWidth) ? false:true;
}

function getEmbedCode(embed) {
	var	code = embed.split("https://www.youtube.com/embed/");
	if(typeof code[1] !== "undefined"){
		var yCode = code[1].split("\"");
		if(typeof yCode[0] !== "undefined"){
			return yCode[0];
		}else{
			return "";
		}
	}
	return "";
}

// function popupAlert(){
// 	var html = "<div id='popup-panel' class='bg-popup-alert'>";
// 			html +="<div id='popup-inner-panel'>";
//                 html +="<div class='popup-inner-panel-bg'></div>";
// 			// html +="<img src='"+BASE_URL+"/images/member/mom-with-baby.png' class='logo-mom visible-xs'>";
// 				html +="<div id='popup-top-panel'>";
// 					html +="<div id='popup-inner-top-panel'>";
// 						html +="<div id='close-popup'>";
// 						html +="</div>";
// 							if(LANG == "th"){
// 								html +="<div id='text-popup-left'>";
// 								html +="<span id='text-popup1' class='color-gold'>Breastfeeding</span>";
// 								html +="<span id='text-popup2' class='color-gold'>is the Best</span>";
// 								html +="<span id='text-popup3' class='color-gold FLighter'>นมแม่ดีที่สุด</span>";
// 								html +="<span id='text-popup4' class='color-gray FLighter'>มีคุณค่าทางโภชนาการสูงสุด และยังมีภูมิคุ้มกันโรคต่างๆ องค์การอนามัยโลก หรือ WHO แนะนำให้ทารกทานนมแม่เพียงอย่างเดียว 6 เดือน และทานนมแม่ต่อเนื่องไปควบคู่กับอาหารที่เหมาะสมตามวัยจนอายุ 2 ปี หรือมากกว่า S-MomClub ขอร่วมสนับสนุนและส่งเสริมการเลี้ยงลูกด้วยนมแม่</span>";
// 								html +="</div>";
// 							}else{
// 								html +="<div id='text-popup-left' class='popup-en'>";
// 								html +="<span id='text-popup1' class='color-gold'>Breastfeeding is the Best</span>";
// 								html +="<br><span id='text-popup4' class='color-gray FLighter'>WHO recommends mothers worldwide to exclusively breastfeed infants for the child's first six months to achieve optimal growth, development and health. Thereafter, they should be given nutritious complementary foods and continue breastfeeding up to the age of two years or beyond. S-MomClub strictly follows such statement and fully support breastfeeding policy</span>";
// 								html +="</div>";
// 							}
// 						html +="<div id='text-popup-right'>";
// 							html +="<div class='mom-img'>";
// 								// html +="<img src='"+BASE_URL+"/images/home/img-mom.png' class='img-responsive mom-img'></img>";
// 							html +="</div>";
// 						html +="</div>";

// 					html +="</div>";
// 				html +="</div>";
// 			html +="</div>";
// 		html +="</div>";

// 	return html;
// }

// function createdim(message){
// 	$("body").append("<div id='dim-panel'>" + message + "<div id='dim-inner-panel'></div></div>").addClass("nav-active");
// 	dimClick();
// 	closePopup();
// }
// function closePopup(){
// 	if(typeof($("#close-popup").length) != "undefined"){
// 		$("#close-popup").click(function(){
// 			removeDim();
//             var pathName = window.location.pathname;
//             setMemberBreastfeedingPanel("1",pathName);
// 		})
// 	}
// }

// function removeDim(){
// 	$("#dim-panel").remove();
// 	$("body").removeClass("nav-active");
// }
// function dimClick(){
// 	$("#dim-inner-panel").click(function(){
// 		removeDim();
// 	})
// }
// function resizePopup(){
// 	var heightPopup = 0;
// 	// if(WIN_WIDTH_GLOBAL > WIN_HEIGHT_GLOBAL){
// 	// 	heightPopup = WIN_HEIGHT_GLOBAL - 200;
// 	// }else{
// 		heightPopup = WIN_HEIGHT_GLOBAL - 50;
// 	// }
// 	$(".bg-popup-alert").find("#popup-inner-top-panel").height(heightPopup);
// }

function checkAndriod(){
	var ua = navigator.userAgent.toLowerCase();
	return ua.indexOf("android") > -1; //&& ua.indexOf("mobile");
}
