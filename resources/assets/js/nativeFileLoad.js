var tempJavaScriptPreload;
var tempCssPreload;
var tempAsyncJS;
var tempCallbackFunction;
var tempCountCss = 0;
var onloadCount = 0;
var tempCssLoadStatus = [];

function loadCss(option) {
    if (typeof(tempCssPreload) != "undefined") {
        var loadCssFunction = function() {
        	if( ++onloadCount !=2 ) return;
            var linkTag = document.createElement('link');
            linkTag.rel = 'stylesheet';
            var instantLink = [];
            var headerTag = document.getElementsByTagName('head')[0];
            var sheet , cssRules ;
            for (var i in tempCssPreload) {
                if (tempCssPreload[i] != "") {
                    instantLink[i] = linkTag.cloneNode();
                    instantLink[i].href = tempCssPreload[i];
                    if (typeof(sheet) == "undefined" || typeof(sheet) == "cssRules") {
                        if ('sheet' in instantLink[i]) {
                            sheet = 'sheet'; cssRules = 'cssRules';
                        }
                        else {
                            sheet = 'styleSheet'; cssRules = 'rules';
                        }
                    }
                    headerTag.appendChild(instantLink[i]);
                    tempCssLoadStatus[i] = false;
                }
            }
            var tempCssLoadTime = setInterval( function() {
                try {
                    for (var i in instantLink) {
                        if (instantLink[i][sheet] && instantLink[i][sheet][cssRules].length ) {
                            tempCssLoadStatus[i] = true;
                        }
                    }
                    var checkStatus = true;
                    for (var j in tempCssLoadStatus) {
                        if (tempCssLoadStatus[i] == false) {
                            checkStatus = false;
                        }
                    }
                    if (checkStatus == true) {
                        clearInterval(tempCssLoadTime);
                        clearTimeout(timeout_preload);
                        loadJS();
                    }

                } catch( e ) {} finally {}
            }, 50);
            timeout_preload = setTimeout( function() {
                clearInterval( tempCssLoadTime);
                clearTimeout( timeout_preload );
                for (var j in instantLink) {
                    headerTag.removeChild(instantLink[j]);
                }
                loadJS();
            }, 15000);
        };

        var raf;
        if (typeof(requestAnimationFrame) != "undefined") {
            raf = requestAnimationFrame;
        } else if (typeof(mozRequestAnimationFrame) != "undefined") {
            raf = mozRequestAnimationFrame;
        } else if (typeof(webkitRequestAnimationFrame) != "undefined") {
            raf = webkitRequestAnimationFrame;
        } else if (typeof(msRequestAnimationFrame) != "undefined") {
            raf = msRequestAnimationFrame;
        }
        if (raf) {
            raf(function(){
                loadCssFunction();
            });
        }else{
            onloadCount++;
        }

        if (typeof(window.onload) == "function") {
            var oldFunc = window.onload;
            window.onload = function() {
                oldFunc();
                loadCssFunction();
            };
        } else {
            window.onload = function() {
                loadCssFunction();
            };
        }

    } else {
        loadJS();
    }
}
function initFileload(cssFile,jsFile,option) {
    var TempCssFile = cssFile;
    if (typeof(option) != "undefined") {
        if (typeof(option.callback) != "undefined") {
            tempCallbackFunction = option.callback;
        }
    }
    tempCssPreload = cssFile;
    tempJavaScriptPreload = jsFile;
    var bodyElement = document.getElementsByTagName("body");
    if (bodyElement.length > 0) {
        loadCss(option);
    }
}
function loadJS(next) {
    if (typeof(tempJavaScriptPreload) != "undefined" && tempJavaScriptPreload.length > 0) {
        if (next == null) {
            next = 0;
        }
        if (typeof(tempJavaScriptPreload[next]) != "undefined") {
            if (tempJavaScriptPreload[next] != "") {
                var tag = document.createElement("script");
                tag.type = "text/javascript";
                tag.src = tempJavaScriptPreload[next];
                if (typeof(tempJavaScriptPreload[next+1]) != "undefined") {
                    tag.onload = function() {
                        loadJS(next+1);
                    };
                } else {
                    tag.onload = function() {
                        loadAsyncJS();
                    };
                }
                document.body.appendChild(tag);
            } else {
                if (typeof(tempJavaScriptPreload[next+1]) != "undefined") {
                    loadJS(next+1);
                } else {
                    loadAsyncJS();
                }
            }
        }
    } else {
        loadAsyncJS();
    }
}
function initLoadAsyncJS(ext_js_array) {
    if (typeof(tempAsyncJS) == "undefined") {
        tempAsyncJS = ext_js_array;
    } else {
        for (var i in ext_js_array) {
            tempAsyncJS.push(ext_js_array.shift());
        }
    }

}
function loadAsyncJS(next) {
    if (typeof(tempAsyncJS) != "undefined") {
        if (next == null) {
            next = 0;
        }
        if (tempAsyncJS[next] != "") {
            var tag = document.createElement("script");
            tag.setAttribute("defer","defer");
            tag.type = "text/javascript";
            tag.src = tempAsyncJS[next];
            if (typeof(tempAsyncJS[next+1]) != "undefined") {
                loadAsyncJS(next+1);
            } else {
                runCallBack();
            }
            document.body.appendChild(tag);
        } else {
            if (typeof(tempAsyncJS[next+1]) != "undefined") {
                loadAsyncJS(next+1);
            } else {
                runCallBack();
            }
        }
    } else {
        runCallBack();
    }
}
function runCallBack() {
    if (typeof(tempCallbackFunction) != "undefined") {
        tempCallbackFunction();
    }
}