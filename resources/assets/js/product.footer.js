$(document).ready(function () {
	$(".product_footer_slick").slick({
		dots: true,
	  	infinite: false,
	  	slidesToShow: 3,
		slidesToScroll: 1,
		variableWidth: false,
		responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: false,
		        dots: false,
		        variableWidth: false
		      }
		    }
		 ]
	});
});