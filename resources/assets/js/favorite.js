$(document).ready(function(){
	callListFavorite();
});

function callListFavorite(){
	if($(".list-item-momtip").length > 0 && $("#signout_btn").length > 0){
		$.ajax({
			url: BASE_LANG + 'favorite',
			type: 'GET',
			data: null,
			dataType: 'json'
		})
		.success(function(d) {
			var favorite = d["favorite"];
			createFavoriteIcon(favorite);
		})
	}
}

function createFavoriteIcon(favorite){
	$(".list-item-momtip").each(function(a){
		var nowElm = $(this);
		var id = nowElm.data("id").toString();
		if($.inArray( id,favorite ) > -1){
			if(nowElm.find(".list-body-img-panel .favorite-icon,.list-body-img-panel_cover .favorite-icon, .relate-item-image .favorite-icon").length <= 0){
				nowElm.find(".list-body-img-panel, .list-body-img-panel_cover, .relate-item-image").append(itemFavorite());
				nowElm.find(".list-body-img-panel .favorite-icon,.list-body-img-panel_cover .favorite-icon, .relate-item-image .favorite-icon").css({
					"position": "absolute",
					"left" : "3px",
					"bottom" : "3px",
					"width": "30px",
    				"height": "30px"
				})
			}
		}

	})
}

function itemFavorite(){
	var html = "";
	html = "<span class='favorite-icon'><img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAACz0lEQVRIS8WXW0gUYRTHvzM7M+pe1FWhTUMxb5l52WAVerBQgnww6sWXIAqKbgS9FBFFRhFdXoLoRkER9NJLkg8GkZUPgS60XjJbL4mSZrCr5rqrOzP7nfg0ZVlsZ2bb2nka5vzP+Z3v++Yczgck7EFEUBSljlJax3HcZkRMD7frfQeAWUrpZ47j2nmebwcAXIkBKy+IuF6W5WZCSLFegEb9oCAIzQDwnemXwAyqKMrdv12hWgJsB3ieP87gwLZXluV7hJASNcc42d2CIBxj0HpEPB+noJrCAMAVCAaD1wGgWpNHnESI2MVW3IKIaXGKqSkMAHhBkqS3mtTxFdFEgYkq2O9xG8c/3CjwTfVYCYYg2Zrvy7YfGMsq2u31DL3MnHQ9yVucGbUQMKDFVjmTu+3MiCmrJKC2QVHBAY/b2N+yfytVFvnIQOZ1ldPzP3oyIr9zfLJStufpR6MKPCp4oPXQlrkJZ5Za9pH21ByHp7Tx0adoflHBzoeOWhqSOL1gziBSx2FnR8zgzvtV2wnB1X6uPQHAmqPd72MGdz9rqA76Jo3agcvKJEt2oGpfW1fM4G+dt3InXI836gXn2A9+3VBzajxmcEgKcL3P9zqk+akUrXDRbFuoaHrhNIhGGjOYOfqmXOYvrUfsNBQ0qME5Q1JoU+MDl8Vmn1fTqjYQFsA7/CpjpP1cOdLQH3804AxYUHe1L7Nw17QalNk1gZmQdanRd5fL1iovVj75Oy70s26mBaoLzMSzYx1pQ69Pl4d3Mtapinbe7EvPq/2pFaobvHzm3ebBtpMVSnBO5JNSpeKG270WW5XqmUYmpXmrwx0D3sGU4TdnSwvrrw0YM4sX9Kz0t5YmbhBI5OiTmGEvYeMtO2y/358tiuKd/zHQS5J0wmQyTa52IgYXBOHiv7zCyLJ8iUGX6ji8FCIubSWIaCWE6B4EVksGYIZS6l7r0vYLyJVtgNLdwfQAAAAASUVORK5CYII=' alt='icon-favorite'></span>";
	return html;
}