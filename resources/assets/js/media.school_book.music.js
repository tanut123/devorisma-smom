var mouseStartX;
var mouseStartY;
var controlDrag = false;
var controlDragVolum = false;
var beginDrag = false;
var tempControlPercent = 0;
var tempVolumPercent = 100;
var musicPlayInterval = "";
var playStatus = "";
var music_control_mode = "";
var download_hover = false;
var tempWindowWidth;
var tempReadyState = 0;
var tempLoadingDot = 0;
var tempLoadingState = false;
$(window).resize(function(){
    if (temp_media_school_book_mode == "music") {
        adjust_sound_control_size();
        crvVolumAnime(tempVolumPercent);
    }
});
function adjust_sound_control_size() {
    var w_width = $(window).width();
    var w_height = $(window).height();
    if (w_width < 768) {
        music_control_mode = "mobile";
        var vol_control = w_width - 40;
        $("#volumControlPanel").attr("width",vol_control);
    } else {
        music_control_mode = "desktop";
        $("#volumControlPanel").attr("width",216);
    }
    if (tempWindowWidth > 767 && $(window).width() <= 767) {
        tempWindowWidth = $(window).width()
        $(".music_list_panel").perfectScrollbar('update');
    }
    if (tempWindowWidth < 768 && $(window).width() >= 768) {
        tempWindowWidth = $(window).width()
        $(".music_list_panel").perfectScrollbar('update');
    }

}
function init_sound_control() {
    tempWindowWidth = $(window).width();
    tempReadyState = 4;
    tempLoadingDot = 0;
    tempLoadingState = false;
    adjust_sound_control_size();
    function stopDefAction(evt) {
        evt.preventDefault();
    }
/*    $(".music_list_panel").mCustomScrollbar({
        theme:"dark"
    });*/

    $(".music_list_panel").perfectScrollbar({wheelPropagation:false});



    document.getElementById('soundControlPanel').addEventListener('click', stopDefAction, false);
    $(".music_list_row").click(function() {
        if (download_hover == false) {
            var music_player = document.getElementById("media_music_player");
            music_player.load();
            tempLoadingDot = 0;
            tempReadyState = 0;
            tempLoadingState = true;
            music_player.src = $(this).attr("mp3_value");
            //console.log(2,tempReadyState);
            /*var music_source = document.getElementById("music_source");
             music_source.src = $(this).attr("mp3_value");*/
            $(".play_active").removeClass("play_active");
            $(this).addClass("play_active");
            tempControlPercent = 0;
            tempVolumPercent = music_player.volume*100
            crvAnime(tempControlPercent);
            crvVolumAnime(tempVolumPercent);

            music_player.play();
        }
    });
    $(".download").mouseover(function() {
        download_hover = true;
    });
    $(".download").mouseout(function() {
        download_hover = false;
    });

    $('#soundControlPanel').bind('touchstart mousedown', function(e){
        setStartMove(e);
    });
    $('#soundControlPanel').bind('touchmove mousemove', function(e){
        e.preventDefault()
        moveClient(e);
    });
    $('#soundControlPanel').bind('touchend mouseup', function(e){
        setStopMove();
    });
    $('#soundControlPanel').click(function() {
        var music_player = document.getElementById("media_music_player");
        if (music_player.paused == true) {
            music_player.play();
        } else {
            music_player.pause();
        }
    });
    $('#volumControlPanel').bind('touchstart mousedown', function(e){
        setStartMoveVolum(e);
    });
    $('#volumControlPanel').bind('touchmove mousemove', function(e){
        e.preventDefault()
        moveClientVolum(e);
    });
    $('#volumControlPanel').bind('touchend mouseup', function(e){
        setStopMoveVolum();
    });


    crvAnime(tempControlPercent);
    crvVolumAnime(100);

    showTime();
}
function setStartMoveVolum(event) {
    mouseStartX = pointerEventToXY(event).x;
    mouseStartY = pointerEventToXY(event).y;
    controlDragVolum = true;
}

function moveClientVolum(event) {
    var music_player = document.getElementById("media_music_player");
    event.preventDefault();
    if (controlDragVolum == true && music_player.src != "") {
        var calcurrent = pointerEventToXY(event).x - mouseStartX;
        calcurrent = calcurrent * 0.5;
        tempVolumPercent += calcurrent;
        if (tempVolumPercent < 0) {
            tempVolumPercent = 0;
        } else if (tempVolumPercent > 100) {
            tempVolumPercent = 100;
        }
        mouseStartX = pointerEventToXY(event).x;
        mouseStartY = pointerEventToXY(event).y;
        crvVolumAnime(tempVolumPercent);
        music_player.volume = tempVolumPercent/100;
    }
}
function setStopMoveVolum() {
    controlDragVolum = false;
}
function showTime() {
    //console.log(11,musicPlayInterval);
    if (musicPlayInterval == "" || musicPlayInterval > 3) {
        //console.log(12);
        musicPlayInterval = setInterval(function() {
            if (temp_media_school_book_mode == "music") {
                var music_player = document.getElementById("media_music_player");
                if (music_player.duration && music_player.src != "") {
                    var currentTime = music_player.currentTime;
                    var minusTime = (music_player.duration - currentTime);
                    tempControlPercent = currentTime / music_player.duration * 100;
                    $(".current").html(convertMinute(currentTime));
                    var showMinus = "-"+convertMinute(minusTime);
                    $(".minus_time").html(showMinus);
                    crvAnime(tempControlPercent);
                    //console.log(1,tempReadyState);
                    if (tempLoadingState == true && music_player.readyState == 4) {
                        tempReadyState = music_player.readyState;
                        tempLoadingState = false;
                        //console.log(1);
                    }
                }
                if (music_player.paused == true) {
                    playStatus = "stop";
                } else {
                    playStatus = "play";
                }
            } else {
                clearInterval(musicPlayInterval);
                musicPlayInterval = "";
            }
        },200);
    }
}
function setStopMove() {
    controlDrag = false;
    if (beginDrag == true) {
        setTimeout(function(){
            var music_player = document.getElementById("media_music_player");
            if (music_player.src != "") {
                beginDrag = false;
                music_player.play();
            }
        },100);
    }
}
function setStartMove(event) {
    mouseStartX = pointerEventToXY(event).x;
    mouseStartY = pointerEventToXY(event).y;
    controlDrag = true;
}
function moveClient(event) {
    event.preventDefault();
    var music_player = document.getElementById("media_music_player");
    if (controlDrag == true && music_player.src != "") {
        if (beginDrag == false) {
            var diffX = pluseValue(mouseStartX - pointerEventToXY(event).x);
            var diffY = pluseValue(mouseStartY - pointerEventToXY(event).y);
        }
        if (diffX > 10 || diffY > 10 || beginDrag == true) {

            beginDrag = true;
            var calcurrent = mouseStartY - pointerEventToXY(event).y;
            calcurrent = calcurrent * 0.5;
            tempControlPercent += calcurrent;
            if (tempControlPercent < 0) {
                tempControlPercent = 0;
            } else if (tempControlPercent > 100) {
                tempControlPercent = 99.9;
            }
            mouseStartX = pointerEventToXY(event).x;
            mouseStartY = pointerEventToXY(event).y;
            crvAnime(tempControlPercent);
            var duration = music_player.duration;
            playTime = duration * tempControlPercent/100;
            music_player.currentTime = playTime;
            music_player.play();
        }
    }
}
function pluseValue(value) {
    if (value < 0) {
        return value * -1;
    } else {
        return value;
    }
}
function pointerEventToXY(e) {
    var out = {x:0, y:0};
    if(e.type == 'touchstart' || e.type == 'touchmove' || e.type == 'touchend' || e.type == 'touchcancel'){
        var touch = e.originalEvent.touches[0] || e.originalEvent.changedTouches[0];
        out.x = touch.pageX;
        out.y = touch.pageY;
    } else if (e.type == 'mousedown' || e.type == 'mouseup' || e.type == 'mousemove' || e.type == 'mouseover'|| e.type=='mouseout' || e.type=='mouseenter' || e.type=='mouseleave') {
        out.x = e.pageX;
        out.y = e.pageY;
    }
    return out;
}
function crvVolumAnime(percent) {
    var c = document.getElementById("volumControlPanel");
    var ctx = c.getContext("2d");
    var lineWidth = (c.width - 20);
    var middlePoint = c.height / 2;
    var linesize;
    if (music_control_mode == "mobile") {
        linesize = 7;
    } else {
        linesize = 3
    }
    var radius;
    if (music_control_mode == "mobile") {
        radius = 10;
    } else {
        radius = 5;
    }
    var linePercent = percent / 100 * (lineWidth-radius);
    var leftPoiont = 10 + (radius/2);
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.beginPath();
    ctx.lineWidth = linesize;
    ctx.moveTo(leftPoiont,middlePoint);
    ctx.strokeStyle = "rgba(255,255,255,0.2)";
    ctx.lineTo(leftPoiont+lineWidth-radius,middlePoint);
    ctx.stroke();

    ctx.beginPath();
    ctx.lineWidth = linesize;
    ctx.moveTo(leftPoiont,middlePoint);
    ctx.strokeStyle = "#f3c85b";
    ctx.lineTo(leftPoiont+linePercent,middlePoint);
    ctx.stroke();

    ctx.beginPath();
    ctx.shadowColor = 'rgba(0, 0, 0, 0.5)';
    ctx.shadowBlur = 1;
    ctx.shadowOffsetX = 1;
    ctx.shadowOffsetY = 1;
    ctx.arc(leftPoiont+linePercent,middlePoint, radius, 0, 2 * Math.PI);
    ctx.fillStyle = '#f3c85b';
    ctx.fill();

}
function crvAnime(percent) {
    if($("#soundControlPanel").size() == 0) return;

    var c = document.getElementById("soundControlPanel");
    var ctx = c.getContext("2d");
    var percent_cal = ((percent / 100 * 2)-0.5);
    var loadingText = "";
    for (var i = 0; i <= tempLoadingDot; i++) {
        loadingText =  loadingText+".";
    }
    if (tempLoadingDot == 3) {
        tempLoadingDot = 0;
    } else {
        tempLoadingDot++;
    }
    ctx.clearRect(0, 0, c.width, c.height);
    ctx.beginPath();
    ctx.lineWidth = 7;
    ctx.strokeStyle = "rgba(255,255,255,0.2)";
    ctx.arc(108, 108, 92, 0, 2 * Math.PI);
    ctx.stroke()
    ctx.beginPath();
    ctx.shadowColor = 'rgba(0, 0, 0, 0.5)';
    ctx.shadowBlur = 3;
    ctx.shadowOffsetX = 1;
    ctx.shadowOffsetY = 1;
    ctx.lineWidth = 7;
    ctx.strokeStyle = "#f3c85b";
    ctx.arc(108, 108, 92, 4.71, percent_cal * Math.PI);
    ctx.stroke();
    //console.log(5,tempReadyState);
    if (tempReadyState != 4) {
        //.log(3,tempReadyState);
        ctx.font="30px db_helvethaica_x";
        ctx.textAlign="center";
        ctx.fillStyle = "rgba(255,255,255,0.5)";
        ctx.fillText("Loading",108,115);
        ctx.fillText(loadingText,108,125);
    } else {
        if (playStatus != "play") {
            ctx.beginPath();
            ctx.moveTo(90,108);
            ctx.lineTo(130,108);
            ctx.lineTo(90,80);
            ctx.lineTo(90,108);
            ctx.fillStyle = '#f3c85b';
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.lineTo(90,135);
            ctx.lineTo(130,108);
            ctx.lineTo(90,108);
            ctx.fillStyle = '#d0b15b';
            ctx.fill();
            ctx.closePath();

        } else {
            ctx.beginPath();
            ctx.rect(90,90,13,22);
            ctx.fillStyle = '#f3c85b';
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.rect(114,90,13,22);
            ctx.fillStyle = '#f3c85b';
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.rect(90,112,13,22);
            ctx.fillStyle = '#d0b15b';
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.rect(114,112,13,22);
            ctx.fillStyle = '#d0b15b';
            ctx.fill();
            ctx.closePath();
        }
    }
}
function convertMinute(secInput) {
    var sec_int = Math.round(secInput);
    var sec = (sec_int % 60);
    if (sec < 10) {
        sec = '0'+ sec.toString();
    }
    var minute = (sec_int - sec) / 60;
    return minute+":"+sec;
}
