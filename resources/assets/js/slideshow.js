
var startingSlide = 0;
var loadOnClick;
var imgTransition;
var idPage;
var idLan;
var videoClone = {};
var myRatio;
var checkSmartPhone = _mobile.smartphone();
var checkIpad = _mobile.ipad();
var countVideo = 1;
var playerDetailState = 0,noPlay = true,playerActive;
var videoFullScreen = false;
var noHome;
$(document).ready(function () {
    // alert(navigator.userAgent);
    loadOnClick = $('#slidecanvas').attr('data-loadonclick');
    imgTransition = $('#slidecanvas').attr('data-imgTransition');
    idPage = $('#slidecanvas').attr('data-idPage');
    idLan = $('#slidecanvas').attr('data-idLan');

    resizeTimeout2 = setTimeout(function() {
        setLayoutBanner();
        startCycle();
        loadSlideshow();
    },40);
    if (checkSmartPhone) $('#main_home').addClass('smartphone');
    else $('#main_home').addClass('nosmartphone');
    if (checkIpad || (checkSmartPhone && $(window).width()>768)) $('#main_home').addClass('ipad');
    var resizeTimeout;
    detectIpadOrientation(100);
    $(window).resize(function () {
        detectIpadOrientation(1);
        setLayoutBanner();
        resizeImages();
    });

    if ($('#slideshow .wrap_slide_content:first-child .cycle_type').val() != "image") fadePlayBtn('fadeIn');



    $('.play_box').bind('touchstart',function(){
        playNow();
    });

    $('#slideshow .video_content').each(function(i,v){
        var e = $(v);
        e.parents('.wrap_slide_content').attr('id','wrap_'+e.attr('id'));
        videoClone[e.attr('id')] = e.clone();
    });
    myRatio = $("#slidecanvas").attr('data-ratio');
    noHome = $('.my_home').size()<=0;
    if (noHome) {
        scrollingToStop();
        $(window).scroll(function(){
            scrollingToStop();
        });

        $(window).on('beforeunload', function() {
            $(window).scrollTop(0);
        });
    }
});

$.urlParam = function (name) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (!results) {
        return 0;
    }
    return results[1] || 0;
}

if ($.urlParam('startingSlide') != '') {
    startingSlide = $.urlParam('startingSlide') - 1;
}

var tmp_count_p = 0;
var tmp_count_l = 0;
var tmp_height_p = 0;
var tmp_height_l = 0;
function setLayoutBanner(){
    var width = $(window).width();
    var height = $(window).height();
    var height_head = $("#main_header").height();
    // alert(height);
    // var sm_menu = $("#desktop-menu").outerWidth();
    // var side_right = $(".side-right").width();
    // var n_width = width - (sm_menu+side_right);
    // var bar_h = $("#bar-menu").outerHeight();
    // $("#contain-body .detail-highlight").css({ width:n_width-48});
    if(width < 768){
        // var win_height = $(window).height();
        // var xs_menu = $("#xs-menu").outerHeight();
        if (window.orientation == 0 || window.orientation == 180) {
            if(tmp_count_p == 0){
                tmp_height_p = height;
                tmp_count_p++;
            }
            $(".home-page").css({ width:"100%",height:tmp_height_p});
        } else if (window.orientation == 90 || window.orientation == -90) {
            if(tmp_count_l == 0){
                tmp_height_l = height;
                tmp_count_l++;
            }
            $(".home-page").css({ width:"100%",height:tmp_height_l});
        }else{
            $(".home-page").css({ width:"100%",height:height});
        }

    }else{
        // var menu_height = $("#desktop-menu").outerHeight();
        // alert(height+"ss"+$("#desktop-menu").outerHeight());
        $(".home-page").css({width:"100%", height:height});//width:n_width,
    }
    if ($('#banner_promot').size()>0) {
        var percentResize = $(window).height() * 90 / 100;
        $(".home-page").css({width:"100%", height:percentResize+'px'});
    }
}
function loadSlideshow() {
    if ($('#slidecanvas').length > 0) {
        resizeImages(); loadImages();

    }
}

function loadImages() {

    var totalImages = $("#slideshow img").not(':eq(' + startingSlide + ')').size();

    //load only the first image (or selected by qs)
    var firstImage = $('#slideshow img:eq(' + startingSlide + ')');
    var img1 = new Image();
    $(img1).load(nextImages).attr('src', $(firstImage).attr('src'));

    function nextImages() {
        //prepare it and display the first image
        $(firstImage).attr('src', $(firstImage).attr('src')).fadeIn(imgTransition, function () {

            //show first caption, and add special class to exclude it from first loop
            // var cpt = $(this).attr('data-cpt');
            // if (cpt.length > 0) {
                // $('#capCycle').html(cpt).addClass('firstCpt').fadeIn(250);
            // }

            //assign +/- class and the click event for show/hide caption

        });

        //not single
        if ((totalImages > 0) && (loadOnClick == 0)) {
            $('#slideshow img').not(':eq(' + startingSlide + ')').each(function () {
                var img = new Image();
                $(img).load(prepareTheCycle).attr('src', $(this).attr('src'));
            });

        } else {

            //remove loading...
            // $('#loader').fadeOut(250);

        }

    }

    function prepareTheCycle() {
        if (--totalImages) return;
        $('#slideshow img').not(':eq(' + startingSlide + ')').each(function () {
            $(this).attr('src', $(this).attr('src'));
        });

        //add a delay to wait the end of first fadein (ie bug)
        // var timeout = setTimeout(function () { startCycle() }, 1000);
    }

}

function goToNextPrevImage(direction) {
    //define elements
    var $active = $('#slideshow img.active');
    var $next;
    //get direction
    if (direction == 'nxt') {
        $next = $active.next().length ? $active.next() : $('#slideshow img:first');
    }
    if (direction == 'prv') {
        $next = $active.prev();
        if ($active.attr('src') == $('#slideshow img:first').attr('src')) {
            $next = $('#slideshow img:last');
        }
    }

    if ($next.attr('src') != $next.attr('src')) {
        //show loader, only if is not already loaded
        $('#loader').fadeIn(250);
    }

    //load image
    var imgToLoad = new Image();
    $(imgToLoad).load(animateNextImg).attr('src', $next.attr('src'));
    //transition
    function animateNextImg() {
        $('#loader:visible').fadeOut(250); //hide loader, if visible
        //hide caption
        // $('#capCycle').not('.hide').fadeOut(250, function () { $(this).empty() });

        $active.addClass('last-active');
        $next
        .css({ opacity: 0.0 })
        .attr('src', $next.attr('src'))
        .show()
        .addClass('active')
        .animate({ opacity: 1.0 }, imgTransition, function () {
            $active.removeClass('active last-active').hide();
            var cpt = $(this).attr('data-cpt');
            if (cpt.length > 0) {
                // $('#capCycle').html(cpt);
                // $('#capCycle').not('.hide').fadeIn(250);
            }
        })
    }
}
var apiFnc,checkLoad = false;
function onYouTubeIframeAPIReady(){
    if (!checkLoad) {
        if (typeof(apiFnc) == "function") {
            apiFnc();
            checkLoad = true;
        } else {
            setTimeout(function(){
                checkLoad = false;
                onYouTubeIframeAPIReady()
            },200);
        }
    }
}

var myPlayer = {},played = false,moreOne = false;
function startCycle() {
    moreOne = $('#slideshow .wrap_slide_content').size()>1;
    var cycleSlide = $('#slideshow').cycle({
        fx: 'fade',
        speed: imgTransition,
        timeout: 4000,
        sync: true,
        pause:0,
        loop:1,
        // pager:  '#slide_pager',
        pager:  '.super_pager',
        containerResize: false,
        slideResize: false,
        continueAuto: false,
        slides: '> .wrap_slide_content',
        timeClass: '.cycle_type',
        startingSlide: 0,
        cycleLoaded: function(opts){
            var firstSlide = $('#slideshow .wrap_slide_content:eq(0)'),typeCycle = firstSlide.find('.cycle_type').val(),dataTime = parseInt(firstSlide.find('.cycle_type').attr('data-time')) * 1000;
            setTimeDelay(dataTime);
            if (typeCycle != "image") {
                if (opts.timeoutId) clearTimeout(opts.timeoutId);
                destroyPlayer();
                    if (typeCycle == "youtube") {
                        apiFnc = function(){
                            loadVideo(typeCycle,firstSlide.find('.video_content').attr('id'));
                        }
                    } else {
                        checkLoad = true;
                        loadVideo(typeCycle,firstSlide.find('.video_content').attr('id'));
                    }
                setTimeout(function(){
                    // remove cycle sentinel ---- if type 'video' or 'youtube' video double play.
                    $('#slideshow .cycle-sentinel .video_content').remove();
                },200);
            }

            $('#slideshow').touchwipe({
                preventDefaultEvents : false,
                min_move_x : 100,
                wipeLeft : function(){
                    cycleNext("next");
                },
                wipeRight : function(){
                    cycleNext("prev");
                }
            });

            $(window).scrollTop(0);
            animateScrollD();
        }
    });
    // loadVideo($('#slideshow .wrap_slide_content:eq(0) .cycle_type').val(),$('#slideshow .wrap_slide_content:eq(0) .video_content').attr('id'));
    cycleSlide.on('cycle-after',cycleAfterSlide);
    cycleSlide.on('cycle-before',cycleBeforeSlide);
}
var timeoutDelay;
function clearTimeOutDelay(){ if (timeoutDelay) clearTimeout(timeoutDelay); }
function setTimeDelay(time){ if (timeoutDelay) clearTimeout(timeoutDelay); timeoutDelay = setTimeout(function(){ cycleNext(); },time); }

function cycleAfterSlide(slideOpts, curr, next, fwd){
    var activeElem = $('#slideshow .'+curr.slideActiveClass),time = parseInt(activeElem.find(curr.timeClass).attr('data-time')) * 1000;
    setTimeDelay(time);
}

function cycleBeforeSlide(e, opts, outgoing, incoming, forward) {
    var activeSlide = $(incoming),cycleType = activeSlide.find('.cycle_type').val(),videoID = activeSlide.find('.video_content').attr('id');
    destroyPlayer();
    addVideoOnRemove(videoID);
    loadVideo(cycleType,videoID);
}

var playerReadyInterval,pauseInverval,playerState;
function loadVideo(type,videoID) {
    if (typeof(videoID) == "undefined") { fadePlayBtn('fadeOut'); return; }
    if (videoID.indexOf('#') == -1) videoID = '#'+videoID;
    var dataVideo = $(videoID).attr('data-video');
    nowPlay = type;
    if (type == "video") {
        setTimeout(function(){ fadePlayBtn('fadeIn'); },$.fx.speeds.slow)
        nowVideo = videoID;
        if ($(videoID).find('.big-video-wrap').size()>0) {
            if (!checkSmartPhone) {
                setTimeout(function(){clearTimeOutDelay()},500);
                playNow();
            } else {
                if (checkIpad) resizeCoverFirstRatio('on',10);
            }
            return;
        }

        var BV = new $.BigVideo({ container : $(videoID) , doLoop : (!moreOne && !checkSmartPhone) });
        BV.init();
        // BV.show("http://video-js.zencoder.com/oceans-clip.mp4");
        BV.show(dataVideo);
        if (typeof(myPlayer[type]) != "object") myPlayer[type] = {};
        myPlayer[type][videoID] = BV.getPlayer();
        countVideo++;
        myPlayer[type][videoID].ready(function(){
            myPlayer[type][videoID].on("play", function(){
                clearTimeOutDelay();
                resizeCoverFirstRatio('on',10);
                removeCoverPhoto(videoID);
                fadePlayBtn('fadeOut');
                played = true;
                $(videoID).parents('.wrap_slide_content').addClass('playing');
                // $(videoID).find('.big-video-wrap').css('visibility','visible');
            });
            if (!checkSmartPhone) {
                myPlayer[type][videoID].play();
            } else {
                if (checkIpad) resizeCoverFirstRatio('on',10);
            }
            // var timePlay = setTimeout(function(){
            //     // if (!played) loadVideo(type,videoID);
            //     if (!(myPlayer[nowPlay])) loadVideo(type,videoID);
            //     else clearTimeout(timePlay);
            // },2000);
            // fadePlayBtn('fadeIn');
            myPlayer[type][videoID].on("ended", function(){
                // ended
                // setTimeout(function(){
                    destroyPlayer(0);
                    cycleNext();
                    if (!moreOne) fadePlayBtn('fadeIn');
                    // played = false;
                // },400);
            });

            if (typeof(scrollingToStop) == "function") scrollingToStop();
        });
    } else if (type == "youtube") {
        fadePlayBtn('fadeOut');
        var code = getEmbedCode(dataVideo);
        var playVar = {
            controls: 0,
            showinfo: 0,
            rel: 0,
            loop: moreOne?0:1,
            autoplay: checkSmartPhone?0:1,
            playsinline: 1,
            wmode: 'transparent'
        }
        if (playVar.loop==1) playVar["playlist"] = code[1];

        myPlayer[type] = new YT.Player(videoID.split('#')[1], {
            videoId: code[1],
            playerVars: playVar,
            events: {
                onReady: function(event) {
                    // event.target.playVideo();
                    // if (checkWindowSize() == "desktop") playNow();
                    // fadePlayBtn('fadeIn');
                    if (!checkSmartPhone) resizeCoverFirstRatio('on',10);
                    if (typeof(scrollingToStop) == "function") scrollingToStop();
                },
                onStateChange: function(event) {
                    playerState = event.data;
                    if (event.data == 0) {
                        cycleNext();
                        destroyPlayer();
                        played = false;
                        // if (!moreOne) fadePlayBtn('fadeIn');
                    }
                    if (event.data == 1) {
                        clearTimeOutDelay();
                        played = true;
                        resizeCoverFirstRatio('on',100);
                        removeCoverPhoto(videoID);
                        fadePlayBtn('fadeOut');
                        $(videoID).parents('.wrap_slide_content').addClass('playing');
                    }
                    if (event.data == 3) {
                        clearTimeOutDelay();
                    }
                }
            }
        });
    }
}

function removeCoverPhoto(videoID) {
    // if ($(window).width()<768) {
    if (checkSmartPhone && $(videoID).parent().find('.cycle_type').val() != "youtube") {
        $(videoID).animate({ opacity : 1 },$.fx.speeds.slow);
        $(videoID).parents('.wrap_slide_content').find('.cover_video_content').fadeOut('slow');
    }
}

var nowPlay = null,nowVideo;
function playNow(setTime){
    if (nowPlay != null && !played) {
        if (nowPlay == "video") {
            if (!checkSmartPhone) {
                setPlayerInterval(function(){
                    if (played) clearInterval(playerReadyInterval);
                    else {
                        played = false;
                        myPlayer[nowPlay][nowVideo].play();
                        if ((typeof(setTime) == "undefined")) myPlayer[nowPlay][nowVideo].currentTime(0);

                    }
                },500);
            } else {
                myPlayer[nowPlay][nowVideo].play();
                if ((typeof(setTime) == "undefined")) myPlayer[nowPlay][nowVideo].currentTime(0);
            }
            // removeCoverPhoto('#'+$('#slideshow .cycle-slide-active .video_content').attr('id'));
            removeCoverPhoto(nowVideo);
        } else {
            if (typeof(myPlayer[nowPlay].playVideo) == "function") myPlayer[nowPlay].playVideo();
            // if (checkWindowSize() == "mobile") {
            if (checkSmartPhone) {
                setPlayerInterval(function(){
                    if (playerState == 1) clearInterval(playerReadyInterval);
                    else {
                        // myPlayer[nowPlay].playVideo();
                        played = false;
                        $('#slidecanvas .play_box').click();
                    }
                },1000);
            }
        }
        // fadePlayBtn('fadeOut');
    }
}
function destroyPlayer(ended){
    if (!moreOne && !checkSmartPhone) return;
    if (myPlayer[nowPlay] && played) {
        if (nowPlay == "video") {
            if(typeof(myPlayer['video'][nowVideo].pause) == "function") {
                if(typeof(ended) == "undefined") myPlayer['video'][nowVideo].pause();
                // $('#slideshow .video_content').empty();
            }
        } else if (nowPlay == "youtube") {
            myPlayer['youtube'].destroy();
        }
        // myPlayer[nowPlay] = null;
        $('#slideshow .cover_video_content').show();
        if(!checkSmartPhone) $('#slideshow .video_content').css('opacity',1);
        else $('#slideshow .video_content').css('opacity',0);
    }
    played = false;
    resizeCoverFirstRatio();
    $('.wrap_slide_content.playing').removeClass('playing');
}

function setPlayerInterval(fnc,time){
    if (typeof(time) == "undefined") time = 1000;
    if (typeof(fnc) == "function") {
        if (playerReadyInterval) clearInterval(playerReadyInterval);
        playerReadyInterval = setInterval(fnc,time);
    }
}

function cycleNext(action){
    if (typeof(action) == "undefined" || (action != "next" && action != "prev")) action = "next";
    $('#slideshow').cycle(action);
}


function addVideoOnRemove(videoID){
    $.each(videoClone,function(k,v){
        if ($('#'+k).size()<=0) {
            videoClone[k].prependTo($('#wrap_'+k));
        }
    });
}

function fadePlayBtn(action) {
    if (checkSmartPhone) {
        if (action != "fadeIn" && action != "fadeOut") action = "fadeIn";
        $('#slidecanvas .play_box')[action]('fast');
    }
}

function resizeCoverFirstRatio(on,time){
    if (typeof(time) == "undefined") time = 500;
    if (typeof(on) != "undefined") {
        setTimeout(function(){
            resizeImages(myRatio.split(',')[0]);
        },parseInt(time));
    } else {
        resizeImages();
    }
}

function resizeImages(ratio,reElem) {
    if ($('#slideshow').size()<=0) return;
    if (typeof(myRatio) == "undefined") return;
    var wh = $('#slidecanvas').height();
    var ww = $('#slidecanvas').width();
    if (typeof(reElem) == "undefined") reElem = $('#slideshow .slide_content');
    if (typeof(ratio) == "undefined") {
        ratio = myRatio.split(',');
        if (played) {
            resizeImages(ratio[0]);
            return;
        }
        ratio = ww>wh?ratio[0]:ratio[1];
    }
    if (ww<768 && !played) {
        ratio = myRatio.split(',')[1];    }
    if (checkIpad && checkSmartPhone) {
        var tmpRatio = myRatio.split(',');
        if (played) ratio = tmpRatio[0];
        else {
            if ($('#main_home').hasClass('landscape')) {
                ratio = tmpRatio[0];
            } else if ($('#main_home').hasClass('portrait')) {
                ratio = tmpRatio[1];
            }
        }
    }
    var imgw = wh * ratio;
    var fh;
    var fw;
    if (imgw <= ww) {
        fh = ww / ratio;
        fw = ww;
    } else {
        fh = wh;
        fw = imgw;
    }

    // reElem.css({"height":fh,"width":fw});
    reElem.each(function(i,v) {
        if (noHome) {
            // $(this).css({"height":parseInt(fh)*0.9,"width":fw});
            if($(window).width() < 768){
                $(this).css({"height":fh,"width":fw*1.2});
            }else{
                $(this).css({"height":fh,"width":fw});
            }
        } else {
            $(this).css({"height":fh,"width":fw});
        }
    });
    if (fw >= ww) {
        if (noHome) {
            if($(window).width() < 768){
               $('#slideshow').css('left', - (((fw*1.2) - ww) /2 ));
            }else{
               $('#slideshow').css('left', - ((fw - ww) /2 ));
            }
        } else {
             $('#slideshow').css('left', - ((fw - ww) /2 ));
        }
        $('#slideshow').css('top', -(fh - wh) / 2);
    }
    moveCapture(ww,wh);
}

var footerFixHeight = 0,slideCapture = null,slideLinkBanner = null;
function moveCapture(slideWidth,slideHeight) {
    if ($('#slideshow').size()<=0) return;
    if (footerFixHeight == 0) footerFixHeight = $('#footer_fix').height();
    if (slideCapture == null) slideCapture = $('#slideshow .slide_capture');
    if (slideLinkBanner == null) slideLinkBanner = $('#slideshow .link_banner');
    var leftContent = $('#slideshow').position().left;
    // capture
    slideCapture.each(function(){
        var e = $(this);
        var captureWidth = e.outerWidth(),captureHeight = e.outerHeight();
        var headerHeight = $('#header').height();
        var allTop = ((slideHeight - captureHeight) / 2) - footerFixHeight;
        if (slideWidth > 768) {
            var movement = 55;

            // move from center
            if (e.hasClass('center')) {
                var allLeft = (slideWidth - captureWidth) / 2;
                e.css('left',allLeft - leftContent);
                // move from top
                e.css('top',allTop);
            }
            // move from left
            if (e.hasClass('left')) {
                var allRight = (slideWidth - captureWidth);
                e.css('left',-(leftContent) + movement);
            }
            // move from right
            if (e.hasClass('right')) {
                var allRight = (slideWidth - captureWidth);
                e.css('left',allRight - leftContent - movement);
            }
            // move from top
            if (e.hasClass('top')) {
                allTop = headerHeight + movement;
                // e.css('top',allTop);
                e.css('top',allTop);
            }
            // move from bottom
            if (e.hasClass('bottom')) {
                allTop = slideHeight - captureHeight - footerFixHeight - movement;
                // e.css('top',allTop);
                e.css('top',allTop);
            }
        } else {
            var allLeft = (slideWidth - captureWidth) / 2;
            e.css('left',allLeft - leftContent);
            e.css('top',allTop);
        }

        if ((headerHeight + 10) >= allTop) {
            e.css('top',headerHeight + 10);
        } else if ((allTop + captureHeight) > slideHeight) {
            // e.css('top',allTop - 20);
        }
    });

    // link banner
    slideLinkBanner.each(function(){
        var e = $(this);
        e.width(slideWidth);
        e.height(slideHeight);
        e.css('left',leftContent * (-1));
    });
}

function detectIpadOrientation(time) {
    // if ($('#main_home').hasClass('ipad')) {
        if (typeof(time) == "undefined") time = 0;

        setTimeout(function(){
            // if (Math.abs(window.orientation) === 90) {
            if ($(window).width() > $(window).height()) {
                // alert(1);
                // $('#main_home.ipad').addClass('landscape').removeClass('portrait');
                $('#main_home').addClass('landscape').removeClass('portrait');
            } else {
                // alert(2);
                // $('#main_home.ipad').removeClass('landscape').addClass('portrait');
                $('#main_home').removeClass('landscape').addClass('portrait');
            }
            resizeImages();
        },parseInt(time));
    // }
}

function stopPlayer(){
    // if (!moreOne && !checkSmartPhone) return;
    if (myPlayer[nowPlay] && played) {
        if (nowPlay == "video") {
            if(typeof(myPlayer['video'][nowVideo].pause) == "function") {
                myPlayer['video'][nowVideo].pause();
                // $('#slideshow .video_content').empty();
            }
        } else if (nowPlay == "youtube") {
            myPlayer['youtube'].stopVideo();
        }
        // myPlayer[nowPlay] = null;
        // $('#slideshow .cover_video_content').show();
        // if(!checkSmartPhone) $('#slideshow .video_content').css('opacity',1);
        // else $('#slideshow .video_content').css('opacity',0);
    }
    played = false;
    resizeCoverFirstRatio();
    $('.wrap_slide_content.playing').removeClass('playing');
}
var tmpPlay = true;
function scrollingToStop(){
    if (videoFullScreen) return;
    if (!noHome) return;
    if (checkSmartPhone) return;
    var windowScroll = $(window).scrollTop(),bodyHeight = $(document).height(),wHeight = $(window).height();
    var totalScroll = bodyHeight - wHeight;
    var stopPercent = 10,stopTo = totalScroll * (stopPercent / 100);
    if (windowScroll > stopTo) {
        stopPlayer();
        if (!noPlay) {
            if (tmp_pd["currentplay"] && typeof(tmp_pd["currentplay"].isPlaying) == "function" && !tmp_pd["currentplay"].isPlaying()) {
                tmp_pd["currentplay"].play(0);
            }
        }
        if (!tmpPlay) {
            tmpPlay = true;
            if (tmp_pd["currentplay"] && typeof(tmp_pd["currentplay"].isPlaying) == "function" && !tmp_pd["currentplay"].isPlaying()) {
                tmp_pd["currentplay"].play(0);
            }
        }
    } else {
        playNow(0);
        if (!noPlay) {
            if (tmp_pd["currentplay"]) {
                tmp_pd["currentplay"].pause(0);
            }
            tmpPlay = false;
        }
    }
}

function captureLinkOut(e) {
    e = $(e);
    if (e.find('.transparent_link').size()>0) {
        var link = e.find('.transparent_link').val();
        if (link) {
            window.open(link,"_blank");
        }
    }
    return false;
}