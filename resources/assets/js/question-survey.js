var ObjSurvey = {};

var thankPage = false;
//var duration = 3000;
var	ans_facebook = 0;
var	ans_line = 0;
var	ans_website = 0;
var objSocial = {
	ans_facebook : 0,
	ans_line : 0,
	ans_website : 0
};

var sec1 = "0";
var sec2 = "1";
var sec3 = "2";
var chk1 = false;
var chk2 = false;
var chk3 = false;
var index1 = "";
var index2 = "";
var index3 = "";
$(document).ready(function(){
	$('#page-1-new').fadeOut();
	$('#page-1').fadeOut();
	$('#page-2').fadeOut();
	$('#page-3').fadeOut();
	$('#page-thank-you').fadeOut();

	$('.area-desc-other').val("");
	//$(".custom-radio").removeClass('active-radio');

	setTimeout(GotoPage1new, 3000);
	$('.radio-choice').prop('checked', false);
	$(".all-sub-choice").hide();
	save_know_smom();
	nexttopage3();
	saveService();

	$(".radio-choice").change(function(){
		$('.active-radio').next().removeClass('font-face-med');
		$('.active-radio').next().removeClass('font-black');
		$('.radio-choice').parent().removeClass('active-radio');

		$('.radio-choice:checked').parent().addClass('active-radio');
		$('.active-radio').next().addClass('font-face-med');
		$('.active-radio').next().addClass('font-black');

		$(".all-sub-choice").slideUp("slow");

		none_pointer_radio($(this));
	});

	$(".sub-radio-choice").change(function(){
		$('.active-sub-radio').next().removeClass('font-face-med');
		$('.active-sub-radio').next().removeClass('font-black');
		$('.sub-radio-choice').parent().removeClass('active-sub-radio');

		$(this).parent().addClass('active-sub-radio');
		$(this).parent().next().addClass('font-face-med');
		$(this).parent().next().addClass('font-black');

		none_pointer_radio($(this));
	});

	$(".radio-answer").change(function(){
		if ($(".radio-answer:checked").length == 1) {
		}else if($(".radio-answer:checked").length == 2) {
			setTimeout(function(){
				var num = "";
					$(".answer-social").each(function(e,v){
					var elm = $(this);
					elm.find(".social-radio").each(function(ee,vv){
						if($(this).hasClass("active-social-radio")){
							if(sec1 == e){
								chk1 = true;
								index1 = $(vv).index();
								num += "|"+$(vv).index();
							}
							if(sec2 == e){
								chk2 = true;
								index2 = $(vv).index();
								num += "|"+$(vv).index();
							}
							if(sec3 == e){
								chk3 = true;
								index3 = $(vv).index();
								num += num+"|"+$(vv).index();
							}
						}
					})
				});
				if(!chk1){
					selectIndex(index1,index2,index3,0,num);
				}
				if(!chk2){
					selectIndex(index1,index2,index3,1,num);
				}
				if(!chk3){
					selectIndex(index1,index2,index3,2,num);
				}

			},10);
		}else{

		}

		// close_val_social($(this));
	});

	$('#yes').change(function(){
		$(".all-sub-choice").slideDown( "slow" );
	});


	// page2

	$('#about_smom_comment').val('');
	$('#suggestion_comment').val('');
	//$(".btn-next ").hide();
	$(".warp-all-choice").hide();

	$('#ever').change(function(){
		$(".all-sub-choice").show();
		//$(".btn-next ").show();
		$(".warp-all-choice").removeClass('hide-page');
		$(".warp-all-choice").slideDown({
			duration: 'slow',
			complete: function(){
				// if ($(window).width() > 990) {
				// 	$('.warp-page2').height($(document).height());
				// 	$('.box-question2').height($(document).height());
				// }
			}
		});
	});

	$(".facebook-radio-answer").change(function(){
		$(".facebook-social-radio").removeClass('active-social-radio');
		$(".facebook-number-center").removeClass('number-center-active');
		$(this).parent().addClass('active-social-radio');
		$(this).next().addClass('number-center-active');
		close_val_social();
	});

	$(".line-radio-answer").change(function(){
		$(".line-social-radio").removeClass('active-social-radio');
		$(".line-number-center").removeClass('number-center-active');
		$(this).parent().addClass('active-social-radio');
		$(this).next().addClass('number-center-active');
		close_val_social();
	});

	$(".website-radio-answer").change(function(){
		$(".website-social-radio").removeClass('active-social-radio');
		$(".website-number-center").removeClass('number-center-active');
		$(this).parent().addClass('active-social-radio');
		$(this).next().addClass('number-center-active');
		close_val_social();
	});

	$(".number-center").click(function(){
		$(this).prev('.radio-answer').trigger('click');
	});


	$(".line-radio-answer").change(function(){
		answer_line = $('input[name="line_answer"]:checked').val();
		value_line = answer_line;
	});

	$(".website-radio-answer").change(function(){
		answer_web = $('input[name="website_answer"]:checked').val();
		value_web = answer_web;
	});

	$(".pleased_rating").change(function(){
		pleased_rating = $('input[name="pleased_rating"]:checked').val();
		value_pleased = pleased_rating;
	});

	$(".credible_rating").change(function(){
		credible_rating = $('input[name="credible_rating"]:checked').val();
		value_credible = credible_rating;
	});

	$(".conveniently_rating").change(function(){
		conveniently_rating = $('input[name="conveniently_rating"]:checked').val();
		value_conveniently = conveniently_rating;
	});

	$(".facebook-radio-answer").change(function(){
		ans_facebook = $('input[name="facebook_answer"]:checked').val();
		ObjSurvey.service_from_fb = ans_facebook;
		objSocial["ans_facebook"] = ans_facebook;
		saveDataEver_q2();
	});

	$(".line-radio-answer").change(function(){
		ans_line = $('input[name="line_answer"]:checked').val();
		ObjSurvey.service_from_line = ans_line;
		objSocial["ans_line"] = ans_line;
		saveDataEver_q2();
	});

	$(".website-radio-answer").change(function(){
		ans_website = $('input[name="website_answer"]:checked').val();
		ObjSurvey.service_from_web = ans_website;
		objSocial["ans_website"] = ans_website;
		saveDataEver_q2();
	});

	$(".pleased_rating").change(function(){
		pleased_rating = $('input[name="pleased_rating"]:checked').val();
		ObjSurvey.rating_pleased = pleased_rating;
		saveDataEver_q2();
	});

	$(".credible_rating").change(function(){
		credible_rating = $('input[name="credible_rating"]:checked').val();
		ObjSurvey.rating_credible = credible_rating;
		saveDataEver_q2();
	});

	$(".conveniently_rating").change(function(){
		conveniently_rating = $('input[name="conveniently_rating"]:checked').val();
		ObjSurvey.rating_conveniently = conveniently_rating;
		saveDataEver_q2();
	});

	$(".interest_rating").change(function(){
		interest_rating = $('input[name="interest_rating"]:checked').val();
		ObjSurvey.rating_interest = interest_rating;
		saveDataEver_q2();
	});

	$(".useful_rating").change(function(){
		useful_rating = $('input[name="useful_rating"]:checked').val();
		ObjSurvey.rating_useful = useful_rating;
		saveDataEver_q2();
	});

	$(".ans-like-club").change(function(){
		//alert($('input[name="like_club"]:checked').val());
		like_club_val = $('input[name="like_club"]:checked').val();
		ObjSurvey.like_club = like_club_val;
		saveDataEver_q2();
	});

	// page 3;

	saveData_q3();
	$(".click-choice").change(function(){
		$(".custom-radio").removeClass('active-radio');
		$(this).parent().addClass('active-radio');
	});

});

$( window ).resize(function() {
 // 	if ($(window).width() > 990) {
	// 	$('.warp-page2').height($(document).height());
	// 	$('.box-question2').height($(document).height());
	// }else{
	// 	$('.warp-page2').css('height','auto');
	// 	$('.box-question2').css('height','auto');
	// }
});

function selectIndex(index1,index2,index3,inx,num){
	// $(".radio-answer").parent().addClass('none-pointer-event');
	var i = 1;
	var rs = 0;
	for (i = 1 ; i <= 3; i++) {
		if(num.indexOf(i) < 1){
			rs = i ;
		}
	}
		if(index1 == ""){
			$(".answer-social").eq(0).find(".social-radio").eq(Number(rs)-1).find("input").click();

		}
		if(index2 == ""){
			$(".answer-social").eq(1).find(".social-radio").eq(Number(rs)-1).find("input").click();

		}
		if(index3 == ""){
			$(".answer-social").eq(2).find(".social-radio").eq(Number(rs)-1).find("input").click();

		}
}

function close_val_social(){
	$(".radio-answer").parent().removeClass('none-pointer-event');
	var i = 1;
	for(i = 1 ; i <=3; i++){

		$(".radio-answer[value='"+i+"']").each(function(){
			if($(this).parent().hasClass("active-social-radio")){
				$(".radio-answer[value='"+i+"']").parent().addClass('none-pointer-event');
			}
		});
	}


}

function none_pointer_radio(ele){
	var name_radio =  ele.attr('name');
	$("input[name='"+name_radio+"']").parent().parent().addClass('none-pointer-event');
}


function checkPageOne(obj){

		if($('input[name=quetion-status]:checked').length > 0  && $('input[name=quetion-age]:checked').length > 0 && $('input[name=quetion-living]:checked').length > 0){

			ObjSurvey.status_mom = $('input[name=quetion-status]:checked').val();
			ObjSurvey.age_mom = $('input[name=quetion-age]:checked').val();
			ObjSurvey.living_mom = $('input[name=quetion-living]:checked').val();
			setTimeout(GotoPage1,1000);
		}

}


function save_know_smom(){
	$(".know_smom").change(function(){
		if ($('#yes').is(':checked')){
			value = $('input[name="answer-ques1"]:checked').val();
			ObjSurvey.know_about_smom = value;
			$(".know_smom_from").change(function(){
				var value_know = $('input[name="know-from"]:checked').val();
				ObjSurvey.know_from = value_know;
				if (value_know != "other") {
					setTimeout(GotoPage2, 1000);
					//$('#yes').prop('checked', false);
				}else{
					$('.btn-next').show();
					$(".desc-other").slideDown();
				}
			});
		}else{
			value = $('input[name="answer-ques1"]:checked').val();
			ObjSurvey.know_about_smom = value;
			setTimeout(GotoThankYou, 1000);
		}
	});
}

function nexttopage3(){
	$(".btn-next").click(function(){
		ObjSurvey.other = $('.area-desc-other').val();
		if ($(".area-desc-other").val() == "") {
			$("#varlidate-other").modal("show");
		}else{
			//$(this).addClass('none-pointer-event');
			setTimeout(GotoPage2, 1000);
			$(".area-desc-other").val("");
			$('.btn-next').addClass('btn-wait');
		}
	});
}

function saveDataEver_q2(){
	//('.btn-next').addClass('btn-wait');
	var value_fb   = $('input[name="facebook_answer"]:checked').length;
	var value_line = $('input[name="line_answer"]:checked').length;
	var value_web  = $('input[name="website_answer"]:checked').length;
	var value_pleased  = $('input[name="pleased_rating"]:checked').length;
	var value_credible  = $('input[name="credible_rating"]:checked').length;
	var value_conveniently  = $('input[name="conveniently_rating"]:checked').length;
	var value_interest  = $('input[name="interest_rating"]:checked').length;
	var value_useful  = $('input[name="useful_rating"]:checked').length;
	var value_like_club  = $('input[name="like_club"]:checked').length;
	if ((value_fb > 0) && (value_line > 0) && (value_web > 0) && (value_pleased > 0) && (value_credible > 0) && (value_conveniently > 0) && (value_interest > 0) && (value_useful > 0) && (value_like_club > 0) ) {
		setTimeout(GotoPage3, 1000);
	}
}
function saveService() {
	$(".service_smom").change(function(){
		if ($('#ever').is(':checked')) {
			var value_ever = $('input[name="choice-ques2"]:checked').val();
			ObjSurvey.service_smom = value_ever;
		}else{
			var value_never = $('input[name="choice-ques2"]:checked').val();
			ObjSurvey.service_smom = value_never;
			setTimeout(GotoThankYou, 1000);
		}
	});
}

function assocArraySize (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};
function saveData_q3(){
	$('.click-choice').change(function(){
		value_page3 = this.value;
		ObjSurvey.think_about_brand = value_page3;
		$(this).prop('checked', false);
		setTimeout(GotoThankYou, 1000);
	});
}
function GotoPage1new(){
	$('#page-intro-smom').fadeOut();
	$('#page-1').fadeOut();
	$('#page-2').fadeOut();
	$('#page-3').fadeOut();
	$('#page-thank-you').fadeOut();
	$('#page-1-new').fadeIn(2000);
}

function GotoPage1(){
	$('#page-intro-smom').fadeOut();
	$('#page-1-new').fadeOut();
	$('#page-2').fadeOut();
	$('#page-3').fadeOut();
	$('#page-thank-you').fadeOut();
	$('#page-1').fadeIn(2000);

}
function GotoPage2(){
	$('#page-intro-smom').fadeOut();
	$('#page-1-new').fadeOut();
	$('#page-1').fadeOut();
	$('#page-3').fadeOut();
	$('#page-thank-you').fadeOut();
	$('#page-2').fadeIn(2000);

	// if ($(window).width() > 990) {
	// 	var heightBoxQ2 = $('.box-img').eq(1).height();
	// 	$('.box-question2').height(heightBoxQ2-10);
	// }

}
var finish = false;
function GotoThankYou() {
	$('#page-intro-smom').fadeOut();
	$('#page-1-new').fadeOut();
	$('#page-1').fadeOut();
	$('#page-2').fadeOut();
	$('#page-3').fadeOut();
	$('#page-thank-you').fadeIn(2000);
	if ((assocArraySize(ObjSurvey) != "" || assocArraySize(ObjSurvey) != 0) && finish === false) {
		finish = true;

		$.ajax({
	        url: BASE_URL + '/mommysurveycmd',
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        },
	        type: 'POST',
	        dataType: 'json',
	        data: ObjSurvey,
	        success: function(data) {
	        	thankPage = true;
	        	setTimeout(GotoPageHome, 3000);

	        	$(".facebook-radio-answer").prop('checked', false);
				$(".line-radio-answer").prop('checked', false);
				$(".website-radio-answer").prop('checked', false);
				$(".pleased_rating").prop('checked', false);
				$(".credible_rating").prop('checked', false);
				$(".conveniently_rating").prop('checked', false);
				$(".interest_rating").prop('checked', false);
				$(".useful_rating").prop('checked', false);
				$(".ans-like-club").prop('checked', false);
	        }
	    });
	}
}

function GotoPage3(){
	$('#page-intro-smom').fadeOut();
	$('#page-1').fadeOut();
	$('#page-2').fadeOut();
	$('#page-thank-you').fadeOut();
	$('#page-3').fadeIn(2000);
}

function GotoPageHome(){
	$('#page-intro-smom').fadeOut();
	$('#page-1').fadeOut();
	$('#page-2').fadeOut();
	$('#page-thank-you').fadeOut();
	$('#page-3').fadeOut();
	window.location.href = 'https://www.s-momclub.com/th/home';
}

function validateSocial(){
 	var chkValSocial = true;
 	var tmpSelect = new Array();
 	$.each(objSocial,function(i,v) {
 		if (v === 0 || tmpSelect.indexOf(v) == -1) {
 			tmpSelect.push(v);
 		}
 	});
 	if (tmpSelect.length < assocArraySize(objSocial)) {
 		chkValSocial = false;
 		$('#varlidate-value').modal("show");
 	}
    return chkValSocial;
}

