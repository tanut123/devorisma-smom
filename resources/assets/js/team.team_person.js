$(document).ready(function() {
    $(".person_detail").mouseover(function(){
        var last_child = $(this).children()[3];
        if (!$(last_child).hasClass("hover")) {
            $(last_child).addClass("hover");
            $(last_child).show();
        }
    });
    $(".person_detail").mouseout(function(){
        var last_child = $(this).children()[3];
        if ($(last_child).hasClass("hover")) {
            $(last_child).removeClass("hover");
            $(last_child).hide();
        }
    });
    $(".person_detail").click(function() {
        var person_id = $(this).attr("person_id");
        $.ajax({
            url: BASE_LANG + 'team/person',
            type: 'POST',
            data: { 'person_id':person_id},
            dataType: 'json'
        })
            .success(function(d) {
                //var img = document.getElementById();
                $("#popup_img").attr('xlink:href', d['image_url']);
                $('.name').html(d["name"]);
                $('.position').html(d["position"]);
                $('.personal_detail').html(d["detail"]);
                $("#team_person_detail").fadeIn();
            })
            .fail(function() {
                alert("Load Data Fail")
            });
    });
    $(".close_popup_btn").click(function() {
        $("#team_person_detail").fadeOut();
    });
});