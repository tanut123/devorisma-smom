function plainFade() {
    var cssText = ".hidebody {" +
        "display: block;" +
        "opacity: 0; " +
        "}" +
        ".showbody {" +
        "opacity: 1;" +
        "display: block;" +
        "transition: opacity 0.5s ease-out;" +
        "}";
    var headStyle = document.getElementsByTagName('head')[0];
    var styleElement = document.createElement("style");
    styleElement.setAttribute("id", "preloadCss");
    styleElement.setAttribute('type', 'text/css');
    var browser = navigator.userAgent;
    if (browser.indexOf("MSIE 8") > -1) {
        styleElement.styleSheet.cssText = cssText;
    } else {
        styleElement.innerHTML = cssText;
    }
    headStyle.appendChild(styleElement);
    addClass(document.body,"hidebody")
}
function fadeBodyIn() {
    addClass(document.body,"loaded");
    setTimeout(function() {
        addClass(document.body,"showbody");
        setTimeout(function() {
            removeClass(document.body,"hidebody");
            removeClass(document.body,"showbody");
        },500);
    },500);

}

function addClass(element,classname) {
    var cn = element.className;
    if( cn.indexOf( classname ) != -1 ) {
        return;
    }
    if( cn != '' ) {
        classname = ' '+classname;
    }
    element.className = cn+classname;
}

function removeClass(element,classname) {
    var cn = element.className;
    var rxp = new RegExp( "\\s?\\b"+classname+"\\b", "g" );
    cn = cn.replace( rxp, '' );
    element.className = cn;
}