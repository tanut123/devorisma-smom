var animeInterval;
var loadingElement;
function preload(cssFile,option) {
    var d_option = {background:"#000",color:"#FFF",color_loader:[{size:"100",color:"#e74c3c",time:"2"},{size:"120",color:"#f9c922",time:"3"},{size:"140",color:"#00F",time:"4"}],loadingText:"LOADING",loadType:"dom"};
    //var d_option = {background:"#000",color:"#FFF",color_loader:{main:"#3498db",before:"#e74c3c",after:"#f9c922"},loadingText:"LOADING"};
    if (typeof(option) != "undefined") {
        if (typeof(option.background) != "undefined") {
            d_option.background = option.background;
        }
        if (typeof(option.color) != "undefined") {
            d_option.color = option.color;
        }
        if (typeof(option.loadingText) != "undefined") {
            d_option.loadingText = option.loadingText;
        }
        if (typeof(option.color_loader) != "undefined") {
            d_option.color_loader = option.color_loader;
        }
    }

    //var loadCss = 'body.loading_overflow{overflow:hidden;}.hidden,.invisible{visibility:hidden}.chromeframe{margin:.2em 0;background:#ccc;color:#000;padding:.2em 0}p{line-height:1.33em;color:#7E7E7E}h1{color:#EEE}#loader-wrapper{position:fixed;top:0;left:0;width:100%;height:100%;z-index:999999}#loader{display:block;position:relative;left:50%;top:50%;width:150px;height:150px;margin:-75px 0 0 -75px;border-radius:50%;border:3px solid transparent;border-top-color:' + d_option.color_loader.main + ';-webkit-animation:spin 2s linear infinite;animation:spin 2s linear infinite;z-index:1001}#loader-inner{display:block;position:relative;left:50%;top:50%;width:150px;height:150px;margin:-75px 0 0 -75px;border-radius:50%;border:3px solid transparent;border-top-color:' + d_option.color_loader.inner_main + ';-webkit-animation:spin 2s linear infinite;animation:spin 2s linear infinite;z-index:1001}#loader-inner:before{top:5px;left:5px;right:5px;bottom:5px;border:3px solid transparent;border-top-color:' + d_option.color_loader.inner_before + ';-webkit-animation:spin 3s linear infinite;animation:spin 3s linear infinite}#loader:before{top:5px;left:5px;right:5px;bottom:5px;border:3px solid transparent;border-top-color:' + d_option.color_loader.before + ';-webkit-animation:spin 3s linear infinite;animation:spin 3s linear infinite}#loader:after,#loader:before,#loader-inner:before{content:"";position:absolute;border-radius:50%}#loader:after{top:15px;left:15px;right:15px;bottom:15px;border:3px solid transparent;border-top-color:' + d_option.color_loader.after + ';-webkit-animation:spin 1.5s linear infinite;animation:spin 1.5s linear infinite}.ir,.visuallyhidden{border:0;overflow:hidden}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0);-ms-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-ms-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spin{0%{-webkit-transform:rotate(0);-ms-transform:rotate(0);transform:rotate(0)}100%{-webkit-transform:rotate(360deg);-ms-transform:rotate(360deg);transform:rotate(360deg)}}#loader-wrapper .loader-section{position:fixed;top:0;width:51%;height:100%;background:' + d_option.background + ';z-index:1000}#loader-wrapper .loader-section.section-left{left:0}#loader-wrapper .loader-section.section-right{right:0}.loaded #loader-wrapper .loader-section.section-left{-webkit-transform:translateX(-100%);-ms-transform:translateX(-100%);transform:translateX(-100%);-webkit-transition:all .7s .3s cubic-bezier(.645,.045,.355,1);transition:all .7s .3s cubic-bezier(.645,.045,.355,1)}.loaded #loader-wrapper .loader-section.section-right{-webkit-transform:translateX(100%);-ms-transform:translateX(100%);transform:translateX(100%);-webkit-transition:all .7s .3s cubic-bezier(.645,.045,.355,1);transition:all .7s .3s cubic-bezier(.645,.045,.355,1)}.loaded #loader{opacity:0;-webkit-transition:all .3s ease-out;transition:all .3s ease-out}.loaded #loader-wrapper{visibility:hidden;-webkit-transform:translateY(-100%);-ms-transform:translateY(-100%);transform:translateY(-100%);-webkit-transition:all .3s 1s ease-out;transition:all .3s 1s ease-out}#content{margin:0 auto;padding-bottom:50px;width:80%;max-width:978px}.ir{background-color:transparent}.ir:before{content:"";display:block;width:0;height:150%}.hidden{display:none!important}.visuallyhidden{clip:rect(0 0 0 0);height:1px;margin:-1px;padding:0;position:absolute;width:1px}.visuallyhidden.focusable:active,.visuallyhidden.focusable:focus{clip:auto;height:auto;margin:0;overflow:visible;position:static;width:auto}.clearfix:after,.clearfix:before{content:" ";display:table}.clearfix:after{clear:both}@media print{blockquote,img,pre,tr{page-break-inside:avoid}*{background:0 0!important;color:#000!important;box-shadow:none!important;text-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:" (" attr(href) ")"}abbr[title]:after{content:" (" attr(title) ")"}.ir a:after,a[href^="javascript:"]:after,a[href^="#"]:after{content:""}blockquote,pre{border:1px solid #999}thead{display:table-header-group}img{max-width:100%!important}@page{margin:.5cm}h2,h3,p{orphans:3;widows:3}h2,h3{page-break-after:avoid}}';
    var loadCss = 'body.loading_overflow{overflow:hidden;}.chromeframe{margin:.2em 0;background:#ccc;color:#000;padding:.2em 0}p{line-height:1.33em;color:#7e7e7e}h1{color:#eee}#loader-wrapper{position:fixed;display:table;top:0;left:0;width:100%;height:100%;z-index:9999998}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0deg);-ms-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);-ms-transform:rotate(360deg);transform:rotate(360deg)}}@keyframes spin{0%{-webkit-transform:rotate(0deg);-ms-transform:rotate(0deg);transform:rotate(0deg)}100%{-webkit-transform:rotate(360deg);-ms-transform:rotate(360deg);transform:rotate(360deg)}}#loader-wrapper .loader-section{position:fixed;top:0;width:51%;height:100%;background:'+d_option.background+';z-index:1000;-webkit-transform:translateX(0);-ms-transform:translateX(0);transform:translateX(0)}#loader-wrapper .loader-section.section-left{left:0}#loader-wrapper .loader-section.section-right{right:0}.loaded #loader-wrapper .loader-section.section-left{-webkit-transform:translateX(-100%);-ms-transform:translateX(-100%);transform:translateX(-100%);-webkit-transition:all .7s .3s cubic-bezier(0.645,0.045,0.355,1);transition:all .7s .3s cubic-bezier(0.645,0.045,0.355,1)}.loaded #loader-wrapper .loader-section.section-right{-webkit-transform:translateX(100%);-ms-transform:translateX(100%);transform:translateX(100%);-webkit-transition:all .7s .3s cubic-bezier(0.645,0.045,0.355,1);transition:all .7s .3s cubic-bezier(0.645,0.045,0.355,1)}.loaded #loader{opacity:0;-webkit-transition:all .3s ease-out;transition:all .3s ease-out}.loaded #loader-wrapper{visibility:hidden;-webkit-transform:translateY(-100%);-ms-transform:translateY(-100%);transform:translateY(-100%);-webkit-transition:all .3s 1s ease-out;transition:all .3s 1s ease-out}.no-js #loader-wrapper{display:none}.no-js h1{color:#222}#content{margin:0 auto;padding-bottom:50px;width:80%;max-width:978px}.ir{background-color:transparent;border:0;overflow:hidden;*text-indent:-9999px}.ir:before{content:"";display:block;width:0;height:150%}.hidden{display:none!important;visibility:hidden}.visuallyhidden{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.visuallyhidden.focusable:active,.visuallyhidden.focusable:focus{clip:auto;height:auto;margin:0;overflow:visible;position:static;width:auto}.invisible{visibility:hidden}.clearfix:before,.clearfix:after{content:" ";display:table}.clearfix:after{clear:both}.clearfix{*zoom:1}@media print,(-o-min-device-pixel-ratio:5/@media print{*{background:transparent!important;color:#000!important;box-shadow:none!important;text-shadow:none!important}a,a:visited{text-decoration:underline}a[href]:after{content:" (" attr(href) ")"}abbr[title]:after{content:" (" attr(title) ")"}.ir a:after,a[href^="javascript:"]:after,a[href^="#"]:after{content:""}pre,blockquote{border:1px solid #999;page-break-inside:avoid}thead{display:table-header-group}tr,img{page-break-inside:avoid}img{max-width:100%!important}@page{margin:.5cm}p,h2,h3{orphans:3;widows:3}h2,h3{page-break-after:avoid}}';
    var styleGen = "";
    var data = d_option.color_loader;
    var styleElementLoad = document.createElement("div");
    styleElementLoad.setAttribute("id", "loader-wrapper");
    var loadderElement = document.createElement("div");
    loadderElement.setAttribute("id", "loader");
    styleElementLoad.appendChild(loadderElement);
    var sectionLeftElement = document.createElement("div");
    addClass(sectionLeftElement,"loader-section");
    addClass(sectionLeftElement,"section-left");
    styleElementLoad.appendChild(sectionLeftElement);
    var sectionRightElement = document.createElement("div");
    addClass(sectionRightElement,"loader-section");
    addClass(sectionRightElement,"section-right");
    var instantLoader = document.createElement("div");
    var newInstant = [];
    for (var i in data) {
        newInstant[i] = instantLoader.cloneNode();
        addClass(newInstant[i],"loader"+i);
        loadderElement.appendChild(newInstant[i]);
        var randomRotate = parseInt((Math.random() * 360)+1);
        var loader = "#loader .loader"+i+" {" +
            " position: absolute;" +
            " top: 50%;" +
            " left: 50%;" +
            " margin-left: -"+(data[i].size/2)+"px;" +
            " margin-top: -"+(data[i].size/2)+"px;" +
            " border-radius: 50%;" +
            " border-width: 15px;" +
            " border-color: transparent "+data[i].color+" transparent transparent;" +
            " border-style: solid;" +
            " display: block;" +
            " width: "+data[i].size+"px;" +
            " height: "+data[i].size+"px;" +
            " z-index: 9999999;" +
            " -webkit-animation: spin "+data[i].time+"s linear infinite;" +
            " animation: spin "+data[i].time+"s linear infinite;" +
            " }";
        styleGen += loader;
    }

    styleElementLoad.appendChild(sectionRightElement);
    loadCss = styleGen + loadCss;



    var headStyle = document.getElementsByTagName('head')[0];
    var styleElement = document.createElement("style");
    styleElement.setAttribute("id", "preloadCss");
    styleElement.setAttribute('type', 'text/css');
    var browser = navigator.userAgent;
    if (browser.indexOf("MSIE 8") > -1) {
        styleElement.styleSheet.cssText = loadCss;
    } else {
        styleElement.innerHTML = loadCss;
    }
    headStyle.appendChild(styleElement);
    var bodyElement = document.body;
    bodyElement.insertBefore(styleElementLoad, bodyElement.childNodes[0]);
    //document.body.insertBefore(styleElementLoad, document.body);
    if (!hasClass(document.body,"loading_overflow")) {
        addClass(document.body,"loading_overflow");
    }

    if (cssFile != null) {
        var cssFileT = cssFile;
        var loadCssFunction = function() {
            var l = document.createElement('link'); l.rel = 'stylesheet';
            l.href = cssFileT;
            var h = document.getElementsByTagName('head')[0];

            var sheet, cssRules;
            if ( 'sheet' in l ) {
                sheet = 'sheet'; cssRules = 'cssRules';
            }
            else {
                sheet = 'styleSheet'; cssRules = 'rules';
            }
            h.appendChild(l);
            var timeout_id = setInterval( function() {
                try {
                    if ( l[sheet] && l[sheet][cssRules].length ) {
                        clearInterval(timeout_id);
                        clearTimeout(timeout_preload);
                        closePreloadPanel();

                    }
                } catch( e ) {} finally {}
            }, 10 ),
            timeout_preload = setTimeout( function() {
                clearInterval( timeout_id );
                clearTimeout( timeout_preload );
                h.removeChild(l);
                closePreloadPanel();

            }, 15000);
        };
        if (d_option.loadType == "raf") {
            var raf;
            if (typeof(mozRequestAnimationFrame) != "undefined") {
                raf = mozRequestAnimationFrame;
            } else if (typeof(webkitRequestAnimationFrame) != "undefined") {
                raf = webkitRequestAnimationFrame;
            } else if (typeof(msRequestAnimationFrame) != "undefined") {
                raf = msRequestAnimationFrame;
            } else if (typeof(requestAnimationFrame) != "undefined") {
                raf = requestAnimationFrame;
            }
            if (raf) {
                //pattern1
                /*        var loadFunction = function() {
                 var loadedCss = function() {
                 preLoadCss(cssFile);
                 }
                 window.onload = loadedCss;
                 }
                 raf(loadFunction);*/


                //pattern2
                /*        var loadFunction = function() {
                 preLoadCss(cssFile);
                 }
                 raf(loadFunction);*/


                //pattern3
                raf(loadCssFunction);
            } else {
                window.addEventListener('load', function() {
                    loadCssFunction();
                });
            }
        } else if (d_option.loadType == "dom") {
            (document.addEventListener)?document.addEventListener("DOMContentLoaded",loadCssFunction):loadCssFunction();
        } else if (d_option.loadType == "win") {
            window.addEventListener('load', function() {
                loadCssFunction();
            });
        }
    } else {
        closePreload();
    }
}
function closePreloadPanel() {
    addClass(document.body,"loaded");
    removeClass(document.body,"loading_overflow");
    var prv_gg = document.getElementById("prevent_ggsp");
/*    if (typeof(prv_gg) != "undefined") {
        setTimeout(function() {
            prv_gg.parentNode.removeChild(prv_gg);
        },3000);
    }*/
}
function initPreload(cssFile,option) {
    //console.log(1);
    var TempCssFile = cssFile;
    var TempOption = option;
    var bodyElement = document.getElementsByTagName("body");
    if (bodyElement.length > 0) {
        preload(TempCssFile,TempOption);
    } else {
        setTimeout(function() {
            initPreload(TempCssFile,TempOption)
        },1);
    }
}
function closePreload() {
    window.onload = function() {
        closePreloadPanel();
    }
}
function addClass(element,classname) {
    var cn = element.className;
    //test for existance
    if( cn.indexOf( classname ) != -1 ) {
        return;
    }
    //add a space if the element already has class
    if( cn != '' ) {
        classname = ' '+classname;
    }
    element.className = cn+classname;
}
function removeClass(element,classname) {
    var cn = element.className;
    var rxp = new RegExp( "\\s?\\b"+classname+"\\b", "g" );
    cn = cn.replace( rxp, '' );
    element.className = cn;
}
function hasClass(element,classname) {
    var cn = element.className;
    if( cn.indexOf( classname ) > -1 ) {
        return true;
    } else {
        return false;
    }
}