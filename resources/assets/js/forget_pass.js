$(document).ready(function() {
    send_forgot_init();
});
function send_forgot_init() {
    $("#send_req").click(function() {
    	if(validateEmail($('input[name="email"]').val())){
	        $.ajax({
	            url: BASE_LANG + "password/check_reset_request",
	            data: {email:$('input[name="email"]').val()},
	            type: "POST",
	            cache: false,
	            dataType: 'json',
	            success: function(resp){
	                if (resp.status == "success") {
	                    alert(RESET_REQUEST_SENT);
	                } else {
	                    alert(RESET_REQUEST_FAIL);
	                }
	            }
	        });
	    }else{
	    	alert(EMAIL_VALID_MESSAGE);
	    }
    });
}

function validateEmail(email){
	return /^(\s)*(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))(\s)*$/.test(email);
}