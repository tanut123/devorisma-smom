var tempRateClick = false;
$(document).ready(function () {
	changeWidthYoutube();
	$('#relate-list').slick({
		dots: false,
	  	infinite: false,
	  	slidesToShow: 2,
		slidesToScroll: 1,
		variableWidth: true,
		responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: false,
		        dots: false,
		        variableWidth: true
		      }
		    }
		 ]
	});
	$("#total_view").load(BASE_LANG + "page-view/trick_mom/"+page_uid,function(){
		$("#detail-views").css("visibility","visible");
	});
	var data_rate = localStorage.getItem("rating_"+page_uid);
	if(data_rate == null){
		hoverRating();
		clickRating();
	}else{
		// for (var i = 1; i <= data_rate; i++) {
			$(".rate"+data_rate).addClass("active");
			$(".rate"+data_rate).append("<div class='rating-active'></div>");
		// };
	}

	if($('.relate-item').length < 2){
		$('#relate-list').hide()
	}
	checkListFavorite();
});

function changeWidthYoutube(){
	$("#video-panel").find("#youtube-player iframe").attr("width","100%").attr("height","500px");
}

function hoverRating(){
	$(".rating-number").mouseover(function(){
		var nowSelect = $(this);
		var answerVal = nowSelect.data("val");
		var anserSelect = nowSelect.parent().find(".rating-number");
		// anserSelect.removeClass("active-hover");
		// anserSelect.each(function(a,e){
		// 	$(e).addClass("active-hover");
		// 	if((a+1) == answerVal){
		// 		return false;
		// 	}
		// });
		// if($(".rating-number").hasClass("active")){
		// 	nowSelect.append("<div class='rating-active'></div>");
		// }
		// $(".rating-number").unbind( "click" );
		nowSelect.append("<div class='rating-active'></div>");
		nowSelect.addClass("active");
	}).mouseleave(function(event) {
		if (tempRateClick == false){
			ratingClearHover();
		}
	});
}

function ratingClearHover(){
	// $(".rating-number").removeClass("active-hover");
	$(".rating-number").removeClass("active");
	$(".rating-active").remove();
}

function clickRating(){
	$(".rating-number").bind("click touchstart",function(event){
		event.preventDefault();
		tempRateClick = true;
		var nowSelect = $(this);
		var answerVal = nowSelect.data("val");
		nowSelect.nextAll("input").val(answerVal);
		var anserSelect = nowSelect.parent().find(".rating-number");
		nowSelect.append("<div class='rating-active'></div>");
		alert(CONFIRM_RATING);
		setTimeout(function(){
			ratingSend();
			$('.modal').modal("hide");
			$(".rating-number").unbind( "click touchstart" );
			$(".rating-number").unbind("mouseover");
		}, 2000);
	});

	// $(".rating-number").on("touch",function(){
	// 	var nowSelect = $(this);
	// 	var answerVal = nowSelect.data("val");
	// 	var anserSelect = nowSelect.parent().find(".rating-number");
	// 	if(!$(".rating-number").hasClass("active")){
	// 		nowSelect.append("<div class='rating-active'></div>");
	// 		nowSelect.addClass("active");
	// 	}

	// 	// anserSelect.removeClass("active-hover");
	// 	// anserSelect.removeClass("active");
	// 	// anserSelect.each(function(a,e){
	// 	// 	$(e).addClass("active-hover");
	// 	// 	$(e).addClass("active");
	// 	// 	if((a+1) == answerVal){
	// 	// 		return false;
	// 	// 	}
	// 	// });
	// });


	// if(!isMobileDevice()){
	// 	hoverRating();
	// }
}

function ratingSend(){
	if(checkValRating()){
		var rate = $('#inp-rating').val();
		var data = "&page_uid="+ page_uid + "&rate=" + rate;
		$.ajax({
			url: BASE_LANG + "momtip/rating",
			data: "mode=add" + data,
			type: "POST",
			cache: false,
			success: function(){
				localStorage.setItem("rating_"+page_uid,rate);
			}
		});
	}else{

	}
}

function checkValRating(){
	var answer = false;
	if($("#inp-rating").val() != ""){
		answer = true;
	}
	return answer;
}

function manageFavorite(momtip_id){
	var url = BASE_LANG;
	var type = $("#manage-favorite").data("type");
	if(type == "add"){
		url += 'favorite/add';
	}else if(type == "remove"){
		url += 'favorite/remove';
	}
	$.ajax({
		url: url,
		type: 'POST',
		data: {'momtip_id': momtip_id},
		dataType: 'json'
	})
	.success(function(d) {
		changeTypeFavorite(type);
		$("#manage-favorite img").remove();
		$("#manage-favorite span").prepend(typeFavorite(type));
	});
}

function checkListFavorite(){
	if($("#manage-favorite").length > 0 && $("#signout_btn").length > 0){
		$.ajax({
			url: BASE_LANG + 'favorite',
			type: 'GET',
			data: null,
			dataType: 'json'
		})
		.success(function(d) {
			var favorite = d["favorite"];
			var id = page_uid.toString();
			$("#manage-favorite img").remove();
			if($.inArray( id,favorite ) > -1){
				changeTypeFavorite("add");
				$("#manage-favorite span").prepend(typeFavorite("add"));
			}else{
				changeTypeFavorite("remove");
				$("#manage-favorite span").prepend(typeFavorite("remove"));
			}
			if($("#detail-share .list-body-link-bookmark").is(":visible")){
				$("#manage-favorite").hide();
			}else{
				$("#manage-favorite").show();
			}
		})
	}
}

function changeTypeFavorite(type){
	if(type == "add"){
		$("#manage-favorite").data("type","remove");
	}else{
		$("#manage-favorite").data("type","add");
	}
}

function typeFavorite(type){
	if(type == "add"){
		return "<img id='icon-favorite-remove' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAOCAYAAADwikbvAAABYElEQVQoU52SvUtCURjGn/fe6ydFcIsaChKHyI+tIgkaHGzpP2hrsq2oiHJyMogKsyWntv6CWnJwUEKoNi1oEIMcirwQhR/X6z1xpCI1M3u28/L+Hs7h/AgAMkGXUZH1DQBrAMwATgy6GPCspB9TYfdQVaiFACwAKAPYkxVhxxXMqFQH+/UsGIZ50fcQWIiBAs1zEPJyQbBT8nB8kzHablnoMCBiW5SIOAoA5G5hAAqHqwCkf8AaJSOOUwbMdwsTcEaJsHMaAkt1C0MnD3EoceA4B8H35wKG2Ozy7VwdvtgfsdSknhxAg50L2JOovdlmVh9KdZgnvjs2IBnFewDWXwqKmlob9a7fPfOdL5gfPmy6afN1ikEXndy6z/IGmA9jUXufWTVlGowj5MvGisvnz740GvjDHePHNrP0arkE4AaQ1npLU97FHPe6Sd82D7yKThhKleKRxWRdmvRfc5Fa8g7lpHBX7OSEEAAAAABJRU5ErkJggg==' alt='Favorite add'>";
	}else{
		return "<img id='icon-favorite-add' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAOCAYAAADwikbvAAACBUlEQVQoU4WSPWhTURiGny83SZNYEeI/CpYOkpsUFxWLNELB6iA6OHRQUQtKXIz/op06VfC33i4GhaqoQwcHfwaNUDBBCuoiTVMcSgXF3xbEmrQ39+bIKY3Y1NJvfM953nP43lcAch0x/1i4fBY4BQSAB76y0d54fOBrf1fD8pLH7QT2ABPAlfCY52KsI2fLFLi4PIxilTb6dwTVqZD2ah3hU3jUUy/Z7sg5peSCQt0Xr+9o0PCPF+zCCUESKFWPyLBCpUL+0LWia9cqp9QtyF4RdV4yljkKhB3bXdp8+v2PyivPLq1bEPK78YJtZLafefe7ovddXrvE6ze+A2MaLgHezyvy3tZW3FlfrBJ6ezFWfjEdwJGsZT5RsAPUrnhy6PF8cMaK7AR5JPBUMl3RTXhUP/Ainsy3zA+baWArZWkUfTlz3XyOoMG78WT+wFwGGcu8A+xHkY4fy2+bgl9dXR10vbUjIMsEbjUl84erDbKWeVPBIVDfDGe8bvPJj8UpWM/0Fj8AIUFuNyUH2ypnWSvao1AHgYJju2sqqfyF9cXpNg3q6HTuW5JD+15akXs6Vx2Nr2xEdesqpjNgLaZT9YsCdk1uunEPgd26URP+yVhLYvjnzAb+Zzt9PXUB76/ga6ABGHAWFjc2t43oXlfVd47Vvkmt9xUnCzeCNaEjGxJvdZFmzR99hMFXQ74dsQAAAABJRU5ErkJggg==' alt='Favorite remove'>";
	}
}