var pointAnswer = 0;
var limit = 10;
var levelString = "";

$(document).ready(function(){
	$(".btn-answer").click(function(){
		var btn = $(this);
		if(btn.hasClass("correct")){
			++pointAnswer;
		}

		var currentQuestion = $(".question-list.active");
		var currentNum = currentQuestion.data("question");

		if( currentNum < limit ){
			currentQuestion.removeClass("active");
			$(".question-list").eq(currentNum).addClass("active");

			var progress = $(".answer-progress");
			var progressWidth = ((currentNum+1) * 10) + "%";
			progress.css("width",progressWidth);

			var progressText = $(".answer-text");
			progressText.html((currentNum+1)+"/10");
		}else{
			
			$(".answer-progress-bar").hide();
			$(".answer-panel").hide();

			$(".main-title").html("");
			$(".main-subtitle").html("วัดความถนัดศักยภาพความถนัดลูก");

			if(pointAnswer < 6){
				$(".result-answer-panel").eq(0).show();
				// $(".result-btn").text("คลิกเพื่ออ่านบทความ");
				// $(".result-btn-text").text("");
				levelString = $(".result-answer-panel").eq(0).find("input[name='result-answer-level']").val();
			}else if(pointAnswer < 9){
				$(".result-answer-panel").eq(1).show();
				levelString = $(".result-answer-panel").eq(1).find("input[name='result-answer-level']").val();
			}else{
				$(".result-answer-panel").eq(2).show();
				levelString = $(".result-answer-panel").eq(2).find("input[name='result-answer-level']").val();
			}
			$(".result-panel").fadeIn();
		}
	});

	resultAnswer();
	memberWebLoginFacebookAnswer();

	$(".bth-login").click(function(){
		sendAuthenAnswer();
	});
});

function resultAnswer(){
	$(".result-btn").click(function(){
		var member_id = $("input[name='member_web_id']").val();
		var mode = $("input[name='mode']").val();
		// if( pointAnswer > 5){
			if(member_id != ""){
				window.location.href = BASE_URL + "/พรสวรรค์สร้างได้ใน1000วันแรก/ใบรับรอง/" + mode + "/" + levelString;
			}else{
				$(".popup-answer-panel").show();
			}
		// }else{
		// 	var url_detail = "";
			
		// 	if(mode == "ภาษา"){
		// 		url_detail = "แววด้านการสื่อสารและภาษา";
		// 	}else if(mode == "ศิลปะ"){
		// 		url_detail = "ลูกของเรามีแววด้านศิลปะเป็นพิเศษหรือไม่นะ";
		// 	}else if(mode == "ดนตรี"){
		// 		url_detail = "ลูกของเรามีแววด้านดนตรีเป็นพิเศษหรือไม่นะ";
		// 	}else if(mode == "คณิตศาสตร์"){
		// 		url_detail = "ลูกของเรามีแววด้านคณิตศาสตร์เป็นพิเศษหรือไม่นะ";
		// 	}else if(mode == "วิทยาศาสตร์"){
		// 		url_detail = "ลูกของเรามีแววด้านวิทยาศาสตร์และนักประดิษฐ์เป็นพิเศษหรือไม่นะ";
		// 	}else if(mode == "ความเป็นผู้นำ"){
		// 		url_detail = "ลูกของเรามีแววด้านความเป็นผู้นำเป็นพิเศษหรือไม่นะ";
		// 	}

		// 	window.location.href = BASE_URL + "/พรสวรรค์สร้างได้ใน1000วันแรก/" +url_detail;
		// }
	});

	$(".btn-close").click(function(){
		$(".popup-answer-panel").hide();
	});
}


function memberWebLoginFacebookAnswer(){
	$(".login-facebook").click(function(){
		if($(this).find(".fa").length > 0) return false;
		$(this).addClass("active");
		$(this).append('<i class="fa fa-spinner fa-spin"></i>');
		memberWebcheckLoginStateAnswer(function(info){
			callLoginFacebook(info,"login");
		});
	})
}

function callLoginFacebook(info,type){
	$.ajax({
		url: BASE_LANG + 'sign_in/facebook',
		type: 'POST',
		data: {'facebook_id': info.id},
		dataType: 'json'
	})
	.success(function(d) {
		if(d["cmd"] == "y"){
			if(type == "login"){
				var mode = $("input[name='mode']").val();
				window.location.href = BASE_URL + "/พรสวรรค์สร้างได้ใน1000วันแรก/ใบรับรอง/" + mode + "/" + levelString;
			}else{
				alert("Cannot login.");
			}
		}else if(d["cmd"] == "n"){
			addMemberByFacebookAnswer(info);
		}else{
			removeLoadingFacebook();
			alert(d["msg"]);
		}
	});
}

function memberWebcheckLoginStateAnswer(callState) {
  	memberWebFacebookLoginAnswer(function(callInfo){
  		if(callInfo){
  			callState(callInfo);
  		}else{
  			removeLoadingFacebook();
  		}
  	});
}

function memberWebFacebookLoginAnswer(callLogin) {
	FB.login(function(response){
		if (response.authResponse) {
		 	memberWebGetInfoLogin(function(callInfo){
		 		callLogin(callInfo);
		 	});
		} else {
			callLogin(false);
		}
	}, {scope: 'email'});
}

function sendAuthenAnswer() {
	var checkField=true;
	var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	if (!$("input[name='username']").val().match(re)) {
		alert(LOGIN_USERNAME_VALIDATE);
		checkField = false;
	}
	if (checkField == true) {
		$.ajax({
			url: BASE_LANG + 'sign_in/login',
			data: {
				ux:$("input[name='username']").val(),
				up:$("input[name='password']").val(),
				ucsrf:csrf_token
			},
			type: 'POST',
			dataType: 'json',
			success: function(resp) {
				if (resp.status == "success") {
					var mode = $("input[name='mode']").val();
					window.location.href = BASE_URL + "/พรสวรรค์สร้างได้ใน1000วันแรก/ใบรับรอง/" + mode + "/" + levelString;
				} else if (resp.status == "password_expire") {
					alert("รหัสผ่านของคุณหมดอายุ");
					window.open(BASE_LANG + "password/expire","_blank");
				} else {
					if (resp.msg == "login_failed") {
						alert(INVALID_PASSWORD_EMAIL);
					} else {
			            alert(resp.msg);
			        }
				}
			}
		});
	}
}

function addMemberByFacebookAnswer(info){
	var dataPost = {};
	dataPost = {
		"facebook_id" : info.id
		,"displayname" : info.name
		,"member_type" : "2"
		,"email" : info.email
		,"password" : ""
		,"firstname" : info.first_name
		,"lastname" : info.last_name
		,"mobile" : ""
		,"title" : ""
		,"gendar" : ""
		,"birthday" : ""
		,"mode" : "add"
	};

	$.ajax({
		url: BASE_LANG + 'register/register',
		type: 'POST',
		data: dataPost,
		dataType: 'json'
	})
	.success(function(d) {
		if(d["cmd"] == "y"){
			callLoginFacebook(info,"login");
		}else{
			removeLoadingFacebook();
			alert(d["message"]);
		}
	});
}