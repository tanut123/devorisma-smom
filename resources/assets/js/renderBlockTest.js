function consoleHeader() {
    console.log("Header Execute Javascript");
    consoleRaf();
    consoleDOMReady();
}
function consoleFooter() {
    console.log("Footer Execute Javascript");
}
function consoleRaf() {
    var raf;
    if (typeof(requestAnimationFrame) != "undefined") {
        raf = requestAnimationFrame;
    } else if (typeof(mozRequestAnimationFrame) != "undefined") {
        raf = mozRequestAnimationFrame;
    } else if (typeof(webkitRequestAnimationFrame) != "undefined") {
        raf = webkitRequestAnimationFrame;
    } else if (typeof(msRequestAnimationFrame) != "undefined") {
        raf = msRequestAnimationFrame;
    }
    if (raf) {
        raf(function(){
            console.log("Request Animation Frame Active");
        });
    } else {
        console.log("Browser Not Support Request Animation Frame");
    }
}
function consoleDOMReady() {
    if (typeof(document.addEventListener) != "undefined"){
        document.addEventListener("DOMContentLoaded",function() {
            console.log("DOMContentLoaded Active");
        });
    } else {
        console.log("Browser Not Support DOMContentLoaded");
    }
}
function consoleWindowLoaded() {
    if (typeof(window.onload) == "function") {
        var oldFunc = window.onload;
        window.onload = function() {
            console.log("window.load Active");
            oldFunc();
        };
    } else {
        window.onload = function() {
            console.log("window.load Active");
        };
    }
}
consoleHeader();