$("document").ready(function(){
	initSearch();
})

function initSearch(){
	resultHide();
	if(getWord() != ""){
		searchCall(0, SEARCH_LIMIT);
	}else{
		$("#search-detail-panel").hide();
	}
	searchInputClick();
	loadMore();
}

function searchInputClick(){
	$("#btn-submit-search,#btn-submit-search-mobile").click(function(){
		$("#list-result-inner-panel").html("");
		searchCall(0, SEARCH_LIMIT);
	})
	$("#input-search").keypress(function (e) {
	  if (e.which == 13) {
	  	$("#list-result-inner-panel").html("");
		searchCall(0, SEARCH_LIMIT);
		return false;
	  }
	});
}

function getListCount(){
	var lengthList = $("#list-result-inner-panel li").length;
	if(typeof(lengthList) == "undefined"){
		return 0;
	}
	return lengthList;
}

function searchCall(page, limit){
	checkWord(function(check){
		if(check["c"]){
			$("#search-detail-panel").show();
			var elmListMain = $("#list-result-inner-panel");
			elmListMain.append(loadingAjax());
			$.ajax({
				url: BASE_LANG + 'search/list',
				type: 'POST',
				data: { 'word': getWord(), 'page': page, 'limit': limit},
				dataType: 'json'
			})
			.success(function(d) {
				if(page == 0){
					if(d.list != ""){
						elmListMain.html(d.list);
					}else{
						elmListMain.html("<div class='global-loading-text FXregular lighter'>" + DATA_NOT_FOUND + "</div>");
					}
				}else{
					if(d.list != ""){
						elmListMain.append(d.list);
					}
				}
				// replaceWordStyle();
				loadMoreCheck(d.nextpage);
				elmListMain.find(".global-loading").remove();

			}).fail(function() {
				elmListMain.find(".global-loading").remove();
				elmListMain.html(dataNotFound());
				loadMoreCheck('');
				resultHide();
			})
		}else{
			$("#search-detail-panel").hide();
			alert(check["m"]);
		}
	});
}

function checkWord(callback){
	var word =  $("#input-search").val();
	var data = {};
	if(word == ""){
		data["c"] = false;
		data["m"] = CORE_SEARCH_ALERT_NULL;
		return callback(data);
	}else if(word.length < 3){
		data["c"] = false;
		data["m"] = CORE_SEARCH_ALERT_MIN_WORD;
		return callback(data);
	}else{
		data["c"] = true;
		return callback(data);
	}
}

function loadMore(){
	$("#loadmore").click(function(){
		searchCall(getListCount(), SEARCH_LIMIT);
	})
}

function getWord(){
	return $("#input-search").val();
}

function loadMoreCheck(page){
	if(page == '' || page == 0){
		loadMorehide();
	}else{
		loadMoreShow();
	}
}

function resultShow(){
	$("#result-text").show();
}

function resultHide(){
	$("#result-text").hide();
}

function loadMoreShow(){
	$("#loadmore").show();
}
function loadMorehide(){
	$("#loadmore").hide();
}

function replaceWordStyle(){
	$(".list-detail").each(function(){
		var word = getWord();
		var detail = $(this).html();
		var newHtml =  detail.replace(word, "<span class='FBold color-blue'>" + getWord() + "</span>");
		$(this).html(newHtml);
	})
}