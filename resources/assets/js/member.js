checkMobile = false;
LOGIN_STATUS = false;
WIN_WIDTH_GLOBAL = 0;
WIN_HEIGHT_GLOBAL = 0;
var innerWidth = 0;
var innerHeight = 0;
var tempCheckBoxHover = false;


$(document).ready(function(){
	$(".inp-text").placeholder();
	menuCLickMember();
	listStatusClick();
	listStatusChange();
	checkBoxClick();
	windoReisze();
	bodyClick();
	autoScrollTo();
	bannerGiftClick();
	$(".checkbox_box_panel").mouseover(function() {
		tempCheckBoxHover = true;
	});
	$(".checkbox_box_panel").mouseout(function() {
		tempCheckBoxHover = false;
	});
	$(".child_date").datepicker({
	  changeMonth: true,
	  changeYear: true,
	  dateFormat: "dd/mm/yy",
	  maxDate: "+280D",
	  minDate: '0'
	});
});

var temp_width = 0;
function windoReisze(){
	$(window).resize(function(){
		WIN_WIDTH_GLOBAL = $(this).width();
		WIN_HEIGHT_GLOBAL = $(this).height();
		if(temp_width != WIN_WIDTH_GLOBAL){
			if(WIN_WIDTH_GLOBAL < 768){
				checkMobile = true;
				formEvent();
				$(".form-panel-desktop").hide();
				resizePopup();
				islandscape();
			}else{
				checkMobile = false;
				formEvent();
				$(".form-panel-mobile").hide();
				resizePopup();
			}
			temp_width = WIN_WIDTH_GLOBAL;
		}
	});
	var winWidth = $(window).width();
	WIN_WIDTH_GLOBAL = winWidth;
	var winHeight = $(window).height();
	WIN_HEIGHT_GLOBAL = winHeight;
	if(winWidth < 768){
		checkMobile = true;
		formEvent();
		$(".form-panel-desktop").hide();
	}else{
		checkMobile = false;
		formEvent();
		$(".form-panel-mobile").hide();
	}
	resizePopup();

}

function formEvent(type){
	changeActive($(".menu-list-panel.active"));
}

function menuCLickMember(){
	$(".menu-list-panel").click(function(){
		if (tempCheckBoxHover == false) {
			if ($(this).attr("id") == "facebook-register" && document.getElementById('chk1').checked == false) {
				alert(MEMBER_ACCEPT_REQUIRED);
				return false;
			}
			if ($(this).attr("id") == "email-register" && document.getElementById('chk2').checked == false) {
				alert(MEMBER_ACCEPT_REQUIRED);
				return false;
			}
			resetForm();
			changeActive($(this));
			$(".checkbox-panel").addClass("active");
			$(".checkbox-panel").find("input").val("T");
		}
	});
}

function changeActive(elm){
	if (tempCheckBoxHover == false) {
		var nowElm = elm;
		nowElm.addClass("active");
		$(".menu-list-panel").not(nowElm).removeClass("active");
		if(elm.data("type") == "facebook"){
			checkLoginState();
		}
		if(isMobileDevice() || checkMobile == true){
			showFormMobile(elm);
		}else{
			showForm(elm);
		}
	}
}

function showForm(elm){
	var id = elm.attr("id");
	var formID = $("#" + id + "-form");
	$(".form-panel").not(formID).hide();
	$(".form-panel").removeClass("r-active");
	formID.addClass("r-active");
	formID.show();
	validateForm(formID);
}

function showFormMobile(elm){
	var id = elm.attr("id");
	var formID = $("#" + id + "-mobile-form");
	formID.show();
	$(".form-panel").removeClass("r-active");
	formID.addClass("r-active");
	validateForm(formID);
	$(".form-panel").not(formID).hide();
}

function validateForm(elm){
	var registerForm = $(this).parents(".form-register");
		var data =  registerForm.serialize();
		// $(".formErrorContent").hide();
		elm.find(".form-register").validationEngine('detach');
		elm.find(".form-register").validationEngine('attach', {
            promptPosition: "bottomLeft",
            showOneMessage: true,
            autoHideDelay: 5000,
            autoHidePrompt: true,
            addFailureCssClassToField: "error",
            scroll: false,
            'custom_error_messages': {
                '.firstname': {
                    'required': {
                        'message': MEMBER_FIRSTNAME_REQUIRED
                    },
                    'custom[onlyLetterNumberCus]': {
                        'message': MEMBER_FIRSTNAME_VALID
                    }
                },
                '.lastname': {
                    'required': {
                        'message': MEMBER_LASTNAME_REQUIRED
                    },
                    'custom[onlyLetterNumberCus]': {
                        'message': MEMBER_LASTNAME_VALID
                    }
                },
                '.email': {
                    'required': {
                        'message': MEMBER_EMAIL_REQUIRED
                    },
                    'custom[email]': {
                        'message': MEMBER_EMAIL_VALID
                    },
                    'groupRequired':{
                    	'message': EMAIL_MOBILE_SELECT
                    }
                },
                '.status': {
                    'required': {
                        'message': MEMBER_STATUS_REQUIRED
                    }
                },
                '.mobile': {
                    'required': {
                        'message': MEMBER_MOBILE_REQUIRED
                    },
                    'custom[phone]': {
                        'message': MEMBER_MOBILE_VALID
                    },
                    'groupRequired':{
                    	'message': EMAIL_MOBILE_SELECT
                    }
                },
                '.age': {
                    'required': {
                        'message': MEMBER_AGE_REQUIRED
                    }
                },
                '.accept': {
                    'required': {
                        'message': MEMBER_ACCEPT_REQUIRED
                    }
                },
                '.eating_choice': {
	                'required': {
	                    'message': 'กรุณาเลือกตัวเลือก'
	                }
	            },
	            '.eating_text': {
	                'required': {
	                    'message': 'กรุณากรอกยี่ห้อนมบำรุงครรภ์'
	                }
	            },
            },
            onValidationComplete: function(form, status) {
                if (status) {
                    $.ajax({
                        url: BASE_LANG + "member/cmd",
                        data: $(".r-active form").serialize() + "&cmd=register",
                        type: 'POST',
                        dataType: 'json',
                        cache: false,
                        success: function(r) {
                            if (r.c == "y") {
                            	createdim(popupMessage());
                            } else if(r.c == "re") {
                            	createdim(popupRepeatMessage());
                            }else{
                            	createdim(popupErrorMessage());
                            }
                        }
                    });
                }
            }
        });
	elm.find(".btn-register").unbind("click");
	elm.find(".btn-register").click(function(){
		elm.find(".form-register").find(".formErrorContent").show();
		elm.find(".form-register").submit();
	})
}

function listStatusClick(){
	$(".label-list-text,.label-list-text span:eq(0)").click(function(){
		listStatusEvent();
	})
}

function listStatusEvent(){
	if($(".status-list-panel").is(":visible")){
		$(".status-list-panel").hide();
	}else{
		$(".status-list-panel").show();
	}
}

function listStatusChange(){
	$(".status-list").click(function(){
		nowElm = $(this);
		var p_id = nowElm.parents("form").attr("id");
		$(".status-list").removeClass("active");
		nowElm.addClass("active");
		var textStatus = nowElm.text().replace("• ", "");
		$(".label-list-text span").text(textStatus);
		$(".status-mom").val(nowElm.data("value"));
		$(".child_date").val("");
		$(".antenatal_place").val("");
		$(".box-child_date").hide();
		$(".panel-eating").html("");
		if(nowElm.data("value") == 2){
			createdim(popupAlertstatus());
			resizePopup();
		}else if(nowElm.data("value") == 1){
			$(".box-child_date").show();
			CreateEatingPanel(1,p_id);
		}
		islandscape();
	})
}

function CreateEatingPanel(status,pid){
	var status = (status == "1")?"no-margin":"";
	var e = '<div class="panel-eating-choice '+status+'">';
		e += '<span class="data-left FLighter">คุณแม่ทานนมบำรุงครรภ์หรือไม่ :</span>';
		e += '<span class="clearfix visible-xs"></span>';
		e += '<span class="data-right">';
		e += '<div class="radio-box ">';
		e += '<input class="input-radio" type="radio" name="eating_choice_'+pid+'" id="eat1_'+pid+'" value="T"><label for="eat1_'+pid+'" class="text-radio FLighter" ><span></span>ทาน</label>';
		e += '</div>';
		e += '<div class="radio-box ">';
		e += '<input class="input-radio" type="radio" name="eating_choice_'+pid+'" id="eat2_'+pid+'" value="F"><label for="eat2_'+pid+'" class="text-radio FLighter" ><span></span>ไม่ทาน</label>';
		e += '</div>';
		e += '<input type="text" class="hidden-value eating_choice validate[required]" name="eating_choice" id="eating_'+pid+'" value="">';
		e += '</span>';
		e += '<span class="clearfix"></span>';
		e += '</div>';
		e += '<div class="panel-eating-text"></div>';
	$("form#"+pid+" .panel-eating").html(e);
	selectEatingChoice(pid);
}

function selectEatingChoice(pid){
	$('#eat1_'+pid).click(function(){
		$('#eating_'+pid).val($(this).val());
		var html = '<span class="content-list col-xs-12 col-sm-6 col-md-6 col-lg-6 col-no-padding">';
			html += '<label class="label-placeholder FLighter" for="">ยี่ห้อนมบำรุงครรภ์</label>';
			html += '<input type="text" name="eating_text" class="inp-text FLighter eating_text validate[required]">';
			html += '</span>';
		$(".panel-eating-text").html(html);
		$(".inp-text").placeholder();
	});
	$('#eat2_'+pid).click(function(){
		$('#eating_'+pid).val($(this).val());
		$(".panel-eating-text").html("");
	});
}

function popupAlertstatus(){
	var html = "<div id='popup-panel' class='bg-popup-alert'>";
			html +="<div id='popup-inner-panel'>";
				html +="<div id='popup-top-panel'>";
					html +="<div id='popup-inner-top-panel'>";
						html +="<div id='close-popup'>";
						html +="</div>";
						html +="<div id='text-popup-left'>";
							if(LANG == "th"){
								html +="<span id='text-popup1' class='color-gray FLighter'>ขอขอบคุณสำหรับความไว้วางใจ สู่ครอบครัว เอส-มัมคลับ</span>";
								html +="<span id='text-popup2' class='color-gray FLighter'><p><img src='"+BASE_CDN+"/images/member/mom-with-baby.png' class='logo-mom visible-xs logo-lanscape'>S-Momclub สนับสนุนการทานนมแม่สำหรับลูกน้อยวัย 0-12 เดือน ตามนโยบายขององค์การอนามัยโลก เราขอร่วมเป็นหนึ่งและมอบกำลังใจให้คุณแม่ เพื่อให้ลูกน้อยได้ดื่มนมแม่ได้นานที่สุด คุณแม่จะได้รับสิทธิพิเศษจากการสมัครสมาชิก เช่น SMS เคล็ดลับส่งเสริมพัฒนาการ นิตยสารราย 3 เดือน และบริการให้คำปรึกษาด้านโภชนาการและพัฒนาการเด็ก จากทีมงานพยาบาลและผู้เชี่ยวชาญของ S-Momclub <img src='"+BASE_CDN+"/images/member/mom-with-baby.png' class='logo-mom logo-portait visible-xs'> ผ่านหลากหลายช่องทาง เช่น โทรศัพท์ เฟสบุ๊ค ไลน์ เว็บไซต์ โมบายแอฟ<span id='text-popup3' class='color-blue FBold'> ยกเว้น การเยี่ยมเยียนสุขภาพบุตรทางโทรศัพท์ จนกว่าน้องจะมีอายุครบ 1 ขวบปี</span></p></span>";
								html +="<span id='text-popup4' class='color-gray FLighter'><p>เอส-มัมคลับขอร่วมเป็นส่วนหนึ่งในการสร้างศักยภาพให้กับลูกน้อยเพื่อการเจริญเติบโตที่สมวัยแข็งแรง</p></span>";
								html +="<span id='btn-accept-condition' class='color-gray FLighter'>รับทราบและยอมรับเงื่อนไข</span>";
							}
						html +="</div>";
					html +="</div>";
				html +="</div>";
			html +="</div>";
		html +="</div>";

	return html;
}

function popupMessage(){
	var html ="<div id='popup-panel' class='bg-popup-message message-panel-ok'>";
			html +="<div id='popup-inner-panel'>";
				html +="<div id='close-popup'>";
				html +="</div>";
				if(LANG == "th"){
					html +="<span class='text-bold FXregular color-gold message-ok'>ยินดีต้อนรับสู่ครอบครัว <img src='" + BASE_CDN  + "/images/header/logo-smom.svg' alt='logo-smom' class='logo-s-mom-popup'></span>";
					html +="<span class='text-lighter FLighter color-gray message-ok-upper'>ทางบริษัทได้รับข้อมูลของท่านแล้ว<br/> ภายใน 30 วัน ทีมงานจะติดต่อกลับเพื่อยืนยันการจัดส่ง welcome pack ขอบคุณที่ให้เราได้ร่วมเป็นส่วนหนึ่งในการพัฒนาศักยภาพลูกน้อยของคุณ </span>";
				}else{
					html +="<span class='text-bold FXregular color-gold'>ยินดีต้อนรับคุณแม่ S-MomClub </span>";
					html +="<span class='text-lighter FLighter color-gray'>ทางเราได้รับข้อมูลแล้ว เจ้าหน้าที่จะทำการติดต่อกลับ<br/>ตามข้อมูลที่กรอกไว้ค่ะ</span>";

				}
			html +="</div>";
		html +="</div>";
	return html;
}

function popupRepeatMessage(){
	var html ="<div id='popup-panel' class='bg-popup-message'>";
			html +="<div id='popup-inner-panel'>";
				html +="<div id='close-popup'>";
				html +="</div>";
				if(LANG == "th"){
					// html +="<span class='text-bold FXregular color-gold'>ยินดีต้อนรับคุณแม่ S-mom club </span>";
					html +="<span class='text-lighter FLighter color-gray'>ท่านมีข้อมูลอยู่ในระบบแล้วค่ะ</span>";
				}else{
					html +="<span class='text-lighter FLighter color-gray'>ข้อมูลของท่านมีอยู่ในระบบแล้วค่ะ</span>";

				}
			html +="</div>";
		html +="</div>";
	return html;
}

function popupErrorMessage(){
	var html ="<div id='popup-panel' class='bg-popup-message'>";
			html +="<div id='popup-inner-panel'>";
				html +="<div id='close-popup'>";
				html +="</div>";
				if(LANG == "th"){
					html +="<span class='text-lighter FLighter color-gray'>เกิดข้อผิดพลาดระหว่างบันทึกข้อมูล กรุณาลองใหม่อีกครั้งค่ะ</span>";
				}else{
					html +="<span class='text-lighter FLighter color-gray'>เกิดข้อผิดพลาดระหว่างบันทึกข้อมูล กรุณาลองใหม่อีกครั้งค่ะ</span>";

				}
			html +="</div>";
		html +="</div>";
	return html;
}

function acceptCondition(){
	if(typeof($("#btn-accept-condition").length) != "undefined"){
		$("#btn-accept-condition").click(function(){
			removeDim();
			var p = $(".r-active").find("form").attr("id");
			CreateEatingPanel(2,p);
		});
	}
}

function closePopup(){
	if(typeof($("#close-popup").length) != "undefined"){
		$("#close-popup").click(function(){
			clearStatus();
			removeDim();
		})
	}
}

function checkBoxClick(){
	$(".checkbox-panel").click(function(){
		nowElm = $(this);
		if(nowElm.hasClass("active")){
			nowElm.removeClass("active");
			nowElm.find("input").val("");
		}else{
			nowElm.addClass("active");
			nowElm.find("input").val("T");
		}
	})
}

function acceptCheck(field, rules, i, options){
	if(field.val() != "T"){
		return options.allrules.required.alertText;
	}
}
function ageCheck(field, rules, i, options){
	if(field.val() != "T"){
		return options.allrules.required.alertText;
	}
}

function resetForm(){
	$(".form-register").find("input[type=text], textarea").val("");
	$(".checkbox-panel").each(function(){
		$(this).removeClass("active");
		$(this).find("input").val("");
	});
	resetStatus();
}

function resetStatus(){
	$(".status-list").removeClass("active");
	$(".label-list-text span").text(MEMBER_STATUS);
	$(".status-mom").val("");
}
// Facebook API

function getInfo() {
    FB.api('/me', function(response) {
    	if(typeof(response.error) != "undefined"){
    		removeDim();
    		checkLoginState();
    	}else{
	    	var elm = $(".form-panel:visible").find(".form-register");
	    	elm.find(".firstname").val(response.first_name);
	    	elm.find(".lastname").val(response.last_name);
	    	elm.find(".email").val(response.email);
    		removeDim();
    	}
    });
  }


function statusChangeCallback(response) {

    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      getInfo();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      facebookLogin();
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
	    facebookLogin();
    }
  }

function checkLoginState() {
	FB.getLoginStatus(function(response) {
	  createdim(loadingAjax());
	  statusChangeCallback(response);
	});
}

function facebookLogin() {
	FB.login(function(response){
		if (response.authResponse) {
		 	getInfo();
		} else {
		 	hideFacebookActiveForm();
		 	removeDim();
		}
	}, {scope: 'email'});
}

function createdim(message){
	$("body").append("<div id='dim-panel'>" + message + "<div id='dim-inner-panel'></div></div>").addClass("nav-active");
	dimClick();
	closePopup();
	acceptCondition();
}
function removeDim(){
	if($(".bg-popup-message").is(":visible")){
		window.location.reload();
	}
	$("#dim-panel").remove();
	$("body").removeClass("nav-active");
}
function dimClick(){
	$("#dim-inner-panel").click(function(){
		clearStatus();
		removeDim();
	})
}

function clearStatus(){
	if($("#popup-panel.bg-popup-alert").length >0){
		resetStatus();;
	}
}

function hideFacebookActiveForm(){
	if(WIN_WIDTH_GLOBAL < 768){
		var id = $(".menu-list-panel.active").attr("id");
		var formID = $("#" + id + "-mobile-form");
		formID.hide();
	}else{
		var id = $(".menu-list-panel.active").attr("id");
		var formID = $("#" + id + "-form");
		formID.hide();
	}
	$(".menu-list-panel.active").removeClass("active");
}

function resizePopup(){
	var heightPopup = 0;
	// if(WIN_WIDTH_GLOBAL > WIN_HEIGHT_GLOBAL){
	// 	heightPopup = WIN_HEIGHT_GLOBAL - 200;
	// }else{
		heightPopup = WIN_HEIGHT_GLOBAL - 200;
	// }

	$(".bg-popup-alert").find("#popup-inner-top-panel").height(heightPopup);
}

function bodyClick(){
	$('body').click(function(e) {
	    var target = $(e.target);
	    if(!target.is('.label-list-text,.label-list-text *') && !target.is('.status-list-panel')) {
	       if ( $('.status-list-panel').is(':visible') ) $('.status-list-panel').hide();
	    }
	});
}

function bannerGiftClick(){
	if(typeof($(".gift_link_text").length) != "undefined"){
		$(".gift_link_text").click(function(){
			autoScrollTo(true);
		})
	}
	$(".footer_link_thin").click(function(){
		setTimeout(function(){
			autoScrollTo(true);
		},200);
	});
	$(".menu-mobile-list-child a").click(function(){
		setTimeout(function(){
			autoScrollTo(true);
		},200);
	})
}

function autoScrollTo(clickEvent){
	var url = window.location.href;
	var top = 0;
	if(url.indexOf("#privilege") > 0){
		top = $("#privilege-to").offset().top;
	}else if(url.indexOf("#register") > 0){
		top = $("#register-to").offset().top;
	}
	if(clickEvent){
		$("html,body").animate({scrollTop:top}, 500);
	}else{
		setTimeout(function(){
			$("html,body").animate({scrollTop:top}, 500);
		},2000);
	}
}


function islandscape(){
	var lanscape = ( WIN_HEIGHT_GLOBAL > WIN_WIDTH_GLOBAL) ? false:true;
	if(lanscape){
		$(".logo-lanscape").removeClass("hide-logo");
		$(".logo-lanscape").addClass("show-logo");
		$(".logo-portait").addClass("hide-logo");
	}else{
		$(".logo-portait").removeClass("hide-logo");
		$(".logo-portait").addClass("show-logo");
		$(".logo-lanscape").addClass("hide-logo");
	}
}