$(document).ready(function(){
	$('.trick-mom-body-list').slick({
	  	infinite: true,
	  	slidesToShow: 1,
		slidesToScroll: 1,
		variableWidth: true,
		responsive: [
		    {
		      breakpoint: 767,
		      settings: {
		        dots: true,
		        variableWidth: false,
		      }
		    }
		 ]
	});
	setHeightDescription();
});

function changePositionImageFooter(){
	var elmPanel = $(".list-footer");
	var imgBanner = $(".list-footer-img-desktop");
	var heightElm = elmPanel.height();
	imgBanner.load(function() {
		var heightImg = imgBanner.height();
		var difHeight = heightImg-heightElm;
		if(difHeight > 0){
			imgBanner.css({"top": -difHeight/2});
		}
	});

	var heightImg = imgBanner.height();
	var difHeight = heightImg-heightElm;
	if(difHeight > 0){
		imgBanner.css({"top": -difHeight/2});
	}
}

function setHeightDescription(){
	$(".list-item-momtip").each(function(a,b){
		var this_item = $(b);
		var description = this_item.find(".list-body-title .txt-black");
		description.JJJud({"max_rows":2});
	});
}