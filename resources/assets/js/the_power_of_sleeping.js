$(document).ready(function(){
	$('#data1').click(function(){
		changeContent('1');
	});
	$('#data2').click(function(){
		changeContent('2');
	});
	$('#data3').click(function(){
		changeContent('3');
	});

	setTimeout(function(){
		$('.hl-banner-secfirst').slick({
		  	infinite: true,
		  	slidesToShow: 1,
			slidesToScroll: 1,
			variableWidth: true,
			autoplay: true,
			responsive: [
			    {
			      breakpoint: 767,
			      settings: {
			      	slidesToShow: 1,
			        variableWidth: false,
			      	infinite: true
			      }
			    }
			 ]
		});
		$('.hl-banner-sectwo').slick({
		  	infinite: true,
		  	slidesToShow: 1,
			slidesToScroll: 1,
			variableWidth: true,
			autoplay: true,
			autoplaySpeed: 6000,
			responsive: [
			    {
			      breakpoint: 767,
			      settings: {
			      	slidesToShow: 1,
			        variableWidth: false,
			        infinite: true,
			        autoplaySpeed: 6000
			      }
			    }
			 ]
		});
		$('.video_slick').slick({
			dots: false,
			slidesToShow: 3,
			slidesToScroll: 3,
			responsive: [
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots: false
					}
				}
			]
		});

		initVideoAction();
	},500);

	setHeightDescription();
});

function changeContent(data){
	var html = "";
	if(data == '1'){
		html += ' <div class="panel-1"><div class="text-1 FLighter color-gold">ข้อมูลโภชนาการ</div><div class="text-2 FLighter color-gold">S-26 พีอี โกลด์</div><p class="text-3 FLighter"><span class="bold">มี 9 สารอาหารสำคัญ</span> ได้แก่ แคลเซียมสูง วิตามินเอสูง วิตามินซีสูง วิตามินดีสูง เหล็กสูง มีไอโอดีน สังกะสี วิตามินบี 1 และวิตามินอี<br class="dtnl"></p><p class="text-3 FLighter"><span class="bold">พร้อม 9 สารอาหารเพิ่มเติม</span>  ได้แก่ ดีเอชเอ จากสาหร่ายทะเลเซลล์เดียว 3.6 มก. เอเอ 5.2 มก. อินอซิทอล 15 มก. ลูทีน 40 มคก. <br class="dtnl">ทอรีน 9.4 มก. โคลีน 60 มก. แอล-คาร์นิทีน 3.4 มก. นิวคลีโอไทด์ 5 ชนิด 5.2 มก. ต่อแก้ว และผสมใยอาหารจากธรรมชาติ</p></div>';
	}else if(data == '2'){
		html += '<div class="panel-2"><div class="content-left hidden-xs col-xs-12 col-sm-4 col-md-4 col-lg-4 col-no-padding"><img class="img1" src="' + BASE_CDN + '/images/product/pe_gold/s26-pe-gold-howto.png" alt="s26-pe-gold-milk"></div><div class="content-right col-xs-12 col-sm-8 col-md-8 col-lg-8 col-no-padding"><div class="content-wrap-inner"><span class="text-1 color-gold FLighter">วิธีเตรียมนม</span><span class="text-2 FLighter">S-26 พีอี โกลด์</span><img class="img1 visible-xs img-responsive" src="' + BASE_CDN + '/images/product/pe_gold/s26-pe-gold-howto.png" alt="s26-pe-gold-milk"><span class="text-5 color-gray-light FLighter">ใช้ช้อนตวงที่มีในกล่อง เพื่อตวงนมผง 5 ช้อนตวง (44 กรัม) เติมลงในแก้วที่มีน้ำอุ่นหรือน้ำต้มสุก<br class="dtnl">ที่เย็นแล้ว ปริมาณ 175 มิลลิลิตร คนจนละลายดี จะได้นม 1 แก้ว ปริมาณ 200 มิลลิลิตร<br class="dtnl">ไม่ควรผสมนมให้เข้มข้นขึ้นโดยเพิ่มปริมาณนมผง หรือผสมนมให้เจือจางลงโดยเพิ่มปริมาณน้ำ<br class="dtnl">ในกรณีที่แพทย์มิได้ให้คำแนะนำเป็นอย่างอื่น โปรดใช้สัดส่วนที่แนะนำต่อไปนี้</span><span class="table-panel color-gray FLighter"><ul class="table-title"><li class="text-3 table-list table-first"><div style="padding-top: 4px;">ปริมาณน้ำอุ่นหรือ <br>น้ำต้มสุกที่เย็นแล้ว</div></li><li class="text-3 table-list table-second">จำนวนช้อนตวงต่อ 1  แก้ว</li><li class="text-3 table-list table-third">จำนวนแก้วต่อวัน</li><li class="clearfix"></li></ul><ul class="table-dtail"><li class="text-4 table-list table-first">175 มล.</li><li class="text-4 table-list table-second">5</li><li class="text-4 table-list table-third">2</li><li class="clearfix"></li></ul><div class="clearfix"></div></span><span class="text-5 color-gray-light FLighter">1 ช้อนตวง  =  8.8 กรัม</span><span class="text-5 color-gray-light FLighter">ข้อแนะนำ<ul><li>ดื่มเพียงวันละ 2 แก้ว</li><li>ควรเตรียมนมเพียงครั้งละ 1 แก้วเพื่อรับประทานทันที และปฏิบัติตามข้อแนะนำอย่างเคร่งครัด</li><li>ควรให้เด็กรับประทานนมนี้จากแก้ว</li><li>หากทานน้ำนมที่ผสมแล้วไม่หมด ให้ทิ้งน้ำนมที่เหลือทั้งหมด</li></ul></span></div></div><div class="clearfix"></div></div>';
	}else if(data == '3'){
		html += '<div class="panel-3"><div class="content-left col-xs-12 col-sm-6 col-sm-pull-6 col-md-6 col-md-pull-6 col-lg-6 col-lg-pull-6 col-no-padding"><div class="content-wrap-inner"><span class="text-1">การเก็บรักษา</span><img class="img1 img-responsive visible-xs" src="' + BASE_CDN + '/images/product/pe_gold/s26-pe-gold-save.png" alt="s26-pe-gold-save"><ul class="list FThin"><li>ถุงอลูมิเนียมที่ยังไม่ได้เปิดใช้ให้เก็บไว้ในที่แห้งและเย็น</li><li>ถุงที่เปิดใช้แล้ว ต้องพับปากถุงให้ปิดสนิทหลังจากใช้ทุกครั้งเพื่อให้นมผง<br class="dtnl">ใหม่เสมอ และเก็บไว้ที่แห้งและเย็น</li><li>ควรใช้ให้หมดภายใน 4  สัปดาห์ หลังจากเปิดถุงแล้ว หลีกเลี่ยงการเก็บ<br class="dtnl">ในที่อุณหภูมิร้อนจัด 	หรือเย็นจัดเป็นระยะเวลานาน</li></ul></div></div><div class="content-right hidden-xs col-xs-12 col-sm-6 col-sm-push-6 col-md-6 col-md-push-6 col-lg-6 col-lg-push-6 col-no-padding"><img class="img1 " src="' + BASE_CDN + '/images/product/pe_gold/s26-pe-gold-save.png" alt="s26-pe-gold-save"></div><div class="clearfix"></div></div>';
	}
	if(!$('#article-information-panel').is(":animated")){
		$('#article-information-panel').hide().html(html).fadeIn('slow');
	}
};

function setHeightDescription(){
	$(".list-item-momtip").each(function(a,b){
		var this_item = $(b);
		var height = this_item.find(".list-body-title .txt").innerHeight();
		var description = this_item.find(".list-body-desc .txt");
		if(description.text().trim() != ""){
			var heightMod = Math.round(height / 30);
			if(heightMod == 1){
				description.JJJud({"max_rows":2});
			}else if(heightMod == 2){
				description.JJJud({"max_rows":1});
			}else{
				description.hide();
			}
		}
	});
}

function initVideoAction() {

	$(".close_video").mouseup(function () {

		$(".video_iframe").attr("src","");
		$("#video_light_box").css("display","none");

	});
	$(".video_panel").mouseover(function() {
		tempVideoHover = true;
	});
	$(".video_panel").mouseout(function() {
		tempVideoHover = false;
	});
	$(".play_video_icon").on('click touchstart', function () {
		var video_data = $(this).attr("video_data");
		if ($(this).attr("type") == "youtube") {
			var new_iframe = $("<iframe>",{allowfullscreen:"",frameborder:"0",src:"https://www.youtube.com/embed/"+video_data+"?enablejsapi=1&autoplay=1"}).addClass("video_iframe");
			$(".video_panel").html("");
			$(".video_panel").append(new_iframe);
			$("#video_light_box").css("display","table");
		} else {
			var new_video = $("<video>",{controls:"true",autoplay:"autoplay"}).addClass("video_iframe");
			var new_source = $("<source>",{type:"video/mp4"}).attr("src",video_data);
			new_video.append(new_source);
			$(".video_panel").html("");
			$(".video_panel").append(new_video);
			$("#video_light_box").css("display","table");
		}
		//<iframe class="video_iframe" id='video_light_box_iframe' allowfullscreen="" frameborder="0" src="https://www.youtube.com/embed/{{$dt_video_hl}}?enablejsapi=1&autoplay=1"></iframe>
	});
	$(".video_align").on('click touchstart', function () {
		if (tempVideoHover == false) {
			$(".video_iframe").attr("src", "");
			$("#video_light_box").css("display", "none");
		}
	});
}
(function($){
    $.fitText = function(el, options){
    }
    $.fn.fitText = function(options){
        return true;
    };

})(jQuery);