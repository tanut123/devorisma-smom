var temp_media_school_book_mode = "game";
$(document).ready(function(){
	setTimeout(function() {
		$('#list-menu-top').slick({
			dots: true,
			infinite: false,
			slidesToShow: 4,
			slidesToScroll: 1,
			variableWidth: true,
			responsive: [
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						infinite: false,
						dots: false,
						variableWidth: false
					}
				}
			]
		});
	},300);

	menulistClick(true);
	slideChangeMobile();
	$(window).resize(function () {
        menulistClick(false);
		slideChangeMobile();
    });
});


function arrowMobileActive(elm){
    temp_media_school_book_mode = elm.data('name');
	var type = elm.data('name');
	$(".active-arrow img").attr("src",BASE_CDN + "/images/media/school_book/" + type + "-active.png").attr("alt",type);
}


function slideChangeMobile(){
	$('#list-menu-top').on('afterChange', function(event, slick, currentSlide, nextSlide){
	  	$(".list-menu-item").eq(currentSlide).click();
	});
}

function menulistClick(first){
	$(".list-menu-item").unbind('click');
	$(".list-menu-item").click(function(){
		var thisElm = $(this);
		thisElm.addClass("list-menu-item-active");
		$(".list-menu-item").not(thisElm).removeClass("list-menu-item-active");
		changeContentID(thisElm);
		callAjaxContent(thisElm);
		arrowMobileActive(thisElm);
	});
	if(first){
		$(".list-menu-item").eq(0).click();
	}
}

function changeContentID(elm){
	var id = elm.attr("id");
	$(".container-wrapper").attr("id",id + "-panel");
}

function callAjaxContent(elm){
	var type = elm.data('name');

	$(".container-detail").html(loadingAjax());
	$(".container-wrapper-bottom").hide();
	$.ajax({
		url: BASE_LANG + 'school_book/'+type,
		type: 'POST',
		dataType: 'html',
		success: function(d) {
			if(d != ""){
				if($("#media_music_player").size() > 0){
					document.getElementById("media_music_player").pause();
					document.getElementById("media_music_player").currentTime = 0;
				}
				$(".container-detail").html('');
				$(".container-detail").append(d);
	            if (type=='music') {
	                init_sound_control();
	            }
			}else{
				$(".container-detail").html(DATA_NOT_FOUND);
			}
			$(".container-wrapper-bottom").show();

		}
	});

}

