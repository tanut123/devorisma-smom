var windowMode = ($(window).width()>=768)?true:false;
var currentMode = ($(window).width()>=768)?true:false;
$(document).ready(function () {
    var window_height = $(window).height();
    var window_width = $(window).width();

    $("#globalsearch, #inp-search").placeholder();

    $(window).resize(function () {
        var window_width = $(window).width();
        var window_height = $(window).height();
        if (window_width >= 768) {
            $("#nav-menu-mobile-close").click();
        	currentMode = true;
        }else{
        	currentMode = false;
        }

        setHeightMenu(window_height);
        setBoxSearchWidth();

        if(windowMode != currentMode){
			windowMode = currentMode;
			switchMenuLine();
		}

    });
    var nua = navigator.userAgent;
    var is_android = ((nua.indexOf('Mozilla/5.0') > -1 && nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1) && (nua.indexOf('Chrome') > -1));

    setHeightMenu(window_height);
    menuListClick();
    searchClick();
    menuClick();
    searchBoxClick();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	$("#trick-mom-body-list .list-body .list-body-content-panel a.list-body-title").JJJud({max_rows: 3, waitElement: ".loaded"});
	webMemberAuthenInit();
	memberWebLoginFacebook();

	switchMenuLine();
	$('.close-line-box').click(function(){
		$('.line-float-box').fadeOut(300);
		if($(window).width()>=768){
			$("#login_bar").css("top",0);
		}else{
			$("#login_bar").css("top","110px");
		}
	});

});
function webMemberAuthenInit() {
	$("#pwd_inp").keypress(function(event) {
		if (!event) {
			event = window.event;
		}
		if (event.keyCode == 13) {
			if(!$(".modal").is(":visible")){
				sendAuthen();
			}else{
				$(".modal").modal('hide');
			}
		}
	});
	$("#sign_in_case").click(function() {
		if ($(".sign_in").hasClass("sign_in_hover")) {
			$(".sign_in").removeClass("sign_in_hover");
			$(".case1").removeClass("login_btn_panel");
			$(".case2").removeClass("login_input_panel");
		} else {
			$(".sign_in").addClass("sign_in_hover");
			$(".case1").addClass("login_btn_panel");
			$(".case2").addClass("login_input_panel");
		}
	});
	$("#sign_in_btn").click(function() {
		sendAuthen();
	});
	$("#signout_btn").click(function() {
		$.ajax({
			url: BASE_LANG + 'sign_in/logout',
			data: {
				ux:$("#email_inp").val(),
				up:$("#pwd_inp").val(),
				ucsrf:csrf_token
			},
			type: 'POST',
			dataType: 'json',
			success: function(resp) {
/*				$("#login_case_panel").html(resp.html);
				webMemberAuthenInit();*/
				location.reload();
			}
		});
	});
	$(".user_display").click(function() {
		if ($(".case3 .right_side").hasClass("show_menu")) {
			$(".case3 .right_side").removeClass("show_menu");
		} else {

			$(".case3 .right_side").addClass("show_menu");
		}
	});
}
function sendAuthen() {
	var checkField=true;
	var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	if (!$("#email_inp").val().match(re)) {
		alert(LOGIN_USERNAME_VALIDATE);
		checkField = false;
	}
/*	if (checkField == true && $("#pwd_inp").val().length < 10) {
		alert(LOGIN_PASSWORD_VALIDATE);
		checkField = false;
	}*/
	if (checkField == true) {
		$.ajax({
			url: BASE_LANG + 'sign_in/login',
			data: {
				ux:$("#email_inp").val(),
				up:$("#pwd_inp").val(),
				ucsrf:csrf_token
			},
			type: 'POST',
			dataType: 'json',
			success: function(resp) {
				if (resp.status == "success") {
					location.reload();
				} else if (resp.status == "password_expire") {
					window.location.href = BASE_LANG + "password/expire";
				} else {
					if (resp.msg == "login_failed") {
						alert(INVALID_PASSWORD_EMAIL);
					} else {
			            alert(resp.msg);
			        }
				}
			}
		});
	}
}

function searchBoxClick(){
	$("#icon-search").click(function(){
		checkWordCore("#inp-search",function(check){
			if(check["c"]){
				$("#submit-search").submit();
			}else{
				alert(check["m"]);
			}
		});
	});
	$("#btn_search").click(function(){
		checkWordCore("#globalsearch", function(check){
			if(check["c"]){
				$("#search-desktop-form").submit();
			}else{
				alert(check["m"]);
			}
		});
	});
	$("#inp-search").keypress(function (e) {
	  if (e.which == 13) {
		checkWordCore("#inp-search", function(check){
			if(check["c"]){
				$("#submit-search").submit();
			}else{
				alert(check["m"]);
			}
		});
		return false;
	  }
	});
	$("#globalsearch").keypress(function (e) {
	  if (e.which == 13) {
		checkWordCore("#globalsearch", function(check){
			if(check["c"]){
				$("#search-desktop-form").submit();
			}else{
				alert(check["m"]);
			}
		});
		return false;
	  }
	});
}

function menuListClick(){
	// if(isMobileDevice()){
	// 	$(".menu-list-mobile a").hover(function(){
	// 		var elmChlid = $(this).parent(".menu-list-mobile").next(".menu-mobile-list-child");
	// 		if(elmChlid.is(":visible")){
	// 			elmChlid.slideUp();
	// 			$(this).find(".arrow-normal").removeClass("active");
	// 			$(this).find(".arrow-active").removeClass("active");
	// 		}else{
	// 			elmChlid.slideDown();
	// 			$(this).find(".arrow-normal").addClass("active");
	// 			$(this).find(".arrow-active").addClass("active");
	// 		}
	// 		$(".menu-mobile-list-child").not(elmChlid).slideUp();
	// 	})
	// }else{
	$(".menu-list-mobile a").click(function(){
		var elmChlid = $(this).parent(".menu-list-mobile").next(".menu-mobile-list-child");
		$(".menu-list-mobile a").find(".arrow-active").removeClass("active");
		$(".menu-list-mobile a").find(".arrow-normal").removeClass("active");
		if(elmChlid.is(":visible")){
			elmChlid.slideUp();
			$(this).find(".arrow-normal").removeClass("active");
			$(this).find(".arrow-active").removeClass("active");
		}else{
			elmChlid.slideDown();
			$(this).find(".arrow-normal").addClass("active");
			$(this).find(".arrow-active").addClass("active");
		}
		$(".menu-mobile-list-child").not(elmChlid).slideUp();
	});
	$(".menu-mobile-list-child a").click(function(){
		$("#header-menu-mobile-btn").removeClass("active");
		$("#menu-mobile-list").hide();
		bodyClearFixed();
	});
	// }
}

function setHeightMenu(window_height){
	var height = window_height-$("#menu-mobile").height();
	$("#menu-mobile-list-panel").height(height);
}

function setBoxSearchWidth(){
	var width = $("#menu-mobile-search-panel").width()-$("#icon-search").width()-28;
	$("#inp-search").width(width);
}

function searchClick(){
	$("#header-search-mobile-btn").click(function(){
		var thisElm = $(this);
		var searchElm = $("#menu-mobile-search");
		var btnList = $("#menu-mobile-list");
		childEvent(thisElm, searchElm, btnList);
		setBoxSearchWidth();
	})
}

function menuClick(){
	$("#header-menu-mobile-btn").click(function(){
		var thisElm = $(this);
		var searchElm = $("#menu-mobile-search");
		var btnList = $("#menu-mobile-list");
		childEvent(thisElm, btnList, searchElm);
	})
}

function childEvent(nowElm, selector_select, selector_close){

	var thisElm = nowElm;
	var searchElm = selector_select;
	var btnList = selector_close;

	if(searchElm.is(":visible")){
		bodyClearFixed();
		thisElm.removeClass("active");
		searchElm.hide();
	}else{
		thisElm.addClass("active");
		if(btnList.is(":visible")){
			thisElm.siblings().removeClass("active");
			btnList.hide();
		}
		bodyFixed();
		searchElm.slideDown();
	}
}

function bodyFixed(){
	$("body").addClass("nav-active");
}
function bodyClearFixed(){
	$("body").removeClass("nav-active");
}

function loadingAjax(){
	var html = "<div class='global-loading'><i class='fa fa-spinner fa-spin fa-3x'></i><div class='global-loading-text lighter'>Loading...</div></div>";
	return html;
}
function dataNotFound(){
	if(typeof DATA_NOT_FOUND !== "undefiend"){
		var html = "<div class='global-loading'><div class='global-loading-text FXregular lighter'>" + DATA_NOT_FOUND + "</div></div>";
		return html;
	}
	return "";
}

function checkWordCore(input , callback){
	var word =  $(input).val();
	var data = {};
	if(word == ""){
		data["c"] = false;
		data["m"] = CORE_SEARCH_ALERT_NULL;
		return callback(data);
	}else if(word.length < 3){
		data["c"] = false;
		data["m"] = CORE_SEARCH_ALERT_MIN_WORD;
		return callback(data);
	}else{
		data["c"] = true;
		return callback(data);
	}
}

function removeLoadingFacebook(){
	$("#login_bar .facebook").removeClass("active");
	$("#login_bar .facebook .fa-spinner").remove();
	if($("#btn-facebook").length > 0){
		$("#btn-facebook").removeClass("active");
		$("#btn-facebook").html('<img width="203" src="'+ BASE_CDN +'/images/register/btn-reg-facebook.png">');
	}
}

function memberWebLoginFacebook(){
	$(".facebook_panel .facebook").click(function(){
		if($(this).find(".fa").length > 0) return false;
		$(this).addClass("active");
		$(this).append('<i class="fa fa-spinner fa-spin"></i>');
		memberWebcheckLoginState(function(info){
			callLoginMember(info,"login");
		});
	})
}

function callLoginMember(info,type){
	$.ajax({
		url: BASE_LANG + 'sign_in/facebook',
		type: 'POST',
		data: {'facebook_id': info.id},
		dataType: 'json'
	})
	.success(function(d) {
		if(d["cmd"] == "y"){
			if(type == "login"){
				window.location.reload();
			}else if(type == "register"){
				window.location.href = BASE_LANG + "register/form/profile";
			}else{
				alert("Cannot login.");
			}
		}else if(d["cmd"] == "n"){
			addMemberByFacebook(info);
		}else{
			removeLoadingFacebook();
			alert(d["msg"]);
		}
	});
}

function addMemberByFacebook(info){
	var dataPost = {};
	dataPost = {
		"facebook_id" : info.id
		,"displayname" : info.name
		,"member_type" : "2"
		,"email" : info.email
		,"password" : ""
		,"firstname" : info.first_name
		,"lastname" : info.last_name
		,"mobile" : ""
		,"title" : ""
		,"gendar" : ""
		,"birthday" : ""
		,"mode" : "add"
	};

	$.ajax({
		url: BASE_LANG + 'register/register',
		type: 'POST',
		data: dataPost,
		dataType: 'json'
	})
	.success(function(d) {
		if(d["cmd"] == "y"){
			if(typeof(d["p"]) !== 'undefined'){
				var footer = {foot:{"ตกลง":function(){
								window.location.href = BASE_LANG + "home";
							}
						}
				}
				alert(CORE_REGISTER_COMPLETE_MESSAGE,"",footer);
			}else{
				callLoginMember(info,"register");
			}
		}else{
			removeLoadingFacebook();
			alert(d["message"]);
		}
	});
}

function memberWebcheckLoginState(callState) {
	// FB.getLoginStatus(function(response) {
	// 	if (callState.status === 'connected') {
	// 	 	memberWebGetInfoLogin(function(callInfo){
	// 	 		callState(callInfo);
	// 	 	});
	// 	} else {
		  	memberWebFacebookLogin(function(callInfo){
		  		if(callInfo){
		  			callState(callInfo);
		  		}else{
		  			removeLoadingFacebook();
		  		}
		  	});
	// 	}
	// });
}

function memberWebFacebookLogin(callLogin) {
	FB.login(function(response){
		if (response.authResponse) {
		 	memberWebGetInfoLogin(function(callInfo){
		 		callLogin(callInfo);
		 	});
		} else {
			callLogin(false);
		}
	}, {scope: 'email'});
}

function memberWebGetInfoLogin(callInfo) {
	FB.api('/me', function(response) {
		if(typeof(response.error) != "undefined"){
			window.location.reload();
		}else{
			callInfo(response);
		}
	});
}

function switchMenuLine(){
	if(currentMode){
		$("#login_bar").css("top",0);
	}else{
		if($(".line-float-box").is(":visible")){
			$("#login_bar").css("top","185px");
		}else{
			$("#login_bar").css("top","110px");
		}
	}

}