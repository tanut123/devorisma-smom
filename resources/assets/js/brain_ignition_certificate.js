var ww = $(window).width();
var certificateBox = $(".certificate-box");

$(document).ready(function(){
	$("#kid-name").on("change keyup copy paste cut",function(){
		var name = $(this).val();
		$(".certificate-name").text("น้อง"+name);
	});

	// $(".btn-confirm").click(function(){
	// 	genCertificate();
	// });

	$(".btn-share").click(function(){
		shareFacebook();
	});

    if(ww > 767){
        certificateBox.css("transform","scale(1)");
    }else{
        var scalePoint = (ww - 60) / 786;
        certificateBox.css("transform","scale("+scalePoint+")");
    }
    $(window).resize(function(){
        ww = $(this).width();
        if(ww > 767){
            certificateBox.css("transform","scale(1)");
        }else{
            var scalePoint = (ww - 60) / 786;
            certificateBox.css("transform","scale("+scalePoint+")");
        }
    });
});

// function genCertificate(){
// 	var kidName = "น้อง" + $("#kid-name").val();
// 	var cerMode = $("input[name='mode']").val();

// 	var canvas = document.getElementById("certificate-canvas");
//     var ctx = canvas.getContext("2d");

//     var background = new Image();
// 	background.src = BASE_URL + "/images/brain_ignition/certificate/certificate.jpg";

// 	background.onload = function(){
// 	    ctx.drawImage(background,0,0);

// 	    ctx.fillStyle = "#FFF";
// 		ctx.font = "30px db_helvethaicamonx";
// 		ctx.textAlign = 'center';
// 		ctx.fillText(kidName, 558, 278);

// 		ctx.fillStyle = "#43AD9E";
// 		ctx.font = "60px db_helvethaicamonx_bold";
// 		ctx.textAlign = 'center';
// 		ctx.fillText(cerMode, 560, 398);

//         if (navigator.msSaveBlob) {
//             var blob = canvas.msToBlob();
//             return navigator.msSaveBlob(blob, 'certificate.jpg');
//         } else {
//     		var image = canvas.toDataURL("image/jpeg").replace("image/jpeg", "image/octet-stream");
//             var a = document.createElement('a');
//              a.href = image;
//              a.download = 'certificate.jpg';
//              document.body.appendChild(a);
//              a.click();
//              a.remove();
//         }
// 	}

// }

function shareFacebook(){
	var kidName = "น้อง" + $("#kid-name").val();
	var cerMode = $("input[name='mode']").val();
    var cerLevel = $(".certificate-level").text();

	var canvas = document.getElementById("certificate-canvas");
    var ctx = canvas.getContext("2d");

    var background = new Image();
	background.src = BASE_CDN + "/images/brain_ignition/certificate/certificate.jpg";
	var d = new Date();
    var math = d.getTime() + Math.floor((Math.random() * 1000) + 1);
	background.onload = function(){
	    ctx.drawImage(background,0,0);

	    ctx.fillStyle = "#FFF";
		ctx.font = "30px db_helvethaicamonx";
		ctx.textAlign = 'center';
		ctx.fillText(kidName, 558, 257);

		ctx.fillStyle = "#43AD9E";
		ctx.font = "60px db_helvethaicamonx_bold";
		ctx.textAlign = 'center';
		ctx.fillText(cerMode, 560, 368);

        ctx.fillStyle = "#045597";
        ctx.font = "30px db_helvethaicamonx_bold";
        ctx.textAlign = 'center';
        ctx.fillText(cerLevel, 566, 408);

        var image = canvas.toDataURL();
        var image_name = "certificate_" + math;
        $.ajax({
            type: "POST",
            url: BASE_URL + "/certificate_save",
            data: {
                imgData: image,
                imgName: image_name
            }
        }).done(function(r) {
        });
    }
    FB.ui({
        method: 'share_open_graph',
        action_type: 'og.shares',
        action_properties: JSON.stringify({
            object: BASE_URL + "/certificate_share/" + math
        })
      },function(response) {
        if (response && !response.error_code) {

            var cerMode = $("input[name='mode']").val();
            var url_detail = "";

            if(cerMode == "ภาษา"){
                url_detail = "แววด้านการสื่อสารและภาษา";
            }else if(cerMode == "ศิลปะ"){
                url_detail = "ลูกของเรามีแววด้านศิลปะเป็นพิเศษหรือไม่นะ";
            }else if(cerMode == "ดนตรี"){
                url_detail = "ลูกของเรามีแววด้านดนตรีเป็นพิเศษหรือไม่นะ";
            }else if(cerMode == "คณิตศาสตร์"){
                url_detail = "ลูกของเรามีแววด้านคณิตศาสตร์เป็นพิเศษหรือไม่นะ";
            }else if(cerMode == "วิทยาศาสตร์"){
                url_detail = "ลูกของเรามีแววด้านวิทยาศาสตร์และนักประดิษฐ์เป็นพิเศษหรือไม่นะ";
            }else if(cerMode == "ความเป็นผู้นำ"){
                url_detail = "ลูกของเรามีแววด้านความเป็นผู้นำเป็นพิเศษหรือไม่นะ";
            }

            window.location.href = BASE_URL + "/พรสวรรค์สร้างได้ใน1000วันแรก/" +url_detail;
        } else {
          // alert('Error while posting.');
        }
    });
}