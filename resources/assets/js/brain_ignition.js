var myPlayer = {};
var nowPlay;
var nowVideo;
var played = false;
var isMobile = false;
var countVideo = 0;

var limit = 6;
var page = 0;
var tmp_start_width = "";

$(document).ready(function(){
	$(".button.brain-tip").click(function(){
		var elm = $(this);
		if(elm.hasClass("active")){
			elm.removeClass("active");
			$(".dynamic-panel").removeClass("active");
		}else{
			$(".button.active").removeClass("active");
			elm.addClass("active");
			$(".dynamic-panel").addClass("active");
		}
	});

	// isMobile = DetectMobile();
	initSlide();
	ajaxList();
	$(".button-loadmore").click(function () {
		ajaxList();
	});
	$(window).resize(function () {
		if(tmp_start_width > 767 && $(window).width() < 768){
			tmp_start_width = $(window).width();
			page = 0;
			limit = 3;
			$(".data-panel").html(" ");
			$(".button-loadmore").css({"display":"none"});
			ajaxList();

		}
		if(tmp_start_width < 768 && $(window).width() > 767){
			tmp_start_width = $(window).width();
			page = 0;
			limit = 6;
			$(".data-panel").html(" ");
			$(".button-loadmore").css({"display":"none"});
			ajaxList();
		}
	});
});

function initSlide(){
	$(".slide-panel").slick({
		dots: true,
		infinite: true,
		fade: true,
	}).on('beforeChange', function(event, slick, currentSlide, nextSlide){
		if(checkType() == "image" && !played){
			clearAutoPlay();
		}else{
			checkToPauseVideo();
		}
	}).on('afterChange', function(event, slick, currentSlide){
		if(checkType() == "image"){
			clearAutoPlay();
			playAuto();
		}else{
			checkToPlayVideo();
		}
	});

	loaderImage();

	var elmActive = getNowElement().find(".slide-list-inner");
	var type = elmActive.data("type");
	if(type == "image" && !played){
		playAuto();
	}else{
		checkToPlayVideo();
	}
}

function loaderImage(){
	$(".image-loader").each(function(){
		var elm = $(this);
		var image = elm.data("image");
		var type = elm.data("type");
		var imgTag = "<img src='" + image + "' class='images-list slide-list-inner' data-type='" + type + "' />";
		elm.after(imgTag);
	});

	// $(".image-loader-video").each(function(){
	// 	var elm = $(this);
	// 	var image = elm.data("image");
	// 	var type = elm.data("type");
	// 	var imgTag = "<img src='" + image + "' class='mobile-detect visible-xs' data-type='" + type + "' />";
	// 	elm.after(imgTag);
	// });
}

function loadVideo(type, elm) {
	var videoID = elm.attr("id");
    if (typeof(videoID) == "undefined") { return; }
    if (videoID.indexOf('#') == -1) videoID = '#'+videoID;
    nowPlay = type;
    if (type == "video") {
        nowVideo = videoID;
        if ($(videoID).find('.big-video-wrap').size()>0) {
            if (!isMobile) {
                myPlayer[nowPlay][nowVideo].play();
            }
            return;
        }
	   	var source = elm.data("source");
	   	var banner_id = "#" + elm.parent().attr("id");
	    var BV = new $.BigVideo({ container : $(videoID), doLoop : (!moreContent() && !isMobile)   });
	    BV.init();
	    BV.show(source);
        if (typeof(myPlayer[type]) != "object") myPlayer[type] = {};
        myPlayer[type][videoID] = BV.getPlayer();
        countVideo++;
        myPlayer[type][videoID].ready(function(){
            myPlayer[type][videoID].on("play", function(){
                clearAutoPlay();
                getNowElement().find(".link-video").addClass("link-video-active");
                played = true;
                // mute sound
                this.volume(0);
            });
             myPlayer[type][videoID].on("pause", function(){
             	if(isMobile){
	                played = false;
	                clearAutoPlay();
	                playAuto();
	                // $(".mobile-detect.active").removeClass("active");
	                $(".playing").removeClass("playing");
             	}
            });

            if (!isMobile) {
                myPlayer[type][videoID].play();
            } else {
            }
            myPlayer[type][videoID].on("ended", function(){
                destroyPlayer(0);
                slickNext();
            });
        });
    } else if (type == "youtube") {
        var code = $(videoID).data("source");
        var youtube_id = $(videoID).prev(".player_list").attr("id");
		var options = { videoId: code };
		if(getNowElement().find(".tubular-panel").length > 0){
			return ;
		}
		$(".slide-list").find(".player_list").each(function(){
			var id = $(this).attr("id");
			var codeID = $(this).data("source");
			var options_yt = {
					videoId: codeID,
					mute: true,
	 				onStateChangeApi: function(state){
	 					// end video
	 					if (state.data == 0) {
	                        slickNext();
	                        played = false;
	                    }
	                    // play video
	                    if (state.data == 1) {
	                    	if(isMobile || media == "tablet"){
	                    		clearAutoPlay();
	                    	}
	                        played = true;
	                        if($(".slide-panel").hasClass("andriod-device") || $(".slide-panel").hasClass("ipad-device")){
	                        	getNowElement().find("iframe").addClass("active");
	                        }
	                    }
	                     if (state.data == 2) {
	                     	getNowElement().find(".link-video").removeClass("link-video-active");
	                     	if(isMobile){
		                        played = false;
		                        clearAutoPlay();
		                        playAuto();
	                     	}
	                    }
	 				}
	 			};
			if(getNowElement().find(".player_list").attr("id") == id && !isMobile){
				 options_yt["onReadyApi"] = function(e){
					if(!isMobile){
						e.target.playVideo();
					}
				};
			}
			if(!moreContent()){
				options_yt["repeat"] = true;
			}
        	$('#' + id).tubular(options_yt);
		})
    }
}

function checkToPauseVideo(){
	if(checkType() == "video"){
		if(!isMobile || media == "tablet"){
			myPlayer[nowPlay][nowVideo].pause();
		}
	}else{
		getNowElement().find(".player_list").tubular('pause');
	}
}

function checkToPlayVideo(){
	loadVideo(checkType(), getNowElement().find(".slide-list-inner"));
	if(checkType() == "video"){
		if(!isMobile){
			myPlayer[nowPlay][nowVideo].play();
			clearAutoPlay();
		}else{
			clearAutoPlay();
			playAuto();
		}
	}else if(checkType() == "youtube"){
		if(!isMobile){
			clearAutoPlay();
			getNowElement().find(".player_list").tubular('play').tubular('play');
		}else{
			clearAutoPlay();
			playAuto();
		}
	}
}

function checkType(){
	return getNowElement().find(".slide-list-inner").data("type");
}

function getNowElement(){
	return $(".slide-panel").find(".slick-active");
}

function DetectMobile(){
	var status = false;
	if(navigator.userAgent.match(/iPad/i)){
	 	status = true;
	}

	if(navigator.userAgent.match(/(iPod|iPhone)/)){
	 	status = true;
	}

	if(navigator.userAgent.match(/Android/i)){
	 	status = true;
	}

	if(navigator.userAgent.match(/BlackBerry/i)){
	 	status = true;
	}

	if(navigator.userAgent.match(/webOS/i)){
	 	status = true;
	}

	return status;
}

function moreContent(){
	return ($(".slide-panel").find(".slide-list").length > 1)? true: false;
}

function slickNext(){
	$('.slide-panel').slick('slickNext');
}

var interval;
function playAuto(){
	var timeIn = 7;
	interval = setInterval(function(){
		if(!played || checkType() == "image"){
			slickNext();
		}
	}, timeIn*1000);
}

function clearAutoPlay(){
	if(interval){
		clearInterval(interval);
	}
}

function destroyPlayer(ended){
    if (!moreContent() && !isMobile) return;
    if (myPlayer[nowPlay] && played) {
        if (nowPlay == "video") {
            if(typeof(myPlayer['video'][nowVideo].pause) == "function") {
                if(typeof(ended) == "undefined") myPlayer['video'][nowVideo].pause();
            }
        } else if (nowPlay == "youtube") {
            myPlayer['youtube'].destroy();
        }
    }
    played = false;
}

function playPauseVideo(){
	var elm = getNowElement().find(".slide-list-inner");
	var video_id = "#" + elm.attr("id");
	// elm.siblings(".mobile-detect").addClass("active");
	myPlayer["video"][video_id].play();
	played = false;
	elm.parent().addClass("playing");
	getNowElement().find("play-control").remove();
}

function ajaxList() {
	$(".data-panel").append(loadingAjax());
	page = page +1;
	var new_url = BASE_URL + "/พรสวรรค์สร้างได้ใน1000วันแรก/loadpage";
	$.ajax({
		url: new_url,
		type: "POST",
		data: {
			"page" : page,
			"limit" : limit,
		},
		dataType: "json"
	})
	.success(function (data) {
		$(".global-loading").remove();
		$(".data-panel").append(data.ajax_html);
		chk_loadmore(data["has_loadmore"]);
	})
}

function chk_loadmore(has_loadmore) {
	if(has_loadmore == false){
		$(".button-loadmore").hide();
	}else{
		$(".button-loadmore").show();
	}
}