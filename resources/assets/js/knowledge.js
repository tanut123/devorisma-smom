$(document).ready(function () {
	initSlide();
});

function initSlide(){
	$(".list-panel-sub").each(function(){
		$(this).slick({
			dots: false,
	        infinite: false,
	        autoplay: false,
	        slidesToShow: 3,
	        slidesToScroll: 1,
	        responsive: [
	            {
	                breakpoint: 767,
	                settings: {
	                    slidesToShow: 1,
	                    slidesToScroll: 1
	                }
	            }
	        ]
		});
	});
	setHeightDescription();
}

function setHeightDescription(){
	$(".list-item").each(function(a,b){
		var this_item = $(b);
		var height = this_item.find(".txt-list-panel .list-title").innerHeight();
		var description = this_item.find(".txt-list-panel .short-desc");
		if(description.text().trim() != ""){
			var heightMod = Math.round(height / 30);
			if(heightMod == 1){
				description.JJJud({"max_rows":2});
			}else if(heightMod == 2){
				description.JJJud({"max_rows":1});
			}else{
				description.hide();
			}
		}
	});
}