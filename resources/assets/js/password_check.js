var currentPw;
var currentConPw;
var checkStatus = false;
var checkFormat = false;

$(document).ready(function(){
    checkPassword();
    SendData();
    validateForm();
    checkPasswordInput();
});
function checkPassword(){
    $('#password').keyup(function(){
        checkStrength($('#password').val());
        currentPw = $('#password').val();
        validatePassword();
    });
    $('#confirm-password').keyup(function(){
        currentConPw = $('#confirm-password').val();
        validatePassword();
    })
}

function checkStrength(password){
    var checkPass = true;
    var level = 5;
    if (!password.match(/([a-z])/)) {
        checkPass = false;
        level--;
    }
    if (!password.match(/([A-Z])/)) {
        checkPass = false;
        level--;
    }
    if (!password.match(/([0-9])/)) {
        checkPass = false;
        level--;
    }
    if (!password.match(/([ !"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~])/)) {
        checkPass = false;
        level--;
    }
    if (password.length > 0 && password.length < 10) {
        checkPass = false;
        level--;
    }
    $('.lv1 , .lv2 , .lv3 , .lv4').css('background-color','#e3d9bf');
    var backgroundColor = new Array();
    backgroundColor[0] = "#e3d9bf";
    backgroundColor[1] = "red";
    backgroundColor[2] = "red";
    backgroundColor[3] = "orange";
    backgroundColor[4] = "#ecd077";
    backgroundColor[5] = "green";
    var displayLevel;
    if (level > 4) {
        displayLevel = 4;
    } else {
        if (password.length > 0) {
            displayLevel = level;
        } else {
            displayLevel = 0;
        }
    }
    for (var i = 1;i<=displayLevel;i++) {
        $(".lv"+i).css('background-color',backgroundColor[level]);
    }
    checkFormat = checkPass;
    return checkPass;
}


function validatePassword(){
    if(currentPw != currentConPw) {
/*        if (currentPw == null) {
            $('#checkmatch').html("กรุณาใส่รหัสผ่าน");
        }else{
            if(currentConPw!=null){
                $('#checkmatch').html("รหัสผ่านไม่ตรงกัน");
            }
        }*/
        checkStatus = false;
    } else if (currentPw.length < 10) {
        //$('#checkmatch').html("รหัสผ่านจะต้องมีความยาวอย่างน้อย 10 ตัว อักษร");
        checkStatus = false;
    } else {
        if (currentPw.length != 0) {
            $('#checkmatch').html("");
            checkStatus = true;
        }
    }
}

function SendData(){
    $('.btn-submit').click(function(){
        if (checkFormat == true && checkStatus == true) {
            $.ajax({
                url: BASE_LANG + "password/change",
                data: {new_pass:$('#password').val(),csrf_token:csrf_token},
                type: "POST",
                cache: false,
                dataType: 'json',
                success: function(resp){
                    if (resp.status=="success") {
                        var footer = {foot:{"OK":function(){
                            window.location.href = BASE_LANG + "home";
                        }}};
                        alert(CHANGE_PASS_SUCCESS,"",footer);

                    } else if (resp.status=="using") {
                        alert(CHANGE_PASS_USING);
                    } else {
                        alert(CHANGE_PASS_EVER_USING);
                    }
                }
            });
        } else {

        }
    })


}

function checkPasswordInput(){

    $("#password").removeClass("validate[required, funcCall[checkValidatePassword]]");
    $("#confirm-password").removeClass("validate[required,equals[password]]");

    $("#password").on("keyup",function(){
        if($(this).val() != ""){
            $("#password").addClass("validate[required, funcCall[checkValidatePassword]]");
            $("#confirm-password").addClass("validate[required,equals[password]]");
        }else{
            $("#password").removeClass("validate[required, funcCall[checkValidatePassword]]");
            $("#confirm-password").removeClass("validate[required,equals[password]]");
        }
    })
}
function checkValidatePassword(field, rules, i, options){
    if(!checkStrength(field.val())) {
        return "* กรุณาระบุรหัสผ่านให้ตรงตามเงื่อนไข";
    }
}
function validateForm(){
    var submitForm = $("#register-form");
    submitForm.validationEngine('detach');
    submitForm.validationEngine('attach', {
        promptPosition: "bottomLeft",
        showOneMessage: true,
        addFailureCssClassToField: "error",
        scroll: false,
        onValidationComplete: function(form, status) {
            if(status){
                var data = $("#register-form").serializeArray();
                $.ajax({
                    url: BASE_LANG + "register/register",
                    data: data,
                    type: "POST",
                    cache: false,
                    dataType: 'json',
                    success: function(d){
                        if(d["cmd"] == "y"){
                            if($("input[name=mode]").val() == "edit"){
                                window.location.reload(true);
                            }else{
                                var footer = {foot:{"ตกลง":function(){
                                    window.location.href = BASE_LANG + "home";
                                }}};
                                alert(REGISTER_COMPLETE_MESSAGE,"",footer);
                            }
                        }else{
                            alert(d["message"]);
                        }
                    }
                })
            }
        }
    });
}