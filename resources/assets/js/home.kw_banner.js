$(document).ready(function(){
    calculate_kl_size();
    $(".left_banner").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    });
    $(".right_top_1").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    });
    $(".right_top_2").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    });
    $(".right_bottom_banner").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    });
    $(".left_banner").mouseover(function() {
        $(".left_banner .slick-prev").show();
        $(".left_banner .slick-next").show();
    });
    $(".left_banner").mouseout(function() {
        $(".left_banner .slick-prev").hide();
        $(".left_banner .slick-next").hide();
    });
    $(".right_top_1").mouseover(function() {
        $(".right_top_1 .slick-prev").show();
        $(".right_top_1 .slick-next").show();
    });
    $(".right_top_1").mouseout(function() {
        $(".right_top_1 .slick-prev").hide();
        $(".right_top_1 .slick-next").hide();
    });
    $(".right_top_2").mouseover(function() {
        $(".right_top_2 .slick-prev").show();
        $(".right_top_2 .slick-next").show();
    });
    $(".right_top_2").mouseout(function() {
        $(".right_top_2 .slick-prev").hide();
        $(".right_top_2 .slick-next").hide();
    });
    $(".right_bottom_banner").mouseover(function() {
        $(".right_bottom_banner .slick-prev").show();
        $(".right_bottom_banner .slick-next").show();
    });
    $(".right_bottom_banner").mouseout(function() {
        $(".right_bottom_banner .slick-prev").hide();
        $(".right_bottom_banner .slick-next").hide();
    });

});
$(window).resize(function () {
    calculate_kl_size()
});
function calculate_kl_size() {
    var w_width = $(window).width();
    if (w_width < 767) {
        $(".img_banner_left").css("width",(w_width-40)+"px");
        $(".left_banner_data").css("width",(w_width-40)+"px");

        $(".right_banner").css("width",(w_width-40)+20+"px");

        $(".img_banner_right1").css("width",Math.floor(((w_width-45)/2)-5)+"px");
        $(".right_top_1").css("width",Math.floor(((w_width-45)/2)+5)+"px");
        $(".right_top_1").css("height",Math.floor(((w_width-45)/2)-5)+"px");

        $(".img_banner_right2").css("width",Math.floor(((w_width-45)/2)-5)+"px");
        $(".right_top_2").css("width",Math.floor(((w_width-45)/2)+5)+"px");
        $(".right_top_2").css("height",Math.floor(((w_width-45)/2)-5)+"px");

        $(".right_top_banner_data").css("width",Math.floor(((w_width-45)/2)-5)+"px");
        $(".right_top_banner_data").css("height",Math.floor(((w_width-45)/2)-5)+"px");
        $(".right_top_banner_data").css("border-radius","5%");

        $(".img_banner_rightb").css({"width":(w_width-40)+"px","height":"100%"});
        $(".right_bottom_banner_data").css("width",(w_width-40)+"px");
        $(".right_bottom_banner_data").css("height",Math.floor(((w_width-45)/2)-5)+"px");

    } else {
        $(".img_banner_left").css("width","465px");
        $(".left_banner_data").css("width","465px");
        $(".left_banner_data").css("border-radius","");

        $(".right_banner").css("width","485px");

        $(".img_banner_right1").css("width","220px");
        $(".right_top_1").css("width","230px");
        $(".right_top_1").css("height","220px");

        $(".img_banner_right2").css("width","220px");
        $(".right_top_2").css("width","230px");
        $(".right_top_2").css("height","220px");

        $(".right_top_banner_data").css("width","220px");
        $(".right_top_banner_data").css("height","220px");
        $(".right_top_banner_data").css("border-radius","");

        $(".img_banner_rightb").css("width","465px");
        $(".right_bottom_banner_data").css("width","465px");
        $(".right_bottom_banner_data").css("height","220px");

        $(".right_bottom_banner_data").css("border-radius","");
    }
}