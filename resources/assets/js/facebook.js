window.fbAsyncInit = function() {
    FB.init({
        appId: FB_APPID,
        xfbml      : true,
      	version    : 'v2.4'
    });

    fbCheckLogin();
};


function fbCheckLogin() {
    var data;
    if (typeof FB === "undefined") {
        alertMessage("ไม่สามารถเชื่อมต่อกับ facebook ได้", true);
    }
    FB.XFBML.parse;
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            LOGIN_STATUS = response;
        } else {
            LOGIN_STATUS = false;
        }
    });
}


(function(d, s, id){
 var js, fjs = d.getElementsByTagName(s)[0];
 if (d.getElementById(id)) {return;}
 js = d.createElement(s); js.id = id;
 js.src = "//connect.facebook.net/en_US/sdk.js";
 fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));