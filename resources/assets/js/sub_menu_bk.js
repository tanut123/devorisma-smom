var tmp_parent = "";
var tmp_child = "";
// $(".bg-sub-hover").hide();
$(document).ready(function () {
	$(".menu-list").mouseover(function(){
        var nowNormal =  $(this).next();
        	tmp_child = nowNormal;
        if(nowNormal.hasClass("list_submenu_hover")){
	        nowNormal.addClass("list_submenu_enable");
	        nowNormal.prev().addClass("hover");
	        $(".list_submenu_hover").not(nowNormal).removeClass("list_submenu_enable");
	        $(".list_submenu_hover").not(nowNormal).prev().removeClass("hover");
	        if($("#sub-menu-product").hasClass("list_submenu_enable")){
				$("#TabNav").next().removeClass("bg-sub-hover");
				$("#TabNav").next().addClass("bg-sub-hover-product");
			}else{
	        	$("#TabNav").next().addClass("bg-sub-hover");
			}
        }else{
        	//tmp_parent.removeClass("hover");
        	tmp_child.removeClass("list_submenu_enable");
        	$(this).addClass("hover");
    		$(".list_submenu_hover").removeClass("list_submenu_enable");
    		$("#TabNav").next().removeClass("bg-sub-hover");
        }
		tmp_parent = $(this);
    });

    $(".menu-list").mouseleave(function () {
    	if(tmp_child != $(this).next()){
    		$(this).removeClass("hover");
    	}
    	$("#TabNav").next().removeClass("bg-sub-hover");
    });

    $(".list_submenu_hover").mouseover(function () {
    	if(!tmp_parent.hasClass("hover")){
    		tmp_parent.addClass("hover");
    	}
    	if($("#sub-menu-product").hasClass("list_submenu_enable")){
			$("#TabNav").next().removeClass("bg-sub-hover");
			$("#TabNav").next().addClass("bg-sub-hover-product");
		}else{
        	$("#TabNav").next().addClass("bg-sub-hover");
		}
    });
	$(".list_submenu_hover").mouseleave(function(){
  		$(this).removeClass("list_submenu_enable");
  		$(this).prev().removeClass("hover");
  		$("#TabNav").next().removeClass("bg-sub-hover");
	});

	$("#menu_page").mouseleave(function () {
		$(".list_submenu_hover").removeClass("list_submenu_enable");
		$(".menu-list").removeClass("hover");
		$("#TabNav").next().removeClass("bg-sub-hover");
	});

	$("#lang_search").hover(function () {
		$(".list_submenu_hover").removeClass("list_submenu_enable");
		$(".menu-list").removeClass("hover");
		$("#TabNav").next().removeClass("bg-sub-hover");
	});
});