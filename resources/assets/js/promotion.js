var limit = 6;
var page = 0;
var tmp_start_width = "";
$(document).ready(function () {
	tmp_start_width = $(window).width();
	if($(window).width() < 768){
		limit = 3;
	}
	ajaxList();
	$(".load-more").click(function () {
		ajaxList();
	});
	$(window).resize(function () {
		if(tmp_start_width > 767 && $(window).width() < 768){
			limit = 3;
			tmp_start_width = $(window).width();
			page = 0;
			$(".list-panel").html(" ");
			$(".load-more").css({"display":"none"});
			ajaxList();

		}
		if(tmp_start_width < 768 && $(window).width() > 767){
			limit = 6;
			tmp_start_width = $(window).width();
			page = 0;
			$(".list-panel").html(" ");
			$(".load-more").css({"display":"none"});
			ajaxList();
		}
	});
});
function getList() {
	return $(".list-panel .list-item ").length;
}
function ajaxList() {
	$(".list-panel").append(loadingAjax());
	page = page +1;
	$.ajax({
		url: BASE_LANG + "promotion/loadpage",
		type: "POST",
		data: {"page" : page,"limit" : limit},
		dataType: "json"
	})
	.success(function (data) {
		$(".global-loading").remove();
		$(".list-panel").append(data.ajax_html);
		chk_loadmore(data["has_loadmore"]);
	})
}

function chk_loadmore(has_loadmore) {
	if(has_loadmore == false){
		$(".load-more").hide();
	}else{
		$(".load-more").show();
	}
}