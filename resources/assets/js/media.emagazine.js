$(document).ready(function () {
	$(".c_load").click(function () {
		var ref_id = $(this).attr("data-id");
		$.ajax({
			url: BASE_LANG+"page-view-magazine/"+ref_id,
			type: 'GET',
			dataType: "json",
			success: function(result){
            	$("#m_view_"+result.m_id).next().html("Download: "+result.count);
        	}
		});
	})
});