var tmp_parent = "";
var tmp_child = "";
var tempTime
// $(".bg-sub-hover").hide();
$(document).ready(function () {
	$("#TabNav").next().hide();
	$("#TabNav").next().next().hide();

	$('.menu-list').bind('touchstart mouseover', function(e){
		clearTimeout(tempTime)
	    hoverMenu(e,$(this));
	});

    $(".menu-list").mouseleave(function () {
    	tempTime = setTimeout(function(){
    		clearhover();
    	},500)
    });

    $(".list_submenu_hover").mouseover(function () {
    	clearTimeout(tempTime)
		if (tmp_parent != "") {
			if(!tmp_parent.hasClass("hover")){
				tmp_parent.addClass("hover");
			}
		}
    	if($("#sub-menu-product-dt").hasClass("list_submenu_enable")){
			$("#TabNav").next().hide();
			$("#TabNav").next().next().show();
		}else{
        	$("#TabNav").next().show();
        	$("#TabNav").next().next().hide();
		}
    });
	$(".list_submenu_hover").mouseleave(function(){
		tempTime = setTimeout(function(){
    		clearhover();
    	},200)
  		$(this).removeClass("list_submenu_enable");
  		$(this).prev().removeClass("hover");
  		$("#TabNav").next().hide();
    	$("#TabNav").next().next().hide();
	});

	// $("#menu_page").mouseleave(function () {
	// 	$(".list_submenu_hover").removeClass("list_submenu_enable");
	// 	$(".menu-list").removeClass("hover");
	// 	$("#TabNav").next().hide();
 //    	$("#TabNav").next().next().hide();
	// });

	$("#lang_search").hover(function () {
		$(".list_submenu_hover").removeClass("list_submenu_enable");
		$(".menu-list").removeClass("hover");
		$("#TabNav").next().hide();
    	$("#TabNav").next().next().hide();
	});
	// //setTimeout(function() {
	$(".list_submenu_enable").removeClass("list_submenu_enable");
	// //},300);
});

function hoverMenu (event,element) {
	var nowNormal =  element.next();
	var nowNormalDK = $("#" + element.data("id"))
	if(event.type == "touchstart" ){
		element.unbind("mouseleave");
		clearhover();
	}
    	tmp_child = nowNormalDK;
        if(nowNormalDK.hasClass("list_submenu_hover")){
	        nowNormalDK.addClass("list_submenu_enable");
	        nowNormalDK.prev().addClass("hover");
	        $(".list_submenu_hover").not(nowNormalDK).removeClass("list_submenu_enable");
	        $(".list_submenu_hover").not(nowNormalDK).prev().removeClass("hover");
	        if($("#sub-menu-product-dt").hasClass("list_submenu_enable")){
				$("#TabNav").next().hide();
				$("#TabNav").next().next().show();
			}else{
	        	$("#TabNav").next().show();
	        	$("#TabNav").next().next().hide();
			}
        }else{
        	//tmp_parent.removeClass("hover");
        	// alert(event.type);
        	tmp_child.removeClass("list_submenu_enable");
        	element.addClass("hover");
    		$(".list_submenu_hover").removeClass("list_submenu_enable");
    		$("#TabNav").next().hide();
    		$("#TabNav").next().next().hide();
        }
		tmp_parent = element;
}

function clearhover () {
	// if(tmp_child != tmp_child.next()){
	// 	$(".menu-list").removeClass("hover");
	// }
	$("#TabNav").next().hide();
	$("#TabNav").next().next().hide();
}