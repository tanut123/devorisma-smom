var tempTimeoutHover = null;
var tempTimeoutDoctorName = null;
var tempTimeoutDoctorDetail = null;
var tempTimeoutScrollArrow = null;
var tempScrollAction = null;
var tempIndex = 0;
var tempTouchX;
var tempMaxScroll;
var tempSwiftTimeOut = null;
var tempActiveLast = false;
var tempMenuBtnHomer = false;
var tempFadeTime = null;
var autoOn;
var autoTime = 5000;

var topPanelArticle = ($(".wyeth-article-panel").offset().top - 70);
var topPanelLegacy = ($(".wyeth-legacy-panel").offset().top - 70);
var topPanelContact = ($(".wyeth-contact-panel").offset().top - 70);
var topPanelFooter= ($("#footer_panel").offset().top - 70);

$(document).ready(function() {
    var url = window.location.href;
    if (url.indexOf("wyeth-legacy") > -1) {
        scroll_to_anchor("wyeth-legacy");
    }
    if (url.indexOf("wyeth-team") > -1) {
        scroll_to_anchor("wyeth-team");
    }
    if (url.indexOf("wyeth-contact") > -1) {
        scroll_to_anchor("wyeth-contact");
    }
    if (url.indexOf("wyeth-article") > -1) {
        scroll_to_anchor("wyeth-article");
    }

    $(window).scroll(function() {
        var windowScroll = $(window).scrollTop();
        if (windowScroll < topPanelArticle) {
            $(".wyeth_nav_bar").removeClass("active");
        } else if( windowScroll > topPanelFooter){
            $(".wyeth_nav_bar").removeClass("active");
        } else{
            $(".wyeth_nav_bar").addClass("active");
        }
        if (windowScroll > topPanelArticle & windowScroll < topPanelLegacy) {
            $(".wyeth_btn1").addClass("active");
        } else {
            $(".wyeth_btn1").removeClass("active");
        }
        if (windowScroll > topPanelLegacy & windowScroll < topPanelContact) {
            $(".wyeth_btn2").addClass("active");
        } else {
            $(".wyeth_btn2").removeClass("active");
        }
        if (windowScroll > topPanelContact & windowScroll < topPanelFooter) {
            $(".wyeth_btn3").addClass("active");
        } else {
            $(".wyeth_btn3").removeClass("active");
        }
    });
    $("#doctor1").mouseover(function() {
        animetionDoctor(1);
    });
    $("#doctor1").mouseout(function() {
        clearAnimetion(1);
    });
    $("#doctor2").mouseover(function() {
        animetionDoctor(2);
    });
    $("#doctor2").mouseout(function() {
        clearAnimetion(2);
    });
    $("#doctor3").mouseover(function() {
        animetionDoctor(3);
    });
    $("#doctor3").mouseout(function() {
        clearAnimetion(3);
    });
    $("#doctor4").mouseover(function() {
        animetionDoctor(4);
    });
    $("#doctor4").mouseout(function() {
        clearAnimetion(4);
    });
    $("#doctor5").mouseover(function() {
        animetionDoctor(5);
    });
    $("#doctor5").mouseout(function() {
        clearAnimetion(5);
    });
    $(".slide_panel").bind("touchstart", function(event) {
        tempTouchX = event.originalEvent.touches[0].pageX;
    });
    $(".slide_panel").bind("touchend", function(event) {
        var change = event.originalEvent.changedTouches[0].pageX;
        var direction = "";
        var active = false;
        var activeIndex;
        if (tempTouchX > change) {
            if (tempTouchX - change > 50) {
                direction = "up";
                active = true;
            }
        }
        if (tempTouchX < change) {
            if (change - tempTouchX > 50) {
                direction = "down";
                active = true;
            }
        }
        if (active == true) {
            if (direction == "up" && tempIndex < 14) {
                activeIndex = tempIndex+1;
            }
            if (direction == "down" && tempIndex > 1) {
                activeIndex = tempIndex-1;
            }
            var scroll = 0;
            if ($(window).width() < 767) {
                scroll = ((activeIndex - 1) * 50)-5;
            } else {
                scroll = $(".data"+activeIndex+" .history_dot").attr("scroll");
            }
            if (typeof(activeIndex) != "undefined") {
                scrollToX($("#slide_panel"),scroll);
                activeYear(activeIndex);
            }
        }
    });
    $(".history_dot").click(function() {
    	clearInterval(autoOn);
        var active_number = $(this).attr("data");
        var scroll = 0;
        if ($(window).width() < 767) {
            scroll = ((active_number - 1) * 50)-5;
        } else {
            scroll = $(this).attr("scroll");
        }
        scrollToX($("#slide_panel"),scroll);
        activeYear(active_number);
        autoOn = setInterval(autoSlide,autoTime);
    });
    $(".left_arrow").bind("touchstart mousedown",function() {
        tempActiveLast = false;
        if (tempTimeoutScrollArrow == null && !$(".left_arrow").hasClass("deactive")) {
            tempTimeoutScrollArrow = setTimeout(function() {
                scrollLeft()
            },20);
        }
    });
    $(".right_arrow").bind("touchstart mousedown",function() {
        if (tempTimeoutScrollArrow == null && !$(".right_arrow").hasClass("deactive")) {
            tempTimeoutScrollArrow = setTimeout(function () {
                scrollRight()
            }, 20);
        }
    });
    $(".left_arrow").bind("touchend mouseout mouseup",function() {
        cancelScrollArrow();
    });
    $(".right_arrow").bind("touchend mouseout mouseup",function() {
        cancelScrollArrow();
    });
    $("#slide_panel").scrollLeft($("#slide_panel")[0].scrollWidth);
    tempMaxScroll = $("#slide_panel").scrollLeft();
    $("#slide_panel").scrollLeft(0);
    $(".left_arrow").addClass("deactive");
    activeYear(1);

    autoOn = setInterval(autoSlide,autoTime);

    $(".data_cell .data .detail_box").hover(function(){
    	clearInterval(autoOn);
    },function(){
    	autoOn = setInterval(autoSlide,autoTime);
    });
});

function autoSlide(){
	var size = $('.history_dot').size();
	var nextSlide;
	deactiveYear();
	if(tempIndex < size){
		nextSlide = tempIndex+1;
	}else if(tempIndex == size){
		nextSlide = 1;
	}
    var scroll = 0;
    if(nextSlide == 1){
    	scroll = 0;
    }else if ($(window).width() < 767) {
        scroll = ((nextSlide - 1) * 50)-5;
    } else {
        scroll = $('.history_dot:eq('+tempIndex+')').attr("scroll");
    }

    scrollToX($("#slide_panel"),scroll);
	activeYear(nextSlide);
}

function scroll_to_anchor(anchor_id){
    var tag = $("#"+anchor_id+"");
    $('html,body').animate({scrollTop: tag.offset().top},'slow');
}
function checkActiveArrow() {
    if ($("#slide_panel").scrollLeft() == tempMaxScroll && $("#slide_panel").scrollLeft() != 0 || tempActiveLast == true) {
        $(".right_arrow").addClass("deactive");
    } else {
        $(".right_arrow").removeClass("deactive");
    }
    if ($("#slide_panel").scrollLeft() == 0) {
        $(".left_arrow").addClass("deactive");
    } else {
        $(".left_arrow").removeClass("deactive");
    }
    tempMaxScroll = $("#slide_panel").scrollLeft();
}
function scrollLeft() {
    if ($("#slide_panel").scrollLeft() > 0) {
        $("#slide_panel").scrollLeft($("#slide_panel").scrollLeft()-10);
        tempTimeoutScrollArrow = setTimeout(function() {
            scrollLeft();
        },10);
    } else {
        cancelScrollArrow();
    }
    checkActiveArrow();
}
function scrollRight() {
    if ($("#slide_panel").scrollLeft() <= tempMaxScroll) {
        $("#slide_panel").scrollLeft($("#slide_panel").scrollLeft()+10);
        tempTimeoutScrollArrow = setTimeout(function() {
            scrollRight();
        },10);
    } else {
        cancelScrollArrow();
    }
    checkActiveArrow();
}
function cancelScrollArrow() {
    clearTimeout(tempTimeoutScrollArrow);
    tempTimeoutScrollArrow = null;
}
function scrollToX(element,scroll) {
    var currentScrollX = $(element).scrollLeft();
    var scrollDirection = null;
    if (currentScrollX > scroll) {
        scrollDirection = "down";
    } else if (currentScrollX < scroll) {
        scrollDirection = "up";
    }
    clearTimeout(tempScrollAction);
    tempScrollAction = scrollAction(element,scroll,scrollDirection);
}
function scrollAction(element,scroll,direction) {
    if (direction == "up" && $(element).scrollLeft() < scroll) {
        clearTimeout(tempScrollAction);
        tempScrollAction = setTimeout(function() {
            $(element).scrollLeft($(element).scrollLeft()+5);
            scrollAction(element,scroll,direction);
        },5);
    }
    if (direction == "down" && $(element).scrollLeft() > scroll) {
        clearTimeout(tempScrollAction);
        tempScrollAction = setTimeout(function() {
            $(element).scrollLeft($(element).scrollLeft()-5);
            scrollAction(element,scroll,direction);
        },5);
    }
    checkActiveArrow();
}
function activeYear(num) {
	num = parseInt(num);
	tempIndex = parseInt(tempIndex);
    if (num != tempIndex) {
        tempIndex = num;
        deactiveYear();
        $(".data"+num+" .data").addClass("active");
        clearTimeout(tempFadeTime);
        var detail = $(".data"+num+" .data .detail_box .detail").html();
        $(".mobile_detail_panel").css({"transition":"none","opacity":0});
        $(".mobile_detail_panel").slideUp();
        if ($(window).width() < 768) {
            clearTimeout(tempSwiftTimeOut);
            tempSwiftTimeOut = setTimeout(function() {
                $(".mobile_detail_panel").html(detail);
                $(".mobile_detail_panel").slideDown();
                $(".mobile_detail_panel").css("transition","opacity .5s");
                clearTimeout(tempFadeTime);
                tempFadeTime = setTimeout(function(){
        			$(".mobile_detail_panel").css("opacity",1);
                },500);
            },500);
        }
        if (num == 14) {
            tempActiveLast = true;
        } else {
            tempActiveLast = false;
        }
    }
}
function deactiveYear() {
    for (var i = 1; i <= 14; i++) {
        $(".data"+i+" .data").removeClass("active");
    }
}
function animetionDoctor(id) {
	clearTimeout(tempTimeoutHover);
		tempTimeoutHover = setTimeout(function(){
	    $("#doc_detail_"+id+" .doctor_relative .line").addClass("line_show");
	    clearTimeout(tempTimeoutDoctorDetail);
	    tempTimeoutDoctorDetail = setTimeout(function() {
	        $("#doc_detail_"+id+" .doctor_relative .doctor_position").addClass("doctor_position_show");
	    },500);
	    clearTimeout(tempTimeoutDoctorName);
	    tempTimeoutDoctorName = setTimeout(function() {
	        $("#doc_detail_"+id+" .doctor_relative .doctor_name").addClass("doctor_name_show");
	    },300);
    },500);
}
function clearAnimetion(id) {
	clearTimeout(tempTimeoutHover);
    clearTimeout(tempTimeoutDoctorDetail);
    clearTimeout(tempTimeoutDoctorName);
    $("#doc_detail_"+id+" .doctor_relative .line").removeClass("line_show");
    $("#doc_detail_"+id+" .doctor_relative .doctor_position").removeClass("doctor_position_show");
    $("#doc_detail_"+id+" .doctor_relative .doctor_name").removeClass("doctor_name_show");
}