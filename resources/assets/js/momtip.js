DESKTOP_LIST = 6;
MOBILE_LIST = 3;
LIST = 6;

$(document).ready(function(){
	initList();
	$( window ).resize(function() {
	});
});

function loadMore(){
	$("#loadmore").click(function(){
		callAjaxList(getList(), LIST);
	})
}
function initList(){
	checkListLimit();
	loadMore();
	callAjaxList(0,LIST);
}

function checkListLimit(){
	LIST = DESKTOP_LIST;
	if(isMobileDevice()){
		LIST = MOBILE_LIST;
	}
}

function getList(){
	return $("#list-panel .list-item").length;
}

function callAjaxList(page, limit){
	$("#list-panel").append(loadingAjax());
	$.ajax({
		url: BASE_LANG + 'momtip/list',
		type: 'POST',
		data: { 'type': getType(), 'page': page, 'limit': limit},
		dataType: 'json'
	})
	.success(function(d) {
			$(".panel_cat").append(d.sub_panel);
			for (var i in d.sub_panel_list) {
				$("#list-panel_"+d.sub_panel_list[i]).append(d[d.sub_panel_list[i]+"_html"]);
				$("#list-panel_"+d.sub_panel_list[i]).slick({
					dots: false,
			        infinite: false,
			        slidesToShow: 3,
			        slidesToScroll: 1,
			        responsive: [
			            {
			                breakpoint: 767,
			                settings: {
			                    slidesToShow: 1,
			                    slidesToScroll: 1
			                }
			            }
			        ]
				});
			};
			callListFavorite();
			setHeightDescription();
	}).fail(function() {
		$("#list-panel").find(".global-loading").remove();
		$("#list-panel").html(dataNotFound());
		$("#list-content").hide();
	})
}

function loadMoreCheck(page){
	if(page == ''){
		$("#loadmore").hide();
	}else{
		$("#loadmore").show();
	}
}

function getType(){
	return $(".panel_cat").data("type");
}

function setHeightDescription(){
	$(".list-item-momtip").each(function(a,b){
		var this_item = $(b);
		var height = this_item.find(".list-body-title strong").innerHeight();
		var description = this_item.find(".list-body-description");
		if(description.text().trim() != ""){
			var heightMod = Math.round(height / 30);
			if(heightMod == 1){
				description.JJJud({"max_rows":2});
			}else if(heightMod == 2){
				description.JJJud({"max_rows":1});
			}else{
				description.hide();
			}
		}
	});
}