$(document).ready(function(){
	$('#growup-content2-list').slick({
		dots: true,
	  	infinite: true,
	  	slidesToShow: 1,
		slidesToScroll: 1,
		variableWidth: true,
		autoplay: true,
		autoplaySpeed: 8000,
		responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: true,
		        variableWidth: false,
		        autoplaySpeed: 8000
		      }
		    }
		 ]
	});
	growupAnimate();
	slideChange();
});

function slideChange(){
	$('#growup-content2-list').on('beforeChange', function(event, slick, currentSlide, nextSlide){
	  	var nowElm = $(".ball-list").eq(nextSlide);
		animateSlide(nowElm, false);
	  	animateSlideMobile(nextSlide);
	});
	// $('#growup-content2-list').on('afterChange', function(event, slick, currentSlide, nextSlide){
	// });
}


function growupAnimate(){
	$(".ball-list").click(function(){
		var nowElm = $(this);
		animateSlide(nowElm, true);
	})
}

function triggerSlideShow(position){
	$("#growup-content2-list").find(".slick-dots li").eq(position).find("button").click();
}

function animateSlide(elm,trigger){
	var nowElm = elm;
	var nowElmLeft = nowElm.offset().left;
	var parentLeft = $("#growup-content1-body-timeline").offset().left;
	var elmPosition = nowElmLeft - parentLeft;
	var activeLeft = $("#ball-growup-active").offset().left - parentLeft;
	var direction = "+";
	if(activeLeft >= elmPosition){
		direction = "-";
	}
	var elmID = nowElm.index(".ball-list");
	$("#ball-growup-active").animate({
		"left": elmPosition
	}, 500, function() {
		if(trigger){
			triggerSlideShow(elmID);
		}
	});
}

function animateSlideMobile(index){
	var elm = "";
	if(index <= 3){
		elm = $("#mobile-mom-step1");
	}else if(index > 3 && index <= 7){
		elm = $("#mobile-mom-step2");
	}else{
		elm = $("#mobile-mom-step3");
	}
	elm.addClass("list-mobile-mom-active");
	$(".list-mobile-mom").not(elm).removeClass("list-mobile-mom-active");
}