var tmp_start_width = "";

$(document).ready(function(){


	if($(window).width() < 768){
		tmp_start_width = $(window).width();
		initSlide();
	}
	if($(window).width() > 767){
		tmp_start_width = $(window).width();
		destroySlide();
	}
	$(window).resize(function () {
		if(tmp_start_width > 767 && $(window).width() < 768){
			tmp_start_width = $(window).width();
			initSlide();
		}
		if(tmp_start_width < 768 && $(window).width() > 767){
			tmp_start_width = $(window).width();
			destroySlide();
		}
	});
});

function initSlide(){
	$(".question-box").slick({
		dots: true,
		infinite: true,		
	});
}

function destroySlide(){
	if($(".question-box").hasClass("slick-initialized")){
		$(".question-box").slick("unslick");
	}
}