$( document ).ready(function() {
    
	$('.smart_banner_slide').slick({
	  dots: true,
	  infinite: false,
	  speed: 800,
	  slidesToShow: 1,
	  adaptiveHeight: true
	});

	$('.smart_foundation_slide').slick({
	  dots: true,
	  infinite: false,
	  speed: 800,
	  slidesToShow: 1,
	  adaptiveHeight: true
	});

	$('.smart_doc_slide').slick({
	  dots: false,
	  infinite: false,
	  speed: 800,
	  slidesToShow: 1,
	  adaptiveHeight: true
	});

	$('.smart_detail_slide').slick({
	  dots: true,
	  infinite: true,
	  speed: 800,
	  slidesToShow: 3,
	  slidesToScroll: 1,
	  adaptiveHeight: true,
	  responsive: [
	    
	    {
	      breakpoint: 767,
	      settings: {
	        slidesToShow: 1.665,
	        centerMode: true,
	        slidesToScroll: 1
	      }
	    }
	  ]
	});

	

});