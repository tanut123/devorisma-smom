var GLOBAL_WIN_WIDTH = 0;
var GLOBAL_WIN_HEIGHT = 0;
var WIN_WIDTH_GLOBAL = 0;
var WIN_HEIGHT_GLOBAL = 0;

$(document).ready(function () {
   // setViewPort();
   $("#TabNav").show();
   $("#search_page").show();
   if($("#isMemberPage").length == 0){
		$(".footer_signup").show();
	}

	GLOBAL_WIN_WIDTH = $(window).width();
	GLOBAL_WIN_HEIGHT = $(window).height();
	reSizeDevice();
	sharedSubmit();

    var winWidth = $(window).width();
	var winHeight = $(window).height();
	WIN_WIDTH_GLOBAL = winWidth;
	WIN_HEIGHT_GLOBAL = winHeight;
	resizePopup();
    $(window).resize(function(){
    	WIN_WIDTH_GLOBAL = $(this).width();
		WIN_HEIGHT_GLOBAL = $(this).height();
		if(WIN_WIDTH_GLOBAL < 768){
			resizePopup();
		}else{
			resizePopup();
		}
    });
    if($(".user_display").length > 0){
    	checkUser();
    	lockedoutTime()
    }

    webStorage();

    if (typeof(Storage) !== "undefined") {
        if (sessionStorage.getItem("cookiePolicy") != "" && sessionStorage.getItem("cookiePolicy") != null ) {
            $('.cookie-policy-panel').hide();
        }
    }

    $(".cookie-policy-panel .btn-confirm").on("click",function(){
    	sessionStorage.setItem("cookiePolicy", "closed");
    	$('.cookie-policy-panel').hide();
    });

});

function lockedoutTime() {
	setTimeout(function(){
		$.ajax({
			url: BASE_LANG + 'sign_in/logout',
			data: {
				ux:$("#email_inp").val(),
				up:$("#pwd_inp").val(),
				ucsrf:csrf_token
			},
			type: 'POST',
			dataType: 'json',
			success: function(resp) {
				location.reload();
			}
		});
	},1200 * 1000);
}


var _mobile = {
	userAgent: navigator.userAgent.toLowerCase(),
	checkDevice: function(deviceName){
		return _mobile.userAgent.match(deviceName)==null ? false : true;
	},
	iphone: function(){ return _mobile.checkDevice(/iphone/); },
	ipod: function(){ return _mobile.checkDevice(/ipod/); },
	ipad: function(){ return _mobile.checkDevice(/ipad/); },
	android: function(){ return _mobile.checkDevice(/android/); },
	blackberry: function(){ return _mobile.checkDevice(/blackberry/); },
	palm: function(){ return _mobile.checkDevice(/palm/); },
	symbian: function(){ return _mobile.checkDevice(/symbian/); },
	ios: function(){ return _mobile.iphone() || _mobile.ipod() || _mobile.ipad(); },
	smartphone: function(){ return _mobile.ios() || _mobile.symbian() || _mobile.blackberry() || _mobile.android(); }
}

var	resizeTimeout;
function setViewPort(){
	clearTimeout(resizeTimeout);
	resizeTimeout = setTimeout(function() {
			// alert("ss");
		if(!isAndroid()){
			if(_mobile.smartphone()){
				var content_width, screen_dimension;
				if (window.orientation == 0 || window.orientation == 180) {
					var win_width = $(window).width();
					// alert(win_width);
					if(win_width <=768){
						content_width =1280;
					}else{
						content_width =1280;
						// content_width = "device-width";
					}
					// alert(content_width+"|1");
					screen_dimension = screen.width ; // fudge factor was necessary in my case
				} else if (window.orientation == 90 || window.orientation == -90) {
				// landscape
					var win_width = $(window).width();
					// alert(win_width);
					if(win_width <= 1024){
						content_width = 1280;
						// content_width = "device-width";
					}else{
						content_width = 1280;
					}
					// alert(content_width+"|2");
					screen_dimension = screen.height;
				}
				var viewport_scale = screen_dimension / content_width;
				var meta_an="";
				if(isAndroid()){
					meta_an = "target-densitydpi=device-dpi,";
				}
				var win_width = $(window).width();
				// alert(content_width);
				if(win_width >= 768){
					$('meta[name=viewport]').attr('content',''+meta_an+' width=' + content_width +', initial-scale='+viewport_scale+', minimum-scale=' + viewport_scale + ', maximum-scale=' + viewport_scale);
					$("body").removeClass("loadpage");
				}else{
					$("body").removeClass("loadpage");
				}
			}else{
				$("body").removeClass("loadpage");
			}
		}else{
			$("body").removeClass("loadpage");
		}
	},250);
}
function isAndroid(){
	return navigator.userAgent.match(/Android/i) ? true : false;
}

function isMobileDevice(){
	return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
}

function isRetina(){
	return (window.devicePixelRatio == 2)? true : false;
}

window.alert = function (str,title,setting){
	// Example setting

	// var footer = {foot:{"Submit":close,
	// 						"Cancel":function(){
	// 								alert(1);
	// 							}
	// 						}
	// 				}
	// alert("Hello World","Hello",footer);

	var opts = $.extend(
		{
			title:title,
			body:str,
			foot:  {"Close": close}
		},setting);
	var number_id = createDialog();

	setFooterModal(number_id,opts);

	$("#myModal"+number_id+" .modal-body").html(opts.body);
	$("#myModal"+number_id+" .modal-body img").addClass("img-responsive");

	if(opts.title == "Error"){
		title = opts.title;
		icon = "alert-error.png";
	}else if(opts.title == "Warning"){
		title = opts.title;
		icon = "alert.png";
	}else if(opts.title != "" && typeof(opts.title) != "undefined"){
		title = opts.title;
	}else{
		title = "";
		icon = "alert-information.png";
	}
	$("#myModal"+number_id+" .modal-title").html('<span class="alert-title-img"></span><span class="alert-title">'+title+'</span>');

	$("#myModal"+number_id+"").modal('show');

}

function createDialog(){
	var number_id = Math.round(Math.random()*100000);

	$(".modal").remove();

	var dialog = '<div class="modal fade" id="myModal'+number_id+'" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">';
		dialog += '	<div class="modal-dialog">';
		dialog += ' <div class="modal-content">';
		dialog += '		 <div class="modal-header">';
		// dialog += '		  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		dialog += '			<h4 class="modal-title custom-modal-title">Information</h4>';
		dialog += '		 </div>';
		dialog += '		 <div class="modal-body"></div>';
		dialog += '		 <div class="modal-footer"></div>';
		dialog += '	</div>';
		dialog += '	</div>';
		dialog += '</div>';

	$("body").append(dialog);
	return number_id;
}
var close = "close";
function setFooterModal(number_id,params){
	if(params["config"] === undefined){
		params["config"] = [];
	}
	if(params["foot"] != null){
				var running_id = 0;
				for (var item in params["foot"]) {
					$("#myModal"+number_id+" .modal-footer").append( printButton(item,"middle","btn-"+number_id+"-"+running_id));
					var bindEvent =  params["foot"][item];
					var dataItem = item.toLowerCase();
					if(params["foot"][item] == "close"){
						bindEvent = function(){
							$("#myModal"+number_id+"").modal('toggle');
						}
					}
					var setclass = params["config"][running_id];
					if(setclass == "main"){
						setclass = "sty-btn-main";
					}else if(setclass == "sub"){
						setclass = "sty-btn-sub";
					}else{
						setclass = "sty-btn-default";
					}
					$("#btn-"+number_id+"-"+running_id).addClass(setclass).bind('click',{fn:bindEvent},function(ev){
						ev.data.fn();
						return false;
					});
					running_id++;
				}
			}else{
				$("#myModal"+number_id+" .modal-footer").remove();
			}
}

function printButton(cmd, size, id) {
	var ret = "";
	ret +='<div id="'+id+'" type="button" class="btn btn-default">'+cmd+'</div>';
	return(ret);
}


function reSizeDevice(){
	$(window).resize(function(){
		// if(GLOBAL_WIN_WIDTH != $(this).width()){
			// window.location.reload();
		// }
		GLOBAL_WIN_WIDTH = $(this).width();
		GLOBAL_WIN_HEIGHT = $(this).height();
	});
	$(window).on("orientationchange",function(){
	  	window.location.reload();
	});
}

function sharedSubmit() {
    $("#facebook").click(function() {
        var link = $(this).data("url");
		url = 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(link);
        window.open(url, 'feedDialog', 'width=650,height=380,location=no,menubar=no,scrollbars=yes,resize=yes,status=no,toolbar=no');
    });
    $("#twitter").click(function() {
        var link = encodeURIComponent($(this).data("url"));
        var image = encodeURIComponent($("meta[name='image']").attr("content"));
        var title = $("#detail-title").text();
        url = 'https://twitter.com/intent/tweet?url=' + link + '&text=' + title;
        window.open(url, 'sharer', 'width=680,height=300,location=no,menubar=no,scrollbars=yes,resize=yes,status=no,toolbar=no');
    });

}

function addBookmark(momtip_id,elm){
	$.ajax({
		url: BASE_LANG + 'bookmark/add',
		type: 'POST',
		data: {'momtip_id': momtip_id},
		dataType: 'json'
	})
	.success(function(d) {
		$(".modal").remove();
		if(d["cmd"] == "y"){
			$("#bookmark_count").html(d["count"]);
			var nowElm = $(elm);
			var url = nowElm.data("url");
			var parent = nowElm.parent();
			nowElm.remove();
			if(url != ""){
				parent.append("<a href='" + url + "' class='list-body-link relate-item-detail-link'><img class='list-body-logo' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABsAAAAbCAYAAACN1PRVAAABsklEQVRIib2WvW4CMQzHLXgIRsTWxO5asXRiY6/UnYUFnuEeo7qJgZytLrwGEr3DfgIWtk4sSEgn0QGq0g84rq3y3yLZ+SXxRwxQoXnWaWnmRyo0U8aVCpUmtFeh8rieaeZH86zTqtrrrIrg2sY4UaFShdbGmBrToGDfy6euW7DvGdPAGFMVWqtQaYyTIrh2LZCyH6rQ1pgWy+D6SQKNS/ZJAo1lcH1jWqjQVtkPKyHPD9A0xtQEd8Z+XAX5CWrsxwd/TJ8foHnW2BhTZdzk7O/rQL4qZ3+vjBtjTH80UPZDE9z9FXQKNMHdtyctgmsfYuTH55w14J0KPV18mi8y9mMV2n5KGmOcGNPiUoxepje3yvhqjOFa4CGGtDDGCQAc60ioXAbXr3L+DXAZXF+FynnWacGxYNfXZl5dYJJAQ4XWmvkRqNDsbNb8E/BY+DNQxpUxDerA6gKNaaCMK1ChsmDfqwurAyzY91SoBBPa51PX/Q0MAMACPprQ/lKLyqeua0L7uDeLHbOI2RizzqJ2EICIvREgctcHiPiffZwk0k8NEHkGeVeU6epU0ebGU/3nRPwGRm3+AjGDpggAAAAASUVORK5CYII='></a>");
				$("#manage-favorite").hide();
			}else{
				$("#manage-favorite").show();
			}
		}else{
			alert(BOOKMARK_CANNOT_SAVE);
		}
	});
}

function popupAlert(){
	var html = "<div id='popup-panel' class='bg-popup-alert'>";
			html +="<div id='popup-inner-panel'>";
                html +="<div class='popup-inner-panel-bg'></div>";
				html +="<div id='popup-top-panel'>";
					html +="<div id='popup-inner-top-panel'>";
						html +="<div id='close-popup'>";
						html +="</div>";
							if(LANG == "th"){
								html +="<div id='text-popup-left'>";
								html +="<span id='text-popup1' class='color-gold'>Breastfeeding</span>";
								html +="<span id='text-popup2' class='color-gold'>is the Best</span>";
								html +="<span id='text-popup3' class='color-gold FLighter'>นมแม่ดีที่สุด</span>";
								html +="<span id='text-popup4' class='color-gray FLighter'>มีคุณค่าทางโภชนาการสูงสุด และยังมีภูมิคุ้มกันโรคต่างๆ องค์การอนามัยโลก หรือ WHO แนะนำให้ทารกทานนมแม่เพียงอย่างเดียว 6 เดือน และทานนมแม่ต่อเนื่องไปควบคู่กับอาหารที่เหมาะสมตามวัยจนอายุ 2 ปี หรือมากกว่า S-MomClub ขอร่วมสนับสนุนและส่งเสริมการเลี้ยงลูกด้วยนมแม่</span>";
								html +="</div>";
								html +="<div class='clearfix'></div>";
								html +="<div id='btn-box-panel'>";
									html +="<div id='btn-box-panel-inner'>";
										html +="<a href='javascript:popupCloseEvent();' class='btn FXMed btn-continue'>เข้าสู่เว็บไซต์</a>";
										html +="<a href='https://www.nestle.co.th' class='btn FXMed btn-continue'>ออกจากเว็บไซต์</a>";
									html +="</div>";
								html +="</div>";
							}else{
								html +="<div id='text-popup-left' class='popup-en'>";
								html +="<span id='text-popup1' class='color-gold'>Breastfeeding is the Best</span>";
								html +="<br><span id='text-popup4' class='color-gray FLighter'>WHO recommends mothers worldwide to exclusively breastfeed infants for the child's first six months to achieve optimal growth, development and health. Thereafter, they should be given nutritious complementary foods and continue breastfeeding up to the age of two years or beyond. S-MomClub strictly follows such statement and fully support breastfeeding policy</span>";
								html +="</div>";
								html +="<div class='clearfix'></div>";
								html +="<div id='btn-box-panel'>";
									html +="<div id='btn-box-panel-inner'>";
									html +="<a href='javascript:popupCloseEvent();' class='btn FXMed btn-continue'>Go to website</a>";
									html +="<a href='https://www.nestle.co.th' class='btn FXMed btn-continue'>Leave the site</a>";
									html +="</div>";
								html +="</div>";
							}
						html +="<div id='text-popup-right'>";
							html +="<div class='mom-img'>";
							html +="</div>";
						html +="</div>";

					html +="</div>";
				html +="</div>";
			html +="</div>";
		html +="</div>";

	return html;
}

function createdimPopup(message){
	$("body").append("<div id='dim-panel-popup'>" + message + "<div id='dim-inner-panel'></div></div>").addClass("nav-active");
	$("#close-popup").click(function(){
		popupCloseEvent();
	});
}

function popupCloseEvent(){

	$("#dim-panel-popup").remove();
	$("body").removeClass("nav-active");
    var pathName = window.location.pathname;
    setMemberBreastfeedingPanel("1",pathName);
}

function resizePopup(){
	var heightPopup = 0;

	heightPopup = WIN_HEIGHT_GLOBAL - 50;
	$(".bg-popup-alert").find("#popup-inner-top-panel").height(heightPopup);
}

function webStorage(){
	var currentDate = moment().format('YYYYMMDD');
	var hideDate = "";
	if(typeof(Storage) !== "undefined") {
		if(localStorage.memberPopup){
			if(currentDate >= localStorage.memberPopup){
				localStorage.removeItem('memberPopup');
			}else{
			}
		}else{
			if($('#login_bar .panel_align .case3').is(":visible")){
				hideDate = moment().add(30, 'days').format('YYYYMMDD');
				localStorage.memberPopup = hideDate;
			}else{
			}
		}
	}
}

function checkUser(){
	setInterval(function(){
		$.ajax({
			url: BASE_URL + '/m/chk',
			dataType: 'json'
		})
		.success(function(data) {
			if(data.d == 'e'){
				window.location.reload();
			}
		});
	},30000)
}