var checkFormat = false;
var formClick = false;
var mode = $("input[name=mode]").val();
var winWidth = $( window ).width();
$(document).ready(function(){
	changePosition();
	if(mode == 'view'){
		viewData();
	}else{
		initForm();
	}
	eatingChoice();
});

$( window ).resize(function() {
	winWidth = $( window ).width();
	changePosition();
});

function initForm(type){
	$('#register-form input:radio , #register-form input:text , #register-form input:password').removeProp('disabled');
	$('.btn-edit').hide();
	$('.btn-submit').show();
	SendData();
	dropDownClick();
	validateForm();
	selectRadioButton();
	if($("#allow").val() != 2){
		clickAllowMemberCard();
	}
	setupCalendar();
    checkPasswordInput();
	captchaRefresh();
	chooseMomStatus(type);
}

function viewData(){
	$('#register-form input:radio , #register-form input:text , #register-form input:password').prop('disabled', true);
	$('.btn-submit').hide();
	$('.btn-edit').show();
	$('.btn-edit').click(function(){
		$("input[name=mode]").val('edit');
		$('.main-title').children('span:first').text('แก้ไข');
		$('.main-title').children('span:last').text('ข้อมูล');
		$("html, body").animate({ scrollTop: 0 }, "slow");
		initForm('edit');
	});
}

function changePosition(){
	$('#box-fname').after($('#box-mobile'));
	$('#box-birthday').before($('#box-lname'));
	$('#bfAllow').after($('.box-allow'));
	$('#box-road').after($('#box-moo'))
	$('#box-road2').after($('#box-moo2'))
	if(isMobileDevice() || winWidth < 768){
		var isiPad = navigator.userAgent.match(/iPad/i) != null;
		if(!isiPad){
			$('#box-fname').after($('#box-lname'));
			$('#box-birthday').before($('#box-mobile'));
			$('.register-panel-top-right').before($('.box-allow'));
			$('#box-road').before($('#box-moo'))
			$('#box-road2').before($('#box-moo2'))
		}
	}
}



function captchaRefresh() {
	$(".captcha_refresh").click(function() {
		var date_now = new Date();
		var date_string = date_now.toISOString();
		var date_split = date_string.split(".");
		var date_string_final = date_split[0].replace(/-/ig,"").replace("T","").replace(/:/ig,"");
		var url = BASE_URL+"/captcha/"+date_string_final;
		$("#captcha_img").attr("src",url);
	});
}
function checkPasswordInput(){
	var mode = $("input[name=mode]").val();
	if(mode == "edit"){
		$("#password").removeClass("validate[required, funcCall[checkValidatePassword]]");
		$("#confirm-password").removeClass("validate[required,equals[password]]");
		$("#old-password").removeClass("validate[required]");
		$("#password").on("keyup",function(){
		if($(this).val() != ""){
			$("#password").addClass("validate[required, funcCall[checkValidatePassword]]");
			$("#confirm-password").addClass("validate[required,equals[password]]");
			$("#old-password").addClass("validate[required]");
		}else{
			$("#password").removeClass("validate[required, funcCall[checkValidatePassword]]");
			$("#confirm-password").removeClass("validate[required,equals[password]]");
			$("#old-password").removeClass("validate[required]");
		}
			checkStrength($(this).val());
	})
	} else {
		$("#password").on("keyup",function(){
			checkStrength($(this).val());
		})
	}

}

function dropDownClick(){
	$(".dropdown-list").DDown({
		afterSelectList: function(elm,list,firstInit){
			var dataLength = $('.panel-information-kid').length;
			var dataVal = ((list.data("val")=="")?0:list.data("val"));
			var dataResult = dataVal - dataLength;
			var listhtml = '';
			var count = 0;
			var tab_index = 11;
			$('#check-ddown').val(dataVal);
			if(dataVal >= dataLength && dataResult >= 0 ){
				tab_index = 12 + (dataLength*2);
				count = dataLength * 2;
				for (var i = 1 ; i <= dataResult; i++) {
					++dataLength;
					++count;
					listhtml = '<div class="panel-information-kid"><div class="panel-information-kid-left"><div class="panel-number FLighter">'+dataLength+'.</div><div class="radio-panel"><div class="radio-box"><input class="input-radio list-check" type="radio" name="mom_status_choice['+dataLength+']" id="d'+count+'" value="1"><label for="d'+count+'" class="text-radio FLighter"><span></span>'+PREGNANT+'</label></div><div class="radio-box"><input class="input-radio list-check" type="radio" name="mom_status_choice['+dataLength+']" id="d'+(++count)+'" value="2"><label for="d'+count+'" class="text-radio FLighter"><span></span>'+GIVE_BIRTH+'</label></div></div><input type="text" class="mom_status hidden-value validate[required]" name="mom_status['+dataLength+']" tabindex="'+(++tab_index)+'" ></div><div class="panel-information-kid-right"><input type="text" class="input-data calendar validate[required] datepicker FLighter" name="child_date['+dataLength+']" tabindex="'+(++tab_index)+'" readonly><div class="text-date-child FLighter"></div><div class="location-box"><input type="text" class="input-data location validate[required] FLighter" name="antenatal_place['+dataLength+']" tabindex="'+(++tab_index)+'" placeholder="ตัวอย่าง รพ.บำรุงราษฎร์"><div class="text-location-child FLighter"></div></div></div>';
					$('#register-panel-bottom-child').append(listhtml);
					chooseMomStatus();
				};
			} else {
				$('#register-panel-bottom-child').html('');
				for (var i = 1 ; i <= dataVal; i++) {
					++count;
					listhtml = '<div class="panel-information-kid"><div class="panel-information-kid-left"><div class="panel-number FLighter">'+i+'.</div><div class="radio-panel"><div class="radio-box"><input class="input-radio list-check" type="radio" name="mom_status_choice['+i+']" id="d'+count+'" value="1"><label for="d'+count+'" class="text-radio FLighter"><span></span>'+PREGNANT+'</label></div><div class="radio-box"><input class="input-radio list-check" type="radio" name="mom_status_choice['+i+']" id="d'+(++count)+'" value="2"><label for="d'+count+'" class="text-radio FLighter"><span></span>'+GIVE_BIRTH+'</label></div></div><input type="text" class="mom_status hidden-value validate[required]" name="mom_status['+i+']" tabindex="'+(++tab_index)+'" ></div><div class="panel-information-kid-right"><input type="text" class="input-data calendar validate[required] datepicker FLighter" name="child_date['+i+']" tabindex="'+(++tab_index)+'" readonly><div class="text-date-child FLighter"></div><div class="location-box"><input type="text" class="input-data location validate[required] FLighter" name="antenatal_place['+i+']" tabindex="'+(++tab_index)+'" placeholder="ตัวอย่าง รพ.บำรุงราษฎร์" ><div class="text-location-child FLighter"></div></div></div>';
					$('#register-panel-bottom-child').append(listhtml);
					chooseMomStatus();
				};
			}
			$("#check-ddown").validationEngine("validate");
			$("#captcha_code").validationEngine("updatePromptsPosition");
		}
	});
}

function checkStrength(password){
/*		var strength = 0
		if (password.length >= 10 && password.length <= 20) strength += 10
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1
		if (password.match(/([A-Z])/) && password.match(/([a-z])/))  strength += 1
		if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
		 if (strength == 0 && password.length != 0) {
			$('.lv1').css('background-color','red');
			$('.lv2 , .lv3 , .lv4').css('background-color','#e3d9bf');
			checkFormat = false;
		}else if (strength == 1 || strength == 10) {
			$('.lv1 , .lv2').css('background-color','orange');
			$('.lv3 , .lv4').css('background-color','#e3d9bf');
			checkFormat = false;
		} else if (strength >= 2 && strength < 12 ) {
			$('.lv1 , .lv2 , .lv3').css('background-color','#ecd077');
			$('.lv4').css('background-color','#e3d9bf');
			checkFormat = false;
		} else if (strength >= 12){
			$('.lv1 , .lv2 , .lv3 , .lv4').css('background-color','green');
			checkFormat = true;
		} else {
			$('.lv1 , .lv2 , .lv3 , .lv4').css('background-color','#e3d9bf');
			checkFormat = false;
		}
		return checkFormat;*/
	var checkPass = true;
	var level = 5;
	if (!password.match(/([a-z])/)) {
		checkPass = false;
		level--;
	}
	if (!password.match(/([A-Z])/)) {
		checkPass = false;
		level--;
	}
	if (!password.match(/([0-9])/)) {
		checkPass = false;
		level--;
	}
	if (!password.match(/([ !"#$%&'()*+,-.\/:;<=>?@[\]^_`{|}~])/)) {
		checkPass = false;
		level--;
	}
	if (password.length > 0 && password.length < 10) {
		checkPass = false;
		level--;
	}
	$('.lv1 , .lv2 , .lv3 , .lv4').css('background-color','#e3d9bf');
	var backgroundColor = new Array();
	backgroundColor[0] = "#e3d9bf";
	backgroundColor[1] = "red";
	backgroundColor[2] = "red";
	backgroundColor[3] = "orange";
	backgroundColor[4] = "#ecd077";
	backgroundColor[5] = "green";
	var displayLevel;
	if (level > 4) {
		displayLevel = 4;
	} else {
		if (password.length > 0) {
			displayLevel = level;
		} else {
			displayLevel = 0;
		}
	}
	for (var i = 1;i<=displayLevel;i++) {
		$(".lv"+i).css('background-color',backgroundColor[level]);
	}
	return checkPass;
}

function selectRadioButton(){
		$('#t1').click(function(){
			$('#title').val($(this).val());
		});
		$('#t2 , #t3').click(function(){
			$('#title').val($(this).val());
		});
		$('#g1').click(function(){
			$('#gendar').val($(this).val());
		});
		$('#g2').click(function(){
			$('#gendar').val($(this).val());
		});
		$('#status-mom-choice1,#status-mom-choice2').click(function(){
			$('#status_mom').val($(this).val());
			var e = '<div class="panel-eating-choice">';
				e += '<span class="data-left FLighter">คุณแม่ทานนมบำรุงครรภ์หรือไม่ :</span>';
				e += '<span class="data-right">';
				e += '<div class="radio-box ">';
				e += '<input class="input-radio" type="radio" name="eating-choice" id="eat1" value="T"><label for="eat1" class="text-radio FLighter" ><span></span>ทาน</label>';
				e += '</div>';
				e += '<div class="radio-box ">';
				e += '<input class="input-radio" type="radio" name="eating-choice" id="eat2" value="F"><label for="eat2" class="text-radio FLighter" ><span></span>ไม่ทาน</label>';
				e += '</div>';
				e += '<input type="text" class="hidden-value validate[required]" name="eating" id="eating" value="">';
				e += '</span>';
				e += '<span class="clearfix"></span>';
				e += '</div>';
				e += '<div class="panel-eating-text"></div>';
			$(".panel-eating").html(e);
			eatingChoice();
		});
		$('#status-mom-choice3').click(function(){
			$('#status_mom').val($(this).val());
			$(".panel-eating").html("");
		});
}

	function eatingChoice(){
		$('#eat1').click(function(){
			$('#eating').val($(this).val());
			var html = '<span class="data-left FLighter">ยี่ห้อนมบำรุงครรภ์ :</span>';
				html += '<span class="data-right">';
				html += '<input class="input-data FLighter validate[required]" type="text" id="eating_text" name="eating_text">';
				html += '</span>';
			$(".panel-eating-text").html(html);
		});
		$('#eat2').click(function(){
			$('#eating').val($(this).val());
			$(".panel-eating-text").html("");
		});
	}

	function checkValidatePassword(field, rules, i, options){
		if(!checkStrength(field.val())) {
			return MEMBER_PASSWORD_VALID;
		}
	}

function SendData(){
	$('.btn-submit').click(function(){
		formClick = true;
		$('#register-form').validationEngine('validate');
	})
}

function clickAllowMemberCard(){
	$('.icon-allow').click(function(){
    	if($(this).hasClass('allow')){
    		$(this).removeClass('allow');
    		$('#allow').val('0');
    	} else {
	    	$(this).addClass('allow');
	    	$('#allow').val('1');
    	}
    })
}

function validateButton(){
	$('input:radio').change(
	    function(){
	    	var inp_radio = $(this).parent().nextAll().last();
    		setTimeout(function(){
				inp_radio.validationEngine('validate');
    		},500)
	    })
}

function validateForm(){
	var submitForm = $("#register-form");
	submitForm.validationEngine('detach');
	submitForm.validationEngine('attach', {
        promptPosition: "bottomLeft",
        showOneMessage: true,
        addFailureCssClassToField: "error",
        scroll: false,
        'custom_error_messages': {
        	'#displayname': {
                'required': {
                    'message': MEMBER_DISPLAYNAME_REQUIRED
                }
            },
            '#email': {
                'required': {
                    'message': MEMBER_EMAIL_REQUIRED
                },
                'custom[email]': {
                    'message': MEMBER_EMAIL_VALID
                }
            },
            '#password': {
                'required': {
                    'message': MEMBER_PASSWORD_REQUIRED
                }
            },
            '#confirm-password': {
                'required': {
                    'message': MEMBER_PASSWORD_REQUIRED
                },
                'equals': {
                    'message': MEMBER_CONFIRM_PASSWORD_VALID
                }
            },
            '#title': {
                'required': {
                    'message': MEMBER_TITLE_REQUIRED
                }
            },
             '#firstname': {
                'required': {
                    'message': MEMBER_FIRSTNAME_REQUIRED
                },
                'custom[onlyLetterNumberCus]': {
                    'message': MEMBER_FIRSTNAME_VALID
                }
            },
             '#lastname': {
                'required': {
                    'message': MEMBER_LASTNAME_REQUIRED
                },
                'custom[onlyLetterNumberCus]': {
                    'message': MEMBER_LASTNAME_VALID
                }
            },
            '#gendar': {
                'required': {
                    'message': MEMBER_GENDAR_REQUIRED
                }
            },
            '#mobile': {
                'required': {
                    'message': MEMBER_MOBILE_REQUIRED
                },
                'custom[phone]': {
                    'message': MEMBER_MOBILE_VALID
                }
            },
            '#birthday': {
                'required': {
                    'message': MEMBER_DATE_REQUIRED
                }
            },
            '.calendar': {
                'required': {
                    'message': MEMBER_DATE_REQUIRED
                }
            },
            '#check-ddown': {
                'required': {
                    'message': MEMBER_CHILD_REQUIRED
                }
            },
            '.mom_status': {
                'required': {
                    'message': MEMBER_CHILD_TYPE_REQUIRED
                }
            },
            '#captcha_code': {
                'required': {
                    'message': MEMBER_CAPTCHA_REQUIRED
                }
            },
            '#status_mom': {
                'required': {
                    'message': MEMBER_MOM_STATUS_REQUIRED
                }
            },
            '#eating': {
                'required': {
                    'message': 'กรุณาเลือกตัวเลือก'
                }
            },
            '#eating_text': {
                'required': {
                    'message': 'กรุณากรอกยี่ห้อนมบำรุงครรภ์'
                }
            },

        },
        onValidationComplete: function(form, status) {
        	if(status && formClick){
	        	var data = $("#register-form").serializeArray();
				$.ajax({
					url: BASE_LANG + "register/register",
					data: data,
					type: "POST",
					cache: false,
					dataType: 'json',
					success: function(d){
						if(d["cmd"] == "y"){
							if($("input[name=mode]").val() == "edit"){
								var footer = {foot: {}};
								footer.foot[REGISTER_BUTTON_BACK_TO_HOME] = function(){window.location.href = BASE_LANG + "home";};
								footer.foot[REGISTER_BUTTON_EDIT] = close;
								alert(REGISTER_EDIT_SUCCESS,"",footer);
							}else{
								var footer = {foot:{"ตกลง":function(){
												window.location.href = BASE_LANG + "home";
											}
										}
								}
								alert(REGISTER_COMPLETE_MESSAGE,"",footer);
							}
						}else{
							if(d["cmd"] == "ERR_03" || d["cmd"] == "ERR_06"){
								var footer = {foot:{"ตกลง":function(){
												window.location.href = BASE_LANG + "home";
											}
										}
								}
								alert(REGISTER_COMPLETE_MESSAGE,"",footer);
							}else { // ERR_CAPTCHA , 02 ,04
								alert(d["message"]);
							}
						}
					}
				})
			} else {
				formClick = false;
			}

        }
    });
 	validateButton();
}

function setupCalendar(){
	$( ".input-date" ).datepicker({
	  changeMonth: true,
	  changeYear: true,
	  dateFormat: "dd/mm/yy",
	  defaultDate: "01/01/2002",
	  yearRange: "-80:-13",
	});
	$(".calendar.pregnant").datepicker({
	  changeMonth: true,
	  changeYear: true,
	  dateFormat: "dd/mm/yy",
	  maxDate: "+280D",
	  minDate: '0'
	});
	$(".calendar.lactating").datepicker({
	  changeMonth: true,
	  changeYear: true,
	  dateFormat: "dd/mm/yy",
	  yearRange: "-10:+0",
	  maxDate: '0'
	});
}

function chooseMomStatus(type){
	$('.list-check').click(function(){
			var parent = $(this).parents('.panel-information-kid');
			var val = $(this).val();
			parent.find('.mom_status').val(val);
			var calendar = parent.find('.calendar');
			calendar.val("");
			if(val == 1){
				if(calendar.hasClass('lactating')){
					$(".calendar.lactating").datepicker("destroy");
					calendar.removeClass('lactating');
				}
				parent.addClass('pregnant');
				calendar.addClass('pregnant');
				parent.find(".location-box").addClass('pregnant');
				parent.find('.text-date-child').html(PREGNANT_DATE);
				parent.find('.text-location-child').html("สถานที่ฝากครรภ์");
			} else if (val == 2) {
				if(calendar.hasClass('pregnant')){
					$(".calendar.pregnant").datepicker("destroy");
					parent.removeClass('pregnant');
					calendar.removeClass('pregnant');
					parent.find(".location-box").removeClass('pregnant');
					parent.find(".location").val("");

				}
				parent.find('.calendar').addClass('lactating');
				parent.find('.text-date-child').html(CHILD_BIRTHDATE);
				parent.find('.text-location-child').html("");
			}
	setupCalendar();
	})
	if(type != "edit"){
		// $(".list-mom-type:checked").click();
	}
}