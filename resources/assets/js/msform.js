var trigle = 0;
var checkStatus1 = false;
var checkStatus2 = false;
var checkStatus3 = false;
var modal_Status = false;
var objData = {
    "firstname": "", //ชื่อ
    "lastname" : "", //นามสกุล
    "birthday" : "", //วันเกิด
    "mobile" : "", //เบอร์โทร
    "occupation" : "", //อาชีพ
    "salary" : "", //เงินเดือน
    "status_mom": "", //สถานะแม่
    "has_one_child": "F", //มีลูกกี่คน
    "address" : "", //หมู่
    "moo" : "", //หมู่
    "alley" : "", //ซอย
    "road" : "", //ถนน
    "sub_district" : "", //ตำบล
    "district" : "", //อำเภอ
    "province" : "", //จังหวัด
    "zipcode" : "", //จังหวัด
    "email" : "", //อีเมล
    "line_id" : "", //ไลน์
    "current_product" : "", //current_mom_product
    "reason_to_change" : "", //reason_to_change
    "previous_product" : "", //previous_product
    "web_page" : "",
    "smom_product_name" : "",
    "smom_product_name_other" : "",
    "advertisement": ""
};


$(document).ready(function(){

    $('#checkStatus1').unbind('click');
    $('#checkStatus2').unbind('click');
    $('#checkStatus3').unbind('click');

    ModalPoricy();

    $("#checkStatus1").click(function () {
        checkStatus1 =  $("#checkStatus1").prop("checked");
        if(checkStatus1 == true && checkStatus2 == true && checkStatus3 == true) {
            var modal = document.getElementById('varlidate-status-mom');
            modal.style.display = "none";
        }
    });

    $("#checkStatus2").click(function () {
        checkStatus2 =  $("#checkStatus2").prop("checked");
        if(checkStatus1 == true && checkStatus2 == true && checkStatus3 == true) {
            var modal = document.getElementById('varlidate-status-mom');
            modal.style.display = "none";
        }
    });

    $("#checkStatus3").click(function () {
        checkStatus3 =  $("#checkStatus3").prop("checked");
        if(checkStatus1 == true && checkStatus2 == true && checkStatus3 == true) {
            var modal = document.getElementById('varlidate-status-mom');
            modal.style.display = "none";
        }
    });


    conditionValidation();

    $('.next').click(function(e){
        var pos = $('#msform').offset().top;
        $('HTML, BODY').stop().animate({scrollTop: pos}, 1000);
        // console.log("working!!!!!");
        conditionValidationFromsFocusout("all");
        return false;
    });

    $('input[type=radio][name=status_mom]').change(function() {

        var check = this.value;
        $('.next').unbind('click');
        $('.previous').unbind('click');

        $('.next').click(function(e){
            var pos = $('#msform').offset().top;
            $('HTML, BODY').stop().animate({scrollTop: pos}, 1000);

            var call = conditionValidationFromsFocusout("all");
            var trigle = $('#trigle').val();
            var status_mom = $('input[name=status_mom]:checked').val();
            if(check == 1 && call == true) {
                if(trigle == 0) {
                    $( "#frist-fieldset" ).removeClass( "show" );
                    $("#Pregnant_mothers1").addClass('show');
                    $('.now-progressbar').addClass('step-second-progress').find('.text-box').text('2/3');
                    $('.progressbar-active').addClass('step-second');
                    trigle++;
                    $("#trigle").val(trigle);
                    $("#status_mom").val(status_mom);
                }else if(trigle == 1) {
                    $( "#Pregnant_mothers1" ).removeClass( "show" );
                    $("#Pregnant_mothers2").addClass('show');
                    $('.now-progressbar').addClass('step-third-progress').find('.text-box').text('3/3');
                    $('.progressbar-active').addClass('step-third');
                    trigle++;
                    $("#trigle").val(trigle);
                    $("#status_mom").val(status_mom);
                }
            }else if(check == 2 && call == true) {
                if(trigle == 0) {
                    $( "#frist-fieldset" ).removeClass( "show" );
                    $( "#Status_of_childs1" ).addClass( "show" );
                    $('.now-progressbar').addClass('step-second-progress').find('.text-box').text('2/3');
                    $('.progressbar-active').addClass('step-second');
                    trigle++;
                    $("#trigle").val(trigle);
                    $("#status_mom").val(status_mom);
                }else if(trigle == 1) {
                    $( "#Status_of_childs1" ).removeClass( "show" );
                    $( "#Status_of_childs2" ).addClass('show');
                    $('.now-progressbar').addClass('step-third-progress').find('.text-box').text('3/3');
                    $('.progressbar-active').addClass('step-third');
                    trigle++;
                    $("#trigle").val(trigle);
                    $("#status_mom").val(status_mom);
                }
            }
        });

        $('.previous').click(function(e){
            var trigle = $('#trigle').val();
            var pos = $('#msform').offset().top;
            $('HTML, BODY').stop().animate({scrollTop: pos}, 1000);

            if(check == 1) {
                if(trigle == 1) {
                    $( "#Pregnant_mothers1" ).removeClass( "show" );
                    $("#frist-fieldset").addClass('show');
                    $('.now-progressbar').removeClass('step-second-progress').find('.text-box').text('1/3');
                    $('.progressbar-active').removeClass('step-second');
                    trigle--;
                    $("#trigle").val(trigle);
                    $("#status_mom").val('');
                }
            }else if(check == 2) {
                if(trigle == 1) {
                    $( "#Status_of_childs1" ).removeClass( "show" );
                    $("#frist-fieldset").addClass('show');
                    $('.now-progressbar').removeClass('step-second-progress').find('.text-box').text('1/3');
                    $('.progressbar-active').removeClass('step-second');
                    trigle--;
                    $("#trigle").val(trigle);
                    $("#status_mom").val('');
                }
            }
        });

        $(".submit").click(function(){
            var pos = $('#joinsmom').offset().top;
            $('HTML, BODY').stop().animate({scrollTop: pos}, 1000);
            var submitCall = conditionValidationFromsFocusout("all");
            if(submitCall == true) {
                var status_mom = $('input[name=status_mom]:checked').val();
                if(status_mom == 1) {
                    var fieldset1 =  $("#frist-fieldset").serializeArray();
                    var fieldset2 =  $("#Pregnant_mothers1").serializeArray();
                    var fieldset3 =  $("#Pregnant_mothers2").serializeArray();
                    setValueFrm1(fieldset1);
                    setValueFrm2(fieldset2);
                    setValueFrm3(fieldset3);
                }else {
                    var fieldset1 =  $("#frist-fieldset").serializeArray();
                    var fieldset2 =  $("#Status_of_childs1").serializeArray();
                    var fieldset3 =  $("#Status_of_childs2").serializeArray();
                    var arrChild =  [];
                    $.each($("#Status_of_childs1 .clone-form .element-detail"), function(k,v){
                        var firstname = $(this).find('input[name="firstname[]"]');
                        var lastname = $(this).find('input[name="lastname[]"]');
                        var child_date = $(this).find('input[name="child_date[]"]');
                        var gender = $(this).find('select[name="gender[]"]');
                        var hospital = $(this).find('select[name="child_hospital[]"]');
                        var province = $(this).find('select[name="child_province[]"]');
                        var treatment = $(this).find('select[name="child_treatment[]"]');
                        var inputCheck = $(this).find('.chk_hospital');
                        var value_elm = $(this).find('input[name="hospital_type_bd"]:checked');
                        if(inputCheck.length > 0) {
                            var id = inputCheck.attr('data-id');
                            // console.log(id);
                            var hospital_type = $(this).find('input[name="hospital_type_bd'+id+'"]:checked');
                        }else {
                            var hospital_type = value_elm;
                        }
                        // var d = new Date();
                        // var n = d.getFullYear();
                        // var m = d.getMonth() + 1;
                        // var selfdate = "";
                        // var selfdatesplit = "";

                        var child = {
                            child_date:"",
                            firstname:"",
                            lastname:"",
                            gender:"",
                            hospital:"",
                            hospital_type:"",
                            calu_age:"",
                            province:"",
                            treatment_rights:""
                        };
                        child.child_date = child_date.val();
                        child.gender = gender.val();
                        child.province = province.val();
                        child.hospital = hospital.val();
                        child.lastname = lastname.val();
                        child.firstname = firstname.val();
                        child.treatment_rights = treatment.val();
                        child.hospital_type = hospital_type.val();
                        child.calu_age = Cul_Date_Age(child_date.val());
                        // selfdate = child_date.val();
                        // selfdatesplit = selfdate.split('/');
                        // child.calu_age = n - selfdatesplit[2];
                        // child.calu_age = ((n * 12)+ m) - ((parseInt(selfdatesplit[2])) * 12 + parseInt(selfdatesplit[1]));
                        arrChild[k] = child;
                    });
                    objData.child = arrChild;

                    setValueFrm1(fieldset1);
                    setValueFrmEx(fieldset2);
                    setValueFrm3(fieldset3);

                }

                $.ajax({
                    url: BASE_URL +'/recruitment/register',
                    data: objData,
                    type: 'POST',
                    dataType: 'json',
                    cache: false,
                    success: function(data) {

                        var organic_product = new Array("16","17","18");
                        if(data.cmd == "y") {
                            if(objData.status_mom == 1) {
                                if(objData.child[0].hospital_type == "F") {
                                    if(objData.child[0].treatment_rights == 1) {
                                        if(organic_product.indexOf(objData.current_product) != -1) {
                                            //เข้า case Line กลุ่มแม่ Organic
                                            SuccessCase('case_six');
                                        }else {
                                            //เข้า สแควร์คนท้องคุยกัน
                                            SuccessCase('case_four');
                                        }
                                    }else if(objData.child[0].treatment_rights == 2) {
                                        if(organic_product.indexOf(objData.current_product) != -1) {
                                            //เข้า case Line กลุ่มแม่ Organic
                                            SuccessCase('case_six');
                                        }else{
                                            // สแควร์คนท้องคุยกัน
                                            SuccessCase('case_four');
                                        }
                                    }else if(objData.child[0].treatment_rights == 3) {
                                        if(organic_product.indexOf(objData.current_product) != -1) {
                                            //เข้า case Line กลุ่มแม่ Organic
                                            SuccessCase('case_six');
                                        }else{
                                            // ห้องแม่ท้อง
                                            SuccessCase('case_one');
                                        }
                                    }else {
                                        //กรณีไม่เข้าอะไรเลยดักไว้เพื่อพัง
                                        SuccessCase('case_four');
                                    }
                                }else if(objData.child[0].hospital_type == "T"){
                                    if(objData.child[0].treatment_rights == 1) {
                                        if(organic_product.indexOf(objData.current_product) != -1) {
                                            //เข้า case Line กลุ่มแม่ Organic
                                            SuccessCase('case_six');
                                        }else{
                                            //เข้า สแควร์คนท้องคุยกัน
                                            SuccessCase('case_four');
                                        }
                                    }else if(objData.child[0].treatment_rights == 2) {
                                        if(organic_product.indexOf(objData.current_product) != -1) {
                                            //เข้า case Line กลุ่มแม่ Organic
                                            SuccessCase('case_six');
                                        }else {
                                            //เข้า case_four
                                            SuccessCase('case_four');
                                        }
                                    }else if(objData.child[0].treatment_rights == 3) {
                                        if(organic_product.indexOf(objData.current_product) != -1) {
                                            //เข้า case Line กลุ่มแม่ Organic
                                            SuccessCase('case_six');
                                        }else {
                                            //เข้า case_four
                                            SuccessCase('case_four');
                                        }
                                    }else {
                                        //กรณีไม่เข้าอะไรเลยดักไว้เพื่อพัง
                                        SuccessCase('case_four');
                                    }
                                }
                            }else if(objData.status_mom == 2) {
                                if(objData.has_one_child == "F") {
                                    conditionPopup(objData);
                                } else if(objData.has_one_child == "T") {
                                    conditionPopup(objData);
                                }
                                else {
                                    SuccessCase('testsuccessregister');
                                }
                            }
                        }
                    }
                });

                // $.ajax({
                //     url: BASE_URL +'/recruitment/register',
                //     data: objData,
                //     type: 'POST',
                //     dataType: 'json',
                //     cache: false,
                //     success: function(data) {
                //         if(data.cmd == "y") {
                //             var modal = document.getElementById('testsuccessregister');
                //             modal.style.display = "block";
                //             $('#reback').click(function() {
                //                 location.href = BASE_LANG +'home';
                //             });
                //         }
                //     }
                // });
            }
        });
    });

    $("#quarity_boy").prop('disabled', true); // Warning
    $('input[type=radio][name=has_one_child]').change(function(){
        $('.element-detail').remove();
        $('#quarity_boy').removeClass("border-validation");
        var Choice =  $(this).val();
        MultiAjax(Choice);
    });
    //date input
    $('#birthday').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        yearRange:"c-75:c+0"
    });
    $('#child_date1').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        minDate: new Date()
    });
    $('#child_date1-2').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
    });
    $('#child_dateS').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
    });

    //ส่วน GET ค่ามาแสดง
    GetOccupation();
    GetHospitalProvince();
    GetHospitalName();
    GetProvince();
    GetChildProduct();
    GetProvinceSc();
    GetMomProduct();
    GetPreviousProduct();
    GetReason();
    GetTreatmentRights();
    
    
    //เมื่อมีการเลือกจังหวัดจะสร้าง อำเภอ
    $('#province_address').on('change', function() {
        $('#zipcode1').val("");
        var ElmProvince = $('#select-province').find('#province');
        var idProvince =  ElmProvince.val();
        GetDistrict(idProvince); //API จริง ตัวอย่าง https://s-mom.d.orisma.com/recruitment/list/district?province=กรุงเทพมหานคร
        var id = "sub_district";
        var name = "sub_district";
        var label = "ตำบล/แขวง";
        var alert = "กรุณาระบุตำบล/แขวง";
        var labelselect = "กรุณาระบุตำบล/แขวง";
        var res = createHtmlDropdown(id, name, label, "", alert, labelselect);
        $('#subdistrict').html(res);

        initSelectize($("#"+id));

    });
    //เมื่อมีการเลือกอำเภอจะสร้าง ตำบล
    $('#district_address').on('change', function() {
        $('#zipcode1').val("");
        var ElmProvince = $('#select-province').find('#province');
        var idProvince =  ElmProvince.val();
        var ElmDistrict = $('#select-district').find('#district');
        var idDistrict =  ElmDistrict.val();
        GetSubDistrict(idDistrict); //API fake
        // GetSubDistrict(idProvince, idDistrict); //API จริง ตัวอย่าง https://s-mom.d.orisma.com/recruitment/list/subdistrict?province=กรุงเทพมหานคร&district=คลองสาน
    
    });
    //เมื่อมีการเลือกตำบลจะสร้าง รหัสไปรษณี
    $('#subdistrict').on('change', function() {
        var storagezipcode = $('#storagezipcode').val();
        $('#zipcode1').val(storagezipcode);
    });
    
    //เมื่อมีการเลือกจังหวัดจะสร้าง อำเภอSc
    $('#provinceSc_address').on('change', function() {
        $('#zipcode2').val("");
        var ElmProvince = $('#select-provinceSc').find('#provinceSc');
        var idProvince =  ElmProvince.val();
        GetDistrictSc(idProvince); //API จริง ตัวอย่าง https://s-mom.d.orisma.com/recruitment/list/district?province=กรุงเทพมหานคร
        var id = "sub_districtSc";
        var name = "sub_district";
        var label = "ตำบล/แขวง";
        var alert = "กรุณาระบุตำบล/แขวง";
        var labelselect = "กรุณาระบุตำบล/แขวง";
        var res = createHtmlDropdown(id, name, label, "", alert, labelselect);
        $('#subdistrictSc').html(res);

        initSelectize($("#"+id));

    });
    //เมื่อมีการเลือกอำเภอจะสร้าง ตำบลSc
    $('#districtSc_address').on('change', function() {
        $('#zipcode2').val("");
        var ElmProvince = $('#select-provinceSc').find('#provinceSc');
        var idProvince =  ElmProvince.val();
        var ElmDistrict = $('#select-districtSc').find('#districtSc');
        var idDistrict =  ElmDistrict.val();
        GetSubDistrictSc(idDistrict); //API fake
        // GetSubDistrict(idProvince, idDistrict); //API จริง ตัวอย่าง https://s-mom.d.orisma.com/recruitment/list/subdistrict?province=กรุงเทพมหานคร&district=คลองสาน
    });
        //เมื่อมีการเลือกตำบลจะสร้าง รหัสไปรษณีSc
        $('#subdistrictSc').on('change', function() {
            var storagezipcodeSc = $('#storagezipcodeSc').val();
            $('#zipcode2').val(storagezipcodeSc);
        });

        $( ".from-inputtext, .inline-radio" ).on("change","select", function() {
            if($(this).val() != ""){
                $(this).addClass('selected');
            }else{
                $(this).removeClass('selected');
            }
        });
    numberOnly();

    $('input[type=radio][name=product_name]').change(function () {
        if($(this).val() == 10) {
            if($(this).data("tiger") == 1) {
                var html_ra = '<input type="text" class="text-input-position add-width-input font-xregular validation-input" style="display: inline-block; width: 30%; margin-top: 12px;" id="other-product1" name="other_product" placeholder="กรุณาระบุผลิตภัณฑ์นม">';
                $('.other-elm1').html(html_ra);
                conditionValidation();
            }else {
                var html_ra = '<input type="text" class="text-input-position add-width-input font-xregular validation-input" style="display: inline-block; width: 30%; margin-top: 12px;" id="other-product2" name="other_product" placeholder="กรุณาระบุผลิตภัณฑ์นม">';
                $('.other-elm2').html(html_ra);
                conditionValidation();
            }
        }else {
            $('.other-elm').html("");
            conditionValidation();
        }
    });

});

function detectBrowser() {
    var UA = navigator.userAgent || navigator.vendor || window.opera;

    var isLINE = function(UA){
        return (UA.indexOf("Line/") > -1);
    }
    var isFacebook = function(UA) {
        return (UA.indexOf("FBAN") > -1) || (UA.indexOf("FBAV/") > -1);
    }
    var isOpera = function(UA){
        return (UA.indexOf("Opera") > -1);
    }
    var isChrome = function(UA){
        return (UA.indexOf("Chrome") > -1);
    }
    var isFirefox = function(UA){
        return (UA.indexOf("Firefox") > -1);
    }
    var isSafari = function(UA){
        return (UA.indexOf("Safari") > -1);
    }
    var isMsie = function(UA){
        return (UA.indexOf("Msie") > -1 || UA.indexOf("Edge") > -1 || UA.indexOf("Trident"));
    }

    if(isLINE(UA)) {
        return 'line';
    }
    if(isFacebook(UA)) {
        return 'facebook';
    }
    if(isOpera(UA)) {
        return 'opera';
    }
    if(isChrome(UA)) {
        return 'chrome';
    }
    if(isFirefox(UA)){
        return 'firefox';
    }
    if(isSafari(UA)){
        return 'safari';
    }
    if(isMsie(UA)){
        return 'msie';
    }
    return false;
}

function conditionPopup(objData){
    var tmp_rs = [];
    $.each( objData.child, function( key, value ) {
        if(tmp_rs == "") {
            tmp_rs = value;
        }else if(tmp_rs.calu_age > value.calu_age) {
            tmp_rs = value;
        }
    });
    var organic_product = new Array("128","129","130");
    if(tmp_rs.hospital_type == "F") {
        if(tmp_rs.treatment_rights == 1) {
            if(organic_product.indexOf(objData.current_product) != -1) {
                //เข้า case Line กลุ่มแม่ Organic
                SuccessCase('case_six');
            }else {
                if(tmp_rs.calu_age <= 365) {
                    //เข้า case_five ห้องนมแม่
                    SuccessCase('case_five');
                }else if(tmp_rs.calu_age > 365) {
                    //เข้าดังเดิม
                    SuccessCase('testsuccessregister');
                }else {
                    //เข้าดังเดิม
                    SuccessCase('testsuccessregister');
                }
            }
        }else if(tmp_rs.treatment_rights == 2) {
            if(organic_product.indexOf(objData.current_product) != -1) {
                //เข้า case Line กลุ่มแม่ Organic
                SuccessCase('case_six');
            }else{
                if(tmp_rs.calu_age <= 365) {
                //เข้า case_five ห้องนมแม่
                    SuccessCase('case_five');
                }else{
                    SuccessCase('testsuccessregister');
                }
            }
        }else if(tmp_rs.treatment_rights == 3) {
            if(organic_product.indexOf(objData.current_product) != -1) {
                //เข้า case Line กลุ่มแม่ Organic
                SuccessCase('case_six');
            }else if(objData.current_product == 109) {
                if(tmp_rs.calu_age <= 365) {
                    //เข้า case_two ห้องเบบี๋
                    SuccessCase('case_two');
                }else {
                    //เข้าดังเดิม
                    SuccessCase('testsuccessregister');
                }
            }else {
                if(tmp_rs.calu_age > 365) {
                    //เข้า case_three ห้องเด็กเด็ก
                    SuccessCase('case_three');
                }else {
                    //เข้าดังเดิม
                    SuccessCase('testsuccessregister');
                }
            }
        }
    }else if(tmp_rs.hospital_type == "T") {
        if(tmp_rs.treatment_rights == 1) {
            if(organic_product.indexOf(objData.current_product) != -1) {
                //เข้า case Line กลุ่มแม่ Organic
                SuccessCase('case_six');
            }else {
                if(tmp_rs.calu_age <= 365) {
                    //เข้า case_five ห้องนมแม่
                    SuccessCase('case_five');
                }else {
                    //เข้าดังเดิม
                    SuccessCase('testsuccessregister');
                }
            }
        }else if(tmp_rs.treatment_rights == 2) {
            if(organic_product.indexOf(objData.current_product) != -1) {
                //เข้า case Line กลุ่มแม่ Organic
                SuccessCase('case_six');
            }else {
                if(tmp_rs.calu_age <= 365) {
                    //เข้า case_five ห้องนมแม่
                    SuccessCase('case_five');
                }else {
                    //เข้าดังเดิม
                    SuccessCase('testsuccessregister');
                }
            }
        }else if(tmp_rs.treatment_rights == 3) {
            if(organic_product.indexOf(objData.current_product) != -1) {
                //เข้า case Line กลุ่มแม่ Organic
                SuccessCase('case_six');
            }else {
                if(tmp_rs.calu_age <= 365) {
                    //เข้า case_five ห้องนมแม่
                    SuccessCase('case_five');
                }else {
                    //เข้าดังเดิม
                    SuccessCase('testsuccessregister');
                }
            }
        }
    }
}

function SuccessCase(type){
    var id = "";
    var linklineId = "";
    if(type == 'case_one'){
        id = 'case_one';
        linklineId = 'https://line.me/R/ti/g/_SIEa38OiX';
    }else if(type == 'case_two'){
        id = 'case_two';
        linklineId = 'https://line.me/R/ti/g/9evYiCRM8y';
    }else if(type == 'case_three'){
        id = 'case_three';
        linklineId = 'https://line.me/R/ti/g/GT9eI7W4Dw';
    }else if(type == 'case_four'){
        id = 'case_four';
        linklineId = 'https://line.me/ti/g2/HIOVO7ODZ3';
    }else if(type == 'case_five'){
        id = 'case_five';
        linklineId = 'https://line.me/ti/g2/APWGE1L3C2';
    }else if(type == 'case_six'){
        id = 'case_six';
        linklineId = 'https://line.me/R/ti/g/eDfQEjYqQG';
    }else if(type == 'testsuccessregister'){
        id = 'testsuccessregister';
        // linklineId = 'https://line.me/R/ti/g/eDfQEjYqQG';
    }
    var modal = document.getElementById(id);
    modal.style.display = "block";
    // $('.btnSuccess').click(function() {
    //     location.href = BASE_LANG +'home';
    // });
    // if(type == 'case_one' || type == 'case_two' || type == 'case_three' || type == 'case_four' || type == 'case_five' || type == 'case_six'){
    //     $('.linkOut').click(function() {
    //         location.href = linklineId;
    //     });
    // }
}

function conditionValidation() {
    $(".validation-input").focusout(function(){
        conditionValidationFromsFocusout($(this).attr('id'));
    });

    $(".validation-input").change(function(){
        conditionValidationFromsFocusout($(this).attr('id'));
    });
}

function conditionValidationFromsFocusout(input_id) {
    var status_return = true;
    if(input_id != "all"){ //ตรวจสอบรายตัว
        var elm = $('.validation-input#'+input_id);
        var currentPage = $("#trigle").val(); //ลำดับของหน้า
        var currentStatus = $("#status_mom").val(); //อ้่างอิงว่าหน้านี้ของสถานะไหน
        if(elm.length > 0){
            var type = elm.attr('type');
            var tag = elm.prop("tagName");
            if(currentPage == 0){ //หน้าแรกที่จะเข้าเงื่อนไข
                if(type == "text") {
                    if(input_id == "firstname" || input_id == "lastname") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validateThailanguage(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "birthday") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validateDate(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if (input_id == "mobile" || input_id == "salary") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validatePhone(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }
                }else if(type == "radio") {
                    if(input_id == "status_mom1" || input_id == "status_mom2") {
                        if($('input[name=status_mom]:checked').length <= 0) {
                            $('input[type=radio][name=status_mom]').parent().find('.label-position').addClass('validation-radio');
                            status_return = false;
                        }else {
                            $('input[type=radio][name=status_mom]').parent().find('.label-position').removeClass('validation-radio');
                        }
                    }else if(input_id == "advertisement_one" || input_id == "advertisement_two" || input_id == "advertisement_three" || input_id == "advertisement_four" || input_id == "advertisement_five" || input_id == "advertisement_six" || input_id == "advertisement_seven") {
                        if($('input[name=advertisement]:checked').length <= 0) {
                            $('input[type=radio][name=advertisement]').parent().find('.label-position').addClass('validation-radio');
                            $('input[type=radio][name=advertisement]').parent().find('.title-label').addClass('validation-radio');
                            status_return = false;
                        }else {
                            $('input[type=radio][name=advertisement]').parent().find('.label-position').removeClass('validation-radio');
                            $('input[type=radio][name=advertisement]').parent().find('.title-label').removeClass('validation-radio');
                        }
                    }
                }
                else if(tag == "SELECT") { //เมื่อ type เป็น undefined อาจจะเปลี่ยนมาใช้ tag
                    if(input_id == "occupation") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                            status_return = false;
                        }
                    }
                }else if (type == "email") {
                    if(elm.val() != "") { //กรณีมีค่าส่งมา
                        if(!validateEmail(elm.val())) {
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }else {
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }
                    }else {
                        elm.parent().find('.help-block').removeClass('help-block-show');
                        elm.removeClass("border-validation");
                    }
                }
            }else if (currentPage == 1 && currentStatus == 1) {
                if(type == "text") {
                    if(input_id == "child_date1" || input_id == "child_date1-2") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validateDate(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "child_firstname1") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validateThailanguage(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "address1" || input_id == "moo1" || input_id == "alley1" || input_id == "road1") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "zipcode1") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validatePhone(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }
                }else if(type == "radio") {
                    if(input_id == "hospital_type1" || input_id == "hospital_type2") {
                        if($('input[name=hospital_type]:checked').length <= 0) {
                            $('input[type=radio][name=hospital_type]').parent().find('.label-position').addClass('validation-radio');
                            status_return = false;
                        }else {
                            $('input[type=radio][name=hospital_type]').parent().find('.label-position').removeClass('validation-radio');
                        }
                    }

                }else if (tag == "SELECT") { //เมื่อ type เป็น undefined อาจจะเปลี่ยนมาใช้ tag
                    if(input_id == "child_gender1" || input_id == "child_province" || input_id == "child_hospital" || input_id == "province" || input_id == "district" || input_id == "sub_district" || input_id == "treatment") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                            status_return = false;
                        }
                    }
                }
            }else if(currentPage == 2 && currentStatus == 1) {
                if(type == "text") {
                    if(input_id == "web_page1") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "other-product1" || input_id == "other-product2") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }
                }else if(tag == "SELECT") {
                    if(input_id == "current_product") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                            status_return = false;
                        }
                    }
                }else if (type == "checkbox") {
                    if(input_id == "Checkbox1") {
                        var Checkbox1 =  $('#Checkbox1').prop( "checked" );
                        if(Checkbox1 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }else if(input_id == "Checkbox2") {
                        var Checkbox2 =  $('#Checkbox2').prop( "checked" );
                        if(Checkbox2 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }else if(input_id == "Checkbox3") {
                        var Checkbox3 =  $('#Checkbox3').prop( "checked" );
                        if(Checkbox3 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }
                }
            }else if(currentPage == 1 && currentStatus == 2) {
                if(type == "text") {
                    if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                    }
                }else if(tag == "SELECT") {
                    if(elm.val() != "") { //กรณีมีค่าส่งมา
                        if(input_id == "quarity_boy") {
                            elm.removeClass("border-validation");
                        }else {
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }
                    }else { //กรณีไม่มีค่าส่งมา
                        if(input_id == "quarity_boy") {
                            elm.addClass("border-validation");
                        }else {
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                        }
                        status_return = false;
                    }
                }else if(type == "radio") {
                    if(elm.prop("checked") == false) {
                        elm.parent().parent().find('.label-position').addClass('validation-radio');
                        status_return = false;
                    }else {
                        elm.parent().parent().find('.label-position').removeClass('validation-radio');
                    }
                }
            }else if(currentPage == 2 && currentStatus == 2) {
                if(type == "text") {
                    if(input_id == "web_page2") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "other-product1" || input_id == "other-product2") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }
                }else if(tag == "SELECT") {
                    if(input_id == "current_productSc" || input_id == "previous_product" || input_id == "reason") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                            status_return = false;
                        }
                    }
                }else if (type == "checkbox") {
                    if(input_id == "checkbox_Sc1") {
                        var checkbox_Sc1 =  $('#checkbox_Sc1').prop( "checked" );
                        if(checkbox_Sc1 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }else if(input_id == "checkbox_Sc2") {
                        var checkbox_Sc2 =  $('#checkbox_Sc2').prop( "checked" );
                        if(checkbox_Sc2 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }else if(input_id == "checkbox_Sc3") {
                        var checkbox_Sc3 =  $('#checkbox_Sc3').prop( "checked" );
                        if(checkbox_Sc3 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }
                }
            }
        }
    }else {//ตรวจสอบทั้งหมด
        $.each($(".validation-input"),function(k,v){
            var currentPage = $("#trigle").val(); //ลำดับของหน้า
            var currentStatus = $("#status_mom").val(); //อ้่างอิงว่าหน้านี้ของสถานะไหน
            var input_id = $(v).attr('id');

            var elm = $('.validation-input#'+input_id);
            if(currentPage == 0) {
                var type = elm.attr('type');
                var tag = elm.prop("tagName");
                if(type == "text") {
                    if(input_id == "firstname" || input_id == "lastname") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validateThailanguage(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "birthday") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validateDate(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if (input_id == "mobile" || input_id == "salary") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validatePhone(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }
                }else if(type == "radio") {
                    if(input_id == "status_mom1" || input_id == "status_mom2") {
                        if($('input[name=status_mom]:checked').length <= 0) {
                            $('input[type=radio][name=status_mom]').parent().find('.label-position').addClass('validation-radio');
                            status_return = false;
                        }else {
                            $('input[type=radio][name=status_mom]').parent().find('.label-position').removeClass('validation-radio');
                        }
                    }else if(input_id == "advertisement_one" || input_id == "advertisement_two" || input_id == "advertisement_three" || input_id == "advertisement_four" || input_id == "advertisement_five" || input_id == "advertisement_six" || input_id == "advertisement_seven") {
                        if($('input[name=advertisement]:checked').length <= 0) {
                            $('input[type=radio][name=advertisement]').parent().find('.label-position').addClass('validation-radio');
                            $('input[type=radio][name=advertisement]').parent().find('.title-label').addClass('validation-radio');
                            status_return = false;
                        }else {
                            $('input[type=radio][name=advertisement]').parent().find('.label-position').removeClass('validation-radio');
                            $('input[type=radio][name=advertisement]').parent().find('.title-label').removeClass('validation-radio');
                        }
                    }
                }else if(tag == "SELECT") { //เมื่อ type เป็น undefined อาจจะเปลี่ยนมาใช้ tag
                    if(input_id == "occupation") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                            status_return = false;
                        }
                    }
                }else if (type == "email") {
                    if(elm.val() != "") { //กรณีมีค่าส่งมา
                        if(!validateEmail(elm.val())) {
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }else {
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }
                    }else {
                        elm.parent().find('.help-block').removeClass('help-block-show');
                        elm.removeClass("border-validation");
                    }
                }
            }else if (currentPage == 1 && currentStatus == 1) {
                var type = elm.attr('type');
                var tag = elm.prop("tagName");
                if(type == "text") {
                    if(input_id == "child_date1" || input_id == "child_date1-2") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validateDate(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "child_firstname1") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validateThailanguage(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "address1" || input_id == "moo1" || input_id == "alley1" || input_id == "road1") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "zipcode1") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            if(!validatePhone(elm.val())) {
                                elm.parent().find('.help-block').addClass('help-block-show');
                                elm.addClass("border-validation");
                                status_return = false;
                            }else {
                                elm.parent().find('.help-block').removeClass('help-block-show');
                                elm.removeClass("border-validation");
                            }
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }
                }else if(type == "radio") {
                    if(input_id == "hospital_type1" || input_id == "hospital_type2") {
                        if($('input[name=hospital_type]:checked').length <= 0) {
                            $('input[type=radio][name=hospital_type]').parent().find('.label-position').addClass('validation-radio');
                            status_return = false;
                        }else {
                            $('input[type=radio][name=hospital_type]').parent().find('.label-position').removeClass('validation-radio');
                        }
                    }

                }else if (tag == "SELECT") { //เมื่อ type เป็น undefined อาจจะเปลี่ยนมาใช้ tag
                    if(input_id == "child_gender1" || input_id == "child_province" || input_id == "child_hospital" || input_id == "province" || input_id == "district" || input_id == "sub_district" || input_id == "treatment") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                            status_return = false;
                        }
                    }
                }
            }else if(currentPage == 2 && currentStatus == 1) {
                var type = elm.attr('type');
                var tag = elm.prop("tagName");
                if(type == "text") {
                    if(input_id == "web_page1") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "other-product1" || input_id == "other-product2") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }
                }else if(tag == "SELECT") {
                    if(input_id == "current_product") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                            status_return = false;
                        }
                    }
                }else if (type == "checkbox") {
                    if(input_id == "Checkbox1") {
                        var Checkbox1 =  $('#Checkbox1').prop( "checked" );
                        if(Checkbox1 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }else if(input_id == "Checkbox2") {
                        var Checkbox2 =  $('#Checkbox2').prop( "checked" );
                        if(Checkbox2 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }else if(input_id == "Checkbox3") {
                        var Checkbox3 =  $('#Checkbox3').prop( "checked" );
                        if(Checkbox3 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }
                }else if (type == "radio") {
                    var input_name =  elm.attr("name");
                    if(input_name == "product_name"){
                        if($('input[name=product_name]:checked').length <= 0){
                            $('input[type=radio][name=product_name]').parent().find('.label-position').addClass('validation-radio');
                            status_return = false;
                        }else{
                            $('input[type=radio][name=product_name]').parent().find('.label-position').removeClass('validation-radio');
                        }
                    }
                }
            }else if(currentPage == 1 && currentStatus == 2) {
                var type = elm.attr('type');
                var tag = elm.prop("tagName");
                if(type == "text" || type == "email") {
                    if(input_id == "address2" || input_id == "moo2" || input_id == "alley2" || input_id == "road2" || input_id == "zipcode2") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }

                }else if(tag == "SELECT") {
                    if(input_id == "provinceSc" || input_id == "districtSc" || input_id == "sub_districtSc") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                            status_return = false;
                        }
                    }else if (input_id == "quarity_boy") {
                        if($('input[name=has_one_child]:checked').val() == "F") {
                            if(elm.val() != "") { //กรณีมีค่าส่งมา
                                elm.removeClass("border-validation");
                            }else { //กรณีไม่มีค่าส่งมา
                                elm.addClass("border-validation");
                                status_return = false;
                            }
                        }else {
                            elm.removeClass("border-validation");
                        }
                    }
                }else if(type == "radio") {
                    if(input_id == "has_one_child2" || input_id == "has_one_child3") {
                        if($('input[name=has_one_child]:checked').length <= 0) {
                            $('input[type=radio][name=has_one_child]').parent().find('.label-position').addClass('validation-radio');
                            status_return = false;
                        }else {
                            $('input[type=radio][name=has_one_child]').parent().find('.label-position').removeClass('validation-radio');
                        }
                    }
                }else {
                    var tired  = $('.clone-form').find('input.validation-input , select.validation-input');
                    if(tired.length > 0) {
                        $.each(tired, function(){
                            if($(this).attr('type') == "text") {
                                if($(this).val() != "") { //กรณีมีค่าส่งมา
                                    $(this).parent().find('.help-block').removeClass('help-block-show');
                                    $(this).removeClass("border-validation");
                                }else { //กรณีไม่มีค่าส่งมา
                                    $(this).parent().find('.help-block').addClass('help-block-show');
                                    $(this).addClass("border-validation");
                                    status_return = false;
                                }
                            }
                            else if($(this).prop("tagName") == "SELECT") { //เมื่อ type เป็น undefined อาจจะเปลี่ยนมาใช้ tag
                                if($(this).val() != "") { //กรณีมีค่าส่งมา
                                    $(this).parent().removeClass("border-validation");
                                    $(this).parent().parent().find('.help-block').removeClass('help-block-show');
                                }else { //กรณีไม่มีค่าส่งมา
                                    $(this).parent().addClass("border-validation");
                                    $(this).parent().parent().find('.help-block').addClass('help-block-show');
                                    status_return = false;
                                }
                            }
                            else if ($(this).attr('type') == "radio") {
                                var elmname = $(this).attr("name");
                                if($('input[name='+elmname+']:checked').length <= 0) {
                                    $('input[type=radio][name='+elmname+']').parent().find('.label-position').addClass('validation-radio');
                                    status_return = false;
                                }else {
                                    $('input[type=radio][name='+elmname+']').parent().find('.label-position').removeClass('validation-radio');
                                }
                            }
                        });
                    }
                }
            }else if(currentPage == 2 && currentStatus == 2) {
                var type = elm.attr('type');
                var tag = elm.prop("tagName");
                if(type == "text") {
                    if(input_id == "web_page2") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }else if(input_id == "other-product1" || input_id == "other-product2") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().find('.help-block').removeClass('help-block-show');
                            elm.removeClass("border-validation");
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().find('.help-block').addClass('help-block-show');
                            elm.addClass("border-validation");
                            status_return = false;
                        }
                    }
                }else if(tag == "SELECT") {
                    if(input_id == "current_productSc" || input_id == "previous_product" || input_id == "reason") {
                        if(elm.val() != "") { //กรณีมีค่าส่งมา
                            elm.parent().removeClass("border-validation");
                            elm.parent().parent().find('.help-block').removeClass('help-block-show');
                        }else { //กรณีไม่มีค่าส่งมา
                            elm.parent().addClass("border-validation");
                            elm.parent().parent().find('.help-block').addClass('help-block-show');
                            status_return = false;
                        }
                    }
                }else if (type == "checkbox") {
                    if(input_id == "checkbox_Sc1") {
                        var checkbox_Sc1 =  $('#checkbox_Sc1').prop( "checked" );
                        if(checkbox_Sc1 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }else if(input_id == "checkbox_Sc2") {
                        var checkbox_Sc2 =  $('#checkbox_Sc2').prop( "checked" );
                        if(checkbox_Sc2 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }else if(input_id == "checkbox_Sc3") {
                        var checkbox_Sc3 =  $('#checkbox_Sc3').prop( "checked" );
                        if(checkbox_Sc3 == false) {
                            elm.parent().find('.label-position').addClass('validation-checkbox');
                            status_return = false;
                        }else {
                            elm.parent().find('.label-position').removeClass('validation-checkbox');
                        }
                    }
                }else if (type == "radio") {
                    var input_name =  elm.attr("name");
                    if(input_name == "product_name"){
                        if($('input[name=product_name]:checked').length <= 0){
                            $('input[type=radio][name=product_name]').parent().find('.label-position').addClass('validation-radio');
                            status_return = false;
                        }else{
                            $('input[type=radio][name=product_name]').parent().find('.label-position').removeClass('validation-radio');
                        }
                    }
                }
            }
        });
    }
    return status_return;
}



function validateThailanguage(rs) {
    var re = new RegExp("^([A-Z]|[a-z]|[ๅภถุึคตจขชๆไำพะัีรนยบลฃฟหกดเ้่าสวงผปแอิืทมใฝูฎฑธํ๊ณฯญฐฅฤฆฏโฌ็๋ษศซฉฮฺ์ฒฬฦ])+$", "g");
    return re.test(rs);
}

function validateEmail(rs) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(rs);
}

function validateDate(rs) {
    var re = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
    return re.test(rs);
}

function validatePhone(rs) {
    var re = (/^\d+$/);
    return re.test(rs);
}

function GetOccupation() { //GET อาชีพ
    $.ajax({
        url: BASE_URL +'/recruitment/list/occupation',
        // data : {data: "occupation"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.smom_occupation_id+'">'+v.title+'</option>';
            });
            var id = "occupation";
            var name = "occupation";
            var label = "อาชีพ"; 
            var alert = "กรุณาระบุอาชีพ";
            var labelselect = "กรุณาระบุอาชีพ";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#render-occupation').html(res);
            conditionValidation();
            ModalPoricy();
            // initDropdown(id);
            initSelectize($("#"+id));
            
        }
    });
}

function initDropdown(id){
    if($("#"+id).length > 0){
        $( ".from-inputtext #"+id ).change( function() {
            if($(this).val() != ""){
                $(this).addClass('selected');
            }else{
                $(this).removeClass('selected');
            }
         });
    }
    
}

function GetHospitalProvince() { //GET จังหวัดของโรงพยาบาล
    $.ajax({
        url: BASE_URL +'/recruitment/list/province',
        // data : {data: "province"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.smom_address_id+'">'+v.province+'</option>';
            });
            var id = "child_province";
            var name = "child_province[]";
            var label = "จังหวัด"; 
            var alert = "กรุณาระบุจังหวัด";
            var labelselect = "กรุณาระบุจังหวัด";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#hospital_province').html(res);
            conditionValidation();

            initSelectize($("#"+id));
        }
    });
}

function GetHospitalName() { //GET ชื่อของโรงพยาบาล
    $('input[name=hospital_type]').click(function () {
        var StatusProvince = $('input[name=hospital_type]:checked').val();
        if(StatusProvince === "T") {
            var types = "รัฐบาล";
        }else {
            var types = "เอกชน";
        }
        $.ajax({
            url: BASE_URL +'/recruitment/list/place',
            data : {type: types},
            type: 'GET',
            dataType: 'json',
            cache: false,
            success: function(data) {
                var option = '';
                $.each(data, function(k,v){
                    option += '<option value="'+v.smom_place_id+'">'+v.name+'</option>';
                });
                var id = "child_hospital";
                var name = "child_hospital[]";
                var label = "ชื่อโรงพยาบาล";
                var alert = "กรุณาระบุชื่อโรงพยาบาล";
                var labelselect = "กรุณาระบุชื่อโรงพยาบาล";
                var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
                $('#hospital_name').html(res);
                conditionValidation();

                initSelectize($("#"+id));
            }
        });
    });
}

function GetProvince() { //GET จังหวัดของสถานะแม่อันแรก
    $.ajax({
        url: BASE_URL +'/recruitment/list/province',
        // data : {data: "province"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.province+'">'+v.province+'</option>';
            });
            var id = "province";
            var name = "province";
            var label = "จังหวัด";
            var alert = "กรุณาระบุจังหวัด";
            var labelselect = "กรุณาระบุจังหวัด";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#province_address').html(res);
            conditionValidation();

            initSelectize($("#"+id));
        }
    });
}

function GetDistrict(idProvince) { //GET อำเภอของสถานะแม่อันแรก
    $.ajax({
        url: BASE_URL +'/recruitment/list/district',
        data : {province: idProvince},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.district+'">'+v.district+'</option>';
            });
            var id = "district";
            var name = "district";
            var label = "อำเภอ/เขต";
            var alert = "กรุณาระบุอำเภอ/เขต";
            var labelselect = "กรุณาระบุอำเภอ/เขต";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#district_address').html(res);
            conditionValidation();

            initSelectize($("#"+id));
        }
    });
}

function GetSubDistrict(idDistrict) { //GET ตำบลของสถานะแม่อันแรก
    $.ajax({
        url: BASE_URL +'/recruitment/list/subdistrict',
        data : {district: idDistrict},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            var Ozipcode = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.smom_address_id+'">'+v.subdistrict+'</option>';
                Ozipcode = v.zipcode;
            });
            var id = "sub_district";
            var name = "sub_district";
            var label = "ตำบล/แขวง";
            var alert = "กรุณาระบุตำบล/แขวง";
            var labelselect = "กรุณาระบุตำบล/แขวง";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $("#storagezipcode").val(Ozipcode);
            $('#subdistrict').html(res);
            conditionValidation();

            initSelectize($("#"+id));
        }
    });
}

function GetMomProduct() { //GET current_product ของสถานะแรก
    $.ajax({
        url: BASE_URL +'/recruitment/list/mom_product',
        // data : {data: "child_product"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.smom_mom_product_id+'">'+v.title+'</option>';
            });
            var id = "current_product";
            var name = "current_product";
            var label = "ผลิตภัณฑ์นมที่ใช้ปัจุบัน";
            var alert = "กรุณาเลือกผลิตภัณฑ์นมที่ใช้ปัจุบัน";
            var labelselect = "กรุณาเลือกผลิตภัณฑ์นมที่ใช้ปัจุบัน";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#currentproduct').html(res);
            conditionValidation();

           initSelectize($("#"+id));
        }
    });
}

function GetChildProduct() { //GET current_product ของสถานะสอง
    $.ajax({
        url: BASE_URL +'/recruitment/list/child_product',
        // data : {data: "mom_product"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.smom_child_product_id+'">'+v.formula_name+'('+v.product_name+')'+'</option>';
            });
            var id = "current_productSc";
            var name = "current_product";
            var label = "ผลิตภัณฑ์นมที่ใช้ปัจุบัน";
            var alert = "กรุณาเลือกผลิตภัณฑ์นมที่ใช้ปัจุบัน";
            var labelselect = "กรุณาเลือกผลิตภัณฑ์นมที่ใช้ปัจุบัน";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#currentproductSc').html(res);
            conditionValidation();

            initSelectize($("#"+id));
        }
    });
}

function GetPreviousProduct() { //GET previous_product ของสถานะอันสอง
    $.ajax({
        url: BASE_URL +'/recruitment/list/child_product',
        // data : {data: "child_product"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.smom_child_product_id+'">'+v.formula_name+'('+v.product_name+')'+'</option>';
            });
            var id = "previous_product";
            var name = "previous_product";
            var label = "ผลิตภัณฑ์นมที่ใช้ก่อนหน้านี้";
            var alert = "กรุณาเลือกผลิตภัณฑ์นมที่ใช้ก่อนหน้านี้";
            var labelselect = "กรุณาเลือกผลิตภัณฑ์นมที่ใช้ก่อนหน้านี้";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#previousproduct').html(res);
            conditionValidation();

            initSelectize($("#"+id));
        }
    });
}

function GetReason() { //GET reason ของสถานะอันสอง
    $.ajax({
        url: BASE_URL +'/recruitment/list/reason',
        // data : {data: "reason"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.smom_reson_change_id+'">'+v.remark+'</option>';
            });
            var id = "reason";
            var name = "reason";
            var label = "สาเหตุที่เปลี่ยนนม";
            var alert = "กรุณาระบุสาเหตุที่เปลี่ยนนม";
            var labelselect = "กรุณาระบุสาเหตุที่เปลี่ยนนม";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#reasonSc').html(res);
            conditionValidation();

            initSelectize($("#"+id));
        }
    });
}

function GetProvinceSc() { //GET จังหวัดของสถานะแม่อันสอง
    $.ajax({
        url: BASE_URL +'/recruitment/list/province',
        // data : {data: "province"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.province+'">'+v.province+'</option>';
            });
            var id = "provinceSc";
            var name = "province";
            var label = "จังหวัด";
            var alert = "กรุณาระบุจังหวัด";
            var labelselect = "กรุณาระบุจังหวัด";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#provinceSc_address').html(res);
            conditionValidation();

            initSelectize($("#"+id));
            
        }
    });
}

function GetDistrictSc(idProvince) { //GET อำเภอของสถานะแม่อสอง
    $.ajax({
        url: BASE_URL +'/recruitment/list/district',
        data : {province: idProvince},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.district+'">'+v.district+'</option>';
            });
            var id = "districtSc";
            var name = "district";
            var label = "อำเภอ/เขต";
            var alert = "กรุณาระบุอำเภอ/เขต";
            var labelselect = "กรุณาระบุอำเภอ/เขต";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $('#districtSc_address').html(res);
            conditionValidation();

            initSelectize($("#"+id));
        }
    });
}

function GetSubDistrictSc(idDistrict) { //GET ตำบลของสถานะแม่อสอง
    $.ajax({
        url: BASE_URL +'/recruitment/list/subdistrict',
        data : {district: idDistrict},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {
            var option = '';
            var OzipcodeSc = '';
            $.each(data, function(k,v){
                option += '<option value="'+v.smom_address_id+'">'+v.subdistrict+'</option>';
                OzipcodeSc = v.zipcode;
            });
            var id = "sub_districtSc";
            var name = "sub_district";
            var label = "ตำบล/แขวง";
            var alert = "กรุณาระบุตำบล/แขวง";
            var labelselect = "กรุณาระบุตำบล/แขวง";
            var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
            $("#storagezipcodeSc").val(OzipcodeSc);
            $('#subdistrictSc').html(res);
            conditionValidation();

            initSelectize($("#"+id));
        }
    });
}

function GetTreatmentRights() { //GET สิทธิ์
    var data =  [
            {
                "value": "1",
                "label": "ใช้สิทธิ์ 30 บาท"
            },
            {
                "value": "2",
                "label": "ใช้สิทธิ์ประกันสังคม"
            },
            {
                "value": "3",
                "label": "ชำระค่ารักษาพยาบาลเอง"
            }
        ];
    var option = '';
    $.each(data, function(k,v){
        option += '<option value="'+v.value+'">'+v.label+'</option>';
    });
    var id = "treatment";
    var name = "child_treatment[]";
    var label = "สิทธิ์การรักษาพยาบาล";
    var alert = "กรุณาระบุสิทธิ์การรักษาพยาบาล";
    var labelselect = "กรุณาระบุสิทธิ์การรักษาพยาบาล";
    var res = createHtmlDropdown(id, name, label, option, alert, labelselect);
    $('#treatment_rights').html(res);
    conditionValidation();
    initSelectize($("#"+id));
}


function MultiAjax(Choice) {
    var province = $.ajax({
        url: BASE_URL +'/recruitment/list/province',
        // data : {data: "province"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {}
    });
    var place_Gov = $.ajax({
        url: BASE_URL +'/recruitment/list/place',
        data : {type: "รัฐบาล"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {}
    });

    var place_Pri = $.ajax({
        url: BASE_URL +'/recruitment/list/place',
        data : {type: "เอกชน"},
        type: 'GET',
        dataType: 'json',
        cache: false,
        success: function(data) {}
    });

    $.when( province , place_Gov, place_Pri ).done(function( pr, p_gov, p_pri ) {
        var option_province = '';
            $.each(pr[0], function(key,val){
            option_province += '<option value="'+val.smom_address_id+'">'+val.province+'</option>';
        });
        var id_province = "child_province2";
        var name_province = "child_province[]";
        var label_province = "จังหวัด";
        var alert_province = "กรุณาระบุจังหวัด";
        var labelselect_province = "กรุณาระบุจังหวัด";
        var res_province = createHtmlDropdown(id_province, name_province, label_province, option_province, alert_province, labelselect_province);


        var option_place_gov = '';
        $.each(p_gov[0], function(k,v){
            option_place_gov += '<option value="'+v.smom_place_id+'">'+v.name+'</option>';
        });
        var id_place_gov = "child_hospital2";
        var name_place_gov = "child_hospital[]";
        var label_place_gov = "ชื่อโรงพยาบาล";
        var alert_place_gov = "กรุณาระบุชื่อโรงพยาบาล";
        var labelselect_place_gov = "กรุณาระบุชื่อโรงพยาบาล";
        var res_place_gov = createHtmlDropdown(id_place_gov, name_place_gov, label_place_gov, option_place_gov, alert_place_gov, labelselect_place_gov);

        var option_place_pri = '';
        $.each(p_pri[0], function(k,v){
            option_place_pri += '<option value="'+v.smom_place_id+'">'+v.name+'</option>';
        });
        var id_place_pri = "child_hospital2";
        var name_place_pri = "child_hospital[]";
        var label_place_pri = "ชื่อโรงพยาบาล";
        var alert_place_pri = "กรุณาระบุชื่อโรงพยาบาล";
        var labelselect_place_pri = "กรุณาระบุชื่อโรงพยาบาล";
        var res_place_pri = createHtmlDropdown(id_place_pri, name_place_pri, label_place_pri, option_place_pri, alert_place_pri, labelselect_place_pri);

        var data =  [
            {
                "value": "1",
                "label": "ใช้สิทธิ์ 30 บาท"
            },
            {
                "value": "2",
                "label": "ใช้สิทธิ์ประกันสังคม"
            },
            {
                "value": "3",
                "label": "ชำระค่ารักษาพยาบาลเอง"
            }
        ];
        var option_treatment = '';
        $.each(data, function(k,v){
            option_treatment += '<option value="'+v.value+'">'+v.label+'</option>';
        });
        var id_treatment = "child_treatment";
        var name_treatment = "child_treatment[]";
        var label_treatment = "สิทธิ์การรักษาพยาบาล";
        var alert_treatment = "กรุณาระบุสิทธิ์การรักษาพยาบาล";
        var labelselect_treatment = "กรุณาระบุสิทธิ์การรักษาพยาบาล";
        var res_treatment = createHtmlDropdown(id_treatment, name_treatment, label_treatment, option_treatment, alert_treatment, labelselect_treatment);

        if(Choice ==  "T") {
            CreateElmentFromT(res_province, res_place_gov, res_place_pri, res_treatment);
        }else {
            CreateElmentFromN(option_province, option_place_gov, option_place_pri, option_treatment);
        }
    });
}

function createHtmlDropdown(id, name, label, data, alert, labelselect){
    var str = '';
    str += '<label for="'+id+'" class="label-position-input font-xregular">'+label+'</label>';
        str += '<div class="text-input-position input-selectize font-xregular" id="select-'+id+'">';
        str += '<select id="'+id+'" class="validation-input" name="'+name+'">';
            str += '<option value="">'+labelselect+'</option>';
            str += data;
        str += '</select>';
        // str += '<span class="fa-sort-desc"></span>';
    str += '</div>';
    str += '<span class="help-block" id="help-block-'+id+'">'+alert+'</span>';

    return str;
}

function CreateElmentFromT(province, place_gov, place_pri, treatment) {
    var html = "";
    html += '<div class="element-detail">';
        html += '<span class="fs-detail font-xregular fs-title-md tracks">บุตรคนที่ 1</span>';
        html += '<div class="from-inputtext">';
            html += '<label for="firstname2" class="label-position-input font-xregular">ชื่อบุตร ภาษาไทย</label>';
            html += '<input type="text" class="text-input-position font-xregular validation-input" id="firstname2" name="firstname[]" placeholder="กรุณาระบุชื่อ">';
            html += '<span class="help-block" id="">กรุณาระบุชื่อ</span>';
        html += '</div>';
        html += '<div class="from-inputtext">';
            html += '<label for="lastname2" class="label-position-input font-xregular">นามสกุลบุตร ภาษาไทย</label>';
            html += '<input type="text" class="text-input-position font-xregular validation-input" id="lastname2" name="lastname[]" placeholder="กรุณาระบุนามสกุล">';
            html += '<span class="help-block" id="">กรุณาระบุนามสกุล</span>';
        html += '</div>';
        html += '<div class="from-inputtext">';
            html += '<label for="child_date2" class="label-position-input font-xregular">วันเกิดบุตร</label>';
            html += '<input type="text" class="text-input-position font-xregular validation-input bd_baby" id="child_date2" name="child_date[]" placeholder="กรุณาระบุวันเกิด" readonly>';
            html += '<span class="help-block" id="">กรุณาระบุวันเกิด</span>';
            html += '<div class="stydate-icon"></div>';
        html += '</div>';
        html += '<div class="from-inputtext">';
            html += '<label for="gender2" class="label-position-input font-xregular">เพศ</label>';
            html += '<div class="text-input-position input-selectize font-xregular" id="gender2">';
                html += '<select id="sax_baby" class="validation-input input-gender" name="gender[]">';
                    html += '<option value="">กรุณาระบุเพศ</option>';
                    html += '<option value="male">ชาย</option>';
                    html += '<option value="female">หญิง</option>';
                html += '</select>';
                html += '<span class="fa-sort-desc"></span>';
            html += '</div>';
            html += '<span class="help-block" id="">กรุณาระบุเพศ</span>';
        html += '</div>';
    html += '<br>';
    html +='<div class="from-radio">';
        html +='<span class="fs-detail font-xregular fs-title-ssm">สถานที่คลอดบุตร</span>';
        html +='<div class="inline-radio inline-radio-left">';
            html +='<input class="radio-position validation-input" type="radio" name="hospital_type_bd" id="hospital_type_bd1" value="T">';
            html +='<label class="label-position label-position-sm-font font-xregular" for="hospital_type_bd1"><span></span>โรงพยาบาลรัฐบาล</label>';
        html +='</div>';
        html +='<div class="inline-radio inline-radio-left">';
            html +='<input class="radio-position validation-input" type="radio" name="hospital_type_bd" id="hospital_type_bd2" value="F">';
            html +='<label class="label-position label-position-sm-font font-xregular" for="hospital_type_bd2"><span></span>โรงพยาบาลเอกชน</label>';
        html += '</div>';
    html += '</div>';
    html += '<div class="from-inputtext">';
        html += ""+treatment+"";
    html += '</div>';
    html += '<div class="from-inputtext hidden-xs">';
    html += '</div>';
    html += '<div class="from-inputtext">';
        html += ""+province+"";
    html += '</div>';
    html += '<div class="from-inputtext" id="TypePl">';
        html += '<label for="" class="label-position-input font-xregular">ชื่อโรงพยาบาล</label>';
        html += '<input type="text" class="text-input-position font-xregular validation-input" placeholder="กรุณาระบุชื่อโรงพยาบาล" disabled>';
        html += '<span class="help-block" id="">กรุณาระบุชื่อโรงพยาบาล</span>';
    html += '</div>';
    html += '</div>';
    $("#quarity_boy").prop('disabled', true);
    $("#quarity_boy").val('');
    $('.clone-form').html(html);

    initSelectize($(".clone-form select"));

    $('.bd_baby').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        yearRange:"c-75:c+0"
    });
    conditionValidation();
    $( ".from-inputtext" ).on("change","select", function() {
        if($(this).val() != ""){
            $(this).addClass('selected');
        }else{
            $(this).removeClass('selected');
        }
    });
    $('input[name=hospital_type_bd]').click(function () {
        var TypeProvince = $('input[name=hospital_type_bd]:checked').val();
        if(TypeProvince == "T") {
            $('#TypePl').html(place_gov);
        }else {
            $('#TypePl').html(place_pri);
        }

        initSelectize($("#child_hospital2"));
    });
}

function CreateElmentFromN(option_province, option_place_gov, option_place_pri, option_treatment) {
    $("#quarity_boy").prop('disabled', false);
    $('#quarity_boy').on('keyup change', function(){
        var elements = document.getElementsByClassName('element-detail');
        var quarity =  $(this).val();
        if(quarity > elements.length) {
            //ถ้าquarity มีค่ามากกว่า elements จะทำเพิ่ม
            var countCreate = quarity
            var rs = "";
            var i = 1;
            var name_province = "child_province[]";
            var label_province = "จังหวัด";
            var alert_province = "กรุณาระบุจังหวัด";
            var labelselect_province = "กรุณาระบุจังหวัด";

            var name_place = "child_hospital[]";
            var label_place = "ชื่อโรงพยาบาล";
            var alert_place = "กรุณาระบุชื่อโรงพยาบาล";
            var labelselect_place = "กรุณาระบุชื่อโรงพยาบาล";

            var arr_res_place_gov = [];
            var arr_res_place_pri = [];

            var name_treatment = "child_treatment[]";
            var label_treatment = "สิทธิ์การรักษาพยาบาล";
            var alert_treatment = "กรุณาระบุสิทธิ์การรักษาพยาบาล";
            var labelselect_treatment = "กรุณาระบุสิทธิ์การรักษาพยาบาล";


            while (i <= countCreate) {
                var id_province = "child_province2"+i;
                var id_place = "child_hospital2"+i;
                var id_treatment = "child_treatment2"+i;
                var res_province = createHtmlDropdown(id_province, name_province, label_province, option_province, alert_province, labelselect_province);
                var res_place_gov = createHtmlDropdown(id_place, name_place, label_place, option_place_gov, alert_place, labelselect_place);
                var res_place_pri = createHtmlDropdown(id_place, name_place, label_place, option_place_pri, alert_place, labelselect_place);
                var res_treatment = createHtmlDropdown(id_treatment, name_treatment, label_treatment, option_treatment, alert_treatment, labelselect_treatment);


                arr_res_place_gov[i] = createHtmlDropdown(id_place, name_place, label_place, option_place_gov, alert_place, labelselect_place);
                arr_res_place_pri[i] = createHtmlDropdown(id_place, name_place, label_place, option_place_pri, alert_place, labelselect_place);

                rs += '<div class="element-detail">';
                rs += '<span class="fs-detail font-xregular fs-title-md tracks">บุตรคนที่ '+i+'</span>';
                rs += '<div class="from-inputtext">';
                rs += '<label for="fristname_baby'+i+'" class="label-position-input font-xregular">ชื่อบุตร ภาษาไทย</label>';
                rs += '<input type="text" class="text-input-position font-xregular validation-input" id="fristname_baby'+i+'" name="firstname[]" placeholder="กรุณาระบุชื่อ">';
                rs += '<span class="help-block" id="">กรุณาระบุชื่อ</span>';
                rs += '</div>';
                rs += '<div class="from-inputtext">';
                rs += '<label for="lastname_baby'+i+'" class="label-position-input font-xregular">นามสกุลบุตร ภาษาไทย</label>';
                rs += '<input type="text" class="text-input-position font-xregular validation-input" id="lastname_baby'+i+'" name="lastname[]" placeholder="กรุณาระบุนามสกุล">';
                rs += '<span class="help-block" id="">กรุณาระบุนามสกุล</span>';
                rs += '</div>';
                rs += '<div class="from-inputtext">';
                rs += '<label for="bd_baby'+i+'" class="label-position-input font-xregular">วันเกิดบุตร</label>';
                rs += '<input type="text" class="text-input-position font-xregular validation-input bd_baby" id="bd_baby'+i+'" name="child_date[]" placeholder="กรุณาระบุวันเกิด" readonly>';
                rs += '<span class="help-block" id="">กรุณาระบุวันเกิด</span>';
                rs += '<div class="stydate-icon"></div>';
                rs += '</div>';
                rs += '<div class="from-inputtext">';
                rs += '<label for="sax_baby'+i+'" class="label-position-input input-selectize font-xregular">เพศ</label>';
                rs += '<div class="text-input-position input-selectize font-xregular" id="select-sax_baby'+i+'">';
                rs += '<select id="sax_baby'+i+'" class="validation-input input-gender" name="gender[]">';
                rs += '<option value="">กรุณาระบุเพศ</option>';
                rs += '<option value="male">ชาย</option>';
                rs += '<option value="female">หญิง</option>';
                rs += '</select>';
                rs += '<span class="fa-sort-desc"></span>';
                rs += '</div>';
                rs += '<span class="help-block" id="">กรุณาระบุเพศ</span>';
                rs += '</div>';
                rs += '<br>';
                rs +='<div class="from-radio">';
                rs +='<span class="fs-detail font-xregular fs-title-ssm">สถานที่คลอดบุตร</span>';
                rs +='<div class="inline-radio inline-radio-left">';
                rs +='<input class="radio-position validation-input chk_hospital" data-id="'+i+'" type="radio" name="hospital_type_bd'+i+'" id="hospital_type_bd1'+i+'" value="T">';
                rs +='<label class="label-position label-position-sm-font font-xregular" for="hospital_type_bd1'+i+'"><span></span>โรงพยาบาลรัฐบาล</label>';
                rs +='</div>';
                rs +='<div class="inline-radio inline-radio-left">';
                rs +='<input class="radio-position validation-input chk_hospital" data-id="'+i+'" type="radio" name="hospital_type_bd'+i+'" id="hospital_type_bd2'+i+'" value="F">';
                rs +='<label class="label-position label-position-sm-font font-xregular" for="hospital_type_bd2'+i+'"><span></span>โรงพยาบาลเอกชน</label>';
                rs += '</div>';
                rs += '</div>';
                rs += '<div class="from-inputtext">';
                rs += ""+res_treatment+"";
                rs += '</div>';
                rs += '<div class="from-inputtext hidden-xs">';
                rs += '</div>';
                rs += '<div class="from-inputtext">';
                rs += ""+res_province+"";
                rs += '</div>';
                rs += '<div class="from-inputtext repeat_hospital">';
                rs += '<label for="" class="label-position-input font-xregular">ชื่อโรงพยาบาล</label>';
                rs += '<input type="text" class="text-input-position font-xregular validation-input" placeholder="กรุณาระบุชื่อโรงพยาบาล" disabled>';
                rs += '<span class="help-block" id="">กรุณาระบุชื่อโรงพยาบาล</span>';
                rs += '</div>';
                rs += '</div>';
                i++;
            }
        $('.clone-form').html(rs);
        initSelectize($(".clone-form select"));

        $('.bd_baby').datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: "dd/mm/yy",
            yearRange:"c-75:c+0"
        });
        conditionValidation();
        $( ".from-inputtext" ).on("change","select", function() {
            if($(this).val() != ""){
                $(this).addClass('selected');
            }else{
                $(this).removeClass('selected');
            }
        });
        $('.chk_hospital').click(function () {
            var inputName = $(this).attr('name');
            var id = $(this).attr('data-id');
            var TypeProvince = $('input[name='+inputName+']:checked').val();
            if(TypeProvince == "T") {
                $(this).parents('.element-detail').find('.repeat_hospital').html(arr_res_place_gov[id]);
                initSelectize($(this).parents('.element-detail').find('select').not('.selectized'));
                // $('#TypePl2').html(res_place_gov);
            }else {
                $(this).parents('.element-detail').find('.repeat_hospital').html(arr_res_place_pri[id]);
                initSelectize($(this).parents('.element-detail').find('select').not('.selectized'));
                // $('#TypePl2').html(res_place_pri);
            }
        });
        }else if(quarity < elements.length) {
            //ถ้าquarity มีค่าน้อยกว่า elements จะลบออก
            $.each($(".element-detail"),function(k,v){
                if(k > quarity-1) {
                    $(this).remove();
                }
            });
        }
    });
}

function ModalPoricy() {
    $('.from-inputtext, .inline-radio').on("focus touchstart click",".validation-input", function(event) {
        if(modal_Status == false) {
            checkStatus1 = $('#checkStatus1').prop('checked',false);
            checkStatus2 = $('#checkStatus2').prop('checked',false);
            checkStatus3 =$('#checkStatus3').prop('checked',false);
            var modal = document.getElementById('varlidate-status-mom');
            modal.style.display = "block";
            modal_Status = true;
        }
    });
}

function setValueFrm1(fieldset1){
    $.each(fieldset1, function(k,v){

        if(v.name ==  "firstname"){
            objData.firstname = v.value;
        }
        else if(v.name == "lastname"){
            objData.lastname = v.value;
        }
        else if(v.name == "birthday"){
            objData.birthday = v.value;
        }
        else if(v.name == "mobile"){
            objData.mobile = v.value;
        }
        else if(v.name == "occupation"){
            objData.occupation = v.value;
        }
        else if(v.name == "salary"){
            objData.salary = v.value;
        }
        else if(v.name == "status_mom"){
            objData.status_mom = v.value;
        }
        else if(v.name == "email"){
            objData.email = v.value;
        }
        else if(v.name == "line_id"){
            objData.line_id = v.value;
        }
        else if(v.name == "advertisement") {
            objData.advertisement = v.value;
        }
    });
}

function setValueFrm2(fieldset2){
    // var d = new Date();
    // var n = d.getFullYear();
    // var m = d.getMonth() + 1;
    // var selfdate = "";
    // var selfdatesplit = "";

    objData.has_one_child = "T";
    var child = {
        child_date:"",
        firstname:"",
        lastname:"",
        gender:"",
        hospital:"",
        hospital_type:"", //ส่วนที่แก้
        calu_age: "", //ส่วนที่แก้
        province:"",
        treatment_rights:""
    };
    var arrChild =  [];
    $.each(fieldset2, function(k,v){
        if(v.name == "child_date[]"){
            child.child_date = v.value;
            child.calu_age = Cul_Date_Age(v.value);
            // selfdate = v.value;
            // selfdatesplit = selfdate.split('/');
            // child.calu_age = n - selfdatesplit[2];
            // child.calu_age = ((n * 12)+ m) - ((selfdatesplit[2]) * 12 + parseInt(selfdatesplit[1]));

        }
        else if(v.name == "child_date[]"){
            child.child_date = v.value;
            child.calu_age = Cul_Date_Age(v.value);
            // selfdate = v.value;
            // selfdatesplit = selfdate.split('/');
            // child.calu_age = n - selfdatesplit[2];
            // child.calu_age = ((n * 12)+ m) - ((selfdatesplit[2]) * 12 + parseInt(selfdatesplit[1]));
        }
        else if(v.name == "child_gender[]" || v.name == "gender[]"){
            child.gender = v.value;
        }
        else if(v.name == "child_province[]"){
            child.province = v.value;
        }
        else if(v.name == "child_hospital[]"){
            child.hospital = v.value;
        }
        else if(v.name == "child_firstname[]" || v.name == "lastname[]"){
            child.lastname = v.value;
        }
        else if(v.name == "firstname[]"){
            child.firstname = v.value;
        }
        else if(v.name == "child_treatment[]") {
            child.treatment_rights = v.value;
        }
        else if(v.name == "address"){
            objData.address = v.value;
        }
        else if(v.name == "moo"){
            objData.moo = v.value;
        }
        else if(v.name == "alley"){
            objData.alley = v.value;
        }
        else if(v.name == "road"){
            objData.road = v.value;
        }
        else if(v.name == "sub_district"){
            objData.sub_district = v.value;
            objData.zipcode = v.value;
            objData.district = v.value;
            objData.province = v.value;
        }
        else if(v.name == "has_one_child") {
            objData.has_one_child = v.value;
        }
        else if(v.name == "hospital_type") {
            child.hospital_type = v.value;
        }
    });

    arrChild.push(child);
    objData.child = arrChild;
}

function setValueFrm3(fieldset3){
    $.each(fieldset3, function(k,v){
        if(v.name == "current_product"){
            objData.current_product = v.value;
        }
        else if(v.name == "web_page"){
            objData.web_page = v.value;
        }
        else if(v.name == "previous_product"){
            objData.previous_product = v.value;
        }
        else if(v.name == "reason"){
            objData.reason_to_change = v.value;
        }
        else if(v.name == "product_name"){
            objData.smom_product_name = v.value;
        }else if(v.name == "other_product") {
            objData.smom_product_name_other = v.value;
        }
    });
}

function setValueFrmEx(fieldset2) {
    $.each(fieldset2, function(k,v){
        if(v.name == "address"){
            objData.address = v.value;
        }
        else if(v.name == "moo"){
            objData.moo = v.value;
        }
        else if(v.name == "alley"){
            objData.alley = v.value;
        }
        else if(v.name == "road"){
            objData.road = v.value;
        }
        else if(v.name == "sub_district"){
            objData.sub_district = v.value;
            objData.zipcode = v.value;
            objData.district = v.value;
            objData.province = v.value;
        }
        else if(v.name == "has_one_child") {
            objData.has_one_child = v.value;
        }
    });
}

function numberOnly(){
    $(".number-only").keydown(function (e) {
        if (e.key !== undefined && !e.key.match(/[0-9]/) && !(e.key == "Backspace") && !(e.key == "Enter") && !(e.key == "Tab") && !(e.key == "ArrowLeft") && !(e.key == "ArrowRight")){
                return false;
            }
    });
}

function initSelectize(elm){
    elm.selectize({});
}

function Cul_Date_Age(date_born) {
    var initDate = new Date();
    var day = initDate.getDate();
    var month = initDate.getMonth() + 1;
    var year = initDate.getFullYear();
    var fulldate = day + "/" + month + "/" + year;

    var rs_date_now = fulldate;
    var re_date_born = date_born;
    re_date_born = re_date_born.split("/");
    rs_date_now = rs_date_now.split("/");
    sDate = new Date(re_date_born[2],re_date_born[1]-1,re_date_born[0]);
    eDate = new Date(rs_date_now[2],rs_date_now[1]-1,rs_date_now[0]);
    var daysDiff = Math.round((eDate-sDate)/86400000);
    return daysDiff;
}
