$(document).ready(function(){
	autoScrollTo();
	facebookClick();
});

function autoScrollTo(){
	var url = window.location.href;
	var top = 0;
	if(url.indexOf("#privilege") > 0){
		top = $("#privilege").offset().top;
	}

	if(url.indexOf("#register") > 0){
		top = $("#section-register").offset().top;
		if(isMobileDevice()){
			top -= $("#login_bar").height();
			top -= $("#menu-mobile").height();
		}
	}
	setTimeout(function(){
		$("html,body").scrollTop(top);
	},50)
}

function facebookClick(){
	$("#login_bar .facebook a").click(function(){
		$("#sign_in_case").click();
		autoScrollTo();
	})
}

function popupActive(){
	$("body").addClass("register");

	var htmlPopup = '<div class="popup-panel"><div class="popup-panel-inner FMonX"><div class="popup-panel-box"><div class="popup-close"></div><div class="popup-title-image"></div><div class="popup-title-text FMonXMed">เพียงพิมพ์ <span class="color-red">“ยืนยัน”</span> พร้อม ชื่อ-สกุล เบอร์โทรศัพท์</div><div class="popup-detail-panel"><div class="popup-text-box">ข้าพเจ้าอายุเกิน 13 ปีเเละประสงค์ที่รับข้อมูลเกียวกับการส่งเสริมการขาย ข้อเสนอเเละส่วนลดจาก S-MomClub รวมถึงการติดต่อสื่อสารจากชื่อทางการค้าอื่นๆ ของ Nestlé ข้าพเจ้ายอมรับว่า Nestlé จะประมวลข้อมูลส่วนของข้าพเจ้า ตาม<a class="linkout" href="'+BASE_LANG+'page/privacy" target="_blank">ประกาศเกี่ยวกับความเป็นส่วนตัวของ Nestlé</a></div><div class="popup-condition"><div class="popup-checkbox"></div><div class="popup-text FMonX">อ่านและยอมรับ ข้อตกลงเเละเงื่อนไข สําหรับเว็บไซต์ของเนสท์เล่ <br class="hidden-xs"/><a class="linkout" href="'+BASE_LANG+'page/terms-and-conditions" target="_blank">ดูรายละเอียดเพิ่มเติมได้ที่นี่</a></div></div><a class="popup-btn FX disabled" href="http://line.me/ti/p/@S-MomClub" alt="S-MomClub" target="_blank">ตกลง</a></div></div></div>';

	$("#content-panel").append(htmlPopup);

	$(".popup-checkbox").click(function(){
		if($(this).hasClass("active")){
			if(!$(".popup-btn").hasClass("disabled")){
				$(".popup-btn").addClass("disabled");
			}
			$(this).removeClass("active");
		}else{
			$(this).addClass("active");
			$(".popup-btn").removeClass("disabled");
		}
	});

	$(".popup-btn").click(function(){
		if($(this).hasClass("disabled")){
			return false;
		}else{
			closePopup();
			return true;
		}
	});

	$(".popup-close").click(function(){
		closePopup();
	});
}

function closePopup(){
	$("body").removeClass("register");
	$(".popup-checkbox").removeClass("active");
	$(".popup-btn").addClass("disabled");
	$(".popup-panel").remove();
}