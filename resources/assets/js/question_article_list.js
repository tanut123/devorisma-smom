var limit = 6;
var page = 0;
var tmp_start_width = "";
var data_category = $("input#category").val();

$(document).ready(function () {
	tmp_start_width = $(window).width();
	if($(window).width() < 768){
		limit = 3;
	}
	ajaxList();
	$(".load-more").click(function () {
		ajaxList();
	});
	$(window).resize(function () {
		if(tmp_start_width > 767 && $(window).width() < 768){
			limit = 3;
			tmp_start_width = $(window).width();
			page = 0;
			$(".list-panel").html(" ");
			$(".load-more").removeClass("load");
			ajaxList();

		}
		if(tmp_start_width < 768 && $(window).width() > 767){
			limit = 6;
			tmp_start_width = $(window).width();
			page = 0;
			$(".list-panel").html(" ");
			$(".load-more").removeClass("load");
			ajaxList();
		}
	});
});

function getList() {
	return $(".list-panel .list-item ").length;
}

function ajaxList() {
	$(".list-panel").append(loadingAjax());
	$(".load-more").removeClass("load");
	page = page +1;
	$.ajax({
		url: BASE_LANG + "qa/" + data_category + "/loadpage",
		type: "POST",
		data: {
			"page" : page,
			"limit" : limit,
			"category" : data_category
		},
		dataType: "json"
	})
	.success(function (data) {
		$(".global-loading").remove();
		$(".list-panel").append(data.ajax_html);
		chk_loadmore(data["has_loadmore"]);
		setHeightDescription();
	})
}

function chk_loadmore(has_loadmore) {
	if(has_loadmore == false){
		$(".load-more").removeClass("load");
	}else{
		$(".load-more").addClass("load");
	}
}

function setHeightDescription(){
	$(".list-item").each(function(a,b){
		var this_item = $(b);
		var height = this_item.find(".txt-list-panel .list-title").innerHeight();
		var description = this_item.find(".txt-list-panel .short-desc");
		if(description.text().trim() != ""){
			var heightMod = Math.round(height / 30);
			if(heightMod == 1){
				description.JJJud({"max_rows":2});
			}else if(heightMod == 2){
				description.JJJud({"max_rows":1});
			}else{
				description.hide();
			}
		}
	});
}